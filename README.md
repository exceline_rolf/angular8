### Biggest changes

`@ngx-config`, `us-date-picker` and `us-data-table` not updated for `Angular 8`. 
`@ViewChild()` needs a static boolean passed as second argument. [More info](https://angular.io/guide/static-query-migration)

### Upgrade from 7 to 8

Make sure you have a working copy of Exceline with Angular 7 installed. See other guide for this.
There's no need to delete the `node_modules` or `package-lock.json` this time.

First upgrade the global angular version first:

~~~~bash
npm install -g @angular/cli
~~~~

Since `TypeScript` has a newer version that Angular 8 does not support, install correct version:

~~~~bash
npm install typescript@3.4.5
~~~~

Then run the following:

~~~~bash
ng update
  @angular/cli
  @angular/core
~~~~

If you get any `red`-coloured errors after running this, fix them and run 

~~~~bash
ng update @angular/core --from 7 --to 8 --migrate-only
~~~~

Else it should update a bunch of content queries automatically. You may have to update some manually. [More info](https://angular.io/guide/static-query-migration)

Next, update `@ng-bootstrap� to newest:

~~~~bash
npm install @ng-bootstrap/ng-bootstrap@latest
~~~~

Alot of imports are changed when updating to Angular 8. These has to be changed.

Change the `"peerDependencies"` in both `package.json` files in `node_modules/@ngx-config/core` and `node_modules/@ngx-config/http-loader` from:

~~~~json
"peerDependencies": {
    "@angular/common": ">=7.0.0 <8.0.0",
    "@angular/core": ">=7.0.0 <8.0.0",
    "@angular/platform-browser-dynamic": ">=7.0.0 <8.0.0",
    "rxjs": ">=6.0.0",
    "@ngx-config/core": ">=7.0.0 <8.0.0"
  }
~~~~

to

~~~~json
"peerDependencies": {
    "@angular/common": ">=7.0.0 <9.0.0",
    "@angular/core": ">=7.0.0 <9.0.0",
    "@angular/platform-browser-dynamic": ">=7.0.0 <9.0.0",
    "rxjs": ">=6.0.0",
    "@ngx-config/core": ">=7.0.0 <9.0.0"
  }
~~~~

##### Compile/serve errors

**Error**

`Property 'msMatchesSelector' does not exist on type 'Element'` 

**Fix** [^2] [^3]

Since `TypeScript 3.1` removes some vendor specific types from their `lib.d.ts`, `msMatchesSelector` is no longer a property of `Element`. This error occurs in `app/shared/components/us-tree-table/dom-handler.ts` - `line 236`.

To fix this error, create a file named `dom.ie.d.ts` inside `app/shared/components/us-tree-table` with the following content:
~~~~ts
interface Element {
    msMatchesSelector(selectors: string): boolean;
}
~~~~

##### Runtime errors

**Error**

`Endpoint unreachable`

**Fix**

Check if you have the `src/assets/config.json` file. Without it no APIs are defined.

#### Notes

`us-date-picker` will not work in `Angular 8`. I removed all contents of `us-data-picker.component.html` and the app serves.

---

`us-data-table` will not work in `Angular 8`. I have commented out all calls for this in `HTML` and commented out the whole of the `export` body of `us-data-table.component`. 

[^1]: https://bertrandg.github.io/angular-split/#/changelog
[^2]: https://stackoverflow.com/questions/52696154/latest-typescript-breaking-changes-in-lib-dom-ts-file#52697231
[^3]: https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#typescript-31