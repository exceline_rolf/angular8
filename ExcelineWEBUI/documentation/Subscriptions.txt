The prefered method of cleaning up subscriptions is the method from https://stackoverflow.com/questions/38008334/angular-rxjs-when-should-i-unsubscribe-from-subscription/51732897?stw=2#51732897.
More info on rxPipes: https://stackoverflow.com/questions/56748777/rxjs-pipe-with-one-argument?noredirect=1#comment100055733_56748777 and https://reactive.how/rxjs/pipeable-operators


%%%%%%%%%%%%%%%%%% -- Boilerplate code for all components -- %%%%%%%%%%%%%%%%%%

import { takeUntil } from 'rxjs/operators'; <-- Important, as this is the newest directory. As of Rx5.X, operators such as takeUntil are pipe able. This allows for tree-shaking (https://webpack.js.org/guides/tree-shaking/)
import { Subject } from 'rxjs';

private destroy$ = new Subject<void>(); // destroy$ is a private Subject that all subscriptions in the component will be enslaved to.

// Example
this.potatoService.getPotato.pipe( //
  takeUntil(this.destroy$) //  It's important to add the takeUntil operator as the last one to prevent leaks with intermediate observables in the operator chain.
).subscribe(...)


ngOnDestroy() {
  this.destroy$.next(); When destroy$ sends a signal, it sends a 'complete' signal to all slave subscriptions.
  this.destroy$.complete(); Close and unsubscribe from the destroy$ subject.
}

%%%%%%%%%%%%%%%%%% -- Note on calling .unsubscribe -- %%%%%%%%%%%%%%%%%%
It is wholly allowed to  manually unsubscribe in a components logic. The method above will ensure that all subscriptions are completed when the parent component enters the ngOnDestroy life-cycle.

Note on unsubscribing from route (private route: ActivatedRoute):
  The onComplete callback is not run when the subscription is closed. The fix is to include take(1) in the pipe, closing it right away.

this.route.url.pipe(
      take(1), <-- get one value, then unsubscribe.
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.setTitle()
      }
    }, err => {
      console.log(err)
    }, () => {}
    );

%%%%%%%%%%%%%%%%%% -- Importing RXJS into the project -- %%%%%%%%%%%%%%%%%%
The only import paths we should use, are:
- rxjs
- rxjs/operators
- rxjs/testing
- rxjs/webSocket
- rxjs/ajax

When we finally upgrade to v6, should replace 'rxjs' with just 'rxjs'
Thats it. Do NOT use e.g. rxjs/add/operators/XXX <-- No bueno

%%%%%%%%%%%%%%%%%% -- Event emitter -- %%%%%%%%%%%%%%
If manually subscribing to an instance of EventEmitter, you must manually unsubscribe to it aswell.
