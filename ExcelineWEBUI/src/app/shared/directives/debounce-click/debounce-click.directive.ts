import { Directive, OnInit, HostListener, Output, EventEmitter, OnDestroy, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';


@Directive({
  selector: '[appDebounceClick]',
  exportAs: 'appDebounceClick'
})
export class DebounceClickDirective implements OnInit, OnDestroy {
  private clicks = new Subject();
  private destroy$ = new Subject<void>();

  @Input() debounceTime = 500; // Default waiting period. Can be set in html with [debounceTime]="milliseconds"
  @Output() debounceClick = new EventEmitter(); // Function to be called after @debounceTime has passed
  constructor( )  {}

  ngOnInit() {
    this.clicks.pipe(
      debounceTime(this.debounceTime),
      takeUntil(this.destroy$)
    ).subscribe(e => this.debounceClick.emit(e));
  }

  @HostListener('click', ['$event'])
  clickEvent(event) {
    event.preventDefault(); /* These two lines prevent the click */
    event.stopPropagation(); /* event from bubbling up to the parent component. */


    this.clicks.next(event);
  }

ngOnDestroy() {
  this.destroy$.next();
  this.destroy$.complete();
}

}
