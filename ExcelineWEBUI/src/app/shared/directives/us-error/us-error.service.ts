import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Injectable()
export class UsErrorService {

  static validateAllFormFields(formGroup: FormGroup, formErrors: any, validationMessages: any) {
    Object.keys(formGroup.controls).forEach(field => {
      formErrors[field] = '';
      const control = formGroup.get(field);
      if (control && !control.valid) {
        const messages = validationMessages[field];
        for (const key in control.errors) {
          if (control.errors.hasOwnProperty(key)) {
            formErrors[field] += messages[key] + ' ';
          }
        }
      }
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control, formErrors, validationMessages);
      }
    });

  }

  static onValueChanged(formGroup: FormGroup, formErrors: any, validationMessages: any) {
    if (!formGroup) { return; }
    for (const field in formErrors) {
      if (formErrors.hasOwnProperty(field)) {
        const control = formGroup.get(field);
        formErrors[field] = '';
        if (control && !control.valid) {
          const messages = validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

}
