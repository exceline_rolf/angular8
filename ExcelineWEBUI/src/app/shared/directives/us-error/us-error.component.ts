import {
  Component,
  OnInit,
  Input,
  ElementRef,
  Renderer2,
  Injector,
  ComponentFactoryResolver,
  ViewContainerRef,
  NgZone, ComponentRef,
  Output,
  EventEmitter,
  TemplateRef,
  OnDestroy,
  Directive,
} from '@angular/core';
import { ExceErrorConfig } from '../exce-error/exce-error-config';
import { PopupService } from '../exce-error/util/popup';
import { ErrorWindowComponent } from './error-window/error-window.component'
import { positionElements } from '../exce-error/util/positioning';
import { listenToTriggers } from '../exce-error/util/triggers';

let nextId = 0;

@Directive({
  selector: '[us-error]', exportAs: 'us-error',
  providers: []
})

export class UsErrorDirective implements OnInit, OnDestroy {

  private errorMsg: string;
  @Input('errorMsg') set $errorMsg(value: string) {
    this.errorMsg = value;
  }
  get $errorMsg(): string {
    return this.errorMsg;
  }

  private displayError: boolean;
  @Input('displayError') set $displayError(value: boolean) {
    this.displayError = value;
    if (this.displayError) {
      this._renderer.addClass(this._elementRef.nativeElement, 'us-error');
      this.open();
    } else {
      this._renderer.removeClass(this._elementRef.nativeElement, 'us-error');
      this.close();
    }
  }

  get $displayError(): boolean {
    return this.displayError;
  }

  /**
  * Content to be displayed as popover.
  */
  @Input() ngbPopover: string | TemplateRef<any>;

  @Input() container: string;
  /**
   * Placement of a popover. Accepts: "top", "bottom", "left", "right"
   */
  @Input() placement: 'top' | 'bottom' | 'left' | 'right';

  private _ngbPopoverWindowId = `exce-error-${nextId++}`;
  private _popupService: PopupService<ErrorWindowComponent>;
  private _windowRef: ComponentRef<ErrorWindowComponent>;
  private _unregisterListenersFn;
  private _zoneSubscription: any;

  constructor(
    private _elementRef: ElementRef,
    private _renderer: Renderer2,
    injector: Injector,
    componentFactoryResolver: ComponentFactoryResolver,
    viewContainerRef: ViewContainerRef,
    config: ExceErrorConfig,
    ngZone: NgZone) {
    this._popupService = new PopupService<ErrorWindowComponent>(
      ErrorWindowComponent, injector, viewContainerRef, _renderer, componentFactoryResolver);

    if (!this.placement) {
      this.placement = 'top'
    }

    this._zoneSubscription = ngZone.onStable.subscribe(() => {
      if (this._windowRef) {
        this._windowRef = this._popupService.open(this.ngbPopover);
        this._windowRef.instance.$title = this.errorMsg;
        this._windowRef.instance.id = this._ngbPopoverWindowId;
        this._windowRef.instance.placement = this.placement;

        this._renderer.setAttribute(this._elementRef.nativeElement, 'aria-describedby', this._ngbPopoverWindowId);

        if (this.container === 'body') {
          window.document.querySelector(this.container).appendChild(this._windowRef.location.nativeElement);
        }

        this._windowRef.changeDetectorRef.detectChanges();
        this._windowRef.changeDetectorRef.markForCheck();
        positionElements(
          this._elementRef.nativeElement, this._windowRef.location.nativeElement, this.placement,
          this.container === 'body');
      }
    });

  }

  open() {
    if (!this._windowRef) {
      this._windowRef = this._popupService.open(this.ngbPopover);
      this._windowRef.instance.$title = this.errorMsg;
      this._windowRef.instance.id = this._ngbPopoverWindowId;
      this._windowRef.instance.placement = this.placement;

      this._renderer.setAttribute(this._elementRef.nativeElement, 'aria-describedby', this._ngbPopoverWindowId);

      if (this.container === 'body') {
        window.document.querySelector(this.container).appendChild(this._windowRef.location.nativeElement);
      }

      this._windowRef.changeDetectorRef.detectChanges();
      this._windowRef.changeDetectorRef.markForCheck();

      positionElements(
        this._elementRef.nativeElement, this._windowRef.location.nativeElement, this.placement,
        this.container === 'body')
    }
  }

  /**
   * Closes an element’s popover. This is considered a “manual” triggering of the popover.
   */
  close(): void {
    if (this._windowRef) {
      this._renderer.removeAttribute(this._elementRef.nativeElement, 'aria-describedby');
      this._popupService.close();
      this._windowRef = null;
    }
  }

  ngOnInit() {
    this._unregisterListenersFn = listenToTriggers(
      this._renderer, this._elementRef.nativeElement, 'manual', this.open.bind(this), this.close.bind(this),
      this.toggle.bind(this));
  }

  ngOnDestroy() {
    this._zoneSubscription.unsubscribe();
    this.close();
    this._unregisterListenersFn();
  }

  /**
 * Toggles an element’s popover. This is considered a “manual” triggering of the popover.
 */
  toggle(): void {
    if (this._windowRef) {
      this.close();
    } else {
      this.open();
    }
  }

  /**
   * Returns whether or not the popover is currently being shown
   */
  isOpen(): boolean { return this._windowRef != null; }
}
