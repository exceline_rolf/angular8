import { Directive, ElementRef, Input, HostListener, Output, EventEmitter, OnInit } from '@angular/core';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import * as $ from 'jquery';


@Directive({
  selector: '[amountConverter]'
  // tslint:disable-next-line:use-host-property-decorator
})

export class AmountConverterDirective implements OnInit {

  @Output() amountFormatChange: EventEmitter<string> = new EventEmitter<string>();
  private locale: string;

  get $locale(): string {
    return this.locale;
  }

  @Input('locale') set $locale(value) {
    this.locale = value;
    this.format(this.elementRef.nativeElement.value);

  }

  constructor(
    private toolbarService: ExceToolbarService,
    private elementRef: ElementRef
  ) { }

  ngOnInit() { }

  @HostListener('change') onchange() {
    let formattingValue: any = this.elementRef.nativeElement.value;
    if (this.locale === 'nb-NO') {
      const reg = /\,/gi;
      formattingValue = formattingValue.replace(reg, '.');
    }
    this.format(formattingValue);
  }

  format(value: number | string) {
    value = new Intl.NumberFormat(this.locale, { minimumFractionDigits: 2 }).format(Number(value));
    this.amountFormatChange.emit(value);
  }
}
