## 1.	Introduction

This is a **custom validation directive** that we can attach **to get the error messages displayed**. It has the ability to expand the library through customizing the validations.

## 2.	How to Use

1.	Initially import the whole **module** to the project
>	(Which includes ->	 
>	*	`ExceErrorConfig` – injectable;
>	*	`ExceErrorWindow` – component;
>	*	`ExceErro`r - directive)

2.	Bind the `exceError` directive property with the **FormControl** we are associated with
>	For instance: `[exceError] = "complexForm.controls ['lastName']"`
>	Is the required code segment that we need to have to implement it in the **form of** `complexForm` with the **formGroup** lastName.

Now the messages for conventional validations of **min/max length, required fields and pattern** will be given (these can be customized as discussed below).

3.	We can specify the validation type pattern through the  `validationType` input property, so that the validation message will be displayed accordingly. For this to work the `Validator.pattern(...)` need to be used in the field
>   Currently the validation messages are defined for 
-   email   ->  bind the property `validationType`  as email. eg: `[validationType]='email'`
-   postalcode  ->  bind the property `validationType`  as postalcode. eg: `[validationType]='postalcode'`

4.  We can give custom error messages through `validationMesge` input property, so that for pattern validation we can specify the custom validation message.

```
NOTE: IF THE **validationMesge** AND **validationType** BOTH INPUT PROPERTIES ARE SPECIFIED THEN ONLY **validationMesge** WILL BE TAKEN INTO CONSIDERATION
```

with the **FormControl** we are associated with

## 3.	How to Customize
-	The `forbiddenNameValidator` function in `exce-error.ts` does the customization validation at the moment (you can add any similar for further required validations). 
-	For every key press `validate(x: AbstractControl)` method is called (that is inherited from the `Validator` class.
-	`open()`, `close()` and `toggle()` methos in `exce-error.ts` handle the popup window.
-	The template of ExceErrorWindow is loaded as the pop-up template.


>   Backed with :heart: :heart: :heart: by [Unicorn-Solutions](http://www.unicorn-solutions.com/) **Exceline team**


