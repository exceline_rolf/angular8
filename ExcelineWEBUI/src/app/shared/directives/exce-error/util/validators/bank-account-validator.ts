import { FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
export class BankAccountValidator {
    static BankAccount(control: AbstractControl) {

        if (control.value != null) {
            try {
                let sum = 0;
                const weights: number[] = [1, 2, 3, 4, 5, 6, 7, 2, 3, 4, 5, 6, 7];
                const accNo: string = control.value;
                const numberList: number[] = new Array(0);

                for (var i = 0; i < accNo.length; i++) {
                    numberList.push(Number(accNo.charAt(i)));
                }
                numberList.reverse();

                for (var i = 0; i < numberList.length; i++) {
                    sum += numberList[i] * weights[i];
                }

                if (sum % 11 === 0) {
                    return null;
                } else {
                    return { BankAccount: false };
                }

            } catch (e) {
                return { BankAccount: false };
            }
        }
    }
}



