import { FormControl } from '@angular/forms';

export class UsernameValidator {
    static cannotContainSpace(control: FormControl) {

        if (control.value != null) {
            // tslint:disable-next-line:curly
            if (control.value.length > 3)
                return { bankAccount: true };

        }
        return null;
    }
}
