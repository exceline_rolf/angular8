import { CommonHttpService } from './common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Injectable } from '@angular/core';
import { ExceMessageService } from '../components/exce-message/exce-message.service';
import { Subject } from 'rxjs';
import { ShopHomeService } from 'app/modules/shop/shop-home/shop-home.service';
import { Observable } from 'rxjs';
// import { ExceShopService } from 'app/modules/shop/services/exce-shop.service';

@Injectable()
export class ExceSessionService {
    sessionApiURL: any;
  objectvalue: any;
  user: string;
  key: string;
  selectedText: any;
  maxreqlength: number;
  server: string;
  isBankTerminalActive = false;

  public isBankTerminalActiveEvent = new Subject<any>();

  constructor(
    private exceMessageService: ExceMessageService,
    private commonHttpService: CommonHttpService,
    private shopHomeService: ShopHomeService,
    private config: ConfigService
  ) {
    this.sessionApiURL = this.config.getSettings('EXCE_API.SESSION');
  }

  AddSession(command, objectvalue, key, user) {
    this.server = 'localhost:60024';
    this.maxreqlength = 1500;
    this.selectedText = command;
    this.objectvalue = objectvalue;
    this.key = key;
    this.user = user;
    this._speakText();
  }

  private _formatCommand(b, a) {
    return 'http://' + this.server + '/' + b + '/dummy.gif' + a + '&timestamp=' + new Date().getTime() + '&key=' + this.key + '|' + this.user + '|' + this.selectedText;
  }

  private _speakText() {
    const self = this;
    const image = new Image(1, 1);
    image.onerror = function () {
      self._showerror()
    };
    image.src = this._formatCommand('operation', '?value=' + this.objectvalue);
    this.isBankTerminalActive = true;
    if (this.selectedText === 'GYMID') {
      this.isBankTerminalActiveEvent.next(true)
    }

  }

  private _showerror() {
    this.isBankTerminalActive = false;
    // this.shopHomeService.isBankTerminalActive = false;
    this.isBankTerminalActiveEvent.next(false)
    if (this.selectedText === 'MACHINE' && !isNaN(Number(this.objectvalue)) && Number(this.objectvalue) === 1) {
      // dont write
    } else {
      this.exceMessageService.openMessageBox('ERROR',
      {
        messageTitle: 'ERROR',
        messageBody: 'COMMON.COMMUNICATIONERROR'
      });
    }
  }

  getSessionValue(sessionKey: string): Observable<any> {
    const url = this.sessionApiURL + '/GetSessionValue?sessionKey=' + sessionKey
    return this.commonHttpService.GetSessionValue(url, {
      method: 'GET',
      auth: true
    }, '', true);
  }

  getInitialSessionValue(sessionKey: string,  userName: string, count: any): Observable<any> {
    const url = this.sessionApiURL + '/GetSessionValue?sessionKey=' + sessionKey
    return this.commonHttpService.GetSessionValue(url, {
      method: 'GET',
      auth: true
    }, userName, false);
  }

  // private _bufferText(f) {
  //   const self = this;
  //   let c = true;
  //   const b = Math.floor((f.length + this.maxreqlength - 1) / this.maxreqlength);
  //   for (let d = 0; d < b; d++) {
  //     const g = d * this.maxreqlength;
  //     const a = Math.min(f.length, g + this.maxreqlength);
  //     const e = new Image(1, 1);
  //     e.onerror = function () {
  //       self._showerror()
  //     };
  //     e.src = this._formatCommand('buffertext', '?totalreqs=' + b + '&req=' + (d + 1) + '&text=' + f.substring(g, a) + '&clear=' + c); c = false
  //   }
  // }


}

