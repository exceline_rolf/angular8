import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceSchedulerComponent } from './exce-scheduler.component';

describe('ExceSchedulerComponent', () => {
  let component: ExceSchedulerComponent;
  let fixture: ComponentFixture<ExceSchedulerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceSchedulerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceSchedulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
