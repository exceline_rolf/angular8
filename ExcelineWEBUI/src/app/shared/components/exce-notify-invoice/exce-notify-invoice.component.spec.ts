import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceNotifyInvoiceComponent } from './exce-notify-invoice.component';

describe('ExceNotifyInvoiceComponent', () => {
  let component: ExceNotifyInvoiceComponent;
  let fixture: ComponentFixture<ExceNotifyInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceNotifyInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceNotifyInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
