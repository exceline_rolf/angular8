import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  NgModule,
  ModuleWithProviders,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChange,
  ViewContainerRef
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'us-autocomplete',
  templateUrl: './us-autocomplete.component.html',
  styleUrls: ['./us-autocomplete.component.scss']
})
export class UsAutocompleteComponent implements OnInit, OnChanges {
  @Input() items: any[];
  @Input() config: any;
  @Output() selectEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectDefaultEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() inputChangedEvent: EventEmitter<any> = new EventEmitter<any>();
  inputElement: HTMLInputElement;
  value: string;
  candidates: any[];
  candiatesLabels: any[];
  selectedIndex: number;
  showAutoComplete: boolean;
  placeholder: string;
  private sourceField: any;
  private thisElement: HTMLElement;
  @Input() textValue: string;

  autocompleteSearchList = [];
  // [{ id: "NAME", name: "Name :", isNumber: false }, { id: "ADDRESS", name: "Address :", isNumber: false },
  // { id: "MOBILE", name: "Mobile :", isNumber: true }, { id: "EMAIL", name: "Email :", isNumber: true }]

  constructor(elementRef: ElementRef) {
    this.thisElement = elementRef.nativeElement;
    this.selectedIndex = -1;
    this.showAutoComplete = false;
    this.value = '';
  }

  ngOnInit() {

    this.placeholder = 'autocomplete';
    this.inputElement = <HTMLInputElement>(this.thisElement.querySelector('input'));

    if (!this.isNull(this.config)) {
      if (!this.isNull(this.config.placeholder)) {
        this.placeholder = this.config.placeholder;
      }
      if (!this.isNull(this.config.sourceField)) {
        this.sourceField = this.config.sourceField;
      }
    }
   // this.filterItems(this.value);
    this.inputElement.focus();
  }

  ngOnChanges() {
    if (this.value) {
      this.filterItems(this.value);
    }else {
      this.showAutoComplete = false;
    }
  }

  focusEvent() {
    this.selectedIndex = -1;
  }

  public setSearchValue(items: any) {
    this.autocompleteSearchList = items;
    this.filterItems(this.value);
  }

  enterText(event: any) {
    this.showAutoComplete = true;
    const total = this.candidates.length;
    switch (event.keyCode) {
      case 27:
        this.showAutoComplete = false;
        break;
      case 38: // arrow key up
        if (this.selectedIndex === -1) {
          this.selectedIndex = 0;
        }
        this.selectedIndex = (total + this.selectedIndex - 1) % total;
        break;
      case 40: // arrow key down
        this.selectedIndex = (total + this.selectedIndex + 1) % total;
        break;
      case 13:
        if (this.candidates.length > 0) {
          this.onSelect(this.selectedIndex);
        }
        event.preventDefault();
        break;
      default:
        this.value = event.target.value;
        this.inputChangedEvent.emit(this.value);
        break;
    }
    this.filterItems(event.target.value);
  }

  public getTextValue () {
    if (this.selectedIndex !== -1) {
      return this.candidates[this.selectedIndex];
    } else {
      const newItem = {
        id: '',
        name: '',
        searchText: '',
        searchVal: this.textValue !== undefined ? this.textValue : ''
      };
      return newItem;
    }
  }

  onSelect(idx: number) {
    this.showAutoComplete = false;
    if (idx !== -1) {

      this.value = this.candiatesLabels[idx];
      const selectedItem = this.candidates[idx];
      selectedItem.searchText = selectedItem.id + ' : ' + selectedItem.searchVal;
      this.selectEvent.emit(selectedItem);
      this.selectedIndex = -1;
    } else {
      const newItem = {
        id: '',
        name: '',
        searchText: this.getSearchText(this.value),
        searchVal: this.textValue
      };
      this.selectEvent.emit(newItem);
    }

  }

  getSearchText(searchName: string) {
    const searchArray = searchName.split(':');
    if (searchArray.length > 1) {
      return this.autocompleteSearchList.find(x => x.name.split(':')[0].trim() === searchArray[0].trim()).id + ' : ' + searchArray[1];
    }
    return searchName;
  }

  filterItems(search: string) {
    const field = this.sourceField;
    const filterItem = this.filterItem;
    this.items = [];
    const searchList = [];
    let lastCharacter;

    for (let i = 0; i < this.autocompleteSearchList.length; i++) {
      const keywordparts = search.toLowerCase().split(':');

      if (((keywordparts.length > 1 && !isNaN(Number(keywordparts[1]))) || (keywordparts.length === 1 && !isNaN(Number(keywordparts[0]))))
        && this.autocompleteSearchList[i].isNumber) {// string
        const newItem = {
          id: this.autocompleteSearchList[i].id,
          name: this.autocompleteSearchList[i].name,
          searchText: this.autocompleteSearchList[i].name + keywordparts[keywordparts.length - 1],
          searchVal: keywordparts[keywordparts.length - 1]
        };

        lastCharacter = newItem.searchText[newItem.searchText.length - 1];
        searchList.push(newItem);
      } else if (((keywordparts.length > 1 && isNaN(Number(keywordparts[1]))) || (keywordparts.length === 1 && isNaN(Number(keywordparts[0]))))
        && !this.autocompleteSearchList[i].isNumber) {
        const newItem = {
          id: this.autocompleteSearchList[i].id,
          name: this.autocompleteSearchList[i].name,
          searchText: this.autocompleteSearchList[i].name + keywordparts[keywordparts.length - 1],
          searchVal: keywordparts[keywordparts.length - 1]
        }
        lastCharacter = newItem.searchText[newItem.searchText.length - 1];
        searchList.push(newItem);
      }
      this.items = searchList;
      if (lastCharacter === ':') {
         this.showAutoComplete = false;
        this.value = ''
      }
    }

    if (this.items) {
      // this.candidates = this.items.filter(
      //   function (item) {
      //     return filterItem(item, field, search);
      //   });
      this.candidates = this.items;
      this.buildLabels();
    }
  }

  private getFieldValue(object: any, path: any) {
    if (typeof object === 'string') {
      return object;
    }
    if (path instanceof Array) {
      let result: any = object;
      path.forEach((element: any) => {
        if (result !== null && result !== undefined
          && result[element] !== null && result[element] !== undefined) {
          result = result[element];
        } else {
          result = '';
        }
      });
      return result;
    } else {
      return object[path] || '';
    }
  }

  private isNull(object: any) {
    return object === null || object === undefined;
  }

  private buildLabels() {
    const field = this.sourceField;
    const getFieldValue = this.getFieldValue;
    this.candiatesLabels = this.candidates.map((e: any) => getFieldValue(e, field));
  }

  private filterItem(item: any, path: any, search: string) {
    if (search === null || search === undefined || search.length === 0) {
      return true;
    }
    let result: any;
    if (typeof item === 'string') {
      result = item;
    } else if (path instanceof Array) {
      result = item;
      path.forEach((element: any) => {
        if (result !== null && result !== undefined
          && result[element] !== null && result[element] !== undefined) {
          result = result[element];
        } else {
          result = '';
        }
      });
    } else {
      result = item[path] || '';
    }
    return result.toLowerCase().indexOf(search.toLowerCase()) >= 0;
  }

}

// @Directive({
//   selector: '[usAutocomplete]',
//   exportAs: 'autocompletedirective'
// })
// export class AutocompleteDirective implements OnInit, OnDestroy, OnChanges {
//   @Input() config: any;
//   @Input() items: any;
//   @Input() ngModel: String;
//   @Output() ngModelChange = new EventEmitter();
//   @Output() inputChangedEvent = new EventEmitter();
//   @Output() selectEvent = new EventEmitter();
//   @Output() selectDefaultEvent = new EventEmitter();
//   searchList: any;


//   private componentRef: ComponentRef<UsAutocompleteComponent>;
//   private thisElement: HTMLElement;
//   private autocompleteElement: HTMLElement;
//   private inputElement: HTMLInputElement;
//   private tabIndex: number;

//   constructor(private resolver: ComponentFactoryResolver,
//     public viewContainerRef: ViewContainerRef) {
//     this.thisElement = this.viewContainerRef.element.nativeElement;
//   }

//   ngOnInit() {
//     this.createDiv();

//   }

//   ngOnDestroy() {
//     if (this.componentRef) {
//       this.componentRef.instance.selectEvent.unsubscribe();
//       this.componentRef.instance.inputChangedEvent.unsubscribe();
//     }
//     document.removeEventListener('click', this.hideAutocomplete);
//   }

//   ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
//     if (changes['items'] && this.componentRef) {
//       const component = this.componentRef.instance;
//       component.items = changes['items'].currentValue;
//       component.autocompleteSearchList = this.items;
//       component.filterItems(component.value);
//     }
//   }

//   @HostListener('click')
//   @HostListener('focus')
//   showAutocomplete() {

//     this.hideAutocomplete();
//     this.createAutocomplete();

//     const newArray = this.items.map(o => {
//       return { name: o.name, id: o.id, isNumber: o.isNumber };
//     });

//     this.componentRef.instance.autocompleteSearchList = newArray;
//   }

//   hideAutocomplete = (event?: any): void => {

//     if (!this.componentRef) {
//       return;
//     }
//     if (!event || (event.target !== this.thisElement && event.type === 'click')) {
//       this.componentRef.instance.value = '';
//       this.componentRef.destroy();

//       this.componentRef = undefined;
//     }
//     if (this.inputElement['tabIndex'] < 0) {
//       this.inputElement['tabIndex'] = this.tabIndex;
//     }
//   }

//   onInputChanged = (val: string) => {
//     const component = this.componentRef.instance;
//     const lastCharacter = val[val.length - 1];

//     if (lastCharacter === ':') {
//       this.inputElement.value = '';
//       component.value = '';
//     } else {
//       this.inputElement.value = val;
//       if (val !== this.ngModel) {
//         this.ngModelChange.emit(val);
//       }
//     }
//     const newArray = this.items.map(o => {
//       return { name: o.name, id: o.id, isNumber: o.isNumber };
//     });

//     this.componentRef.instance.autocompleteSearchList = newArray;
//     component.filterItems(val);
//     this.inputChangedEvent.emit(val);

//   }

//   onSelect = (item: any) => {
//     const component = this.componentRef.instance;
//     const val = component.value;

//     if (item === '-1') {
//       const returnItem = [
//         { 'Id': '', 'Name': '', 'SearchText': val }
//       ]
//       // this.selectDefaultEvent.emit(val);
//       this.selectEvent.emit(returnItem[0]);
//       this.hideAutocomplete();
//       return;
//     } else {
//       const returnItem = [
//         { 'Id': item.id, 'Name': item.name, 'SearchText': item.id + ' :' + item.searchVal }
//       ]
//       this.selectEvent.emit(returnItem[0]);
//       if (this.inputElement) {
//         this.inputElement.value = '' + val;
//       }
//       this.hideAutocomplete();
//     }
//     if (this.inputElement) {
//       this.inputElement.value = '' + val;
//     }
//     this.hideAutocomplete();
//   }

//   private createDiv() {
//     const element = document.createElement('span');
//     element.style.display = 'inline-block';
//     element.style.position = 'relative';
//     element.style.width = '100%';
//     element.style.display = 'flex';
//     this.thisElement.parentElement.insertBefore(element, this.thisElement.nextSibling);
//     element.appendChild(this.thisElement);
//     document.addEventListener('click', this.hideAutocomplete);
//   }

//   private createAutocomplete() {
//     const factory = this.resolver.resolveComponentFactory(UsAutocompleteComponent);
//     this.componentRef = this.viewContainerRef.createComponent(factory);
//     const component = this.componentRef.instance;
//     component.config = this.config;
//     component.items = this.items;
//     component.selectEvent.subscribe(this.onSelect);
//     component.inputChangedEvent.subscribe(this.onInputChanged);
//     this.autocompleteElement = this.componentRef.location.nativeElement;
//     this.autocompleteElement.style.display = 'none';
//     this.inputElement = <HTMLInputElement>this.thisElement;
//     if (this.thisElement.tagName !== 'INPUT' && this.autocompleteElement) {
//       this.inputElement = <HTMLInputElement>this.thisElement.querySelector('input');
//       this.inputElement.parentElement.insertBefore(this.autocompleteElement, this.inputElement.nextSibling);
//     }
//     component.value = this.inputElement.value;
//     this.tabIndex = this.inputElement['tabIndex'];
//     this.inputElement['tabIndex'] = -100;
//     if (this.componentRef) {
//       const rect = this.thisElement.getBoundingClientRect();
//       const style = this.autocompleteElement.style;
//       style.width = rect.width + 'px';
//       style.position = 'absolute';
//       style.zIndex = '1';
//       style.top = '0';
//       style.left = '0';
//       style.display = 'inline-block';
//     }
//   }
// }


export class AutocompleteModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AutocompleteModule
    };
  }
}
