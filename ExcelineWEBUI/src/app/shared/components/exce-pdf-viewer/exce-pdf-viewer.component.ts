
import { Component, ElementRef, ViewChild } from '@angular/core';
import { ExcePdfViewerService } from './exce-pdf-viewer.service';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-exce-pdf-viewer',
  templateUrl: './exce-pdf-viewer.component.html',
  styleUrls: ['./exce-pdf-viewer.component.scss']
})

export class ExcePdfViewerComponent {
  private Title: string;
  private filePath: string;
  private pdfModalRef: any;


  @ViewChild('pdfViewer', { static: true }) pdfViewer: ElementRef;

  constructor(
    private messageService: ExcePdfViewerService,
    private usbModel: UsbModal,
    public sanitizer: DomSanitizer
  ) {
    this.messageService.pdfViewerSourceCalled$.subscribe((data) => {
      this.Title = data.title
      this.filePath = data.filePath
      this.pdfModalRef = usbModel.open(this.pdfViewer, { width: '1000' });

    })
    // this.messageService.componentMethodCalled$.subscribe((data) => {
    //   this.msgTitle = data.msgTitle;
    //   this.msgBody = data.msgBody;
    //   this.msgType = data.msgType;
    //   this.msgBoxId = data.msgBoxId;

    //   if (this.msgTitle && this.msgTitle.trim().length > 0) {
    //     this.translateGerSubscription = this.translate.get(this.msgTitle.trim()).subscribe((val: string) => {
    //       this.msgTitle = val;
    //     });
    //   }

    //   if (this.msgBody && this.msgBody.trim().length > 0) {
    //     this.translateGerSubscription = this.translate.get(this.msgBody.trim()).subscribe((val: string) => {
    //       this.msgBody = val;
    //     });
    //   }

    //   if (this.msgType === 'CONFIRM') {
    //     this.IsVisibleYes = false;
    //     this.IsVisibleNo = false;
    //     this.IsVisibleOk = true;
    //     this.IsVisibleCancel = true;

    //     if (this.usMessage) {
    //       this.usMessage.close();
    //       this.usMessage = usbModel.open(this.messageBox, { width: '500' });
    //     } else {
    //       this.usMessage = usbModel.open(this.messageBox, { width: '500' });
    //     }
    //   } else if (this.msgType === 'WARNING' || 'ERROR' || 'SUCCESS') {
    //     this.IsVisibleYes = true;
    //     this.IsVisibleNo = true;
    //     this.IsVisibleOk = false;
    //     this.IsVisibleCancel = true;

    //     if (this.usMessage) {
    //       this.usMessage.close();
    //       this.usMessage = usbModel.open(this.messageBox, { width: '500' });
    //     } else {
    //       this.usMessage = usbModel.open(this.messageBox, { width: '500' });
    //     }
    //   } else {
    //   }
    // });
  }

  // btnYesHandler() {
  //   this.usMessage.close();
  //   this.messageService.yesClickHandler(this.msgBoxId);
  // }

  // btnNoHandler() {
  //   this.usMessage.close();
  //   this.messageService.noClickHandler(this.msgBoxId);
  // }

  // btnOkHandler() {
  //   this.usMessage.close();
  //   this.messageService.okClickHandler(this.msgBoxId);
  // }

}
