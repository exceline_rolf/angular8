import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable()
export class ExcePdfViewerService {
    private pdfViewerSource = new Subject<any>();

    pdfViewerSourceCalled$ = this.pdfViewerSource.asObservable();

    openPdfViewer(title: string, filePath: string) {
        this.pdfViewerSource.next({ title: title, filePath: filePath });
    }
}
