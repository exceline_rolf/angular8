import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceMessageComponent } from './exce-message.component';

describe('ExceMessageComponent', () => {
  let component: ExceMessageComponent;
  let fixture: ComponentFixture<ExceMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
