
import {
  AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild, forwardRef, Renderer2
} from '@angular/core';
import 'intl-tel-input';
import * as jQuery from 'jquery';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, } from '@angular/forms';

export interface ITimeInputFieldChanged {
  value: string;
  extension: string;
  numberType: string,
  valid: boolean;
  validationError: any
}

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => IntlTelInputComponent),
  multi: true
};

const noop = () => {
};

@Component({
  selector: 'intl-tel-input',
  templateUrl: './intl-tel-input.component.html',
  styleUrls: ['./intl-tel-input.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})

export class IntlTelInputComponent implements ControlValueAccessor {

  @Input() fullValue: string;
  @Output() fullValueChange = new EventEmitter<ITimeInputFieldChanged>();
  @Input() id: string;
  @Input() intlOptions: any;
  @Input() value: string;
  @Output() valueChange = new EventEmitter<string>();
  @Output() valueBlur = new EventEmitter<string>();
  @Output() valueFocus = new EventEmitter();
  @ViewChild('intlInput', { static: true }) intlInput: ElementRef;

  private extension: string;

  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  constructor(
    private _renderer: Renderer2) { }

  onInputChange(value: string) {
    const intlInput = jQuery(this.intlInput.nativeElement)
    this.value = value;
    this.valueChange.emit(value);
    this.fullValue = intlInput.intlTelInput('getNumber');
    this.extension = intlInput.intlTelInput('getSelectedCountryData').dialCode;
    const validationErrorCode = intlInput.intlTelInput('getValidationError');
    let validationMessage = 'VALID';
    switch (validationErrorCode) {
      case 1:
        validationMessage = 'INVALID_COUNTRY_CODE'
        break;
      case 2:
        validationMessage = 'TOO_SHORT'
        break;
      case 3:
        validationMessage = 'TOO_LONG'
        break;
      case 4:
        validationMessage = 'NOT_A_NUMBER'
        break;
      case 5:
        break;
      default:
        validationMessage = 'VALID'
        break;
    }
    this.fullValueChange.emit({
      value: value,
      extension: this.extension,
      numberType: intlInput.intlTelInput('getNumberType'),
      valid: intlInput.intlTelInput('isValidNumber'),
      validationError: validationMessage
    });
    // this.writeValue(this.fullValue);
  }

  onBlur(value: string) {
    this.valueBlur.emit(value);
  }

  onFocus() {
    this.valueFocus.emit();
  }

  writeValue(value: any): void {

    if (value && (value !== this.value)) {
      this.fullValue = value;
      setTimeout(() => {
        const phoneInput = jQuery(`input#${this.id}`);
        phoneInput.intlTelInput('setNumber', this.fullValue);
      }, 100);
    }
  }
  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: () => any): void { this.onTouchedCallback = fn; }

  setDisabledState?(isDisabled: boolean): void {
    throw new Error('Method not implemented.');
  }

}
