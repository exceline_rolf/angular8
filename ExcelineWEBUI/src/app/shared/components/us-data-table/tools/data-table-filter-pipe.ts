// import {Pipe} from '@angular/core';
import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { DatePipe } from '@angular/common';

// Tell Angular2 we're creating a Pipe with TypeScript decorators
// tslint:disable-next-line:use-pipe-transform-interface
@Pipe({
  name: 'DataTableFilterPipe'
})
export class DataTableFilterPipe {

  /**
 * Make http  call with logging
 * @param param url of the service call
 * @param SearchType the http options
 * @param dataList the http options
 * @param args the http options
 * @returns {Observable<any>}
 */
  transform(param: any, SearchType: any, dataList: any, args?: any, lang?: any, dateFormat?: any): any {
    // param is the filtering object name
    // Searchtype is the Search Method for switch-Case
    // dataList is the JSON array of objects
    // args are the characters for filtering
    if (args.length === 0) {
      return dataList;
    }
    // tslint:disable-next-line:prefer-const
    let resultArray;
    const arrayToReturn = new Array<any>();

    for (const item of dataList) {
      switch (SearchType) {
        // for text search
        case 'TEXT':
          if (item[param].toString().toLowerCase().match('^.*' + args.toString().toLowerCase() + '.*$')) {
            arrayToReturn.push(item);
          }
          break;

        // filter the Name in manage-activities (Filter By Names)
        case 'NAMEFILTER':
          if (item[param].toString().toLowerCase().indexOf(args.toLowerCase()) !== -1) {
            arrayToReturn.push(item);
          }
          break;

        // to filter using dropdown lists
        case 'SELECT':
          // tslint:disable-next-line:triple-equals // To work with different data types

          if (item[param].toString().trim() === args.toString()) {
            arrayToReturn.push(item);
          }
          break;
        case 'NULL_SELECT':
          if (args === 'NULL' && item[param] == null) {
            arrayToReturn.push(item);
          } else if (args === 'NOT_NULL' && item[param] != null) {
            arrayToReturn.push(item);
          }
          break;
        case 'DATE_FILTER':
          // To filter date objects of any date format
          const argDate = new Date(args)
          const itemDate = new Date(item[param])
          if ((argDate.getDate() === itemDate.getDate()) && (argDate.getMonth() === itemDate.getMonth()) &&
            (argDate.getFullYear() === itemDate.getFullYear())) {
            arrayToReturn.push(item);
          }
          break;

        case 'DATE':
          const datePipe = new DatePipe(lang);
          const date = item[param];

          const formateDate = datePipe.transform(date, dateFormat);
          if (formateDate.toLowerCase().indexOf(args.toLowerCase()) !== -1) {
            arrayToReturn.push(item);
          }
          break;

        case 'FROM_DATE':
          //To filter Javascript From dates

          if (Date.parse(item[param]) > Date.parse(args)) {
            arrayToReturn.push(item);
          }
          break;

        case 'TO_DATE':
          //To filter Javascript TO dates
          if (Date.parse(item[param]) < Date.parse(args)) {
            arrayToReturn.push(item);
          }
          break;

        case 'DATE_MATCH':
          //To filter Javascript TO dates
          if (Date.parse(item[param]) == Date.parse(args)) {
            arrayToReturn.push(item);
          }
          break;


        default:
          if (item[param].match('^.*' + args + '.*$')) {
            arrayToReturn.push(item);
          }
      }
    }



    return arrayToReturn;
  }
}
