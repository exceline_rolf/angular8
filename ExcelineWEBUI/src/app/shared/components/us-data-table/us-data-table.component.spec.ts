import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsDataTableComponent } from './us-data-table.component';

describe('UsDataTableComponent', () => {
  let component: UsDataTableComponent;
  let fixture: ComponentFixture<UsDataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsDataTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
