import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsDataGridComponent } from './us-data-grid.component';

describe('UsDataGridComponent', () => {
  let component: UsDataGridComponent;
  let fixture: ComponentFixture<UsDataGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsDataGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsDataGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
