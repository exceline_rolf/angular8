import { Injectable } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { MetaDataService } from './meta-data.service';

function padNumber(value: number) {
  if (isNumber(value)) {
    var returnValue = `0${value}`.slice(-2); // return `0${value}`.slice(-2); // itinial value
    return returnValue;
  } else {
    return '';
  }
}

function isNumber(value: any): boolean {
  var returnVal = !isNaN(toInteger(value));
  return returnVal;
}

function toInteger(value: any): number {
  return parseInt(`${value}`, 10);
}

@Injectable()
export class NgbDateFrParserFormatterService {

  constructor(private _metaData: MetaDataService) { }

  parse(value: string): NgbDateStruct { // called only when the input is from the keyboard
    // debugger;
    if (value) {
      var dateParts: Array<any>;
      if (this._metaData.getCurrentLanguage() === 'en' || this._metaData.getCurrentLanguage() === 'en-US') {
        dateParts = value.trim().split('/');
        return this.arrangeReturnObject(this._metaData.getCurrentLanguage(), dateParts);
      } else if (this._metaData.getCurrentLanguage() === 'nb') {
        dateParts = value.trim().split('.');
        return  this.arrangeReturnObject(this._metaData.getCurrentLanguage(), dateParts);
      } else if (this._metaData.getCurrentLanguage() === 'es') {
        dateParts = value.trim().split('.');
        return  this.arrangeReturnObject(this._metaData.getCurrentLanguage(), dateParts);
      } else {
        dateParts = value.trim().split('/');
        return  this.arrangeReturnObject(this._metaData.getCurrentLanguage(), dateParts);
      }


    }
    return null;
  }


  format(date: NgbDateStruct): string {
    let stringDate: string = '';
    if (date) {
      if (this._metaData.getCurrentLanguage() === 'en' || this._metaData.getCurrentLanguage() === 'en-US') {
        stringDate += isNumber(date.month) ? padNumber(date.month) + '/' : '';
        stringDate += isNumber(date.day) ? padNumber(date.day) + '/' : '';
        stringDate += date.year;
      } else if (this._metaData.getCurrentLanguage() === 'nb') {
        stringDate += isNumber(date.day) ? padNumber(date.day) + '.' : '';
        stringDate += isNumber(date.month) ? padNumber(date.month) + '.' : '';
        stringDate += date.year;
      } else if (this._metaData.getCurrentLanguage() === 'es') {
        stringDate += isNumber(date.day) ? padNumber(date.day) + '/' : '';
        stringDate += isNumber(date.month) ? padNumber(date.month) + '/' : '';
        stringDate += date.year;
      }
    }
    return stringDate;
  }

  arrangeReturnObject(language: string, dateParts: Array<any>): NgbDateStruct {
    if (language === 'en' || language === 'en-US') {
      // arranging the date object
      if (dateParts.length === 1 && isNumber(dateParts[0])) {
        return { year: toInteger(dateParts[0]), month: null, day: null };
      } else if (dateParts.length === 2 && isNumber(dateParts[0]) && isNumber(dateParts[1])) {
        return { year: toInteger(dateParts[1]), day: toInteger(dateParts[0]), month: null };
      } else if (dateParts.length === 3 && isNumber(dateParts[0]) && isNumber(dateParts[1]) && isNumber(dateParts[2])) {
        var returnValue = { year: toInteger(dateParts[2]), month: toInteger(dateParts[0]), day: toInteger(dateParts[1]) };
        return returnValue;
      }
    } else if (language === 'es' || language === 'nb') {
      if (dateParts.length === 1 && isNumber(dateParts[0])) {
        return { year: toInteger(dateParts[0]), month: null, day: null };
      } else if (dateParts.length === 2 && isNumber(dateParts[0]) && isNumber(dateParts[1])) {
        return { year: toInteger(dateParts[1]), month: toInteger(dateParts[0]), day: null };
      } else if (dateParts.length === 3 && isNumber(dateParts[0]) && isNumber(dateParts[1]) && isNumber(dateParts[2])) {
        return { year: toInteger(dateParts[2]), month: toInteger(dateParts[1]), day: toInteger(dateParts[0]) };
      }
    } else {
      return null;
    }
  }

}
