import { Injectable } from '@angular/core';
import { MetaDataService } from './meta-data.service';
import { TranslateDataService } from './translate-data.service';
import { Headers, DatePickerObject } from './date-picker-data';

@Injectable()
export class ValidatorService {

  constructor(
    private _metaDataService: MetaDataService,
    private _translator:TranslateDataService
  ) { }

// will validate the currency and update the dataObject required to save in the _metadataService
  /******************************************************************************************************************************
  * the main function of this method is to check if the currnency is inputed according to the local language format
  * if the currency is according to the inputed currency format then update the object
  * if the input value is not according to the language native currency format then give an error message
  * if the value is only integers then updat the value
  * if the value is containing decimal places with the correct language format decimal then update the data object
  ******************************************************************************************************************************/
  public Validator(headerMetaData: Headers, value: any, dataObject: any): ValidatorReturnObj {
    const currentLanguage = this._metaDataService.getCurrentLanguage();
    let validatorReturn = new ValidatorReturnObj();
      if (headerMetaData.dataType === 'date' ) {  // the date will be validated from the plugin
        // debugger;
        if ((value !== null || value !== undefined) && !(value instanceof Object)) {
          var originalDate = this._metaDataService.getSingleObjFromID(dataObject[this._metaDataService.getIdHeader()])[headerMetaData.HeaderName];
          validatorReturn.dataInOriginalFormat = originalDate;
          validatorReturn.dataToDisplay = this._translator.translateDate(originalDate);
        }else if(value instanceof Object){
          validatorReturn.dataToDisplay = value;
          validatorReturn.dataInOriginalFormat = this.ArrahgeDateToDisplay(value);
        }

      } else if (headerMetaData.dataType === 'currency' && value !== null && value !== undefined) {
       validatorReturn = this.CheckCurrencyInput(currentLanguage,value.trim());
      }else {
        validatorReturn.dataInOriginalFormat = value;
        validatorReturn.dataToDisplay = value;
      }

    return validatorReturn;
  }

// arrange the currency input without the separators
  private CheckCurrencyInput(language: string, value: string): ValidatorReturnObj {
    let inputValue = value.trim();
    const checkCurrencyReturn = new ValidatorReturnObj();
    if (language === 'en' || language === 'en-US') { // get the language first
      // define the regular expression to suit the language format
      const regExp = /^(0|[1-9][0-9]{0,2}(?:(,[0-9]{3})*|[0-9]*))(\.[0-9][0-9]){0,1}$/;
      if (regExp.test(inputValue)) { // check if the value to be of proper format

        checkCurrencyReturn.dataToDisplay = this.ArrangeCurrencyToDisplay(value, {thousandSeparator: ',', decimalPlace: '.'} );
        checkCurrencyReturn.dataInOriginalFormat = this.ArrangeCurrencyToStore(value,{thousandSeparator: ',', decimalPlace: '.'} );

          return checkCurrencyReturn;
      }else{
        return null;
      }
    }else if (language === 'es') {
      const regExp = /^(0|[1-9][0-9]{0,2}(?:(.[0-9]{3})*|[0-9]*))(\,[0-9][0-9]){0,1}$/;
      if (regExp.test(inputValue)) { // check if the value to be of proper format
        checkCurrencyReturn.dataToDisplay = this.ArrangeCurrencyToDisplay(value, {thousandSeparator: '.', decimalPlace: ','} );
        checkCurrencyReturn.dataInOriginalFormat = this.ArrangeCurrencyToStore(value,{thousandSeparator: '.', decimalPlace: ','} );

          return checkCurrencyReturn;
      }else{
        return null;
      }
    }else if (language === 'nb') {
      const regExp = /^(0|[1-9][0-9]{0,2}(?:(\s[0-9]{3})*|[0-9]*))(\,[0-9][0-9]){0,1}$/;
      if (regExp.test(inputValue)) { // check if the value to be of proper format
        checkCurrencyReturn.dataToDisplay = this.ArrangeCurrencyToDisplay(value, {thousandSeparator: ' ', decimalPlace: ','} );
        checkCurrencyReturn.dataInOriginalFormat = this.ArrangeCurrencyToStore(value,{thousandSeparator: ' ', decimalPlace: ','} );

          return checkCurrencyReturn;
      }else {
        return null;
      }
    }


  }

  // arrenge the currency to be displayed in the text box
  // this method will get either with decimal place placed propertly, thousand separators placed -
  // -properly or non of them or both of them in correct possition
  private ArrangeCurrencyToDisplay(amount: string, stringsToAdd: numberFormattingChars): string {
    let numberStringToReturn: string = amount;
    if (!numberStringToReturn.includes(stringsToAdd.decimalPlace) && !numberStringToReturn.includes(stringsToAdd.thousandSeparator)) {
      // if the number is without any separation charactors add the thousand separator first
      if (numberStringToReturn.length >= 4) {
        for (let index = numberStringToReturn.length; index > 0; index -= 3) { // iterate to add the thousand separator
          if (index !== numberStringToReturn.length) {
            numberStringToReturn = [numberStringToReturn.slice(0,index),stringsToAdd.thousandSeparator,numberStringToReturn.slice(index)].join('');
          }
        }
      }
      numberStringToReturn += stringsToAdd.decimalPlace + '00';
    }else if (!numberStringToReturn.includes(stringsToAdd.thousandSeparator) && numberStringToReturn.includes(stringsToAdd.decimalPlace)) {
      if (numberStringToReturn.length >= 7) {
        for (var index2 = numberStringToReturn.length - 6; index2 >= 0; index2 -= 3) { // iterate to add the thousand separator
          numberStringToReturn = [numberStringToReturn.slice(0,index2),stringsToAdd.thousandSeparator,numberStringToReturn.slice(index2)].join('');
        }
      }

    } else if (!numberStringToReturn.includes(stringsToAdd.decimalPlace) && numberStringToReturn.includes(stringsToAdd.thousandSeparator)){
      numberStringToReturn += stringsToAdd.decimalPlace + '00';
    } else if(numberStringToReturn.includes(stringsToAdd.decimalPlace) && numberStringToReturn.includes(stringsToAdd.thousandSeparator)){
      numberStringToReturn = numberStringToReturn;
    }else {
      numberStringToReturn = null;
    }
    return numberStringToReturn;
  }

  // arrange the currency to be stored in the _metadata service
  private ArrangeCurrencyToStore(amount: string, stringsToRemove: numberFormattingChars): number {
    let numberToReturn:number;
    let numberString = amount;
    if(numberString.includes(stringsToRemove.thousandSeparator)){
      const regExpp = new RegExp(stringsToRemove.thousandSeparator, 'g');
      numberString = numberString.replace(regExpp, '');
    } if (numberString.includes(stringsToRemove.decimalPlace)) {
      const regExpp2 = new RegExp(stringsToRemove.decimalPlace, 'g');
      numberString = numberString.replace(regExpp2, '');
      numberString = [numberString.slice(0,(numberString.length - 2)), '.', numberString.slice(numberString.length - 2)].join('');
    }
    numberToReturn = parseFloat(numberString);
    return numberToReturn;
  }

  private ArrahgeDateToDisplay(value:DatePickerObject):Date{
    return new Date(value.year,value.month,value.day);
  }

  
}

export class ValidatorReturnObj{
  dataToDisplay: any;
  dataInOriginalFormat: any;
}

class numberFormattingChars{
  thousandSeparator:string;
  decimalPlace:string;
}
