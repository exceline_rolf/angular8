import { Injectable } from '@angular/core';

@Injectable()
export class MetaDataService {

  private dataObjOriginal: any[];
  private headersData: string[];
  private dateHeaders: string[];
  private curencyHeaders: string[];
  private selectedLanguage: string;
  private idHeader: string;

  constructor() { }

  public setDataObj(dataObj: any[]): void {
    let tempHeaders: Array<string>; // to collect all the headers
    let indexNumber: number; // to collect the index number of the id

    // a new instance of original data object is set here
    this.dataObjOriginal = Object.assign({}, dataObj);
    this.dataObjOriginal = Object.keys(this.dataObjOriginal).map(value => this.dataObjOriginal[value]);
    for (var index = 0; index < dataObj.length; index++) {
      this.dataObjOriginal[index] = Object.assign({}, dataObj[index])
    }

    // the following loop is to identify what would be the header with the id property
    tempHeaders = Object.getOwnPropertyNames(this.dataObjOriginal[0]);
    for (var index2 = 0; index2 < tempHeaders.length; index2++) { // iterating through the temp headers
      if (tempHeaders[index2].toUpperCase().includes('ID')) { // checking whether the headers include charactors 'ID'
        indexNumber = tempHeaders[index2].toUpperCase().lastIndexOf('ID');  // get the last occurence of 'ID'
        if (indexNumber !== undefined) {
          if ((indexNumber + 2) === tempHeaders[index2].length) { // check whether the id is the last 2 charactors
            this.idHeader = tempHeaders[index2];
            this.setIdHeader(tempHeaders[index2]);
            break;
          }
        }
      }
    }
  }

  public getDataObj(): any[] {
    return this.dataObjOriginal;
  }

  public updateDataObject(dataObj: any, header: any, value: any): Object {
    if (this.dataObjOriginal !== undefined) {
      for (var index = 0; index < this.dataObjOriginal.length; index++) {
        if (this.dataObjOriginal[index][this.idHeader] === dataObj[this.idHeader]) {
          this.dataObjOriginal[index][header.HeaderName] = value;
          return this.dataObjOriginal[index]; // returns the updated data object
        }
      }
    }
  }

  public getSingleObjFromID(id: number): any {  // we take the id header from this service to the view and is send back here
    for (var index = 0; index < this.dataObjOriginal.length; index++) {
      if (this.dataObjOriginal[index][this.getIdHeader()] === id) {
        return this.dataObjOriginal[index];
      }
    }
  }

  public setHeaders(headers: string[]): void {
    // set a new instance of the headers data
    this.headersData = Object.assign({}, headers);
    this.headersData = Object.keys(this.headersData).map(value => this.headersData[value]);
  }

  public getHeaders(): string[] {
    return this.headersData;
  }

  public setDateHeader(dateHeaders: string[]): void {
    this.dateHeaders = dateHeaders;
  }

  public getDateHeader(): string[] {
    return this.dateHeaders;
  }

  public setCurrencyHeader(currencyHeaders: string[]) {
    this.curencyHeaders = currencyHeaders;
  }

  public getCurrencyHeaders(): string[] {
    return this.curencyHeaders;
  }

  public setCurrentLanguage(currentLanguage: string): void {
    this.selectedLanguage = currentLanguage;
  }

  public getCurrentLanguage(): string {
    return this.selectedLanguage;
  }

  public setIdHeader(header: string): void {
    this.idHeader = header;
  }

  public getIdHeader(): string {
    return this.idHeader;
  }

}
