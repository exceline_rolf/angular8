import { Injectable } from '@angular/core';
import { PostalArea } from 'app/shared/SystemObjects/Common/PostalArea';
import { CommonHttpService } from 'app/shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Subject } from 'rxjs';

@Injectable()
export class ExcePostalcodeService {
  private userApiURL: string;
  private memberApiURL: string;
  // PostalDataObj = new Subject<any>();

  constructor(private commonHttpService: CommonHttpService,
    private config: ConfigService) {
    this.userApiURL = this.config.getSettings('EXCE_API.COMMON');
    this.memberApiURL = this.config.getSettings('EXCE_API.MEMBER');
  }

  savePostalArea(postalArea: PostalArea) {
    console.log(postalArea);
    const url = this.memberApiURL + '/SavePostalArea';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: postalArea
    })
  }

  checkPostalCodeExits(postalCode: string) {
    console.log(postalCode);
    if (postalCode === '12345') {
      return true;
    } else {
      return false;
    }
  }

  // sentPostalData(dataObj) {
  //   this.PostalDataObj.next(dataObj);
  // }


}
