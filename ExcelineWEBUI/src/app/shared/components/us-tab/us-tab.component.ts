import { Component, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { TabContentComponent } from './tab-content/tab-content.component';
import { UsTabService } from './us-tab.service';

@Component({
  selector: 'us-tab',
  templateUrl: './us-tab.component.html',
  styleUrls: ['./us-tab.component.scss']
})
export class UsTabComponent implements AfterContentInit {

  @ContentChildren(TabContentComponent) tabs: QueryList<TabContentComponent>;

  constructor(private usTabService: UsTabService) { }
  // contentChildren are set
  ngAfterContentInit() {
    // get all active tabs
    const activeTabs = this.tabs.filter((tab) => tab.active);

    // if there is no active tab set, activate the first
    if (activeTabs.length === 0) {
      this.selectTab(this.tabs.first, 0);
    }
  }

  selectTab(tab: TabContentComponent, index) {
    if (tab) {
      // deactivate all tabs
      // tslint:disable-next-line:no-shadowed-variable
      this.tabs.toArray().forEach(tab => tab.active = false);

      // activate the tab the user has clicked on.
      tab.active = true;
      this.usTabService.tabSelectionChange(index);
    }
  }

}
