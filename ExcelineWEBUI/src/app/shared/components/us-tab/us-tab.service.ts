import { Injectable } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
@Injectable()
export class UsTabService {

   @Output() tabSelectionChanged: EventEmitter<any> = new EventEmitter();
    @Output() verticleTabSelectionChanged: EventEmitter<any> = new EventEmitter();

    constructor() { }

    tabSelectionChange(index) {
        this.tabSelectionChanged.emit(index);
    }

    vTabSelectionChange(index) {

        this.verticleTabSelectionChanged.emit(index);
    }


}
