import { TestBed, inject } from '@angular/core/testing';

import { ExceAddmemberService } from './exce-addmember.service';

describe('ExceAddmemberService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceAddmemberService]
    });
  });

  it('should be created', inject([ExceAddmemberService], (service: ExceAddmemberService) => {
    expect(service).toBeTruthy();
  }));
});
