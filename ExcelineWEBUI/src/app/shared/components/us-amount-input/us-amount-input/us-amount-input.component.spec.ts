import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsAmountInputComponent } from './us-amount-input.component';

describe('UsAmountInputComponent', () => {
  let component: UsAmountInputComponent;
  let fixture: ComponentFixture<UsAmountInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsAmountInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsAmountInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
