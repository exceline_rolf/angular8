import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsDatePickerComponent } from './us-date-picker.component';

describe('UsDatePickerComponent', () => {
  let component: UsDatePickerComponent;
  let fixture: ComponentFixture<UsDatePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsDatePickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsDatePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
