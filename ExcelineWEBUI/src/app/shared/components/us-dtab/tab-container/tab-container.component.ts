import { AfterContentInit, Component, ContentChildren, QueryList } from '@angular/core';
import { UsDtabComponent } from '../us-dtab.component';
import { BasicContent } from '../basic-content';
import { Observable, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'dtab-container',
  templateUrl: './tab-container.component.html',
  styleUrls: ['./tab-container.component.scss']
})
export class TabContainerComponent implements AfterContentInit {

  @ContentChildren(UsDtabComponent) tabs: QueryList<UsDtabComponent>;

  private contentSbj = new BehaviorSubject<BasicContent>(null);
  content = this.contentSbj.asObservable();

  ngAfterContentInit() {
    const activeTabs = this.tabs.filter((tab) => tab.active);
    if (activeTabs.length === 0) {
      this.selectTab(this.tabs.first);
    }
  }

  selectTab(tab: UsDtabComponent) {
    this.tabs.toArray().forEach(tab => tab.active = false);
    tab.active = true;
    this.contentSbj.next(tab.contentRef);
  }
}
