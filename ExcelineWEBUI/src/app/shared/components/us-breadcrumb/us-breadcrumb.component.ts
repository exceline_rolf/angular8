import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { UsBreadcrumbService } from './us-breadcrumb.service';
import { OnDestroy } from '@angular/core';


@Component({
    selector: 'us-breadcrumb',
    templateUrl: './us-breadcrumb.component.html',
    styleUrls: ['./us-breadcrumb.component.scss'],
})
export class UsBreadcrumbComponent implements OnInit, OnChanges, OnDestroy {
    @Input() useBootstrap = true;
    @Input() prefix = '';

    public _urls: string[];
    public _routerSubscription: any;

    constructor(
        private router: Router,
        private breadcrumbService: UsBreadcrumbService,
        private _location: Location
    ) { }

    ngOnInit(): void {
        this._urls = new Array();

        if (this.prefix.length > 0) {
            this._urls.unshift(this.prefix);
        }

        this._routerSubscription = this.router.events.subscribe((navigationEnd: NavigationEnd) => {

            if (navigationEnd instanceof NavigationEnd) {
                this._urls.length = 0; // Fastest way to clear out array
                this.generateBreadcrumbTrail(navigationEnd.urlAfterRedirects ? navigationEnd.urlAfterRedirects : navigationEnd.url);
            }
        });
    }

    closeClicked() {
        this._urls.splice(-1, 1);
        // this._location.back();
        if (this._urls.length > 0) {
            this.navigateTo(this._urls[this._urls.length - 1]);
        } else {
            this.navigateTo('/');
        }
    }

    ngOnChanges(changes: any): void {
        if (!this._urls) {
            return;
        }

        this._urls.length = 0;
        this.generateBreadcrumbTrail(this.router.url);
    }

    generateBreadcrumbTrail(url: string): void {
        if (!this.breadcrumbService.isRouteHidden(url)) {
            // Add url to beginning of array (since the url is being recursively broken down from full url to its parent)
            this._urls.unshift(url);
        }

        if (url.lastIndexOf('/') > 0) {
            this.generateBreadcrumbTrail(url.substr(0, url.lastIndexOf('/'))); // Find last '/' and add everything before it as a parent route
        } else if (this.prefix.length > 0) {
            this._urls.unshift(this.prefix);
        }
    }

    navigateTo(url: string): void {
        if (url === '/adminstrator') {
            url = '/adminstrator/home';
        } else if (url === '/uss-admin') {
            url = '/adminstrator/home';
        } else if (url === '/economy') {
            url = '/economy/home';
        } else if (url === '/reporting') {
            url = '/reporting/home';
        }
        // else {
        //     url = '/';
        // }
        this.router.navigateByUrl(url);
    }

    friendlyName(url: string): string {
        return !url ? '' : this.breadcrumbService.getFriendlyNameForRoute(url);
    }

    ngOnDestroy(): void {
        if (this._routerSubscription) {
            this._routerSubscription.unsubscribe();
        }
    }


}
