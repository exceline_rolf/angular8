import { Component, OnInit, EmbeddedViewRef, Input, ViewContainerRef, OnDestroy } from '@angular/core';

@Component({
  selector: 'us-tree-header-loader',
  template: ``
})

export class ColumnHeaderTemplateLoaderComponent implements OnInit, OnDestroy {

  @Input() column: any;

  view: EmbeddedViewRef<any>;

  constructor(public viewContainer: ViewContainerRef) { }

  ngOnInit() {
    this.view = this.viewContainer.createEmbeddedView(this.column.headerTemplate, {
      '\$implicit': this.column
    });
  }

  ngOnDestroy() {
    this.view.destroy();
  }
}
