import { Component, OnInit, AfterContentInit, Input, EventEmitter, ContentChildren, Output, QueryList, TemplateRef, ContentChild } from '@angular/core';

export class UsTemplate {

  @Input() type: string;

  @Input('usTemplate') name: string;

  constructor(public template: TemplateRef<any>) { }

  getType(): string {
    return this.name;
  }
}


@Component({
  selector: 'us-tree-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss']
})
export class ColumnComponent implements AfterContentInit {
  @Input() field: string;
  @Input() colId: string;
  @Input() sortField: string;
  @Input() filterField: string;
  @Input() header: string;
  @Input() footer: string;
  @Input() sortable: any;
  @Input() editable: boolean;
  @Input() filter: boolean;
  @Input() filterMatchMode: string;
  @Input() filterType: string;
  @Input() rowspan: number;
  @Input() colspan: number;
  @Input() style: any;
  @Input() styleClass: string;
  @Input() hidden: boolean;
  @Input() expander: boolean;
  @Input() selectionMode: string;
  @Input() filterPlaceholder: string;
  @Input() filterMaxlength: number;
  @Input() frozen: boolean;
  @Output() sortFunction: EventEmitter<any> = new EventEmitter();
  @ContentChildren(UsTemplate) templates: QueryList<any>;
  @ContentChild(TemplateRef, /* TODO: add static flag */ {static: true}) template: TemplateRef<any>;
  @ContentChild('dataTableCell', /* TODO: add static flag */ {static: true}) cellTemplate;


  public headerTemplate: TemplateRef<any>;
  public bodyTemplate: TemplateRef<any>;
  public footerTemplate: TemplateRef<any>;
  public filterTemplate: TemplateRef<any>;
  public editorTemplate: TemplateRef<any>;

  ngAfterContentInit(): void {
    this.templates.forEach((item) => {
      switch (item.getType()) {
        case 'header':
          this.headerTemplate = item.template;
          break;
        case 'body':
          this.bodyTemplate = item.template;
          break;

        case 'footer':
          this.footerTemplate = item.template;
          break;

        case 'filter':
          this.filterTemplate = item.template;
          break;

        case 'editor':
          this.editorTemplate = item.template;
          break;

        default:
          this.bodyTemplate = item.template;
          break;
      }
    });
  }
}
