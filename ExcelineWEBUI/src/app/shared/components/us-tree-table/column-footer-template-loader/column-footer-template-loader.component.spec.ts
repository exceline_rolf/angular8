import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnFooterTemplateLoaderComponent } from './column-footer-template-loader.component';

describe('ColumnFooterTemplateLoaderComponent', () => {
  let component: ColumnFooterTemplateLoaderComponent;
  let fixture: ComponentFixture<ColumnFooterTemplateLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnFooterTemplateLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnFooterTemplateLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
