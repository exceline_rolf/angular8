import { Component, OnInit, OnDestroy, Input, EmbeddedViewRef, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'us-tree-footer-loader',
  template: ``
})
export class ColumnFooterTemplateLoaderComponent implements OnInit, OnDestroy {

  @Input() column: any;

  view: EmbeddedViewRef<any>;

  constructor(public viewContainer: ViewContainerRef) { }

  ngOnInit() {
    this.view = this.viewContainer.createEmbeddedView(this.column.footerTemplate, {
      '\$implicit': this.column
    });
  }

  ngOnDestroy() {
    this.view.destroy();
  }
}
