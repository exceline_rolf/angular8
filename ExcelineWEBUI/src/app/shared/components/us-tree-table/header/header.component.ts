import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'us-tree-header',
  template: '<ng-content></ng-content>'
})

export class HeaderComponent {

}

