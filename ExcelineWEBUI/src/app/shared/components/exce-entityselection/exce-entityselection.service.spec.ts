import { TestBed, inject } from '@angular/core/testing';

import { ExceEntityselectionService } from './exce-entityselection.service';

describe('ExceEntityselectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceEntityselectionService]
    });
  });

  it('should be created', inject([ExceEntityselectionService], (service: ExceEntityselectionService) => {
    expect(service).toBeTruthy();
  }));
});
