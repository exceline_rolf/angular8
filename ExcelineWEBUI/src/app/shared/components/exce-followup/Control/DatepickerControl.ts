import { ControlBase } from 'app/shared/components/exce-followup/Control/ControlBase';

export class DatepickerControl extends ControlBase<string> {
  controlType = 'datepicker';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}
