import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceAddFollowUpComponent } from './exce-add-follow-up.component';

describe('ExceAddFollowUpComponent', () => {
  let component: ExceAddFollowUpComponent;
  let fixture: ComponentFixture<ExceAddFollowUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceAddFollowUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceAddFollowUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
