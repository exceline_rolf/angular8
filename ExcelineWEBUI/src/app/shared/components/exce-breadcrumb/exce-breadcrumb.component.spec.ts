import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceBreadcrumbComponent } from './exce-breadcrumb.component';

describe('ExceBreadcrumbComponent', () => {
  let component: ExceBreadcrumbComponent;
  let fixture: ComponentFixture<ExceBreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceBreadcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
