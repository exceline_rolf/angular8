
import { Component, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { VtabContentComponent } from './vtab-content/vtab-content.component';
import { UsTabService } from '../us-tab/us-tab.service';
import { Output, EventEmitter, Input } from '@angular/core';
@Component({
  selector: 'us-tab-verticle',
  templateUrl: './us-tab-verticle.component.html',
  styleUrls: ['./us-tab-verticle.component.scss']
})
export class UsTabVerticleComponent implements AfterContentInit {

  showBadge = false;
  badgeValue: string;
  @Output() tabChanged: EventEmitter<any> = new EventEmitter();


  @ContentChildren(VtabContentComponent) tabs: QueryList<VtabContentComponent>;
  constructor(private usTabService: UsTabService) {
  }
  // contentChildren are set
  ngAfterContentInit() {
    // get all active tabs
    const activeTabs = this.tabs.filter((tab) => tab.active);

    // if there is no active tab set, activate the first
    if (activeTabs.length === 0) {
      this.selectTab(this.tabs.first, 0);
    }
  }

  selectTab(tab: VtabContentComponent, index) {

    // deactivate all tabs
    this.tabs.toArray().forEach(tab => tab.active = false);
    this.tabChanged.emit()

    // activate the tab the user has clicked on.
    tab.active = true;
    this.usTabService.vTabSelectionChange(index);
  }

}
