import { Component, Input } from '@angular/core';
import { NgComponentOutlet } from '@angular/common';

@Component({
  selector: 'vtab-content',
  templateUrl: './vtab-content.component.html',
  styleUrls: ['./vtab-content.component.scss']
})

export class VtabContentComponent {
  @Input('tabTitle') title: string;
  @Input() active = false;
  @Input() badgeValue?: string;

}
