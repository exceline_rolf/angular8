import { Component, ElementRef, OnInit, ViewChild, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
// import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
// import { ExcelineMember } from 'app/modules/membership/models/ExcelineMember';
// import { LoDashStatic } from 'lodash';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ExceBreadcrumbService } from '../../components/exce-breadcrumb/exce-breadcrumb.service';
// import { DataTableResource } from '../../../shared/components/us-data-table/tools/data-table-resource';
import { UsbModal } from '../../components/us-modal/us-modal';
import { UsTabService } from '../../components/us-tab/us-tab.service';
// import { ISlimScrollOptions } from '../../../shared/directives/us-scroll/classes/slimscroll-options.class';
import { UsScrollService } from '../../../shared/directives/us-scroll/us-scroll.service';
import { MemberRole } from '../../enums/us-enum';
import { PreloaderService } from '../../services/preloader.service';
import { ExceMemberService } from '../../../modules/membership/services/exce-member.service';
// import { asTextData } from '@angular/core/src/view';
// import value from '*.json';
// import { filter } from 'rxjs/operator/filter';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { AdminService } from 'app/modules/admin/services/admin.service';
import {takeUntil} from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'exce-memberlist',
  templateUrl: './exce-memberlist.component.html',
  styleUrls: ['./exce-memberlist.component.scss']
})
export class ExceMemberlistComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchID: number = JSON.parse(Cookie.get('selectedBranch')).BranchId;
  public selectedBranch: any;

  private All: String;
  public userBranches: any[];
  public onlyonebranch = false;

  // user for hover over member
  // @ViewChild('memberInfo') memberInfo: ElementRef;

  viewHeight: any;
  columnDefs: any
  rowData: any;
  gridApi: any;
  gridColumnApi: any;
  searchHasMoreMembers = false;
  newMemberRegisterView: any;
  displayBranches: { BranchId: number; BranchName: String; }[];
  statuses: any;
  lang: any;
  filterBranchId = -1;
  filterStatusId = -1;
  filterRoleId = '';
  activeCount: any;
  inactiveCount: any;
  allCount: any;
  selectedMembers = [];
  memberColumns: any;

  roles = [{Name: 'Medlem', RoleId: 'MEM'}, {Name: 'Bedrift', RoleId: 'COM'}, {Name: 'Sponsor', RoleId: 'SPO'} ]

  // used for hover over member
  // isHidden = true;

// default norwegian
names = {
  LastName: 'Etternavn',
  FirstName: 'Fornavn',
  CustId: 'Kundenummer',
  Age: 'Alder',
  Mobile: 'Mobil',
  Email: 'Epost',
  MemberCardNo: 'Kortnummer',
  ContractNo: 'Kontraktsnummer',
  OrderAmount: 'Prisplan',
  Status: 'Status',
  BranchName: 'Avdeling',
  BranchId: 'BranchID',
  RoleId: 'Role',
  ID: 'MemberId',
  ContractId: 'ContractId'
}

colNamesNo = {
  LastName: 'Etternavn',
  FirstName: 'Fornavn',
  CustId: 'Kundenummer',
  Age: 'Alder',
  Mobile: 'Mobil',
  Email: 'Epost',
  MemberCardNo: 'Kortnummer',
  ContractNo: 'Kontraktsnummer',
  OrderAmount: 'Prisplan',
  Status: 'Status',
  BranchName: 'Avdeling',
  BranchId: 'BranchID',
  RoleId: 'Role',
  ID: 'MemberId',
  ContractId: 'ContractId'
}

colNamesEn = {
  LastName: 'LastName',
  FirstName: 'FirstName',
  CustId: 'CustomerNo',
  Age: 'Age',
  Mobile: 'Mobile',
  Email: 'Email',
  MemberCardNo: 'CardNo',
  ContractNo: 'ContractNo',
  OrderAmount: 'Priceplan',
  Status: 'Status',
  BranchName: 'BranchName',
  BranchId: 'BranchID',
  RoleId: 'Role',
  ID: 'MemberId',
  ContractId: 'ContractId'
}

  @Input() allowMultipleSelect = false; // default should be false
  @Input() newMemberVisibe = false;
  @Input() columnsToShow = [] // list of column to show in ag-grid format : ['CustId', 'LastName', 'FirstName', 'Age', 'Mobile'];
  @Input() hideRoleSelect = false;
  @Input() hideStatusSelect = false;
  @Input() hideBranchSelect = false;
  @Input() getType = 'MEMBER';

  @Output() memberSelectedDblClick = new EventEmitter();
  @Output() membersSelected = new EventEmitter();
  @Output() closeViewEvent = new EventEmitter();
  @Output() openModalEvent = new EventEmitter();

  memberApi: any;



  constructor(
    private router: Router,
    private memberService: ExceMemberService,
    private modalService: UsbModal,
    private usScrollService: UsScrollService,
    private usTabService: UsTabService,
    private toolbarService: ExceToolbarService,
    private adminService: AdminService,
    private translateService: TranslateService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
  }

  ngOnInit() {
    // setting header names for ag grid by language
    this.lang = this.toolbarService.getSelectedLanguage()
    switch (this.lang.id) {
      case 'no':
        this.names = this.colNamesNo;
        break;
      case 'en':
        this.names = this.colNamesEn;
        break;
    }
    // getting branches
    this.memberService.getBranches().pipe(
      takeUntil(this.destroy$)
      ).subscribe(branches => {
      if (branches) {
        this.userBranches = branches.Data;
        const branchList = [{ BranchId: -2, BranchName: this.All }];
        branches.Data.map(item => { return { BranchId: item.BranchId, BranchName: item.BranchName } })
          .forEach(item => branchList.push(item));
        this.displayBranches = branches.Data;
        this.onlyonebranch = this.userBranches.length === 2;
        this.selectedBranch = this.userBranches[0];
      }
    }, null, null);
    this.usTabService.tabSelectionChanged.pipe(
      takeUntil(this.destroy$)
      ).subscribe(event => {
      this.usScrollService.reloadScrollBar();
    });

    // getting memberCount for active, inactive and all members in db
    // this.memberService.getMemberCount(this.filterBranchId).subscribe(
    //   result => {
    //     this.activeCount = result.Data[0];
    //     this.inactiveCount = result.Data[1];
    //     this.allCount = result.Data[2];
    //   }
    // )

    // get status on member
    const getAllStatuses = false;
    // filter out those who are not in use and wont return any members
    this.memberService.getMemberStatus(getAllStatuses).pipe(
      takeUntil(this.destroy$)
      ).subscribe(status => {
        this.statuses = status.Data;
      }
    )

    this.memberColumns = [
      {headerName: this.names.FirstName, field: 'FirstName', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, // checkboxSelection: true
    },
      {headerName: this.names.LastName, field: 'LastName',  filter: 'agTextColumnFilter', suppressMenu: true,
        floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true
    },
      {headerName: 'Slett', field: '',  filter: 'agTextColumnFilter', width: 60, suppressMenu: true,
      floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressFilter: true, suppressMovable: true, autoHeight: true, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'},
     cellRenderer: function(params) {
      return '<button class="btn btn-danger btn-icon-min" title="Slett"><span class="icon-cancel"></span></button>';
      }}
    ]

    // defining column properties for ag grid
    this.columnDefs = [
      {headerName: this.names.CustId, field: 'CustId', width: 100, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, // checkboxSelection: true
    },
      {headerName: this.names.LastName, field: 'LastName',  filter: 'agTextColumnFilter', suppressMenu: true, cellClass: 'clickPointer', cellStyle: {'cursor': 'pointer'},
        floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.FirstName, field: 'FirstName', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Age, field: 'Age', width: 75, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Mobile, field: 'Mobile', width: 100, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Email, field: 'Email', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.MemberCardNo, field: 'MembercardNo', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true},
      {headerName: this.names.ContractNo, field: 'ContractNo', width: 100, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'},
        icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.OrderAmount, field: 'OrderAmount', width: 100, suppressMenu: true, suppressFilter: true,
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Status, field: 'Status', suppressMenu: true, suppressFilter: true,  width: 100,
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.BranchName, field: 'BranchName', width: 200, suppressMenu: true, suppressFilter: true,
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: 'RoleId', field: 'RoleId', hide: true, width: 200, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, suppressMovable: true},
      {headerName: 'BranchId', field: 'BranchId', hide: true, width: 200, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, suppressMovable: true},
      {headerName: 'ID', field: 'ID', hide: true, width: 200, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, suppressMovable: true},
      {headerName: 'ContractId', field: 'ContractId', hide: true, width: 200, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, suppressMovable: true},
    ];

  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  memberMouseOver(event) {
    return;
  //   console.log(event)
  //   const x = event.event.clientX + 'px';
  //   const y = event.event.clientY + 'px';

  //   console.log(x, y)
  //   console.log(this.memberInfo)
  //     // this.isHidden = false;
  //     this.memberInfo.nativeElement.style.backgroundColor = '#FF0000';
  //     this.memberInfo.nativeElement.style.top = y;
  //     this.memberInfo.nativeElement.style.marginLeft = x;
  //     // this.renderer.setStyle(this.memberInfo.nativeElement, 'bottom', 0);
  }

  // called when page is done loading. First call to get members done here
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
    this.filterChanged(null);
    if (this.columnsToShow.length > 0) {
      params.columnApi.columnController.allDisplayedColumns.forEach(element => {
        this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(element), false)
        this.columnsToShow.forEach(showCol => {
          if (showCol === element.colId) {
            this.gridColumnApi.setColumnVisible(this.gridColumnApi.getColumn(element), true)
          }
        });
      });
    }

        // auto fits columns to fit screen
        params.api.sizeColumnsToFit();
  }

  onGridReady2(params) {
    this.memberApi = params.api;
    // auto fits columns to fit screen
    params.api.sizeColumnsToFit()
  }

  // on filter changed
  filterChanged(event) {
    // checking in grid if any filters has values
    const values = this.gridApi.filterManager.allFilters;
    const columns = this.gridApi.columnController.columnApi.columnController.displayedCenterColumns;
    const returnList = [];
    const filteredColumns = [['LastName', ''], ['FirstName', ''], ['CustId', ''], ['Age', ''], ['Mobile', ''], ['Email', ''],
      ['MembercardNo', ''], ['ContractNo', ''], ['BranchName', '']];
    filteredColumns.forEach(filterElement => {
        columns.forEach(col => {
          if (col.filterActive) {
            const headerName = col.colDef.field;
            const filterText = values[headerName].filterPromise.resolution.filterText;
            const filterInfo = [headerName, filterText];
            if (filterElement[0] === filterInfo[0]) {
              filterElement = filterInfo;
            }
          }
        })
        returnList.push(filterElement)
    });

  // filter values
  const filterMemberList = {
    LastName: returnList[0][1],
    FirstName: returnList[1][1],
    CustId: returnList[2][1],
    Age: returnList[3][1],
    Mobile: returnList[4][1],
    Email: returnList[5][1],
    MembercardNo: returnList[6][1],
    ContractNo: returnList[7][1],
    BranchName: returnList[8][1],
    FilterBranchId: this.filterBranchId,
    FilterStatusId: this.filterStatusId,
    FilterRoleId: this.filterRoleId
  }
    switch (this.getType) {
      case 'MEMBER':
      // get members from DB. Using filter values
      this.memberService.getMembersList(filterMemberList).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        members => {
          const rowData = [];
          members.Data.forEach(memb => {
            const el = {
              LastName: memb.LastName,
              FirstName: memb.FirstName,
              CustId: memb.CustId,
              Age: memb.Age,
              Mobile: memb.Mobile,
              Email: memb.Email,
              MembercardNo: memb.MembercardNo,
              ContractNo: memb.MainContractNo,
              OrderAmount: memb.OrderAmount,
              Status: memb.StatusName,
              BranchName: memb.BranchName,
              BranchId: memb.BranchID,
              RoleId: memb.RoleId,
              Id: memb.ID,
              ID: memb.ID,
              ContractId: memb.ContractId,
              Name: memb.Name
            }
            rowData.push(el)
          });
          if (rowData.length > 50 && rowData.length === 51) {
            rowData.pop();
            this.searchHasMoreMembers = true;
          }
          this.rowData = rowData;
        }
      )
      break;
      case 'INSTRUCTOR':
      this.memberService.getInstructors('', true).pipe(
        takeUntil(this.destroy$)
      ).subscribe( instructors => {
        const rowData = [];
        instructors.Data.forEach(inst => {
          const el = {
            LastName: inst.LastName,
            FirstName: inst.FirstName,
            CustId: inst.CustId,
            Age: inst.Age,
            Mobile: inst.Mobile,
            Email: inst.Email,
            BranchName: inst.BranchName,
            BranchId: inst.BranchId,
            RoleId: inst.RoleId,
            Id: inst.Id,
            ID: inst.Id,
            Name: inst.Name
          }
          rowData.push(el)
        });
        this.rowData = rowData;
      })
      break;
      case 'EMPLOYEE':
          this.hideRoleSelect = false;
          this.hideStatusSelect = false;
          this.hideBranchSelect = false;
          this.adminService.GetGymEmployees(-2, '', true, -1).pipe(
            takeUntil(this.destroy$)
          ).subscribe(employyes => {
            const rowData = [];
            employyes.Data.forEach(employee => {
              const emp = {
                Id: employee.Id,
                ID: employee.Id,
                CustId: employee.CustId,
                RoleId: MemberRole[employee.RoleId],
                BranchId: employee.BranchId,
                LastName: employee.LastName,
                FirstName: employee.FirstName
              }
              rowData.push(emp);
            });
            this.rowData = rowData;
          })
      break;
    }
  }

  branchChange(branchId) {
    this.filterBranchId = branchId;
    this.filterChanged(null)
  }

  statusChange(statusId) {
    this.filterStatusId = statusId;
    this.filterChanged(null)
  }

  memberSelectedDblClickCalled(event) {
      this.memberSelectedDblClick.next(event)
  }

  rowSelected(event) {
    if (this.allowMultipleSelect === true) {
      event.node.setSelected(true)
      let add = true;
      const member = event.data;
      const list = this.selectedMembers
      list.forEach(mem => {
        if (mem.CustId === member.CustId) {
          add = false;
        }
      });
      if (add) {
        list.push(member)
        this.selectedMembers = list;
        this.memberApi.gridCore.gridOptions.api.setRowData(this.selectedMembers)
      }
    }
  }

  selectedMemberRowSelect(event) {
    this.gridApi.gridOptionsWrapper.gridOptions.api.deselectAll();
    event.node.setSelected(true)
    const member = event.data;
    const list = [];
    this.selectedMembers.forEach(mem => {
      if (mem.CustId !== member.CustId) {
        list.push(mem)
      }
    });
    this.selectedMembers = list;
    this.memberApi.gridCore.gridOptions.api.setRowData(this.selectedMembers)
  }

  okClose(event) {
    if (this.allowMultipleSelect) {
      this.membersSelected.next(this.selectedMembers);
    } else {
      this.closeViewEvent.next(true);
    }
  }

  closeView() {
    this.closeViewEvent.next(true);
  }

  newMemberModal() {
    this.openModalEvent.next(true);
  }
}

