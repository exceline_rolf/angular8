import { Component, OnInit, OnDestroy } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ExceLoginService } from './modules/login/exce-login/exce-login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
// tslint:disable-next-line:import-blacklist
import { Observable, Subscription, Subject, timer } from 'rxjs';
import { ConfigService } from '@ngx-config/core';
import { Extension } from './shared/Utills/Extensions';
import { ExceSessionService } from './shared/services/exce-session.service';
import { VersionCheckService } from './version-check.service';
import { environment } from 'environments/environment.prod';

// Memory leak fix test
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private isUserLoggedIn: boolean = false;
  private _componentName = 'app-component';
  private destroy$ = new Subject<void>();

  timer: any;
  sub: any;
  timerController = 0;
  gymCode: any;
  sessionkey: any;
  timerEvent: Subscription;


  constructor(
    private exceLoginService: ExceLoginService,
    private router: Router,
    private config: ConfigService,
    private exceSessionService: ExceSessionService,
    private versionCheckService: VersionCheckService,
    private exceMessage: ExceMessageService
  ) {
    this.isUserLoggedIn = exceLoginService.IsUserLoggedIn;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (branch) => {
        this.isUserLoggedIn = Boolean(branch.BranchId);
      }
    );
    this.exceLoginService.userLogingOut.pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => {
      this.isUserLoggedIn = false;
      this.router.navigate(['/']);
      }
    );
  }

  ngOnInit(): void {
    this.timer = timer(0, 1000 * 60 * 60);
    // this.timer = Observable.timer(0, 1000);
    this.sub = this.timer.pipe(
      takeUntil(this.destroy$)
    ).subscribe(t => this.tickerFunc(t));
    const params = new URL(location.href).searchParams;
    const companyCode = params.get('CompanyCode');
    if (this.config.getSettings('ENV') === 'DEV') {
      this.gymCode = this.config.getSettings('GYMCODE')
      this.exceLoginService.CompanyCode = this.gymCode;
    } else {
      this.gymCode = companyCode;
      this.exceLoginService.CompanyCode = this.gymCode;
    }
    // get the machine name
    this.sessionkey = Extension.createGuid();
    if (this.sessionkey) {
      this.exceSessionService.AddSession('MACHINE', '1', this.sessionkey, this.gymCode);
      this.timer = timer(0, 2000);
      this.timerEvent = this.timer.subscribe(X => this.getMachineName(X, this.sessionkey));
    }

    // this.versionCheckService.initVersionCheck(environment.versionCheckURL);
  }

  getMachineName(tick: Number, sessionKey: string) {
    if (this.getIsUserLoggedIn()) {
      this.exceSessionService.getSessionValue(sessionKey).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        result => {
          if (result) {
            if (result.Data !== 'ERROR' && result.Data !== 'DUPLICATE' && result.Data !== 'SUCCESS') {
              this.timerEvent.unsubscribe();
              this.exceLoginService.MachineName = result.Data;
            } else {
              // if (tick === 60) {
              //   this.timerEvent.unsubscribe();
              this.exceLoginService.MachineName = 'DEFAULT';
              // }
            }
          }
        }, error => {
          this.timerEvent.unsubscribe();
          this.exceLoginService.MachineName = 'DEFAULT';
        }
      )
    }
  }

  getIsUserLoggedIn() {
    return this.isUserLoggedIn;
  }

  handleClick() {
    this.timerController = 0;
  }

  tickerFunc(tick) {
    if (this.isUserLoggedIn) {
      this.timerController = this.timerController + 1;
      if (this.timerController > 10) {
        this.timerController = 0;
        // this.isUserLoggedIn = false;
        // this.exceLoginService.IsUserLoggedIn = false;
        // Cookie.delete('currentUser');
        // Cookie.set('isUserLoggedIn', 'false')
        // this.exceLoginService.setToken(false);
        // this.exceLoginService.CurrentUser = null;
        // this.exceLoginService.setHttpServiceUserToNull();
        // this.exceLoginService.userLogingOut.emit(true);
        this.exceMessage.openMessageBox('ERROR',
      {
        messageTitle: 'COMMON.Timeout',
        messageBody: 'COMMON.InactiveUse'
      });
      this.exceLoginService.logout()
      }
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
