import { RouteReuseStrategy, ActivatedRouteSnapshot, DetachedRouteHandle } from '@angular/router';

/** Interface that stores:
 * ActivatedRouteSnapshot, useful for determining wheter or not you should attach a route, e.g. this.shouldAttach
 * DetachRouteHandle, offered up by this.retrieve, in case a stored route is to be attached
 */
interface RouteStorageObject {
  snapshot: ActivatedRouteSnapshot,
  handle: DetachedRouteHandle
}

export class CustomRouteReuseStategy implements RouteReuseStrategy {

  /**
   * acceptedRoutes holds the routes which should be attached.
    */
  private acceptedRoutes: string[] = ['card/:Id/:BranchId/:Role/InfoWithNotes']

  /**
   * Object that stores RSOs indexed by keys. Keys will all be a path (as in route.routeConfig.path)
   * Allows for checking if a route is stored for the specified path.
   */
  storedRoutes: { [key: string]: RouteStorageObject } = {};

  /**
   * Check the route.routeConfig.path to see if it is a path that is wished to be stored
   * @param route The route the user currently is on, and would like to know if we want to store it
   * @returns boolean indicating that we want to (true) or do not want to (false) store that route
   */
  shouldDetach(route: ActivatedRouteSnapshot): boolean {
    if (this.acceptedRoutes.indexOf(route.routeConfig.path) > -1 ) {
      return true
    }
    return false;
  }

  /**
   * Creates the object to be stored
   * @param route Stored for later comparison to requested routes
   * @param handle Later to be retrieved by this.retrieve, and offered up to whatever controller is using THIS class
   */
  store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
    const storedRoute: RouteStorageObject = {
      snapshot: route,
      handle: handle
    };
    if (route.data.shouldReuse) {
      this.storedRoutes[route.routeConfig.path] = storedRoute;
    }
  }

  /**
   * Determines whether or not there is a stored route, and if there is, wheter or not it should be rendered in place of the requested route
   * @param route The route the user requested
   * @returns boolean indicating whether or not the render stored the route
   */
  shouldAttach(route: ActivatedRouteSnapshot): boolean {
    return !!route.routeConfig && !!this.storedRoutes[route.routeConfig.path];
  }

  /**
   * If it exists, find the locally stored instance of the requested route, and return it.
   * @param route New route the user has requested.
   * @returns DetachedRouteHandle object which can be used to render the component
   */
  retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
    if (!route.routeConfig) {return null};
    return this.storedRoutes[route.routeConfig.path];
  }

  /**
   * Determine whether or not the current route should be reused
   * @param future The route the user is going to, as triggered by the router
   */
  shouldReuseRoute(future: ActivatedRouteSnapshot): boolean {
    return future.data.shouldReuse || false;
  }

}
