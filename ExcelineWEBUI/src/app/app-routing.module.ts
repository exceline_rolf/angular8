import { Inject, NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRoute, Params, Router } from '@angular/router';

import { UsTileMenuComponent } from './modules/common/us-tile-menu/us-tile-menu.component';
import { LogoutComponent } from './modules/login/logout/logout.component';
import { UsTileFeaturesComponent } from './modules/common/us-tile-menu/us-tile-features/us-tile-features.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';

import { ExceToolbarService } from './modules/common/exce-toolbar/exce-toolbar.service';

import { IAppConfig } from './app-config/app-config.interface';
import { APP_CONFIG } from './app-config/app-config.constants';

import { AuthGuard } from './guards/auth.guard';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ConfigService } from '@ngx-config/core';
import { TranslateService } from '@ngx-translate/core';

export const routes: Routes = [

  { path: '', component: UsTileMenuComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'features/:id', component: UsTileFeaturesComponent },
  { path: 'adminstrator', loadChildren: () => import('app/modules/admin/admin.module').then(m => m.AdminModule) },
  { path: 'sample', loadChildren: () => import('app/modules/sample/sample.module').then(m => m.SampleModule) },
  { path: 'membership', loadChildren: () => import('app/modules/membership/membership.module').then(m => m.MembershipModule) },
  { path: 'shop', loadChildren: () => import('app/modules/shop/shop.module').then(m => m.ShopModule) },
  { path: 'operations', loadChildren: () => import('app/modules/operations/operations.module').then(m => m.OperationsModule) },
  { path: 'class', loadChildren: () => import('app/modules/class/class.module').then(m => m.ClassModule) },
  { path: 'uss-admin', loadChildren: () => import('app/modules/uss-admin/uss-admin.module').then(m => m.UssAdminModule) },

  { path: 'economy', loadChildren: () => import('app/modules/economy/economy.module').then(m => m.EconomyModule) },
  { path: 'reporting', loadChildren: () => import('app/modules/reporting/reporting.module').then(m => m.ReportingModule) },
  { path: 'gym', loadChildren: () => import('app/modules/gym/gym.module').then(m => m.GymModule) },
  { path: '**', component: PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {
  private lang: any;

  constructor(
    private translate: TranslateService,
    private exceToolbarService: ExceToolbarService,
    private route: ActivatedRoute,
    private router: Router,
    private appConfig: ConfigService,
    @Inject(APP_CONFIG) private config: IAppConfig

  ) {

    const langs = [];
    this.config.LANGUAGES.map(ln => {
      langs.push(ln.ID);
    })
    this.translate.addLangs(langs);
    translate.setDefaultLang(this.config.LANGUAGE_DEFAULT.ID);

    this.lang = { id: this.config.LANGUAGE_DEFAULT.ID, title: this.config.LANGUAGE_DEFAULT.NAME }
    this.exceToolbarService.setSelectedLanguage(this.lang);

    this.translate.use(this.lang.id);
    this.exceToolbarService.langUpdated.subscribe(
      (lang) => {
        this.translate.use(lang.id);
      }
    );

    this.route.queryParams.subscribe((params: Params) => {
      const companyCode = params['CompanyCode'];
      if (companyCode) {
        Cookie.set('companyCode', companyCode, this.appConfig.getSettings('COOKIE_EXPIRATION', 'DAYS'));
      }
    })
  }

}

export const routeingComponents = [
  UsTileMenuComponent,
  LogoutComponent,
  PageNotFoundComponent
]


