import { AfterViewInit } from '@angular/core';
import { USPUIFieldStatus } from './../../../../../shared/enums/us-enum';
import { Component, OnInit, EventEmitter, OnDestroy, ViewChild, enableProdMode, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { McBasicInfoService } from '../../mc-basic-info/mc-basic-info.service';
import { ExceMemberService } from '../../../services/exce-member.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ExceToolbarService } from '../../../../common/exce-toolbar/exce-toolbar.service';
import { ExceLoginService } from '../../../../login/exce-login/exce-login.service';
import { Subscription } from 'rxjs';
import { AmountConverterPipe } from '../../../../../shared/pipes/amount-converter.pipe';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Extension } from '../../../../../shared/Utills/Extensions';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { ExceContractTmpSelectComponent } from '../../../../../shared/components/exce-contract-tmp-select/exce-contract-tmp-select.component';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { UsbModalRef } from 'app/shared/components/us-modal/modal-ref';
import deepcopy from 'ts-deepcopy';
import { PreloaderService } from 'app/shared/services/preloader.service';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { ColumnDataType, MemberRole, PaymentTypes } from 'app/shared/enums/us-enum';
import { filter } from 'rxjs/operators';
import { ConfigService } from '@ngx-config/core';
import { toString } from '../../../../../shared/directives/exce-error/util/util';
import { error } from 'util';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { saveAs } from 'file-saver/FileSaver';
import { Router, ActivatedRoute } from '@angular/router';
import { ExceTitleService } from '../../../services/exce-title.service';
import { McContractService } from '../mc-contract.service';
import { ExcePdfViewerService } from 'app/shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { DataTableResource } from 'app/shared/components/us-data-table/tools/data-table-resource';
import { ShopHomeService } from 'app/modules/shop/shop-home/shop-home.service';
import { UsDatePickerComponent } from '../../../../../shared/components/us-date-picker/us-date-picker.component';
import { AdminService } from '../../../../../modules/admin/services/admin.service';
import { IMyDpOptions } from '../../../../../shared/components/us-date-picker/interfaces';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { start } from 'repl';
import { ExceEconomyService } from '../../../../economy/services/exce-economy.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


const now = new Date();
@Component({
  selector: 'mc-contract-details',
  templateUrl: './mc-contract-details.component.html',
  styleUrls: ['./mc-contract-details.component.scss']
})
export class McContractDetailsComponent implements OnInit, OnDestroy {
  /* Private variables */
  private destroy$ = new Subject<void>();
  private _templateType: string;
  private _gymSettings: any;
  private _selectedOrders: any;
  private _contractId = -1;
  private _branchId: number;
  private _gyms: any;
  private _contractCategories: any;
  private _signUpCategories: any;
  private _accessProfiles: any;
  private _gender: any;
  private _selectedMember: any;
  private _memberBranchId = 1;
  private _installments: any;
  private _signedDate: any;
  private _templateOpened = false;
  private _dateFormat: string;
  private _contractConditions: any;
  private deletedItems: any[] = [];
  private _deletedItems: any = [];
  private _addedItems: any = [];
  private rowColors: any;
  private _hit = 1;
  private _installmentList: any[] = [];
  private _invoiceList: any[] = [];
  private _bookingList: any[] = [];
  private _months: any[] = [
    { Id: 1, Name: 'MEMBERSHIP.January' },
    { Id: 2, Name: 'MEMBERSHIP.February' },
    { Id: 3, Name: 'MEMBERSHIP.March' },
    { Id: 4, Name: 'MEMBERSHIP.April' },
    { Id: 5, Name: 'MEMBERSHIP.May' },
    { Id: 6, Name: 'MEMBERSHIP.June' },
    { Id: 7, Name: 'MEMBERSHIP.July' },
    { Id: 8, Name: 'MEMBERSHIP.August' },
    { Id: 9, Name: 'MEMBERSHIP.September' },
    { Id: 10, Name: 'MEMBERSHIP.October' },
    { Id: 11, Name: 'MEMBERSHIP.November' },
    { Id: 12, Name: 'MEMBERSHIP.December' }
  ];
  private _selectedGymSettings: string[] = [
    'EcoEditRemainingPunches',
    'EcoIsMemberFee',
    'EcoIsMemberFeeMonth',
    'OtherPhoneCode',
    'ReqMobileRequired',
    'EcoIsPrintReceiptInShop'
  ]

  /* Public variables */

  addNewMemberModalRef: UsbModalRef;
  isOrderDeleteEnable = true;
  isOrderDeleteVisible = true;
  isAutoRenewEnable = true;
  isAutoRenewVisible = true;
  isPriceGuarantyEnable = true;
  isPriceGuarantyVisible = true;
  isLockUntilEnable = true;
  isLockUntilVisible = true;
  isValidPeriodEnable = true;
  isValidPeriodVisible = true;
  isContractCardGymEnable = true;
  isContractCardGymVisible = true;
  isMonthlyItemPriceEnable = true;
  isMonthlyItemPriceVisible = true;
  isItemPriceEnable = true;
  isItemPriceVisible = true;
  isNextTemplateEnable = true;
  isNextTemplateVisible = true;
  isContractTemplateEnable = true;
  isContractTemplateVisible = true;
  isNoOfvisitsVisible = true;
  isNoOfvisitsEnabled = false;
  isSoldVisitsEnabled = false;
  isSoldVisitsVisible = true;
  _userInfo: any;
  _originalBranchID: any;
  _removingItem: any;
  _removingItemtype: string;
  popupReference: UsbModalRef;
  _popupContent: any;
  _notifyType: string;
  shopModalReference: any;
  items = [];
  itemResource: any;
  modalCreditNoteReference: UsbModalRef;
  _deletingOrder: any;
  modalReference: UsbModalRef;
  _mergingOrder: any;
  enableRenew = false;
  _isOrderInvoicevisible = true;
  _isOrderSMSEmailvisible = true;
  _isOrderCheckOutvisible = true;
  _isOrderInvoiceEnable = true;
  _isOrderSMSEmailEnable = true;
  _isOrderCheckOutEnable = true;
  DefaultOperationStatus = true;
  _serviceAmountDifference: any;
  ContractChanged = false;
  _user: any;
  bookingModalRef: UsbModalRef;
  addGroupModalRef: UsbModalRef;
  _removingMember: any;
  orderlineModel: UsbModalRef;
  contractItemModel: UsbModalRef;
  templateModel: UsbModalRef;
  groupmembersModel: UsbModalRef;
  contractConditionModel: UsbModalRef;
  _orderlineType: string;
  _deletedItem: any;
  public contractForm: FormGroup;
  public selectedContract: any;
  public selectedIndex = 1;
  public locale: string;
  selectedOrder: any;
  selectionType: string;
  public startUpitems: any = [];
  public monthlyItems: any = [];
  public groupMembers: any = [];
  public groupMemberCount = 0;
  public itemCount: number;
  public entityItems = [];
  public multipleRowSelect = true;
  public selectedDetail: any = {};
  public enableResignRemove = false;
  public autocompleteItems = [{ id: 'NAME', name: 'NAMEE :', isNumber: false }, { id: 'ID', name: 'Idd :', isNumber: true }];
  public columns = [
    { property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DATE },
    { property: 'Mobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
  ];
  public entitySelectionType = 'MEM';
  public defaultRoleType = 'ALL';
  @ViewChild('dataTable', { static: false }) public dataTable;

  public IsBrisIntegrated = false;
  public BtnDeleteOrderEnable = true;
  public BtnDeleteOrderVisible = true;
  public orderOpreationDisabled = true;
  public memberInvoicesList: any[] = [];
  public selectedCreditNote: any = {};


  limit = 25;



  public firstOrderHas = true;

  blobUrl: any;
  blob: any;
  currentInvoice: any;
  generatePdfView: UsbModalRef;

  isShowInfo = false;
  type: string;
  message: string;


  @ViewChild('memberFeeLastInvoicedDate', { static: false }) memberFeeLastInvoicedDate: UsDatePickerComponent;
  @ViewChild('signedDate', { static: false }) signedDate: UsDatePickerComponent;
  @ViewChild('ContractStartDate', { static: false }) ContractStartDate: UsDatePickerComponent;
  @ViewChild('Pdfwindow', { static: true }) Pdfwindow: ElementRef;
  @ViewChild('shopHomeModel', { static: true }) shopModal: ElementRef;
  @ViewChild('invoiceNotify', { static: true }) invoiceNotify: ElementRef;
  @ViewChild('orderDetails', { static: true }) orderDetails: ElementRef;
  @ViewChild('creditNote', { static: true }) creditNote: ElementRef;
  @ViewChild('invoiceDetails', { static: true }) invoiceDetails: ElementRef;

  _isMemberFee: any;
  _isMemberFeeMonth: any;
  isBookingActivity = false;


  formErrors = {
    'ContractStartDate': '',
    'ContractEndDate': '',
    'TemplateNo': '',
    'TemplateName': '',
    'NextTemplateNo': '',
    'NextTemplateName': '',
    'StartUpItemPrice': '',
    'ServiceAmount': '',
    'ContractPrize': '',
    'MemberFeeArticleName': '',
    'GroupContractMemberCount': '',
    'contractCondition': '',
    'signedDate': '',
    'NumberOfVisits': '',
    'AvailableVisits': '',
    'SoldPunches': '',
    'LockInPeriodUntilDate': '',
    'PriceGuarantyUntillDate': '',
    'RenewedDate': '',
    'ClosedDate': '',
    'ResignCategory': '',
    'IsATG': '',
    'IsInvoiceDetails': '',
    'AutoRenew': '',
    'memberFeeLastInvoicedDate': ''
  };

  validationMessages = {
    'ContractStartDate': {
      'required': 'REPORT.ContractStartDateReq'
    },
    'ContractEndDate': {
      'required': 'REPORT.ContractEndDateReq',
      'compare' : 'REPORT.ContractStartEndDateCompare'
    },
  };
  isContractFormSubmitted: boolean;
  orderDefs: any;
  invoiceDefs: any;
  lang: any;

  names = {
    No: '#',
    Periode: 'Periode',
    OrderAmount: 'Ordrebeløp',
    Duedate: 'Forfall',
    ID: 'ID',
    InvoiceNo: 'Fakturanummer',
    InvoiceDate: 'Fakturert',
    Total: 'Total',
    Balance: 'Saldo',
    Payer: 'Betalt av/for',
    Content: 'Innhold',
    Type: 'Type',
    Status: 'Status'
  }

  colNamesNo = {
    No: '#',
    Periode: 'Periode',
    OrderAmount: 'Ordrebeløp',
    Duedate: 'Forfall',
    ID: 'ID',
    InvoiceNo: 'Fakturanummer',
    InvoiceDate: 'Fakturert',
    Total: 'Total',
    Balance: 'Saldo',
    Payer: 'Betalt av/for',
    Content: 'Innhold',
    Type: 'Type',
    Status: 'Status'
  }

  colNamesEn = {
    No: '#',
    Periode: 'Periode',
    OrderAmount: 'Order amount',
    Duedate: 'Due date',
    ID: 'ID',
    InvoiceNo: 'Invoice no',
    InvoiceDate: 'Invoice date',
    Total: 'Total',
    Balance: 'Balance',
    Payer: 'Paid for/by',
    Content: 'Content',
    Type: 'Type',
    Status: 'Status'
  }


  gridApi: any;
  gridColumnApi: any;
  gridApiInv: any;
  gridColumnApiInv: any;
  getRowHeight: (params: any) => any;
  gridIsSized = false;
  orderListSelected = true;
  mergeOrdersDisable = true;
  saveEvent: Subscription;


  private _isFreezeDetailsNeeded = false;
  private _isFreezeView = false;
  hasNewerTraingContract = false;

  constructor(
    private adminService: AdminService,
    private basicInfoService: McBasicInfoService,
    private memberService: ExceMemberService,
    private loginservice: ExceLoginService,
    private toolBarService: ExceToolbarService,
    private fb: FormBuilder,
    private modalService: UsbModal,
    private messageService: ExceMessageService,
    private translateService: TranslateService,
    private config: ConfigService,
    private router: Router,
    private sanitizer: DomSanitizer,
    private titleService: ExceTitleService,
    private economyService: ExceEconomyService,
    private contractService: McContractService,
    private excePdfViewerService: ExcePdfViewerService,
    private shopHomeService: ShopHomeService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.adminService.GetGymCompanySettings('other').pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      this.IsBrisIntegrated = result.Data.IsBrisIntegrated;
    })
    this._contractId = this.route.snapshot.params['ContractId'];
    this.titleService.setTitle('MEMBERSHIP.Contract');
    this._branchId = this.loginservice.SelectedBranch.BranchId;
    this.fetchData(this.loginservice.SelectedBranch.BranchId);

    this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
    ).subscribe(currentMember => this._selectedMember = currentMember);

    this._user = this.loginservice.CurrentUser.username;
    this._userInfo = this.loginservice.UserInfo;
    this.contractForm = this.fb.group({
      'TemplateNo': [null, Validators.required],
      'TemplateName': [null],
      'NextTemplateNo': [null],
      'NextTemplateName': [null],
      'StartUpItemPrice': [null],
      'ServiceAmount': [null],
      'ContractPrize': [null],
      'MemberFeeArticleName': [null],
      'GroupContractMemberCount': [null],
      'contractCondition': [null],
      'signedDate': [null],
      'ContractStartDate': [null, [Validators.required]],
      'ContractEndDate': [null, [Validators.required]],
      'NumberOfVisits': [null],
      'AvailableVisits': [null],
      'SoldPunches': [null],
      'LockInPeriodUntilDate': [null],
      'PriceGuarantyUntillDate': [null],
      'RenewedDate': [null],
      'ClosedDate': [null],
      'ResignCategory': [null],
      'IsATG': [null],
      'IsInvoiceDetails': [null],
      'AutoRenew': [null],
      'memberFeeLastInvoicedDate': [null]

    });
    this.handleSubOperations();

    this.lodaContract(true);

    this.memberService.getAccessProfiles(this._selectedMember.Gender).pipe(takeUntil(this.destroy$)).subscribe(
      accessresult => {
        if (accessresult) {
          this._accessProfiles = accessresult.Data;
        }
      }
    );

    this.memberService.getCategories('PACKAGE').pipe(takeUntil(this.destroy$)).subscribe(
      packageresult => {
        if (packageresult) {
          this._contractCategories = packageresult.Data;
        }
      }
    );

    this.memberService.getCategories('SIGNUP').pipe(takeUntil(this.destroy$)).subscribe(
      signupresult => {
        if (signupresult) {
          this._signUpCategories = signupresult.Data;
        }
      }
    );

    this.memberService.getBranches().pipe(takeUntil(this.destroy$)).subscribe(
      brresult => {
        if (brresult) {
          this._gyms = brresult.Data;
        }
      }
    );

    this.memberService.getSelectedGymSettings({ settingNames: this._selectedGymSettings, isGymSetting: true, branchId: this._branchId }).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this._gymSettings = result.Data;
        }
      }
    );

    const language = this.toolBarService.getSelectedLanguage();
    this.lang = language;
    this.locale = language.culture;
    this._dateFormat = 'DD.MM.YYYY';

    switch (this.lang.id) {
      case 'no':
        this.names = this.colNamesNo;
        break;
      case 'en':
        this.names = this.colNamesEn;
        break;
    }

    this.toolBarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );

    this.basicInfoService.updatedOrderList.pipe(
      takeUntil(this.destroy$)
      ).subscribe(res => {
      if (res === 'update-from-freese-success') {
        this.getInstallments();
      }
    });

    this.messageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
      switch (value.id) {
        case 'MONTHLY':
          this.deleteMonthlyItem();
          break;
        case 'ITEM':
          this.deleteStartUpItem();
          break;
        case 'GROUP':
          this.removeGroupMember();
          break;
        case 'MERGE':
          this.mergeSelectedOrders();
          break;
        case 'ORDER':
          this.deleteOrder();
          break;
        case 'INVOICE':
          this.generateInvoice(true);
          break;
        case 'ADDINVOICEFEE':
          if (this._notifyType === 'PRINT') {
            this.generateInvoiceDocument(true);
          } else if (this._notifyType === 'SMS_EMAIL') {
            this.generateInvoiceEmailSMS(true);
          }

          break;
        case 'RESIGN':
          this.removeResignation()
          break;
        case 'NEXT':
          this.removeNextTemplate();
          break;
        case 'MEMBERFEE':
          this.removeMemberFee();
          break;
        case 'CONTRACTCON':
          this.removeContractCondition();
          break;
        case 'CONITEM':
          this.deleteContractItem();
          break;
      }
    });

    this.messageService.noCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      switch (value.id) {
        case 'ADDINVOICEFEE':
          if (this._notifyType === 'PRINT') {
            this.generateInvoiceDocument(false);
          } else if (this._notifyType === 'SMS_EMAIL') {
            this.generateInvoiceEmailSMS(false);
          }
          break;
        case 'INVOICE':
          this.generateInvoice(false);
          break;
      }
    });

    this.orderDefs = [
      {headerName: this.names.No, field: 'InstallmentNo', width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true,
      headerCheckboxSelection: true,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: true},
      {headerName: this.names.Periode, field: 'Text', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.OrderAmount, field: 'Amount', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return;
        }}},
      {headerName: this.names.Duedate, field: 'DueDate', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const d = params.value.split('T');
          const dDate = d[0].split('-');
          return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
        } else {
          return
        }}},
      {headerName: '', field: 'Betaling', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.data.IsOrderCheckOutvisible) {
          if (params.data.IsCheckOutEnable) {
            return '<button class="btn btn-primary btn-icon-min" title="Betaling"><span class="icon-checkout"></span></button>';
          } else {
            return '<button disabled class="btn btn-primary btn-icon-min" title="Betaling"><span class="icon-checkout"></span></button>';
          }
        } else {
          return
        }
        }
      },
      {headerName: '', field: 'SMS/EPOST', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.data.IsOrderSMSEmailvisible) {
          if (params.data.OrderOperationsEnabled) {
            return '<button class="btn btn-primary btn-icon-min" title="SMS/Epost faktura"><span class="icon-sms"></span></button>';
          } else {
            return '<button disabled class="btn btn-primary btn-icon-min" title="SMS/Epost faktura"><span class="icon-sms"></span></button>';
          }
        } else {
          return
        }
        }
      },
      {headerName: '', field: 'Faktura', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.data.IsOrderInvoicevisible) {
          if (params.data.OrderOperationsEnabled) {
            return '<button class="btn btn-primary btn-icon-min" title="Opprett faktura"><span class="icon-invoice"></span></button>';
          } else {
            return '<button disabled class="btn btn-primary btn-icon-min" title="Opprett faktura"><span class="icon-invoice"></span></button>';
          }
        } else {
          return;
        }
        }
      },
      {headerName: '', field: 'Detaljer', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        return '<button class="btn btn-primary btn-icon-min" title="Ordredetaljer"><span class="icon-detail"></span></button>';
        }
      },
      {headerName: '', field: 'Slett', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, hide: !this.isOrderDeleteVisible, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.data.HasAddonFromShop) {
          return '<button disabled class="btn btn-danger btn-icon-min" title="Slett"><span class="icon-cancel"></span></button>';
        } else {
          return '<button class="btn btn-danger btn-icon-min" title="Slett"><span class="icon-cancel"></span></button>';
        }
        }
      },
      {headerName: '', field: '', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 20, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, hide: !this.isOrderDeleteVisible, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true
      },
    ];

    this.invoiceDefs = [
      {headerName: this.names.ID, field: 'MembeInvoiceId', suppressMenu: true, width: 60, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.InvoiceNo, field: 'InvoiceNo', suppressMenu: true, width: 60, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true},
    {headerName: this.names.InvoiceDate, field: 'InvoiceDate', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const d = params.value.split('T');
          const dDate = d[0].split('-');
          return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
        } else {
          return
        }
      }
    },
      {headerName: this.names.Duedate, field: 'InvoiceDueDate', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const d = params.value.split('T');
          const dDate = d[0].split('-');
          return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
        } else {
          return
        }
      }
    },
    {headerName: this.names.Total, field: 'InvoiceAmount', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, width: 60, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return;
        }
      }
    },
    {headerName: this.names.Balance, field: 'InvoiceBalance', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, width: 60, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return;
        }
      }
    },
    {headerName: this.names.Payer, field: 'PayeePayerName', autoHeight: true, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, width: 80, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        switch (params.data.PayDirection) {
          case 1:
            return '<div class="d-flex align-items-center">'
              + '<span><svg class="d-block" version="1.1" id="pthleft" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"'
              + 'x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path fill="#2286D4" d="M12,0c6.6,0,12,5.4,12,12s-5.4,12-12,12C5.4,24,0,18.6,0,12S5.4,0,12,0z M10.5,16.5v-3c0,0,6-1.5,9,3c0-5-4-9-9-9v-3l-6,6L10.5,16.5z" />'
              + '</svg></span><span>' + params.value + '</span></div>';
          case 2:
            return '<div class="d-flex align-items-center">'
                + '<span><svg class="d-block" version="1.1" id="pthright" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"'
                + 'x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
                + 'xml:space="preserve">'
                + '<path fill="#C6731F" d="M12,0C5.4,0,0,5.4,0,12s5.4,12,12,12c6.6,0,12-5.4,12-12S18.6,0,12,0z M13.5,16.5v-3c0,0-6-1.5-9,3c0-5,4-9,9-9v-3l6,6L13.5,16.5z" />'
                + '</svg></span><span>' + params.value + '</span></div>';
          default:
            return;
        }
      }
    },
    {headerName: '', field: 'Innhold', suppressMenu: true, autoHeight: true, suppressSorting: true, suppressFilter: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, width: 50, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        return '<ng-template><button class="btn btn-primary btn-icon-min" title="pdf">'
            + '<span class="icon-pdf"></span></button></ng-template>'
        }
      },
      {headerName: 'Type', field: 'InvoiceType', suppressMenu: true, autoHeight: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, width: 50, suppressFilter: true, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        switch (params.value) {
          case 'ATG':
            return '<span title="' + params.data.InvoiceType + '">'
              + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="M5.5,10.3l1,3.5c0,0,0,0.1,0,0.1h-2c0,0-0.1,0,0-0.1L5.5,10.3C5.4,10.3,5.4,10.3,5.5,10.3C5.5,10.3,5.5,10.3,5.5,10.3z M23.8,12c0,6.5-5.3,11.8-11.8,11.8C5.5,23.8,0.2,18.5,0.2,12C0.2,5.5,5.4,0.2,12,0.2C18.5,0.2,23.8,5.5,23.8,12z M8.4,16.2c0-0.1,0-0.1,0-0.2L6,8C5.9,7.7,5.8,7.6,5.5,7.6C5.2,7.6,5,7.7,4.9,8l-2.4,8c0,0.1,0,0.1,0,0.2c0,0.2,0.1,0.3,0.2,0.4c0.1,0.1,0.3,0.2,0.4,0.2c0.3,0,0.5-0.1,0.6-0.4L4,15.1c0,0,0-0.1,0.1-0.1h2.7c0,0,0.1,0,0.1,0.1l0.4,1.3c0.1,0.3,0.3,0.4,0.6,0.4c0.2,0,0.3-0.1,0.4-0.2C8.4,16.5,8.4,16.4,8.4,16.2z M14.9,8.2c0-0.2-0.1-0.3-0.2-0.4c-0.1-0.1-0.3-0.2-0.4-0.2h-3.9c-0.2,0-0.3,0.1-0.4,0.2C9.9,7.9,9.8,8,9.8,8.2S9.9,8.5,10,8.6c0.1,0.1,0.3,0.2,0.4,0.2h1.3c0,0,0.1,0,0.1,0.1v7.4c0,0.2,0.1,0.3,0.2,0.4c0.1,0.1,0.3,0.2,0.4,0.2s0.3-0.1,0.4-0.2c0.1-0.1,0.2-0.3,0.2-0.4V8.8c0,0,0-0.1,0.1-0.1h1.3c0.2,0,0.3-0.1,0.4-0.2C14.9,8.5,14.9,8.3,14.9,8.2z M21.5,14.6v-2.4c0-0.2-0.1-0.3-0.2-0.4c-0.1-0.1-0.3-0.2-0.4-0.2h-1.4c-0.2,0-0.3,0.1-0.4,0.2c-0.1,0.1-0.2,0.3-0.2,0.4s0.1,0.3,0.2,0.4c0.1,0.1,0.3,0.2,0.4,0.2h0.7c0,0,0.1,0,0.1,0.1v1.8c0,0.3-0.1,0.5-0.3,0.8s-0.5,0.3-0.8,0.3h-0.6c-0.3,0-0.5-0.1-0.8-0.3c-0.2-0.2-0.3-0.5-0.3-0.8V9.8c0-0.3,0.1-0.6,0.3-0.8s0.5-0.3,0.8-0.3h0.6c0.5,0,0.8,0.2,1,0.6c0.1,0.2,0.3,0.3,0.5,0.3c0.2,0,0.3-0.1,0.4-0.2c0.1-0.1,0.2-0.2,0.2-0.4c0-0.1,0-0.2-0.1-0.3c-0.2-0.4-0.5-0.7-0.8-0.9c-0.4-0.2-0.8-0.3-1.2-0.3h-0.6c-0.6,0-1.1,0.2-1.6,0.7c-0.4,0.4-0.7,1-0.7,1.6v4.8c0,0.6,0.2,1.1,0.7,1.6c0.4,0.4,1,0.7,1.6,0.7h0.6c0.6,0,1.1-0.2,1.6-0.7C21.3,15.7,21.5,15.2,21.5,14.6z"'
              + '/>'
              + '</svg></span>'
          case 'SMS':
            return '<span title="' + params.data.InvoiceType + '">'
              + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="M12,0.2C5.4,0.2,0.2,5.5,0.2,12c0,6.5,5.3,11.8,11.8,11.8c6.5,0,11.8-5.3,11.8-11.8C23.8,5.5,18.5,0.2,12,0.2z M18.9,14.7c0,0.7-0.6,1.3-1.3,1.3h-3l-5.9,4l1.4-4H6.8c-0.7,0-1.3-0.6-1.3-1.3V7.3C5.5,6.6,6.1,6,6.8,6h10.8c0.7,0,1.3,0.6,1.3,1.3V14.7z"'
              + '/>'
              + '</svg></span>'
          case 'IN':
          return '<span title="' + params.data.InvoiceType + '">'
            + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="M12,0.2C5.4,0.2,0.2,5.5,0.2,12S5.5,23.8,12,23.8S23.8,18.5,23.8,12S18.5,0.2,12,0.2z M7.6,16 c0,0.4-0.1,0.7-0.3,1c-0.2,0.2-0.4,0.3-0.8,0.3c-0.3,0-0.5-0.1-0.7-0.3c-0.2-0.2-0.3-0.5-0.3-1V8c0-0.4,0.1-0.7,0.3-0.9 C6,6.9,6.3,6.8,6.6,6.8s0.6,0.1,0.8,0.3c0.2,0.2,0.3,0.5,0.3,1V16z M18.4,15.9c0,0.9-0.4,1.3-1.1,1.3c-0.2,0-0.3,0-0.5-0.1 c-0.1,0-0.3-0.1-0.4-0.2c-0.1-0.1-0.2-0.2-0.3-0.4c-0.1-0.1-0.2-0.3-0.3-0.5l-3.8-5.9v5.9c0,0.4-0.1,0.7-0.3,0.9 c-0.2,0.2-0.4,0.3-0.7,0.3c-0.3,0-0.5-0.1-0.7-0.3c-0.2-0.2-0.2-0.5-0.2-0.9V8.3c0-0.3,0-0.6,0.1-0.8c0.1-0.2,0.2-0.4,0.4-0.5 c0.2-0.1,0.4-0.2,0.7-0.2c0.2,0,0.3,0,0.5,0.1S11.9,7,12,7.1c0.1,0.1,0.2,0.2,0.3,0.4c0.1,0.1,0.2,0.3,0.3,0.5l3.9,5.9v-6 c0-0.4,0.1-0.7,0.2-0.9c0.2-0.2,0.4-0.3,0.7-0.3c0.3,0,0.5,0.1,0.7,0.3c0.2,0.2,0.2,0.5,0.2,0.9V15.9z"'
              + '/> </svg></span>';
          case 'Shop':
          return '<span title="' + params.data.InvoiceType + '">'
              + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="m15.3 8.4h-6.6c0-1.9 1.5-3.4 3.3-3.4s3.3 1.5 3.3 3.4zm8.5 3.6c0 6.5-5.3 11.8-11.8 11.8s-11.8-5.3-11.8-11.8 5.2-11.8 11.8-11.8c6.5 0 11.8 5.3 11.8 11.8zm-5.7 2.6l0.4-6.2h-2.5c0-2.3-1.8-4.1-4.1-4.1s-4.1 1.8-4.1 4.1h-2.4l0.6 6.2-0.3 4h12.7l-0.3-4z"'
              + '/> </svg></span>';
          case 'IP':
            return '<span title="' + params.data.InvoiceType + '">'
              + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="M16.6,9.1c0.3,0.3,0.4,0.7,0.4,1.1c0,0.4-0.1,0.7-0.3,0.9c-0.2,0.2-0.4,0.4-0.8,0.5c-0.3,0.1-0.8,0.2-1.3,0.2h-1.4V8.7h1.4C15.7,8.7,16.3,8.8,16.6,9.1z M23.8,12c0,6.5-5.3,11.8-11.8,11.8S0.2,18.5,0.2,12S5.4,0.2,12,0.2C18.5,0.2,23.8,5.5,23.8,12z M8.7,8.2c0-0.4-0.1-0.8-0.3-1S7.9,6.9,7.6,6.9C7.3,6.9,7.1,7,6.9,7.2s-0.3,0.5-0.3,1v8.3c0,0.4,0.1,0.8,0.3,1s0.5,0.3,0.8,0.3c0.3,0,0.6-0.1,0.8-0.3s0.3-0.5,0.3-1V8.2z M19.2,10.2c0-0.5-0.1-1-0.2-1.4c-0.2-0.4-0.4-0.7-0.7-1c-0.3-0.3-0.7-0.5-1.1-0.6C16.7,7.1,16,7,15.2,7h-2.8c-0.5,0-0.8,0.1-1,0.3c-0.2,0.2-0.3,0.5-0.3,1v8.2c0,0.4,0.1,0.7,0.3,1s0.5,0.3,0.8,0.3c0.3,0,0.6-0.1,0.8-0.3c0.2-0.2,0.3-0.5,0.3-1v-3h2c1.3,0,2.3-0.3,3-0.8C18.9,12.1,19.2,11.3,19.2,10.2z"'
              + '/> </svg></span>';
          case 'PP':
            return '<span title="' + params.data.InvoiceType + '">'
            + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
            + 'xml:space="preserve">'
            + '<path class="fill-primary" d="M16.8,8.7h-1.4v3.1h1.4c0.5,0,0.9-0.1,1.2-0.2c0.3-0.1,0.6-0.3,0.8-0.5s0.3-0.5,0.3-0.9c0-0.5-0.1-0.8-0.4-1.1C18.4,8.9,17.8,8.7,16.8,8.7z M7.3,8.7H5.9v3.1h1.4c0.5,0,0.9-0.1,1.2-0.2c0.3-0.1,0.6-0.3,0.8-0.5s0.3-0.5,0.3-0.9c0-0.5-0.1-0.8-0.4-1.1C8.8,8.9,8.2,8.7,7.3,8.7z M12,0.2C5.4,0.2,0.2,5.5,0.2,12S5.5,23.8,12,23.8S23.8,18.5,23.8,12S18.5,0.2,12,0.2z M10.6,12.6C10,13.2,9,13.4,7.8,13.4H5.9v2.9c0,0.4-0.1,0.7-0.3,0.9c-0.2,0.2-0.4,0.3-0.7,0.3c-0.3,0-0.6-0.1-0.8-0.3c-0.2-0.2-0.3-0.5-0.3-0.9V8.4c0-0.5,0.1-0.8,0.3-1c0.2-0.2,0.5-0.3,1-0.3h2.7c0.8,0,1.4,0.1,1.8,0.2c0.4,0.1,0.8,0.3,1.1,0.6s0.5,0.6,0.7,1c0.2,0.4,0.2,0.8,0.2,1.3C11.6,11.3,11.3,12.1,10.6,12.6z M20.2,12.6c-0.6,0.5-1.6,0.8-2.9,0.8h-1.9v2.9c0,0.4-0.1,0.7-0.3,0.9c-0.2,0.2-0.4,0.3-0.7,0.3s-0.6-0.1-0.8-0.3c-0.2-0.2-0.3-0.5-0.3-0.9V8.4c0-0.5,0.1-0.8,0.3-1s0.5-0.3,1-0.3h2.7c0.8,0,1.4,0.1,1.8,0.2s0.8,0.3,1.1,0.6c0.3,0.3,0.5,0.6,0.7,1s0.2,0.8,0.2,1.3C21.2,11.3,20.9,12.1,20.2,12.6z"'
            + '/> </svg></span>';
          case 'SP':
            return '<span title="' + params.data.InvoiceType + '">'
            + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
            + ' xml:space="preserve">'
            + '<path class="fill-primary" d="M18.8,9.4c0.3,0.3,0.4,0.7,0.4,1.1c0,0.4-0.1,0.7-0.3,0.9s-0.4,0.4-0.8,0.5c-0.3,0.1-0.7,0.2-1.2,0.2h-1.4V8.9h1.4C17.9,8.9,18.5,9.1,18.8,9.4z M23.8,12c0,6.5-5.3,11.8-11.8,11.8S0.2,18.5,0.2,12S5.4,0.2,12,0.2C18.5,0.2,23.8,5.5,23.8,12z M11.6,14.6c0-0.5-0.1-1-0.3-1.3s-0.4-0.7-0.8-0.9c-0.3-0.2-0.7-0.4-1.2-0.6c-0.5-0.2-1-0.3-1.6-0.5c-0.5-0.1-0.8-0.2-1-0.3c-0.2-0.1-0.4-0.1-0.6-0.3c-0.2-0.1-0.4-0.2-0.5-0.4s-0.2-0.3-0.2-0.5C5.5,9.5,5.7,9.3,6,9c0.3-0.2,0.8-0.4,1.3-0.4c0.6,0,1,0.1,1.3,0.3c0.3,0.2,0.5,0.5,0.7,0.9c0.1,0.3,0.3,0.5,0.4,0.6c0.1,0.1,0.3,0.2,0.5,0.2c0.3,0,0.5-0.1,0.7-0.3c0.2-0.2,0.3-0.4,0.3-0.7c0-0.3-0.1-0.6-0.2-0.8c-0.1-0.3-0.4-0.6-0.7-0.8S9.6,7.6,9.1,7.4c-0.5-0.2-1-0.2-1.7-0.2C6.7,7.2,6,7.3,5.4,7.5s-1,0.6-1.4,1C3.7,9,3.6,9.5,3.6,10.1c0,0.6,0.1,1.1,0.4,1.5c0.3,0.4,0.7,0.7,1.2,1c0.5,0.2,1.1,0.4,1.9,0.6c0.6,0.1,1,0.2,1.3,0.4C8.8,13.6,9,13.8,9.2,14c0.2,0.2,0.3,0.5,0.3,0.8c0,0.4-0.2,0.8-0.6,1.1c-0.4,0.3-0.9,0.4-1.5,0.4c-0.5,0-0.8-0.1-1.1-0.2s-0.5-0.3-0.6-0.5c-0.2-0.2-0.3-0.5-0.4-0.8c-0.1-0.3-0.2-0.5-0.4-0.6c-0.2-0.1-0.3-0.2-0.6-0.2c-0.3,0-0.5,0.1-0.7,0.3c-0.2,0.2-0.3,0.4-0.3,0.7c0,0.4,0.1,0.9,0.4,1.4C4.1,16.7,4.5,17,5,17.3c0.7,0.4,1.5,0.6,2.5,0.6c0.9,0,1.6-0.1,2.2-0.4c0.6-0.3,1.1-0.7,1.4-1.2C11.5,15.8,11.6,15.2,11.6,14.6z M21.3,10.5c0-0.5-0.1-0.9-0.2-1.3c-0.2-0.4-0.4-0.7-0.7-1c-0.3-0.3-0.7-0.5-1.1-0.6c-0.4-0.1-1-0.2-1.8-0.2h-2.7c-0.5,0-0.8,0.1-1,0.3c-0.2,0.2-0.3,0.5-0.3,1v8c0,0.4,0.1,0.7,0.3,0.9c0.2,0.2,0.4,0.3,0.8,0.3c0.3,0,0.6-0.1,0.7-0.3c0.2-0.2,0.3-0.5,0.3-1v-2.9h1.9c1.3,0,2.3-0.3,2.9-0.8C21,12.3,21.3,11.5,21.3,10.5z"'
            + '/> </svg></span>';
          case 'Email':
            return '<span title="' + params.data.InvoiceType + '">'
            + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
            + 'xml:space="preserve">'
            + '<path class="fill-primary" d="M12,0.2C5.4,0.2,0.2,5.5,0.2,12S5.5,23.8,12,23.8S23.8,18.5,23.8,12S18.5,0.2,12,0.2z M12,6.5h6.4l-3.2,3.2L12,12.9L8.8,9.7L5.6,6.5H12z M19.3,17.1H4.7V7.6h0L8.2,11l3.8,3.7l3.8-3.7l3.4-3.4h0V17.1z"'
            + '/> </svg></span>';
          case 'Paper':
            return '<span title="' + params.data.InvoiceType + '">'
              + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="M12,0.2C5.4,0.2,0.2,5.5,0.2,12S5.5,23.8,12,23.8S23.8,18.5,23.8,12S18.5,0.2,12,0.2z M17.8,17.6c0,0.6-0.5,1.1-1.1,1.1H7.3c-0.6,0-1.1-0.5-1.1-1.1V6.4c0-0.6,0.5-1.1,1.1-1.1h5.9v3c0,0.9,0.7,1.6,1.6,1.6h3V17.6z M17.8,8.9L17.8,8.9h-3c-0.4,0-0.7-0.3-0.7-0.7v-3L17.8,8.9L17.8,8.9L17.8,8.9z"'
              + '/> </svg></span>';
          case 'CN':
            return '<span title="' + params.data.InvoiceType + '">'
            + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
            + 'xml:space="preserve">'
            + '<path class="fill-primary" d="M6.7,17.9h7.4v-3.1h3.1V6.1H6.7V17.9z M10.1,8.9h5v1.2h-5V8.9z M8.3,13.3c0.3-0.3,0.8-0.3,1.1,0l0.8,0.8l2.8-2.8c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1l-3.8,3.8l-1.9-1.9C8.1,14.1,8.1,13.6,8.3,13.3z M12,0.2C5.4,0.2,0.2,5.5,0.2,12S5.5,23.8,12,23.8S23.8,18.5,23.8,12S18.5,0.2,12,0.2z M18.5,15.4c0,0.2-0.1,0.3-0.2,0.4l-3.1,3.1c-0.1,0.1-0.3,0.2-0.4,0.2H6.1c-0.3,0-0.6-0.3-0.6-0.6v-13c0-0.3,0.3-0.6,0.6-0.6h11.8c0.3,0,0.6,0.3,0.6,0.6V15.4z"'
            + '/> </svg></span>'
        default:
          return;
        }}},
        {headerName: 'Status', field: 'InvoiceStatus', suppressMenu: true, autoHeight: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, width: 80, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
        }, suppressMovable: true, cellRenderer: function(params) {
            if (params.data.IsPartiallyPaid) {
              return ' <span class="badge badge-warning py-1">' + params.value + '</span>';
            } else if (params.data.IsFullyPaid) {
              return '<span class="badge badge-success py-1">' + params.value + '</span>';
            } else if (params.data.IsNotPaid) {
              return '<span class="badge badge-danger py-1">' + params.value + '</span>';
            }
          }

        },
        {headerName: '', field: 'Detaljer', suppressMenu: true, suppressSorting: true, autoHeight: true, suppressFilter: true, floatingFilterComponentParams: {suppressFilterButton: true},
          filterParams: {newRowsAction: 'keep'}, width: 50, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
          }, suppressMovable: true, cellRenderer: function(params) {
            return '<ng-template> <button class="btn btn-primary btn-icon-min" title="Detaljer"><span class="icon-detail"></span></button></ng-template>';
        }
      },
    ];

    this.getRowHeight = function(params) {
      return 30;
    }

  }

  onGridReady(params, type) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    params.api.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  onGridReadyInvoices(params) {
    this.gridApiInv = params.api;
    this.gridColumnApiInv = params.columnApi;
    // auto fits columns to fit screen
    this.gridApiInv.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApiInv.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  cellClickedOrder(event) {
    const order = event.data;
    const column = event.column.colDef.field;
    switch (column) {
      case 'Betaling':
      if (event.data.IsOrderCheckOutvisible && event.data.IsCheckOutEnable) {
        this.openShop(this.shopModal, order)
      }
      break;
      case 'SMS/EPOST':
      if (event.data.IsOrderSMSEmailvisible && event.data.OrderOperationsEnabled) {
        this.generateInvoiceEvent(order, this.invoiceNotify, 'SMS_EMAIL');
      }
      break;
      case 'Faktura':
      if (event.data.IsOrderInvoicevisible && event.data.OrderOperationsEnabled) {
        this.generateInvoiceEvent(order, this.invoiceNotify, 'PRINT')
      }
      break;
      case 'Detaljer':
      this.openDetailView(this.orderDetails, order)
      break;
      case 'Slett':
      if (event.data.HasAddonFromShop) {
        break;
      }
      this.removeOrder(order)
      break;
    }
  }

  orderDBClicked(event) {
    const order = event.data;
    this.openDetailView(this.orderDetails, order);
  }

  cellClickedInvoices(event) {
    const invoice = event.data;
    const column = event.column.colDef.field;
    switch (column) {
      case 'Innhold':
      // this.openShop(this.shopModal, order)
      this.invoiceContent(invoice)
      break;
      case 'Detaljer':
      // this.generateInvoiceEvent(order, this.invoiceNotify, 'SMS_EMAIL')
      this.viewInvoiceDetails(invoice)
      break;
    }
  }

  invoicesDBClicked(event) {
    const invoice = event.data;
    this.viewInvoiceDetails(invoice);
  }



  lodaContract(initialLoad: boolean) {
    this.memberService.getContractDetails(this._contractId, this._branchId).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.selectedContract = result.Data;

          this.isBookingActivity = this.selectedContract.IsBookingActivity;
          if (this.selectedContract.MemberId === this._selectedMember.Id) {
             this.selectedContract.IsMemberContract = true;
             this.selectedContract.IsGroup = false;
          }else {
            this.selectedContract.IsMemberContract = false;
            this.selectedContract.IsGroup = true;
          }

          if (this.selectedContract.ClosedDate != null && (moment(this.selectedContract.ContractEndDate).isAfter(moment(new Date()), 'day'))) {
            this.enableResignRemove = true;
          } else {
            this.enableResignRemove = false;
          }
          this.selectedContract.InstalllmentList = [];
          this.startUpitems = this.selectedContract.StartUpItemList;
          this.monthlyItems = this.selectedContract.EveryMonthItemList;

          // manipulate the group member dates
          this.selectedContract.GroupMembers.forEach(gropMember => {
            gropMember.StartDate = { date: { year: moment(gropMember.GmStartDate).year(), month: moment(gropMember.GmStartDate).month() +
              1, day: moment(gropMember.GmStartDate).date() } };
            gropMember.EndDate = gropMember.GmEndDate ? { date: Extension.GetFormatteredDate(gropMember.GmEndDate) } : null;
            gropMember.GmStartDate = moment(gropMember.GmStartDate).format('DD/MM/YYYY');
            gropMember.GmEndDate = gropMember.GmEndDate ? moment(gropMember.GmEndDate).format('DD/MM/YYYY') : null;
          });

          this.groupMembers = this.selectedContract.GroupMembers;
          this.groupMemberCount = this.groupMembers.length;
          this.contractForm.patchValue(this.selectedContract);
          this.contractForm.patchValue({ signedDate: { date: Extension.GetFormatteredDate(this.selectedContract.CreatedDate) } });
          this.contractForm.patchValue({ ContractStartDate: { date: Extension.GetFormatteredDate(this.selectedContract.ContractStartDate) } });
          this.contractForm.patchValue({ ContractEndDate: { date: Extension.GetFormatteredDate(this.selectedContract.ContractEndDate) } });

          this.contractForm.patchValue({ LockInPeriodUntilDate: { date: Extension.GetFormatteredDate(this.selectedContract.LockInPeriodUntilDate) } });
          this.contractForm.patchValue({ PriceGuarantyUntillDate: { date: Extension.GetFormatteredDate(this.selectedContract.PriceGuarantyUntillDate) } });
          this.contractForm.patchValue({ RenewedDate: { date: Extension.GetFormatteredDate(this.selectedContract.RenewedDate) } });
          this.contractForm.patchValue({ ClosedDate: { date: Extension.GetFormatteredDate(this.selectedContract.ClosedDate) } });

          this.contractForm.patchValue(
            {
              contractCondition: this.selectedContract.ContractCondition,
              NumberOfVisits: this.selectedContract.AvailableVisits,
              AvailableVisits: this.selectedContract.AvailableVisits,
              SoldPunches: this.selectedContract.NumberOfVisits
            }
          );

          if (initialLoad) {
            this.getInstallments();
            this.getContractInvoice(this._hit);
            this.loadBookings();
          } else {
            this.selectedContract.InstalllmentList = this._installmentList;
            this.memberInvoicesList = this._invoiceList;
            this.selectedContract.ContractBookingList = this._bookingList;
          }

          if ((moment(this.selectedContract.ContractEndDate).isBefore(new Date(), 'day') ||
          moment(new Date()).diff(moment(this.selectedContract.ContractEndDate), 'days') > 30) && this.selectedContract.Status !== 'Closed') {
            this.enableRenew = true;
          }

          if (this.selectedContract.ContractTypeName === 'PUNCHCARD') {
            this.isNoOfvisitsEnabled = this._gymSettings.EcoEditRemainingPunches;
            this.isSoldVisitsEnabled = this._gymSettings.EcoEditRemainingPunches;
          }
          if (this.selectedContract.ActivityName === 'Trening') {
            this.memberService.getMemberContractSummeries(this._selectedMember.Id, this._selectedMember.BranchId, this._isFreezeDetailsNeeded, this._isFreezeView).pipe(
              takeUntil(this.destroy$)
              ).subscribe(
            contractsRes => {
              if (contractsRes) {
                // this._contractSummeries = contracts.Data;
                // this.getRowColors = this.getRowColors.bind(this);
                // this.itemCount = Number(this._contractSummeries.length);
                // this.itemResource = new DataTableResource(result.Data);
                // this.itemResource.count().then(count => this.itemCount = count);
                const contracts = contractsRes.Data;
                if (contracts.length > 1) {
                  let dateDate = this.selectedContract.ContractEndDate.split('T')[0];
                  const thisEndDate = new Date(dateDate[0], dateDate[1], dateDate[2])
                  contracts.forEach(con => {
                    if (con.ActivityName === 'Trening' && this.selectedContract.Id !== con.Id) {
                      dateDate = con.EndDate.split('T')[0];
                      const enddate = new Date(dateDate[0], dateDate[1], dateDate[2]);
                      if (enddate > thisEndDate) {
                        this.hasNewerTraingContract = true;
                      }
                    }
                  });
                }
              }
            }
          );
          }
        }
      }
    );
  }


  dateValidator() {
    const startDate = moment();
    startDate.year(this.contractForm.value.ContractStartDate.date.year);
    startDate.month(this.contractForm.value.ContractStartDate.date.month);
    startDate.day(this.contractForm.value.ContractStartDate.date.day);
    const endDate = moment();
    endDate.year(this.contractForm.value.ContractEndDate.date.year);
    endDate.month(this.contractForm.value.ContractEndDate.date.month);
    endDate.day(this.contractForm.value.ContractEndDate.date.day);

    if ((this.contractForm.value.ContractStartDate && this.contractForm.value.ContractEndDate) &&
        (endDate.diff(startDate, 'days') < 0)) {
        this.contractForm.controls['ContractEndDate'].setErrors({
          'compare' : true
        });
        this.translateService.get('MEMBERSHIP.IllogicalDate').pipe(takeUntil(this.destroy$)).subscribe(translatedValue =>
          this.formErrors.ContractEndDate = translatedValue);
      }
  }


  compareStartEndDate() {
    return new Promise(resolve => {
      setTimeout(() => {
        if (
          this.contractForm.value.ContractStartDate && this.contractForm.value.ContractEndDate &&
          (this.contractForm.value.ContractStartDate >= this.contractForm.value.ContractEndDate)) {
          resolve({
            'compare': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    })
  }

  fetchData(branch) {
    this.adminService.getGymSettings('ECONOMY', null, branch).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this._isMemberFee = result.Data[0].IsMemberFee;
          this._isMemberFeeMonth = result.Data[0].IsMemberFeeMonth;
        }
      }
    )
  }

  getInstallments() {
    if (this.selectedContract.IsGroup) {
      this.isOrderDeleteEnable = false;
      this.isContractCardGymEnable = false;
    }
    this.memberService.getInstallments(this._memberBranchId, this._contractId).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.selectedContract.InstalllmentList = result.Data;
          this._installmentList = result.Data;
          let totalorderAmount = 0;

          this.selectedContract.InstalllmentList.forEach(ins => {
            totalorderAmount += ins.ServiceAmount;
          });

          this._serviceAmountDifference = this.selectedContract.EveryMonthItemPrice - totalorderAmount;

          if (this.selectedContract.BranchId !== this._branchId) {
            this.HandleOrderOperations(false, true, true);
            this.DefaultOperationStatus = false;
          }

          this.selectedContract.InstalllmentList.forEach(installment => {
            installment.BtnDeleteOrderEnable = this.BtnDeleteOrderEnable;
            installment.BtnDeleteOrderVisible = this.BtnDeleteOrderVisible;
            installment.IsTreat = true;
            if (!this.selectedContract.IsMemberContract) {
              installment.OrderOperationsEnabled = false;
              installment.EnableInvoice = false;
              installment.IsCheckOutEnable = false;

            }

            // apply the suboperation changes
            if (installment.IsCheckOutEnable) {
              installment.IsCheckOutEnable = this._isOrderCheckOutEnable ? installment.IsCheckOutEnable : false;
            }
            if (installment.OrderOperationsEnabled) {
              installment.OrderOperationsEnabled = this._isOrderSMSEmailEnable ? installment.OrderOperationsEnabled : false;
            }
            if (installment.EnableInvoice) {
              installment.EnableInvoice = this._isOrderInvoiceEnable ? installment.EnableInvoice : false
            }
            installment.IsOrderCheckOutvisible = this._isOrderCheckOutvisible ? true : false;
            installment.IsOrderSMSEmailvisible = this._isOrderSMSEmailvisible ? true : false;
            installment.IsOrderInvoicevisible = this._isOrderInvoicevisible ? true : false;
          });

          if (this.selectedContract.InstalllmentList.filter(X => X.InstallmentNo === 1)[0]) {
            this.firstOrderHas = true;
          } else {
            this.firstOrderHas = false;
          }
        }
      }
    );
  }

  loadBookings() {
    // get the contract bookings
    this.memberService.getContractBookings(this.selectedContract.Id).pipe(takeUntil(this.destroy$)).subscribe(
      bookingResult => {
        this._bookingList = bookingResult.Data;
        this.selectedContract.ContractBookingList = bookingResult.Data;
      }
    );
  }

  templateSelected(event: any) {
    const template = event;
     this.templateModel.close();
    if (this._templateType === 'NTEMP') {
      this.selectedContract.NextTemplateNo = template.TemplateNumber;
      this.selectedContract.NextTemplateId = template.PackageId;
      this.selectedContract.NextTemplateName = template.PackageName;

      this.contractForm.patchValue({
        NextTemplateNo: this.selectedContract.NextTemplateNo,
        NextTemplateName: this.selectedContract.NextTemplateName,
      }
      );

    } else {

      if (template.MaxAge <= this._selectedMember.Age && this._selectedMember.Age > 0 && template.MaxAge > 0) {
        this.messageService.openMessageBox('WARNING', {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.MsgAgeNotMatch'
        });
      } else {
        let remainingOrder = 0;
        let startInstallment: any;
        const bookingItem = this.selectedContract.EveryMonthItemList.filter(X => X.IsBookingArticle === true)[0];
        if (this.selectedContract.InstalllmentList.length > 0) {
          startInstallment = this.selectedContract.InstalllmentList.filter(Y => Y.InstallmentNo === 1);
          remainingOrder = this.selectedContract.NoOfInstallments - this.selectedContract.InstalllmentList.length;
        }

        if (this.selectedContract.ContractTypeCode !== template.PackageCategory.Code || this.selectedContract.ActivityId !== template.ActivityId) {
          this.messageService.openMessageBox('WARNING', {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.MsgSelectSameContractType'
          });
        } else {
          if (this.selectedContract.ContractId !== template.PackageId) {

            this.selectedContract.ContractStartDate = this.contractForm.get('ContractStartDate').value;
            this.selectedContract.ContractEndDate = this.contractForm.get('ContractEndDate').value
            this.selectedContract.MemberFeeMonth = template.MemberFeeMonth;
            this.selectedContract.TemplateName = template.PackageName;
            this.selectedContract.TemplateNo = template.TemplateNumber;
            this.selectedContract.NextTemplateNo = template.NextTemplateNo > 0 ? template.NextTemplateNo : '';
            this.selectedContract.NextTemplateId = template.NextTemplateId;
            this.selectedContract.NextTemplateName = template.NextContractTemplateName;
            this.selectedContract.ContractId = template.PackageId;
            this.selectedContract.ContractName = template.PackageName;
            this.selectedContract.ActivityId = template.ActivityId;
            this.selectedContract.ActivityName = template.ActivityName;
            this.selectedContract.ContractTypeId = template.ContractTypeValue.Id;
            this.selectedContract.ArticleId = template.ArticleNo;
            this.selectedContract.ArticleName = template.ArticleText;
            this.selectedContract.CreatedUser = this._user;
            this.selectedContract.MemberFeeMonth = template.ServicePrice;
            // this.selectedContract.ServiceAmount = template.Price;

            if (startInstallment != null) {
              this.selectedContract.StartUpItemPrice = template.StartUpItemPrice;
              this.selectedContract.StartUpItemList = template.StartUpItemList;
            }

            this.selectedContract.ContractTypeName = template.ContractTypeValue.Code;
            this.selectedContract.OrderPrice = template.OrderPrice;
            this.selectedContract.ContractDescription = template.ContractDescritpion;
            this.selectedContract.CategoryId = template.PackageCategory.Id;
            this.selectedContract.ContractCategoryName = template.PackageCategory.Name;
            this.selectedContract.ContractTypeCode = template.PackageCategory.Code;

            if (bookingItem) {
              bookingItem.NumberOfOrders = this.selectedContract.InstalllmentList.Count;
            }

            this.selectedContract.EveryMonthItemList.forEach(oldMonthlyItem => {
              this._deletedItems.push(oldMonthlyItem);
              this.selectedContract.EveryMonthItemList = [];
            });

            template.EveryMonthItemList.forEach(monthlyItem => {
              const mItem: any = deepcopy<any>(monthlyItem);
              if (mItem.ActivityID === this.selectedContract.ActivityId) {
                mItem.IsActivityArticle = true;
              }
              this.selectedContract.EveryMonthItemList.push(mItem);
              this._addedItems.push(mItem);
            });

            const candidateActivityItem: any = this.selectedContract.EveryMonthItemList.filter(X => X.IsActivityArticle === true && X.EndOrder < 0)[0];
            if (!candidateActivityItem) {
              const activityArticles: any = this.selectedContract.EveryMonthItemList.filter(X => X.IsActivityArticle === true);
              if (activityArticles.length > 0) {

                // const maxEndOrder: number = Math.max.apply(Math, activityArticles.forEach(actItem => { actItem.EndOrder }));

                // candidateActivityItem = activityArticles.filter(X => X.EndOrder === maxEndOrder)[0];
                // candidateActivityItem.EndOrder = -1;
              }
              this.ContractChanged = true
              this.HandleOrderOperations(false, false, false);
              this.contractForm.patchValue(this.selectedContract);

            }
          }
        }
      }
    }

  }

  deletenextTemplate() {
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'NEXT'
      });
  }

  removeNextTemplate() {
    this.selectedContract.NextTemplateNo = ''
    this.selectedContract.NextTemplateId = -1;
    this.selectedContract.NextTemplateName = '';

    this.contractForm.patchValue({
      NextTemplateNo: this.selectedContract.NextTemplateNo,
      NextTemplateName: this.selectedContract.NextTemplateName
    });
  }

  updateAmounts(value, controlName) {
    this.contractForm.get(controlName).patchValue(value);
  }

  openStartupItems(content: any) {
    this.startUpitems = deepcopy<any>(this.selectedContract.StartUpItemList);
    this.contractItemModel = this.modalService.open(content, { width: '762' });
  }

  openMonthlyItems(content: any) {
    this.monthlyItems = deepcopy<any>(this.selectedContract.EveryMonthItemList);
    this.contractItemModel = this.modalService.open(content, { width: '762' });
  }

  openContractTemplates(content: any, templateType: string) {
    this._templateOpened = true;
    this._templateType = templateType;
    this.templateModel = this.modalService.open(content);
  }

  openContractConditions(content: any) {
    this.memberService.GetGymCompanySettings('CONTRACT').pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this._contractConditions = result.Data;
          this.contractConditionModel = this.modalService.open(content, { width: '597' });
        }
      }
    );
  }

  openGroupMembers(content: any) {
    this.groupMembers = deepcopy<any>(this.selectedContract.GroupMembers);
    this.groupmembersModel = this.modalService.open(content);
  }

  changeContractGym(selectedGymId: any) {
    if (selectedGymId) {
      this.selectedContract.BranchId = selectedGymId;
    }
  }

  changeContractCategory(selectedCategoryId: any) {
    if (selectedCategoryId) {
      this.selectedContract.ContractTypeId = selectedCategoryId;
    }
  }

  changeSignUpCategory(selectedSignUpCatId: any) {
    if (selectedSignUpCatId) {
      this.selectedContract.SignUpCategoryId = selectedSignUpCatId;
    }
  }

  changeAccessProfile(selectedAccessProfileId: any) {
    if (selectedAccessProfileId) {
      this.selectedContract.AccessProfileId = selectedAccessProfileId;
    }
  }

  changeMemberFeeMonth(selectedMonth: any) {
    if (selectedMonth) {
      this.selectedContract.MemberFeeMonth = selectedMonth;
    }
  }

  // remove startup items
  removeStartUpItem(item: any) {
    this._deletedItem = item;
    const removeditemIndex = this.startUpitems.indexOf(this._deletedItem);
    if (removeditemIndex > -1) {

      this.messageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.ConfirmDelete',
          msgBoxId: 'ITEM'
        });
    }
  }

  openBookingView(content: any) {
    this.bookingModalRef = this.modalService.open(content);
  }

  deleteStartUpItem() {
    const deleteingIndex = this.startUpitems.indexOf(this._deletedItem);
    this.startUpitems.splice(deleteingIndex, 1);
    this.deletedItems.push(this._deletedItem);
  }

  removeMonthlyItem(deletingItem: any) {
    this._deletedItem = deletingItem;
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'MONTHLY'
      });
  }


  deleteMonthlyItem() {
    const deleteingIndex = this.monthlyItems.indexOf(this._deletedItem);
    this.monthlyItems.splice(deleteingIndex, 1);
  }

  contractItemsUpdated(itemType: string) {
    if (itemType === 'STARTUP') {
      this.selectedContract.StartUpItemList = this.startUpitems;
      let itemPrice = 0;
      if (this.firstOrderHas) {
        // do all the calculations
        const firstInstallment = this.selectedContract.InstalllmentList.filter(X => X.InstallmentNo === 1)[0];

        firstInstallment.AddOnList = [];
        let Priority = 1;
        firstInstallment.Amount = 0;
        firstInstallment.AdonPrice = 0;
        firstInstallment.ItemAmount = 0;
        firstInstallment.Balance = 0;
        this.selectedContract.StartUpItemList.forEach(sItem => {
          firstInstallment.AddOnList.push(sItem);
          itemPrice += sItem.Price;
          firstInstallment.Amount += sItem.Price;
          firstInstallment.AdonPrice += sItem.Price;
          firstInstallment.ItemAmount += sItem.Price;
          firstInstallment.Balance = firstInstallment.Amount;
          firstInstallment.EstimatedOrderAmount = firstInstallment.Amount - firstInstallment.Discount;
          Priority++;
        });

        this.selectedContract.StartUpItemPrice = itemPrice;
        this.contractForm.patchValue({
          StartUpItemPrice: this.selectedContract.StartUpItemPrice
        });
        this.UpdateContractPrice();
        this.ContractChanged = true;
        this.contractItemModel.close();
      }
    } else if (itemType === 'MONTHLY') {
      this.selectedContract.EveryMonthItemList = this.monthlyItems;
      if (this.validateServices(this.selectedContract.EveryMonthItemList)) {
        const errorItem = this.selectedContract.EveryMonthItemList.filter(X => X.StartOrder < 1)[0];
        if (errorItem) {
          this.messageService.openMessageBox('WARNING', {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.MsgStartOrderShouldbe'
          });
        } else {
          this.ContractChanged = true;
          this.contractItemModel.close();
        }
      } else {
        this.messageService.openMessageBox('WARNING', {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.MsgServicesNotifnPeroperOrder'
        });
      }
    }
  }

  validateServices(monthlyServices: any): boolean {
    let validationResult = true;
    monthlyServices.forEach(item => {
      if (item.ActivityID === this.selectedContract.ActivityId) {
        if (item.StartOrder > item.EndOrder && item.EndOrder > 0) {
          validationResult = false;
        }
      }

      monthlyServices.forEach(innerItem => {
        if (innerItem.ActivityID === this.selectedContract.ActivityId) {
          if (item !== innerItem) {
            if (item.StartOrder <= innerItem.StartOrder && item.EndOrder < 1) {
              validationResult = false;
            }

            if (item.EndOrder >= innerItem.StartOrder && innerItem.EndOrder >= item.EndOrder && item.EndOrder > 0 && innerItem.EndOrder > 0) {
              validationResult = false;
            }
          }
        }
      });
    });
    return validationResult;
  }

  UpdateContractPrice() {
    this.selectedContract.ContractPrize = this.selectedContract.StartUpItemPrice + this.selectedContract.EveryMonthItemPrice;
    this.contractForm.patchValue({
      ContractPrize: this.selectedContract.ContractPrize
    });
  }

  closeContractItemsView() {
    this.contractItemModel.close();
  }

  orderlineModelClose() {
    this.orderlineModel.close();
  }

  selectArticles(content: any, type: string) {
    this._orderlineType = type;

    switch (this._orderlineType) {
      case 'MEMBERFEE':
        this.selectionType = 'ITEM';
        break;
      case 'MONTHLY':
        this.selectionType = 'SERVICE';
        break;
      case 'STARTUP':
        this.selectionType = 'ITEM';
        break;
    }
    this.orderlineModel = this.modalService.open(content, { width: '800' });
  }


  selectItemByDoubleClick(article) {
    switch (this._orderlineType) {
      case 'MEMBERFEE':
        this.handleMemberFee(article);
        break;
      case 'MONTHLY':
      const monthlyArticles = [];
      const contractItem: any = {
          ArticleId: article.Id,
          ItemName: article.Description,
          Price: article.DefaultPrice,
          StartOrder: 0,
          EndOrder: 0,
          NumberOfOrders: 0,
          Quantity: article.Quantity,
          UnitPrice: article.DefaultPrice
        }
        monthlyArticles.push(contractItem);
        this.handleContractItems(monthlyArticles, 'MONTHLY');
        break;
      case 'STARTUP':
      const startUpArticles = [];
      const startUpItem: any = {
          ArticleId: article.Id,
          ItemName: article.Description,
          Price: article.DefaultPrice,
          Quantity: article.Quantity,
          UnitPrice: article.DefaultPrice
        }
        startUpArticles.push(startUpItem);
        this.handleContractItems(startUpArticles, 'STARTUP');
        break;
    }
    this.orderlineModel.close();
  }

  selectMultipleArticles(articles) {
    if (articles.length > 0) {
      switch (this._orderlineType) {
        case 'MEMBERFEE':
          this.handleMemberFee(articles[0]);
          break;
        case 'MONTHLY':
        const monthlyArticles = [];
          articles.forEach(article => {
            const contractItem: any = {
              ArticleId: article.Id,
              ItemName: article.Description,
              Price: article.DefaultPrice,
              StartOrder: 0,
              EndOrder: 0,
              NumberOfOrders: 0,
              Quantity: article.Quantity,
              UnitPrice: article.DefaultPrice
            }
            monthlyArticles.push(contractItem);
          });
          this.handleContractItems(monthlyArticles, 'MONTHLY');
          break;
        case 'STARTUP':
        const startUpArticles = [];
          articles.forEach(article => {
            const startUpItem: any = {
              ArticleId: article.Id,
              ItemName: article.Description,
              Price: article.DefaultPrice,
              Quantity: article.Quantity,
              UnitPrice: article.DefaultPrice
            }
            startUpArticles.push(startUpItem);
          });
          this.handleContractItems(startUpArticles, 'STARTUP');
          break;
      }
    }
    this.orderlineModel.close();
  }

  handleContractItems(contractitems: any, itemType: string) {
    if (itemType === 'MONTHLY') {
      contractitems.forEach(monthlyItem => {
        monthlyItem.StartOrder = 1;
        monthlyItem.EndOrder = this.selectedContract.NoOfInstallments;
        monthlyItem.NumberOfOrders = this.selectedContract.NoOfInstallments;
        this.monthlyItems.push(monthlyItem);
        // this.updateEconomy();
      });
    } else if (itemType === 'STARTUP') {
      contractitems.forEach(strtupItem => {
        this.startUpitems.push(strtupItem);
        /// this.updateEconomy();
      });
    }
  }

  handleMemberFee(memberFeeArticle: any) {
    if (memberFeeArticle) {
      const contractItem: any = {
        ArticleId: memberFeeArticle.Id,
        ItemName: memberFeeArticle.Description,
        Price: memberFeeArticle.Price,
        Quantity: 1
      }

      this.selectedContract.MemberFeeArticleID = contractItem.ArticleId;
      this.selectedContract.MemberFeeArticleName = contractItem.ItemName;

      // update the economy form
      this.contractForm.patchValue({
        MemberFeeArticleName: this.selectedContract.MemberFeeArticleName
      });
    }
  }

  deleteGroupMember(gMember: any) {
    this._removingMember = gMember;
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'GROUP'
      });
  }

  removeGroupMember() {
    const gMemberIndex = this.groupMembers.indexOf(this._removingMember);
    this.groupMembers.splice(gMemberIndex, 1);
  }

  groupMembersUpdated() {
    this.selectedContract.GroupMembers = this.groupMembers;
    this.groupMemberCount = this.selectedContract.GroupMembers.length;
    this.closeView('GROUP');
  }

  addGroupMembers(content: any) {
    this.addGroupModalRef = this.modalService.open(content);
  }

  // Search a member in member selection
  searchMember(val: any): void {
    PreloaderService.showPreLoader();
    this.memberService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
      'SEARCH', val.roleSelected.id, val.hit, false,
      false, '').pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        PreloaderService.hidePreLoader();
        if (result) {
          this.entityItems = result.Data;
          this.itemCount = result.Data.length;
        } else {
        }
      }, err => {
        PreloaderService.hidePreLoader();
      });
  }

  MemberSelected(selectedmembers: any) {
    let duplicateFound = false;
    let message = '';
    selectedmembers.forEach(member => {
      const existingmembers = this.groupMembers.filter(X => X.Id === member.Id)
      if (existingmembers.length > 0) {
        duplicateFound = true;
        if (message === '') {
          message = existingmembers[0].Name;
        } else {
          message += ', ' + existingmembers[0].Name;
        }
      } else {
        member.GmStartDate = moment(this.selectedContract.ContractStartDate).format('DD/MM/YYYY');
        member.StartDate = { date: { year: moment(this.selectedContract.ContractStartDate).year(), month: moment(this.selectedContract.ContractStartDate).month() +
          1, day: moment(this.selectedContract.ContractStartDate).date() } };
        this.groupMembers.push(member);
      }
    });
    if (duplicateFound) {
      let messageBody: string;
      this.translateService.get('MEMBERSHIP.MsgDuplicateGroupMember').pipe(
        takeUntil(this.destroy$)
      ).subscribe(translatedValue => messageBody = translatedValue)
      this.messageService.openMessageBox('WARNING', {
        messageTitle: 'Exceline',
        messageBody: message + ' ' + messageBody
      });
    }
    this.closeView('GROUPSELECTTION');
  }

  // get Add Member View
  addMemberView(content: any) {
    this.addNewMemberModalRef = this.modalService.open(content, { width: '800' });
    this.closeView('GROUPSELECTTION');
  }

  addNewMember(member: any) {
    member.GmStartDate = moment(this.selectedContract.ContractStartDate).format('DD/MM/YYYY');
    // this.selectedContract.GroupMembers.push(member);
    this.groupMembers.push(member);
    this.addNewMemberModalRef.close();
  }

  contractConditionSelected(condition) {
    if (condition) {
      this.selectedContract.ContractCondition = condition.Name;
      this.selectedContract.ContractConditionId = condition.Id;
      this.contractForm.patchValue({
        contractCondition: this.selectedContract.ContractCondition
      });
    }
    this.closeView('CONDITIONS');
  }

  closeMemberSelectionView() {
    this.closeView('GROUPSELECTTION');
  }

  groupDateChanged(event, item: any, dateType: string) {
    if (dateType === 'START') {
      item.StartDate = event;
    } else if (dateType === 'END') {
      item.EndDate = event;
    }
  }

  orderUpdateSuccess() {
    this.modalReference.close();
    this.getInstallments();
    this.updateServiceAmounts();
  }

  closeOderDetailModal() {
    this.modalReference.close();
    this.getInstallments();
    this.updateServiceAmounts();
  }

  printGroupMembers() {
    this.memberService.printGroupContratMembers(this.selectedContract.Id, '1').pipe(takeUntil(this.destroy$)).subscribe(result => {
      PreloaderService.hidePreLoader();
      if (result.Data.FileParth.trim() !== '') {
        this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
      }
    });
  }

  closeView(type: string) {
    switch (type) {
      case 'GROUP':
        this.groupmembersModel.close();
        break;
      case 'ORDER':
        this.groupmembersModel.close();
        break;
      case 'GROUPSELECTTION':
        this.addGroupModalRef.close();
        break;
      case 'CONDITIONS':
        this.contractConditionModel.close();
        break;
      case 'BOOKING':
        this.bookingModalRef.close();
        break;
      case 'TEMPLATE':
        this.templateModel.close();
        break;
    }
  }

  openFreeze() {
    this.titleService.setTitle('MEMBERSHIP.FreezeContract');
    this.selectedIndex = 2;

  }
  cancelFreeze() {
    this.lodaContract(true);
    this.selectedIndex = 1;
  }

  deleteMemeberFee() {
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'MEMBERFEE'
      });
  }

  removeMemberFee() {
    this.selectedContract.MemberFeeArticleID = -1;
    this.selectedContract.MemberFeeArticleName = '';
    // update the economy form
    this.contractForm.patchValue({
      MemberFeeArticleName: this.selectedContract.MemberFeeArticleName
    });
  }

  deleteContractCondition() {
    this.messageService.openMessageBox('CONFIRM', {
      messageTitle: 'Exceline',
      messageBody: 'MEMBERSHIP.ConfirmDelete',
      msgBoxId: 'CONTRACTCON'
    });
  }

  removeContractCondition() {
    this.selectedContract.ContractCondition = '';
    this.selectedContract.ContractConditionId = -1;
    this.contractForm.patchValue({
      contractCondition: this.selectedContract.ContractCondition
    });
  }

  HandleOrderOperations(enabled: boolean, isOtherGym: boolean, islock: boolean) {
    if (!this.IsBrisIntegrated) {
      this.selectedContract.InstalllmentList.forEach(inst => {
        inst.OrderOperationsEnabled = enabled;
        inst.IsLocked = islock;
        inst.IsCheckOutEnable = enabled;
        inst.EnableInvoice = enabled;
      });
    } else if (this.ContractChanged) {
      this.selectedContract.InstalllmentList.forEach(inst => {
        inst.OrderOperationsEnabled = false;
        inst.IsLocked = true;
        inst.IsCheckOutEnable = false;
        inst.EnableInvoice = false;
      });
    }
  }

  HandleButtons(status: boolean, installment: any) {
    if (installment != null) {
      if (this.DefaultOperationStatus) {
        installment.OrderOperationsEnabled = status;
        installment.IsCheckOutEnable = status;
      }
      installment.EnableInvoice = status;
      installment.IsTreat = status;


      installment.IsCheckOutEnable = this._isOrderCheckOutEnable ? installment.IsCheckOutEnable : false;
      installment.OrderOperationsEnabled = this._isOrderSMSEmailEnable ? installment.OrderOperationsEnabled : false;
      installment.EnableInvoice = this._isOrderInvoiceEnable ? installment.EnableInvoice : false;

      installment.IsOrderCheckOutvisible = this._isOrderCheckOutvisible ? true : false;
      installment.IsOrderSMSEmailvisible = this._isOrderSMSEmailvisible ? true : false;
      installment.IsOrderInvoicevisible = this._isOrderInvoicevisible ? true : false;
    }
  }

  navSelected(event) {
    if (event.srcElement.name === 'ord') {
      this.orderListSelected = true;
      this.getInstallments();
    } else if (event.srcElement.name === 'inv') {
      this.orderListSelected = false;
      this.getInstallments();
      this.getContractInvoice(this._hit)
    }
  }

  invoiceNav(event) {
    // this is to solve issue by haveing two ag-grids, wont sizeColsTofit() in ongridready
    this.gridApiInv.sizeColumnsToFit();
  }

  AddOrderEvent() {
    let newDueDate;
    if (!this.selectedContract.IsGroup) {
      const newInstallment: any = {};
      const lastInstallment = this.selectedContract.InstalllmentList[this.selectedContract.InstalllmentList.length - 1];
      if (lastInstallment) {
        newInstallment.MemberId = lastInstallment.MemberId;
        newInstallment.MemberContractId = lastInstallment.MemberContractId;
        newInstallment.InstallmentDate = this.manipulateDateObject(this.dateAdd(lastInstallment.InstallmentDate, 1, 'months'));

        if (this._selectedMember.DueDate > 0) {
          const nextMonth = this.dateAdd(lastInstallment.DueDate, 1, 'months');
          newInstallment.DueDate = this.manipulateDateObject(new Date(moment(lastInstallment.DueDate).year(), moment(lastInstallment.DueDate).month() + 1, this._selectedMember.DueDate));
        } else {
          newInstallment.DueDate = this.manipulateDateObject(this.dateAdd(lastInstallment.DueDate, 1, 'months'));
        }
        newInstallment.OriginalDueDate = newInstallment.DueDate;
        newInstallment.CreatedUser = this._user
        newInstallment.TransferDate = this.manipulateDateObject(this.dateAdd(newInstallment.OriginalDueDate, -6, 'days'));
        newInstallment.Amount = 0;
        newInstallment.BillingDate = this.manipulateDateObject(new Date());
        newInstallment.AdonPrice = 0;
        newInstallment.InstallmentDate = this.manipulateDateObject(new Date());
        newInstallment.InstallmentNo = lastInstallment.InstallmentNo + 1;
        newInstallment.InstallmentType = 'M';
        newInstallment.Balance = 0;
        newInstallment.TrainingPeriodStart = this.manipulateDateObject(this.dateAdd(lastInstallment.TrainingPeriodEnd, 1, 'days'));
        newInstallment.TrainingPeriodEnd = this.manipulateDateObject(this.dateAdd(this.dateAdd(newInstallment.TrainingPeriodStart, 1, 'months'), -1, 'days'));
        newInstallment.IsATG = this.selectedContract.IsATG;
        newInstallment.ActivityPrice = 0;
        newInstallment.IsInvoiceFeeAdded = false;
        if (lastInstallment.PayerId !== -1) {
          newInstallment.PayerId = lastInstallment.PayerId;
        } else {
          newInstallment.PayerId = lastInstallment.MemberId;
        }
        newInstallment.ServiceAmount = 0;
        newInstallment.ItemAmount = 0;
        newInstallment.MemberContractNo = lastInstallment.MemberContractNo;
        newInstallment.Text = moment(newInstallment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(newInstallment.TrainingPeriodEnd).format(this._dateFormat);
        newInstallment.TemplateId = lastInstallment.TemplateId;
        if (this.selectedContract.MemberCreditPeriod > 0) {
          newInstallment.EstimatedInvoiceDate = this.manipulateDateObject(this.dateAdd(newInstallment.DueDate, this.selectedContract.MemberCreditPeriod * -1, 'days'));
        } else {
          newInstallment.EstimatedInvoiceDate = this.manipulateDateObject(this.dateAdd(newInstallment.DueDate, this.selectedContract.CreditPeriod * -1, 'days'));
        }
      } else {
        const toDay: Date = new Date();
        newInstallment.MemberId = this.selectedContract.MemberId;
        newInstallment.MemberContractId = this.selectedContract.Id;
        newInstallment.InstallmentDate = this.manipulatedate(toDay);

        if (this._selectedMember.DueDate > 0) {
           newDueDate = new Date(toDay.getFullYear(), toDay.getMonth(), this._selectedMember.DueDate);
        } else {
           newDueDate = toDay;
        }
        if (moment(newDueDate).isBefore(moment(new Date()))) {
          newDueDate = this.dateAdd(newDueDate, 1, 'months');
        }
        newInstallment.DueDate = this.manipulateDateObject(newDueDate);
        newInstallment.CreatedUser = this._user;
        newInstallment.Amount = 0;
        newInstallment.BillingDate = this.manipulateDateObject(toDay);
        newInstallment.InstallmentDate = this.manipulateDateObject(toDay);
        newInstallment.AdonPrice = 0;
        newInstallment.InstallmentNo = this.selectedContract.NoOfInstallments + 1;
        newInstallment.InstallmentType = 'M';
        newInstallment.Balance = 0;
        newInstallment.TrainingPeriodStart = this.manipulateDateObject(toDay);
        newInstallment.TrainingPeriodEnd = this.manipulateDateObject(this.dateAdd(this.dateAdd(toDay, 1, 'months'), -1, 'days'));
        newInstallment.OriginalDueDate = newInstallment.DueDate;
        newInstallment.ActivityPrice = 0;
        newInstallment.IsInvoiceFeeAdded = false;
        if (this._selectedMember.GuardianId > 0) {
          newInstallment.PayerId = this._selectedMember.GuardianId;
        } else {
          newInstallment.PayerId = this.selectedContract.MemberId;
        }
        newInstallment.ServiceAmount = 0;
        newInstallment.ItemAmount = 0;
        newInstallment.MemberContractNo = this.selectedContract.MemberContractNo;
        newInstallment.Text = moment(newInstallment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(newInstallment.TrainingPeriodEnd).format(this._dateFormat);
        newInstallment.TemplateId = this.selectedContract.ContractId;
        if (this.selectedContract.MemberCreditPeriod > 0) {
          newInstallment.EstimatedInvoiceDate = this.dateAdd(newInstallment.DueDate, this.selectedContract.MemberCreditPeriod * -1, 'days');
        } else {
          newInstallment.EstimatedInvoiceDate = this.dateAdd(newInstallment.DueDate, this.selectedContract.CreditPeriod * -1, 'days');
        }
      }

      // add the installment
      this.memberService.addInstallment(newInstallment).pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          if (result) {
            if (result.Data === 1) {
              this.selectedContract.NoOfInstallments += 1;
              this.getInstallments();
            }
          }
        }
      )
    }
  }


  ordersSelected(event: any) {
    const orders = this.gridApi.getSelectedRows();
    if (orders.length > 0) {
      this.mergeOrdersDisable = false;
    } else {
      this.mergeOrdersDisable = true;
    }
  }

  mergeOrders() {
    const selectedOrders = this.gridApi.getSelectedRows();

    this._mergingOrder = deepcopy<any>(this.selectedContract.InstalllmentList);
    // const selectedOrders = deepcopy<any>(this._mergingOrder.filter(X => X.IsSelected === true));

    if (selectedOrders.length > 0) {

      let allselected: string;
      let intoOneOrder: string;
      let proceed: string;
      this.translateService.get('MEMBERSHIP.AllSelected').pipe(takeUntil(this.destroy$)).subscribe(value => allselected = value);
      this.translateService.get('MEMBERSHIP.IntoOneOrder').pipe(takeUntil(this.destroy$)).subscribe(value => intoOneOrder = value);
      this.translateService.get('MEMBERSHIP.Proceed').pipe(takeUntil(this.destroy$)).subscribe(value => proceed = value);
      this.messageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: allselected + ' ' + selectedOrders.length.toString() + ' ' + intoOneOrder + '' + selectedOrders[0].InstallmentNo + ' ' + proceed,
          msgBoxId: 'MERGE'
        });
      this._selectedOrders = selectedOrders;
    }
  }

  mergeSelectedOrders() {
    const deletedInstallments: any = [];
    const removedOrders: number[] = [];
    const sourceOrder = this._selectedOrders[0];
    const updatedOrders: any[] = [];
    let removeOrderList = '';
    let i: number;
    for (i = 1; i < this._selectedOrders.length; i++) {
      sourceOrder.Amount += this._selectedOrders[i].Amount;
      sourceOrder.Balance += this._selectedOrders[i].Balance;
      sourceOrder.ServiceAmount += this._selectedOrders[i].ServiceAmount;
      sourceOrder.ItemAmount += this._selectedOrders[i].ItemAmount;
      sourceOrder.ActivityPrice += this._selectedOrders[i].ActivityPrice;
      sourceOrder.AdonPrice += this._selectedOrders[i].AdonPrice;
      sourceOrder.EstimatedOrderAmount += this._selectedOrders[i].EstimatedOrderAmount;
      if (i === this._selectedOrders.length - 1) {
        sourceOrder.TrainingPeriodEnd = this._selectedOrders[i].TrainingPeriodEnd;
        sourceOrder.Text = moment(sourceOrder.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(sourceOrder.TrainingPeriodEnd).format(this._dateFormat);
      }

      this._selectedOrders[i].AddOnList.forEach(adon => {
        const samilarAdon: any = sourceOrder.AddOnList.filter(x => x.ArticleId === adon.ArticleId)[0];
        if (samilarAdon) {
          samilarAdon.Price += adon.Price;
        } else {
          sourceOrder.AddOnList.push(adon);
        }
      });
      deletedInstallments.push(this._selectedOrders[i]);
      sourceOrder.IsSelected = false;
    }

    deletedInstallments.forEach(deletedOrder => {
      const removingIndex = this._mergingOrder.indexOf(deletedOrder);
      this._mergingOrder.splice(removingIndex, 1);
      removedOrders.push(deletedOrder.Id);
      removeOrderList += deletedOrder.OrderNo + ', ';
    });

    updatedOrders.push(sourceOrder);
    const mergerDetails = {
      updatedOrders: updatedOrders,
      removedOrder: removedOrders,
      removedOrderNo: removeOrderList,
      branchID: this._branchId,
      memberContractId: this.selectedContract.Id
    }
    this.memberService.mergeOrders(mergerDetails).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          if (result.Data.length > 0) {
            this.selectedContract.InstalllmentList = result.Data;
            if (this.selectedContract.BranchId !== this._branchId) {
              this.HandleOrderOperations(false, true, true);
              this.DefaultOperationStatus = false;
            } else {
              this.DefaultOperationStatus = true;
            }
            this.selectedContract.InstalllmentList.forEach(ins => {
              ins.IsTreat = true;
            });
          }
        }
      },
      err => {
        if (err) {
          this.messageService.openMessageBox('WARNING', {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.MsgErrorInUpdating'
          });
        }
      }
    );
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  renewContract() {
    if (this.selectedContract.ContractTypeCode === 'CONTRACT') {
      this.contractService.setSelectedContract(this.selectedContract);
      this.titleService.setTitle('MEMBERSHIP.Renew')
      this.router.navigate(['/membership/card/' + this._selectedMember.Id + '/' + this._selectedMember.BranchId + '/' +
        MemberRole[this._selectedMember.Role] + '/contracts/' + this.selectedContract.Id + '/contract-renew']);
    }
  }

  moveOrders() {
    this.contractService.setSelectedContract(this.selectedContract);
    this.titleService.setTitle('MEMBERSHIP.MoveOrders');
    this.router.navigate(['/membership/card/' + this._selectedMember.Id + '/' + this._selectedMember.BranchId + '/' +
      MemberRole[this._selectedMember.Role] + '/contracts/' + this.selectedContract.Id + '/contract-move-orders']);
  }

  openDetailView(content, selectedOrder) {
    this.selectedOrder = selectedOrder;
    const dueDate = new Date(selectedOrder.DueDate);
    this.selectedOrder.DueDateFormated = { year: dueDate.getFullYear(), month: dueDate.getMonth() + 1, day: dueDate.getDate() };


    if (!selectedOrder.isOpened || selectedOrder.isFormatedDates) {
      const traningStart = new Date(selectedOrder.TrainingPeriodStart);
      this.selectedOrder.TrainingPeriodStart = { year: traningStart.getFullYear(), month: traningStart.getMonth() + 1, day: traningStart.getDate() };
      const traningEnd = new Date(selectedOrder.TrainingPeriodEnd);
      this.selectedOrder.TrainingPeriodEnd = { year: traningEnd.getFullYear(), month: traningEnd.getMonth() + 1, day: traningEnd.getDate() };
      const installmentDate = new Date(selectedOrder.InstallmentDate);
      this.selectedOrder.InstallmentDate = { year: installmentDate.getFullYear(), month: installmentDate.getMonth() + 1, day: installmentDate.getDate() };
      const originalDueDate = new Date(selectedOrder.OriginalDueDate);
      this.selectedOrder.OriginalDueDate = { year: originalDueDate.getFullYear(), month: originalDueDate.getMonth() + 1, day: originalDueDate.getDate() };
      const estimatedInvoiceDate = new Date(selectedOrder.EstimatedInvoiceDate);
      this.selectedOrder.EstimatedInvoiceDate = { year: estimatedInvoiceDate.getFullYear(), month: estimatedInvoiceDate.getMonth() + 1, day: estimatedInvoiceDate.getDate() };
    }

    selectedOrder.isOpened = true;
    this.modalReference = this.modalService.open(content);
  }

  removeOrder(item) {
    this._deletingOrder = item;
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'ORDER'
      });
  }

  deleteOrder() {
    const deletingOrders: number[] = [];
    deletingOrders.push(this._deletingOrder.Id);
    const deletingOrderDetails = {
      installmentIdList: deletingOrders,
      memberContractId: this.selectedContract.Id
    }

    this.memberService.deleteOrder(deletingOrderDetails).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          const deletingIndex = this.selectedContract.InstalllmentList.indexOf(this._deletingOrder);
          this.selectedContract.InstalllmentList.splice(deletingIndex, 1);
          this.gridApi.setRowData(this.selectedContract.InstalllmentList);
          const totalprice = Number(this.contractForm.get('ContractPrize').value);
          this.contractForm.patchValue({
            'ContractPrize': totalprice - this._deletingOrder.Amount
          })
        } else {
          this.messageService.openMessageBox('WARNING', {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.MsgOrderDeleteError'
          });
        }
      },
      er => {
        this.messageService.openMessageBox('WARNING', {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.MsgOrderDeleteError'
        });
      }
    )
  }

  generateInvoiceEvent(item, content, type: string) {
    this.selectedOrder = item;
    this._notifyType = type;
    this._popupContent = content;
    this.DefaultOperationStatus = false;
    this.HandleButtons(false, this.selectedOrder);
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.MsgInvoice',
        msgBoxId: 'INVOICE'
      }
    );
    this.getInstallments();
    this.getContractInvoice(this._hit)
  }

  generateInvoice(generateInvoice: boolean) {
    if (generateInvoice) {
      if (this.selectedOrder.IsInvoiceFeeAdded) {
        this.messageService.openMessageBox('CONFIRM',
          {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.MsgaddInvoiceFee',
            msgBoxId: 'ADDINVOICEFEE'
          }
        );
      } else {
        if (this._notifyType === 'PRINT') {
          this.generateInvoiceDocument(false);
        } else if (this._notifyType === 'SMS_EMAIL') {
          this.generateInvoiceEmailSMS(false);
        }
      }
    } else {
      this.DefaultOperationStatus = true;
      this.HandleButtons(true, this.selectedOrder);
    }
  }

  generateInvoiceDocument(invoiceFeeAdded: boolean) {
    if (!invoiceFeeAdded) {
      this.selectedOrder.IsInvoiceFeeAdded = false;
    }

    const invoiceDetails = this.getInvoiceGenerationDetails('PRINT');
    this.memberService.generateInvoice(invoiceDetails).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          if (result.Data.PrintResult.FileParth) {
            if (result.Data.PrintResult.FileParth === 'INV') {
              const content = this.Pdfwindow;
              PreloaderService.showPreLoader();
              this.openGenerateConfirmModal(content, result.Data.InvoiceNo);
            } else {
              this.excePdfViewerService.openPdfViewer('Exceline', result.Data.PrintResult.FileParth);
            }
            this.getInstallments();
            // set the memberfee invoice date
            if (this.selectedOrder.InstallmentType === 'MF') {
              this.selectedContract.MemFeeLastInvoicedDate = new Date();
              this.contractForm.patchValue({
                memberFeeLastInvoicedDate: {
                  date: {
                    year: new Date().getFullYear(),
                    month: new Date().getMonth(),
                    day: new Date().getDate()
                  }
                }
              });
            }
          } else {
            this.messageService.openMessageBox('WARNING',
              {
                messageTitle: 'Exceline',
                messageBody: 'MEMBERSHIP.MsgInvoicedGenFailed',
              });
            this.DefaultOperationStatus = true;
            this.HandleButtons(true, this.selectedOrder);
          }
        }
      });
  }

  generateInvoiceEmailSMS(invoiceFeeAdded: boolean) {
    if (!invoiceFeeAdded) {
      this.selectedOrder.IsInvoiceFeeAdded = false;
    }
    this.popupReference = this.modalService.open(this._popupContent);
  }

  getInvoiceGenerationDetails(invoiceType: string) {
    let generateInvoiceTitle: string;
    this.translateService.get('MEMBERSHIP.GenerateInvoiceTitle').pipe(takeUntil(this.destroy$)).subscribe(translatedvalue => generateInvoiceTitle = translatedvalue);
    const paymentDetails: any = {};
    paymentDetails.PaymentSource = PaymentTypes[PaymentTypes.INSTALLMENTS];
    const invoiceDetails = {
      invoiceBranchId: this._branchId,
      installment: this.selectedOrder,
      paymentDetail: paymentDetails,
      invoiceType: invoiceType,
      notificationTitle: generateInvoiceTitle,
      memberBranchID: this._selectedMember.BranchId
    }
    return invoiceDetails;
  }

  invoiceContent(item: any) {
    const content = this.Pdfwindow;
    PreloaderService.showPreLoader();
    this.openGenerateConfirmModal(content, item.InvoiceNo);
  }

  printfunc(invoiceno: any) {
    this.currentInvoice = invoiceno;
    this.economyService.GetInvoice(invoiceno, this.IsBrisIntegrated, this._selectedMember.BranchId).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
      if (result) {
      const contentType = 'application/pdf',
      b64Data = result.Data;
      this.blob = this.b64toBlob(b64Data, contentType);
      this.blobUrl = URL.createObjectURL(this.blob); }
      });
  }


  b64toBlob = (b64Data, contentType= '', sliceSize= 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize),
        byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

const blob = new Blob(byteArrays, {type: contentType});
return blob;
}

openContractConfirmModal() {
  const content = this.Pdfwindow;
  this.printContract()
  this.generatePdfView = this.modalService.open(content, {width: '800'});
}

openGenerateConfirmModal(content: any, invoicenum: any) {
  this.printfunc(invoicenum);
  this.generatePdfView = this.modalService.open(content, {width: '800'});
 }



PrintPDF() {
  const iframe = document.createElement('iframe');
  iframe.style.display = 'none';
  iframe.src = this.blobUrl;
  document.body.appendChild(iframe);
  iframe.contentWindow.print();
}

SavePDF() {
  if (this.currentInvoice === undefined) {
    saveAs(this.blob, this.selectedContract.MemberContractNo + '.pdf');
  }else {
    saveAs(this.blob, this.currentInvoice + '.pdf');
  }

}


viewInvoiceDetails(selectedInvoice) {
  if (selectedInvoice.InvoiceType !== 'CN') {

    this.selectedDetail.ArItemNo = selectedInvoice.Id;
    let isFullyPaid = false;
    let isSponsorInvoice = false;
    if (selectedInvoice.Id > 0) {
      isFullyPaid = selectedInvoice.IsFullyPaid;
      isSponsorInvoice = selectedInvoice.IsSponsorInvoice;
    }

    this.selectedDetail.IsFullyPaid = isFullyPaid;
    this.selectedDetail.IsSponsorInvoice = isSponsorInvoice;
    this.selectedDetail.MemberId = this._selectedMember.Id;
    this.selectedDetail.IsShopInvoice = selectedInvoice.IsShopInvoice;
    this.selectedDetail.PayDirection = selectedInvoice.PayDirection;
    this.selectedDetail.GuardianId = this._selectedMember.GuardianId;
    this.selectedDetail.Email = this._selectedMember.Id !== this._selectedMember.GuardianId && this._selectedMember.GuardianId > 0 ? this._selectedMember.GuardianEmail : this._selectedMember.Email;
    this.selectedDetail.SmsNo = this._selectedMember.Id !== this._selectedMember.GuardianId && this._selectedMember.GuardianId > 0 ?
     this._selectedMember.GuardianMobile : this._selectedMember.Mobile;
    this.selectedDetail.IsBrisIntegrated = this.IsBrisIntegrated;
    this.modalReference = this.modalService.open(this.invoiceDetails);
  } else {
    this.memberService.GetCreditNoteDetails(selectedInvoice.Id).pipe(takeUntil(this.destroy$)).subscribe(result => {
      this.selectedCreditNote = result.Data;
      this.selectedCreditNote.Amount = selectedInvoice.InvoiceAmount;
      this.selectedCreditNote.Balance = selectedInvoice.InvoiceBalance;
      this.selectedCreditNote.InvoiceNo = selectedInvoice.InvoiceNo;
      this.selectedCreditNote.Id = selectedInvoice.Id;
      this.selectedCreditNote.CustomerNo = this._selectedMember.CustId;
      this.selectedCreditNote.IsAddView = false;
      this.modalCreditNoteReference = this.modalService.open(this.creditNote);
    });
  }

}

  getContractInvoice(hit) {
    this.memberService.GetMemberInvoicesForContract(this._selectedMember.Id, this.selectedContract.Id, hit).pipe(takeUntil(this.destroy$)).subscribe(result => {
      result.Data.forEach(invoice => {
        invoice.DebtCollectionStatus = this.setDebtCollectionStatus(invoice.DebtCollectionStatus);
        this.setInvoiceStatus(invoice);
        this.rowColors = this.getRowColors.bind(this)
      });
      this._invoiceList = result.Data;
      this.memberInvoicesList = result.Data;
    });
  }

  setDebtCollectionStatus(debtColStatus) {
    let status;
    switch (debtColStatus) {
      case 'DW':
        this.translateService.get('MEMBERSHIP.INVDW').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => status = tranlstedValue)
        return status;
      case 'PPD':
        this.translateService.get('MEMBERSHIP.INVPartlyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => status = tranlstedValue)
        return status;
      case 'CPD':
        this.translateService.get('MEMBERSHIP.INVFullyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => status = tranlstedValue)
        return status;
      case 'NPD':
        this.translateService.get('MEMBERSHIP.INVNotPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => status = tranlstedValue)
        return status;
      default:
        this.translateService.get('MEMBERSHIP.INVNotApplicable').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => status = tranlstedValue)
        return status;
    }
  }

  setInvoiceStatus(invoice) {
    let invoiceStatus;
    if (invoice.InvoiceType === 'CN') {
      invoice.InvoiceNo = invoice.InvoiceNo + '-' + invoice.MembeInvoiceId;
      if (invoice.InvoiceBalance === invoice.InvoiceAmount * -1) {
        this.translateService.get('MEMBERSHIP.INVNotPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
        invoice.InvoiceStatus = invoiceStatus;
        invoice.IsNotPaid = true;
      } else if (invoice.InvoiceBalance === 0) {
        this.translateService.get('MEMBERSHIP.INVFullyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
        invoice.InvoiceStatus = invoiceStatus;
        invoice.IsFullyPaid = true;
      } else if (invoice.InvoiceBalance > 0) {
        this.translateService.get('MEMBERSHIP.INVPartlyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
        invoice.InvoiceStatus = invoiceStatus;
        invoice.IsPartiallyPaid = true;
      }
    } else {
      if (invoice.InvoiceBalance === invoice.InvoiceAmount && invoice.InvoiceAmount !== 0) {
        if (invoice.RemindCollStatus === 'DW') {
          this.translateService.get('MEMBERSHIP.INVStatusDW').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          invoice.InvoiceStatus = invoiceStatus;
        } else if (invoice.RemindCollStatus === 'REM') {
          this.translateService.get('MEMBERSHIP.INVStatusReminder').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          invoice.InvoiceStatus = invoiceStatus;
        } else if (invoice.RemindCollStatus === 'SREM') {
          this.translateService.get('MEMBERSHIP.INVDSmsReminder').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          invoice.InvoiceStatus = invoiceStatus;
        } else {
          this.translateService.get('MEMBERSHIP.INVNotPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          invoice.InvoiceStatus = invoiceStatus;
        }
        invoice.IsNotPaid = true;
      } else if (invoice.InvoiceBalance <= 0) {
        if (invoice.PaymentTypes !== '') {
          const payments = invoice.PaymentTypes.split(','); // invoice.PaymentTypes.Trim(',').Split(new char[] { ',' });
          if (payments.Length > 1) {
            this.translateService.get('MEMBERSHIP.INVStatusDW').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
            invoice.InvoiceStatus = invoiceStatus + '++';
          } else {
            this.translateService.get('MEMBERSHIP.INVFullyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
            invoice.InvoiceStatus = invoiceStatus + ' [' + this.localizePayment(payments[0].trim()) + ']';
          }
        } else {
          this.translateService.get('MEMBERSHIP.INVFullyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          invoice.InvoiceStatus = invoiceStatus;
        }
        invoice.IsFullyPaid = true;
      } else if (invoice.InvoiceBalance < invoice.InvoiceAmount && invoice.InvoiceBalance > 0) {
        this.translateService.get('MEMBERSHIP.INVPartlyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
        invoice.InvoiceStatus = invoiceStatus;
        invoice.IsPartiallyPaid = true;
      }
    }
  }

  localizePayment(type) {
    let typeMsg;
    switch (type) {
      case 'OP':
        this.translateService.get('MEMBERSHIP.OCR').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'DC':
        this.translateService.get('MEMBERSHIP.PtDC').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'TMN':
        this.translateService.get('MEMBERSHIP.PtTMN').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'CSH':
        this.translateService.get('MEMBERSHIP.PtCSH').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'BS':
        this.translateService.get('MEMBERSHIP.PtBS').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'OA':
        this.translateService.get('MEMBERSHIP.PtOA').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'GC':
        this.translateService.get('MEMBERSHIP.PtGC').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'LOP':
        this.translateService.get('MEMBERSHIP.OCR').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'NOR':
        this.translateService.get('MEMBERSHIP.NOR').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'RD':
        this.translateService.get('MEMBERSHIP.RD').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'BNK':
        this.translateService.get('MEMBERSHIP.PtBank').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'PB':
        this.translateService.get('MEMBERSHIP.PrepaidBalance').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      default:
        return '';
    }
  }

  getRowColors(invoice) {
    if (invoice.InvoiceType === 'CN') {
      return 'rgb(255, 255, 197)';
    }
  }

  closeInvoiceDetailModel() {
    this.modalReference.close();
    this.getContractInvoice(this._hit)
  }

  reloadDataGrid(params) {
    this.itemResource = new DataTableResource(params);
    this.itemCount = Number(params.length);
    this.itemResource.query(params).then(items => this.items = items);
  }

  reloadItems(params) {
    if (this.itemResource) {

      if (params.sortBy) { // sort by header

        const sortKey = this.getSortName(params.sortBy);
        this.memberService.updateUserRoutine({ Key: 'SrtMemInvoice', Value: sortKey }).pipe(takeUntil(this.destroy$)).subscribe(
          result => {
            if (result) {
              this._hit = 1;
              this.dataTable._offset = 0;
              this.getContractInvoice(this._hit)
            } else {
            }
          }
          )

      } else {

        this.handelPagination(params);
        this.getContractInvoice(this._hit)
      }
    }
  }

  getSortName(currentSortItem: string) {
    let name = '';
    if (currentSortItem === 'InvoiceNo') {
      name = 'InvoiceNo';
    } else if (currentSortItem === 'MembeInvoiceId') {
      name = 'Id';
    } else if (currentSortItem === 'InvoiceDate') {
      name = 'InvoiceDate';
    } else if (currentSortItem === 'InvoiceDueDate') {
      name = 'InvoiceDueDate';
    } else if (currentSortItem === 'InvoiceAmount') {
      name = 'Total';
    } else if (currentSortItem === 'InvoiceBalance') {
      name = 'Balance';
    } else if (currentSortItem === 'ContractId') {
      name = 'ContractNo';
    }
    return name;
  }

  handelPagination(params) {
    if ((this._hit * this.limit) === params.offset) {
      this._hit = this._hit + 1;
    } else {
      if (this._hit > 0) {
        this._hit = this._hit - 1;
      }
    }
  }

  closeCreditNoteModal() {
    this.modalCreditNoteReference.close();
    this.getContractInvoice(this._hit)
  }

  closeShopAfterSave() {
    this.shopModalReference.close();
    this.getInstallments();
  }

  openShop(content, selectedOrder) {
    this.DefaultOperationStatus = false;
    this.HandleButtons(false, selectedOrder);
    const orderArticles = [];
    this.selectedOrder = selectedOrder;
    this.shopHomeService.$priceType = 'MEMBER';
    this.shopHomeService.$isCancelBtnHide = true;
    this._selectedMember.BranchID = this._selectedMember.BranchId;
    this.shopHomeService.$selectedMember = this._selectedMember;
    this.shopHomeService.$isUpdateInstallments = true;
    selectedOrder.AddOnList.forEach(addon => {
      if (this.selectedContract.ContractTypeCode === 'PUNCHCARD' && addon.CategoryCode === 'ActivityArticle') {
        let addonText: string;
        this.translateService.get('MEMBERSHIP.Punches').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => addonText = translatedValue);
        addon.ItemName = addon.ItemName + ' - ' + this.selectedContract.NumberOfVisits + ' ' + addonText;
      }
      orderArticles.push({
        ArticleNo: addon.ArticleId,
        Id: addon.ArticleId,
        Description: addon.ItemName,
        UnitPrice: addon.UnitPrice,
        DiscountAmountPercentage: addon.Discount,
        OrderQuantity: addon.Quantity,
        DefaultPrice: addon.Price,
        Price: addon.Price,
        isReturn: false,
        EmployeePrice: addon.EmployeePrice,
        SelectedPrice: addon.UnitPrice,
        ReturnComment: '',
        Discount: 0
      })
    });
    this.shopHomeService.ShopItems = orderArticles;
    this.shopHomeService.$selectedOder = selectedOrder;
    this.shopModalReference = this.modalService.open(content);
  }

  closeShop() {
    this.shopModalReference.close();
    this.shopHomeService.clearShopItems();
    this.shopHomeService.$selectedMember = null;
    this.getInstallments();
  }

  removeResign() {
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.MsgResignRemoveConfirm',
        msgBoxId: 'RESIGN'
      });
  }

  removeResignation() {
    const resignRemoveDetails = {
      memberContractId: this.selectedContract.Id
    }
    this.memberService.removeResignation(resignRemoveDetails).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          if (result.Data) {
            this.lodaContract(true);
          }
        }
      }
    );
  }

  manipulatedate(dateInput: any) {
    if (dateInput !== null) {
      if (dateInput.date !== undefined) {
        const tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
        return moment(tempDate).format('YYYY-MM-DD').toString();
      } else if (dateInput !== undefined) {
        if (dateInput.date === undefined) {
          return
        }
        const date = dateInput.split('T')
        return date[0].toString();
      }
    }
  }

  closeEmailSMSView() {
    this.popupReference.close();
  }

  onNotificationSuccess(result: any) {
    this.popupReference.close();
    if (result) {
      const Notification = {
        Title: 'Generering av faktura',
        Description: 'Ordre ble manuelt generert til faktura',
        CreatedUser: this.loginservice.CurrentUser.username,
        Role: MemberRole[this._selectedMember.Role],
        TypeId: 4,
        SeverityId: 3,
        StatusId: 1,
        BranchID: this._branchId,
        MemberID: this._selectedMember.Id,
        RecordType: 'ORD'
      }
      // logging event
      this.adminService.AddEventToNotifications(Notification).pipe(takeUntil(this.destroy$)).subscribe( res => {
        // do nothing for now
        if (res) {
        }
      })
      this.getInstallments();
    } else {
      this.getInstallments();
    }
  }

  updateOrders() {
    // if (this._addedItems.length > 0) {
    const newInstallmentList = [];
    if (this.selectedContract.EveryMonthItemList.length > 0) {
      this.selectedContract.InstalllmentList.forEach(installment => {
        if (!installment.IsSponsored) {
          if (moment(installment.DueDate).isAfter(moment(), 'day')) {
            let Priority = 1;
            installment.SponsoredAmount = 0;
            installment.Discount = 0;
            installment.Amount = 0;
            installment.ActivityPrice = 0;
            installment.ServiceAmount = 0;
            installment.AdonPrice = 0;
            installment.AddOnList = [];
            this.selectedContract.EveryMonthItemList.forEach(Mitem => {
              const item = deepcopy<any>(Mitem);
              if (item.EndOrder > 0) {
                if (installment.InstallmentNo >= item.StartOrder && installment.InstallmentNo <= item.EndOrder) {
                  installment.AddOnList.push(item);
                  installment.Amount += item.Price;
                  if (!item.IsActivityArticle) {
                    installment.AdonPrice += item.Price;
                  } else {
                    installment.ServiceAmount += item.Price;
                    installment.ActivityPrice += item.Price;
                  }
                  Priority++;
                }
              } else if (installment.InstallmentNo >= item.StartOrder) { // no end order found
                installment.AddOnList.push(item);
                installment.Amount += item.Price;
                if (!item.IsActivityArticle) {
                  installment.AdonPrice += item.Price;
                } else {
                  installment.ServiceAmount += item.Price;
                  installment.ActivityPrice += item.Price;
                }
                Priority++;
              }
            });
            installment.Balance = installment.Amount;
            installment.EstimatedOrderAmount = installment.Amount - installment.Discount;
            installment.PriceListItem = this.selectedContract.ContractName;
          }
        }
        newInstallmentList.push(installment)
      });
      this.gridApi.setRowData(newInstallmentList);
    }
    this.ContractChanged = false;
    this.updateServiceAmounts();
  }

  updateOrderPeriod(item: any, type: string, value: any) {
    if (type === 'START') {
      item.StartOrder = Number(value);
    } else if (type === 'END') {
      item.EndOrder = Number(value);
    }
  }

  removeContractItem(item: any, type: string) {
    this._removingItemtype = type;
    this._removingItem = item;

    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'CONITEM'
      });

  }

  deleteContractItem() {
    if (this._removingItemtype === 'STARTUP') {
      const startIndex = this.startUpitems.indexOf(this._removingItem);
      this.startUpitems.splice(startIndex, 1);
    } else if (this._removingItemtype === 'MONTHLY') {
      const startIndex = this.monthlyItems.indexOf(this._removingItem);
      this.monthlyItems.splice(startIndex, 1);
    }
  }

  updateServiceAmounts() {
    let updatedServicePrice = 0;
    this.selectedContract.InstalllmentList.forEach(inst => {
      updatedServicePrice = inst.Amount + updatedServicePrice;
    });
    this.selectedContract.EveryMonthItemPrice = this._serviceAmountDifference + updatedServicePrice;
    this.selectedContract.ContractPrize = this.selectedContract.EveryMonthItemPrice;
    this.contractForm.patchValue({
      ContractPrize: this.selectedContract.ContractPrize,
      ServiceAmount: this.selectedContract.MemberFeeMonth,
    })
  }

  saveMemberContract() {
    this.dateValidator();
    this.isContractFormSubmitted = true;
    if (this.contractForm.valid) {
      if (this.ContractChanged) {
        this.messageService.openMessageBox('WARNING', {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.MsgUpdateOrders'
        });
      } else {
        this.selectedContract.ContractStartDate = this.manipulatedate(this.contractForm.value.ContractStartDate);
        this.selectedContract.ContractEndDate = this.manipulatedate(this.contractForm.value.ContractEndDate);
        this.selectedContract.IsATG = this.contractForm.value.IsATG;
        this.selectedContract.AutoRenew = this.contractForm.value.AutoRenew;
        this.selectedContract.IsInvoiceDetails = this.contractForm.value.IsInvoiceDetails;
        this.selectedContract.LockInPeriod = this.contractForm.value.LockInPeriod;
        this.selectedContract.LockInPeriodUntilDate = this.manipulatedate(this.contractForm.value.LockInPeriodUntilDate);
        this.selectedContract.GuarantyPeriod = this.contractForm.value.GuarantyPeriod;
        this.selectedContract.PriceGuarantyUntillDate = this.manipulatedate(this.contractForm.value.PriceGuarantyUntillDate);
        this.selectedContract.GroupMembers = this.groupMembers;
        this.selectedContract.GroupMembers.forEach(gropMember => {
          gropMember.GmStartDate = this.manipulatedate(gropMember.StartDate)
          gropMember.GmEndDate = gropMember.EndDate ? this.manipulatedate(gropMember.EndDate) : null;
        });
        // Update orders with ATG status
        this.selectedContract.InstalllmentList.forEach(installment => {
          installment.IsATG = this.selectedContract.IsATG;
        });
        this.memberService.updateMemerContract(this.selectedContract).pipe(takeUntil(this.destroy$)).subscribe(result => {
          if (result) {
            if (this._branchId !== this.selectedContract.BranchId) {
              this.DefaultOperationStatus = false;
              this.HandleOrderOperations(false, true, true);
            } else {
              this.DefaultOperationStatus = true;
              this.HandleOrderOperations(true, false, false);
            }
            this.translateService.get('MEMBERSHIP.SaveSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans =>
              this.message = trans);
            this.type = 'SUCCESS';
            this.isShowInfo = true;
            setTimeout(() => {
              this.isShowInfo = false;
            }, 4000);
            // switch (result.Data) {
              /* case -1:
                this.messageService.openMessageBox('WARNING', {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.MsgorderUpdateError'
                });
                this.resetContractBranch();
                break; */
/*                 case -2:
                this.messageService.openMessageBox('WARNING', {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.msgGymAccountNumbersNotEqual'
                });
                this.resetContractBranch();
                break;
              case -3:
                this.messageService.openMessageBox('WARNING', {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.MsgContractBookingsarethere'
                });
                this.resetContractBranch();
                break;
              case -4:
                this.messageService.openMessageBox('WARNING', {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.MsgNomatchingBasicActivity'
                });
                this.resetContractBranch();
                break;
              case -5:
                this.messageService.openMessageBox('WARNING', {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.ReturnShopAccountBalance'
                });
                this.resetContractBranch();
                break;
              case -6:
                this.messageService.openMessageBox('WARNING', {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.ReturnPrepaisAccountBalace'
                });
                this.resetContractBranch();
                break;
              case -7:
                this.messageService.openMessageBox('WARNING', {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.MsgPaytheShopBalance'
                });
                this.resetContractBranch();
                break;
              case -8:
                this.messageService.openMessageBox('WARNING', {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.MsgDuplicateBasicContracts'
                });
                this.resetContractBranch();
                break;
              case 2:
                this.messageService.openMessageBox('WARNING', {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.MsgMemberGymChangedwithContract'
                });
                this.resetContractBranch();

                if (this._branchId !== this.selectedContract.BranchId) {
                  this.DefaultOperationStatus = false;
                  this.HandleOrderOperations(false, true, true);
                } else {
                  this.DefaultOperationStatus = true;
                  this.HandleOrderOperations(true, false, false);
                }
                break; */
              /* default:
                if (this._branchId !== this.selectedContract.BranchId) {
                  this.DefaultOperationStatus = false;
                  this.HandleOrderOperations(false, true, true);
                } else {
                  this.DefaultOperationStatus = true;
                  this.HandleOrderOperations(true, false, false);
                }
                this.translateService.get('MEMBERSHIP.SaveSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans =>
                  this.message = trans);
                this.type = 'SUCCESS';
                this.isShowInfo = true;
                setTimeout(() => {
                  this.isShowInfo = false;
                }, 4000);
                break; */
            // }
          }
        }, errorFromBE => {
          this.resetContractBranch();
          this.translateService.get('MEMBERSHIP.SaveNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans =>
            this.message = trans);
          this.type = 'DANGER';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
          }
        )
      }
    }
  }

  resetContractBranch() {
    this.selectedContract.BranchId = this._originalBranchID;
    if (this._branchId !== this.selectedContract.BranchId) {
      this.DefaultOperationStatus = false;
      this.HandleOrderOperations(false, true, true);
    } else {
      this.DefaultOperationStatus = true;
      this.HandleOrderOperations(true, false, false);
    }
  }

  manipulateDateObject(date: Date) {
    return moment(date).format('YYYY-MM-DD').toString();
  }

  handleSubOperations() {
    this._userInfo.SubOperationList.forEach(subOperation => {
      if (subOperation.ExtendedInfo.OperationFieldList.length > 0) {
        const subOperationName = subOperation.DisplayName.replace(/\s/g, '');
        switch (subOperationName) {
          case 'OrderListCheckOut':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this._isOrderCheckOutvisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this._isOrderCheckOutEnable = false;
              this._isOrderCheckOutvisible = true;
            }
            break;

          case 'OrderListSMS/EMAILInvoice':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this._isOrderSMSEmailvisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this._isOrderSMSEmailEnable = false;
              this._isOrderSMSEmailvisible = true;
            }
            break;

          case 'OrderListInvoice':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this._isOrderInvoicevisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this._isOrderInvoiceEnable = false;
              this._isOrderInvoicevisible = true;
            }
            break;

          case 'SoldPunches':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE && this.selectedContract.ContractTypeName === 'PUNCHCARD') {
              this.isSoldVisitsVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE && this.selectedContract.ContractTypeName === 'PUNCHCARD') {
              this.isSoldVisitsEnabled = false;
              this.isSoldVisitsVisible = true;
            }
            break;

          case 'Remainingpunches':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE && this.selectedContract.ContractTypeName === 'PUNCHCARD') {
              this.isNoOfvisitsVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE && this.selectedContract.ContractTypeName === 'PUNCHCARD') {
              this.isNoOfvisitsEnabled = false;
              this.isNoOfvisitsVisible = true;
            }
            break;

          case 'Contracttemplate':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this.isContractTemplateVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this.isContractTemplateEnable = false;
              this.isContractTemplateVisible = true;
            }
            break;

          case 'Nexttemplate':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this.isNextTemplateVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this.isNextTemplateEnable = false;
              this.isNextTemplateVisible = true;
            }
            break;

          case 'Itemprice':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this.isItemPriceVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this.isItemPriceEnable = false;
              this.isItemPriceVisible = true;
            }
            break;

          case 'MonthlyItemPrice':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this.isMonthlyItemPriceVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this.isMonthlyItemPriceEnable = false;
              this.isMonthlyItemPriceVisible = true;
            }
            break;

          case 'ContractCardGym':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this.isContractCardGymVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this.isContractCardGymEnable = false;
              this.isContractCardGymVisible = true;
            }
            break;

          case 'Validperiod':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this.isValidPeriodVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this.isValidPeriodEnable = false;
              this.isValidPeriodVisible = true;
            }
            break;

          case 'Lockuntil':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this.isLockUntilVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this.isLockUntilEnable = false;
              this.isLockUntilVisible = true;
            }
            break;

          case 'Priceguaranty':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this.isPriceGuarantyVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this.isPriceGuarantyEnable = false;
              this.isPriceGuarantyVisible = true;
            }
            break;

          case 'Autorenew':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this.isAutoRenewVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this.isAutoRenewEnable = false;
              this.isAutoRenewVisible = true;
            }
            break;

          case 'Orderdelete':
            if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.HIDE) {
              this.isOrderDeleteVisible = false;
            } else if (subOperation.ExtendedInfo.OperationFieldList[0].FieldStatus === USPUIFieldStatus.DISABLE) {
              this.isOrderDeleteEnable = false;
              this.isOrderDeleteVisible = true;
            }
            break;

        }
      }
    });
  }

  backToContracts() {
    const base = '/membership/card/' + this._selectedMember.Id + '/' + this._selectedMember.BranchId + '/' + MemberRole[this._selectedMember.Role] + '/contracts/';
    this.router.navigate([base]);
  };

  resignContract() {
    if (this.selectedContract != null && (this.selectedContract.Closedate == null)) {
      this.router.navigate(['membership/card/' + this._selectedMember.Id + '/' + this._branchId + '/ ' +
       MemberRole[this._selectedMember.Role] + '/contracts/' + this.selectedContract.Id + '/resign']);
    }
  }
/*
  printContract() {
    this.memberService.PrintContractDocument(this.selectedContract.Id).pipe(takeUntil(this.destroy$)).subscribe(result => {
      PreloaderService.hidePreLoader();
      if (result.Data.FileParth.trim() !== '') {
        this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
      }
    });
  }
*/


printContract() {
  this.memberService.getContractPrint(this.selectedContract.Id).pipe(takeUntil(this.destroy$)).subscribe(
    result => {
      if (result) {
      const contentType = 'application/pdf',
      b64Data = result.Data;
      this.blob = this.b64toBlob(b64Data, contentType);
      this.blobUrl = URL.createObjectURL(this.blob); }
    });
}

  printCATG() {
    this.memberService.ViewMemberATGContract(this._selectedMember.Id).pipe(takeUntil(this.destroy$)).subscribe(result => {
      PreloaderService.hidePreLoader();
      if (result.Data.FileParth.trim() !== '') {
        this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
      }
    },
      err => {
        PreloaderService.hidePreLoader();
        this.messageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.MsgATGDocFailed' });
      });
  };

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();

    if (this.bookingModalRef) {
      this.bookingModalRef.close();
    }

    if (this.saveEvent) {
      this.saveEvent.unsubscribe();
    }

    if (this.modalReference) {
      this.modalReference.close();
    }

    if (this.popupReference) {
      this.popupReference.close();
    }

    if (this.orderlineModel) {
      this.orderlineModel.close();
    }

    // // Memory leak fix test
    // console.log(this._componentName + ' component is destroyed.');
    // this.subscriptions.printTotalSubscriptions();
    // this.subscriptions.unsubAllActiveFromCurrentComponent(this._componentName);
    // this.subscriptions.printTotalActiveSubscriptions();
  }
  ///////////////////////////////////
}
