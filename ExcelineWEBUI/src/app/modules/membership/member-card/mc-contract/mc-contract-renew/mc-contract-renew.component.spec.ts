import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McContractRenewComponent } from './mc-contract-renew.component';

describe('McContractRenewComponent', () => {
  let component: McContractRenewComponent;
  let fixture: ComponentFixture<McContractRenewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McContractRenewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McContractRenewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
