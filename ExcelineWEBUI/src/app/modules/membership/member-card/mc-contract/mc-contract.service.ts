import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class McContractService {
  private _selectedContract: any;
  
  @Output() contractSelectedEvent: EventEmitter<any> = new EventEmitter();

  constructor() { }

  setSelectedContract(selectedContract: any) {
    this._selectedContract = selectedContract;
    this.contractSelectedEvent.emit(selectedContract);
  }

  getSelectedContract() {
    return this._selectedContract;
  }
}
