import { ExceToolbarService } from '../../../../common/exce-toolbar/exce-toolbar.service';
import { Component, OnInit, Pipe, PipeTransform, AfterViewInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { McBasicInfoService } from '../../mc-basic-info/mc-basic-info.service';
import { ExceMemberService } from '../../../services/exce-member.service';
import { Router } from '@angular/router';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MemberRole } from 'app/shared/enums/us-enum';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ExcelineMember } from 'app/modules/membership/models/ExcelineMember';

@Component({
  selector: 'app-mc-contracts',
  templateUrl: './mc-contracts.component.html',
  styleUrls: ['./mc-contracts.component.scss']
})

export class McContractsComponent implements OnInit, AfterViewInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private _branchID: number;
  private _isFreezeDetailsNeeded = false;
  private _isFreezeView = false;
  private itemResource?: any;
  private emailForm: FormGroup;

  public _contractSummeries = [];
  memberID: number;
  locale: string;
  itemCount = 0;
  selectedMember: ExcelineMember;
  branchId: number;

  columnDefs: any;
  gridColumnApi: any;
  gridApi: any;
  lang: any;

    // default norwegian
    names = {
      ContractNo: 'Kontraktsnummer',
      IsATG : 'Avtalegiro',
      Template: 'Fakturanummer',
      Activity: 'Aktivitet',
      Registered: 'Registrert',
      EndDate: 'Sluttdato',
      RenewedDate: 'Fornyelsesdato',
      Orders: 'Antall ordrer',
      IsGroup: 'Gruppekontrakt',
      StartPrice: 'Varepris',
      MonthPrice: 'Månedspris',
      TotalPrice: 'Totalpris',
      Branch: 'Senter',
    }

    colNamesNo = {
      ContractNo: 'Kontraktsnummer',
      IsATG : 'Avtalegiro',
      Template: 'Fakturanummer',
      Activity: 'Aktivitet',
      Registered: 'Registrert',
      EndDate: 'Sluttdato',
      RenewedDate: 'Fornyelsesdato',
      Orders: 'Antall ordrer',
      IsGroup: 'Gruppekontrakt',
      StartPrice: 'Varepris',
      MonthPrice: 'Månedspris',
      TotalPrice: 'Totalpris',
      Branch: 'Senter',
    }

    colNamesEn = {
      ContractNo: 'ContractNo',
      IsATG : 'ATG',
      Template: 'Template',
      Activity: 'Activity',
      Registered: 'Signed date',
      EndDate: 'End date',
      RenewedDate: 'Renewed date',
      Orders: 'No of orders',
      IsGroup: 'Groupcontract',
      StartPrice: 'Items price',
      MonthPrice: 'Monthly price',
      TotalPrice: 'Totalpris',
      Branch: 'Gym',
    }


  // tslint:disable-next-line:max-line-length
  constructor(
    private basicInfoService: McBasicInfoService,
    private memberService: ExceMemberService,
    private toolbarService: ExceToolbarService,
    private router: Router,
    private loginService: ExceLoginService,
    private fb: FormBuilder,
    private messageService: ExceMessageService,

  ) {
    this.emailForm = fb.group({
      // 'email': [null, Validators.required],
    })
  }

  ngOnInit() {
    this._branchID = this.loginService.SelectedBranch.BranchId;
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

  this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
    (lang) => {
      if (lang) {
        this.locale = lang.culture;
      }
    });

         // setting header names for ag grid by language
    this.lang = this.toolbarService.getSelectedLanguage()
    switch (this.lang.id) {
      case 'no':
        this.names = this.colNamesNo;
        break;
      case 'en':
        this.names = this.colNamesEn;
        break;
    }

    this.columnDefs = [
      {headerName: this.names.ContractNo, field: 'ContractNo', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.IsATG, field: 'IsATG', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        if (params.value) {
          return 'Ja';
        } else {
          return 'Nei';
        }
      }},
      {headerName: this.names.Template, field: 'TemplateName', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true},
    {headerName: this.names.Activity, field: 'ActivityName', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true},
    {headerName: this.names.Registered, field: 'SignedDate', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, cellRenderer: function(params) {
      if (params.value) {
        const d = params.value.split('T');
        const dDate = d[0].split('-');
        return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
      } else {
        return
      }
    }},
    {headerName: this.names.EndDate, field: 'EndDate', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, cellRenderer: function(params) {
      if (params.value) {
        const d = params.value.split('T');
        const dDate = d[0].split('-');
        return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
      } else {
        return
      }
    }},
    {headerName: this.names.RenewedDate, field: 'RenewedDate', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, cellRenderer: function(params) {
      if (params.value) {
        const d = params.value.split('T');
        const dDate = d[0].split('-');
        return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
      } else {
        return
      }
    }},
    {headerName: this.names.Orders, field: 'NoOfOrders', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true},
    {headerName: this.names.IsGroup, field: 'IsGroupContract', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, cellRenderer: function(params) {
      if (params.value) {
        return 'Ja';
      } else {
        return 'Nei';
      }
    }},
    {headerName: this.names.StartPrice, field: 'StartUpItemsPrice', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, cellRenderer: function(params) {
      if (params.value) {
        const val = Number(params.value)
        return val.toFixed(2);
      } else {
        return '0.00';
      }
    }},
    {headerName: this.names.MonthPrice, field: 'ServiceAmount', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, cellRenderer: function(params) {
      if (params.value) {
        const val = Number(params.value)
        return val.toFixed(2);
      } else {
        return '0.00';
      }
    }},
    {headerName: this.names.TotalPrice, field: 'TotalPrice', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, cellRenderer: function(params) {
      if (params.value) {
        const val = Number(params.value)
        return val.toFixed(2);
      } else {
        return '0.00';
      }
    }},
    {headerName: this.names.Branch, field: 'GymName', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true},
  ]

  this.messageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      this.addContract();
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    params.api.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  ngAfterViewInit(): void {
    this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
    ).subscribe((currentMember): ExcelineMember => this.selectedMember = currentMember)
    this.memberService.getMemberContractSummeries(this.selectedMember.Id, this.selectedMember.BranchId,
      this._isFreezeDetailsNeeded, this._isFreezeView).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this._contractSummeries = result.Data;
          this.getRowColors = this.getRowColors.bind(this);
          this.itemCount = Number(this._contractSummeries.length);
          this.itemResource = new DataTableResource(result.Data);
          this.itemResource.count().then(count => this.itemCount = count);
        }
      }
    );
  }

  reloadItems(params) {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this._contractSummeries = items);
    }
  }

  openNewMemberContract() {
    if (this.selectedMember.BranchId !== this._branchID) {
      this.messageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.ContractHomeGymChange'
        });
    } else {
      this.addContract();
    }
  }

  addContract() {
    if (this.selectedMember.BirthDate && this.selectedMember.Role === 1 && this.selectedMember.BirthDate !== new Date(1900, 1, 1)) {
      if (this.selectedMember.Age >= 18 || this.selectedMember.GuardianId !== -1) {
        this.router.navigate(['/membership/card/' + this.selectedMember.Id + '/' + this.selectedMember.BranchId + '/' + MemberRole[this.selectedMember.Role] + '/contracts/register-contract'])
      } else {
        this.messageService.openMessageBox('WARNING',
          {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.MsgCannotSignUp'
          });
      }
    } else if (this.selectedMember.BirthDate === null) {
      this.messageService.openMessageBox('',
          {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.MsgBirthdayNull'
          });
    } else {
      this.router.navigate(['/membership/card/' + this.selectedMember.Id + '/' + this.selectedMember.BranchId + '/' + MemberRole[this.selectedMember.Role] + '/contracts/register-contract'])
    }
  }
  selectContract(event: any) {
    const selectedRow = event.data;
    const base = '/membership/card/' + this.selectedMember.Id + '/' + this.selectedMember.BranchId + '/' + MemberRole[this.selectedMember.Role] + '/contracts/';
    this.router.navigate([base + '/' + selectedRow.Id]);
  }

  getRowColors(memberContracts) {
    if (moment(memberContracts.EndDate).isBefore(moment(), 'day')) {
      return 'rgb(255, 255, 204)';
    }
  }

  onStep1Next() {
  }

  onStep2Next() {
  }

  onStep3Next() {
  }

  onStep4Next() {
  }

  onComplete() {
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


}


