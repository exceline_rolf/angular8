import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McInfoWithNotesComponent } from './mc-info-with-notes.component';

describe('McInfoWithNotesComponent', () => {
  let component: McInfoWithNotesComponent;
  let fixture: ComponentFixture<McInfoWithNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McInfoWithNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McInfoWithNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
