import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ExceMemberService } from 'app/modules/membership/services/exce-member.service';
import { McBasicInfoService } from 'app/modules/membership/member-card/mc-basic-info/mc-basic-info.service';
import { ExcelineMember } from 'app/modules/membership/models/ExcelineMember';
import { ExceSessionService } from 'app/shared/services/exce-session.service';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service'
import { Observable, Subject, Subscription, of, timer } from 'rxjs';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import { TranslateService } from '@ngx-translate/core';
import { EntitySelectionType, ColumnDataType, MemberSearchType, MemberRole } from '../../../../shared/enums/us-enum';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ConfigService } from '@ngx-config/core';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceToolbarService } from '../../../common/exce-toolbar/exce-toolbar.service';
import { Router } from '@angular/router';
import { ExcePdfViewerService } from '../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { AdminService } from '../../../admin/services/admin.service';
import { start } from 'repl';
import {takeUntil, concatMap, tap, distinctUntilChanged, filter, switchMap } from 'rxjs/operators';
// import { ExceMemberService } from '../../services/exce-member.service';

@Component({
  selector: 'mc-info-with-notes',
  templateUrl: './mc-info-with-notes.component.html',
  styleUrls: ['./mc-info-with-notes.component.scss']
})
export class McInfoWithNotesComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private newMemberRegisterViews: any;
  private selectedMember: any;
  private isChangeGroupMember = false;
  private isIntroducedByChanged = false;
  private groupMemberLstCount: number;
  private familyMemberLstCount = 0;
  private isChangeFamilyMember = false;
  private branchId: number;
  private entitySelectionViewTitle: string;
  private defaultRole = 'ALL'
  private multipleRowSelect = false;
  private memberRegisterView: any;
  private itemCount: number;
  private isDbPagination = true;
  private entitySelectionType: string;
  private entitySearchType: string;
  private columns: any;
  private addMemberView: any;
  private sub: Subscription;

  public memberInforNote: any = {};
  public memberInfoForm: FormGroup;
  public atgBranchList = [];
  public groupMemberList = [];
  public familyMemberList = [];
  _user: any;
  atgStatusList = [];
  isBranchVisible = true;
  isStatusVisible = true;
  isRoleVisible = true;
  isAddBtnVisible = true;
  isBasicInforVisible = true;
  items = [];
  timer: Observable<number>;
  FamilyMemberName: string;
  GroupName: string;
  public regDate = '';
  public introducedDate = '';
  brisIntegration = false;
  userGyms: any[];
  public selectedGym: any;
  newMemberRegisterView: any;
  auotocompleteItems = [{ id: 'NAME', name: 'NAMEE :', isNumber: false }, { id: 'ID', name: 'Idd :', isNumber: true }]
  contractPeriode: any;
  introducedByMemberId: any;
  isIntroducedByName = false;
  isUsingIntroducedBy: boolean;
  isUsingBrisIntegration: boolean;
  private _componentName = 'mc-info-with-notes-component';

  // VIEWS
  familyRegView: any;

  // MEMBERLIST COLUMNS
  familyRegColumns = ['CustId', 'LastName', 'FirstName', 'Age', 'Mobile'];
  // Handler for subscription (introducedBy)
  introducedBySubscriber: Subscription;
  isIntroducedByVisible: boolean;

  // Variables in this block are intentionally left ambiguous, as they are used for various backend confirmations.
  type: string;
  isShowInfo: boolean;
  message: String;
  // -----

  FamilyMember: any = {
    BranchId : -1,
    MemberId : -1,
    CustId : -1,
    RoleId : -1,
    Name : ''
  }
/*   FamilyMemberBranchId: number;
  FamilyMemberId: number;
  FamilyMemberRoleId: number */

  currentMemberSubscriber: Subscription;
  IsUsingGantner: boolean;

  constructor(
    private basicInfoService: McBasicInfoService,
    private router: Router,
    private loginservice: ExceLoginService,
    private fb: FormBuilder,
    private exceMemberService: ExceMemberService,
    private translateService: TranslateService,
    private modalService: UsbModal,
    private config: ConfigService,
    private exceMessageService: ExceMessageService,
    private exceToolbarService: ExceToolbarService,
    private excePdfViewerService: ExcePdfViewerService,
    private exceSessionService: ExceSessionService,
    private adminService: AdminService,
    private exceBreadCrumbService: ExceBreadcrumbService
  ) {
    this._user = this.loginservice.CurrentUser.username;
    this.memberInfoForm = this.fb.group({
      'InstructorName': [{ value: null, disabled: true }],
      'InstructorId': [],
      'CreatedUser': [{ value: null, disabled: true }],
      'IntroducedByName': [{ value: null, disabled: true }],
      'IntroducedDate': [{ value: null, disabled: true }],
      'MemberShipPeriod': [{ value: null, disabled: true }],
      'ChargeLimitAtg': [null],
      'AtgKid': [{ value: null, disabled: true }],
      'OldKid': [],
      'branchSelected': [this.selectedGym],
      'LastVisitDate': [{ value: null, disabled: true }],
      'NoOfVisiteLast30': [{ value: null, disabled: true }],
      'NoOfVisiteLast365': [{ value: null, disabled: true }],
      'GuestCardNo': [],
      'IsFollowUp': [],
      'IsSendAdvertisement': [],
      'LastFollowUpDate': [{ value: null, disabled: true }],
      'NoOfFollowUpLast30': [{ value: null, disabled: true }],
      'NoOfFollowUpLast365': [{ value: null, disabled: true }],
      'NextFollowUpDate': [{ value: null, disabled: true }],
      'AdcSignedDate': [],
      'RegisterDate': [{ value: null, disabled: true }],
      'PinCode': [{ value: null }],
      'GATCardNo': [],
      'CheckFingerPrint': [],
      'CtNumber': [{ value: null, disabled: true }],
      'CtName': [{ value: null, disabled: true }],
      'CtStartDate': [{ value: null, disabled: true }],
      'CtEndDate': [{ value: null, disabled: true }],
      'FreezeFromDate': [{ value: null, disabled: true }],
      'FreezeToDate': [{ value: null, disabled: true }],
      'Note': [],
      'School': [{ value: null, disabled: true }],
      'LastPaidSemesterFee': [{ value: null, disabled: true }],
      'groupMembersCount': [{ value: null, disabled: true }],
      'familyMembersCount': [{ value: null, disabled: true }]
    });
  }

  ngOnInit() {
    this.userGyms = this.exceToolbarService.getUserBranches();
    this.branchId = this.loginservice.SelectedBranch.BranchId;
    this.basicInfoService.currentMember.pipe(
      switchMap(currentMember => {
        if (currentMember) {
          this.selectedMember = currentMember;
          if (this.selectedMember.IntroducedByName) {
            this.isIntroducedByName = true;
            this.brisIntegration = this.selectedMember.IsBrisIntegrated;
          }
          return this.exceMemberService.GetMemberInfoWithNotes(this.branchId, this.selectedMember.Id).pipe(
            takeUntil(this.destroy$)
            )
        } else {
          return of('null').pipe(
            takeUntil(this.destroy$)
          )
        }
      }),
        takeUntil(this.destroy$)
      ).subscribe(result => {
      this.patchMemberInforWithNote(result.Data);
      this.selectedGym = this.userGyms.find(x => x.BranchId === result.Data.BranchId);
      this.IsUsingGantner = result.Data.IsUsingGantner;
      this.atgStatusList = result.Data.AtgStatusList;
    }, null , null)
/*    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          if (member.IntroducedByName) {
            this.isIntroducedByName = true;
          }
          this.brisIntegration = member.IsBrisIntegrated;
          this.selectedMember = member;
          this.exceMemberService.GetMemberInfoWithNotes(this.branchId, member.Id).pipe(takeUntil(this.destroy$)).subscribe(result => {
            this.patchMemberInforWithNote(result.Data);
            this.selectedGym = this.userGyms.find(x => x.BranchId === result.Data.BranchId);
            this.IsUsingGantner = result.Data.IsUsingGantner;
            this.atgStatusList = result.Data.AtgStatusList;
          };
        }
      },
      error => {
        console.log(error);
      }); */


    this.adminService.GetGymCompanySettings('other').pipe(takeUntil(this.destroy$)).subscribe(result => {
      this.isUsingBrisIntegration = result.Data.IsBrisIntegrated;
    }, null, null);
    this.adminService.getGymSettings('OTHER', null, this.branchId).pipe(takeUntil(this.destroy$)).subscribe( result => {
      if (result) {
        this.isUsingIntroducedBy = result.Data[0].IsUsingIntroducedBy;
      }
    }, null, null)

    this.introducedBySubscriber = this.memberInfoForm.get('IntroducedByName').valueChanges.pipe(
      distinctUntilChanged(), // no duplicate values
      takeUntil(this.destroy$)
      ).subscribe(introducer =>  {
      !introducer ? this.isIntroducedByVisible = false : this.isIntroducedByVisible = true;
    }, null, null)

  }

  familyMembersSelected(event) {
    const newFams = event;
    newFams.forEach(element => {
      let add = true;
      element.Name = element.FirstName + ' ' + element.LastName;
      this.familyMemberList.forEach(mem => {
        if (mem.CustId === element.CustId || mem.CustId === this.selectedMember.CustId) {
          add = false;
        }
      });
      if (add) {
        this.familyMemberList.push(element)
      }
    });
    this.memberInfoForm.patchValue({
      familyMembersCount: this.familyMemberList.length,
    });
    this.familyMemberLstCount = this.familyMemberList.length;
    this.isChangeFamilyMember = true;
    this.familyRegView.close();
  }

  familyMemberSelected(event) {
    const list = [];
    const x = this.familyMemberList;
    list.push(event.data)
    this.familyMembersSelected(list)
  }

  familySelectionView(content) {
    const familyMemberId = this.memberInforNote.FamilyMemberId > 0 ? this.memberInforNote.FamilyMemberId : this.memberInforNote.Id;
    this.exceMemberService.GetFamilyMembers(familyMemberId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      this.familyMemberList = result.Data;
      this.familyMemberList = this.familyMemberList.filter(x => x.Id !== this.memberInforNote.Id);
      this.memberInfoForm.patchValue({
        familyMembersCount: this.familyMemberList.length,
      });
      this.familyMemberLstCount = this.familyMemberList.length;
    }, null, null)
    this.familyRegView = this.modalService.open( content , { width: '1200' } )
  }

  ngOnDestroy() {
    if (this.memberRegisterView) {
      this.memberRegisterView.close();
    }
    if (this.newMemberRegisterViews) {
      this.newMemberRegisterViews.close();
    }
    if (this.addMemberView) {
      this.addMemberView.close();
    }
    if (this.familyRegView) {
      this.familyRegView.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  getDisplayDate(val) {
    if (val === '0001-01-01T00:00:00') {
      val = null
    }
    val = new Date(val);
    let result;
    if ((val.getFullYear() === 1970 || val.getFullYear() === 1900) && val.getMonth() === 0 &&
      val.getDate() === 1) {
      result = null;
    } else {
      result = {
        date: {
          year: val.getFullYear(),
          month: val.getMonth() + 1,
          day: val.getDate()
        }
      };
    }
    return result;
  }

  patchMemberInforWithNote(memberInfor: any) {
    this.memberInforNote = memberInfor;
    memberInfor.LastVisitDate =  this.getDisplayDate(memberInfor.LastVisitDate);
    memberInfor.LastFollowUpDate = this.getDisplayDate(memberInfor.LastFollowUpDate);
    memberInfor.NextFollowUpDate = this.getDisplayDate(memberInfor.NextFollowUpDate);
    memberInfor.CtStartDate = this.getDisplayDate(memberInfor.CtStartDate);
    memberInfor.CtEndDate = this.getDisplayDate(memberInfor.CtEndDate);
    memberInfor.FreezeFromDate = this.getDisplayDate(memberInfor.FreezeFromDate);
    memberInfor.FreezeToDate = this.getDisplayDate(memberInfor.FreezeToDate);
    memberInfor.AdcSignedDate = this.getDisplayDate(memberInfor.AdcSignedDate);

    memberInfor.GroupMemberLstCount = memberInfor.GroupMemberLstCount === 0 ? null : memberInfor.GroupMemberLstCount;
    memberInfor.FamilyMmeberLstCount = memberInfor.FamilyMmeberLstCount === 0 ? null : memberInfor.FamilyMmeberLstCount;
    memberInfor.PinCode = memberInfor.PinCode === 0 ? null : memberInfor.PinCode;
    memberInfor.ChargeLimitAtg = memberInfor.ChargeLimitAtg === 0 ? null : memberInfor.ChargeLimitAtg;
    this.memberInfoForm.patchValue({
      InstructorName: memberInfor.Instructor,
      InstructorId: memberInfor.InstructorId,
      CreatedUser: memberInfor.CreatedUser,
      IntroducedByName: memberInfor.IntroducedByName,
      IntroducedDate: memberInfor.IntroduceDate,
      MemberShipPeriod: memberInfor.MemberShipPeriod,
      ChargeLimitAtg: memberInfor.ChargeLimitAtg,
      AtgKid: memberInfor.AtgKid,
      OldKid: memberInfor.OldKid,
      LastVisitDate: memberInfor.LastVisitDate,
      NoOfVisiteLast30: memberInfor.NoOfVisiteLast30,
      NoOfVisiteLast365: memberInfor.NoOfVisiteLast365,
      GuestCardNo: memberInfor.GuestCardNo,
      IsFollowUp: memberInfor.IsFollowUp,
      IsSendAdvertisement: memberInfor.IsSendAdvertisement,
      LastFollowUpDate: memberInfor.LastFollowUpDate,
      NoOfFollowUpLast30: memberInfor.NoOfFollowUpLast30,
      NoOfFollowUpLast365: memberInfor.NoOfFollowUpLast365,
      NextFollowUpDate: memberInfor.NextFollowUpDate,
      AdcSignedDate: memberInfor.AdcSignedDate,
      RegisterDate: memberInfor.RegisterDate,
      PinCode: memberInfor.PinCode,
      GATCardNo: memberInfor.GATCardNo,
      CheckFingerPrint: memberInfor.CheckFingerPrint,
      CtNumber: memberInfor.CtNumber,
      CtName: memberInfor.CtName,
      CtStartDate: memberInfor.CtStartDate,
      CtEndDate: memberInfor.CtEndDate,
      FreezeFromDate: memberInfor.FreezeFromDate,
      FreezeToDate: memberInfor.FreezeToDate,
      Note: memberInfor.Note,
      School: memberInfor.School,
      LastPaidSemesterFee: memberInfor.LastPaidSemesterFee,
      groupMembersCount: memberInfor.GroupMemberLstCount,
      familyMembersCount: memberInfor.FamilyMmeberLstCount
    });

    // this.FamilyMemberName = memberInfor.FamilyMemberName;
    this.FamilyMember.BranchId = memberInfor.FamilyMemberBranchId;
    this.FamilyMember.RoleId = memberInfor.FamilyMemberRoleId;
    this.FamilyMember.Name = memberInfor.FamilyMemberName;
    this.FamilyMember.CustId = memberInfor.FamilyMemberCustId;
    this.FamilyMember.MemberId = memberInfor.FamilyMemberId;
    // console.log(this.FamilyMember)
/*
    this.FamilyMemberBranchId = memberInfor.FamilyMemberBranchId;
    this.FamilyMemberId = memberInfor.FamilyMemberId,
    this.FamilyMemberRoleId = memberInfor.FamilyMemberRoleId */
    // this.FamilyMemberName = memberInfor.FamilyMemberName;



    this.GroupName = memberInfor.GroupName;
    this.regDate = memberInfor.RegisterDate;
    this.introducedDate = memberInfor.IntroduceDate;
    this.groupMemberLstCount = memberInfor.GroupMemberLstCount;
    this.familyMemberLstCount = memberInfor.FamilyMmeberLstCount;
    this.monthsOnContract(memberInfor);
    this.introducedByMemberId = memberInfor.IntroducedById;
  }

  monthsOnContract(form) {
    this.contractPeriode = 0;
    if (form.CtStartDate) {
      const startDate = new Date(form.CtStartDate.date.year, form.CtStartDate.date.month - 1, form.CtStartDate.date.day);
      const endDate = new Date(form.CtEndDate.date.year, form.CtEndDate.date.month - 1, form.CtEndDate.date.day)
      const toDay = new Date();
      if (startDate) {
          const diff = {
            years: toDay.getFullYear() - startDate.getFullYear(),
            months: (toDay.getMonth() + 1) - (startDate.getMonth() + 1),
            days: toDay.getDate() - startDate.getDate()
          }
          this.contractPeriode = diff.years * 12 + diff.months
      } else {
        this.contractPeriode = '';
      }
    }
  }
  groupMemberLink($event, content) {
    // Block quote commented out: Does not seem to do anythin.
/*     $event.preventDefault()
    $event.stopPropagation()
    const url = 'http://localhost:3000/#/membership/card/' + + this.memberInforNote.GroupId + '/' + '1' + '/' + MemberRole[this.memberInforNote.Role];
    window.open(url, '_blank'); */
    // this.router.navigate(url)
    //  this.memberRegisterView = this.modalService.open(content, { width: '800' });
    // this.router.navigate([url]);
  }
  printAtg(val) {
    PreloaderService.showPreLoader();
    this.exceMemberService.ViewMemberATGContract(this.selectedMember.Id).pipe(takeUntil(this.destroy$)).subscribe(result => {
      PreloaderService.hidePreLoader();
      if (result.Data.FileParth.trim() !== '') {
        this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
      }
    },
      err => {
        PreloaderService.hidePreLoader();
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.MsgATGDocFailed' });
      });
  }
  saveBasicInfo(value) {
    // save introduced by
    // console.log(this.memberInfoForm.get('IntroducedByName'))
    // if (this.memberInfoForm.get('IntroducedByName') && this.isIntroducedByChanged === true) {
    //   console.log(this.selectedMember.Id, this.introducedByMemberId)
    //   this.exceMemberService.UpDateIntroducedBy(this.selectedMember.Id, this.introducedByMemberId).pipe(takeUntil(this.destroy$)).subscribe(
    //     res => {
    //       console.log(res)
    //     }
    //   )

    // }

    // rest of save
    const inforDetails = Object.assign({}, value);
    inforDetails.BasicContractId = this.memberInforNote.BasicContractId;
    if (value.CtStartDate !== undefined && value.CtStartDate !== null) {
      const ctsDate = new Date(value.CtStartDate.date.year + '-' + value.CtStartDate.date.month + '-' + value.CtStartDate.date.day);
      inforDetails.CtStartDate = ctsDate;
    }
    if (value.CtEndDate !== null && value.CtEndDate !== undefined) {
      const cteDate = new Date(value.CtEndDate.date.year + '-' + value.CtEndDate.date.month + '-' + value.CtEndDate.date.day);
      inforDetails.CtEndDate = cteDate;
    }
    if (value.AdcSignedDate !== null) {
      const atgDate = new Date(value.AdcSignedDate.date.year + '-' + value.AdcSignedDate.date.month + '-' + value.AdcSignedDate.date.day);
      inforDetails.AdcSignedDate = atgDate;
    }
    if (this.introducedByMemberId) {
      inforDetails.IntroducedById = this.introducedByMemberId;
    }

    inforDetails.AtgStatusList = this.atgStatusList;
    inforDetails.BranchId = this.selectedMember.BranchId;
    if (this.isChangeGroupMember) {
      inforDetails.GroupMemberList = this.groupMemberList;
    } else {
      inforDetails.GroupMemberList = null;
    }
    if (this.isChangeFamilyMember) {
      inforDetails.FamilyMmeberList = this.familyMemberList;
    } else {
      inforDetails.FamilyMmeberList = null;
    }
    inforDetails.Id = this.selectedMember.Id;
    PreloaderService.showPreLoader();
    this.exceMemberService.SaveMemberInfoWithNotes(inforDetails).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        const sTextArray = res.Data.split(':');
        if (sTextArray[0] === 'GATERROR') {
          this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.ErrGATCardNumberDuplicated' + ' : ' + sTextArray[1] });
        } else if (res !== '-1') {
          if (sTextArray.Length > 0) {
            const atgKid = !sTextArray[0].Equals('-2') ? sTextArray[0] : '';
            this.memberInfoForm.patchValue({
              AtgKid: atgKid
            });
          }
        }
        this.translateService.get('MEMBERSHIP.SaveSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans => this.message = trans);
        this.type = 'SUCCESS';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      },
      err => {
        PreloaderService.hidePreLoader();
        this.translateService.get('MEMBERSHIP.SaveNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans => this.message = trans);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      })
  }

  atgStatusOnChange(deviceValue, selectedItem) {
    this.atgStatusList.find(x => x.Id === selectedItem.Id).SelectedStatus = deviceValue;
  }

  viewGymsForAccount(val, content) {
    this.exceMemberService.GetGymsforAccountNumber(val.AccountNumber).pipe(takeUntil(this.destroy$)).subscribe(result => {
      this.atgBranchList = result.Data;
      this.memberRegisterView = this.modalService.open(content, { width: '800' });
    },
      err => {
        PreloaderService.hidePreLoader();
      })
  }

  gymChange(branch) {
    this.validateChangeGymInMember(branch);
  }

  validateChangeGymInMember(newBranch) {
    if (this.selectedMember.BranchId !== newBranch.BranchId) {
      this.exceMemberService.validateChangeGymInMember(this.selectedMember.BranchId, newBranch.BranchId, this.selectedMember.Id).pipe(
        takeUntil(this.destroy$)
        ).subscribe(
        res => {
          switch (res.Data) {
            case 1:
              this.selectedMember.BranchId = newBranch.BranchId;
              // MemberInfoWithNoteVm.Member.BranchName = newBranch1.BranchName;
              // MemberInfoWithNoteVm.SelectedBranch = newBranch1;
              // MemberShipSession.GetInstance().BranchId = newBranch1.Id;
              break;
            case 2:
              this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.msgPleseChangeGymFromBAContract' });
              break;
            case -1:
              this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.msgGymAccountNumbersNotEqual' });
              break;
            case -2:
              this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.msgBasicActivityDifferentSelectedGym' });
              break;
            case -3:
              this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.msgGymAccountNumbersNotEqual' });
              break;
            case -4:
              this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.msgBasicActivityDifferentSelectedGym' });
              break;
            case -5:
              this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.ReturnShopAccountBalance' });
              break;
            case -6:
              this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.MsgPaytheShopBalance' });
              break;
            case -7:
              this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.ReturnPrepaisAccountBalace' });
              break;
            case -8:
              this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.ReturnPrepaidAndShopAccountBalace' });
              break;
            case -9:
              this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.ReturnPrepaidAndNegShopAccountBalace' });
              break;
            default:
              this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.msgErrorInValidation' });
              break;
          }
        })
    }
  }

  changeSendAdvertising(val) {
    this.memberInforNote.IsSendAdvertisement = val;
    this.memberInfoForm.patchValue({ IsSendAdvertisement: val });
  }

  changeFollowpUp(val) {
    this.memberInforNote.IsFollowUp = val;
    this.memberInfoForm.patchValue({ IsFollowUp: val });
  }

  generatePinCode() {
    const val = Math.floor(1000 + Math.random() * 9000);
    this.memberInfoForm.patchValue({ PinCode: val });
  }

  getGatCardNo() {
    const sessionKey = this.createGuid();
    if (sessionKey) {
      // this.addSession('GATCARDNO', '', sessionKey, 'alpha/exceadmin');
      this.exceSessionService.AddSession('GATCARDNO', '', sessionKey, this._user);
      // this.timer = Observable.timer(0, 2000);
      timer(0, 2000).pipe(takeUntil(this.destroy$)).subscribe(t => this.tickerFunc(t, sessionKey));
    }
  }

  createGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      // tslint:disable-next-line:no-bitwise
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  tickerFunc(tick: number, sessionKey: string) {
    this.exceMemberService.getSessionValue(sessionKey).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result.Data) {
          this.sub.unsubscribe();
          if (result.Data === 'ERROR') {
            // Error while uploading - add message
          } else {
            this.exceMemberService.validateGatCardNo(result.Data).pipe(takeUntil(this.destroy$)).subscribe(
              getCardResult => {
                if (getCardResult.Data) {
                  if (!getCardResult.Data) {
                    this.memberInfoForm.controls['GatCardNo'].setErrors({
                      'gatCardTaken': true
                    });
                  } else {
                    this.memberInfoForm.patchValue({ GatCardNo: result.Data });
                  }
                }
              }
            )
          }
        } else if (tick === 9) {
          this.sub.unsubscribe();
        }
      })

    // if (tick === 9) {
    //   this.sub.unsubscribe();
    // }
    //  this.ticks = tick
  }

  employeeSelectionModal(content: any) {
    this.translateService.get('MEMBERSHIP.SelectEmployee').pipe(
      takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
    this.isBranchVisible = false;
    this.multipleRowSelect = false;
    this.isRoleVisible = false;
    this.isStatusVisible = false;
    this.isAddBtnVisible = false;
    this.isDbPagination = false;
    this.isBasicInforVisible = false;
    this.entitySelectionType = EntitySelectionType[EntitySelectionType.INS];
    this.columns = [{ property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'CustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ]
    PreloaderService.showPreLoader();
    this.exceMemberService.getInstructors('', true).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          PreloaderService.hidePreLoader();
          this.items = result.Data;
          this.itemCount = result.Data.length;
          this.newMemberRegisterViews = this.modalService.open(content, { width: '800' });
        } else {
          PreloaderService.hidePreLoader();
        }
      }
      )

  }

  selectIntroducedView(content) {
    this.translateService.get('MEMBERSHIP.MemberSelection').pipe(
      takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
    this.newMemberRegisterView = this.modalService.open(content, { width: '900'});
  }

  removeIntroducedBy() {
    this.memberInfoForm.patchValue({
      IntroducedByName: ''
    })
    this.isIntroducedByChanged = true;
    this.introducedByMemberId = -1;
  }

  memberSelectionView(content: any, searchType: any) {
    this.translateService.get('MEMBERSHIP.MemberSelection').pipe(
      takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue)
    this.entitySearchType = searchType;
    this.isAddBtnVisible = true;
    this.isBranchVisible = true;
    this.isRoleVisible = true;
    this.isStatusVisible = true;
    this.isAddBtnVisible = true;
    this.isDbPagination = true;
    this.isBasicInforVisible = true;
    this.defaultRole = 'ALL'
    this.multipleRowSelect = true;
    this.items = [];
    this.columns = [];
    this.entitySelectionType = EntitySelectionType[EntitySelectionType.MEM];

    if (searchType === MemberSearchType[MemberSearchType.FAMILYMEMBER] || searchType === MemberSearchType[MemberSearchType.GROUP]
      || searchType === MemberSearchType[MemberSearchType.INTRODUCEDBY]) {
      this.columns = [{ property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DATE },
      { property: 'Mobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
      ]
    }

    if (searchType === MemberSearchType[MemberSearchType.GROUP]) {
      if (this.groupMemberList.length === 0 && this.groupMemberLstCount > 0) {
        this.isChangeGroupMember = true;
        const groupMemberId = this.memberInforNote.GroupId > 0 ? this.memberInforNote.GroupId : this.memberInforNote.Id;
        this.exceMemberService.GetGroupMembers(groupMemberId).pipe(takeUntil(this.destroy$)).subscribe(result => {
          this.groupMemberList = result.Data;
          this.groupMemberList = this.groupMemberList.filter(x => x.Id !== this.memberInforNote.Id);
          this.memberInfoForm.patchValue({
            groupMembersCount: this.groupMemberList.length,
          });
          this.groupMemberLstCount = this.groupMemberList.length;
          this.newMemberRegisterView = this.modalService.open(content, {});
        },
          error => {
            console.log(error);
          })
      } else {
        this.newMemberRegisterView = this.modalService.open(content, {});
      }
    }

    if (searchType === MemberSearchType[MemberSearchType.FAMILYMEMBER]) {
      if (this.familyMemberList.length === 0 && this.familyMemberLstCount > 0) {
        this.isChangeFamilyMember = true;
        const familyMemberId = this.memberInforNote.FamilyMemberId > 0 ? this.memberInforNote.FamilyMemberId : this.memberInforNote.Id;
        this.exceMemberService.GetFamilyMembers(familyMemberId).pipe(takeUntil(this.destroy$)).subscribe(result => {
          this.familyMemberList = result.Data;
          this.familyMemberList = this.familyMemberList.filter(x => x.Id !== this.memberInforNote.Id);
          this.memberInfoForm.patchValue({
            familyMembersCount: this.familyMemberList.length,
          });
          this.familyMemberLstCount = this.familyMemberList.length;
          this.newMemberRegisterView = this.modalService.open(content, {});
        },
          error => {
            console.log(error);
          })
      } else {
        this.newMemberRegisterView = this.modalService.open(content, {});
      }
    }
  }

  /*
  * Remove this functionallity, as a member can have multiple family members. Add route to famMember in the 'Se familiemedlemmer' popup instead.
   */
  routeToFamilyMember() {
/*     if ( this.FamilyMember.MemberId === -1 ) {
      return;
    }
    if (this.FamilyMember.MemberId === this.selectedMember.Id || this.FamilyMember.MemberId === 0) {
      this.translateService.get('MEMBERSHIP.canNotRouteToSelf').pipe(takeUntil(this.destroy$)).subscribe(trans => this.message = trans);
      this.type = 'INFO';
      this.isShowInfo = true;
      setTimeout(() => {
        this.isShowInfo = false;
      }, 4000);
      return;
    }
    const url = '/membership/card/' + this.FamilyMember.MemberId + '/' + this.FamilyMember.BranchId + '/' + 'MEM';
    this.exceBreadCrumbService.addBreadCumbItem.next({
      name: this.FamilyMember.Name,
      url: url
    });
    this.router.navigate([url]); */
  }

  groupMembersView() {
    if (this.groupMemberList.length === 0 && this.groupMemberLstCount > 0) {
      this.isChangeGroupMember = true;
      const groupMemberId = this.memberInforNote.GroupId > 0 ? this.memberInforNote.GroupId : this.memberInforNote.Id;
      this.exceMemberService.GetGroupMembers(groupMemberId).pipe(takeUntil(this.destroy$)).subscribe(result => {
        this.groupMemberList = result.Data;
        this.groupMemberList = this.groupMemberList.filter(x => x.Id !== this.memberInforNote.Id);
        this.memberInfoForm.patchValue({
          groupMembersCount: this.groupMemberList.length,
        });
        this.groupMemberLstCount = this.groupMemberList.length;
      },
        error => {
          console.log(error);
        })
    }
  }
  familyMembersView() {
    if (this.familyMemberList.length === 0 && this.familyMemberLstCount > 0) {
      this.isChangeFamilyMember = true;
      const familyMemberId = this.memberInforNote.FamilyMemberId > 0 ? this.memberInforNote.FamilyMemberId : this.memberInforNote.Id;
      this.exceMemberService.GetFamilyMembers(familyMemberId).pipe(takeUntil(this.destroy$)).subscribe(result => {
        this.familyMemberList = result.Data;
        this.familyMemberList = this.familyMemberList.filter(x => x.Id !== this.memberInforNote.Id);
        this.memberInfoForm.patchValue({
          familyMembersCount: this.familyMemberList.length,
        });
        this.familyMemberLstCount = this.familyMemberList.length;
      },
        error => {
          console.log(error);
        })
    }
  }

  closeFamilyMemberSelect() {
    this.familyRegView.dismiss();
  }

  closeView() {
    this.newMemberRegisterView.close();
  }

  selectIntroducedBy(member) {
      this.memberInfoForm.patchValue({
        IntroducedByName: member.FirstName + ' ' + member.LastName
      })
      this.isIntroducedByChanged = true;
      this.introducedByMemberId = member.ID;
      this.newMemberRegisterView.close();

  }

  selectedRowItem(item) {
    if (this.entitySelectionType === EntitySelectionType[EntitySelectionType.INS]) {
      this.memberInfoForm.patchValue({ InstructorId: item.Id })
      this.memberInfoForm.patchValue({ InstructorName: item.Name })
    }
    if (this.newMemberRegisterViews) {
      this.newMemberRegisterViews.close();
    }
  }
  selectedRowItems(item) {
    if (this.entitySelectionType === EntitySelectionType[EntitySelectionType.MEM]) {
      switch (this.entitySearchType) {
        case MemberSearchType[MemberSearchType.GROUP]:
          this.isChangeGroupMember = true;
          this.groupMemberSelectionView(null, item);
          break;

        case MemberSearchType[MemberSearchType.FAMILYMEMBER]:
          this.isChangeFamilyMember = true;
          this.familyMemberSelectionView(null, item);
          break;
      }
    }
    this.newMemberRegisterView.close();
  }
  searchDeatail(val: any) {
    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.MEM]:
        PreloaderService.showPreLoader();
        this.exceMemberService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
          this.entitySearchType, val.roleSelected.id, val.hit, false,
          false, '').pipe(takeUntil(this.destroy$)).subscribe(result => {
            if (result) {
              PreloaderService.hidePreLoader();
              if (this.entitySearchType === MemberSearchType[MemberSearchType.GROUP]) {
                this.groupMemberList.forEach(gMember => {
                  const index: number = result.Data.findIndex(item => item.Id === gMember.Id);
                  if (index !== -1) {
                    result.Data.splice(index, 1);
                  }
                });

                const memIndex: number = result.Data.findIndex(item => item.Id === this.selectedMember.Id);
                if (memIndex !== -1) {
                  result.Data.splice(memIndex, 1);
                }

                this.items = result.Data;
                this.itemCount = result.Data.length;
              } else if (this.entitySearchType === MemberSearchType[MemberSearchType.FAMILYMEMBER]) {

                this.familyMemberList.forEach(gMember => {
                  const index: number = result.Data.findIndex(item => item.Id === gMember.Id);
                  if (index !== -1) {
                    result.Data.splice(index, 1);
                  }
                });

                const memIndex: number = result.Data.findIndex(item => item.Id === this.selectedMember.Id);
                if (memIndex !== -1) {
                  result.Data.splice(memIndex, 1);
                }

                this.items = result.Data;
                this.itemCount = result.Data.length;
              } else if (this.entitySearchType === MemberSearchType[MemberSearchType.INTRODUCEDBY]) {
                this.items = result.Data;
                this.itemCount = result.Data.length;
              }
            } else {
              PreloaderService.hidePreLoader();
            }
          }
          )
        break;
      case EntitySelectionType[EntitySelectionType.INS]:
        PreloaderService.showPreLoader();
        this.exceMemberService.getInstructors(val.searchVal, true).pipe(takeUntil(this.destroy$)).subscribe(result => {
            if (result) {
              PreloaderService.hidePreLoader();
              this.items = result.Data;
            } else {
              PreloaderService.hidePreLoader();
            }
          }
          )
        break;
    }

  }
  addMemberModal(content: any) {
    this.addMemberView = this.modalService.open(content, { width: '900' });
    this.newMemberRegisterView.close();
  }

  addNewFamilyMember(content: any) {
    this.addMemberView = this.modalService.open(content, { width: '900' });
  }

  addEntity(item: any) {
    switch (this.entitySearchType) {
      case MemberSearchType[MemberSearchType.GROUP]:
        this.isChangeGroupMember = true;
        this.groupMemberSelectionView(item, null);
        break;
    }
    this.addMemberView.close();
  }

  addEntityFam(mem: any) {
    const list = [];
    list.push(mem);
    this.familyMembersSelected(list);
    this.addMemberView.close();
  }

  groupMemberSelectionView(item: any, itemList: any) {
    if (this.groupMemberList.length === 0 && this.groupMemberLstCount > 0) {
      const groupMemberId = this.memberInforNote.GroupId > 0 ? this.memberInforNote.GroupId : this.memberInforNote.Id;
      this.exceMemberService.GetGroupMembers(groupMemberId).pipe(takeUntil(this.destroy$)).subscribe(result => {
        this.groupMemberList = result.Data;
        this.groupMemberList = this.groupMemberList.filter(x => x.Id !== this.memberInforNote.Id);
        if (itemList !== null) {
          itemList.forEach(i => {
            this.groupMemberList.push(i);
          })
        } else {
          this.groupMemberList.push(item);
        }
        this.memberInfoForm.patchValue({
          groupMembersCount: this.groupMemberList.length,
        });
        this.groupMemberLstCount = this.groupMemberList.length;
      },
        error => {
          console.log(error);
        })
    } else {
      if (itemList !== null) {
        itemList.forEach(i => {
          this.groupMemberList.push(i);
        })
      } else {
        this.groupMemberList.push(item);
      }
      this.memberInfoForm.patchValue({
        groupMembersCount: this.groupMemberList.length,
      });
      this.groupMemberLstCount = this.groupMemberList.length;
    }
  }

  familyMemberSelectionView(item: any, itemList: any) {
    if (this.familyMemberList.length === 0 && this.familyMemberLstCount > 0) {
      this.isChangeGroupMember = true;
      const familyMemberId = this.memberInforNote.FamilyMemberId > 0 ? this.memberInforNote.FamilyMemberId : this.memberInforNote.Id;
      this.exceMemberService.GetFamilyMembers(familyMemberId).pipe(takeUntil(this.destroy$)).subscribe(result => {
        this.familyMemberList = result.Data;
        this.familyMemberList = this.familyMemberList.filter(x => x.Id !== this.memberInforNote.Id);
        if (itemList !== null) {
          itemList.forEach(i => {
            this.familyMemberList.push(i);
          })
        } else {
          this.familyMemberList.push(item);
        }
        this.memberInfoForm.patchValue({
          familyMembersCount: this.familyMemberList.length,
        });
        this.familyMemberLstCount = this.familyMemberList.length;
      }, null, null);
    } else {
      if (itemList !== null) {
        itemList.forEach(i => {
          this.familyMemberList.push(i);
        })
      } else {
        this.familyMemberList.push(item);
      }
      this.memberInfoForm.patchValue({
        familyMembersCount: this.familyMemberList.length,
      });
      this.familyMemberLstCount = this.familyMemberList.length;
    }
  }

  deleteGroupMember(deletedItem) {
    const index: number = this.groupMemberList.indexOf(deletedItem);
    if (index !== -1) {
      this.isChangeGroupMember = true;
      this.groupMemberList.splice(index, 1);
      this.memberInfoForm.patchValue({
        groupMembersCount: this.groupMemberList.length,
      });
      this.groupMemberLstCount = this.groupMemberList.length;
    }
  }
  deleteFamilyMember(deletedItem) {
    const index: number = this.familyMemberList.indexOf(deletedItem);
    if (index !== -1) {
      this.isChangeFamilyMember = true;
      this.familyMemberList.splice(index, 1);
      this.memberInfoForm.patchValue({
        familyMembersCount: this.familyMemberList.length,
      });
      this.familyMemberLstCount = this.familyMemberList.length;
    }
  }
  changeGroupMember() {
    this.memberInfoForm.patchValue({
      groupMembersCount: this.groupMemberList.length,
    });
  }

  changeFamilyMember() {

  }
  printGroupMember() {
    PreloaderService.showPreLoader();
    // this.excePdfViewerService.openPdfViewer('Exceline', 'http://www.pdf995.com/samples/pdf.pdf');
    this.exceMemberService.printGroupMemberListByMember(this.selectedMember.Id).pipe(takeUntil(this.destroy$)).subscribe(result => {
      PreloaderService.hidePreLoader();
      if (result.Data.FileParth.trim() !== '') {
        // TO DO ee.IsPriview
        // this.excePdfViewerService.openPdfViewer('Exceline', 'http://www.pdf995.com/samples/pdf.pdf');
        this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
      }
    },
      err => {
        PreloaderService.hidePreLoader();
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.MsgATGDocFailed' });
      });
  }
}
