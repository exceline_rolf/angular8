import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { ExcelineMember } from '../models/ExcelineMember';
import { Component, OnInit, ElementRef, ComponentFactoryResolver, ViewContainerRef, ViewChild, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { ExceMemberService } from '../services/exce-member.service';
import { McBasicInfoService } from './mc-basic-info/mc-basic-info.service';
import { ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UsBreadcrumbService } from '../../../shared/components/us-breadcrumb/us-breadcrumb.service';
import { ISlimScrollOptions } from '../../../shared/directives/us-scroll/classes/slimscroll-options.class';
import { McContractsComponent } from './mc-contract/mc-contracts/mc-contracts.component';
import { McInfoWithNotesComponent } from './mc-info-with-notes/mc-info-with-notes.component';
import { McBookingComponent } from './mc-booking/mc-booking.component';
import { McClassesComponent } from './mc-classes/mc-classes.component';
import { McXtraLogComponent } from './mc-xtra-log/mc-xtra-log.component';
import { McIntroducedMembersComponent } from './mc-introduced-members/mc-introduced-members.component';
import { McEventLogComponent } from './mc-event-log/mc-event-log.component'
import { McCommunicationLogComponent } from './mc-communication-log/mc-communication-log.component';
import { McPaymentsComponent } from 'app/modules/membership/member-card/mc-payments/mc-payments.component';
import { McInterestsComponent } from './mc-interests/mc-interests.component';
import { McSponsoringComponent } from './mc-sponsoring/mc-sponsoring.component';
import { McVisitComponent } from './mc-visit/mc-visit.component'
import { McContractRegComponent } from './mc-contract/mc-contract-reg/mc-contract-reg.component';
import { McContractDetailsComponent } from './mc-contract/mc-contract-details/mc-contract-details.component';
import { McOrdersDetailsComponent } from './mc-orders/mc-orders-details/mc-orders-details.component';
import { McOrdersHomeComponent } from './mc-orders/mc-orders-home/mc-orders-home.component';
import { McDocumentsComponent } from './mc-documents/mc-documents.component';
import { McFollowUpComponent } from './mc-follow-up/mc-follow-up.component';
import { McResignComponent } from './mc-resign/mc-resign.component';
import { MemberCardService } from 'app/modules/membership/member-card/member-card.service';
import { McTrainingProgramComponent } from '../member-card/mc-training-program/mc-training-program.component';
import { McEconomyComponent } from 'app/modules/membership/member-card/mc-economy/mc-economy.component';
import { McFreezeHomeComponent } from '../member-card/mc-freeze/mc-freeze-home/mc-freeze-home.component';
import { TranslateService } from '@ngx-translate/core';
import { ExceTitleService } from '../services/exce-title.service';
import { CropperSettings } from '../../../shared/components/us-crop-image/cropperSettings';
import { UsCropImageComponent } from '../../../shared/components/us-crop-image/us-crop-image.component';
import { Bounds } from '../../../shared/components/us-crop-image/model/bounds';
import { UsbModalRef } from 'app/shared/components/us-modal/modal-ref';
import * as moment from 'moment';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { ExceMessageService} from 'app/shared/components/exce-message/exce-message.service';
import { AdminService } from '../../admin/services/admin.service';
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.scss']
})
export class MemberCardComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private selectedMember: ExcelineMember;

  snapShotModel: UsbModalRef;
  _memberID: number;
  _branchID: number;
  _memberRole: string;
  _editMode = false;
  _isMenuOpen = true;
  state = 'INFOWITHNOTE';
  title: string;
  // selectedMember?: any;
  countries?: any;
  options: ISlimScrollOptions;
  tabContentHeight: number;
  contractComponent: McContractsComponent;
  isleftPanelCollapsed: boolean;
  loadedInfo = false;
  isFromEmployeeCard = false
  popupview: UsbModalRef;
  popUpUps: any;
  selectedPopUp: any;
  public memberImage = 'assets/images/member-card-avatar.svg'

  @Input() MemberId;
  @Input() BranchId;
  @Input() Role;

  @ViewChild('mainContainer', { read: ViewContainerRef, static: false }) mainContainer;

  /* **********************************************************************
   * variables for cropping image
   * Tue Apr 03 2018 11:16:51 GMT+0530 (Sri Lanka Standard Time)
  **********************************************************************/
  atDecition = true;
  useWebcam = false;
  useFileUpload = false;
  reader = new FileReader();
  name: string;
  data1: any;
  cropperSettings1: CropperSettings;
  croppedWidth: number;
  croppedHeight: number;
  @ViewChild('cropper', {static: false}) cropper: UsCropImageComponent;
  @ViewChild('PopUp', { static: true }) popUp: ElementRef;
  isIntroducedBy: boolean;
  /////////////////////////////////////

  // Memory leak fix test

  constructor(
    private elRef: ElementRef,
    private memberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private route: ActivatedRoute,
    private breadcrumbService: UsBreadcrumbService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private viewContainerRef: ViewContainerRef,
    private translateService: TranslateService,
    private memberCardService: MemberCardService,
    private modalService: UsbModal,
    private router: Router,
    private titleService: ExceTitleService,
    private exceBreadcrumbService: ExceBreadcrumbService,
    private exceMessageService: ExceMessageService,
    private adminService: AdminService
  ) {
    this._memberID = this.route.snapshot.params['Id'];
    this._branchID = this.route.snapshot.params['BranchId'];
    this._memberRole = this.route.snapshot.params['Role'];
    if (this._memberRole === 'EMP') {
      this.isFromEmployeeCard = true;
      this._memberRole = 'MEM'
    }
    this.options = {
      barBackground: '#C9C9C9',
      gridBackground: '#D9D9D9',
      barBorderRadius: '10',
      barWidth: '6',
      gridWidth: '2'
    };
    if (this.isFromEmployeeCard) {
      breadcrumbService.hideRoute('/adminstrator/employee-card');
      breadcrumbService.hideRoute('/adminstrator/employee-card/' + this.route.snapshot.params['Id']);
      breadcrumbService.hideRoute('/adminstrator/employee-card/' + this.route.snapshot.params['Id'] + '/' + this.route.snapshot.params['BranchId']);
    } else {
      breadcrumbService.hideRoute('/membership/card');
      breadcrumbService.hideRoute('/membership/card/' + this.route.snapshot.params['Id']);
      breadcrumbService.hideRoute('/membership/card/' + this.route.snapshot.params['Id'] + '/' + this.route.snapshot.params['BranchId']);
    }

    /***********************************************************************
    * initialize values for cropping image
    * Tue Apr 03 2018 11:16:51 GMT+0530 (Sri Lanka Standard Time)
    **********************************************************************/
    this.cropperSettings1 = new CropperSettings();
    this.cropperSettings1.width = 100;
    this.cropperSettings1.height = 100;
    this.cropperSettings1.croppedWidth = 200;
    this.cropperSettings1.croppedHeight = 200;
    this.cropperSettings1.canvasWidth = 200;
    this.cropperSettings1.canvasHeight = 200;
    this.cropperSettings1.minWidth = 10;
    this.cropperSettings1.minHeight = 10;
    this.cropperSettings1.rounded = false;
    this.cropperSettings1.keepAspect = false;
    this.cropperSettings1.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings1.cropperDrawSettings.strokeWidth = 2;
    this.data1 = {};
    //////////////////////////////////////////////////////////////////////
  }

  ngOnInit() {
    /* Can get last member from localstorage here */

    this.route.url.pipe(
      take(1),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.setTitle()
      }
    }, null, null);

    this.titleService.currentTitle.pipe(
      takeUntil(this.destroy$)
      ).subscribe(title => {
      this.title = title;
    }, err => {}, () => {});

    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        this.selectedMember = member;
      }, err => {}, () => {}
    );
    this.memberService.getMemberDetails(this._memberID, this._branchID, this._memberRole).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          if (result.Data.Id === 0) {
            result.Data = this.selectedMember;
          };
          if (result.Data.ImagePath !== null && result.Data.ImagePath !== '') {
            this.memberImage = '/memberimages' + result.Data.ImageURLDomain;
          } else {
            this.memberImage = 'assets/images/member-card-avatar.svg';
          }
          // this.basicInfoService.setSelectedMember(result.Data); // removed as it is called twice: Here and in mc-basic-info onInit()
          this.exceBreadcrumbService.addBreadCumbItem.next({
            name: this.selectedMember.Name,
            url: '/membership/card/' + this.route.snapshot.params['Id'] + '/' + this.route.snapshot.params['BranchId'] + '/' + this._memberRole
          });
          // Does the same thing when isFromEmployeeCard is true and false
/*           if (this.isFromEmployeeCard) {
            this.exceBreadcrumbService.addBreadCumbItem.next({
              name: this.selectedMember.Name,
              url: '/membership/card/' + this.route.snapshot.params['Id'] + '/' + this.route.snapshot.params['BranchId'] + '/' + this._memberRole
            });
          } else {
            this.exceBreadcrumbService.addBreadCumbItem.next({
              name: this.selectedMember.Name,
              url: '/membership/card/' + this.route.snapshot.params['Id'] + '/' + this.route.snapshot.params['BranchId'] + '/' + this._memberRole
            });
          } */

          this.popUpUps = result.Data.FollowUpDetailsList;
          if (this.popUpUps.length > 0) {
            this.selectedPopUp = this.popUpUps.pop();
            this.popupview = this.modalService.open(this.popUp, { width: '300' });
          } else {
            // do nothing
          }
        }
      }, null, null);

      this.adminService.getGymSettings('OTHER', null, this.BranchId).pipe(takeUntil(this.destroy$)).subscribe( result => {
        if (result) {
          this.isIntroducedBy = result.Data[0].IsUsingIntroducedBy;
        }
      }, err => {}, () => {});

    this.basicInfoService.getEditMode().pipe(takeUntil(this.destroy$)).subscribe(editMode => {
      this._editMode = editMode
    }, err => {}, () => {});

    this.basicInfoService.addNewContractEvent.pipe(takeUntil(this.destroy$)).subscribe(addContractStatus => {
      if (addContractStatus === true) {
        this.setCardState('REGCONTRACT');
      }
    }, null, null);

    this.basicInfoService.selectContractEvent.pipe(takeUntil(this.destroy$)).subscribe(cardState => {
      if (cardState === 'CONTRACTCARD') {
        this.setCardState('CONTRACTCARD');
      }
    }, null, null);

    this.setTitle();
    this.loadedInfo = true
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.popupview) {
      this.popupview.close();
    }
    if (this.snapShotModel) {
      this.snapShotModel.close();
    }
  }

  btnOkHandler() {
    this.popupview.close();
    if (this.popUpUps.length > 0) {
      this.selectedPopUp = this.popUpUps.pop();
      this.popupview = this.modalService.open(this.popUp, { width: '300' });
    }
  }


  /***********************************************************************
   * functions for cropping image
   * Tue Apr 03 2018 11:16:51 GMT+0530 (Sri Lanka Standard Time)
   **********************************************************************/
  wantToUploadFile() {
    this.atDecition = false;
    this.useFileUpload = true;
    this.useWebcam = false;
  }

  wantToUseWebCam() {
    this.atDecition = false;
    this.useFileUpload = false;
    this.useWebcam = true;
    setInterval(() => {
      document.getElementById('preloader').style.display = 'none'
    }, 5000)
  }

  cropped(bounds: Bounds) {
    this.croppedHeight = bounds.bottom - bounds.top;
    this.croppedWidth = bounds.right - bounds.left;
  }

  fileChangeListener($event) {
    const image: any = new Image();
    const file: File = $event.target.files[0];
    const myReader: FileReader = new FileReader();
    const that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
    };
    myReader.readAsDataURL(file);
  }

  cancelAction() {
    this.atDecition = true;
    this.useFileUpload = false;
    this.useWebcam = false;
    this.data1 = {};
  }

  ////////////////////////////////////////////////////////////////////////

  doesImageExists(url: string): boolean { // will check if the image exists at a perticualr url
    const http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status !== 404;
  }

  setTitle() {
    const currentComponent = this.router.url.split('/')[this.router.url.split('/').length - 1]
    switch (currentComponent) {
      case 'infoWithNotes': {
        this.title = 'MEMBERSHIP.miwnInfoWithNotes';
        break;
      }
      case 'contract-details': {
        this.title = 'MEMBERSHIP.Contract';
        break;
      }

      case 'sponsoring': {
        this.title = 'MEMBERSHIP.Sponsoring';
        break;
      }
      case 'contracts': {
        this.title = 'MEMBERSHIP.ViewContracts';
        break;
      }
      case 'booking': {
        this.title = 'MEMBERSHIP.Booking';
        break;
      }

      case 'classes': {
        this.title = 'MEMBERSHIP.Classes';
        break;
      }

      case 'introduce-members': {
        this.title = 'MEMBERSHIP.IntroducedMembersTitle';
        break;
      }

      case 'interests': {
        this.title = 'MEMBERSHIP.Interests';
        break;
      }

      case 'xtra-log': {
        this.title = 'MEMBERSHIP.XtraLog';
        break;
      }

      case 'communication-log': {
        this.title = 'MEMBERSHIP.CommunicationLogTitle';
        break;
      }

      case 'event-log': {
        this.title = 'MEMBERSHIP.EventLogTitle';
        break;
      }

      case 'freeze': {
        this.title = 'MEMBERSHIP.FreezeContract';
        break;
      }

      case 'payments': {
        this.title = 'MEMBERSHIP.PHVPaymentHistory';
        break;
      }

      case 'REGCONTRACT': {
        this.title = 'MEMBERSHIP.RegisterContract';
        break;
      }

      case 'orders': {
        this.title = 'MEMBERSHIP.Orders';
        break;
      }

      case 'visits': {
        this.title = 'MEMBERSHIP.VisitC';
        break;
      }
      case 'documents': {
        this.title = 'MEMBERSHIP.DocumentsTitle';
        break;
      }

      case 'resign': {
        this.title = 'MEMBERSHIP.ResignContract';
        break;
      }
      case 'invoices': {
        this.title = 'MEMBERSHIP.INVInvoices';
        break;
      }
      case 'followUp': {
        this.title = 'MEMBERSHIP.Followup';
        break;
      }
      case 'training-program': {
        this.title = 'MEMBERSHIP.TrainingProgramsC';
        break;
      }
      case 'ecconomy': {
        this.title = 'MEMBERSHIP.EconomyC';
        break;
      }
      case 'purchase-history': {
        this.title = 'MEMBERSHIP.Shop';
        break;
      }
      case 'shop-account': {
        this.title = 'MEMBERSHIP.Shop';
        break;
      }
      case 'shop': {
        this.title = 'MEMBERSHIP.Shop';
        break;
      }
      case 'contract-move-orders':
        this.title = 'MEMBERSHIP.MoveOrders';
        break;

      default: {
      }
        this.closeNav();
    }

  }
  editCustomer() {
    this.basicInfoService.updateEditMode(true, 'CARD');
    this._editMode = true;
  }

  editImage(content) {
    this.snapShotModel = this.modalService.open(content, { width: '800' });
    // this.basicInfoService.updateEditMode(true, 'IMAGE');
  }

  openNav() {
    if (!this._isMenuOpen) {
      this._isMenuOpen = true;
      document.getElementById('mc-menu-slide').style.width = '200px';
      this.elRef.nativeElement.querySelector('.off-canvas-panel-ani').style.marginRight = '200px';
    } else {
      this.closeNav();
    }
  }

  closeNav() {
    this._isMenuOpen = false;
    if (document.getElementById('mc-menu-slide')) {
      document.getElementById('mc-menu-slide').style.width = '0';
    }
    if (this.elRef.nativeElement.querySelector('.off-canvas-panel-ani')) {
      this.elRef.nativeElement.querySelector('.off-canvas-panel-ani').style.marginRight = '0';
    }
  }

  setCardState(status: string) {
    const parentRoute = '/membership/card/' +
      this.route.snapshot.params['Id'] + '/' +
      this.route.snapshot.params['BranchId'] + '/' +
      this._memberRole

    if (this.loadedInfo) {
      this.memberCardService.$isleftPanelCollapsed = true;
      this.isleftPanelCollapsed = true;
    } else {
      this.memberCardService.$isleftPanelCollapsed = false;
      this.isleftPanelCollapsed = false;
      this.loadedInfo = true;
    }

    this._editMode = false;
    switch (status) {
      case 'INFOWITHNOTE': {
        this.router.navigate([parentRoute + '/infoWithNotes']);
        this.translateService.get('MEMBERSHIP.miwnInfoWithNotes').pipe(
          takeUntil(this.destroy$)
          ).subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'CONTRACTCARD': {
        this.router.navigate([parentRoute + '/contract-details']);
        this.translateService.get('MEMBERSHIP.Contract').pipe(
          takeUntil(this.destroy$)
          ).subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'MEMBERSPONSORING': {
        this.router.navigate([parentRoute + '/sponsoring']);
        this.translateService.get('MEMBERSHIP.Sponsoring').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'MEMBERCONTRACTS': {
        this.router.navigate([parentRoute + '/contracts']);
        this.translateService.get('MEMBERSHIP.ViewContracts').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'MEMBERBOOKING': {
        this.router.navigate([parentRoute + '/booking']);
        this.translateService.get('MEMBERSHIP.Booking').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'MEMBERCLASSES': {
        this.router.navigate([parentRoute + '/classes']);
        this.translateService.get('MEMBERSHIP.Classes').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'INTRODUCEDMEMBERS': {
        this.router.navigate([parentRoute + '/introduce-members']);
        this.translateService.get('MEMBERSHIP.IntroducedMembersTitle').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'INTERESTS': {
        this.router.navigate([parentRoute + '/interests']);
        this.translateService.get('MEMBERSHIP.Interests').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'MEMBERXTRALOG': {
        this.router.navigate([parentRoute + '/xtra-log']);
        this.translateService.get('MEMBERSHIP.XtraLog').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'COMMUNICATIONLOG': {
        this.router.navigate([parentRoute + '/communication-log']);
        this.translateService.get('MEMBERSHIP.CommunicationLogTitle').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'MEMBEREVENTLOG': {
        this.router.navigate([parentRoute + '/event-log']);
        this.translateService.get('MEMBERSHIP.EventLogTitle').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'FREEZE': {
        this.router.navigate([parentRoute + '/freeze']);
        this.translateService.get('MEMBERSHIP.FreezeContract').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'MEMBERPAYMENT': {
        this.router.navigate([parentRoute + '/payments']);
        this.translateService.get('MEMBERSHIP.PHVPaymentHistory').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'REGCONTRACT': {
        this.router.navigate([parentRoute + '/']);
        this.translateService.get('MEMBERSHIP.RegisterContract').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'ORDERS': {
        this.router.navigate([parentRoute + '/orders']);
        this.translateService.get('MEMBERSHIP.Orders').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }


      case 'VISITS': {
        this.router.navigate([parentRoute + '/visits']);
        this.translateService.get('MEMBERSHIP.VisitC').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'DOCUMENTS': {
        this.router.navigate([parentRoute + '/documents']);
        this.translateService.get('MEMBERSHIP.DocumentsTitle').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'RESIGN': {
        this.router.navigate([parentRoute + '/resign']);
        this.translateService.get('MEMBERSHIP.ResignContract').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'FOLLOWUP': {
        this.router.navigate([parentRoute + '/followUp']);
        this.translateService.get('MEMBERSHIP.FollowUp').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'TRAINING': {
        this.router.navigate([parentRoute + '/training-program']);
        this.translateService.get('MEMBERSHIP.TrainingProgramsC').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'ECCONOMY': {
        this.router.navigate([parentRoute + '/ecconomy']);
        this.translateService.get('MEMBERSHIP.EconomyC').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'INVOICES': {
        this.router.navigate([parentRoute + '/invoices']);
        this.translateService.get('MEMBERSHIP.INVInvoices').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'SHOP': {
        this.router.navigate([parentRoute + '/purchase-history']);
        this.translateService.get('MEMBERSHIP.Shop').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      default: {
      }

    }
    // ** closes nav on routing **
    // this.closeNav();
  }


  close(status) {
    if (status === 'INFOWITHNOTE') {
      this.isleftPanelCollapsed = false;
      this.memberCardService.$isleftPanelCollapsed = this.isleftPanelCollapsed;
    } else if (status === '') {
      this.isleftPanelCollapsed = !this.isleftPanelCollapsed;
      this.memberCardService.$isleftPanelCollapsed = this.isleftPanelCollapsed;
    } else {
      this.isleftPanelCollapsed = true;
      this.memberCardService.$isleftPanelCollapsed = this.isleftPanelCollapsed;
    }
    // this.isleftPanelCollapsed = !this.isleftPanelCollapsed;
    // this.memberCardService.$isleftPanelCollapsed = this.isleftPanelCollapsed;
    this._editMode = false
  }

  /** to save the member */
  saveImage() {
    let tempMember = null;
    this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
    ).subscribe(currentMember => {tempMember = currentMember}, null, null);
    tempMember.Image = this.data1.image;
    this.memberService.updateMember(tempMember).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          const date = new Date();
          const notification = {
            'MemberId': Number(tempMember.Id),
            'BranchId': Number(this._branchID),
            'Notification': {
              'Title': 'IMAGE CHANGE',
              'Description': 'Image was changed',
              'CreatedDate': date,
              'TypeId': 3,
              'SeverityId': 3,
              'StatusId': 1
              }
            }
            this.memberService.AddEventlogForImangeChange(notification).pipe(takeUntil(this.destroy$)).subscribe(
              res => {
              }
            );
        }
/*         this.basicInfoService.setSelectedMember(tempMember);
        this.snapShotModel.close();
        this.memberImage = this.data1.image;
        this.cancelAction(); */
        // this.memberImage = tempMember.ImageURLDomain;
      }, error => {
        this.basicInfoService.setSelectedMember(tempMember);
        this.memberImage = this.data1.image;
        this.snapShotModel.close();
        this.cancelAction();
      }, () => {
        this.basicInfoService.setSelectedMember(tempMember);
        this.snapShotModel.close();
        this.memberImage = this.data1.image;
        this.cancelAction();
      }
    );
/*     this.snapShotModel.close();
    this.memberImage = this.data1.image;
    this.cancelAction(); */
  }


}
