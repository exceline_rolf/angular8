import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { ExceMemberService } from '../../services/exce-member.service';
import { McBasicInfoService } from '../mc-basic-info/mc-basic-info.service';
import { ExceEconomyService } from '../../../economy/services/exce-economy.service';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { ExcePdfViewerService } from '../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { TranslateService } from '@ngx-translate/core';
import { UsbModalRef } from '../../../../shared/components/us-modal/modal-ref';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { Router, ActivatedRoute } from '@angular/router';
import { saveAs } from 'file-saver/FileSaver';
import { ExceLoginService } from './../../../login/exce-login/exce-login.service';
import { AdminService } from '../../../admin/services/admin.service';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-mc-invoices',
  templateUrl: './mc-invoices.component.html',
  styleUrls: ['./mc-invoices.component.scss']
})
export class McInvoicesComponent implements OnInit, OnDestroy {

  IsCNBtnEnable = true;
  modalReference: UsbModalRef;
  modalCreditNoteReference: UsbModalRef;
  selectedDetail: any = {};
  private selectedCreditNote: any = {};
  private invoicesSummeries = [];
  private selectedMember: any;
  private memberId: number;
  private customerNo: string;
  private customerName: string;
  private guardianId: number;
  private email: string;
  private smsNo: string;
  private isBrisIntegrated: boolean;
  private branchId: number;
  private destroy$ = new Subject<void>();
  public rowColors: any;
  blobUrl: any;
  blob: any;
  currentInvoice: any;
  generatePdfView: UsbModalRef;
  anonConfirm: any;
  AnonymizeNameOfSponsorInvoice = false;
  currentInvoiceNum: any;
  currentARItem: any;
  isSponsoredInvoice: any;

  @ViewChild('invoiceDetails', { static: true }) invoiceDetails: ElementRef;
  @ViewChild('creditNote', { static: true }) creditNote: ElementRef;
  @ViewChild('Pdfwindow', { static: true }) pdfView: ElementRef;

  itemResource: any;
  items = [];
  itemCount = 0;
  limit = 25;
  offset = 0;
  hit = 1;

    // default norwegian
    names = {
      ID : 'ID',
      InvoiceNo: 'Fakturanummer',
      Billed: 'Fakturert',
      DueDate: 'Forfall',
      Total: 'Total',
      Balance: 'Saldo',
      ContractNo: 'Kontraktsnummer',
      Payer: 'Betalt av/for'
    }

    colNamesNo = {
      ID: 'ID',
      InvoiceNo: 'Fakturanummer',
      Billed: 'Fakturert',
      DueDate: 'Forfall',
      Total: 'Total',
      Balance: 'Saldo',
      ContractNo: 'Kontraktsnummer',
      Payer: 'Betalt av/for'
    }

    colNamesEn = {
      ID: 'ID',
      InvoiceNo: 'Invoice number',
      Billed: 'Billed',
      DueDate: 'Due date',
      Total: 'Total',
      Balance: 'Saldo',
      ContractNo: 'Contract number',
      Payer: 'Payed by/for'
    }

  columnDefs: any;
  gridApi: any;
  gridColumnApi: any;
  lang: any;
  isRoutedFromMemberList = false;

  constructor(private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private excePdfViewerService: ExcePdfViewerService,
    private translateService: TranslateService,
    private modalService: UsbModal,
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private economyService: ExceEconomyService,
    private exceLoginService: ExceLoginService,
    private exceToolbarService: ExceToolbarService,
    private adminService: AdminService,
    private exceMessageService: ExceMessageService,
    private router: Router) {

      }


  ngOnInit() {
     // setting header names for ag grid by language
     this.lang = this.exceToolbarService.getSelectedLanguage()
     switch (this.lang.id) {
       case 'no':
         this.names = this.colNamesNo;
         break;
       case 'en':
         this.names = this.colNamesEn;
         break;
     }

     this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
    ).subscribe(currentMember => this.selectedMember = currentMember);

    this.columnDefs = [
      {headerName: this.names.ID, field: 'MembeInvoiceId', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.InvoiceNo, field: 'InvoiceNo', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Billed, field: 'DisplayDate', autoHeight: true, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.DueDate, field: 'DisplayDueDate', autoHeight: true, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Total, field: 'DisplayTotal', autoHeight: true, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Balance, field: 'DisplayBalance', autoHeight: true, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.ContractNo, field: 'ContractId', autoHeight: true, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Payer, field: 'PayeePayerName', autoHeight: true, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        switch (params.data.PayDirection) {
          case 1:
            return '<div class="d-flex align-items-center">'
              + '<span><svg class="d-block" version="1.1" id="pthleft" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"'
              + 'x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path fill="#2286D4" d="M12,0c6.6,0,12,5.4,12,12s-5.4,12-12,12C5.4,24,0,18.6,0,12S5.4,0,12,0z M10.5,16.5v-3c0,0,6-1.5,9,3c0-5-4-9-9-9v-3l-6,6L10.5,16.5z" />'
              + '</svg></span><span>' + params.value + '</span></div>';
          case 2:
            return '<div class="d-flex align-items-center">'
                + '<span><svg class="d-block" version="1.1" id="pthright" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"'
                + 'x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
                + 'xml:space="preserve">'
                + '<path fill="#C6731F" d="M12,0C5.4,0,0,5.4,0,12s5.4,12,12,12c6.6,0,12-5.4,12-12S18.6,0,12,0z M13.5,16.5v-3c0,0-6-1.5-9,3c0-5,4-9,9-9v-3l6,6L13.5,16.5z" />'
                + '</svg></span><span>' + params.value + '</span></div>';
          default:
            return;
        }
      }},
      {headerName: '', field: 'Innhold', suppressMenu: true, autoHeight: true, suppressSorting: true, suppressFilter: true, width: 60, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        return '<ng-template><button class="btn btn-primary btn-icon-min" title="pdf">'
            + '<span class="icon-pdf"></span></button></ng-template>'
        }
      },
      {headerName: 'Type', field: 'InvoiceType', suppressMenu: true, autoHeight: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, suppressFilter: true, width: 80, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        switch (params.value) {
          case 'ATG':
            return '<span title="' + params.data.InvoiceTypeTitle + '">'
              + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="M5.5,10.3l1,3.5c0,0,0,0.1,0,0.1h-2c0,0-0.1,0,0-0.1L5.5,10.3C5.4,10.3,5.4,10.3,5.5,10.3C5.5,10.3,5.5,10.3,5.5,10.3z M23.8,12c0,6.5-5.3,11.8-11.8,11.8C5.5,23.8,0.2,18.5,0.2,12C0.2,5.5,5.4,0.2,12,0.2C18.5,0.2,23.8,5.5,23.8,12z M8.4,16.2c0-0.1,0-0.1,0-0.2L6,8C5.9,7.7,5.8,7.6,5.5,7.6C5.2,7.6,5,7.7,4.9,8l-2.4,8c0,0.1,0,0.1,0,0.2c0,0.2,0.1,0.3,0.2,0.4c0.1,0.1,0.3,0.2,0.4,0.2c0.3,0,0.5-0.1,0.6-0.4L4,15.1c0,0,0-0.1,0.1-0.1h2.7c0,0,0.1,0,0.1,0.1l0.4,1.3c0.1,0.3,0.3,0.4,0.6,0.4c0.2,0,0.3-0.1,0.4-0.2C8.4,16.5,8.4,16.4,8.4,16.2z M14.9,8.2c0-0.2-0.1-0.3-0.2-0.4c-0.1-0.1-0.3-0.2-0.4-0.2h-3.9c-0.2,0-0.3,0.1-0.4,0.2C9.9,7.9,9.8,8,9.8,8.2S9.9,8.5,10,8.6c0.1,0.1,0.3,0.2,0.4,0.2h1.3c0,0,0.1,0,0.1,0.1v7.4c0,0.2,0.1,0.3,0.2,0.4c0.1,0.1,0.3,0.2,0.4,0.2s0.3-0.1,0.4-0.2c0.1-0.1,0.2-0.3,0.2-0.4V8.8c0,0,0-0.1,0.1-0.1h1.3c0.2,0,0.3-0.1,0.4-0.2C14.9,8.5,14.9,8.3,14.9,8.2z M21.5,14.6v-2.4c0-0.2-0.1-0.3-0.2-0.4c-0.1-0.1-0.3-0.2-0.4-0.2h-1.4c-0.2,0-0.3,0.1-0.4,0.2c-0.1,0.1-0.2,0.3-0.2,0.4s0.1,0.3,0.2,0.4c0.1,0.1,0.3,0.2,0.4,0.2h0.7c0,0,0.1,0,0.1,0.1v1.8c0,0.3-0.1,0.5-0.3,0.8s-0.5,0.3-0.8,0.3h-0.6c-0.3,0-0.5-0.1-0.8-0.3c-0.2-0.2-0.3-0.5-0.3-0.8V9.8c0-0.3,0.1-0.6,0.3-0.8s0.5-0.3,0.8-0.3h0.6c0.5,0,0.8,0.2,1,0.6c0.1,0.2,0.3,0.3,0.5,0.3c0.2,0,0.3-0.1,0.4-0.2c0.1-0.1,0.2-0.2,0.2-0.4c0-0.1,0-0.2-0.1-0.3c-0.2-0.4-0.5-0.7-0.8-0.9c-0.4-0.2-0.8-0.3-1.2-0.3h-0.6c-0.6,0-1.1,0.2-1.6,0.7c-0.4,0.4-0.7,1-0.7,1.6v4.8c0,0.6,0.2,1.1,0.7,1.6c0.4,0.4,1,0.7,1.6,0.7h0.6c0.6,0,1.1-0.2,1.6-0.7C21.3,15.7,21.5,15.2,21.5,14.6z"'
              + '/>'
              + '</svg></span>'
          case 'SMS':
            return '<span title="' + params.data.InvoiceTypeTitle + '">'
              + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="M12,0.2C5.4,0.2,0.2,5.5,0.2,12c0,6.5,5.3,11.8,11.8,11.8c6.5,0,11.8-5.3,11.8-11.8C23.8,5.5,18.5,0.2,12,0.2z M18.9,14.7c0,0.7-0.6,1.3-1.3,1.3h-3l-5.9,4l1.4-4H6.8c-0.7,0-1.3-0.6-1.3-1.3V7.3C5.5,6.6,6.1,6,6.8,6h10.8c0.7,0,1.3,0.6,1.3,1.3V14.7z"'
              + '/>'
              + '</svg></span>'
          case 'IN':
          return '<span title="' + params.data.InvoiceTypeTitle + '">'
            + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="M12,0.2C5.4,0.2,0.2,5.5,0.2,12S5.5,23.8,12,23.8S23.8,18.5,23.8,12S18.5,0.2,12,0.2z M7.6,16 c0,0.4-0.1,0.7-0.3,1c-0.2,0.2-0.4,0.3-0.8,0.3c-0.3,0-0.5-0.1-0.7-0.3c-0.2-0.2-0.3-0.5-0.3-1V8c0-0.4,0.1-0.7,0.3-0.9 C6,6.9,6.3,6.8,6.6,6.8s0.6,0.1,0.8,0.3c0.2,0.2,0.3,0.5,0.3,1V16z M18.4,15.9c0,0.9-0.4,1.3-1.1,1.3c-0.2,0-0.3,0-0.5-0.1 c-0.1,0-0.3-0.1-0.4-0.2c-0.1-0.1-0.2-0.2-0.3-0.4c-0.1-0.1-0.2-0.3-0.3-0.5l-3.8-5.9v5.9c0,0.4-0.1,0.7-0.3,0.9 c-0.2,0.2-0.4,0.3-0.7,0.3c-0.3,0-0.5-0.1-0.7-0.3c-0.2-0.2-0.2-0.5-0.2-0.9V8.3c0-0.3,0-0.6,0.1-0.8c0.1-0.2,0.2-0.4,0.4-0.5 c0.2-0.1,0.4-0.2,0.7-0.2c0.2,0,0.3,0,0.5,0.1S11.9,7,12,7.1c0.1,0.1,0.2,0.2,0.3,0.4c0.1,0.1,0.2,0.3,0.3,0.5l3.9,5.9v-6 c0-0.4,0.1-0.7,0.2-0.9c0.2-0.2,0.4-0.3,0.7-0.3c0.3,0,0.5,0.1,0.7,0.3c0.2,0.2,0.2,0.5,0.2,0.9V15.9z"'
              + '/> </svg></span>';
          case 'Shop':
          return '<span title="' + params.data.InvoiceTypeTitle + '">'
              + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="m15.3 8.4h-6.6c0-1.9 1.5-3.4 3.3-3.4s3.3 1.5 3.3 3.4zm8.5 3.6c0 6.5-5.3 11.8-11.8 11.8s-11.8-5.3-11.8-11.8 5.2-11.8 11.8-11.8c6.5 0 11.8 5.3 11.8 11.8zm-5.7 2.6l0.4-6.2h-2.5c0-2.3-1.8-4.1-4.1-4.1s-4.1 1.8-4.1 4.1h-2.4l0.6 6.2-0.3 4h12.7l-0.3-4z"'
              + '/> </svg></span>';
          case 'IP':
            return '<span title="' + params.data.InvoiceTypeTitle + '">'
              + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="M16.6,9.1c0.3,0.3,0.4,0.7,0.4,1.1c0,0.4-0.1,0.7-0.3,0.9c-0.2,0.2-0.4,0.4-0.8,0.5c-0.3,0.1-0.8,0.2-1.3,0.2h-1.4V8.7h1.4C15.7,8.7,16.3,8.8,16.6,9.1z M23.8,12c0,6.5-5.3,11.8-11.8,11.8S0.2,18.5,0.2,12S5.4,0.2,12,0.2C18.5,0.2,23.8,5.5,23.8,12z M8.7,8.2c0-0.4-0.1-0.8-0.3-1S7.9,6.9,7.6,6.9C7.3,6.9,7.1,7,6.9,7.2s-0.3,0.5-0.3,1v8.3c0,0.4,0.1,0.8,0.3,1s0.5,0.3,0.8,0.3c0.3,0,0.6-0.1,0.8-0.3s0.3-0.5,0.3-1V8.2z M19.2,10.2c0-0.5-0.1-1-0.2-1.4c-0.2-0.4-0.4-0.7-0.7-1c-0.3-0.3-0.7-0.5-1.1-0.6C16.7,7.1,16,7,15.2,7h-2.8c-0.5,0-0.8,0.1-1,0.3c-0.2,0.2-0.3,0.5-0.3,1v8.2c0,0.4,0.1,0.7,0.3,1s0.5,0.3,0.8,0.3c0.3,0,0.6-0.1,0.8-0.3c0.2-0.2,0.3-0.5,0.3-1v-3h2c1.3,0,2.3-0.3,3-0.8C18.9,12.1,19.2,11.3,19.2,10.2z"'
              + '/> </svg></span>';
          case 'PP':
            return '<span title="' + params.data.InvoiceTypeTitle + '">'
            + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
            + 'xml:space="preserve">'
            + '<path class="fill-primary" d="M16.8,8.7h-1.4v3.1h1.4c0.5,0,0.9-0.1,1.2-0.2c0.3-0.1,0.6-0.3,0.8-0.5s0.3-0.5,0.3-0.9c0-0.5-0.1-0.8-0.4-1.1C18.4,8.9,17.8,8.7,16.8,8.7z M7.3,8.7H5.9v3.1h1.4c0.5,0,0.9-0.1,1.2-0.2c0.3-0.1,0.6-0.3,0.8-0.5s0.3-0.5,0.3-0.9c0-0.5-0.1-0.8-0.4-1.1C8.8,8.9,8.2,8.7,7.3,8.7z M12,0.2C5.4,0.2,0.2,5.5,0.2,12S5.5,23.8,12,23.8S23.8,18.5,23.8,12S18.5,0.2,12,0.2z M10.6,12.6C10,13.2,9,13.4,7.8,13.4H5.9v2.9c0,0.4-0.1,0.7-0.3,0.9c-0.2,0.2-0.4,0.3-0.7,0.3c-0.3,0-0.6-0.1-0.8-0.3c-0.2-0.2-0.3-0.5-0.3-0.9V8.4c0-0.5,0.1-0.8,0.3-1c0.2-0.2,0.5-0.3,1-0.3h2.7c0.8,0,1.4,0.1,1.8,0.2c0.4,0.1,0.8,0.3,1.1,0.6s0.5,0.6,0.7,1c0.2,0.4,0.2,0.8,0.2,1.3C11.6,11.3,11.3,12.1,10.6,12.6z M20.2,12.6c-0.6,0.5-1.6,0.8-2.9,0.8h-1.9v2.9c0,0.4-0.1,0.7-0.3,0.9c-0.2,0.2-0.4,0.3-0.7,0.3s-0.6-0.1-0.8-0.3c-0.2-0.2-0.3-0.5-0.3-0.9V8.4c0-0.5,0.1-0.8,0.3-1s0.5-0.3,1-0.3h2.7c0.8,0,1.4,0.1,1.8,0.2s0.8,0.3,1.1,0.6c0.3,0.3,0.5,0.6,0.7,1s0.2,0.8,0.2,1.3C21.2,11.3,20.9,12.1,20.2,12.6z"'
            + '/> </svg></span>';
          case 'SP':
            return '<span title="' + params.data.InvoiceTypeTitle + '">'
            + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
            + ' xml:space="preserve">'
            + '<path class="fill-primary" d="M18.8,9.4c0.3,0.3,0.4,0.7,0.4,1.1c0,0.4-0.1,0.7-0.3,0.9s-0.4,0.4-0.8,0.5c-0.3,0.1-0.7,0.2-1.2,0.2h-1.4V8.9h1.4C17.9,8.9,18.5,9.1,18.8,9.4z M23.8,12c0,6.5-5.3,11.8-11.8,11.8S0.2,18.5,0.2,12S5.4,0.2,12,0.2C18.5,0.2,23.8,5.5,23.8,12z M11.6,14.6c0-0.5-0.1-1-0.3-1.3s-0.4-0.7-0.8-0.9c-0.3-0.2-0.7-0.4-1.2-0.6c-0.5-0.2-1-0.3-1.6-0.5c-0.5-0.1-0.8-0.2-1-0.3c-0.2-0.1-0.4-0.1-0.6-0.3c-0.2-0.1-0.4-0.2-0.5-0.4s-0.2-0.3-0.2-0.5C5.5,9.5,5.7,9.3,6,9c0.3-0.2,0.8-0.4,1.3-0.4c0.6,0,1,0.1,1.3,0.3c0.3,0.2,0.5,0.5,0.7,0.9c0.1,0.3,0.3,0.5,0.4,0.6c0.1,0.1,0.3,0.2,0.5,0.2c0.3,0,0.5-0.1,0.7-0.3c0.2-0.2,0.3-0.4,0.3-0.7c0-0.3-0.1-0.6-0.2-0.8c-0.1-0.3-0.4-0.6-0.7-0.8S9.6,7.6,9.1,7.4c-0.5-0.2-1-0.2-1.7-0.2C6.7,7.2,6,7.3,5.4,7.5s-1,0.6-1.4,1C3.7,9,3.6,9.5,3.6,10.1c0,0.6,0.1,1.1,0.4,1.5c0.3,0.4,0.7,0.7,1.2,1c0.5,0.2,1.1,0.4,1.9,0.6c0.6,0.1,1,0.2,1.3,0.4C8.8,13.6,9,13.8,9.2,14c0.2,0.2,0.3,0.5,0.3,0.8c0,0.4-0.2,0.8-0.6,1.1c-0.4,0.3-0.9,0.4-1.5,0.4c-0.5,0-0.8-0.1-1.1-0.2s-0.5-0.3-0.6-0.5c-0.2-0.2-0.3-0.5-0.4-0.8c-0.1-0.3-0.2-0.5-0.4-0.6c-0.2-0.1-0.3-0.2-0.6-0.2c-0.3,0-0.5,0.1-0.7,0.3c-0.2,0.2-0.3,0.4-0.3,0.7c0,0.4,0.1,0.9,0.4,1.4C4.1,16.7,4.5,17,5,17.3c0.7,0.4,1.5,0.6,2.5,0.6c0.9,0,1.6-0.1,2.2-0.4c0.6-0.3,1.1-0.7,1.4-1.2C11.5,15.8,11.6,15.2,11.6,14.6z M21.3,10.5c0-0.5-0.1-0.9-0.2-1.3c-0.2-0.4-0.4-0.7-0.7-1c-0.3-0.3-0.7-0.5-1.1-0.6c-0.4-0.1-1-0.2-1.8-0.2h-2.7c-0.5,0-0.8,0.1-1,0.3c-0.2,0.2-0.3,0.5-0.3,1v8c0,0.4,0.1,0.7,0.3,0.9c0.2,0.2,0.4,0.3,0.8,0.3c0.3,0,0.6-0.1,0.7-0.3c0.2-0.2,0.3-0.5,0.3-1v-2.9h1.9c1.3,0,2.3-0.3,2.9-0.8C21,12.3,21.3,11.5,21.3,10.5z"'
            + '/> </svg></span>';
          case 'Email':
            return '<span title="' + params.data.InvoiceTypeTitle + '">'
            + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
            + 'xml:space="preserve">'
            + '<path class="fill-primary" d="M12,0.2C5.4,0.2,0.2,5.5,0.2,12S5.5,23.8,12,23.8S23.8,18.5,23.8,12S18.5,0.2,12,0.2z M12,6.5h6.4l-3.2,3.2L12,12.9L8.8,9.7L5.6,6.5H12z M19.3,17.1H4.7V7.6h0L8.2,11l3.8,3.7l3.8-3.7l3.4-3.4h0V17.1z"'
            + '/> </svg></span>';
          case 'Paper':
            return '<span title="' + params.data.InvoiceTypeTitle + '">'
              + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
              + 'xml:space="preserve">'
              + '<path class="fill-primary" d="M12,0.2C5.4,0.2,0.2,5.5,0.2,12S5.5,23.8,12,23.8S23.8,18.5,23.8,12S18.5,0.2,12,0.2z M17.8,17.6c0,0.6-0.5,1.1-1.1,1.1H7.3c-0.6,0-1.1-0.5-1.1-1.1V6.4c0-0.6,0.5-1.1,1.1-1.1h5.9v3c0,0.9,0.7,1.6,1.6,1.6h3V17.6z M17.8,8.9L17.8,8.9h-3c-0.4,0-0.7-0.3-0.7-0.7v-3L17.8,8.9L17.8,8.9L17.8,8.9z"'
              + '/> </svg></span>';
          case 'CN':
            return '<span title="' + params.data.InvoiceTypeTitle + '">'
            + '<svg class="d-block" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;"'
            + 'xml:space="preserve">'
            + '<path class="fill-primary" d="M6.7,17.9h7.4v-3.1h3.1V6.1H6.7V17.9z M10.1,8.9h5v1.2h-5V8.9z M8.3,13.3c0.3-0.3,0.8-0.3,1.1,0l0.8,0.8l2.8-2.8c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1l-3.8,3.8l-1.9-1.9C8.1,14.1,8.1,13.6,8.3,13.3z M12,0.2C5.4,0.2,0.2,5.5,0.2,12S5.5,23.8,12,23.8S23.8,18.5,23.8,12S18.5,0.2,12,0.2z M18.5,15.4c0,0.2-0.1,0.3-0.2,0.4l-3.1,3.1c-0.1,0.1-0.3,0.2-0.4,0.2H6.1c-0.3,0-0.6-0.3-0.6-0.6v-13c0-0.3,0.3-0.6,0.6-0.6h11.8c0.3,0,0.6,0.3,0.6,0.6V15.4z"'
            + '/> </svg></span>'
        default:
          return;
        }}},
        {headerName: 'Status', field: 'InvoiceStatus', suppressMenu: true, autoHeight: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
        }, suppressMovable: true, cellRenderer: function(params) {
          switch (params.data.InvoiceStatusNo) {
            case 0:
              return ' <span class="badge badge-success py-1">' + params.value + '</span>';
            case 1:
              return '<span class="badge badge-danger py-1">' + params.value + '</span>';
            case 2:
              return '<span class="badge badge-warning py-1">' + params.value + '</span>';
            default:
              return;
          }
          }
        },
        {headerName: '', field: 'Detaljer', suppressMenu: true, suppressSorting: true, autoHeight: true, suppressFilter: true, width: 60, floatingFilterComponentParams: {suppressFilterButton: true},
          filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
          }, suppressMovable: true, cellRenderer: function(params) {
            return '<ng-template> <button class="btn btn-primary btn-icon-min" title="Detaljer"><span class="icon-detail"></span></button></ng-template>';
        }
      },
      ]

    this.hit = 1;
    if (this.selectedMember) {
      this.setMemberDetails(this.selectedMember);
      this.getMemberInvoice(this.selectedMember.Id, this.hit)
    }
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.adminService.GetGymCompanySettings('other').pipe(takeUntil(this.destroy$)).subscribe(result => {
      this.isBrisIntegrated = result.Data.IsBrisIntegrated;
    });
    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          if (!this.selectedMember) {
            this.selectedMember = member;
            if (this.selectedMember.InvoiceNo) {
              this.isRoutedFromMemberList = true;
            }
            this.setMemberDetails(member);
            this.getMemberInvoice(this.selectedMember.Id, this.hit)
            this.branchId = this.selectedMember

          }
          // this.getMemberInvoice(member.Id, this.hit)
        }
      },
      error => {
        // console.log(error);
      });

      this.exceMessageService.noCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
        if (value.id === 'CONFIRM_ANON_NAME') {
          this.AnonymizeNameOfSponsorInvoice = false;
          this.openGenerateConfirmModal(this.pdfView, this.currentInvoiceNum);
        }
      });

      this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
        if (value.id === 'CONFIRM_ANON_NAME') {
          this.AnonymizeNameOfSponsorInvoice = true
        this.openGenerateConfirmModal(this.pdfView, this.currentInvoiceNum);
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.modalCreditNoteReference) {
      this.modalCreditNoteReference.close();
    }
    if (this.modalReference) {
      this.modalReference.close();
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    params.api.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  getRowColors(invoice) {
    if (invoice.InvoiceType === 'CN') {
      return 'rgb(255, 255, 197)';
    }
  }

  cellClicked(event) {
    if (event.column.colDef.field === 'Innhold') {
      if (event.data.InvoiceType === 'SP') {
        this.isSponsoredInvoice = true;
        this.currentInvoiceNum = event.data.InvoiceNo
        this.currentARItem = event.data.Id
    this.anonConfirm = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'COMMON.Confirm',
        messageBody: 'COMMON.AnonBody',
        msgBoxId: 'CONFIRM_ANON_NAME'
      }
    )
      }else {
      this.invoiceContent(event.data, this.pdfView);
      this.isSponsoredInvoice = false;
    }
    } else if (event.column.colDef.field === 'Detaljer') {
      if (event.data.InvoiceType === 'CN') {
        this.viewInvoiceDetails(event);
      } else {
        this.viewInvoiceDetails(event);
      }
    }
  }

  setMemberDetails(member) {
    this.memberId = member.Id;
    this.customerNo = member.CustId;
    this.customerName = member.Name;
    if (member.Id !== member.GuardianId && member.GuardianId > 0) {
      this.guardianId = member.GuardianId;
      this.email = member.GuardianEmail;
      this.smsNo = member.GuardianMobile;
    } else {
      this.email = member.Email;
      this.smsNo = member.Mobile;
    }
  }

  getMemberInvoice(memberId, hit) {
    const invoiceList = []
    this.exceMemberService.GetMemberInvoices(memberId, hit).pipe(takeUntil(this.destroy$)).subscribe(result => {
      result.Data.forEach(invoice => {
        invoiceList.push(invoice)
        invoice.DebtCollectionStatus = this.setDebtCollectionStatus(invoice.DebtCollectionStatus);
        this.setInvoiceStatus(invoice);
        this.rowColors = this.getRowColors.bind(this)
      });

      invoiceList.forEach(invoice => {

        if (invoice.InvoiceType === 'CN') {
          const x = invoice.InvoiceNo.split('-');
          invoiceList.forEach(inv => {
            if ((x[0] === inv.ReferencedInvoiceNo) && (invoice.Id !== inv.Id) && (inv.InvoiceBalance === 0)) {
              let invoiceStatus = ''
              this.translateService.get('MEMBERSHIP.Credited').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue);
              inv.InvoiceStatus = invoiceStatus;
            }
          });
        }
      });
      const tempList = [];

      invoiceList.forEach( invo => {
        const d = invo.InvoiceDate.split('T');
        const dDate = d[0].split('-');
        invo.DisplayDate = dDate[2] + '.' + dDate[1] + '.' + dDate[0];

        if (invo.InvoiceDueDate !== null) {
          const dued = invo.InvoiceDueDate.split('T');
          const duedDate = dued[0].split('-');
          invo.DisplayDueDate = duedDate[2] + '.' + duedDate[1] + '.' + duedDate[0];
        }

        const balance = invo.InvoiceBalance;
        invo.DisplayBalance = balance.toFixed(2);

        const total = invo.InvoiceAmount;
        invo.DisplayTotal = total.toFixed(2);

        let invoiceStatus;
        this.translateService.get('MEMBERSHIP.' + invo.InvoiceType).pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue);
        invo.InvoiceTypeTitle = invoiceStatus;

        if (invo.IsFullyPaid) {
          invo.InvoiceStatusNo = 0;
        } else if (invo.IsNotPaid) {
          invo.InvoiceStatusNo = 1;
        } else if (invo.IsPartiallyPaid) {
          invo.InvoiceStatusNo = 2;
        }

        tempList.push(invo);
      })
      this.invoicesSummeries = tempList;

      if (this.isRoutedFromMemberList) {
        this.invoicesSummeries.forEach(inv => {
          if (inv.InvoiceNo === this.selectedMember.InvoiceNo) {
            const event = {data: {}};
            event.data = inv;
            this.viewInvoiceDetails(event);
          }
        });
        this.isRoutedFromMemberList = false;
      }
    });


  }

  setOffset(): number {
    return this.offset;
  }

  invoiceContent(item, content) {
    this.openGenerateConfirmModal(content, item.InvoiceNo);

    // Lanka code not doing as intendent
    // if (item !== null) {
    //   if (item.InvoiceType === 'CN') {
    //     PreloaderService.showPreLoader();
    //     this.exceMemberService.ViewCreditNoteContent(item.Id).pipe(takeUntil(this.destroy$)).subscribe(result => {
    //       PreloaderService.hidePreLoader();
    //       if (result.Data.FileParth.trim() !== '') {
    //         this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
    //         item.PDFFileParth = result.Data.FileParth;
    //       }
    //     });
    //   } else {
    //     PreloaderService.showPreLoader();
    //     this.exceMemberService.ViewInvoiceDetails(item.Id).pipe(takeUntil(this.destroy$)).subscribe(result => {
    //       PreloaderService.hidePreLoader();
    //       if (result.Data.FileParth.trim() !== '') {
    //         this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
    //         item.PDFFileParth = result.Data.FileParth;
    //       }
    //     });
    //   }
    // }

   // const originFilepath = 'location.origin'
    // var originFilepath = 'http://dev.exceline.net/PDFViewer/'
   // let folderPath = item.PDFFileParth
    // ***indexing and foramting path***
   // const i = folderPath.search(/PDF/i) + 4
   // folderPath = folderPath.substring(i)
    //folderPath = folderPath.replace(/\\/g, '/')
    //const viewPdfPath = originFilepath + folderPath
    // console.log(viewPdfPath)

    // **View PDF**
    //this.excePdfViewerService.openPdfViewer('Exceline', viewPdfPath)


  }

  viewInvoiceDetails(event) {
    const selectedInvoice = event.data;

    if (selectedInvoice.IsFullyPaid === true) {
      this.IsCNBtnEnable = false;
    }
    if (selectedInvoice.InvoiceType !== 'CN') {
      this.selectedDetail.ArItemNo = selectedInvoice.Id;
      let isFullyPaid = false;
      let isSponsorInvoice = false;
      if (selectedInvoice.Id > 0) {
        isFullyPaid = selectedInvoice.IsFullyPaid;
        isSponsorInvoice = selectedInvoice.IsSponsorInvoice;
      }

      this.selectedDetail.IsFullyPaid = isFullyPaid;
      this.selectedDetail.IsSponsorInvoice = isSponsorInvoice;
      this.selectedDetail.MemberId = this.memberId;
      this.selectedDetail.IsShopInvoice = selectedInvoice.IsShopInvoice;
      this.selectedDetail.PayDirection = selectedInvoice.PayDirection;
      this.selectedDetail.GuardianId = this.guardianId;
      this.selectedDetail.Email = this.email;
      this.selectedDetail.SmsNo = this.smsNo;
      this.selectedDetail.IsBrisIntegrated = this.isBrisIntegrated;
      this.modalReference = this.modalService.open(this.invoiceDetails);
    } else {
      this.exceMemberService.GetCreditNoteDetails(selectedInvoice.Id).pipe(takeUntil(this.destroy$)).subscribe(result => {
        this.selectedCreditNote = result.Data;
        this.selectedCreditNote.Amount = selectedInvoice.InvoiceAmount;
        this.selectedCreditNote.Balance = selectedInvoice.InvoiceBalance;
        this.selectedCreditNote.InvoiceNo = selectedInvoice.InvoiceNo;
        this.selectedCreditNote.Id = selectedInvoice.Id;
        this.selectedCreditNote.CustomerNo = this.customerNo;
        this.selectedCreditNote.IsAddView = false;
        this.modalCreditNoteReference = this.modalService.open(this.creditNote);
      });
    }

  }

  closeInvoiceDetailModel() {
    this.modalReference.close();
    this.getMemberInvoice(this.selectedMember.Id, this.hit)
  }

  closeCreditNoteModal() {
    this.modalCreditNoteReference.close();
    this.getMemberInvoice(this.selectedMember.Id, this.hit)
  }

  setDebtCollectionStatus(debtColStatus) {
    let status;
    switch (debtColStatus) {
      case 'DW':
        this.translateService.get('MEMBERSHIP.INVDW').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => status = tranlstedValue)
        return status;
      case 'PPD':
        this.translateService.get('MEMBERSHIP.INVPartlyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => status = tranlstedValue)
        return status;
      case 'CPD':
        this.translateService.get('MEMBERSHIP.INVFullyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => status = tranlstedValue)
        return status;
      case 'NPD':
        this.translateService.get('MEMBERSHIP.INVNotPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => status = tranlstedValue)
        return status;
      default:
        this.translateService.get('MEMBERSHIP.INVNotApplicable').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => status = tranlstedValue)
        return status;
    }
  }

  setInvoiceStatus(invoice) {
    let invoiceStatus;
    if (invoice.InvoiceType === 'CN') {
      invoice.InvoiceNo = invoice.InvoiceNo + '-' + invoice.MembeInvoiceId;

      if (invoice.InvoiceBalance === invoice.InvoiceAmount * -1) {
        this.translateService.get('MEMBERSHIP.INVNotPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
        invoice.InvoiceStatus = invoiceStatus;
        invoice.IsNotPaid = true;
      } else if (invoice.InvoiceBalance === 0) {
        this.translateService.get('MEMBERSHIP.INVFullyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
        invoice.InvoiceStatus = invoiceStatus;
        invoice.IsFullyPaid = true;
      } else if (invoice.InvoiceBalance > 0) {
        this.translateService.get('MEMBERSHIP.INVPartlyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
        invoice.InvoiceStatus = invoiceStatus;
        invoice.IsPartiallyPaid = true;
      }
    } else {
      if (invoice.InvoiceBalance === invoice.InvoiceAmount && invoice.InvoiceAmount !== 0) {
        if (invoice.RemindCollStatus === 'DW') {
          this.translateService.get('MEMBERSHIP.INVStatusDW').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          invoice.InvoiceStatus = invoiceStatus;
        } else if (invoice.RemindCollStatus === 'REM') {
          this.translateService.get('MEMBERSHIP.INVStatusReminder').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          invoice.InvoiceStatus = invoiceStatus;
        } else if (invoice.RemindCollStatus === 'SREM') {
          this.translateService.get('MEMBERSHIP.INVDSmsReminder').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          invoice.InvoiceStatus = invoiceStatus;
        } else {
          this.translateService.get('MEMBERSHIP.INVNotPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          invoice.InvoiceStatus = invoiceStatus;
        }
        invoice.IsNotPaid = true;
      } else if (invoice.InvoiceBalance <= 0) {
        if (invoice.PaymentTypes !== '') {
          const payments = invoice.PaymentTypes.split(','); // invoice.PaymentTypes.Trim(',').Split(new char[] { ',' });
          if (payments.Length > 1) {
            this.translateService.get('MEMBERSHIP.INVStatusDW').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
            invoice.InvoiceStatus = invoiceStatus + '++';
          } else {
            this.translateService.get('MEMBERSHIP.INVFullyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
            invoice.InvoiceStatus = invoiceStatus + ' [' + this.localizePayment(payments[0].trim()) + ']';
          }
        } else {
          this.translateService.get('MEMBERSHIP.INVFullyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          invoice.InvoiceStatus = invoiceStatus;
        }
        invoice.IsFullyPaid = true;
      } else if (invoice.InvoiceBalance < invoice.InvoiceAmount && invoice.InvoiceBalance > 0) {
        this.translateService.get('MEMBERSHIP.INVPartlyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
        invoice.InvoiceStatus = invoiceStatus;
        invoice.IsPartiallyPaid = true;
      }
    }
  }

  localizePayment(type) {
    let typeMsg;
    switch (type) {
      case 'OP':
        this.translateService.get('MEMBERSHIP.OCR').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'DC':
        this.translateService.get('MEMBERSHIP.PtDC').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'TMN':
        this.translateService.get('MEMBERSHIP.PtTMN').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'CSH':
        this.translateService.get('MEMBERSHIP.PtCSH').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'BS':
        this.translateService.get('MEMBERSHIP.PtBS').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'OA':
        this.translateService.get('MEMBERSHIP.PtOA').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'GC':
        this.translateService.get('MEMBERSHIP.PtGC').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'LOP':
        this.translateService.get('MEMBERSHIP.OCR').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'NOR':
        this.translateService.get('MEMBERSHIP.NOR').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'RD':
        this.translateService.get('MEMBERSHIP.RD').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'BNK':
        this.translateService.get('MEMBERSHIP.PtBank').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 'PB':
        this.translateService.get('MEMBERSHIP.PrepaidBalance').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      default:
        return '';
    }
  }

  printfunc(invoiceno: any) {
    this.currentInvoice = invoiceno;
    this.economyService.GetInvoice(invoiceno, this.isBrisIntegrated, this.selectedMember.BranchId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        const contentType = 'application/pdf',
        b64Data = result.Data;
        this.blob = this.b64toBlob(b64Data, contentType);
        this.blobUrl = URL.createObjectURL(this.blob);
      }
    });
  }


  b64toBlob = (b64Data, contentType= '', sliceSize= 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize),
        byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

const blob = new Blob(byteArrays, {type: contentType});
return blob;
}

openGenerateConfirmModal(content: any, invoicenum: any) {
  this.printfunc(invoicenum);
  this.generatePdfView = this.modalService.open(content, {width: '800'});
 }



PrintPDF() {
  const iframe = document.createElement('iframe');
  iframe.style.display = 'none';
  iframe.src = this.blobUrl;
  document.body.appendChild(iframe);
  iframe.contentWindow.print();
}

SavePDF() {
  saveAs(this.blob, this.currentInvoice + '.pdf');
}



}
