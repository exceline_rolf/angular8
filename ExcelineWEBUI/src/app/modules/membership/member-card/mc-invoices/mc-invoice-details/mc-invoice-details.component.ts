import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { ExceMemberService } from '../../../services/exce-member.service';
import { PreloaderService } from '../../../../../shared/services/preloader.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IMyDpOptions } from '../../../../../shared/components/us-date-picker/interfaces';
import { TranslateService } from '@ngx-translate/core';
import { ExceToolbarService } from '../../../../common/exce-toolbar/exce-toolbar.service';
import { ExceLoginService } from '../../../../login/exce-login/exce-login.service';
import { McBasicInfoService } from '../../mc-basic-info/mc-basic-info.service';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { UsbModalRef } from '../../../../../shared/components/us-modal/modal-ref';
import { UsbModal } from '../../../../../shared/components/us-modal/us-modal';
import { ExcePdfViewerService } from '../../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { NotificationMethodType, TextTemplateEnum } from '../../../../../shared/enums/us-enum';
import { Dictionary } from 'lodash';
import { saveAs } from 'file-saver/FileSaver';
import { ShopHomeService } from '../../../../shop/shop-home/shop-home.service';
import { ExceShopService } from '../../../../shop/services/exce-shop.service';
import { ExceGymSetting } from '../../../../../shared/SystemObjects/Common/ExceGymSetting';
import { AdminService } from '../../../../admin/services/admin.service';
import { ExceEconomyService } from '../../../../economy/services/exce-economy.service';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'invoice-details-view',
  templateUrl: './mc-invoice-details.component.html',
  styleUrls: ['./mc-invoice-details.component.scss']
})
export class McInvoiceDetailsComponent implements OnInit, OnDestroy {

  private exceGymSetting: ExceGymSetting = new ExceGymSetting();
  private shopLoginData?: any = {};
  shopModalReference: UsbModalRef;
  @Input() selectedDetail: any;
  @Input() CNBtnDisabled: any;
  @Output() closeInvoiceDetailModel = new EventEmitter();
  public removePaymentForm: FormGroup;
  xtraCategoryList: any;
  selectedCategory: any;
  invoiceForm: FormGroup;
  invoiceDetailsForm: FormGroup;
  selectedInvoice: any = {};
  IsInvoiceFeeEnabled: boolean;
  canPaye = true;
  enableButtons = true;
  brisIntegration = false;
  private branchId: number;
  private isViewEnabled: boolean;
  modalCreditNoteReference: UsbModalRef;
  AnonymizeNameOfSponsorInvoice = false;
  private destroy$ = new Subject<void>();
  private selectedArticles: any;
  private selectedAritemNo: number;
  private aritemNo: number;
  public isSponsorInvoice: boolean;
  private memberId: number;
  public paymentDirection: number;
  private guardianId: number;
  private email: string;
  private smsNo: string;
  private isBrisIntegrated: boolean;

  private paymentConfirmViewcontent: any;
  private paymentConfirmModel: any;
  private paymentRemoveModelRef: any;
  private deletedPayment: any;

  private creditNoteModelContent: any;

  deleteType = 1;
  comment: string;
  paymentId: number;

  blobUrl: any;
  blob: any;
  currentInvoice: any;
  generatePdfView: UsbModalRef;

  @ViewChild('shopHomeModel', { static: true }) shopHomeModel: ElementRef;
  @ViewChild('Pdfwindow', { static: true }) Pdfwindow: ElementRef;

  private smsEmailInvoiceForm: FormGroup;
  private smsChecked: boolean;
  private emailChecked: boolean;
  private smsTemplate: any;
  private emailTemplate: any;
  private smsContent: any;
  private originalSmsContent: any;
  private emailContent: any;
  private originalEmailContent: any;
  private selectedSmsTemplate: any;
  private selectedEmailTemplate: any;
  private smsTemplateList: any;
  private emailTemplateList: any;

  private selectedMember: any;
  private selectedGymsettings = [
    'EcoNegativeOnAccount',
    'EcoSMSInvoiceShop',
    'EcoIsPrintInvoiceInShop',
    'EcoIsPrintReceiptInShop',
    'EcoIsShopOnNextOrder',
    'EcoIsOnAccount',
    'EcoIsDefaultCustomerAvailable',
    'EcoIsPayButtonsAvailable',
    'EcoIsPettyCashSameAsUserAllowed',
    'OtherIsShopAvailable',
    'OtherLoginShop',
    'HwIsShopLoginWithCard',
    'ReqMemberListRequired',
    'OtherIsLogoutFromShopAfterSale',
    'EcoIsBankTerminalIntegrated',
    'EcoIsDailySettlementForAll',
    'OtherIsValidateShopGymId'
  ];

  public fromDatePickerOptions: IMyDpOptions = {
    // disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() },
  };
  constructor(private exceMemberService: ExceMemberService,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private loginservice: ExceLoginService,
    private basicInfoService: McBasicInfoService,
    private exceMessageService: ExceMessageService,
    private modalService: UsbModal,
    private sanitizer: DomSanitizer,
    private economyService: ExceEconomyService,
    private excePdfViewerService: ExcePdfViewerService,
    private shopHomeService: ShopHomeService,
    private shopService: ExceShopService,
    private adminService: AdminService) { }

  ngOnInit() {
    this.removePaymentForm = this.fb.group({
      'comment': [this.comment],
      'deleteType': [this.deleteType],
      'paymentId': [this.paymentId]
    });

    this.smsEmailInvoiceForm = this.fb.group({
      'isSmsChecked': [this.smsChecked],
      'isEmailChecked': [this.emailChecked],
      'smsNo': [{ disabled: false, value: this.smsNo }, [Validators.pattern('^[0-9 ]*$')]], // { value: null, disabled: true }
      'selectedSmsTemplate': [this.selectedSmsTemplate],
      'smsContent': [this.smsContent],
      'emailAddress': [{ disabled: false, value: this.email }, [Validators.email]],
      'selectedEmailTemplate': [this.selectedEmailTemplate],
      'emailContent': [this.emailContent],
    });

    this.branchId = this.loginservice.SelectedBranch.BranchId;
    this.aritemNo = this.selectedDetail.ArItemNo;
    this.memberId = this.selectedDetail.MemberId;
    this.guardianId = this.selectedDetail.GuardianId;
    this.email = this.selectedDetail.Email;
    this.smsNo = this.selectedDetail.SmsNo;
    this.isBrisIntegrated = this.selectedDetail.IsBrisIntegrated;
    this.paymentDirection = this.selectedDetail.PayDirection;
    this.isSponsorInvoice = this.selectedDetail.IsSponsorInvoice;
    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.brisIntegration = member.IsBrisIntegrated;
          this.selectedMember = member;
        }
      },
      error => {
        // console.log(error);
      });
    this.invoiceForm = this.fb.group({
      'ActivityPrice': [{ value: null, disabled: true }],
      'AdonPrice': [{ value: null, disabled: true }],
      'TotalPrice': [{ value: null, disabled: true }],
      'InvoiceDueDate': [],
      'KID': [{ value: null, disabled: true }],
      'DiscountAmount': [{ value: null, disabled: true }],
      'SponsoredAmount': [{ value: null, disabled: true }],
      'InvoiceAmount': [{ value: null, disabled: true }],
      'PartiallyPaidAmount': [{ value: null, disabled: true }]
    });

    this.invoiceDetailsForm = this.fb.group({
      'TrainingStartDate': [{ value: null, disabled: true }],
      'TrainingEnddate': [{ value: null, disabled: true }],
      'TemplateName': [{ value: null, disabled: true }],
      'InvoiceDate': [{ value: null, disabled: true }],
      'OriginalDueDate': [{ value: null, disabled: true }],
      'InvoicePrintDate': [{ value: null, disabled: true }],
      'SmsEmailInvoiceDate': [{ value: null, disabled: true }],
      'FullyPaidDate': [{ value: null, disabled: true }],
      'BranchName': [{ value: null, disabled: true }],
      'OriginalCustomer': [{ value: null, disabled: true }],
      'SmsReminderDate': [{ value: null, disabled: true }],
      'NoOfReminders': [{ value: null, disabled: true }],
      'LastReminderPrintedDate': [{ value: null, disabled: true }],
      'DebtWarningSendDate': [{ value: null, disabled: true }],
      'IsSPInvoice': [{ value: null, disabled: true }],
      'IsATG': [{ value: null, disabled: true }],
      'DeleteRequest': [{ value: null, disabled: true }],
      'SendingNo': [{ value: null, disabled: true }],
      'SendingDate': [{ value: null, disabled: true }],
      'AtgInvoiceStatus': [{ value: null, disabled: true }],
      'AddOnText': [{ value: null, disabled: true }],
      'Comment': [],
      'XtraCategory': [{value: null, disabled: true}]
    });



    //  this.exceMemberService.getSelectedGymSettings(this.selectedGymsettings).pipe(takeUntil(this.destroy$)).subscribe(result => {
    //   // PreloaderService.hidePreLoader();
    //  });

    // this.yesHandlerSubscription = this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
    //   if (value.id === 'CONFIRM_SENT_TO_CERDIT_NOTE') {
    //     this.transferedCreditNoteConfirm();
    //   }
    // });

    this.exceMessageService.noCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      if (value.id === 'CONFIRM_ANON_NAME_DETAILS') {
        this.AnonymizeNameOfSponsorInvoice = false;
        this.openGenerateConfirmModal(this.Pdfwindow, this.selectedInvoice.InvoiceNo);
      }
    })

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      if (value.id === 'CONFIRM_DELETE_PAYMENT') {
        this.deletePaymentConfirm();
      } else if (value.id === 'CONFIRM_SENT_TO_CERDIT_NOTE') {
        this.transferedCreditNoteConfirm();
      } else if (value.id === 'CONFIRM_ANON_NAME_DETAILS') {
        this.AnonymizeNameOfSponsorInvoice = true
        this.openGenerateConfirmModal(this.Pdfwindow, this.selectedInvoice.InvoiceNo);
    }});

    this.shopHomeService.savePaymentFromOrdersEvent.pipe(takeUntil(this.destroy$)).subscribe(
      val => {
        if (val === 0) {
          this.closeShop();
        } else if (val === 1) {
          this.shopHomeService.closeShopEvent.next(true);
        }
      }
    )
    this.GetInvoiceDetails();
  }

  GetInvoiceDetails() {
    this.exceMemberService.GetInvoiceDetails(this.selectedDetail.ArItemNo).pipe(takeUntil(this.destroy$)).subscribe(result => {
      // PreloaderService.hidePreLoader();
      this.selectedInvoice = result.Data;
      this.selectedInvoice.InvoiceTypeFormated = this.invoiceTypeConverter(this.selectedInvoice.InvoiceType);
      this.setInvoiceStatus();
      // if (this.selectedInvoice.InvoiceBalance === 0 ) {
      //   this.canPaye = false;
      // }
      // if (this.selectedInvoice.InvoiceBalance === 0 || (this.selectedInvoice.GymID !== this.branchId && !this.brisIntegration)) {
      if (this.selectedInvoice.InvoiceBalance === 0 || this.selectedInvoice.GymID !== this.branchId) {
        this.canPaye = false;
      }
      if (this.selectedInvoice.GymID !== this.branchId && !this.brisIntegration) {
        this.enableButtons = false;
      }

      const invoiceDueDate = new Date(this.selectedInvoice.InvoiceDueDate);
      this.selectedInvoice.DueDateFormated = { year: invoiceDueDate.getFullYear(), month: invoiceDueDate.getMonth() + 1, day: invoiceDueDate.getDate() };

      this.invoiceForm = this.fb.group({
        ActivityPrice: [{ disabled: true, value: this.selectedInvoice.BasicDetails.ActivityPrice }],
        AdonPrice: [{ disabled: true, value: this.selectedInvoice.BasicDetails.AdonPrice }],
        TotalPrice: [{ disabled: true, value: this.selectedInvoice.BasicDetails.TotalPrice }],
        InvoiceDueDate: [{ date: this.selectedInvoice.DueDateFormated }],
        KID: [{ disabled: true, value: this.selectedInvoice.BasicDetails.KID }],
        DiscountAmount: [{ disabled: true, value: this.selectedInvoice.BasicDetails.DiscountAmount }],
        SponsoredAmount: [{ disabled: true, value: this.selectedInvoice.BasicDetails.SponsoredAmount }],
        InvoiceAmount: [{ disabled: true, value: this.selectedInvoice.BasicDetails.InvoiceAmount }],
        PartiallyPaidAmount: [{ disabled: true, value: this.selectedInvoice.BasicDetails.PartiallyPaidAmount }]
      });
      this.selectedInvoice.OtherDetails.TrainingStartDate = this.getDisplayDate(this.selectedInvoice.OtherDetails.TrainingStartDate);
      this.selectedInvoice.OtherDetails.TrainingEnddate = this.getDisplayDate(this.selectedInvoice.OtherDetails.TrainingEnddate);
      this.selectedInvoice.OtherDetails.InvoiceDate = this.getDisplayDate(this.selectedInvoice.OtherDetails.InvoiceDate);
      this.selectedInvoice.OtherDetails.OriginalDueDate = this.getDisplayDate(this.selectedInvoice.OtherDetails.OriginalDueDate);
      this.selectedInvoice.OtherDetails.InvoicePrintDate = this.getDisplayDate(this.selectedInvoice.OtherDetails.InvoicePrintDate);
      this.selectedInvoice.OtherDetails.SmsEmailInvoiceDate = this.getDisplayDate(this.selectedInvoice.OtherDetails.SmsEmailInvoiceDate);
      this.selectedInvoice.OtherDetails.FullyPaidDate = this.getDisplayDate(this.selectedInvoice.OtherDetails.FullyPaidDate);
      this.selectedInvoice.OtherDetails.SmsReminderDate = this.getDisplayDate(this.selectedInvoice.OtherDetails.SmsReminderDate);
      this.selectedInvoice.OtherDetails.LastReminderPrintedDate = this.getDisplayDate(this.selectedInvoice.OtherDetails.LastReminderPrintedDate);
      this.selectedInvoice.OtherDetails.DebtWarningSendDate = this.getDisplayDate(this.selectedInvoice.OtherDetails.DebtWarningSendDate);
      this.selectedInvoice.OtherDetails.SendingDate = this.getDisplayDate(this.selectedInvoice.OtherDetails.SendingDate);



      this.invoiceDetailsForm = this.fb.group({
        TrainingStartDate: [{ disabled: true, value: { date: this.selectedInvoice.OtherDetails.TrainingStartDate} }],
        TrainingEnddate: [{  disabled: true, value: { date: this.selectedInvoice.OtherDetails.TrainingEnddate } }],
        TemplateName: [{ disabled: true, value: this.selectedInvoice.OtherDetails.TemplateName }],
        InvoiceDate: [{ disabled: true, value: { date: this.selectedInvoice.OtherDetails.InvoiceDate } }],
        OriginalDueDate: [{ disabled: true, value: { date: this.selectedInvoice.OtherDetails.OriginalDueDate } }],
        InvoicePrintDate: [{ disabled: true, value: { date: this.selectedInvoice.OtherDetails.InvoicePrintDate } }],
        SmsEmailInvoiceDate: [{ disabled: true, value: { date: this.selectedInvoice.OtherDetails.SmsEmailInvoiceDate } }],
        FullyPaidDate: [{ disabled: true, value: { date: this.selectedInvoice.OtherDetails.FullyPaidDate } }],
        BranchName: [{ disabled: true, value: this.selectedInvoice.OtherDetails.BranchName }],
        OriginalCustomer: [{ disabled: true, value: this.selectedInvoice.OtherDetails.OriginalCustomer }],
        SmsReminderDate: [{ disabled: true, value: { date: this.selectedInvoice.OtherDetails.SmsReminderDate } }],
        NoOfReminders: [{ disabled: true, value: this.selectedInvoice.OtherDetails.NoOfReminders }],
        LastReminderPrintedDate: [{ disabled: true, value: { date: this.selectedInvoice.OtherDetails.LastReminderPrintedDate } }],
        DebtWarningSendDate: [{ disabled: true, value: { date: this.selectedInvoice.OtherDetails.DebtWarningSendDate } }],
        IsSPInvoice: [{ disabled: true, value: this.selectedInvoice.OtherDetails.IsSPInvoice }],
        IsATG: [{ disabled: true, value: this.selectedInvoice.OtherDetails.IsATG }],
        DeleteRequest: [{ disabled: true, value: this.selectedInvoice.OtherDetails.DeleteRequest }],
        SendingNo: [{ disabled: true, value: this.selectedInvoice.OtherDetails.SendingNo }],
        SendingDate: [{ disabled: true, value: { date: this.selectedInvoice.OtherDetails.SendingDate } }],
        AtgInvoiceStatus: [{ disabled: true, value: this.atgStatusConverter(this.selectedInvoice.OtherDetails.AtgInvoiceStatus) }],
        AddOnText: [{ disabled: true, value: this.selectedInvoice.OtherDetails.AddOnText }],
        Comment: [this.selectedInvoice.OtherDetails.Comment],
        XtraCategory: [{ disabled: true, value: this.selectedCategory }]
      });

      this.exceMemberService.getCategories('XTRACAT').pipe(takeUntil(this.destroy$)).subscribe(res => {
        this.xtraCategoryList = res.Data;
        this.xtraCategoryList.forEach(element => {
          if (element.Code === this.selectedInvoice.OtherDetails.CollectingStatus) {
            this.selectedCategory = element.Name
          }else {this.selectedCategory = ''}
        });
      });
    });

  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.shopModalReference) {
      this.shopHomeService.$shopMode = '';
      this.shopModalReference.close();
    }
    if (this.modalCreditNoteReference) {
      this.modalCreditNoteReference.close();
    }
    if (this.paymentRemoveModelRef) {
      this.paymentRemoveModelRef.close();
    }
  }

  invoiceTypeConverter(typeId) {
    let typeMsg = '';
    switch (typeId) {
      case 1:
        this.translateService.get('MEMBERSHIP.ATG').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 501:
        this.translateService.get('MEMBERSHIP.sms').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 502:
        this.translateService.get('MEMBERSHIP.EmailS').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 500:
        this.translateService.get('MEMBERSHIP.Paper').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 2:
        this.translateService.get('MEMBERSHIP.IP').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 3:
        this.translateService.get('MEMBERSHIP.IN').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 7:
        this.translateService.get('MEMBERSHIP.SP').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 14:
        this.translateService.get('MEMBERSHIP.PP').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      case 101:
        this.translateService.get('MEMBERSHIP.Shop').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      default:
        return '';
    }
  }

  atgStatusConverter(val) {
    let atgStatus = '';
    val = Number(val);
    if (val !== NaN) {
      switch (val) {
        case 1:
          this.translateService.get('MEMBERSHIP.New').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => atgStatus = tranlstedValue)
          return atgStatus;

        case 2:
          this.translateService.get('MEMBERSHIP.NoATG').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => atgStatus = tranlstedValue)
          return atgStatus;

        case 5:
          this.translateService.get('MEMBERSHIP.Deleted').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => atgStatus = tranlstedValue)
          return atgStatus;

        case 6:
          this.translateService.get('MEMBERSHIP.Approved').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => atgStatus = tranlstedValue)
          return atgStatus;

        default:
          this.translateService.get('MEMBERSHIP.New').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => atgStatus = tranlstedValue)
          return atgStatus;
      }
    }
    return '';
  }

  setInvoiceStatus() {
    let invoiceStatus;
    if (this.selectedInvoice.InvoiceStatus === 'NP') {
      this.translateService.get('MEMBERSHIP.INVNotPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
      this.selectedInvoice.InvoiceStatus = invoiceStatus;
    } else if (this.selectedInvoice.InvoiceStatus === 'PP') {
      this.translateService.get('MEMBERSHIP.INVDPartiallyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
      this.selectedInvoice.InvoiceStatus = invoiceStatus;
    } else if (this.selectedInvoice.InvoiceStatus === 'FP') {
      let ptypes = '';
      const paymentList = this.selectedInvoice.OtherDetails.Paymentlist
      let i = 0
      for (i = 0; i < paymentList.length; i++) {
        const ptype = paymentList[i].Type.trim();
        if (ptypes.indexOf(ptype) === -1) {
          ptypes += paymentList[i].Type + ',';
        }
      }
      // this.selectedInvoice.OtherDetails.Paymentlist.forEach(element => {
      //   if (ptypes.indexOf(element.Type) !== -1) {
      //     ptypes += element.Type + ',';
      //   }
      // });
      if (ptypes !== '') {
        const payments = ptypes.slice(0, -1).trim().split(',');
        if (payments.length > 1) {
          this.translateService.get('MEMBERSHIP.INVFullyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          this.selectedInvoice.InvoiceStatus = invoiceStatus + '++';
        } else {
          this.translateService.get('MEMBERSHIP.INVFullyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
          this.selectedInvoice.InvoiceStatus = invoiceStatus + ' [' + this.localizePayment(payments[0].trim()) + ']';
        }
      } else {
        this.translateService.get('MEMBERSHIP.INVFullyPaid').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => invoiceStatus = tranlstedValue)
        this.selectedInvoice.InvoiceStatus = invoiceStatus;
      }
    } else {
      this.selectedInvoice.InvoiceStatus = '';
    }
  }
  localizePayment(type) {
    let typeMsg;
    switch (type) {
      case 'OP':
        this.translateService.get('MEMBERSHIP.OCR').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      case 'DC':
        this.translateService.get('MEMBERSHIP.PtDC').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      case 'TMN':
        this.translateService.get('MEMBERSHIP.PtTMN').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      case 'CSH':
        this.translateService.get('MEMBERSHIP.PtCSH').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      case 'BS':
        this.translateService.get('MEMBERSHIP.PtBS').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      case 'OA':
        this.translateService.get('MEMBERSHIP.PtOA').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      case 'GC':
        this.translateService.get('MEMBERSHIP.PtGC').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      case 'LOP':
        this.translateService.get('MEMBERSHIP.OCR').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      case 'NOR':
        this.translateService.get('MEMBERSHIP.NOR').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      case 'RD':
        this.translateService.get('MEMBERSHIP.RD').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      case 'BNK':
        this.translateService.get('MEMBERSHIP.PtBank').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;
      case 'PB':
        this.translateService.get('MEMBERSHIP.PrepaidBalance').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => typeMsg = tranlstedValue)
        return typeMsg;

      default:
        return '';
    }
  }

  getDisplayDate(val) {
    val = new Date(val);
    let result;
    if ((val.getFullYear() === 1970 || val.getFullYear() === 1900) && val.getMonth() === 0 &&
      val.getDate() === 1) {
      result = null;
    } else {
      result = {
        year: val.getFullYear(),
        month: val.getMonth() + 1,
        day: val.getDate()
      };
    }
    return result;
  }

  saveInvoice() {
    const invoiceFormVal = this.invoiceForm.getRawValue();
    const invoiceDetailsFormVal = this.invoiceDetailsForm.getRawValue();
    const updateInvoice = {
      ArItemNo: this.selectedDetail.ArItemNo, Comment: invoiceDetailsFormVal.Comment,
      DueDate: invoiceFormVal.InvoiceDueDate.date.year + '-' + invoiceFormVal.InvoiceDueDate.date.month + '-' + invoiceFormVal.InvoiceDueDate.date.day
    }
    PreloaderService.showPreLoader();
    this.exceMemberService.UpdateInvoice(updateInvoice).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data === true) {
        const originalDueDate = this.selectedInvoice.DueDateFormated.year + '-' +
        (this.selectedInvoice.DueDateFormated.month >= 10 ? this.selectedInvoice.DueDateFormated.month : '0' + this.selectedInvoice.DueDateFormated.month) + '-' +
        (this.selectedInvoice.DueDateFormated.day >= 10 ? this.selectedInvoice.DueDateFormated.day : '0' + this.selectedInvoice.DueDateFormated.day );
        const newDueDate = invoiceFormVal.InvoiceDueDate.date.year + '-' +
        (invoiceFormVal.InvoiceDueDate.date.month >= 10 ? invoiceFormVal.InvoiceDueDate.date.month : '0' + invoiceFormVal.InvoiceDueDate.date.month) + '-' +
        (invoiceFormVal.InvoiceDueDate.date.day >= 10 ? invoiceFormVal.InvoiceDueDate.date.day : '0' + invoiceFormVal.InvoiceDueDate.date.day);
        const duedatechangedata = {
          Title: 'DUEDATE ALTERED',
          Description: 'Duedate of invoice: ' + this.selectedInvoice.InvoiceNo + ' was changed from ' + originalDueDate + ' to ' + newDueDate,
          RoleId: 'MEM',
          TypeId: 4,
          SeverityId: 3,
          StatusId: 1,
          BranchId: this.selectedMember.BranchId,
          MemberId: this.memberId,
          IsReplySelected: 0,
          RecordType: 'INV'
        }
        if (originalDueDate !== newDueDate) {
          this.exceMemberService.registerChangeOfInvoiceDueDate(duedatechangedata).pipe(takeUntil(this.destroy$)).subscribe();
        }
      }

      PreloaderService.hidePreLoader();
    },
      err => {
        PreloaderService.hidePreLoader();
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.INVDUpdateError' });
      });
    this.cancelInvoice();
  }

  cancelInvoice() {
    this.closeInvoiceDetailModel.emit();
  }

  printReceipt() {
    PreloaderService.showPreLoader();
    // this.excePdfViewerService.openPdfViewer('Exceline', 'http://www.pdf995.com/samples/pdf.pdf');
    this.exceMemberService.printInvoicePaidReceipt(this.aritemNo).pipe(takeUntil(this.destroy$)).subscribe(result => {
      PreloaderService.hidePreLoader();
      if (result.Data.FileParth.trim() !== '') {
        // TO DO ee.IsPriview
        // this.excePdfViewerService.openPdfViewer('Exceline', 'http://www.pdf995.com/samples/pdf.pdf');
        this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
      }
    });
  }

  printSponsorInvoice() {
    const content = this.Pdfwindow
   // PreloaderService.showPreLoader();
   if (this.selectedInvoice.InvoiceTypeFormated === 'SP') {
    this.paymentConfirmModel = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'COMMON.Confirm',
        messageBody: 'COMMON.AnonBody',
        msgBoxId: 'CONFIRM_ANON_NAME_DETAILS'
      }
    )
   } else {
     this.openGenerateConfirmModal(content, this.selectedInvoice.InvoiceNo);
    }
  }

  printfunc(invoiceno: any) {
    this.currentInvoice = invoiceno;
    this.economyService.GetInvoice(invoiceno, this.isBrisIntegrated, this.selectedMember.BranchId).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
      if (result) {
      const contentType = 'application/pdf',
      b64Data = result.Data;
      this.blob = this.b64toBlob(b64Data, contentType);
      this.blobUrl = URL.createObjectURL(this.blob);
    }
    });
  }


  b64toBlob = (b64Data, contentType= '', sliceSize= 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize),
        byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

const blob = new Blob(byteArrays, {type: contentType});
return blob;
}

openGenerateConfirmModal(content: any, invoicenum: any) {
  this.printfunc(invoicenum);
  this.generatePdfView = this.modalService.open(content, {width: '800'});
 }



PrintPDF() {
  const iframe = document.createElement('iframe');
  iframe.style.display = 'none';
  iframe.src = this.blobUrl;
  document.body.appendChild(iframe);
  iframe.contentWindow.print();
}

SavePDF() {
saveAs(this.blob, this.currentInvoice + '.pdf');
}

  invoiceSMSEmail(content) {
    this.smsEmailInvoiceForm.reset();

    if (this.smsNo !== '') {
      this.smsChecked = true;
    } else {
      this.smsChecked = false;
    }
    if (this.email !== '') {
      this.emailChecked = true;
    } else {
      this.emailChecked = false;
    }
    this.getNotificationTemplateListByMethodId();
    // this.smsTemplate = this.SelectedSMSTemplate[0];
    // this.emailTemplate = this.SelectedEmailTemplate[0];
    // this.SMSContent = this.SelectedSMSTemplate[0].Text;
    // this.EmailContent = this.SelectedEmailTemplate[0].Text;
    this.modalService.open(content, { width: '900' });
  }

  changeSmsEvent(event) {
    if (event.target.checked) {
      this.smsContent = this.selectedSmsTemplate.Text;
    } else {
      this.smsContent = '';
    }
  }

  changeEmailEvent(event) {
    if (event.target.checked) {
      this.emailContent = this.selectedEmailTemplate.Text;
    } else {
      this.emailContent = '';
    }
  }

  getNotificationTemplateListByMethodId() {
    this.exceMemberService.getNotificationTemplateListByMethod(NotificationMethodType[NotificationMethodType.SMS],
      TextTemplateEnum[TextTemplateEnum.SMSINVOICE], this.branchId).pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          if (result) {
            this.smsTemplateList = result.Data;
            this.selectedSmsTemplate = this.smsTemplateList[0];
            this.smsContent = this.selectedSmsTemplate.Text;
          }
        }
      );

    // Get Email Template List
    this.exceMemberService.getNotificationTemplateListByMethod(NotificationMethodType[NotificationMethodType.EMAIL],
      TextTemplateEnum[TextTemplateEnum.EMAILINVOICE], this.branchId).pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          if (result) {
            this.emailTemplateList = result.Data;
            this.selectedEmailTemplate = this.emailTemplateList[0];
            this.emailContent = this.selectedEmailTemplate.Text;
          }
        });
  }

  sendSMSOrEmail(value) {
    const senderDetail: Dictionary<string> = {};
    const isSelected: Dictionary<boolean> = {};
    const text: Dictionary<string> = {};
    if (this.smsChecked || this.emailChecked) {
      if (this.smsChecked && value.smsContent !== '' && this.smsNo !== '') {
        isSelected['SMS'] = this.smsChecked;
        text['SMS'] = value.smsContent;
        senderDetail['SMS'] = this.smsNo;
      } else {
        isSelected['SMS'] = false;
      }

      if (this.emailChecked && value.emailContent !== '' && this.email !== '') {
        isSelected['EMAIL'] = this.emailChecked;
        text['EMAIL'] = value.emailContent;
        senderDetail['EMAIL'] = this.email;
      } else {
        isSelected['EMAIL'] = false;
      }

      // if (isSelected.length > 0 && text.length > 0 && senderDetail.length > 0) {
      const sendInvoiceSmsAndEmail = {
        ArItemNo: this.aritemNo,
        MemberId: this.memberId,
        GuardianId: this.guardianId,
        isSelected: isSelected,
        text: text,
        senderDetail: senderDetail,
        branchId: this.branchId
      }
      this.exceMemberService.sendInvoiceSMSANDEmail(sendInvoiceSmsAndEmail).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
        } else {
        }
      }
      );
    }
  }

  viewCreditNote(content) {
    this.selectedAritemNo = this.selectedInvoice.ArItemNO;
    this.selectedArticles = this.selectedInvoice.BasicDetails.ArticleList.map(x => Object.assign({}, x));
    let paidAmount = this.selectedInvoice.InvoiceAmount - this.selectedInvoice.InvoiceBalance;
    const creditedSum = this.selectedArticles.map(c => c.CreditedAmount)
      .reduce((sum, current) => sum + current);
    paidAmount = paidAmount - creditedSum;
    if (paidAmount > 0) {
      this.selectedArticles.forEach(article => {
        if (article.ArticleBalance > 0) {
          if (article.ArticleBalance < paidAmount) {
            paidAmount = paidAmount - article.ArticleBalance;
            article.ArticleBalance = 0;
          } else {
            article.ArticleBalance = article.ArticleBalance - paidAmount;
            paidAmount = 0;
            // break;
          }
        }
      });
    }

    if (this.selectedInvoice.Transfered) {
      let dateTxt = '';
      const sDate = this.selectedInvoice.OtherDetails.SendingDate;
      if (sDate !== null) {
        dateTxt = sDate.day + '.' + sDate.month + '.' + sDate.year;
      }
      this.creditNoteModelContent = content;
      let msgSenttoNetsOn;
      let msgProceed;
      this.translateService.get('MEMBERSHIP.MsgSenttoNetsOn').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => msgSenttoNetsOn = tranlstedValue)
      this.translateService.get('MEMBERSHIP.Proceed').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => msgProceed = tranlstedValue)
      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: msgSenttoNetsOn + dateTxt + msgProceed,
          msgBoxId: 'CONFIRM_SENT_TO_CERDIT_NOTE'
        });
    } else {
      this.modalCreditNoteReference = this.modalService.open(content);
    }
  }

  transferedCreditNoteConfirm() {
    if (this.selectedInvoice != null) {
      this.modalCreditNoteReference = this.modalService.open(this.creditNoteModelContent);
    }
  }
  successCreditNote(balance) {
    if (this.selectedInvoice != null) {
      this.selectedInvoice.InvoiceBalance = balance;
      this.modalCreditNoteReference.close();
      this.GetInvoiceDetails();
    }
  }

  closeCreditNoteModal() {
    this.modalCreditNoteReference.close();
  }

  paymentDeleteConfirmMsg(content: any, selectedItem: any) {
    this.paymentConfirmViewcontent = content;
    this.deletedPayment = selectedItem;
    this.paymentConfirmModel = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'COMMON.Confirm',
        messageBody: 'MEMBERSHIP.SureToRemovePayment',
        msgBoxId: 'CONFIRM_DELETE_PAYMENT'
      }
    );
  }

  deletePaymentConfirm() {
    this.openCategoryModel(this.paymentConfirmViewcontent);
  }

  openCategoryModel(content: any) {
    this.paymentRemoveModelRef = this.modalService.open(content, { size: 'lg', windowClass: 'vatModal-size' });
  }

  deletePayment(value) {

    const delObj = {
      PaymentId: this.deletedPayment.Id,
      KeepPayment: this.deleteType,
      Comment: value.comment
    }

    this.exceMemberService.cancelPayment(delObj).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.paymentRemoveModelRef.close();
          if (result.Data === -1) {
            this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.PaymentCancelError' });
          } else if (result.Data === 2) {
            this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.DailySettlementMade' });
          } else if (result.Data === 1) {
            const index: number = this.selectedInvoice.OtherDetails.Paymentlist.indexOf(this.deletedPayment);
            if (index !== -1) {
              this.selectedInvoice.OtherDetails.Paymentlist.splice(index, 1);
              this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.PaymentCancelSuccess' });
            }
          }
        } else {
        }
      })
  }

  openShop() {
    const content = this.shopHomeModel;
    this.shopService.GetSelectedGymSettings({ GymSetColNames: this.selectedGymsettings, IsGymSettings: true }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        for (const key in res.Data) {
          if (res.Data.hasOwnProperty(key)) {
            switch (key) {
              case 'EcoNegativeOnAccount':
                this.exceGymSetting.OnAccountNegativeVale = res.Data[key];
                break;
              case 'EcoIsShopOnNextOrder':
                this.exceGymSetting.IsShopOnNextOrder = res.Data[key];
                break;
              case 'EcoIsDailySettlementForAll':
                this.exceGymSetting.IsDailySettlementForAll = res.Data[key];
                break;
              case 'EcoIsOnAccount':
                this.exceGymSetting.IsOnAccount = res.Data[key];
                break;
              case 'EcoIsDefaultCustomerAvailable':
                this.exceGymSetting.IsDefaultCustomerAvailable = res.Data[key];
                break;
              case 'EcoIsPayButtonsAvailable':
                this.exceGymSetting.IsPayButtonsAvailable = res.Data[key];
                break;
              case 'EcoIsPettyCashSameAsUserAllowed':
                this.exceGymSetting.IsPettyCashSameAsUserAllowed = res.Data[key];
                break;
              case 'EcoIsPrintInvoiceInShop':
                this.exceGymSetting.IsPrintInvoiceInShop = res.Data[key];
                break;
              case 'EcoIsPrintReceiptInShop':
                this.exceGymSetting.IsPrintReceiptInShop = res.Data[key];
                break;
              case 'OtherIsShopAvailable':
                this.exceGymSetting.IsShopAvailable = res.Data[key];
                break;
              case 'OtherLoginShop':
                this.exceGymSetting.IsShopLoginNeeded = res.Data[key];
                break;
              case 'HwIsShopLoginWithCard':
                this.exceGymSetting.IsShopLoginWithCard = res.Data[key];
                break;
              case 'ReqMemberListRequired':
                this.exceGymSetting.MemberRequired = res.Data[key];
                break;
              case 'OtherIsLogoutFromShopAfterSale':
                this.exceGymSetting.IsLogoutFromShopAfterSale = res.Data[key];
                break;
              case 'EcoSMSInvoiceShop':
                this.exceGymSetting.IsSMSInvoiceShop = res.Data[key];
                break;
              case 'EcoIsBankTerminalIntegrated':
                this.exceGymSetting.IsBankTerminalIntegrated = res.Data[key];
                break;
              case 'OtherIsValidateShopGymId':
                this.exceGymSetting.IsValidateShopGymId = res.Data[key];
                break;
            }

          }
        }
        this.shopLoginData.GymSetting = this.exceGymSetting;
        this.shopService.GetSalesPoint().pipe(takeUntil(this.destroy$)).subscribe(
          salePoints => {
            this.shopLoginData.SalePoint = salePoints.Data[0];
            this.shopHomeService.$salePointAdded = true;
            this.adminService.getGymSettings('HARDWARE', null).pipe(takeUntil(this.destroy$)).subscribe(
              hwSetting => {
                this.shopLoginData.HardwareProfileList = hwSetting.Data;
                this.shopLoginData.HardwareProfileList.forEach(hwProfile => {
                  if (this.shopLoginData.SalePoint != null &&
                    this.shopLoginData.SalePoint.HardwareProfileId === hwProfile.Id) {
                    this.shopLoginData.SelectedHardwareProfile = hwProfile;
                    this.shopLoginData.HardwareProfileId = hwProfile.Id;
                  } else {
                    this.shopLoginData.SelectedHardwareProfile = null;
                    this.shopLoginData.HardwareProfileId = null;
                  }
                  this.shopHomeService.$shopLoginData = this.shopLoginData;

                });
                this.setCheoutEvent(content);
              },
              error => {
              });
          },
          error => {
          });
      },
      error => {
      });
  }

  setCheoutEvent(content) {
    const orderArticles = [];
    // this.selectedOrder = selectedOrder;
    this.shopHomeService.$shopMode = 'INVOICE';
    this.shopHomeService.$selectedInvoice = this.selectedInvoice;
    this.shopHomeService.$priceType = 'MEMBER';
    this.shopHomeService.$isCancelBtnHide = true;
    // this.selectedMember.BranchID = this.selectedMember.BranchId;
    this.shopHomeService.$selectedMember = this.selectedMember;
    this.shopHomeService.$isUpdateInstallments = true;
    let paidAmount = this.selectedInvoice.InvoiceAmount - this.selectedInvoice.InvoiceBalance;
    const creditedSum = this.selectedInvoice.BasicDetails.ArticleList.map(c => c.CreditedAmount).reduce((sum, current) => sum + current);
    paidAmount = paidAmount - creditedSum;
    this.selectedInvoice.BasicDetails.ArticleList.forEach(article => {
      if (article.CreditedAmount > 0) {
        if (article.Price > article.CreditedAmount) {
          article.Price = article.Price - article.CreditedAmount;
          if (article.Price < 0) {
            article.Price = 0;
          }
        } else {
          article.Price = 0;
        }
      }
    });

    if (paidAmount > 0) {
      this.selectedInvoice.BasicDetails.ArticleList.forEach(article => {
        if (article.Price > 0) {
          if (article.Price < paidAmount) {
            paidAmount = paidAmount - article.Price;
            article.Price = 0;
          } else {
            article.Price = article.Price - paidAmount;
            paidAmount = 0;
            // break;
          }
        }
      });
    }
    this.shopHomeService.ShopItems = this.getArticleFromOrderlines();
    // this.shopHomeService.$selectedOder = selectedOrder;
    this.shopModalReference = this.modalService.open(content);
  }

  getArticleFromOrderlines() {
    const orderArticles = [];
    this.selectedInvoice.BasicDetails.ArticleList.forEach(cItem => {
      const DiscountPercentage = cItem.Discount / cItem.Price;
      orderArticles.push({
        Id: cItem.Id,
        ArticleNo: cItem.ArticleId,
        Description: cItem.Name,
        SelectedPrice: cItem.UnitPrice,
        UnitPrice: cItem.UnitPrice,
        DefaultPrice: cItem.Price,
        Price: cItem.Price,
        OrderQuantity: cItem.Quantity,
        Discount: cItem.Discount,
        DiscountPercentage: DiscountPercentage,
        ExpiryDate: cItem.ExpiryDate,
        VoucherNumber: cItem.VoucherNo,
        IsEditable: false,
        VatRate: 0
      })
    });
    return orderArticles;
  }

  closeShopAfterSave() {
    this.shopHomeService.$shopMode = '';
    this.shopModalReference.close();
    this.closeInvoiceDetailModel.emit();
  }

  closeShop() {
    this.shopHomeService.$shopMode = '';
    if (this.shopModalReference) {
      this.shopModalReference.close();
    }
  }

  setPaymentRemoveType(event) {
    if (event) {
      if (event.target.value === 'mdl') {
        this.deleteType = 1;
      }
      if (event.target.value === 'rp') {
        this.deleteType = 0;
      }
    }
  }

}
