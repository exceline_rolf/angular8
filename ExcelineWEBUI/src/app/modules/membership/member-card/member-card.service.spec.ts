import { TestBed, inject } from '@angular/core/testing';

import { MemberCardService } from './member-card.service';

describe('MemberCardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MemberCardService]
    });
  });

  it('should be created', inject([MemberCardService], (service: MemberCardService) => {
    expect(service).toBeTruthy();
  }));
});
