import { Injectable, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { ExcelineMember } from '../../models/ExcelineMember';
import * as moment from 'moment';
import { takeUntil } from 'rxjs/operators';
import { Alert } from 'selenium-webdriver';

@Injectable()
export class McBasicInfoService {
  private _brisStatus = false;
  private _isEditMode = false;
  private _isImageEdit = false;
  private _memberSource = new BehaviorSubject<ExcelineMember>(null); // Can only update in the containing service
  private _updateOrderList = new Subject<string>();

  public countries: any;
  public notifyMethods: any;
  public memberStatus: any;
  public memberCategories: any;
  public gymSettings: any;
  public SelectedContractId = -1;
  public currentMember = this._memberSource.asObservable(); // Read observable in every component
  public updatedOrderList = this._updateOrderList.asObservable();

  @Output() changeEvent: EventEmitter<any> = new EventEmitter();
  @Output() countriesEvent: EventEmitter<any> = new EventEmitter();
  @Output() notifyMethodEvent: EventEmitter<any> = new EventEmitter();
  @Output() memberStatusEvent: EventEmitter<any> = new EventEmitter();
  @Output() changeImageEvent: EventEmitter<any> = new EventEmitter();
  @Output() memberCategoryEvent: EventEmitter<any> = new EventEmitter();
  @Output() gymSettingEvent: EventEmitter<any> = new EventEmitter();
  @Output() addNewContractEvent: EventEmitter<any> = new EventEmitter();
  @Output() selectContractEvent: EventEmitter<any> = new EventEmitter();

  constructor() { }

  setSelectedMember(selectedMember: ExcelineMember) {
    if (moment(selectedMember.BirthDate).year() === 1900 && (moment(selectedMember.BirthDate).month() + 1) === 1 && moment(selectedMember.BirthDate).day() === 1) {
      selectedMember.BirthDate = null;
      selectedMember.Age = 0;
    }
    this._memberSource.next(selectedMember);
  }

  /**
   * @description Subscribes to the current member, and immediately completes.
   * @returns The current member
   * @type ExcelineMember
   */
  getSelectedMember(): any {
/*     let selectedMember: ExcelineMember;
    this.currentMember.pipe(
      takeUntil(this.destroy$)
    ).subscribe(member => {
      if (member == null) { // checks if null or undefined
        selectedMember = null;
      }
      selectedMember = member;
    }, null, () => alert('done'));
    return selectedMember;
 */
}

  updateEditMode(mode: boolean, editMode: string) {
    switch (editMode) {
      case 'CARD': {
        this._isEditMode = mode;
        this.changeEvent.emit(this._isEditMode);
        break;
      }
      // case 'IMAGE': {
      //   this._isImageEdit = mode;
      //   this.changeImageEvent.emit(this._isImageEdit);
      // }
    }
  }

  setUpdateOrderList(statusText: string) {
    this._updateOrderList.next(statusText);
  }
  getEditMode() {
    return this.changeEvent;
  }

  setCountries(countries: any) {
    this.countries = countries;
    this.countriesEvent.emit(this.countries);
  }
  getCountries() {
    return this.countriesEvent;
  }

  setNotifyMethods(nMethods: any) {
    this.notifyMethods = nMethods;
    this.notifyMethodEvent.emit(this.notifyMethods);
  }

  getNotifyMethods() {
    return this.notifyMethodEvent;
  }

  setMemberStatus(memberStatus: any) {
    this.memberStatus = memberStatus;
    this.memberStatusEvent.emit(this.memberStatus);
  }

  getMemberStatus() {
    return this.memberStatusEvent;
  }

  getImageEditMode() {
    return this.changeImageEvent;
  }

  setMemberCategoires(memberCategories: any) {
    this.memberCategories = memberCategories;
    this.memberCategoryEvent.emit(this.memberCategories);
  }

  getMemberCategories() {
    return this.memberCategoryEvent;
  }

  getMemberAge(birthDate: Date) {
    if (birthDate !== new Date(1900, 1, 1)) {
      const today: any = new Date();
      const ageFidMs: any = today - birthDate.getTime();
      const ageDate: Date = new Date(ageFidMs);
      const age: number = ageDate.getUTCFullYear() - 1970;
      return age;
    }
  }

  getGymSettings() {
    return this.gymSettings;
  }

  setGymSettings(gymSettings: any) {
    this.gymSettings = gymSettings;
    this.gymSettingEvent.emit(this.gymSettings);
  }

  setContractRegisterStatus(addContract: boolean) {
    this.addNewContractEvent.emit(addContract);
  }

  setSelectedContract(contractID: number, cardState?: any) {
    this.SelectedContractId = contractID;
    this.selectContractEvent.emit(cardState);
  }

  getSelectedContractId() {
    return this.SelectedContractId;
  }

}
