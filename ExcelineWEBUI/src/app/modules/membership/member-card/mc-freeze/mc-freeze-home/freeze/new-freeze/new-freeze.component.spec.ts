import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFreezeComponent } from './new-freeze.component';

describe('NewFreezeComponent', () => {
  let component: NewFreezeComponent;
  let fixture: ComponentFixture<NewFreezeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFreezeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFreezeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
