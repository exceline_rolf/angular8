import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { ExceToolbarService } from '../../../common/exce-toolbar/exce-toolbar.service';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { ExceMemberService } from '../../services/exce-member.service';
import { McBasicInfoService } from '../mc-basic-info/mc-basic-info.service';
import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MemberRole } from 'app/shared/enums/us-enum';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';
@Component({
  selector: 'app-mc-resign',
  templateUrl: './mc-resign.component.html',
  styleUrls: ['./mc-resign.component.scss']
})
export class McResignComponent implements OnInit, AfterViewInit, OnDestroy {
  branchId: any;
  locale: string;

  public contractSummeries = [];
  private isFreezeDetailsNeeded = false;
  private isFreezeView = false;

  itemResource: any;
  itemCount: number;
  rowColors: any;
  selectedMember: any;
  memSubs: any;
  installments: any;
  contractId: any;
  memberBranchId: any;
  private isResign = true
  private destroy$ = new Subject<void>();

  constructor(
    private basicInfoService: McBasicInfoService,
    private memberService: ExceMemberService,
    private router: Router,
    private exceLoginService: ExceLoginService,
    private toolbarService: ExceToolbarService,
  ) { }

  ngOnInit() {

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );

    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
  }

  ngAfterViewInit(): void {
  this.basicInfoService.currentMember.pipe(
    mergeMap(currentMember => {
      this.selectedMember = currentMember;
      return this.memberService.getMemberContractSummeries(this.selectedMember.Id, this.selectedMember.BranchId, this.isFreezeDetailsNeeded, this.isFreezeView);
    }),
    takeUntil(this.destroy$)
  ).subscribe(result => {
    if (result) {
      this.contractSummeries = result.Data.filter(contract => contract.MemberID === this.selectedMember.Id)
      this.rowColors = this.getRowColors.bind(this);
      this.itemCount = Number(this.contractSummeries.length);
      this.itemResource = new DataTableResource(result.Data);
      this.itemResource.count().then(count => this.itemCount = count);
    }
  });

/*     this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          this.memberService.getMemberContractSummeries(this.selectedMember.Id, this.selectedMember.BranchId, this.isFreezeDetailsNeeded, this.isFreezeView).pipe(takeUntil(this.destroy$)).subscribe
            (
            result => {
              if (result) {
                // this.contractSummeries = result.Data;
                result.Data.forEach(contract => {
                  if (contract.MemberID === this.selectedMember.Id) {
                    this.contractSummeries.push(contract)
                  }
                });
                this.rowColors = this.getRowColors.bind(this);
                this.itemCount = Number(this.contractSummeries.length);
                this.itemResource = new DataTableResource(result.Data);
                this.itemResource.count().then(count => this.itemCount = count);
              }
            }
            )
        }
      }); */
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  reloadItems(params) {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.contractSummeries = items);
    }
  }

  getRowColors(memberContracts) {
    if (memberContracts.ContractEndDate < new Date()) {
      return 'rgb(255, 255, 204)';
    }
  }

  selectContract(item) {
    if (item != null) {
      this.router.navigate(['membership/card/' + this.selectedMember.Id + '/' + this.branchId + '/' + MemberRole[this.selectedMember.Role] + '/resign/' + item.Id]);
    }
  }


}
