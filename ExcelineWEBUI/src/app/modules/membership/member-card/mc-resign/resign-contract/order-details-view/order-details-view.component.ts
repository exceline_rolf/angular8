import { McBasicInfoService } from '../../../mc-basic-info/mc-basic-info.service';
import { ExceLoginService } from '../../../../../login/exce-login/exce-login.service';
import { ShopHomeService } from '../../../../../shop/shop-home/shop-home.service';
import { ExceMessageService } from '../../../../../../shared/components/exce-message/exce-message.service';
import { ExceMemberService } from '../../../../services/exce-member.service';
import { PreloaderService } from '../../../../../../shared/services/preloader.service';
import { EntitySelectionType, ColumnDataType, PaymentTypes } from '../../../../../../shared/enums/us-enum';
import { UsbModalRef } from '../../../../../../shared/components/us-modal/modal-ref';
import { ExceToolbarService } from '../../../../../common/exce-toolbar/exce-toolbar.service';
import { saveAs } from 'file-saver/FileSaver';
import { Component, OnInit, Input, EventEmitter, Output, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { UsbModal } from '../../../../../../shared/components/us-modal/us-modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { ExcePdfViewerService } from '../../../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { Extension } from '../../../../../../shared/Utills/Extensions';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { ExceEconomyService } from '../../../../../economy/services/exce-economy.service';
import { AdminService } from '../../../../../admin/services/admin.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
const now = new Date();

@Component({
  selector: 'order-details-view',
  templateUrl: './order-details-view.component.html',
  styleUrls: ['./order-details-view.component.scss']
})
export class OrderDetailsViewComponent implements OnInit, OnDestroy {

  SelectedEmailTemplate: any;
  SelectedSMSTemplate: any;
  editTemplateModal: any;
  EmailContent: any;
  SMSContent: any;
  EmailTemplate: any;
  SMSTemplate: any;
  EmailAddress: any;
  SmsNo: any;
  EmailChecked: boolean;
  SMSChecked: boolean;
  editTemplateForm: any;
  selectedMember: any;
  memberBranchID: any;
  shopModel: UsbModalRef;
  DefaultCheckOutEnabled: any;
  yesHandlerSubscription: Subscription;
  addonTextList: any;
  selectodCustomer: any;
  addMemberView: UsbModalRef;
  itemCount: any;
  entityItems: any;
  entitySelectionModel: UsbModalRef;
  columns: { property: string; header: string; sortable: boolean; resizable: boolean; width: string; type: any; }[];
  isBasicInforVisible: boolean;
  isAddBtnVisible: boolean;
  isRoleVisible: boolean;
  isStatusVisible: boolean;
  isBranchVisible: boolean;
  entitySearchType: string;
  orderlineModel: UsbModalRef;
  IsMemberEnabled: boolean;
  IsInvoiceFeeEnabled: boolean;
  IsDueDateEnabled: boolean;
  IsPrintButtonEnabled: boolean;
  IsCheckOutEnabled: boolean;
  IsButtonEnabled: boolean;
  IsBrisIntegration: any;
  private _branchId: number;

  blobUrl: any;
  blob: any;
  currentInvoice: any;
  generatePdfView: UsbModalRef;

  type: string;
  isShowSaveInfo = false;
  saveMessage = 'OK';

  @ViewChild('Pdfwindow', { static: true }) Pdfwindow: ElementRef;

  formsErrors = {
    'TrainingPeriodStart': '',
    'TrainingPeriodEnd': '',
    'OriginalDueDate': '',
    'EstimatedInvoiceDate': '',
    'InstallmentDate': '',
    'DueDate': ''
  }

  validationMessages = {
    'TrainingPeriodStart': {
      'required' : 'MEMBERSHIP.Required'
    },
    'TrainingPeriodEnd': {
      'required' : 'MEMBERSHIP.Required'
    },
    'OriginalDueDate': {
      'required' : 'MEMBERSHIP.Required'
    },
    'EstimatedInvoiceDate': {
      'required' : 'MEMBERSHIP.Required'
    },
    'InstallmentDate': {
      'required' : 'MEMBERSHIP.Required'
    },
    'DueDate': {
      'required' : 'MEMBERSHIP.Required'
    }
  }

  @Input() selectedOrder: any;
  @Input() isaEnableShopOperations: boolean;
  @Input() buttonsEnabled: boolean;
  private isViewEnabled: boolean;

  @Input() get IsViewEnabled(): boolean {
    return this.isViewEnabled;
  }

  set IsViewEnabled(value: boolean) {
    this.isViewEnabled = value;
    this.IsButtonEnabled = this.isViewEnabled;
    this.IsCheckOutEnabled = this.isViewEnabled;
    this.IsPrintButtonEnabled = this.isViewEnabled;
    this.IsDueDateEnabled = this.isViewEnabled;
    this.IsInvoiceFeeEnabled = this.isViewEnabled;
    this.IsMemberEnabled = this.isViewEnabled;
    this.DefaultCheckOutEnabled = false;
  }
  @Output() closeOrderDetailModel = new EventEmitter();
  @Output() orderUpdateSuccessEvent = new EventEmitter();

  auotocompleteItems = [
    { id: 'NAME', name: 'NAME :', isNumber: false },
    { id: 'ID', name: 'Id :', isNumber: true },
    { id: 'ADDRESS', name: 'ADDRESS :', isNumber: false },
    { id: 'MOBILE', name: 'MOBILE :', isNumber: false },
    { id: 'EMAIL', name: 'EMAIL :', isNumber: true }];

  orderlineForm: FormGroup;
  orderlineDetailsForm: FormGroup;
  locale: string;
  private modalReference?: any;
  private destroy$ = new Subject<void>();
  public fromDatePickerOptions: IMyDpOptions = {
    // disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() },
  };

  constructor(
    private basicInfoService: McBasicInfoService,
    private exceMemberService: ExceMemberService,
    private toolbarService: ExceToolbarService,
    private fb: FormBuilder,
    private adminService: AdminService,
    private exceMessageService: ExceMessageService,
    private memberService: ExceMemberService,
    private loginservice: ExceLoginService,
    private shopHomeService: ShopHomeService,
    private excePdfViewerService: ExcePdfViewerService,
    private translateService: TranslateService,
    private sanitizer: DomSanitizer,
    private economyService: ExceEconomyService,
    private modalService: UsbModal
  ) {

    this.editTemplateForm = fb.group({
      'IsSMSChecked': [this.SMSChecked],
      'IsEmailChecked': [this.EmailChecked],
      'SmsNo': [{ value: this.SmsNo, disabled: false }, [Validators.pattern('^[0-9 ]*$')]], // { value: null, disabled: true }
      'SelectedSMSTemplate': [this.SMSTemplate],
      'SMSContent': [null],
      'EmailAddress': [{ value: this.EmailAddress, disabled: false }, [Validators.email]],
      'SelectedEmailTemplate': [this.EmailTemplate],
      'EmailContent': [null],
    });


    this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      if (value.id === 'CONFIRM_INVOICEDATE') {
        this.confirmOrderDetails();
      } else if (value.id === 'DELETE_ORDERLINE') {
        this.confirmDeleteOrderLine();
      } else if (value.id === 'DELETE_ORDER') {
        // this.deleteYesHandler();
      } else if (value.id === 'INVOICE_WITH_SPONSORING') {
        this.invoiceWithSopnsoringYesHandler();
      } else if (value.id === 'MSG_INVOICE') {
        this.msgInvoiceYesHandler();
      } else if (value.id === 'ADD_INVOICE_FEE2') {
        this.msgAddInvoiceFeeYesHandler();
      } else if (value.id === 'MSG_INVOICE_SMS') {
        this.msgInvoiceSMSYesHandler(value.optionalData);
      } else if (value.id === 'ADD_INVOICE_FEE_SMS') {
        this.msgAddInvoiceFeeSMSYesHandler();
      }
    });

    this.exceMessageService.noCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      if (value.id === 'ADD_INVOICE_FEE2') {
        this.msgAddInvoiceFeeNoHandler();
      } else if (value.id === 'ADD_INVOICE_FEE_SMS') {
        this.msgAddInvoiceFeeSMSNoHandler();
      }
    });
  }

  ngOnInit() {
    this.adminService.GetGymCompanySettings('other').pipe(takeUntil(this.destroy$)).subscribe(result => {
      this.IsBrisIntegration = result.Data.IsBrisIntegrated;
    });
    this._branchId = this.loginservice.SelectedBranch.BranchId;

    if (this.buttonsEnabled !== undefined) {
      this.IsButtonEnabled = this.buttonsEnabled;
    }
    // get the selected member
    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
        }
      },
      error => {
        console.log(error);
      });

      this.memberBranchID = this.selectedMember.BranchId
      const language = this.toolbarService.getSelectedLanguage();
      this.locale = language.culture;
      // this._dateFormat = 'DD.MM.YYYY';

      this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
        (lang) => {
          if (lang) {
            this.locale = lang.culture;
          }
        }
      );

    this.orderlineForm = this.fb.group({
      ActivityPrice: [{ disabled: true, value: this.selectedOrder.ActivityPrice }],
      AdonPrice: [{ disabled: true, value: this.selectedOrder.AdonPrice }],
      Amount: [{ disabled: true, value: this.selectedOrder.Amount }],
      DueDate: [{ date: this.selectedOrder.DueDateFormated }, Validators.required],
      Text: [this.selectedOrder.Text],
      Discount: [{ disabled: true, value: this.selectedOrder.Discount }],
      SponsoredAmount: [{ disabled: true, value: this.selectedOrder.SponsoredAmount }],
      EstimatedOrderAmount: [this.selectedOrder.EstimatedOrderAmount]
    });
    this.orderlineForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => UsErrorService.onValueChanged(this.orderlineForm, this.formsErrors, this.validationMessages));
    // this.selectedOrder.PayerName = this.selectedOrder.MemberName;
    this.orderlineDetailsForm = this.fb.group({
      TrainingPeriodStart: [{ disabled: !this.isViewEnabled, value: { date: this.selectedOrder.TrainingPeriodStart } }, Validators.required],
      TrainingPeriodEnd: [{ disabled: !this.isViewEnabled, value: { date: this.selectedOrder.TrainingPeriodEnd } }, Validators.required],
      InstallmentDate: [{ disabled: !this.isViewEnabled, value: { date: this.selectedOrder.InstallmentDate } }, Validators.required],
      OriginalDueDate: [{ disabled: !this.isViewEnabled, value: { date: this.selectedOrder.OriginalDueDate } }, Validators.required],
      EstimatedInvoiceDate: [{ disabled: !this.isViewEnabled, value: { date: this.selectedOrder.EstimatedInvoiceDate } }, Validators.required],
      AdonText: [{ disabled: true, value: this.selectedOrder.AdonText }],
      ContractName: [{ disabled: !this.isViewEnabled, value: this.selectedOrder.ContractName }],
      Comment: [{ disabled: !this.isViewEnabled, value: this.selectedOrder.Comment }],
      BranchName: [{ disabled: !this.isViewEnabled, value: this.selectedOrder.BranchName }],
      PayerName: [{ disabled: true, value: this.selectedOrder.PayerName }],
      OriginalCustomer: [{ disabled: !this.isViewEnabled, value: this.selectedOrder.OriginalCustomer }],
      IsSpOrder: [{ disabled: true, value: this.selectedOrder.IsSpOrder }],
      IsInvoiceFeeAdded: [{ disabled: !this.IsInvoiceFeeEnabled, value: this.selectedOrder.IsInvoiceFeeAdded }],
      IsATG: [{ disabled: !this.isViewEnabled, value: this.selectedOrder.IsATG }],

    });
    this.orderlineDetailsForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(_ =>
      UsErrorService.onValueChanged(this.orderlineDetailsForm, this.formsErrors, this.validationMessages)
    );


    this.entitySearchType = 'MEM';
    this.isBranchVisible = true;
    this.isStatusVisible = true;
    this.isRoleVisible = true;
    this.isAddBtnVisible = true;
    this.isBasicInforVisible = true;
    this.columns = [
      { property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'TelMobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];

  }

  // ------------------------ SEND SMS/EMAIL -----------------------

  // to send SMS or Email
  SMSInvoiceEvent(item, editTemplateModal) {

    this.selectedOrder = item;
    this.editTemplateModal = editTemplateModal;

    // MsgInvoice = Are you sure you want to invoice ?
    let messageTitle = '';
    let messageBody = '';
    this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
    this.translateService.get('MEMBERSHIP.MsgInvoice').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'MSG_INVOICE_SMS',
        optionalData: this.selectedOrder.IsInvoiceFeeAdded
      }
    );
  }

  // ADD_INVOICE_FEE_SMS yes
  msgAddInvoiceFeeSMSYesHandler() {
    this.PreapreSMSInvoice();
  }

  // ADD_INVOICE_FEE_SMS no
  msgAddInvoiceFeeSMSNoHandler() {
    this.selectedOrder.IsInvoiceFeeAdded = false;
    this.PreapreSMSInvoice();
  }



  // INVOICE_WITH_SPONSORING
  invoiceWithSopnsoringYesHandler() {

    if (this.selectedOrder.IsInvoiceFeeAdded === true) {
      let messageTitle = '';
      let messageBody = '';
      this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this.translateService.get('MEMBERSHIP.MsgaddInvoiceFee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: messageTitle,
          messageBody: messageBody,
          msgBoxId: 'ADD_INVOICE_FEE2'
        }
      );
    } else {
      this.GenerateInvoiceDocument();
    }
  }

  // ADD_INVOICE_FEE yes
  msgAddInvoiceFeeYesHandler() {
    this.GenerateInvoiceDocument();
  }

  // ADD_INVOICE_FEE no
  msgAddInvoiceFeeNoHandler() {
    this.selectedOrder.IsInvoiceFeeAdded = false;
    this.GenerateInvoiceDocument();
  }

  // to generate the invoice
  GenerateInvoiceDocument() {
    if (this.selectedOrder.AddOnList.length !== 0) {
    this.selectedOrder.DueDate = moment(this.selectedOrder.DueDate).format('YYYY-MM-DD');
    this.selectedOrder.TrainingPeriodStart =
      this.orderlineDetailsForm.value.TrainingPeriodStart.date.year + '-' +
      this.orderlineDetailsForm.value.TrainingPeriodStart.date.month + '-' +
      this.orderlineDetailsForm.value.TrainingPeriodStart.date.day;
    this.selectedOrder.TrainingPeriodEnd =
      this.orderlineDetailsForm.value.TrainingPeriodEnd.date.year + '-' +
      this.orderlineDetailsForm.value.TrainingPeriodEnd.date.month + '-' +
      this.orderlineDetailsForm.value.TrainingPeriodEnd.date.day;
    this.selectedOrder.OriginalDueDate =
      this.orderlineDetailsForm.value.OriginalDueDate.date.year + '-' +
      this.orderlineDetailsForm.value.OriginalDueDate.date.month + '-' +
      this.orderlineDetailsForm.value.OriginalDueDate.date.day;
    this.selectedOrder.InstallmentDate =
      this.orderlineDetailsForm.value.InstallmentDate.date.year + '-' +
      this.orderlineDetailsForm.value.InstallmentDate.date.month + '-' +
      this.orderlineDetailsForm.value.InstallmentDate.date.day;
    this.selectedOrder.EstimatedInvoiceDate =
      this.orderlineDetailsForm.value.EstimatedInvoiceDate.date.year + '-' +
      this.orderlineDetailsForm.value.EstimatedInvoiceDate.date.month + '-' +
      this.orderlineDetailsForm.value.EstimatedInvoiceDate.date.day;

    const generateInvoice = {
      invoiceBranchId: this._branchId,
      installment: this.selectedOrder,
      paymentDetail: { PaymentSource: PaymentTypes[PaymentTypes.INSTALLMENTS] },
      invoiceType: 'PRINT',
      notificationTitle: 'Invoice was generated',
      memberBranchID: this.memberBranchID
    }
    this.exceMemberService.generateInvoice(generateInvoice).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.openGenerateConfirmModal(this.Pdfwindow, result.Data.InvoiceNo)
        this.closeOrderDetailModel.emit();
      } else {
        //  ================= When generate Invoice Failed ====================
        let messageBody = '';
        this.translateService.get('MEMBERSHIP.MsgInvoiceDocGenFailed').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
        this.exceMessageService.openMessageBox('WARNING', {
            messageTitle: 'Exceline',
            messageBody: messageBody,
            msgBoxId: 'INVOICE_FAILED'
          }
        );
      }
    },
      error => {
        console.log(error);
      });
    } else {
      let messageBody = '';
      this.translateService.get('MEMBERSHIP.zeroOrderLineInvoice').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('ERROR', {
          messageTitle: 'Exceline',
          messageBody: messageBody,
          msgBoxId: 'INVOICE_FAILED'
        });
    }
  }


  // MSG_INVOICE
  msgInvoiceYesHandler() {

    let messageTitle = '';
    let messageBody = '';
    this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
    this.translateService.get('MEMBERSHIP.MsgaddInvoiceFee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
    this.exceMessageService.openMessageBox('CONFIRM', {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'ADD_INVOICE_FEE2'
      }
    );
  }

  // MSG_INVOICE_SMS
  msgInvoiceSMSYesHandler(data) {

    // this.payingOrder.IsInvoiceFeeAdded = data;
    if (data === true) {
      let messageTitle = '';
      let messageBody = '';
      this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this.translateService.get('MEMBERSHIP.MsgaddInvoiceFee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('CONFIRM', {
          messageTitle: messageTitle,
          messageBody: messageBody,
          msgBoxId: 'ADD_INVOICE_FEE_SMS',
          optionalData: data
        }
      );
    } else {
      this.PreapreSMSInvoice();
    }
  }

  PreapreSMSInvoice() {
    this.openEditTemplateModal(this.editTemplateModal);
  }

  // to open Edit Template view
  openEditTemplateModal(content: any) {

    this.editTemplateForm.reset();

    if (this.selectedMember.Mobile !== '') {
      this.SMSChecked = true;
      this.editTemplateForm.get('SmsNo').enable();
    } else {
      this.SMSChecked = false;
      this.editTemplateForm.get('SmsNo').disable();
    }
    if (this.selectedMember.Email !== '') {
      this.EmailChecked = true;
      this.editTemplateForm.get('EmailAddress').enable();
    } else {
      this.EmailChecked = false;
      this.editTemplateForm.get('EmailAddress').disable();
    }

    this.SmsNo = this.selectedMember.Mobile;
    this.EmailAddress = this.selectedMember.Email;

    this.SMSTemplate = this.SelectedSMSTemplate[0];
    this.EmailTemplate = this.SelectedEmailTemplate[0];
    this.SMSContent = this.SelectedSMSTemplate[0].Text;
    this.EmailContent = this.SelectedEmailTemplate[0].Text;
    this.modalService.open(content, { width: '600' });
  }



  openModel(content) {
    this.modalReference = this.modalService.open(content, { width: '1500' });
  }

  onDueDateChange(event) {
    this.selectedOrder.DueDate = event.jsdate;
    if (this.selectedOrder.InstallmentType === 'SP') {
      if (this.selectedOrder.SponsorCreditPeriod > 0) {
        this.selectedOrder.EstimatedInvoiceDate = this.dateAdd(this.selectedOrder.DueDate, (this.selectedOrder.SponsorCreditPeriod * -1), 'days');
      } else if (this.selectedOrder.MemberCreditPeriod > 0) {
        this.selectedOrder.EstimatedInvoiceDate = this.dateAdd(this.selectedOrder.DueDate, (this.selectedOrder.MemberCreditPeriod * -1), 'days');
      } else {
        this.selectedOrder.EstimatedInvoiceDate = this.dateAdd(this.selectedOrder.DueDate, (this.selectedOrder.CreditPeriod * -1), 'days');
      }
    } else {
      if (this.selectedOrder.MemberCreditPeriod > 0) {
        this.selectedOrder.EstimatedInvoiceDate = this.dateAdd(this.selectedOrder.DueDate, (this.selectedOrder.MemberCreditPeriod * -1), 'days');
      } else if (this.selectedOrder.CreditPeriod > 0) {
        this.selectedOrder.EstimatedInvoiceDate = this.dateAdd(this.selectedOrder.DueDate, (this.selectedOrder.CreditPeriod * -1), 'days');
      }
    }
    this.selectedOrder.OriginalDueDate = this.selectedOrder.DueDate;
    this.orderlineDetailsForm.patchValue({
         EstimatedInvoiceDate: {date: Extension.GetFormatteredDate(this.selectedOrder.EstimatedInvoiceDate) }
    });
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  AddText(item) {
    item.Description = ' ';
  }

  rowUp(item, index) {
    if (index > 0) {
      const tmp = this.selectedOrder.AddOnList[index - 1];
      this.selectedOrder.AddOnList[index - 1] = this.selectedOrder.AddOnList[index];
      this.selectedOrder.AddOnList[index] = tmp;
    }
  }

  rowDown(item, index) {
    if (index < this.selectedOrder.AddOnList.length) {
      const tmp = this.selectedOrder.AddOnList[index + 1];
      this.selectedOrder.AddOnList[index + 1] = this.selectedOrder.AddOnList[index];
      this.selectedOrder.AddOnList[index] = tmp;
    }

  }

  selectAddonTextModelOpen(content) {
    this.modalReference = this.modalService.open(content, { width: '600' });
    this.memberService.getCategories('ADONTEXT').pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.addonTextList = res.Data;
      }, err => {
      }
    );
  }

  selectADTextRowClick(item) {
    this.orderlineDetailsForm.patchValue({
      'AdonText': item.Description
    })
    this.modalReference.close();
  }

  addonTextModelClose() {
    this.modalReference.close();
  }

  closeOrderDetailsModel() {
    this.closeOrderDetailModel.emit();
  }

  openSelectOrderLine(content) {
    this.orderlineModel = this.modalService.open(content, { width: '800'});
  }

  orderlineModelClose() {
    this.orderlineModel.close();
  }

  selectItemByDoubleClick(article) {
    this.selectedOrder.AddOnList.push({
      ArticleId: article.ArticleNo,
      ItemName: article.Description,
      UnitPrice: article.DefaultPrice,
      Discount: article.Discount,
      Quantity: article.Quantity,
      Price: article.Quantity * article.DefaultPrice

    });
    this.UpdateAmount();
    this.orderlineModel.close();

  }

  selectMultipleArticles(articles) {
    articles.forEach(article => {
      this.selectedOrder.AddOnList.push({
        ArticleId: article.ArticleNo,
        ItemName: article.Description,
        UnitPrice: article.DefaultPrice,
        Discount: article.Discount,
        Quantity: article.Quantity,
        Price: article.Quantity * article.DefaultPrice

      });
    });
    this.UpdateAmount();
    this.orderlineModel.close();
  }

  quantityChange(event, item) {
    this.selectedOrder.AddOnList.forEach(article => {
      if (article.Id === item.Id) {
        if (article.Quantity < 0 || article.Quantity === null || article.Quantity === 0) {
          article.Quantity = 1;
        }
        article.Price = article.Quantity * article.UnitPrice;
      }
    });
  }

  openEntitySelectionModel(content) {
    this.entitySelectionModel = this.modalService.open(content);
  }

  addMemberModal(content: any) {
    this.addMemberView = this.modalService.open(content, { width: '800' });
    this.entitySelectionModel.close();
  }

  addEntity(item: any) {
    this.selecteMember(item);
    this.selectodCustomer = item;
    this.orderlineDetailsForm.patchValue({
      'PayerName': item.Id + ' - ' + item.Name
    })
    this.addMemberView.close();
  }

  searchDeatail(val: any) {
    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.MEM]:
        PreloaderService.showPreLoader();
        this.memberService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
          'SHOP', val.roleSelected.id, val.hit, false,
          false, ''
        ).pipe(takeUntil(this.destroy$)).subscribe(result => {
            this.entityItems = result.Data;
            this.itemCount = result.Data.length;
            PreloaderService.hidePreLoader();
          }, err => {
            PreloaderService.hidePreLoader();
          });
        break;
    }

  }

  selecteMember(item) {
    this.selectodCustomer = item;
    this.orderlineDetailsForm.patchValue({
      'PayerName': item.CustId + ' - ' + item.Name
    });
    this.orderlineDetailsForm.get('PayerName').disable();
    this.entitySelectionModel.close();

  }

  private UpdateAmount(): void {
    let totalAmount = 0;
    if (Number(this.selectedOrder.ActivityPrice) <= 0) {
      const activityItem = this.selectedOrder.AddOnList.filter(X => X.IsActivityArticle === true)[0];
      if (activityItem != null) {
        this.selectedOrder.ActivityPrice = activityItem.Price;
      }
    }
    const serviceItems = this.selectedOrder.AddOnList.filter(X => X.IsStartUpItem === false && Number(X.Price) > 0);
    const startUpItems = this.selectedOrder.AddOnList.filter(X => X.IsStartUpItem === true);
    this.selectedOrder.ServiceAmount = serviceItems.reduce((a, b) => a + Number(b.Price), 0);
    this.selectedOrder.ItemAmount = startUpItems.reduce((a, b) => a + Number(b.Price), 0);
    totalAmount = this.selectedOrder.AddOnList.filter(X => X.ArticleId > 0).reduce((a, b) => a + Number(b.Price), 0);
    this.selectedOrder.Amount = totalAmount;
    this.selectedOrder.EstimatedOrderAmount = this.selectedOrder.AddOnList.reduce((a, b) => a + Number(b.Price), 0);
    this.selectedOrder.AdonPrice = totalAmount - Number(this.selectedOrder.ActivityPrice);
    this.orderlineForm.patchValue({
      AdonPrice: this.selectedOrder.AdonPrice,
      Amount: this.selectedOrder.Amount,
      EstimatedOrderAmount: this.selectedOrder.EstimatedOrderAmount,
      ActivityPrice: this.selectedOrder.ActivityPrice
    });
  }

  private confirmOrderDetails() {
    this.UpdateAmount();

    let shareAmount = 0;
    shareAmount = this.selectedOrder.AddOnList.reduce((a, b) => a + b.Price, 0);
    if (shareAmount < 0 && this.selectedOrder.IsSponsored === true) {
      this.exceMessageService.openMessageBox('WARNING',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.MsgOrderAmountError'
        });
    } else {
      PreloaderService.showPreLoader();

      this.selectedOrder.TransferDate = new Date();
      this.selectedOrder.isFormatedDates = true;
     // this.selectedOrder.AdonPrice = this.selectedOrder.Amount;
     this.memberService.UpdateMemberInstallment(this.selectedOrder).pipe(takeUntil(this.destroy$)).subscribe(
        res => {
          this.orderUpdateSuccessEvent.emit(res.Data);
          PreloaderService.hidePreLoader();
        }, err => {
          this.exceMessageService.openMessageBox('ERROR',
            {
              messageTitle: 'Exceline',
              messageBody: 'MEMBERSHIP.MsgOrderSaveError'
            });
          PreloaderService.hidePreLoader();
        }
     );
    }
  }


  saveOrder() {
    UsErrorService.validateAllFormFields(this.orderlineForm, this.formsErrors, this.validationMessages);
    UsErrorService.validateAllFormFields(this.orderlineDetailsForm, this.formsErrors, this.validationMessages);
    if (this.orderlineDetailsForm.valid && this.orderlineForm.valid) {
    this.selectedOrder.PayerId = this.selectodCustomer ? this.selectodCustomer.Id : this.selectedOrder.PayerId;
    this.selectedOrder.PayerName = this.selectodCustomer ? (this.selectodCustomer.CustId + ' - ' + this.selectodCustomer.Name) : this.selectedOrder.PayerName;
    this.selectedOrder.Text = this.orderlineForm.value.Text;
    this.selectedOrder.Comment = this.orderlineDetailsForm.value.Comment;
    this.selectedOrder.IsATG = this.orderlineDetailsForm.value.IsATG;
    this.selectedOrder.IsInvoiceFeeAdded = this.orderlineDetailsForm.value.IsInvoiceFeeAdded;
    // this.selectedOrder.AdonPrice =

    this.selectedOrder.DueDate = moment(this.selectedOrder.DueDate).format('YYYY-MM-DD');

    this.selectedOrder.TrainingPeriodStart =
      this.orderlineDetailsForm.value.TrainingPeriodStart.date.year + '-' +
      this.orderlineDetailsForm.value.TrainingPeriodStart.date.month + '-' +
      this.orderlineDetailsForm.value.TrainingPeriodStart.date.day;
    this.selectedOrder.TrainingPeriodEnd =
      this.orderlineDetailsForm.value.TrainingPeriodEnd.date.year + '-' +
      this.orderlineDetailsForm.value.TrainingPeriodEnd.date.month + '-' +
      this.orderlineDetailsForm.value.TrainingPeriodEnd.date.day;
    this.selectedOrder.OriginalDueDate =
      this.orderlineDetailsForm.value.OriginalDueDate.date.year + '-' +
      this.orderlineDetailsForm.value.OriginalDueDate.date.month + '-' +
      this.orderlineDetailsForm.value.OriginalDueDate.date.day;
    this.selectedOrder.InstallmentDate =
      this.orderlineDetailsForm.value.InstallmentDate.date.year + '-' +
      this.orderlineDetailsForm.value.InstallmentDate.date.month + '-' +
      this.orderlineDetailsForm.value.InstallmentDate.date.day;
    this.selectedOrder.EstimatedInvoiceDate =
      this.orderlineDetailsForm.value.EstimatedInvoiceDate.date.year + '-' +
      this.orderlineDetailsForm.value.EstimatedInvoiceDate.date.month + '-' +
      this.orderlineDetailsForm.value.EstimatedInvoiceDate.date.day;


    if (new Date(this.selectedOrder.EstimatedInvoiceDate) <= new Date()) {
      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.MsgEstiamtedInvoiceDateError',
          msgBoxId: 'CONFIRM_INVOICEDATE'
        });
    } else {
      this.UpdateAmount();

      let shareAmount = 0;
      shareAmount = this.selectedOrder.AddOnList.reduce((a, b) => a + b.Price, 0);
      if (shareAmount < 0 && this.selectedOrder.IsSponsored) {
        this.exceMessageService.openMessageBox('WARNING',
          {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.MsgOrderAmountError'
          });
      } else {
        PreloaderService.showPreLoader();
        this.selectedOrder.isFormatedDates = true;
        this.selectedOrder.TransferDate = new Date();
        this.selectedOrder.AdonPrice = this.selectedOrder.Amount;
        this.memberService.UpdateMemberInstallment(this.selectedOrder).pipe(takeUntil(this.destroy$)).subscribe(
          res => {
            PreloaderService.hidePreLoader();
            this.translateService.get('MEMBERSHIP.SaveSuccessful').pipe(takeUntil(this.destroy$)).subscribe(value => this.saveMessage = value);
            this.type = 'SUCCESS';
            this.isShowSaveInfo = true;
            setTimeout(() => {
              this.isShowSaveInfo = false;
              this.orderUpdateSuccessEvent.emit(res.Data);
            }, 800);
          }, err => {
            this.translateService.get('MEMBERSHIP.SaveNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(value => this.saveMessage = value);
            PreloaderService.hidePreLoader();
            this.type = 'DANGER';
            this.isShowSaveInfo = true;
            setTimeout(() => {
              this.isShowSaveInfo = false;
            }, 2000);
          }
        );
      }
    }
  }
}
  CheckOutEvent(parameter): void {
    if (this.selectedOrder != null) {
      this.HandleButtons(false);

    }

  }

  removeArticle(item) {
    const index: number = this.selectedOrder.AddOnList.indexOf(item);
    if (index !== -1) {
        this.selectedOrder.AddOnList.splice(index, 1);
        this.UpdateAmount();
    }
  }

  HandleButtons(status): void {
    this.IsButtonEnabled = status;
    this.IsPrintButtonEnabled = status;

    if (this.DefaultCheckOutEnabled) {
      this.IsCheckOutEnabled = status;
    }
  }

  deleteOrderline(item) {
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'DELETE_ORDERLINE'
      });
  }

  confirmDeleteOrderLine() {

  }

  checkOut(shopModel) {
    const orderArticles = [];
    this.selectedOrder = this.selectedOrder;
    this.shopHomeService.$priceType = 'MEMBER';
    this.shopHomeService.$isCancelBtnHide = true;
    this.selectedMember.BranchID = this.selectedMember.BranchId;
    this.shopHomeService.$selectedMember = this.selectedMember;
    this.shopHomeService.$isUpdateInstallments = true;
    this.selectedOrder.AddOnList.forEach(addon => {
      orderArticles.push({
        ArticleNo: addon.ArticleId,
        Id: addon.ArticleId,
        Description: addon.ItemName,
        UnitPrice: addon.UnitPrice,
        DiscountAmountPercentage: addon.Discount,
        OrderQuantity: addon.Quantity,
        DefaultPrice: addon.Price,
        Price: addon.Price,
        isReturn: false,
        EmployeePrice: addon.EmployeePrice,
        SelectedPrice: addon.UnitPrice,
        ReturnComment: '',
        Discount: 0
      })
    });
    this.shopHomeService.ShopItems = orderArticles;
    this.selectedOrder.DueDateFormated =
      this.selectedOrder.DueDateFormated.year + '-' + this.selectedOrder.DueDateFormated.month + '-' + this.selectedOrder.DueDateFormated.day;
    this.selectedOrder.EstimatedInvoiceDate =
      this.selectedOrder.EstimatedInvoiceDate.year + '-' + this.selectedOrder.EstimatedInvoiceDate.month + '-' + this.selectedOrder.EstimatedInvoiceDate.day;
    this.selectedOrder.InstallmentDate =
      this.selectedOrder.InstallmentDate.year + '-' + this.selectedOrder.InstallmentDate.month + '-' + this.selectedOrder.InstallmentDate.day;
    this.selectedOrder.OriginalDueDate =
      this.selectedOrder.OriginalDueDate.year + '-' + this.selectedOrder.OriginalDueDate.month + '-' + this.selectedOrder.OriginalDueDate.day;
    this.selectedOrder.TrainingPeriodEnd =
      this.selectedOrder.TrainingPeriodEnd.year + '-' + this.selectedOrder.TrainingPeriodEnd.month + '-' + this.selectedOrder.TrainingPeriodEnd.day;
    this.selectedOrder.TrainingPeriodStart =
      this.selectedOrder.TrainingPeriodStart.year + '-' + this.selectedOrder.TrainingPeriodStart.month + '-' + this.selectedOrder.TrainingPeriodStart.day;
    this.shopHomeService.$selectedOder = this.selectedOrder;
    this.selectedOrder.isFormatedDates = true;
    this.shopModel = this.modalService.open(shopModel);
  }

  private UpdateExtendedFiled(): void {
    // let _displayName = '';

    try {
      const _fieldList: any[] = [];
      // _fieldList = ManageMemberShipRuntimeVariables.GetSession().OperationExtendedInfo.OperationFieldList;
      // foreach(OperationField field in _fieldList)
      // {
      //   _displayName = field.DisplayName.Replace(" ", "");

      //   switch (_displayName) {

      //     case "DueDate":
      //       {
      //         if (field.FieldStatus == USPUIFieldStatus.HIDE) {
      //           txtDueDate.Visibility = Visibility.Collapsed;
      //           dpDueDate.Visibility = Visibility.Collapsed;
      //         }
      //         else if (field.FieldStatus == USPUIFieldStatus.DISABLE) {
      //           OrderDetailsVm.IsDueDateEnabled = false;
      //         }

      //         break;
      //       }
      //     case "ChangeCustomer":
      //       {
      //         if (field.FieldStatus == USPUIFieldStatus.HIDE) {
      //           txtMember.Visibility = Visibility.Collapsed;
      //           btnMember.Visibility = Visibility.Collapsed;
      //         }
      //         else if (field.FieldStatus == USPUIFieldStatus.DISABLE) {
      //           OrderDetailsVm.IsMemberEnabled = false;

      //         }
      //         break;
      //       }
      //     case "Invoicefee":
      //       {
      //         if (field.FieldStatus == USPUIFieldStatus.HIDE) {
      //           chInvoiceFee.Visibility = Visibility.Collapsed;
      //         }
      //         else if (field.FieldStatus == USPUIFieldStatus.DISABLE) {
      //           OrderDetailsVm.IsInvoiceFeeEnabled = false;

      //         }
      //         break;
      //       }
      //     case "ArticleDelete":
      //       {
      //         if (field.FieldStatus == USPUIFieldStatus.HIDE) {
      //           OrderDetailsVm.BtnArticleDeleteVisible = false;
      //         }
      //         else if (field.FieldStatus == USPUIFieldStatus.DISABLE) {
      //           OrderDetailsVm.BtnArticleDeleteEnabled = true;

      //         }
      //         else {
      //           OrderDetailsVm.BtnArticleDeleteVisible = true;
      //           OrderDetailsVm.BtnArticleDeleteEnabled = true;
      //         }
      //         break;
      //       }

      //     case "OrderDetailsPrintInvoice":
      //       {
      //         if (field.FieldStatus == USPUIFieldStatus.HIDE) {
      //           btnPrint.Visibility = System.Windows.Visibility.Collapsed;
      //         }
      //         else if (field.FieldStatus == USPUIFieldStatus.DISABLE) {
      //           btnPrint.Visibility = System.Windows.Visibility.Visible;
      //           OrderDetailsVm.IsPrintButtonEnabled = false;
      //         }
      //         break;
      //       }
      //     case "OrderDetailsCheckOut":
      //       {
      //         if (field.FieldStatus == USPUIFieldStatus.HIDE) {
      //           btnCheckOut.Visibility = System.Windows.Visibility.Collapsed;
      //         }
      //         else if (field.FieldStatus == USPUIFieldStatus.DISABLE) {
      //           btnCheckOut.Visibility = System.Windows.Visibility.Visible;
      //           OrderDetailsVm.IsCheckOutEnabled = false;
      //         }
      //         break;
      //       }
      //   }
      // }
    } catch (Exception) { }
  }

  closeShop() {
    this.shopModel.close();
  }

  generateInvoice() {
    const tmpSelectedOrder = this.selectedOrder;

    if (tmpSelectedOrder != null && !tmpSelectedOrder.IsSponsored && (tmpSelectedOrder.Discount > 0 || tmpSelectedOrder.SponsoredAmount > 0)) {
      let messageTitle = '';
      let messageBody = '';
      this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this.translateService.get('MEMBERSHIP.MsgIvoiceWithSposoring').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('CONFIRM', {
          messageTitle: messageTitle,
          messageBody: messageBody,
          msgBoxId: 'INVOICE_WITH_SPONSORING'
        }
      );

    } else {
      let messageTitle = '';
      let messageBody = '';
      this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this.translateService.get('MEMBERSHIP.MsgInvoice').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('CONFIRM', {
          messageTitle: messageTitle,
          messageBody: messageBody,
          msgBoxId: 'MSG_INVOICE'
        }
      );
    }
  }

  closeShopAfterSave() {
    this.closeOrderDetailModel.emit();
    this.shopModel.close();
  }

  printOrder() {
    this.memberService.printOrder(this.selectedOrder.Id, ).pipe(takeUntil(this.destroy$)).subscribe(result => {
      PreloaderService.hidePreLoader();
      if (result.Data.FileParth.trim() !== '') {
        this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
      }
    });
  }

  onAmountUpdated(value: any, item: any) {
    if (item.IsActivityArticle) {
      this.selectedOrder.ActivityPrice = value;
    }
    this.UpdateAmount();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.shopModel) {
      this.shopModel.close();
    }
    if (this.addMemberView) {
      this.addMemberView.close();
    }
    if (this.entitySelectionModel) {
      this.entitySelectionModel.close();
    }
    if (this.orderlineModel) {
      this.orderlineModel.close();
    }
  }

  printfunc(invoiceno: any) {
    this.currentInvoice = invoiceno;
    this.economyService.GetInvoice(invoiceno, this.IsBrisIntegration, this.selectedMember.BranchId).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
      if (result) {
      const contentType = 'application/pdf',
      b64Data = result.Data;
      this.blob = this.b64toBlob(b64Data, contentType);
      this.blobUrl = URL.createObjectURL(this.blob); }
      });
  }

  b64toBlob = (b64Data, contentType= '', sliceSize= 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize),
        byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }
  const blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

openGenerateConfirmModal(content: any, invoicenum: any) {
  this.printfunc(invoicenum);
  this.generatePdfView = this.modalService.open(content, {width: '800'});
 }



PrintPDF() {
  const iframe = document.createElement('iframe');
  iframe.style.display = 'none';
  iframe.src = this.blobUrl;
  document.body.appendChild(iframe);
  iframe.contentWindow.print();
}

SavePDF() {
  saveAs(this.blob, this.currentInvoice + '.pdf');
}


}
