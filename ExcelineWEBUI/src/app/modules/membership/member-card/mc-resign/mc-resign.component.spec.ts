import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McResignComponent } from './mc-resign.component';

describe('McResignComponent', () => {
  let component: McResignComponent;
  let fixture: ComponentFixture<McResignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McResignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McResignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
