import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McXtraLogComponent } from './mc-xtra-log.component';

describe('McXtraLogComponent', () => {
  let component: McXtraLogComponent;
  let fixture: ComponentFixture<McXtraLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McXtraLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McXtraLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
