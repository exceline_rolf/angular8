import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { ExceMemberService } from '../../services/exce-member.service'
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-mc-xtra-log',
  templateUrl: './mc-xtra-log.component.html',
  styleUrls: ['./mc-xtra-log.component.scss']
})
export class McXtraLogComponent implements OnInit, OnDestroy {
  private xtraLog: any;
  private filteredXtraLogs: any;
  private itemResource: any;
  public items = [];
  public itemCount = 0;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private dateValue: any = '';
  private selectedMember: any;
  private _hit: number
  private typeValue: any = 'ALL';
  private isNew: any = true;
  public filter_value = '';
  limit = 25;
  private destroy$ = new Subject<void>();

  constructor(private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
  ) {
    this._hit = 1;
  }

  ngOnInit() {
    this.basicInfoService.currentMember.pipe(
      mergeMap(currentMember => {
        this.selectedMember = currentMember;
        return this.exceMemberService.getClaimExportStatusForMember(this.selectedMember.Id, this._hit, this.typeValue, this.dateValue)
      }),
      takeUntil(this.destroy$)
    ).subscribe( nextVal => {
      this.isNew = false;
      this.xtraLog = nextVal.Data;
      for (const xtraLog of this.xtraLog) {
        if (xtraLog.AtgStatus === -1) {
          xtraLog.AtgStatus = '';
        } else if (xtraLog.AtgStatus === 1) {
          xtraLog.AtgStatus = 'Approved';
        } else {
          xtraLog.AtgStatus = 'Deleted';
        }
        if (xtraLog.TransferStatus === -1) {
          xtraLog.TransferStatus = '';
        } else if (xtraLog.TransferStatus === 1) {
          xtraLog.TransferStatus = 'Success';
        } else {
          xtraLog.TransferStatus = 'Failed';
        }
        if (xtraLog.Direction === 1) {
          xtraLog.Direction = true;
        } else {
          xtraLog.Direction = false;
        }
      }
      this.itemResource = new DataTableResource(this.xtraLog);
      this.items = this.xtraLog;
      this.itemCount = Number(this.xtraLog.length);
    })


/*
    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          this.getXtraLogsForTable({ offset: 0, limit: this.limit });
        }
      }); */
  }

  filterInput(typeValue: any, contractValue: any) {
    this.filteredXtraLogs = this.xtraLog
    if (contractValue) {
      this.filteredXtraLogs = this.filterpipe.transform('ContractNo', 'TEXT', this.filteredXtraLogs, contractValue);
    }
    if (typeValue !== 'ALL') {
      this.filteredXtraLogs = this.filterpipe.transform('Category', 'SELECT', this.filteredXtraLogs, typeValue);
    }
    this.itemResource = new DataTableResource(this.filteredXtraLogs);
    this.itemResource.count().then(count => this.itemCount = count);
    this.itemResource.query(this.filteredXtraLogs).then(items => this.items = items);
  }


  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
    if (this.itemResource) {
      if (this.isNew) {
        this._hit = 1;
        this.getXtraLogsForTable(params);
      } else {
        this.handelPagination(params);
        this.getXtraLogsForTable(params);
      }
    }
  }

  handelPagination(params) {
    if ((this._hit * this.limit) === params.offset) {
      this._hit = this._hit + 1;
    } else {
      if (this._hit > 1) {
        this._hit = this._hit - 1;
      }
    }
  }

  getXtraLogsForTable(params) {
    this.exceMemberService.getClaimExportStatusForMember(this.selectedMember.Id, this._hit, this.typeValue, this.dateValue).pipe(takeUntil(this.destroy$)).subscribe
      (result => {
        if (result) {
          this.isNew = false;
          this.xtraLog = result.Data;
          for (const xtraLog of this.xtraLog) {
            if (xtraLog.AtgStatus === -1) {
              xtraLog.AtgStatus = '';
            } else if (xtraLog.AtgStatus === 1) {
              xtraLog.AtgStatus = 'Approved';
            } else {
              xtraLog.AtgStatus = 'Deleted';
            }
            if (xtraLog.TransferStatus === -1) {
              xtraLog.TransferStatus = '';
            } else if (xtraLog.TransferStatus === 1) {
              xtraLog.TransferStatus = 'Success';
            } else {
              xtraLog.TransferStatus = 'Failed';
            }
            if (xtraLog.Direction === 1) {
              xtraLog.Direction = true;
            } else {
              xtraLog.Direction = false;
            }
          }
          this.itemResource = new DataTableResource(this.xtraLog);
          this.items = this.xtraLog;
          this.itemCount = Number(this.xtraLog.length);
        }
      }
      )
  }

  dateValueAssign(dateValue: any) {
    this.dateValue = dateValue.date.year + '-' + dateValue.date.month + '-' + dateValue.date.day
  }

  filterFromButton(typeValue: any) {
    this.filter_value = null;
    this.isNew = true;
    this._hit = 1;
    this.typeValue = typeValue;
    this.reloadItems({ offset: 0, limit: this.limit })
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
