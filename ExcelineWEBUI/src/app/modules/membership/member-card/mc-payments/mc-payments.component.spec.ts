import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McPaymentsComponent } from './mc-payments.component';

describe('McPaymentsComponent', () => {
  let component: McPaymentsComponent;
  let fixture: ComponentFixture<McPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
