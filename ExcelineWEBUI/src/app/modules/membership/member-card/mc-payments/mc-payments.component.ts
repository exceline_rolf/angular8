import { ExcePdfViewerService } from './../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { PreloaderService } from './../../../../shared/services/preloader.service';
import { Extension } from './../../../../shared/Utills/Extensions';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ExceMemberService } from '../../services/exce-member.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { McBasicInfoService } from 'app/modules/membership/member-card/mc-basic-info/mc-basic-info.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { UsbModalRef } from 'app/shared/components/us-modal/modal-ref';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-mc-payments',
  templateUrl: './mc-payments.component.html',
  styleUrls: ['./mc-payments.component.scss']
})
export class McPaymentsComponent implements OnInit, OnDestroy {


  private _branchId: number = JSON.parse(Cookie.get('selectedBranch')).BranchId;
  private printForm: FormGroup;
  private _memberId: number; // = 25227;
  public filterDate: string;
  public filterInvoiceNo: number;
  private startDate: any;
  private endDate: any;
  paymentPrintModelRef: UsbModalRef;

  paymentHistory = [];
  comment: string;
  columDefs: any[];
  lang: any;

  public removePaymentForm: FormGroup;
  deleteType = 1;

  @ViewChild('paymentRemoveModal', { static: true }) paymentRemoveModal: ElementRef;

  // default norwegian
  names = {
    PaidDate: 'Betalt dato',
    PaidTime: 'Betalt tid',
    RegDate: 'Registrert dato',
    RegTime: 'Registrert tid',
    PaymentType: 'Betalingstype',
    InvoiceNo: 'Fakturanummer',
    Contract: 'Kontrakt',
    Orderno: 'Ordrenummer',
    Tempno: 'Malnummer',
    Amount: 'Betalingsbeløp',
    GymName: 'Senter',
    Employee: 'Ansatt'
  }

  colNamesNo = {
    PaidDate: 'Betalt dato',
    PaidTime: 'Betalt tid',
    RegDate: 'Registrert dato',
    RegTime: 'Registrert tid',
    PaymentType: 'Betalingstype',
    InvoiceNo: 'Fakturanummer',
    Contract: 'Kontrakt',
    Orderno: 'Ordrenummer',
    Tempno: 'Malnummer',
    Amount: 'Betalingsbeløp',
    GymName: 'Senter',
    Employee: 'Ansatt'
  }

  colNamesEn = {
    PaidDate: 'Paid date',
    PaidTime: 'Paid time',
    RegDate: 'Registration date',
    RegTime: 'Registration time',
    PaymentType: 'Payment type',
    InvoiceNo: 'Invoice number',
    Contract: 'Contract',
    Orderno: 'Order number',
    Tempno: 'Template number',
    Amount: 'Paid amount',
    GymName: 'Gym name',
    Employee: 'Employee'
  }
  gridApi: any;
  gridColumnApi: any;
  paymentId: any;
  model: void;
  content: ElementRef;
  paymentRemoveModelRef: UsbModalRef;
  private destroy$ = new Subject<void>();
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private basicInfoService: McBasicInfoService,
    private modalService: UsbModal, private fb: FormBuilder,
    private memberService: ExceMemberService, private translate: TranslateService,
    private excePdfViewerService: ExcePdfViewerService,
    private exceToolbarService: ExceToolbarService, private exceMessageService: ExceMessageService) {


    // this.startDate = this.dateAdd(new Date(), -1, 'months');
    // this.endDate = new Date();

    this.printForm = fb.group({
      'startDate': [{ date: Extension.GetFormatteredDate(this.startDate) }],
      'endDate': [{ date: Extension.GetFormatteredDate(this.endDate) }]
    });

    this.removePaymentForm = fb.group({
      'comment': [this.comment],
      'deleteType': [this.deleteType],
      'paymentId': [this.paymentId]
    })
  }

  ngOnInit() {
    // setting header names for ag grid by language
    this.lang = this.exceToolbarService.getSelectedLanguage()
    switch (this.lang.id) {
      case 'no':
        this.names = this.colNamesNo;
        break;
      case 'en':
        this.names = this.colNamesEn;
        break;
    }

    this.basicInfoService.currentMember.pipe(
      mergeMap(currentMember => {
        this._memberId = currentMember.Id;
        return this.memberService.getMemberPaymentsHistory(this._branchId, 1, this._memberId, 'ALL');
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.paymentHistory = [];
        result.Data.forEach(payment => {

          const paidDate = payment.PaymentDateTime.split('T');
          const pdelements = paidDate[0].split('-');
          payment.DisplayPaidDate = pdelements[2] + '.' + pdelements[1] + '.' + pdelements[0];

          const pdtime = paidDate[1].substring(0, 5)
          payment.DisplayPaidTime = pdtime;

          const regDate = payment.PaymentRegDateTime.split('T');
          const rdelements = regDate[0].split('-');
          payment.DisplayRegDate = rdelements[2] + '.' + rdelements[1] + '.' + rdelements[0];

          const rdtime = paidDate[1].substring(0, 5)
          payment.DisplayRegTime = rdtime;

          this.paymentHistory.push(payment);
        });
      }
    });

/*     this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this._memberId = member.Id;
          this.getMemberPaymentsHistory(this._branchId, 1, this._memberId, 'All');
        }
    }); */

    this.columDefs = [
      {headerName: this.names.PaidDate, field: 'DisplayPaidDate', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.PaidTime, field: 'DisplayPaidTime', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.RegDate, field: 'DisplayRegDate', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.RegTime, field: 'DisplayRegTime', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.PaymentType, field: 'PaymentType', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.InvoiceNo, field: 'InvoiceNo', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Contract, field: 'ContractNo', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Orderno, field: 'OrderNo', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Tempno, field: 'ContractTemplateNo', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Amount, field: 'PaymentAmount', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.GymName, field: 'GymName', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Employee, field: 'RegisteredEmpName', autoHeight: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: '', field: 'Slett', autoHeight: true, suppressFilter: true, suppressMenu: true, width: 80, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'align-content': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        return '<button class="btn btn-danger btn-icon-min"' +
        'title="\'Slett\'"><span class="icon-cancel"></span></button>'
      }}
    ]
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    params.api.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  cellClicked(event) {
    const payment = event.data;
    const column = event.column.colDef.field;
    switch (column) {
      case 'Slett':
      this.openCategoryModel(payment);
      break;
    }

  }


  getMemberPaymentsHistory(branchID: number, hit: number, memberId: number, paymentType: string) {
    this.memberService.getMemberPaymentsHistory(branchID, hit, memberId, paymentType).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.paymentHistory = [];

          result.Data.forEach(payment => {

            const paidDate = payment.PaymentDateTime.split('T');
            const pdelements = paidDate[0].split('-');
            payment.DisplayPaidDate = pdelements[2] + '.' + pdelements[1] + '.' + pdelements[0];

            const pdtime = paidDate[1].substring(0, 5)
            payment.DisplayPaidTime = pdtime;

            const regDate = payment.PaymentRegDateTime.split('T');
            const rdelements = regDate[0].split('-');
            payment.DisplayRegDate = rdelements[2] + '.' + rdelements[1] + '.' + rdelements[0];

            const rdtime = paidDate[1].substring(0, 5)
            payment.DisplayRegTime = rdtime;

            this.paymentHistory.push(payment);
          });
        }
      }
      )
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date !== undefined) {
      const tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  printPayment(content: any) {
    this.paymentPrintModelRef = this.modalService.open(content, { width: '300'});
  }

  printPayments() {
    const startDate = this.manipulatedate(this.printForm.value.startDate);
    const endDate = this.manipulatedate(this.printForm.value.endDate);
    this.memberService.printPayments(this._memberId, startDate, endDate).pipe(takeUntil(this.destroy$)).subscribe(result => {
      PreloaderService.hidePreLoader();
      if (result.Data.FileParth.trim() !== '') {
        this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
        this.closePrintPayments();
      }
    });
  }

  closePrintPayments() {
    if (this.paymentPrintModelRef) {
      this.paymentPrintModelRef.close();
    }
  }

  openCategoryModel(payment: any) {
    this.paymentId = payment.PaymentArItemNo;
    this.paymentRemoveModelRef = this.modalService.open(this.paymentRemoveModal, { width : '500'});
  }

  setPaymentRemoveType(memberType: any) {
    this.deleteType = memberType;
  }

  deletePayment(value) {
    this.memberService.cancelPayment({ PaymentId: this.paymentId, KeepPayment: this.deleteType === 1, Comment: this.comment }).pipe(
      mergeMap(next => {
        this.paymentRemoveModelRef.close();
        if (next.Data === 2) {
          let messageTitle = '';
          let messageBody = '';
          (this.translate.get('MEMBERSHIP.MsgPaymentDelete').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
          (this.translate.get('MEMBERSHIP.MsgPaymentDeleteDailySettlement').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
          this.exceMessageService.openMessageBox('ERROR', {
            messageTitle: messageTitle,
            messageBody: messageBody
          });
        }
        return this.memberService.getMemberPaymentsHistory(this._branchId, 1, this._memberId, 'ALL');
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.paymentHistory = [];

        result.Data.forEach(payment => {

          const paidDate = payment.PaymentDateTime.split('T');
          const pdelements = paidDate[0].split('-');
          payment.DisplayPaidDate = pdelements[2] + '.' + pdelements[1] + '.' + pdelements[0];

          const pdtime = paidDate[1].substring(0, 5)
          payment.DisplayPaidTime = pdtime;

          const regDate = payment.PaymentRegDateTime.split('T');
          const rdelements = regDate[0].split('-');
          payment.DisplayRegDate = rdelements[2] + '.' + rdelements[1] + '.' + rdelements[0];

          const rdtime = paidDate[1].substring(0, 5)
          payment.DisplayRegTime = rdtime;

          this.paymentHistory.push(payment);
        });
      }
    });
  }

  closePaymentRemoveForm() {
    this.paymentRemoveModelRef.close();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
     if (this.paymentPrintModelRef) {
      this.paymentPrintModelRef.close();
    }
  }

}
