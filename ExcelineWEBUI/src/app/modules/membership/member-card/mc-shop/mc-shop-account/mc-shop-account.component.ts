import { ExceLoginService } from '../../../../login/exce-login/exce-login.service';
import { ExceToolbarService } from '../../../../common/exce-toolbar/exce-toolbar.service';
import { ExceShopService } from '../../../../shop/services/exce-shop.service';
import { ShopHomeService } from '../../../../shop/shop-home/shop-home.service';
import { McBasicInfoService } from '../../mc-basic-info/mc-basic-info.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { ActivatedRoute, Router } from '@angular/router';
import { ExceGymSetting } from 'app/shared/SystemObjects/Common/ExceGymSetting';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { MemberRole } from 'app/shared/enums/us-enum';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-mc-shop-account',
  templateUrl: './mc-shop-account.component.html',
  styleUrls: ['./mc-shop-account.component.scss']
})

export class McShopAccountComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  branchId: any;
  selectedMember: any;
  selectedGymsettings = [
    'EcoNegativeOnAccount',
    'EcoSMSInvoiceShop',
    'EcoIsPrintInvoiceInShop',
    'EcoIsPrintReceiptInShop',
    'EcoIsShopOnNextOrder',
    'EcoIsOnAccount',
    'EcoIsDefaultCustomerAvailable',
    'EcoIsPayButtonsAvailable',
    'EcoIsPettyCashSameAsUserAllowed',
    'OtherIsShopAvailable',
    'OtherLoginShop',
    'HwIsShopLoginWithCard',
    'ReqMemberListRequired',
    'OtherIsLogoutFromShopAfterSale',
    'EcoIsBankTerminalIntegrated',
    'EcoIsDailySettlementForAll',
    'OtherIsValidateShopGymId'
  ];
  exceGymSetting: ExceGymSetting = new ExceGymSetting();
  shopLoginData?: any = {};
  locale: string;
  creditaccountItemList: any[];
  debitaccountItemList: any[];
  memberShopAccount: any;
  shopPaymentView: any;
  constructor(
    private shopService: ExceShopService,
    private shopHomeService: ShopHomeService,
    private basicInfoService: McBasicInfoService,
    private adminService: AdminService,
    private route: ActivatedRoute,
    private router: Router,
    private exceLoginService: ExceLoginService,
    private toolbarService: ExceToolbarService,
    private modalService: UsbModal
  ) { }

  ngOnInit() {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );

    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );

    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          member.IntCustId = member.CustId;
          this.shopHomeService.$selectedMember = member;
          this.shopHomeService.$priceType = 'MEMBER';
          this.GetMemberShopAccount();
        }
      }
    );

    this.shopService.GetSelectedGymSettings({ GymSetColNames: this.selectedGymsettings, IsGymSettings: true }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {

        for (const key in res.Data) {
          if (res.Data.hasOwnProperty(key)) {
            switch (key) {
              case 'EcoNegativeOnAccount':
                this.exceGymSetting.OnAccountNegativeVale = res.Data[key];
                break;
              case 'EcoIsShopOnNextOrder':
                this.exceGymSetting.IsShopOnNextOrder = res.Data[key];
                break;
              case 'EcoIsDailySettlementForAll':
                this.exceGymSetting.IsDailySettlementForAll = res.Data[key];
                break;
              case 'EcoIsOnAccount':
                this.exceGymSetting.IsOnAccount = res.Data[key];
                break;
              case 'EcoIsDefaultCustomerAvailable':
                this.exceGymSetting.IsDefaultCustomerAvailable = res.Data[key];
                break;
              case 'EcoIsPayButtonsAvailable':
                this.exceGymSetting.IsPayButtonsAvailable = res.Data[key];
                break;
              case 'EcoIsPettyCashSameAsUserAllowed':
                this.exceGymSetting.IsPettyCashSameAsUserAllowed = res.Data[key];
                break;
              case 'EcoIsPrintInvoiceInShop':
                this.exceGymSetting.IsPrintInvoiceInShop = res.Data[key];
                break;
              case 'EcoIsPrintReceiptInShop':
                this.exceGymSetting.IsPrintReceiptInShop = res.Data[key];
                break;
              case 'OtherIsShopAvailable':
                this.exceGymSetting.IsShopAvailable = res.Data[key];
                break;
              case 'OtherLoginShop':
                this.exceGymSetting.IsShopLoginNeeded = res.Data[key];
                break;
              case 'HwIsShopLoginWithCard':
                this.exceGymSetting.IsShopLoginWithCard = res.Data[key];
                break;
              case 'ReqMemberListRequired':
                this.exceGymSetting.MemberRequired = res.Data[key];
                break;
              case 'OtherIsLogoutFromShopAfterSale':
                this.exceGymSetting.IsLogoutFromShopAfterSale = res.Data[key];
                break;
              case 'EcoSMSInvoiceShop':
                this.exceGymSetting.IsSMSInvoiceShop = res.Data[key];
                break;
              case 'EcoIsBankTerminalIntegrated':
                this.exceGymSetting.IsBankTerminalIntegrated = res.Data[key];
                break;
              case 'OtherIsValidateShopGymId':
                this.exceGymSetting.IsValidateShopGymId = res.Data[key];
                break;
            }

          }
        }
        this.shopLoginData.GymSetting = this.exceGymSetting;
        this.shopService.GetSalesPoint().pipe(takeUntil(this.destroy$)).subscribe(
          salePoints => {
            this.shopLoginData.SalePoint = salePoints.Data[0];
            this.adminService.getGymSettings('HARDWARE', null).pipe(takeUntil(this.destroy$)).subscribe(
              hwSetting => {
                this.shopLoginData.HardwareProfileList = hwSetting.Data;
                this.shopLoginData.HardwareProfileList.forEach(hwProfile => {
                  if (this.shopLoginData.SalePoint != null &&
                    this.shopLoginData.SalePoint.HardwareProfileId === hwProfile.Id) {
                    this.shopLoginData.SelectedHardwareProfile = hwProfile;
                    this.shopLoginData.HardwareProfileId = hwProfile.Id;
                  } else {
                    this.shopLoginData.SelectedHardwareProfile = null;
                    this.shopLoginData.HardwareProfileId = null;
                  }
                  this.shopHomeService.$shopLoginData = this.shopLoginData;

                });
              },
              error => {

              });
          },
          error => {

          });



      },
      error => {

      }
    )
    // this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(member => {
    // });
  }
  GetMemberShopAccount(): void {
    this.shopService.GetMemberShopAccount({ memberId: this.selectedMember.Id }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.memberShopAccount = res.Data;
      }, err => {

      }
    )
  }

  gotoShopAccount() {
    this.router.navigate(['membership/card/' + this.selectedMember.Id + '/' + this.branchId + '/' + MemberRole[this.selectedMember.Role] + '/purchase-history']);
  }

  openShopPayment(content) {
    this.shopService.GetMemberShopAccountsForEntityType({ memberId: this.selectedMember.Id, entityType: 'MEM' }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.shopHomeService.$shopAccount = res.Data;
        this.shopHomeService.$isOnAccountPayment = true;
        this.shopPaymentView = this.modalService.open(content);
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.shopPaymentView) {
      this.shopPaymentView.close();
    }
  }

  closePaymentView(): void {
    this.GetMemberShopAccount();
    this.shopPaymentView.close();
  }


}
