import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McCommunicationLogComponent } from './mc-communication-log.component';

describe('McCommunicationLogComponent', () => {
  let component: McCommunicationLogComponent;
  let fixture: ComponentFixture<McCommunicationLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McCommunicationLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McCommunicationLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
