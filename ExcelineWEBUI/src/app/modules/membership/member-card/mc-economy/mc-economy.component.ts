import { Component, OnInit, OnDestroy } from '@angular/core';
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ExceMemberService } from '../../services/exce-member.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { MemberSearchType, ColumnDataType, EntitySelectionType, MemberRole } from 'app/shared/enums/us-enum';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { MemberCardService } from 'app/modules/membership/member-card/member-card.service';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { BankAccountValidator } from 'app/shared/directives/exce-error/util/validators/bank-account-validator';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { AdminService } from '../../../admin/services/admin.service';
import { Router} from '@angular/router';
import { Subject } from 'rxjs';
import { Subscription } from 'rxjs';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service'
import { takeUntil } from 'rxjs/operators';



// const now = new Date();

@Component({
  selector: 'mc-economy',
  templateUrl: './mc-economy.component.html',
  styleUrls: ['./mc-economy.component.scss']
})
export class McEconomyComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  IsVisibleAgressoId: boolean;
  private _smsInvoice: any;
  private _invoiceChange: any;
  private _sponsoringId: any;
  private _guardianLst = [];
  private _guardianName: any;
  private _addNewMemberView: any;
  private _sponsorDelModal: any;
  itemCount: number;
  entityItems = [];
  economyDetails: any;
  selectedMember: any;
  memberForm: FormGroup;
  isAddBtnVisible = true;
  isBasicInforVisible = true;
  isBranchVisible = true;
  isStatusVisible = true;
  isRoleVisible = true;
  isFormChanged = false;
  entitySelectionViewTitle: string;
  isDisabledRole = false;
  defaultRole = 'ALL'
  multipleRowSelect = false;
  isDbPagination = true;
  columns: any;
  entitySelectionType: string;
  memberSelectionView: any;
  entitySearchType: string;
  loggedBranchId: number;
  employeeCategoryList = [];
  auotocompleteItems = [{ id: 'NAME', name: 'NAMEE :', isNumber: false }, { id: 'ID', name: 'Idd :', isNumber: true }];
  IsDelBtnVisibleSpo: boolean;
  selectedSponserCustId: any;
  selectedSponserName: any;
  selectedSponsorStartDate: any;
  selectedSponsorEndDate: any;
  IsDelBtnVisibleGur: boolean;
  selectedGuardianCustId: any;
  selectedGuardianLastName: any;
  selectedGuardianFirstName: any;
  selectedGuardianAddress1: any;
  selectedGuardianAddress2: any;
  selectedGuardianZip: any;
  selectedGuardianCity: any;
  selectedGuardianMobile: any;
  selectedGuardianGuardianList = [];
  originalEconomyDetails: any;
  gudList?: any[] = [];
  formErrors = {
    'AccountNo': ''
  };
  validationMessages = {
    'AccountNo': {
      'BankAccount': 'Invalid Bank Account No.'
    }
  };
  isUsingBrisIntegration: boolean;

  changePrePaidView: any;
  prePaidChangeOption = '1';
  PPform: FormGroup;
  _ppAccountErrorModal: void;
  changePPAcountAmount: number;

  isSponsorSelected: boolean;
  /* ngModel start */
  selectedCategory: any;
/* ngModel end */
  type: string;
  isShowInfo: boolean;
  message: string;

  constructor(
    private _basicInfoService: McBasicInfoService,
    private _exceMemberService: ExceMemberService,
    private _fb: FormBuilder,
    private router: Router,
    private _modalService: UsbModal,
    private _translateService: TranslateService,
    private _exceMessageService: ExceMessageService,
    private _memberCardService: MemberCardService,
    private _loginService: ExceLoginService,
    private adminService: AdminService,
    private _exceBreadCrumbService: ExceBreadcrumbService
  ) {
    this.memberForm = _fb.group({
      // 'AccountNo': [null, [BankAccountValidator.BankAccount]],
      'AccountNo': [null],
      'InvoiceChage': [null],
      'SmsInvoice': [null],
      'NextDueDate': [{ value: null, disabled: true }],
      'LastDueDate': [{ value: null, disabled: true }],
      'DueBalance': [{ value: null, disabled: true }],
      'CreditBalance': [{ value: null, disabled: true }],
      'CreditPeriod': [null],
      'BilledToDate': [{ value: null, disabled: true }],
      'BilledLast365': [{ value: null, disabled: true }],
      'ShopPchtoDate': [{ value: null, disabled: true }],
      'ShoppchLast365': [{ value: null, disabled: true }],
      'OnAccountBalance': [{ value: null, disabled: true }],
      'GuardianId': [null],
      'GuardianCustId': [this.selectedGuardianCustId],
      'GuardianLastName': [this.selectedGuardianLastName],
      'GuardianFirstName': [this.selectedGuardianFirstName],
      'GuardianAddress1': [this.selectedGuardianAddress1],
      'GuardianAddress2': [this.selectedGuardianAddress2],
      'GuardianZipCode': [this.selectedGuardianZip],
      'GuardianZipName': [this.selectedGuardianCity],
      'GuardianMobile': [this.selectedGuardianMobile],
      'GuardianLst': [this.selectedGuardianGuardianList],
      'SponserId': [null],
      'SponserCustId': [this.selectedSponserCustId],
      'SponserName': [this.selectedSponserName],
      'SponsorStartDate': [ this.selectedSponsorStartDate],
      'SponsorEndDate': [this.selectedSponsorEndDate],
      'EmployeeCategoryList': [null],
      'EmployeeCategoryForSponser' : [this.selectedCategory],
      'DiscountCategoy': [null],
      'EmployeeNo': [null],
      'Ref': [null],
      'DueDate': [null],
      'PrepaidBalance': [{
        value: null,
        disabled: true
      }],
      'AgressoId': [null]
    });

    this.PPform =  _fb.group({
      'PPval': [{value: 0}]
    })

    this._exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$
        )).subscribe(value => {
      if (value.id === 'DELETE_SPO') {
        this.sponsorYesHandler();
      }
      if (value.id === 'DELETE_GUA') {
        this.guardianYesHandler();
      }
    }, error => {}, () => {

    });
  }

  ngOnInit() {
    this.loggedBranchId = this._loginService.SelectedBranch.BranchId;
    this.adminService.GetGymCompanySettings('other').pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      this.isUsingBrisIntegration = result.Data.IsBrisIntegrated;
    }, null, null);

    this._basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
      ).subscribe(member => {
      this.selectedMember = member;
        if (this.selectedMember) {
          this.IsVisibleAgressoId = member.IsBrisIntegrated
        }
        if (member) {
          if (member.SponserCustId || member.SponserCustId === null) {
            this.IsDelBtnVisibleSpo = false;
          } else {
            this.IsDelBtnVisibleSpo = true;
          }
          if (member.GuardianCustId === '' || member.GuardianCustId === null) {
            this.IsDelBtnVisibleGur = false;
          } else {
            this.IsDelBtnVisibleGur = true;
          }
        this._exceMemberService.getEconomyDetails(member.Id).pipe(
          takeUntil(this.destroy$)
        ).subscribe( result => {
          if (result) {
            this.economyDetails = result.Data;
            this.originalEconomyDetails = result.Data;
            this.economyDetails.EmployeeCategoryList.forEach(item => {
              this.employeeCategoryList.push(item);
            });
            this.selectedCategory = this.economyDetails.EmployeeCategoryForSponser;
/*             this.memberForm.patchValue({
              EmployeeCategoryForSponser : this.selectedCategory // might cause NgChangeAfterCheck error
            }) */
            // this.memberForm.controls.EmployeeCategoryForSponser.patchValue(this.selectedCategory);
            this._sponsoringId = this.economyDetails.SponsoringId;
            this.isSponsorSelected = (this._sponsoringId === '' ? false : true)
            if (this.economyDetails.NextDueDate === null) {
              this.memberForm.patchValue({ NextDueDate: null });
            } else {
              const nextduedate = new Date(this.economyDetails.NextDueDate);
              this.economyDetails.NextDueDate = {
                date: {
                  year: nextduedate.getFullYear(),
                  month: nextduedate.getMonth() + 1,
                  day: nextduedate.getDate()
                }
              };
            }
            if (this.economyDetails.LastDueDate === null) {
              this.memberForm.patchValue({ LastDueDate: null });
            } else {
              const lastduedate = new Date(this.economyDetails.LastDueDate);
              this.economyDetails.LastDueDate = {
                date: {
                  year: lastduedate.getFullYear(),
                  month: lastduedate.getMonth() + 1,
                  day: lastduedate.getDate()
                }
              };
            }
            if (this._sponsoringId === -1 || this.economyDetails.SponsorStartDate === null) {
              this.memberForm.patchValue({ SponsorStartDate: null });
            } else {
              const sponsorStartDate = new Date(this.economyDetails.SponsorStartDate);
              this.economyDetails.SponsorStartDate = {
                date: {
                  year: sponsorStartDate.getFullYear(),
                  month: sponsorStartDate.getMonth() + 1,
                  day: sponsorStartDate.getDate()
                }
              }
            }

            if (this._sponsoringId === -1 || this.economyDetails.SponsorEndDate === null) {
              this.memberForm.patchValue({ SponsorEndDate: null });
            } else {
              const sponsorEndDate = new Date(this.economyDetails.SponsorEndDate);
              this.economyDetails.SponsorEndDate = {
                date: {
                  year: sponsorEndDate.getFullYear(),
                  month: sponsorEndDate.getMonth() + 1,
                  day: sponsorEndDate.getDate()
                }
              };
            }

            if (this.economyDetails.DueDate > 0) {
              this.economyDetails.DueDate = this.economyDetails.DueDate;
            } else {
              this.economyDetails.DueDate = '';
            }
            if (this.economyDetails.CreditPeriod > 0) {
              this.economyDetails.CreditPeriod = this.economyDetails.CreditPeriod;
            } else {
              this.economyDetails.CreditPeriod = '';
            }
            this.economyDetails.GuardianLst.forEach(item => {
              this._guardianLst = item.Name;
            });
            this.economyDetails.GuardianLst.forEach(element => {
              this.selectedGuardianGuardianList.push(element);
            });
            this.memberForm.patchValue(this.economyDetails);

            this.memberForm.controls['SponsorStartDate'].disable({
              onlySelf: true,
              emitEvent: false
          });
          }
        }, error => {
            console.log(error)
        },
        () => {

        });
      }
    // get form Value change to visible or hide delete button
    this.memberForm.valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(data => {
      if (data.SponserCustId === '' || data.SponserCustId === null) {
        this.IsDelBtnVisibleSpo = false;
      } else {
        this.IsDelBtnVisibleSpo = true;
      }
      if (data.GuardianCustId === '' || data.GuardianCustId === null) {
        this.IsDelBtnVisibleGur = false;
      } else {
        this.IsDelBtnVisibleGur = true;
      }
    }, error => {},
    () => {

    })
  });
}

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();

    if (this.memberSelectionView) {
      this.memberSelectionView.close();
    }
    if (this._addNewMemberView) {
      this._addNewMemberView.close();
    }
  }

  // get InvoiceChange radio button change value
  invoiceChangeStatus(value) {
    this.changeMadeInForm();
    this._invoiceChange = value;
  }

  // get SmsInvoice radio button change value
  smsInvoiceStatus(value) {
    this.changeMadeInForm();
    this._smsInvoice = value;
  }

  // called when parameters is changed
  changeMadeInForm() {
    this.isFormChanged = true;
  }

  resetEconomyDetails() {
    this.selectedGuardianGuardianList = [];
    this.employeeCategoryList = [];
    this.isShowInfo = false; // Hiding saving reminder
    this._basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      member => {
        this.selectedMember = member;
        if (this.selectedMember) {
          this.IsVisibleAgressoId = member.IsBrisIntegrated
        }
        if (member) {
          if (member.SponserCustId || member.SponserCustId === null) {
            this.IsDelBtnVisibleSpo = false;
          } else {
            this.IsDelBtnVisibleSpo = true;
          }
          if (member.GuardianCustId === '' || member.GuardianCustId === null) {
            this.IsDelBtnVisibleGur = false;
          } else {
            this.IsDelBtnVisibleGur = true;
          }

          this._exceMemberService.getEconomyDetails(member.Id).pipe(
            takeUntil(this.destroy$)
            ).subscribe(result => {
            if (result) {
              this.economyDetails = result.Data;
              this.originalEconomyDetails = result.Data;
              this.economyDetails.EmployeeCategoryList.forEach(item => {
                this.employeeCategoryList.push(item);
              });
              this._sponsoringId = this.economyDetails.SponsoringId;
              this.isSponsorSelected = (this._sponsoringId === '' ? false : true)
              if (this.economyDetails.NextDueDate === null) {
                this.memberForm.patchValue({ NextDueDate: null });
              } else {
                const nextduedate = new Date(this.economyDetails.NextDueDate);
                this.economyDetails.NextDueDate = {
                  date: {
                    year: nextduedate.getFullYear(),
                    month: nextduedate.getMonth() + 1,
                    day: nextduedate.getDate()
                  }
                };
              }
              if (this.economyDetails.LastDueDate === null) {
                this.memberForm.patchValue({ LastDueDate: null });
              } else {
                const lastduedate = new Date(this.economyDetails.LastDueDate);
                this.economyDetails.LastDueDate = {
                  date: {
                    year: lastduedate.getFullYear(),
                    month: lastduedate.getMonth() + 1,
                    day: lastduedate.getDate()
                  }
                };
              }
              if (this.economyDetails.SponsorStartDate === null) {
                this.memberForm.patchValue({ SponsorStartDate: null });
              } else {
                const sponsorstartdate = new Date(this.economyDetails.SponsorStartDate);
                this.economyDetails.SponsorStartDate = {
                  date: {
                    year: sponsorstartdate.getFullYear(),
                    month: sponsorstartdate.getMonth() + 1,
                    day: sponsorstartdate.getDate()
                  }
                };
              }

              if (this.economyDetails.SponsorEndDate === null) {
                this.memberForm.patchValue({ SponsorEndDate: null });
              } else {
                const sponsorendate = new Date(this.economyDetails.SponsorEndDate);
                this.economyDetails.SponsorEndDate = {
                  date: {
                    year: sponsorendate.getFullYear(),
                    month: sponsorendate.getMonth() + 1,
                    day: sponsorendate.getDate()
                  }
                };
              }

              if (this.economyDetails.DueDate > 0) {
                this.economyDetails.DueDate = this.economyDetails.DueDate;
              } else {
                this.economyDetails.DueDate = '';
              }
              if (this.economyDetails.CreditPeriod > 0) {
                this.economyDetails.CreditPeriod = this.economyDetails.CreditPeriod;
              } else {
                this.economyDetails.CreditPeriod = '';
              }
              this.economyDetails.GuardianLst.forEach(item => {
                this._guardianLst = item.Name;
              });
              this.economyDetails.GuardianLst.forEach(element => {
                this.selectedGuardianGuardianList.push(element);
              });
              this.memberForm.patchValue(this.economyDetails);
            }
          }, error => {},
          () => {

          });
        }
      }, error => {},
      () => {

      });

    // get form Value change to visible or hide delete button
    this.memberForm.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data.SponserCustId === '' || data.SponserCustId === null) {
        this.IsDelBtnVisibleSpo = false;
      } else {
        this.IsDelBtnVisibleSpo = true;
      }
      if (data.GuardianCustId === '' || data.GuardianCustId === null) {
        this.IsDelBtnVisibleGur = false;
      } else {
        this.IsDelBtnVisibleGur = true;
      }
    }, error => {},
    () => {}
    );
    this.isFormChanged = false;
  }

  routeToGuardian() {
    if ( this.economyDetails.GuardianId === 0 ) {
      return;
    }
    const url = '/membership/card/' + this.economyDetails.GuardianId + '/' + this.economyDetails.GuardianBranchId + '/' + 'MEM';
    this._exceBreadCrumbService.addBreadCumbItem.next({
      name: this.economyDetails.Name,
      url: url
    });
    this.router.navigate([url]);
  }

  routeToSponsor() {
    if ( this.economyDetails.SponserId === -1 ) {
      return;
    }
    const url = '/membership/card/' + this.economyDetails.SponserId + '/' + this.economyDetails.SponsorBranchId + '/' + 'MEM';
    this._exceBreadCrumbService.addBreadCumbItem.next({
      name: this.economyDetails.SponserName,
      url: url
    });
    this.router.navigate([url]);
  }

  // for member selection view
  selectMember(content: any, searchType: any) {
    this._translateService.get('COMMON.MemberSelection').pipe(
      takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
    this.entitySearchType = searchType;
    this.isBranchVisible = true;
    this.isRoleVisible = true;
    this.isStatusVisible = true;
    this.isDbPagination = true;
    this.isBasicInforVisible = true;
    this.defaultRole = 'ALL'
    this.multipleRowSelect = false;
    this.columns = [];

    if (searchType === 'SPONSORS') {
      this.isAddBtnVisible = false;
    }
    if (searchType === 'GUARDIAN') {
      this.isAddBtnVisible = true;
      this.isDisabledRole = false;
    }
    this.entitySelectionType = EntitySelectionType[EntitySelectionType.MEM];
    if (searchType === MemberSearchType[MemberSearchType.GUARDIAN] || searchType === MemberSearchType[MemberSearchType.SPONSORS]) {
      this.columns = [
        { property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DATE },
        { property: 'Mobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
      ];
    }

    if (searchType === MemberSearchType[MemberSearchType.SPONSORS]) {
      this.defaultRole = 'SPO'
      this.isDisabledRole = true;
    } else {
      this.isDisabledRole = false;
    }
    this.memberSelectionView = this._modalService.open(content, {});
  }

  closeMemberSelectionView() {
    this.memberSelectionView.close();
  }

  // for add new member popup
  addNewMember(item: any) {
    switch (this.entitySearchType) {
      case MemberSearchType[MemberSearchType.GUARDIAN]:
        this.memberForm.patchValue({
          GuardianId: item.Id,
          GuardianCustId: item.CustId
        });
        break;
      case MemberSearchType[MemberSearchType.SPONSORS]:
        this.memberForm.patchValue({
          SponserId: item.Id,
          SponserCustId: item.CustId,
          SponserName: item.Name
        });
        break;
    }
    this._addNewMemberView.close();
  }

  // Search a member in member selection
  searchMember(val: any): void {
    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.MEM]:
        PreloaderService.showPreLoader();
        this._exceMemberService.getMembers(this.loggedBranchId, val.searchVal, val.statusSelected.id,
          this.entitySearchType, val.roleSelected.id, val.hit, false,
          false, '').subscribe(result => {
            PreloaderService.hidePreLoader();
            if (result) {
              this.entityItems = result.Data;
              this.itemCount = result.Data.length;
            } else {
            }
          }, err => {
            PreloaderService.hidePreLoader();
          });
        break;
    }
  }

  // get Add Member View
  addMemberView(content: any) {
    this._addNewMemberView = this._modalService.open(content, { width: '800' });
    this.memberSelectionView.close();
  }

  closeAddMemberView() {
    this._addNewMemberView.close();
  }

  // Guardian selected or Sponsor Selected
  memberSelected(item) {
    if (this.entitySelectionType === EntitySelectionType[EntitySelectionType.MEM]) {
      switch (this.entitySearchType) {
        case MemberSearchType[MemberSearchType.GUARDIAN]:
          this.memberForm.patchValue({
            GuardianId: item.Id,
            GuardianCustId: item.CustId,
            GuardianName: item.Name,
            GuardianLastName: item.LastName,
            GuardianFirstName: item.FirstName,
            GuardianAddress1: item.Address1,
            GuardianAddress2: item.Address2,
            GuardianZipCode: item.ZipCode,
            GuardianZipName: item.ZipName,
            GuardianMobile: item.Mobile
          });
          this.selectedMember.GuardianId = item.Id;
          this.economyDetails.GuardianId = item.Id;
          this.economyDetails.GuardianBranchId = item.BranchID;
          break;


        case MemberSearchType[MemberSearchType.SPONSORS]:
        this.isSponsorSelected = true;
          const now = new Date();
          this.memberForm.patchValue({
            SponserId: item.Id,
            SponserCustId: item.CustId,
            SponserName: item.Name,
            SponsorEndDate : {
              date: {
                year: now.getFullYear() + 100,
                month: now.getMonth() + 1,
                day: now.getDate()
              }
            },
            SponsorStartDate : {
              date: {
                year: now.getFullYear(),
                month: now.getMonth() + 1,
                day: now.getDate()
              }
            }
          });
          this.employeeCategoryList = [];
          this._exceMemberService.getEmployeeCategoryBySponsorId(this.loggedBranchId, item.Id).pipe(takeUntil(this.destroy$)).subscribe(result => {
              if (result) {
                result.Data.EmployeeCategoryList.forEach(catItem => {
                  this.employeeCategoryList.push(catItem);
                });
              }
          });
          this.economyDetails.SponserId = item.Id;
          this._sponsoringId = -1;
          this.economyDetails.SponsorBranchId = item.BranchID;
          break;
      }
      this._translateService.get('MEMBERSHIP.SaveToConfirmChange').pipe(
        takeUntil(this.destroy$)
      ).subscribe(e => this.message = e);
      this.type = 'INFO';
      this.isShowInfo = true;
      this.changeMadeInForm();
      this.memberSelectionView.close();
    }
  }

  saveMemberEconomyDetails(value) {
    const memberEconomyDetails = this.memberForm.getRawValue();
    this.selectedMember.AccountNo = memberEconomyDetails.AccountNo;
    this.selectedMember.InvoiceChage = this._invoiceChange;
    this.selectedMember.SmsInvoice = this._smsInvoice;
    this.selectedMember.CreditPeriod = memberEconomyDetails.CreditPeriod;
    this.selectedMember.GuardianId = memberEconomyDetails.GuardianId;
    this.selectedMember.GuardianCustId = memberEconomyDetails.GuardianCustId;
    this.selectedMember.GuardianLastName = memberEconomyDetails.GuardianLastName;
    this.selectedMember.GuardianFirstName = memberEconomyDetails.GuardianFirstName;
    this.selectedMember.GuardianAddress1 = memberEconomyDetails.GuardianAddress1;
    this.selectedMember.GuardianAddress2 = memberEconomyDetails.GuardianAddress2;
    this.selectedMember.GuardianZipCode = memberEconomyDetails.GuardianZipCode;
    this.selectedMember.GuardianZipName = memberEconomyDetails.GuardianZipName;
    this.selectedMember.GuardianMobile = memberEconomyDetails.GuardianMobile;
    this.selectedMember.SponserId = memberEconomyDetails.SponserId;
    this.selectedMember.SponsoringId = this._sponsoringId;
    this.selectedMember.SponserCustId = memberEconomyDetails.SponserCustId;
    this.selectedMember.SponserName = memberEconomyDetails.SponserName;
    this.selectedMember.SponsorStartDate = this.dateConverter(memberEconomyDetails.SponsorStartDate)
    this.selectedMember.SponsorEndDate = this.dateConverter(memberEconomyDetails.SponsorEndDate);
    this.selectedMember.EmployeeCategoryList = memberEconomyDetails.EmployeeCategoryList;
    this.selectedMember.EmployeeCategoryForSponser = this.selectedCategory;
    this.selectedMember.EmployeeNo = memberEconomyDetails.EmployeeNo;
    this.selectedMember.Ref = memberEconomyDetails.Ref;
    this.selectedMember.DueDate = memberEconomyDetails.DueDate;
    this.isShowInfo = false; // Hiding save reminder
    this._exceMemberService.saveEconomyDetails(this.selectedMember).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this._guardianName = this.selectedMember.GuardianFirstName + ' ' + this.selectedMember.GuardianLastName;
        this._memberCardService.SelectedGuardian = {
          GuardianCustId: memberEconomyDetails.GuardianCustId,
          GuardianName: this._guardianName
        }
        // Not used
        /*this._exceMemberService.changePPAccount(this.prePaidChangeOption, this.changePPAcountAmount, this.selectedMember.Id, this.loggedBranchId).pipe(takeUntil(this.destroy$)).subscribe(
          newBalance => {
            this.memberForm.patchValue({
              'PrepaidBalance': newBalance.Data
            })
          }
        )*/
      }
      this._translateService.get('MEMBERSHIP.SaveSuccessful').pipe(takeUntil(this.destroy$)).subscribe(e => this.message = e);
        this.type = 'SUCCESS';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
    }, error => {
      this._translateService.get('MEMBERSHIP.SaveNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(e => this.message = e);
      this.type = 'DANGER';
      this.isShowInfo = true;
      setTimeout(() => {
        this.isShowInfo = false;
      }, 4000);
    });
  }

  deltaSponsor(sponsCat: any) {
    this.selectedCategory = this.employeeCategoryList.find(cat => cat.Id === sponsCat.Id)
}
  compareFn(c1: any, c2: any) {
    return c1 && c2 && c1.Id === c2.Id;
  }
  dateConverter(dateInput: any) {
    if (dateInput !== null) {
      if (dateInput.date !== undefined) {
        const tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
        return moment(tempDate).format('YYYY-MM-DD').toString();
      } else if (dateInput !== undefined) {
        if (dateInput.date === undefined) {
          return
        }
        const date = dateInput.split('T')
        return date[0].toString();
      }
    }
  }

  // Delete selected Sponsor fron the View
  deleteSelectedSponsor(selectedSponserCustId) {
    this.isSponsorSelected = false;
    if (selectedSponserCustId !== null) {
      let messageTitle = '';
      let messageBody = '';
      this._translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this._translateService.get('MEMBERSHIP.ConfirmDelete').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this._sponsorDelModal = this._exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: messageTitle,
          messageBody: messageBody,
          msgBoxId: 'DELETE_SPO'
        });
      this.changeMadeInForm();
    }
  }

  // Clear all Sponsor fields
  sponsorYesHandler() {
    this.selectedSponserCustId = '';
    this.selectedSponserName = '';
    this.selectedMember.SponserCustId = '';
    this.selectedMember.SponserId = -1;
    this.selectedMember.SponserName = '';
    this.selectedMember.SposerRole = '';
    this.selectedMember.SponsorStartDate = '';
    this.selectedMember.SponsorEndDate = '';
    this.selectedMember.EmployeeCategoryList = '';

    this.memberForm.patchValue({
      SponsorStartDate: '', // Comment out if remove startDate when no sponsor
      SponsorEndDate: '',
      SponserId: -1,
      SponserName: '',
      EmployeeCategoryList: '',
    });
    this.economyDetails.SponserId = -1; // To hide hyperlink
    this.IsDelBtnVisibleSpo = false;
    this._translateService.get('MEMBERSHIP.SaveToConfirmChange').pipe(takeUntil(this.destroy$)).subscribe(e => this.message = e);
    this.type = 'INFO';
    this.isShowInfo = true; // Hide message again by saving or resetting
/*     setTimeout(() => {
      this.isShowInfo = false;
    }, 4000); */
  }

  // Delete Selected Guardian from the View
  deleteSelectedGuardian(selectedGuardianCustId) {
    this.selectedMember.GuardianId = 0; // To hide guardian hyperlink
    if (selectedGuardianCustId !== null) {
      let messageTitle = '';
      let messageBody = '';
      this._translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this._translateService.get('MEMBERSHIP.ConfirmDelete').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this._sponsorDelModal = this._exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: messageTitle,
          messageBody: messageBody,
          msgBoxId: 'DELETE_GUA'
        });
      this.changeMadeInForm()
    }
  }

  // clear all guardian fields
  guardianYesHandler() {
    this.memberForm.patchValue({ GuardianId: 0, GuardianLst: '' });
    this.selectedGuardianCustId = '';
    this.selectedGuardianLastName = '';
    this.selectedGuardianFirstName = '';
    this.selectedGuardianAddress1 = '';
    this.selectedGuardianAddress2 = '';
    this.selectedGuardianZip = '';
    this.selectedGuardianCity = '';
    this.selectedGuardianMobile = '';
    this.IsDelBtnVisibleGur = false;

    this._translateService.get('MEMBERSHIP.SaveToConfirmChange').pipe(takeUntil(this.destroy$)).subscribe(e => this.message = e);
    this.type = 'INFO';
    this.isShowInfo = true; // Hide messaga by saving or resetting
/*     setTimeout(() => {
      this.isShowInfo = false;
    }, 4000); */
  }

  addPostalCode(postalCodeItem: any) {
    this.memberForm.patchValue({
      PostCode: postalCodeItem.postalCode,
      PostPlace: postalCodeItem.postalName
    });
  }

  // comment out possibility to change prepaid account, Martin by ref Øyvind.
  // changePrePaidBalance(content) {
  //   this.changePrePaidView = this._modalService.open(content, { width: '250' });
  // }

  radioChange(event) {
    this.prePaidChangeOption = event.target.value;
  }

  saveNewPPAccountBalance(input) {
    const amount = input.input.nativeElement.value;
    this.changePPAcountAmount = Number(amount)
    if (Number(amount) === 0) {
      let messageTitle = '';
      let messageBody = '';
      this._translateService.get('MEMBERSHIP.InputError').pipe(
        takeUntil(this.destroy$)
        ).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this._translateService.get('MEMBERSHIP.InputValueNotZero').pipe(
        takeUntil(this.destroy$)
        ).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this._ppAccountErrorModal = this._exceMessageService.openMessageBox('ERROR',
        {
          messageTitle: messageTitle,
          messageBody: messageBody
        });
      return
    }

    let originalValue = this.memberForm.get('PrepaidBalance').value
    if ( this.prePaidChangeOption === '0' && Number(originalValue) >= Number(amount)) {
      const diff = Number(originalValue) - Number(amount)
      this.memberForm.patchValue({
        'PrepaidBalance': diff
      })
      this.changeMadeInForm();
      this.changePrePaidView.close();
    } else if ( this.prePaidChangeOption === '0') {
      let messageTitle = '';
      let messageBody = '';
      this._translateService.get('MEMBERSHIP.InputError').pipe(
        takeUntil(this.destroy$)
        ).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this._translateService.get('MEMBERSHIP.DecreasePPAcountError').pipe(
        takeUntil(this.destroy$)
        ).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this._ppAccountErrorModal = this._exceMessageService.openMessageBox('ERROR',
        {
          messageTitle: messageTitle,
          messageBody: messageBody
        });
      // errormessage amount bigger than pp-acount
    }
    if (this.prePaidChangeOption === '1') {
      originalValue = Number(originalValue) + Number(amount);
      this.memberForm.patchValue({
        'PrepaidBalance': originalValue
      })
      this.changeMadeInForm();
      this.changePrePaidView.close();
    }
  }

  backToInfoWithNotes() {
    const parentRoute = '/membership/card/' + this.selectedMember.Id + '/' + this.selectedMember.BranchId + '/' + MemberRole[this.selectedMember.Role];
    this.router.navigate([parentRoute]);
  }

}
