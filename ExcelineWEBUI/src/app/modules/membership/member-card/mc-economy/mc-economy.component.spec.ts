import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McEconomyComponent } from './mc-economy.component';

describe('McEconomyComponent', () => {
  let component: McEconomyComponent;
  let fixture: ComponentFixture<McEconomyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McEconomyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McEconomyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
