import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ExceMemberService } from '../../services/exce-member.service';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { McBasicInfoService } from '../mc-basic-info/mc-basic-info.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { ExceShopService } from '../../../shop/services/exce-shop.service';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ShopHomeService } from '../../../shop/shop-home/shop-home.service';
import { ExceGymSetting } from '../../../../shared/SystemObjects/Common/ExceGymSetting';
import { AdminService } from '../../../admin/services/admin.service';
import value from '*.json';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ExcelineMember } from '../../models/ExcelineMember';

// Memory leak fix test


@Component({
  selector: 'credit-note-view',
  templateUrl: './mc-credit-note.component.html',
  styleUrls: ['./mc-credit-note.component.scss']
})
export class McCreditNoteComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private exceGymSetting: ExceGymSetting = new ExceGymSetting();
  private shopLoginData?: any = {};
  private creditNoteConfirmModel: any;
  private branchId: number;
  private chekoutIsEnabled = true;
  private articleSummaryList = [];
  private itemResource?: DataTableResource<any>;
  private addNote = '';
  private selectedInvoice: any = {};
  private originalBalance: any;
  private newInvoiceBalance: any;
  private creditNoteBalance: any;
  private shopPaymentView: any;
  private selectedGymsettings = [
    'EcoNegativeOnAccount',
    'EcoSMSInvoiceShop',
    'EcoIsPrintInvoiceInShop',
    'EcoIsPrintReceiptInShop',
    'EcoIsShopOnNextOrder',
    'EcoIsOnAccount',
    'EcoIsDefaultCustomerAvailable',
    'EcoIsPayButtonsAvailable',
    'EcoIsPettyCashSameAsUserAllowed',
    'OtherIsShopAvailable',
    'OtherLoginShop',
    'HwIsShopLoginWithCard',
    'ReqMemberListRequired',
    'OtherIsLogoutFromShopAfterSale',
    'EcoIsBankTerminalIntegrated',
    'EcoIsDailySettlementForAll',
    'OtherIsValidateShopGymId'
  ];
  selectedCreditNote: any = {};
  _dateFormat: string;
  locale: string;
  curserPosition: any;
  @Input() isAddView: boolean;
  @Input() creditNote: any;
  @Input() memberId: number;
  @Input() memberName: string;
  private discountItem: any;
  @Output() closeCreditNoteModel = new EventEmitter();
  @Output() successCreditNoteEvent = new EventEmitter();
  // private isAddView = false;

  @Input() arItemNo: number;
  @Input() invoice: any;
  @Input() articleList: any;

  articleDefs: any;
  agArticleApi: any;
  agArticleOptions: any;
  GodModeActive: boolean
  isSaveSuccess = false;
  isShowSaveInfo = false;
  saveMessage = 'OK';
  itemCount = 0;
  addcreditNote: any = {};

  model: any;

  saveIsDisabled = true;

  names = {
    ArticleId: 'ArtikkelID',
    Name: 'Navn',
    UnitPrice: 'Varepris',
    Discount: 'Rabatt',
    Quantity: 'Antall',
    Price: 'Totalpris',
    ArticleBalance: 'Saldo',
    CreditNoteAmount: 'Kreditnota beløp',
    CreditedQuantity: 'Kreditert antall',
    Date: 'Dato',
    User: 'Bruker',
    Article: 'Artikkel',
    Amount: 'Beløp',
  }

  colNamesNo = {
    ArticleId: 'ArtikkelID',
    Name: 'Navn',
    UnitPrice: 'Varepris',
    Discount: 'Rabatt',
    Quantity: 'Antall',
    Price: 'Totalpris',
    ArticleBalance: 'Saldo',
    CreditNoteAmount: 'Kreditnota beløp',
    CreditedQuantity: 'Kreditert antall',
    Date: 'Dato',
    User: 'Bruker',
    Article: 'Artikkel',
    Amount: 'Beløp',
  }

  colNamesEn = {
    ArticleId: 'Article ID',
    Name: 'Navn',
    UnitPrice: 'Unit price',
    Discount: 'Discount',
    Quantity: 'Quantity',
    Price: 'Price',
    ArticleBalance: 'Balance',
    CreditNoteAmount: 'Credit note amount',
    CreditedQuantity: 'Credited quantity',
    Date: 'Date',
    User: 'User',
    Article: 'Article',
    Amount: 'Amount',
  }

  agCNoteOptions: any;
  agCNoteApi: any;
  detailsDefs: any;
  startList = [];
  paymentlistDefs: any;
  agPaymentsApi: any;
  agPaymentsOptions: any;
  hasGiftVoucher = false;

  constructor(private exceMemberService: ExceMemberService,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private loginservice: ExceLoginService,
    private basicInfoService: McBasicInfoService,
    private exceMessageService: ExceMessageService,
    private shopService: ExceShopService,
    private modalService: UsbModal,
    private shopHomeService: ShopHomeService,
    private toolBarService: ExceToolbarService,
    private adminService: AdminService) { }

  ngOnInit() {
   this.GodModeActive = (this.loginservice.CurrentUser.username.slice(this.loginservice.CurrentUser.username.indexOf('/') + 1) === 'exceadmin') ? true : false;
   const language = this.toolBarService.getSelectedLanguage();
    this.locale = language.culture;
    this._dateFormat = 'DD.MM.YYYY';

    this.toolBarService.langUpdated.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
    //  this.isAddView = this.creditNote.IsAddView;
    this.branchId = this.loginservice.SelectedBranch.BranchId;
    this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
    ).subscribe((currentMember: ExcelineMember) => {
      this.shopHomeService.$selectedMember = currentMember;
    })
    this.shopHomeService.$selectedMember.IntCustId = this.shopHomeService.$selectedMember.CustId;
    this.shopHomeService.$priceType = 'MEMBER';
    // this.memSubs = this.basicInfoService.memberSource.takeUntil(this.destroy$).pipe(takeUntil(this.destroy$)).subscribe(
    //   member => {
    //     if (member) {
    //       member.IntCustId = member.CustId;
    //       this.shopHomeService.$selectedMember = member;

    //     }
    //   }
    // );
    if (this.isAddView) {
      this.selectedInvoice = this.invoice;
      this.articleList.forEach(article => {
        if (article.Name === 'Gavekort') {
          this.hasGiftVoucher = true;
        }
        article.CNA = '0.00';
        article.CQ = '0';
        this.startList.push(article)
      });
      if (this.hasGiftVoucher) {
        this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'MEMBERSHIP.CreditNoteGiftVoucher',
              msgBoxId: ''
            });
      }
      this.articleSummaryList = this.articleList;
      this.originalBalance = this.invoice.InvoiceBalance;
      this.newInvoiceBalance = this.invoice.InvoiceBalance;

      this.articleSummaryList.forEach(
        item => {
          item.CreditedQuantity = item.Quantity,
          item.CreditNoteAmount = item.ArticleBalance
        }
      );
      this.itemResource = new DataTableResource(this.articleSummaryList);

      this.itemResource.count().then(count => this.itemCount = count);
    } else {
      this.selectedCreditNote = this.creditNote;
      if (this.selectedCreditNote.Details !== null) {
        if (this.selectedCreditNote.Details.length > 0) {
          this.selectedCreditNote.Note = this.selectedCreditNote.Details[0].Note;
        }

        if (this.selectedCreditNote.GymId !== this.branchId) {
          this.chekoutIsEnabled = false;
        }
      }
      const details = [];
      this.selectedCreditNote.Details.forEach(el => {
        const d = el.CreatedDate.split('T');
        const d2 = d[0].split('-');
        const newDate = new Date(Number(d2[0]), Number(d2[1]) - 1, Number(d2[2]))
        el.CreatedDate = newDate;
        details.push(el)
      });
      this.selectedCreditNote.Details = details;
    }

    this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      if (value.id === 'ADDED_CERDIT_NOTE') {
        this.addCreditNoteConfirm();
      } else if (value.id === 'DELETE_CERDIT_NOTE') {
        this.deleteCreditNoteConfirm(value.optionalData);
      }
    });

    this.shopService.GetSelectedGymSettings({ GymSetColNames: this.selectedGymsettings, IsGymSettings: true }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {

        for (const key in res.Data) {
          if (res.Data.hasOwnProperty(key)) {
            switch (key) {
              case 'EcoNegativeOnAccount':
                this.exceGymSetting.OnAccountNegativeVale = res.Data[key];
                break;
              case 'EcoIsShopOnNextOrder':
                this.exceGymSetting.IsShopOnNextOrder = res.Data[key];
                break;
              case 'EcoIsDailySettlementForAll':
                this.exceGymSetting.IsDailySettlementForAll = res.Data[key];
                break;
              case 'EcoIsOnAccount':
                this.exceGymSetting.IsOnAccount = res.Data[key];
                break;
              case 'EcoIsDefaultCustomerAvailable':
                this.exceGymSetting.IsDefaultCustomerAvailable = res.Data[key];
                break;
              case 'EcoIsPayButtonsAvailable':
                this.exceGymSetting.IsPayButtonsAvailable = res.Data[key];
                break;
              case 'EcoIsPettyCashSameAsUserAllowed':
                this.exceGymSetting.IsPettyCashSameAsUserAllowed = res.Data[key];
                break;
              case 'EcoIsPrintInvoiceInShop':
                this.exceGymSetting.IsPrintInvoiceInShop = res.Data[key];
                break;
              case 'EcoIsPrintReceiptInShop':
                this.exceGymSetting.IsPrintReceiptInShop = res.Data[key];
                break;
              case 'OtherIsShopAvailable':
                this.exceGymSetting.IsShopAvailable = res.Data[key];
                break;
              case 'OtherLoginShop':
                this.exceGymSetting.IsShopLoginNeeded = res.Data[key];
                break;
              case 'HwIsShopLoginWithCard':
                this.exceGymSetting.IsShopLoginWithCard = res.Data[key];
                break;
              case 'ReqMemberListRequired':
                this.exceGymSetting.MemberRequired = res.Data[key];
                break;
              case 'OtherIsLogoutFromShopAfterSale':
                this.exceGymSetting.IsLogoutFromShopAfterSale = res.Data[key];
                break;
              case 'EcoSMSInvoiceShop':
                this.exceGymSetting.IsSMSInvoiceShop = res.Data[key];
                break;
              case 'EcoIsBankTerminalIntegrated':
                this.exceGymSetting.IsBankTerminalIntegrated = res.Data[key];
                break;
              case 'OtherIsValidateShopGymId':
                this.exceGymSetting.IsValidateShopGymId = res.Data[key];
                break;
            }

          }
        }
        this.shopLoginData.GymSetting = this.exceGymSetting;
        this.shopService.GetSalesPoint().pipe(takeUntil(this.destroy$)).subscribe(
          salePoints => {
            this.shopLoginData.SalePoint = salePoints.Data[0];
            this.adminService.getGymSettings('HARDWARE', null).pipe(takeUntil(this.destroy$)).subscribe(
              hwSetting => {
                this.shopLoginData.HardwareProfileList = hwSetting.Data;
                this.shopLoginData.HardwareProfileList.forEach(hwProfile => {
                  if (this.shopLoginData.SalePoint != null &&
                    this.shopLoginData.SalePoint.HardwareProfileId === hwProfile.Id) {
                    this.shopLoginData.SelectedHardwareProfile = hwProfile;
                    this.shopLoginData.HardwareProfileId = hwProfile.Id;
                  } else {
                    this.shopLoginData.SelectedHardwareProfile = null;
                    this.shopLoginData.HardwareProfileId = null;
                  }
                  this.shopHomeService.$shopLoginData = this.shopLoginData;
                });
              },
              error => {

              });
          },
          error => {

          });



      },
      error => {

      }
    )

    this.articleDefs = [
      {headerName: this.names.ArticleId, field: 'ArticleId', width: 100, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.Name, field: 'Name',  filter: 'agTextColumnFilter', suppressMenu: true,  cellStyle: {},
        floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.UnitPrice, field: 'UnitPrice',  filter: 'agTextColumnFilter', suppressMenu: true,
      cellStyle: {'textAlign': 'right'},
      floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      },  suppressMovable: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return '0.00';
        }
      }},
      {headerName: this.names.Discount, field: 'Discount',  filter: 'agTextColumnFilter', suppressMenu: true,
      cellStyle: {'textAlign': 'right'},
      floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return '0.00';
        }
      }},
      {headerName: this.names.Quantity, field: 'Quantity', suppressMenu: true,
      cellStyle: {},
      floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Price, field: 'Price',  suppressMenu: true,
      cellStyle: {'textAlign': 'right'},
      floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return '0.00';
        }
      }},
      {headerName: this.names.ArticleBalance, field: 'ArticleBalance',  filter: 'agTextColumnFilter', suppressMenu: true,
      cellStyle: {'textAlign': 'right'},
      floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return '0.00';
        }
      }},
      {headerName: this.names.CreditNoteAmount, field: 'CNA', colId: 'CNA',  filter: 'agTextColumnFilter', suppressMenu: true,
      cellStyle: {'border-color': '#d9dcde', 'background-color': '#F6F6F6', 'cursor': 'text', 'textAlign': 'right'}, singleClickEdit: true, editable: true,
      floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} ,
      sortable: false, suppressMovable: true, // cellRenderer: function(params) { return '0.00'}
    },
      {headerName: this.names.CreditedQuantity, field: 'CQ', colId: 'CQ',  filter: 'agTextColumnFilter', suppressMenu: true, headerClass: 'headerItalic',
      cellStyle: {'border-color': '#d9dcde', 'font-style': 'italic', 'background-color': '#F6F6F6', 'cursor': 'text'},
      floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'},
      sortable: false, suppressMovable: true,  singleClickEdit: true, editable: true, // cellRenderer: function(params) { return '0'},
    }
    ];

    this.detailsDefs = [
      {headerName: this.names.Date, field: 'CreatedDate', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true,
      cellRenderer: function(params) {
        if (params.value) {
          const date = params.value;
          const day = date.getDate();
          let month = date.getMonth() + 1;
          if (month.length === 1) {
            month = '0' + month;
          }
          const year = date.getFullYear();
          return day + '.' + month + '.' + year;
        } else {
          return '';
        }
      }
    },
      {headerName: this.names.User, field: 'User', suppressMenu: true,  cellStyle: {},
        floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Amount, field: 'Amount', suppressMenu: true,  cellStyle: {},
      floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return '0.00';
      }}},
      {headerName: this.names.CreditedQuantity, field: 'CreditedCount', colId: 'CQ',
      suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'},
      sortable: false, suppressMovable: true,  // cellRenderer: function(params) { return '0'},
      }
    ];

    this.paymentlistDefs = [
      {headerName: this.names.Date, field: 'PaymentDate', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true},
    {headerName: 'Type', field: 'Type', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true},
    {headerName: this.names.Amount, field: 'PaymentAmount', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true},
    ]
  }

  articleGridReady(params) {
    this.agArticleApi = params.api;
    this.agArticleOptions = params.api.gridOptionsWrapper.gridOptions;
    this.agArticleOptions.api.hideOverlay();
    this.agArticleApi.sizeColumnsToFit();
  }

  paypementsGridReady(params) {
    this.agPaymentsApi = params.api;
    this.agPaymentsOptions = params.api.gridOptionsWrapper.gridOptions;
    this.agPaymentsOptions.api.hideOverlay();
    this.agPaymentsApi.sizeColumnsToFit();
  }

  clearGrid() {
    this.selectedCreditNote.Details = [];
    this.agArticleApi.forEachNode( function(node) {
      node.setDataValue('ArticleBalance', Number(node.data.CreditNoteAmount))
      node.setDataValue('CNA', '0.00')
      node.setDataValue('CQ', '0')
      node.setDataValue('CreditNoteAmount', 0)
      node.setDataValue('CreditedQuantity', 0)
    })

    // this.articleSummaryList.forEach(article => {
    //   article.CreditNoteAmount = 0;
    //   article.CreditedQuantity = 0;
    // });
    this.saveIsDisabled = true;
    this.addNote = '';
    this.updateAmounts();
  }

  cnoteGridReady(params) {
    this.agCNoteApi = params.api;
    this.agCNoteOptions = params.api.gridOptionsWrapper.gridOptions;
    this.agCNoteOptions.api.hideOverlay();
    this.agCNoteApi.sizeColumnsToFit();
  }

  cellValueChanged(event) {
    let val = event.value
    if (event.newValue === event.oldValue) {
      return;
    }
    if (event.column.colId === 'CNA') {
      if (val && !isNaN(val) && Number(val) <= Number(event.node.data.ArticleBalance)) {
        if (!val.includes('.')) {
          event.node.setDataValue(event.column.colId, Number(val).toFixed(2))
        } else {
          const checkval = val.split('.');
          if (checkval.length === 2 && checkval[1] === 1) {
            val = val + '0';
            event.node.setDataValue(event.column.colId, Number(val).toFixed(2))
          } else if (checkval.length !== 2) {
            event.node.setDataValue(event.column.colId, (0).toFixed(2))
          }
        }
      } else {
        event.node.setDataValue(event.column.colId, (0).toFixed(2))
      }

    } else if (event.column.colId === 'CQ') {
      if (Number(val) > event.node.data.Quantity || Number(val) < 0) {
        event.node.setDataValue(event.column.colId, 0)
        return;
      }
      if (isNaN(Number(val))) {
        event.node.setDataValue(event.column.colId, 0)
      } else {
        event.node.setDataValue(event.column.colId, (Number(val).toFixed(0)))
        event.node.setDataValue('CNA', (Number(val) * (event.node.data.Price - event.node.data.Discount)).toFixed(2))
      }
    }

    this.articleSummaryList.forEach(article => {
      article.CreditNoteAmount = Number(article.CNA);
      article.CreditedQuantity = Number(article.CQ);
    });

    // this.agArticleApi.updateRowData();
  }

  closeEdit() {
    this.agArticleApi.stopEditing();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.shopPaymentView) {
      this.shopPaymentView.close();
    }
  }

  deleteCreditNote(deletedItem) {
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'DELETE_CERDIT_NOTE',
        optionalData: deletedItem
      });

  }
  deleteCreditNoteConfirm(deletedItem) {
    const index: number = this.selectedCreditNote.Details.indexOf(deletedItem);
    if (index !== -1) {
      this.selectedCreditNote.Details.splice(index, 1);
      const updatedArticle = this.articleSummaryList.find(X => X.Id === deletedItem.OrderlineId);

      if (updatedArticle) {

        updatedArticle.ArticleBalance += deletedItem.Amount;
      }
      this.exceMemberService.deleteCreditNote(deletedItem.Id).pipe(takeUntil(this.destroy$)).subscribe( result => {
        if (result) {
          this.translateService.get('MEMBERSHIP.SaveSuccessful').pipe(takeUntil(this.destroy$)).subscribe(value => this.saveMessage = value);
          this.isSaveSuccess = true;
          this.isShowSaveInfo = true;
          setTimeout(() => {
            this.isShowSaveInfo = false;
            this.closeCreditNote();
          }, 3000);
        }else {
          this.translateService.get('MEMBERSHIP.SaveNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(value => this.saveMessage = value);
          this.isSaveSuccess = false;
          this.isShowSaveInfo = true;
          setTimeout(() => {
            this.isShowSaveInfo = false;
          }, 3000);
        }
      })
      this.updateAmounts();
    }
  }

  reloadItems(params) {

    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.articleSummaryList = items);
    }
  }
  okClick() {
    this.closeCreditNoteModel.emit();
  }

  changeModel(inputDP) {
    setTimeout(() => {
      inputDP.selectionEnd = this.curserPosition;
    }, 100);
  }
  onKeyDown(event) {
    this.curserPosition = event.target.selectionEnd;
  }

  QuantityChanged(item, event) {
    const findItem = this.articleSummaryList.find(shopItem => shopItem.Id === item.Id);
    if (findItem) {
      findItem.CreditedQuantity = Number(event);
      findItem.CreditedQuantity = (findItem.CreditedQuantity < 0) ? 0 : findItem.CreditedQuantity;
      findItem.CreditedQuantity = (findItem.CreditedQuantity > findItem.Quantity) ? findItem.Quantity : findItem.CreditedQuantity;
      // findItem.CreditedQuantity = ((findItem.ArticleBalance / findItem.CreditedQuantity) < findItem.CreditNoteAmount) ? 0 : findItem.CreditedQuantity;
      this.articleSummaryList.forEach(elem => {
      if (elem.Id === findItem.Id) {

        elem.CreditedQuantity = findItem.CreditedQuantity;
      }
    }
      );
   // this.reloadItems( this.articleSummaryList)
    }
  }

  creditNoteAmountChanged(item, event) {
    this.discountItem = item;
    this.discountItem.CreditNoteAmount = Number(event);
  }

  addCreditNoteEvent() {
    this.closeEdit();
    if (this.articleSummaryList != null) {
      const creditedSum = this.articleSummaryList.map(c => Number(c.CreditNoteAmount)).reduce((sum, current) => sum + current);
      let sum = 0;
      this.articleSummaryList.forEach(c => {
        sum = sum + c.CreditNoteAmount
      });
      let sum2 = 0;
      this.agArticleApi.forEachNode( function(node) {
        sum2 = sum2 + node.data.CreditNoteAmount;
      })
      if (creditedSum === 0) {
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.MsgAddCreditedAmount' });
      } else {
        const warnArticle = this.articleSummaryList.find(X => X.ArticleBalance < Number(X.CreditNoteAmount) && X.Price > 0);
        if (warnArticle === undefined) {
          this.addCreditNoteToinvoice();
        } else {
          this.exceMessageService.openMessageBox('CONFIRM',
            {
              messageTitle: 'Exceline',
              messageBody: 'MEMBERSHIP.msgConfirmAddCreditNote',
              msgBoxId: 'ADDED_CERDIT_NOTE'
            });
        }
      }
    }
  }

  addCreditNoteConfirm() {
    this.addCreditNoteToinvoice();
  }

  addCreditNoteToinvoice() {
    const orderLineListToUpdate = this.articleSummaryList.filter(x => x.CreditNoteAmount > 0);
    const details = [];
    if (orderLineListToUpdate.length > 0) {
      if (this.addNote !== '') {
        const creditNoteDetails = this.GetCreditNoteList(orderLineListToUpdate);
        creditNoteDetails.forEach(element => {
          details.push(element);
          this.addcreditNote.Details = details;
          this.saveIsDisabled = false;
        });
        this.selectedCreditNote = this.addcreditNote;
        this.updateAmounts();

        this.articleSummaryList.forEach(summary => {
          this.agArticleApi.forEachNode( function(node) {
            if (node.data.Id === summary.Id) {
              node.setDataValue('ArticleBalance', summary.ArticleBalance - summary.CreditNoteAmount)
            }
          })
          // summary.ArticleBalance = summary.ArticleBalance - summary.CreditNoteAmount;
          // summary.CreditNoteAmount = 0;
          // summary.CreditedQuantity = 0;
        });
      } else {
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.CommentNotEmpty' });
      }
    }
  }

  updateAmounts() {
    const creditedSum = this.selectedCreditNote.Details.length > 0 ?
      this.selectedCreditNote.Details.map(c => c.Amount).reduce((sum, current) => sum + current) : 0;

    this.newInvoiceBalance = this.originalBalance - creditedSum;
    if (this.newInvoiceBalance < 0) {
      this.newInvoiceBalance = 0;
    }
    this.creditNoteBalance = creditedSum - this.originalBalance;
    if (this.creditNoteBalance < 0) {
      this.creditNoteBalance = 0;
    }
    this.selectedCreditNote.Amount = creditedSum;
    // this.agCNoteApi.updateRowData();

  }

  GetCreditNoteList(articleList) {
    const creditNoteList = [];
    articleList.forEach(i => {
      creditNoteList.push({
        'OrderlineId': i.Id, 'ArticlePrice': i.Price, 'Amount': Number(i.CreditNoteAmount), 'ArticleId': i.ArticleId, 'IsEnabled': true,
        'ArticleText': i.Name, 'CreditedCount': Number(i.CreditedQuantity), 'CreatedDate': new Date(),
        'User': this.loginservice.CurrentUser.username
      });
    });
    return creditNoteList;
  }
  closeCreditNote() {
    this.closeCreditNoteModel.emit();
  }
  createCreditNote() {
    if (this.addNote === '') {
      return;
    } else if (this.arItemNo > 0) {

      this.addCreditNote();
    }
  }

  addCreditNote() {
    if (this.selectedCreditNote != null && this.selectedCreditNote.Details != null) {
      if (this.selectedCreditNote.Details.length > 0) {
        this.selectedCreditNote.CreatedDate = new Date();
        this.selectedCreditNote.User = this.loginservice.CurrentUser.username;
        this.selectedCreditNote.CreditorNumber = this.selectedInvoice.CreditorNo;
        this.selectedCreditNote.CustomerNo = this.selectedInvoice.CustId;
        this.selectedCreditNote.Kid = this.selectedInvoice.BasicDetails.KID;
        this.selectedCreditNote.InvoiceNo = this.selectedInvoice.InvoiceNo;
        this.selectedCreditNote.CaseNo = this.selectedInvoice.SubCaseNo;
        this.selectedCreditNote.ArItemNo = this.selectedInvoice.ArItemNO;
        this.selectedCreditNote.Note = this.addNote;
        this.selectedCreditNote.BranchId = this.branchId;

        PreloaderService.showPreLoader();
        this.exceMemberService.AddCreditNote(this.selectedCreditNote).pipe(takeUntil(this.destroy$)).subscribe(
          res => {
            PreloaderService.hidePreLoader();
            if (res.Data > 0) {
              this.successCreditNoteEvent.emit(res.Data);
            }
          },
          err => {
            PreloaderService.hidePreLoader();
            this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.MsgCreditorNoteError' });
          });
      }
    }
  }

  openShopPayment(content) {
    this.shopService.GetMemberShopAccountsForEntityType({ memberId: this.memberId, entityType: 'MEM' }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.shopHomeService.$shopMode = 'CREDITNOTE';
        this.shopHomeService.$shopAccount = res.Data;
        this.shopHomeService.$isCreditNote = false;
        this.shopHomeService.$totalPrice = this.selectedCreditNote.Balance;
        this.shopHomeService.$arItemNo = this.selectedCreditNote.Id;
        this.shopPaymentView = this.modalService.open(content);
      });
  }

  closePaymentView(): void {
    this.shopPaymentView.close();
    this.closeCreditNoteModel.emit();
  }
}
