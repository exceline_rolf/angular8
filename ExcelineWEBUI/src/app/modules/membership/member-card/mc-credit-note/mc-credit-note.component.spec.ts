import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McCreditNoteComponent } from './mc-credit-note.component';

describe('McCreditNoteComponent', () => {
  let component: McCreditNoteComponent;
  let fixture: ComponentFixture<McCreditNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McCreditNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McCreditNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
