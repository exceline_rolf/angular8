import { ExcePdfViewerService } from './../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { PreloaderService } from './../../../../shared/services/preloader.service';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../services/exce-member.service'
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const now = new Date();

@Component({
  selector: 'app-mc-classes',
  templateUrl: './mc-classes.component.html',
  styleUrls: ['./mc-classes.component.scss']
})
export class McClassesComponent implements OnInit, OnDestroy {
  private branchID: number = JSON.parse(Cookie.get('selectedBranch')).BranchId
  private memberClasses: any;
  private filteredMemberClasses: any;
  private itemResource: any = 0;
  items = [];
  itemCount = 0;
  resources: any;
  private activityCategories: any;
  private resourceCategories: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private dateValue: any = '';
  private insValue: string;
  private cTypeValue: string;
  private cGroupValue: string;
  private modalReference?: any;
  private memberClassesForm: any;
  private otherMembers: any;
  private rowItem: any;
  private printClassForm: any;
  private printResult: any;
  public rowColors: any;
  private selectedMember: any;
  private today: NgbDateStruct;
  private monthbefore: NgbDateStruct;
  private instuctors: any[];
  public classGroups: any;
  private instructorsPop: any;

  private destroy$ = new Subject<void>();

  constructor(
    private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private excePdfViewerService: ExcePdfViewerService
  ) {
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    let lastMonth = this.dateAdd(now, -1, 'months');
    this.monthbefore = { year: lastMonth.getFullYear(), month: lastMonth.getMonth() + 1, day: lastMonth.getDate() };

    this.memberClassesForm = this.fb.group({
      'GymName': [''],
      'ClassType': [''],
      'ClassLevelId': [''],
      'ClassGroup': [''],
      'Day': [''],
      'InstructorList': [''],
      'Location': [''],
      'ClassKeyWords': ['']
    });

    this.printClassForm = this.fb.group({
      'fromDate': [{ date: this.monthbefore }],
      'toDate': [{ date: this.today }],
    });
  }

  ngOnInit() {
    this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
      ).subscribe(member => {
        if (member) {
          this.selectedMember = member;
          this.exceMemberService.getClassByMemberId(this.selectedMember.BranchId, this.selectedMember.Id, '').pipe(takeUntil(this.destroy$)).subscribe(result => {
              if (result) {
                this.memberClasses = result.Data;
                this.classGroups = []
                // tslint:disable-next-line:forin
                for (let memberClass in this.memberClasses) {
                  let val = this.memberClasses[memberClass].ClassGroup;
                  if (val in this.classGroups) {
                  } else {
                    this.classGroups.push(val);
                  }
                }
                this.itemResource = new DataTableResource(this.memberClasses);
                this.itemResource.count().then(count => this.itemCount = count);
                this.items = this.memberClasses;
                this.itemCount = Number(result.Data.length);
              } else {
              }
            });
        }
      });
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  filterInput(dateValue?: any, insValue?: any, cTypeValue?: any, cGroupValue?: any) {
    this.filteredMemberClasses = this.memberClasses;

    if (dateValue && dateValue !== '') {
      dateValue = this.dateConverter(dateValue);
      this.filteredMemberClasses = this.filterpipe.transform('StartDate', 'DATE_FILTER', this.filteredMemberClasses, dateValue);
    }
    if (insValue !== '' || insValue != null) {
      this.filteredMemberClasses = this.filterpipe.transform('InstructorStrn', 'NAMEFILTER', this.filteredMemberClasses, insValue);
    }
    if (cTypeValue !== '' || cTypeValue != null) {
      this.filteredMemberClasses = this.filterpipe.transform('ClassType', 'NAMEFILTER', this.filteredMemberClasses, cTypeValue);
    }
    if (cGroupValue !== 'ALL') {
      this.filteredMemberClasses = this.filterpipe.transform('ClassGroup', 'SELECT', this.filteredMemberClasses, cGroupValue);
    }
    this.itemResource = new DataTableResource(this.filteredMemberClasses);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredMemberClasses);
    this.insValue = insValue;
    this.cTypeValue = cTypeValue;
    this.cGroupValue = cGroupValue;
  }

  // convert date object in to string
  dateConverter(dateObj: any) {
    let dateS = '';
    if (dateObj) {
      if (dateObj) {
        const splited = dateObj.split('/')
        if (splited.length > 1) {
          dateS = splited[2] + '-' + splited[1] + '-' + splited[0] + 'T00:00:00'
        }
      }
    }
    return dateS;
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  openClassesModel(content: any) {
    this.modalReference = this.modalService.open(content, { width: '1000' });
  }

  openPrintBookingModel(content: any) {
    this.modalReference = this.modalService.open(content, { width: '300' });
  }

  rowDoubleClick(rowEvent) {
    this.rowItem = rowEvent.row.item;
    this.loadDataForModals(this.rowItem);
  }

  rowItemLoad(rowItem) {
    this.rowItem = rowItem
    this.loadDataForModals(this.rowItem);
  }

  openPopOver(rowItem, instructorsPop) {
    this.rowItem = rowItem
    this.loadDataForModals(this.rowItem);
    this.instructorsPop = instructorsPop;
    this.instructorsPop.open();
  }

  closeForm() {
    this.memberClassesForm.reset();
    this.modalReference.close();
  }

  printClasses(printData: any) {
    const fromDate = printData.fromDate.date.year + '-' + printData.fromDate.date.month + '-' + printData.fromDate.date.day;
    const toDate = printData.toDate.date.year + '-' + printData.toDate.date.month + '-' + printData.toDate.date.day;
    this.exceMemberService.PrintClassListByMember(this.selectedMember.BranchId, this.selectedMember.Id, fromDate, toDate).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          this.printResult = result.Data;
          this.modalReference.close();
          PreloaderService.hidePreLoader();
          if (result.Data.FileParth.trim() !== '') {
            this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
          }
        }
      });
  }

  closePopOver(instructorsPop: any) {
    this.instructorsPop.close();
  }

  loadDataForModals(rowItem: any) {
    this.instuctors = []
    // tslint:disable-next-line:forin
    for (let instructor in rowItem.InstructorList) {
      let val = rowItem.InstructorList[instructor];
      this.instuctors.push(val)
    }
    this.memberClassesForm.patchValue(rowItem);
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

}
