import { Component, ElementRef, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ExcelineMember } from 'app/modules/membership/models/ExcelineMember';
import { LoDashStatic } from 'lodash';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { DataTableResource } from '../../../shared/components/us-data-table/tools/data-table-resource';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { UsTabService } from '../../../shared/components/us-tab/us-tab.service';
import { ISlimScrollOptions } from '../../../shared/directives/us-scroll/classes/slimscroll-options.class';
import { UsScrollService } from '../../../shared/directives/us-scroll/us-scroll.service';
import { MemberRole } from '../../../shared/enums/us-enum';
import { PreloaderService } from '../../../shared/services/preloader.service';
import { ExceMemberService } from '../services/exce-member.service';
import value from '*.json';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { McBasicInfoService } from '../member-card/mc-basic-info/mc-basic-info.service';
import { Subscription } from 'rxjs';
import { Subject } from 'rxjs';

import {takeUntil, mergeMap, concatMap, filter } from 'rxjs/operators';


@Component({
  selector: 'member-home',
  templateUrl: './member-home.component.html',
  styleUrls: ['./member-home.component.scss']
})
export class MemberHomeComponent implements OnInit , OnDestroy {
  private branchID: number = JSON.parse(Cookie.get('selectedBranch')).BranchId;
  private All: String;
  private destroy$ = new Subject<void>();

  public userBranches: any[];
  public selectedBranch: any;
  public onlyonebranch = false;


  // user for hover over member
  // @ViewChild('memberInfo') memberInfo: ElementRef;

  viewHeight: any;
  columnDefs: any
  rowData: any;
  gridApi: any;
  gridColumnApi: any;
  searchHasMoreMembers = false;
  newMemberRegisterView: any;
  displayBranches: { BranchId: number; BranchName: String; }[];
  statuses: any;
  roles = [{Name: 'Medlem', RoleId: 'MEM'}, {Name: 'Bedrift', RoleId: 'COM'}, {Name: 'Sponsor', RoleId: 'SPO'} ]
  lang: any;
  filterBranchId = -1;
  filterStatusId = -1;
  filterRoleId = '';
  activeCount: any;
  inactiveCount: any;
  allCount: any;
  vistedMembers: any;

  // used for hover over member
  // isHidden = true;

  // default norwegian
  names = {
    LastName: 'Etternavn',
    FirstName: 'Fornavn',
    CustId: 'Kundenummer',
    Age: 'Alder',
    Mobile: 'Mobil',
    Email: 'Epost',
    MemberCardNo: 'Kortnummer',
    ContractNo: 'Kontraktsnummer',
    OrderAmount: 'Prisplan',
    Status: 'Status',
    BranchName: 'Avedling',
    BranchId: 'BranchID',
    RoleId: 'Role',
    ID: 'MemberId',
    ContractId: 'ContractId',
    InvoiceNo: 'Fakturanummer'
  }

  colNamesNo = {
    LastName: 'Etternavn',
    FirstName: 'Fornavn',
    CustId: 'Kundenummer',
    Age: 'Alder',
    Mobile: 'Mobil',
    Email: 'Epost',
    MemberCardNo: 'Kortnummer',
    ContractNo: 'Kontraktsnummer',
    OrderAmount: 'Prisplan',
    Status: 'Status',
    BranchName: 'Avedling',
    BranchId: 'BranchID',
    RoleId: 'Role',
    ID: 'MemberId',
    ContractId: 'ContractId',
    InvoiceNo: 'Fakturanummer'
  }

  colNamesEn = {
    LastName: 'LastName',
    FirstName: 'FirstName',
    CustId: 'CustomerNo',
    Age: 'Age',
    Mobile: 'Mobile',
    Email: 'Email',
    MemberCardNo: 'CardNo',
    ContractNo: 'ContractNo',
    OrderAmount: 'Priceplan',
    Status: 'Status',
    BranchName: 'BranchName',
    BranchId: 'BranchID',
    RoleId: 'Role',
    ID: 'MemberId',
    ContractId: 'ContractId',
    InvoiceNo: 'InvoiceNo'
  }


  constructor(
    private router: Router,
    private memberService: ExceMemberService,
    private modalService: UsbModal,
    private usScrollService: UsScrollService,
    private usTabService: UsTabService,
    private toolbarService: ExceToolbarService,
    private translateService: TranslateService,
    private exceBreadcrumbService: ExceBreadcrumbService,
    private basicInfoService: McBasicInfoService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'COMMON.MembershipHome',
      url: '/membership'
    });
  }

  ngOnInit() {
    // setting header names for ag grid by language
    this.lang = this.toolbarService.getSelectedLanguage()
    switch (this.lang.id) {
      case 'no':
        this.names = this.colNamesNo;
        break;
      case 'en':
        this.names = this.colNamesEn;
        break;
    }
    // getting branches
    this.memberService.getBranches().pipe(takeUntil(this.destroy$)).subscribe(branches => {
      if (branches) {
        this.userBranches = branches.Data;
        const branchList = [{ BranchId: -2, BranchName: this.All }];
        branches.Data.map(item => { return { BranchId: item.BranchId, BranchName: item.BranchName } })
          .forEach(item => branchList.push(item));
        this.displayBranches = branches.Data;
        this.onlyonebranch = this.userBranches.length === 2;
        this.selectedBranch = this.userBranches[0];
      }
    },
      error => {

    });
    this.usTabService.tabSelectionChanged.pipe(takeUntil(this.destroy$)).subscribe(
      event => {},
      error => {},
      () => {
      this.usScrollService.reloadScrollBar();
    });
    // getting memberCount for active, inactive and all members in db
    this.memberService.getMemberCount(this.filterBranchId).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        this.activeCount = result.Data[0];
        this.inactiveCount = result.Data[1];
        this.allCount = result.Data[2];
      }
    );
    // get status on member
    const getAllStatuses = false;
    // filter out those who are not in use and wont return any members
    this.memberService.getMemberStatus(getAllStatuses).pipe(takeUntil(this.destroy$)).subscribe(
      status => {
        this.statuses = status.Data;
      }
    );
    // defining column properties for ag grid
    this.columnDefs = [
      {headerName: this.names.CustId, field: 'CustId', width: 100, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.LastName, field: 'LastName',  filter: 'agTextColumnFilter', suppressMenu: true, cellClass: 'clickPointer', cellStyle: {'cursor': 'pointer'},
        floatingFilterComponentParams: {suppressFilterButton: true}, filterParams: {newRowsAction: 'keep'} , icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.FirstName, field: 'FirstName', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Age, field: 'Age', width: 75, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Mobile, field: 'Mobile', width: 100, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Email, field: 'Email', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.MemberCardNo, field: 'MembercardNo', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true},
      {headerName: this.names.ContractNo, field: 'ContractNo', width: 100, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'},
        cellRenderer: function(params) {
        return '<div style="color: #4d8693;"' +
        'onmouseover="this.style.color=\'#0074b1\';" onmouseleave="this.style.color=\'#4d8693\';">' + params.value + '</div>'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.OrderAmount, field: 'OrderAmount', width: 100, suppressMenu: true, suppressFilter: true,
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.Status, field: 'Status', suppressMenu: true, suppressFilter: true,  width: 100,
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.BranchName, field: 'BranchName', width: 200, suppressMenu: true, suppressFilter: true,
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: this.names.InvoiceNo, field: 'InvoiceNo', width: 100, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'},
      cellRenderer: function(params) {
        if (params.value) {
          return '<div style="color: #4d8693;"' +
            'onmouseover="this.style.color=\'#0074b1\';" onmouseleave="this.style.color=\'#4d8693\';">' + params.value + '</div>'
        } else {
          return '';
        }}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
      {headerName: 'RoleId', field: 'RoleId', hide: true, width: 200, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, suppressMovable: true},
      {headerName: 'BranchId', field: 'BranchId', hide: true, width: 200, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, suppressMovable: true},
      {headerName: 'ID', field: 'ID', hide: true, width: 200, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, suppressMovable: true},
      {headerName: 'ContractId', field: 'ContractId', hide: true, width: 200, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        cellStyle: {'cursor': 'pointer'}, filterParams: {newRowsAction: 'keep'}, suppressMovable: true},
    ];

    // get visited member by user
    // call
    this.memberService.getVisitedMembers().pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        this.vistedMembers = result.Data;
      }
    );
  }

  memberMouseOver(event) {
    return;
  //   console.log(event)
  //   const x = event.event.clientX + 'px';
  //   const y = event.event.clientY + 'px';

  //   console.log(x, y)
  //   console.log(this.memberInfo)
  //     // this.isHidden = false;
  //     this.memberInfo.nativeElement.style.backgroundColor = '#FF0000';
  //     this.memberInfo.nativeElement.style.top = y;
  //     this.memberInfo.nativeElement.style.marginLeft = x;
  //     // this.renderer.setStyle(this.memberInfo.nativeElement, 'bottom', 0);
  }

  // called when page is done loading. First call to get members done here
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    params.api.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
    // get 50 newest members
    this.filterChanged(null);

    // params.api.gridCore.gridOptions.getRowStyle = function(p) {
    //   console.log(p)
    //   if (p.node.rowIndex % 2 === 0) {
    //     return { background: 'red' }
    //   }
    //   if (p.node.data.Age === 33) {
    //     return { background: 'green' }
    //   }
    // }

    // console.log(params)
  }

  onCellClicked(params) {
/*     if (params.colDef.field === 'ContractNo' && params.value !== '') {
      this.memberService.membercardVisit(params.data.ID).pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          this.memberService.getMemberDetails(params.data.ID, params.data.BranchId, params.data.RoleId).pipe(takeUntil(this.destroy$)).subscribe(
            membD => {
              this.basicInfoService.setSelectedMember(membD.Data);
              const url = '/membership/card/' + params.data.ID + '/' + params.data.BranchId + '/' + params.data.RoleId + '/contracts/' + params.data.ContractId;
              this.router.navigate([url]);
         },
        error => {} ,
        () => {
          });
        }
      );
    } */
    if (params.colDef.field === 'ContractNo' && params.value !== '') {
      this.memberService.membercardVisit(params.data.ID).pipe(
        mergeMap(next => {
          return this.memberService.getMemberDetails(params.data.ID, params.data.BranchId, params.data.RoleId).pipe(
            takeUntil(this.destroy$)
          )
        }),
        takeUntil(this.destroy$)
      ).subscribe(membD => {
        const url = '/membership/card/' + params.data.ID + '/' + params.data.BranchId + '/' + params.data.RoleId + '/contracts/' + params.data.ContractId;
        this.router.navigate([url]);
      })
    }
  }

  // on filter changed
  filterChanged(event) {
    // checking in grid if any filters has values
    const values = this.gridApi.filterManager.allFilters;
    const columns = this.gridApi.columnController.columnApi.columnController.displayedCenterColumns;
    const returnList = [];
    const filteredColumns = [['LastName', ''], ['FirstName', ''], ['CustId', ''], ['Age', ''], ['Mobile', ''], ['Email', ''],
      ['MembercardNo', ''], ['ContractNo', ''], ['BranchName', ''], ['InvoiceNo', '']];
    filteredColumns.forEach(filterElement => {
        columns.forEach(col => {
          if (col.filterActive) {
            const headerName = col.colDef.field;
            const filterText = values[headerName].filterPromise.resolution.filterText;
            const filterInfo = [headerName, filterText];
            if (filterElement[0] === filterInfo[0]) {
              filterElement = filterInfo;
            }
          }
        })
        returnList.push(filterElement)
    });

    // filter values
    let filterMemberList = {};

    if (returnList[9][1] !== '') {
      filterMemberList = {
        LastName: '',
        FirstName: '',
        CustId: '',
        Age: '',
        Mobile: '',
        Email: '',
        MembercardNo: '',
        ContractNo: '',
        BranchName: '',
        InvoiceNo: returnList[9][1],
        FilterBranchId: this.filterBranchId,
        FilterStatusId: this.filterStatusId,
        FilterRoleId: this.filterRoleId
      }
      this.gridApi.destroyFilter('LastName');
      this.gridApi.destroyFilter('FirstName');
      this.gridApi.destroyFilter('CustId');
      this.gridApi.destroyFilter('Age');
      this.gridApi.destroyFilter('Mobile');
      this.gridApi.destroyFilter('Email');
      this.gridApi.destroyFilter('ContractNo');

    } else {
      filterMemberList = {
        LastName: returnList[0][1],
        FirstName: returnList[1][1],
        CustId: returnList[2][1],
        Age: returnList[3][1],
        Mobile: returnList[4][1],
        Email: returnList[5][1],
        MembercardNo: returnList[6][1],
        ContractNo: returnList[7][1],
        BranchName: returnList[8][1],
        InvoiceNo: returnList[9][1],
        FilterBranchId: this.filterBranchId,
        FilterStatusId: this.filterStatusId,
        FilterRoleId: this.filterRoleId
      }
    }


    // get members from DB. Using filter values
    this.memberService.getMembersList(filterMemberList).pipe(takeUntil(this.destroy$)).subscribe(
      members => {
        const rowData = [];
        members.Data.forEach(element => {
          const el = {
            LastName: element.LastName,
            FirstName: element.FirstName,
            CustId: element.CustId,
            Age: element.Age,
            Mobile: element.Mobile,
            Email: element.Email,
            MembercardNo: element.MembercardNo,
            ContractNo: element.MainContractNo,
            OrderAmount: element.OrderAmount,
            Status: element.StatusName,
            BranchName: element.BranchName,
            BranchId: element.BranchID,
            RoleId: element.RoleId,
            ID: element.ID,
            ContractId: element.ContractId,
            InvoiceNo: element.InvoiceNo
          }
          rowData.push(el)
        });
        if (rowData.length > 50 && rowData.length === 51) {
          rowData.pop();
          this.searchHasMoreMembers = true;
        }
        this.rowData = rowData;
      }
    );
  }

  visitedDbClick(member) {
    this.memberService.membercardVisit(member.ID).pipe(
      mergeMap(next => {
        return this.memberService.getMemberDetails(member.ID, member.BranchID, member.RoleId).pipe(
          takeUntil(this.destroy$)
        )
    }),
      takeUntil(this.destroy$)
    ).subscribe(membD => {
      this.basicInfoService.setSelectedMember(membD.Data);
      const url = '/membership/card/' + member.ID + '/' + member.BranchID + '/' + member.RoleId;
      this.router.navigate([url]);
    });
  }

  // navigate to member on dbclick
  dbClickToMemberCard(event) {
      this.memberService.membercardVisit(event.data.ID).pipe(
        mergeMap(next => {
          return this.memberService.getMemberDetails(event.data.ID, event.data.BranchId, event.data.RoleId).pipe(
            takeUntil(this.destroy$)
          )
        }),
        takeUntil(this.destroy$)
      ).subscribe(membD => {
        this.basicInfoService.setSelectedMember(membD.Data);
        const url = '/membership/card/' + event.data.ID + '/' + event.data.BranchId + '/' + event.data.RoleId;
        this.router.navigate([url]);
      })
  }

  newMemberModal(content: any) {
    this.newMemberRegisterView = this.modalService.open(content, {});
  }

  closeRegistration() {
    this.newMemberRegisterView.dismiss();
  }

  branchChange(branchId) {
    this.filterBranchId = branchId;
    this.filterChanged(null);
    this.memberService.getMemberCount(this.filterBranchId).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        this.activeCount = result.Data[0];
        this.inactiveCount = result.Data[1];
        this.allCount = result.Data[2];
    });
  }

  roleChange(roleid) {
    this.filterRoleId = roleid;
    this.filterChanged(null);
  }

  statusChange(statusId) {
    this.filterStatusId = statusId;
    this.filterChanged(null)
  }

  memberRegisterEvent(url: any) {
    this.newMemberRegisterView.dismiss();
    this.router.navigate([url]);
  }

  // clearing all filters in ag-grid
  clearFilters() {
    this.gridApi.setFilterModel(null);
    this.gridApi.onFilterChanged();
}

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.newMemberRegisterView) {
      this.newMemberRegisterView.dismiss();
    }
  }

}

