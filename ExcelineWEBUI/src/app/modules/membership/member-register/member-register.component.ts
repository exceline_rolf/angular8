import { Component, OnInit, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { ExcelineMember } from 'app/modules/membership/models/ExcelineMember';
import { McBasicInfoService } from 'app/modules/membership/member-card/mc-basic-info/mc-basic-info.service';
import { ExceMemberService } from 'app/modules/membership/services/exce-member.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { PostalArea } from 'app/shared/SystemObjects/Common/PostalArea';
import { BankAccountValidator } from 'app/shared/directives/exce-error/util/validators/bank-account-validator';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs';
import { MemberSearchType, ColumnDataType, EntitySelectionType, MemberRole } from 'app/shared/enums/us-enum';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { ExceSessionService } from 'app/shared/services/exce-session.service';
import { Router, NavigationExtras } from '@angular/router';
import { PreloaderService } from 'app/shared/services/preloader.service';
import { TranslateService } from '@ngx-translate/core';
import { CropperSettings } from '../../../shared/components/us-crop-image/cropperSettings';
import { UsCropImageComponent } from '../../../shared/components/us-crop-image/us-crop-image.component';
import { Bounds } from '../../../shared/components/us-crop-image/model/bounds';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';
import { ExcePostalcodeService } from '../../../shared/components/exce-postalcode/exce-postalcode.service';
import { ConfigService } from '@ngx-config/core';
import { TreeviewItem } from '../../../shared/components/us-checkbox-tree';
import value from '*.json';
import { AdminService } from '../../admin/services/admin.service';
import { ExceMessageService } from '../../../shared/components/exce-message/exce-message.service';
import { fakeAsync } from '@angular/core/testing';
import { takeUntil } from 'rxjs/operators';
import { Subject, timer } from 'rxjs';

@Component({
  selector: 'member-register',
  templateUrl: './member-register.component.html',
  styleUrls: ['./member-register.component.scss']
})
export class MemberRegisterComponent implements OnInit, OnDestroy {
  private newMemberRegisterView: any;
  private isMobileNumberValid = true;
  private isPrivateNumberValid = true;
  private isWorkNumberValid = true;
  private groupMemberList = [];
  private destroy$ = new Subject<void>();
  myFormSubmited = false;
  inputMobile: string;
  isEmailBlur = false;
  isCompanyEmailBlur = false;
  isAccountEmailBlur = false;
  isMobileBlur = false;
  isPrivateBlur = false;
  isWorkBlur = false;
  isMobileReq = false;
  isAddressReq = false;
  isPostalCodeReq = false;
  isMemberCardBlur = false;
  wizardNumber = 1;
  errorWizardNumber: number;
  isSaveFollowup = false;
  isValidFollowup = false;
  exceLoginService: any;
  timer: Observable<number>;
  sub: Subscription;
  registerMember: ExcelineMember;
  postalData?: PostalArea = new PostalArea();
  emailForm: FormGroup;
  public memberForm: FormGroup;
  public postalDataForm: FormGroup;
  public countryDataForm: FormGroup;
  loggedBranchId: number;
  isInitialize = false;
  isMember = true;
  memberBirthdate: any;
  nextStepDisabled = true;
  itemCount: number;
  entitySearchType: string;
  entitySelectionType: string;
  isDbPagination = true;
  currentState = 0;
  pageNumber = 1;
  selectedInvoiceCharge = 1;
  selectedSmsEmailInvoice = 0;
  gymSettings: any;
  countries: any;
  postalDataView: any;
  countryDataView: any;
  brisMemberView: any;
  memberCategoies: any;
  public introduceMemberList = [];
  items = [];
  employeeCategoryList = [];
  selectedEmpCategory: any;
  isBranchVisible = true;
  isStatusVisible = true;
  isRoleVisible = true;
  isAddBtnVisible = true;
  isBasicInforVisible = true;
  isDisabledRole = false;
  defaultRole = 'ALL'
  multipleRowSelect = false;
  addMemberView: any;
  columns: any;
  entitySelectionViewTitle: string;
  phoneNumberType;
  phoneNumberDetail: string;
  mobileExitMsg: string;
  public myForm: FormGroup;
  url: any;
  IsBrisIntegrated: boolean;
  BRISSearchResultList: any;
  isUsingIntroducedBy: boolean;
  introducedByView: any;
  introducedByAdded = false;
  postalCodeChange = false;


  firstname: any;
  lastname: any;
  number: any;
  age: any;
  mobile: any;
  status: any;
  gymname: any;

  selectedGymsettings: string[] = ['ReqAddressRequired', 'ReqMobileRequired', 'ReqZipCodeRequired', 'OtherIsShopAvailable', 'OtherPhoneCode']
  auotocompleteItems = [{ id: 'NAME', name: 'NAME :', isNumber: false }, { id: 'ID', name: 'Idd :', isNumber: true }]

  listOfCountryId = [];
  isSponsorCategorySelectionAvailable: boolean;

  @Output() memberSaveEvent = new EventEmitter();
  @Output() canceled = new EventEmitter();

  @ViewChild('memberRegisterWizard', { static: true }) public memberWizard;
  @ViewChild('followUpProfile', { static: true }) public followUpProfile;
  @ViewChild('message', { static: true }) public takenMSG;

  formErrors = {
    'BirthDay': '',
    'Gender': '',
    'FirstName': '',
    'LastName': '',
    'Address1': '',
    'Email': '',
    'PostCode': '',
    'Mobile': '',
    'PrivateTeleNo': '',
    'WorkTeleNo': '',
    'BankAccount': '',
    'MemberCardNo': '',
    'GuestCardNo': '',
    'AccountingEmail': '',
    'CompanyEmail': '',
    'CompanyName': '',
    'GatCardNo': ''
  };

  validationMessages = {
    'Age': {
      'required' : 'MEMBERSHIP.AgeReq'
    },
    'Gender': {
      'required' : 'MEMBERSHIP.MsgMemGenderReq'
    },
    'FirstName': {
      'required': 'MEMBERSHIP.MsgMemFirstNamerequired'
    },
    'LastName': {
      'required': 'MEMBERSHIP.MsgMemLastNamerequied'
    },
    'Address1': {
      'required': 'MEMBERSHIP.MsgMemAddressrequied'
    },
    'Email': {
      'email': 'MEMBERSHIP.MsgInvalidEmail',
      'pattern': 'MEMBERSHIP.EmailPattern',
      'required': 'MEMBERSHIP.EmailReq'

    },
    'PostCode': {
      'required': 'MEMBERSHIP.crPostalCodeReq',
      'postCodeToShort': 'MEMBERSHIP.postCodeToShort',
      'postCodeNotFound': 'MEMBERSHIP.postCodeNotExists'
    },
    'CompanyName': {
      'required': 'MEMBERSHIP.MsgComapnyNameRequired'
    },
    'Mobile': {
      'required': 'MEMBERSHIP.crMobileReq',
      'phoneNumber': 'MEMBERSHIP.InvalidNumber',
      'pattern': 'MEMBERSHIP.numOnlynums',
      'mobileTaken': 'MEMBERSHIP.NumberOccupied',
    },
    'PrivateTeleNo': {
      'phoneNumber': 'MEMBERSHIP.InvalidNumber',
      'pattern': 'MEMEBERSHIP.numOnlynums',
    },
    'WorkTeleNo': {
      'phoneNumber': 'MEMBERSHIP.InvalidNumber',
      'pattern': 'MEMBERSHIP.numOnlynums',
    },
    'BankAccount': {
      'BankAccount': 'Invalid Bank Account No.'
    },
    'MemberCardNo': {
      'memberCardTaken': 'MemberCard exit.',
      'pattern': 'MEMBERSHIP.numOnlynums',
    },
    'GatCardNo': {
      'gatCardTaken': 'GatCardNo exit.',
      'pattern': 'MEMBERSHIP.numOnlynums'
    },
    'AccountingEmail': {
      'email': 'Please enter valid email address'
    },
    'CompanyEmail': {
      'email': 'Please enter valid email address'
    },
    'GuestCardNo': {
      'pattern': 'MEMBERSHIP.numOnlynums',
    }
  };

  /* **********************************************************************
   * variables for cropping image
   * added on Fri Mar 30 2018 10:08:33 GMT+0530 (Sri Lanka Standard Time)
  **********************************************************************/
  atDecition = true;
  useWebcam = false;
  useFileUpload = false;
  reader = new FileReader();
  name: string;
  data1: any;
  cropperSettings1: CropperSettings;
  croppedWidth: number;
  croppedHeight: number;
  @ViewChild('cropper', {static: false}) cropper: UsCropImageComponent;
  iscontractsave = false;
  toUrl: string;
  guardianName: any;
  isGuardianSelected = false;
  branchId: any;
  intlOptions: { initialCountry: string; formatOnDisplay: boolean; separateDialCode: boolean; onlyCountries: string[]; autoPlaceholder: string; };
  genderSelected = false;
  isGuestCardBlur = false;

  aniMessage: any;
  type: string;
  isShowInfo: boolean;


  constructor(
    private basicInfoService: McBasicInfoService,
    private loginservice: ExceLoginService,
    private router: Router,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private memberService: ExceMemberService,
    private translateService: TranslateService,
    private exceSessionService: ExceSessionService,
    private postalCodeService: ExcePostalcodeService,
    private adminService: AdminService,
    private config: ConfigService,
    private MSN: ExceMessageService
  ) {
    this.emailForm = fb.group({
    });

    /***********************************************************************
    * initialize values for cropping image
    * added on Fri Mar 30 2018 10:08:33 GMT+0530 (Sri Lanka Standard Time)
    **********************************************************************/
    this.cropperSettings1 = new CropperSettings();
    this.cropperSettings1.width = 100;
    this.cropperSettings1.height = 100;

    this.cropperSettings1.croppedWidth = 200;
    this.cropperSettings1.croppedHeight = 200;

    this.cropperSettings1.canvasWidth = 200;
    this.cropperSettings1.canvasHeight = 200;

    this.cropperSettings1.minWidth = 10;
    this.cropperSettings1.minHeight = 10;

    this.cropperSettings1.rounded = false;
    this.cropperSettings1.keepAspect = false;

    this.cropperSettings1.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings1.cropperDrawSettings.strokeWidth = 2;

    this.data1 = {};
  }

  ngOnInit() {
    this.branchId = this.loginservice.SelectedBranch.BranchId;
    this.adminService.GetGymCompanySettings('other').pipe(takeUntil(this.destroy$)).subscribe(result => {
      this.IsBrisIntegrated = result.Data.IsBrisIntegrated;
    });
    this.loggedBranchId = this.loginservice.SelectedBranch.BranchId;
    this.registerMember = new ExcelineMember();
    this.registerMember.Role = 'MEM';
    this.registerMember.InvoiceChage = true;
    this.registerMember.SmsInvoice = false;
    // GETTING ALL SERACHVALUES FROM SETTINGS
    this.getMemberSearchCategory();
    this.memberForm = this.fb.group({
      'Gender': [null, Validators.required],
      'LastName': [null, Validators.required],
      'FirstName': [null, Validators.required],
      'CompanyName': [null],
      'Department': [null],
      'Address1': [null],
      'Address2': [null],
      'PostCode': [null],
      'PostPlace': [null],
      'country': [null],
      'MemberCardNo': [null, [Validators.pattern('^[0-9]*$')]],
      'MobilePrefix': [null],
      'Mobile': [null, Validators.pattern('^[0-9]$')],
      'PrivateTeleNoPrefix': [null],
      'PrivateTeleNo': [this.registerMember.PrivateTeleNo, [], this.privateNumberValidator.bind(this)],
      'WorkTeleNoPrefix': [null],
      'WorkTeleNo': [this.registerMember.WorkTeleNo, [], [this.workNumberValidator.bind(this)]],
      'Email': [null, [Validators.pattern('^[A-Åa-å0-9._%+-]+@[A-Åa-å0-9.-]+\.[A-Åa-å]{2,4}$')]],
      'AccountingEmail': [null],
      'CompanyEmail': [null],
      'VatNumber': [null],
      'Age': [null, Validators.required],
      'BirthDate': [null, Validators.required],
      'Role': [this.registerMember.Role],
      'GuestCardNo': [null, [Validators.pattern('^[0-9]*$')]],
      'GatCardNo': [null, [Validators.pattern('^[0-9]{20}$')]],
      'PinCode': [],
      'InvoiceChage': [this.registerMember.InvoiceChage],
      'SmsInvoice': [this.registerMember.SmsInvoice],
      'BankAccount': [null, [BankAccountValidator.BankAccount]],
      'EmployeeNo': [],
      'Ref': [],
      'DueDate': [],
      'AgressoId': [],
      'IntroducedByName': [],
      'IntroducedById': [],
      'IntroducedByCustId': [],
      'GuardianCustId': [],
      'GuardianId': [],
      'GroupId': [],
      'GroupCustId': [],
      'GroupNameForMember': [],
      'SponserId': [],
      'SponserCustId': [],
      'SponserName': [],
      'InstructorId': [],
      'InstructorCustId': [],
      'InstructorName': [],
      'ContactPersonId': [],
      'ContactPersonName': [],
      'employeeCategoryForSponserId': [],
      'employeeCategoryForSponser': []
    })

    this.adminService.getGymSettings('OTHER', null, this.branchId).pipe(takeUntil(this.destroy$)).subscribe( result => {
      if (result) {
        this.isUsingIntroducedBy = result.Data[0].IsUsingIntroducedBy;
      }
    });
    this.myForm = this.fb.group({
      intelNumber: [null, [Validators.required], [this.phoneNumberValidator.bind(this)]],
      extension: [null],
      amount: [null]
    });
    this.postalDataForm = this.fb.group({
      'postalCode': [this.postalData.postalCode, Validators.required],
      'postalName': [this.postalData.postalName, Validators.required],
      'population': [this.postalData.population],
      'houseHold': [this.postalData.houseHold]
    });
    this.countryDataForm = this.fb.group({
      'Id': [null, Validators.required],
      'Name': [null, Validators.required],
      'CountryCode': [null, Validators.required]
    });
    this.memberService.getCategories('MEMBER').pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.memberCategoies = result.Data;
          this.registerMember.MemberCategory = this.memberCategoies.find(x => x.Code === 'MEMBER')
        }
      }
    );
    this.memberService.getSelectedGymSettings({ settingNames: this.selectedGymsettings, isGymSetting: true, branchId: this.loggedBranchId }).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.gymSettings = result.Data;
          if (this.gymSettings.ReqAddressRequired === true) {
            this.memberForm.get('Address1').setValidators(Validators.required);
            this.memberForm.get('Address1').updateValueAndValidity();
            this.isAddressReq = true;
          }
          if (this.gymSettings.ReqZipCodeRequired === true) {
            this.memberForm.get('PostCode').setValidators(Validators.required);
            this.memberForm.get('PostCode').updateValueAndValidity();
            this.isPostalCodeReq = true;
          }
          if (this.gymSettings.ReqMobileRequired === true) {
            this.memberForm.get('Mobile').setValidators(Validators.required);
            this.memberForm.get('Mobile').updateValueAndValidity();
            this.isMobileReq = true;
          }
          this.memberForm.get('Mobile').setAsyncValidators(this.mobileNumberValidator.bind(this));
          this.memberForm.get('Mobile').updateValueAndValidity();

          // this.memberForm.get('PostCode').setValidators(this.postCodeValidator.bind(this));
          // this.memberForm.get('PostCode').updateValueAndValidity();

          // this.memberForm.get('Age').setValidators(Validators.required);
          // this.memberForm.get('Age').updateValueAndValidity();
          // this.memberForm.get('Gender').setValidators(Validators.required);
          // this.memberForm.get('Gender').updateValueAndValidity();
          // this.memberForm.get('LastName').setValidators(Validators.required);
          // this.memberForm.get('LastName').updateValueAndValidity();
          // this.memberForm.get('FirstName').setValidators(Validators.required);
          // this.memberForm.get('FirstName').updateValueAndValidity();

          this.memberForm.statusChanges.pipe(
            takeUntil(this.destroy$)
          ).subscribe(_ => UsErrorService.onValueChanged(this.memberForm, this.formErrors, this.validationMessages));
        }
      }
    );

    // ** GET SEARCH SETTINGS FOR MEMBER SEARCH MODALS **
    this.adminService.getCategoriesByType('SEARCHMEMBERSETTING').pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          // this.categoryItems = result.Data;
          // this.categoryItemsBase = this.categoryItems;
          // this.categoryResource = new DataTableResource(this.categoryItems);
          // this.categoryResource.count().then(count => this.categoryCount = count);
          // this.categoryCount = Number(this.categoryItems.length);

          // if (this.gymListService.SelectedGym) {
          //   this.fetchData(this.gymListService.SelectedGym);
          // }

        }

      });

    this.memberService.getCountries().pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.countries = result.Data;
          this.countries.forEach(element => {
            let tempName;
            this.listOfCountryId.push(element);
            const translateExpression = 'COUNTRIES.' + element.Id;
            this.translateService.get(translateExpression).pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tempName = tranlstedValue);
            element.DisplayName = tempName;
          });
          const selectedCountry = this.countries.filter(x => x.Id === 'NO');
          if (selectedCountry.length > 0) {
            this.getSelectedCountry(selectedCountry[0].Name, false);
          }
        }
      }
    );
    this.isSponserSelectAvailable();

    this.intlOptions = {
      initialCountry: 'no',
      formatOnDisplay: true,
      separateDialCode: true,
      onlyCountries: this.listOfCountryId,
      autoPlaceholder: 'off'
    };

    this.memberForm.get('Age').valueChanges.pipe(takeUntil(this.destroy$)).subscribe(age =>  {
      if (this.gymSettings.ReqMobileRequired === true) {
        return;
      }
      if (this.gymSettings.ReqMobileRequired === false && age < 18) {
        this.memberForm.get('Mobile').setValidators(Validators.required);
        this.memberForm.get('Mobile').updateValueAndValidity();
        this.isMobileReq = true;
      } else {
        this.memberForm.get('Mobile').clearValidators();
        this.memberForm.get('Mobile').updateValueAndValidity();
        this.isMobileReq = false;
      }
    });

    this.memberForm.patchValue( {
      MobilePrefix: '+47',
      PrivateTeleNoPrefix: '+47',
      WorkTeleNoPrefix: '+47'
    });

  } // ngOnInit end

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.postalDataView) {
      this.postalDataView.close();
    }
    if (this.countryDataView) {
      this.countryDataView.close();
    }
    if (this.brisMemberView) {
      this.brisMemberView.close();
    }
    if (this.newMemberRegisterView) {
      this.newMemberRegisterView.close();
    }
    if (this.addMemberView) {
      this.addMemberView.close();
    }
    if (this.introducedByView) {
      this.introducedByView.close();
    }
  }

  /***********************************************************************
    * functions for cropping image
    * added on Fri Mar 30 2018 10:08:33 GMT+0530 (Sri Lanka Standard Time)
    **********************************************************************/
  wantToUploadFile() {
    this.atDecition = false;
    this.useFileUpload = true;
    this.useWebcam = false;
  }

  wantToUseWebCam() {
    this.atDecition = false;
    this.useFileUpload = false;
    this.useWebcam = true;
  }

  cropped(bounds: Bounds) {
    this.croppedHeight = bounds.bottom - bounds.top;
    this.croppedWidth = bounds.right - bounds.left;
  }

  fileChangeListener($event) {
    const image: any = new Image();
    const file: File = $event.target.files[0];
    const myReader: FileReader = new FileReader();
    const that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
    };
    myReader.readAsDataURL(file);
  }

  cancelAction() {
    this.atDecition = true;
    this.useFileUpload = false;
    this.useWebcam = false;
    this.data1 = {};
  }

  phoneNumberValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
      }, 10);
    });
  }

  fullValueChange2(event) {
    this.phoneNumberDetail = event;
    this.myForm.patchValue({
      intelNumber: event.value,
      extension: event.extension
    });
  }

  fullValueChange(event: any, type: string) {
    this.phoneNumberDetail = event;
    if (type === 'MOBILE') {
      this.memberForm.patchValue({
        Mobile: event.value,
      });
      this.memberForm.patchValue({
        MobilePrefix: event.extension
      });
      this.isMobileNumberValid = event.numberType === 1 && event.valid;
    } else if (type === 'PRIVATE') {
      this.memberForm.patchValue({
        PrivateTeleNo: event.value,
      });
      this.memberForm.patchValue({
        PrivateTeleNoPrefix: event.extension
      });
      this.isPrivateNumberValid = event.value === '' || event.valid;
    } else if (type === 'WORK') {
      this.memberForm.patchValue({
        WorkTeleNo: event.value,
      });
      this.memberForm.patchValue({
        WorkTeleNoPrefix: event.extension
      });
      this.isWorkNumberValid = event.value === '' || event.valid;
    }
  }

  // postCodeValidator(control: AbstractControl, type: string) {
  //   return new Promise(resolve => {
  //     setTimeout(() => {
  //       if (this.memberForm.get('PostCode').value.length < 4) {
  //         resolve({
  //           'phoneNumber': true
  //         })
  //       } else {
  //         resolve(null);
  //       }
  //     }, 10);
  //   })
  // }

  mobileNumberValidator(control: AbstractControl, type: string) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (!this.isMobileNumberValid) {
          resolve({
            'phoneNumber': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    })
  }

  // ** GETTING ALL SEARCHITEMS
  getMemberSearchCategory() {
    this.memberService.getMemberSearchCategory(this.loggedBranchId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        //  result.Data = '1-Id-Idd/1-custId-custIdd/0-Name-Namee/0-address-addresss'
        this.auotocompleteItems = [];
        const keywordparts = result.Data.split('/');
        for (let i = 0; i < keywordparts.length; i++) {
          const parts = keywordparts[i].split('-');
          if (parts.length > 2) {
            if (parts[1] === 'BIRTHDATE') {
              const searchItem = { id: parts[1], name: parts[2] + '[ddmmyyyy] :', isNumber: parts[0] === 1 };
              this.auotocompleteItems.push(searchItem)
            } else {
              const searchItem = { id: parts[1], name: parts[2] + ' : ', isNumber: parts[0] === 1 };
              this.auotocompleteItems.push(searchItem)
            }
          }
        }
        // foreach loop ended
        // translate names of searchparamameters
        this.auotocompleteItems.forEach(element => {
          let tempName;
          const translateExpression = 'MEMBERSHIP.' + element.id;
          this.translateService.get(translateExpression).pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tempName = tranlstedValue);
          element.name = tempName  + ' : ';
        });
      } else {
      }
    })
  }

  privateNumberValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (!this.isPrivateNumberValid) {
          resolve({
            'phoneNumber': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    })
  }

  workNumberValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (!this.isWorkNumberValid) {
          resolve({
            'phoneNumber': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    })
  }

  mobileBlur(val: any, type: string) {
    if (val !== '') {
      const msg =  this.takenMSG.nativeElement.innerHTML;
      switch (type) {
        case 'MOBILE':
          this.isMobileBlur = true;
          if (this.memberForm.controls['Mobile'].errors == null || (!this.memberForm.controls['Mobile'].errors.required &&
            !this.memberForm.controls['Mobile'].errors.phoneNumber)) {
            // må levere inn prefix uten +
            let index = this.memberForm.value.MobilePrefix.indexOf('+')
            let valu = this.memberForm.value.MobilePrefix;
            if (index === 0) {
              index = this.memberForm.value.MobilePrefix.split('+')[1];
              valu = index;
            }
            this.memberService.validateMobile(this.memberForm.value.Mobile, valu).pipe(takeUntil(this.destroy$)).subscribe(
              result => {
                const mobileTakenInfo = result.Data;
                if (mobileTakenInfo != null &&  mobileTakenInfo.Item1 !== -1) {
                  const outputMemberId = mobileTakenInfo.Item1;
                  const outputMemberName = mobileTakenInfo.Item2;
                  const errorMsg = outputMemberId + '-' + outputMemberName + ' ' + msg;
                  this.mobileExitMsg = errorMsg;
                  this.memberForm.controls['Mobile'].setErrors({
                    'mobileTaken': true
                  });
                  this.formErrors.Mobile = errorMsg;
                }
              }
            )
          }
          break;
        case 'PRIVATE':
          this.isPrivateBlur = true;
          break;
        case 'WORK':
          this.isWorkBlur = true;
          break;
      }
    }
  }

  mobileFocus(type: string) {
    switch (type) {
      case 'MOBILE':
        this.isMobileBlur = false
        break;
      case 'PRIVATE':
        this.isPrivateBlur = false
        break;
      case 'WORK':
        this.isWorkBlur = false
        break;
    }
  }

  checkFollowUpSave() {
    if (this.isSaveFollowup && this.registerMember.Role === 'MEM') {
      this.isValidFollowup = this.followUpProfile.validateFollowUpProfileForMember();
      return this.isValidFollowup;
    } else {
      this.isValidFollowup = false;
      return true;
    }
  }

  followUpMobileBlur($event) {
    this.memberForm.patchValue({
      Mobile: $event.Mobile,
    });
    this.memberForm.patchValue({
      MobilePrefix: $event.MobilePrefix
    });
  }

  followUpEmaileBlur($event) {
    this.memberForm.patchValue({
      Email: $event
    });
  }

  onComplete() {
    this.myFormSubmited = true;
    const companyInputsValid = (this.memberForm.get('CompanyName').valid && this.memberForm.get('PostCode').valid && this.memberForm.get('MemberCardNo').valid)
    if ((this.memberForm.valid && this.genderSelected && this.registerMember.Role === 'MEM') || (companyInputsValid && this.registerMember.Role === 'COM')) {
      if (this.checkFollowUpSave()) {
        let memberId;
        // ekstra sjekk på fødselsdato da formerrors ikke fanger opp at det mangler verdi i validator
        if (this.registerMember.BirthDate === undefined && this.registerMember.Role !== 'COM') {
          UsErrorService.validateAllFormFields(this.memberForm, this.formErrors, this.validationMessages);
          if (this.registerMember.Role === 'MEM') {
            this.errorStateHandle();
          }
          return
        }
        const empSponserCategory = this.employeeCategoryList.find(x => x.Id === Number(this.memberForm.value.employeeCategoryForSponserId)) // don't use ====
        if (empSponserCategory) {
          this.memberForm.patchValue({ employeeCategoryForSponser: empSponserCategory })
        }

        const member = Object.assign({}, this.memberForm.value);
        member.FirstName = this.registerMember.Role === 'COM' ? this.memberForm.value.Department : this.memberForm.value.FirstName;
        member.LastName = this.registerMember.Role === 'COM' ? this.memberForm.value.CompanyName : this.memberForm.value.LastName;
        member.Gender = this.registerMember.Role === 'MEM' ? this.registerMember.Gender : 'NONE';
        member.Role = this.registerMember.Role;
        member.MemberStatuse = 2;
        member.CountryId = this.registerMember.CountryId;
        member.BirthDate = this.registerMember.BirthDate;
        member.MemberCategory = this.registerMember.MemberCategory;
        member.Name = member.FirstName + ' ' + member.LastName;
        member.BranchId = this.loggedBranchId;
        member.InfoCategoryList = null;
        member.IntroduceMemberLst = this.introduceMemberList;
        member.GroupMemberList = this.groupMemberList;
        member.Image = this.data1.image;
        member.AccountNo = member.BankAccount;
        member.CompanyName = this.memberForm.value.CompanyName

        const memberModel = {
          Member: member,
          BranchId: this.loggedBranchId,
          NotificationTitle: ''
        }
        this.memberService.saveMember(memberModel).pipe(takeUntil(this.destroy$)).subscribe(
          result => {
            if (result) {
              const sTextArray = result.Data.split(':');
              if (sTextArray.length > 0) {
                memberId = sTextArray[0];
                if (memberId > 0) {
                  if (sTextArray.Length > 1) {
                    member.EntNo = Number(sTextArray[1]);
                  }
                  if (sTextArray.Length > 2) {
                    member.CustId = sTextArray[2].ToString();
                  }
                  if (sTextArray.Length > 3) {
                    member.GroupMemberLstCount = Number(sTextArray[3]);
                  }
                }
              }
              if (result && this.data1.image) {
                const date = new Date();
                const notification = {
                  'MemberId': Number(memberId),
                  'BranchId': Number(this.loggedBranchId),
                  'Notification': {
                    'Title': 'IMAGE ADDED',
                    'Description': 'Image was added when member was registered',
                    'CreatedDate': date,
                    'TypeId': 3,
                    'SeverityId': 3,
                    'StatusId': 1
                    }
                  }
                  this.memberService.AddEventlogForImangeChange(notification).pipe(takeUntil(this.destroy$)).subscribe(
                    res => {},
                    error => {},
                    () => {}
                  );
              }
              if (memberId > 0) {
                member.Id = memberId;
                this.memberService.getMemberDetails(memberId, this.loggedBranchId, this.registerMember.Role).pipe(takeUntil(this.destroy$)).subscribe(
                  // tslint:disable-next-line:no-shadowed-variable
                  result => {
                    if (result) {
                      const NewMember = result.Data
                      this.basicInfoService.setSelectedMember(NewMember);
                      if (this.iscontractsave) {
                        this.toUrl = '/membership/card/' + NewMember.Id + '/' + this.loggedBranchId + '/' + this.registerMember.Role + '/contracts/register-contract';
                      } else {
                        this.toUrl = '/membership/card/' + memberId + '/' + this.loggedBranchId + '/' + this.registerMember.Role;
                      }
                      if (this.isValidFollowup) {
                        this.followUpProfile.saveFollowUpFromMember({ 'MemberId': member.Id, 'Mobile': member.Mobile, 'MobilePrefix': member.MobilePrefix, 'Email': member.Email })
                      }
                      this.memberSaveEvent.emit(this.toUrl);
                      // this.router.navigate([url]);
                      // implemente BRIS integration
                    }
                  });
              } else if (memberId === -1) {
                //  USMessage.Show(_errorSaveMsg);
              }
            }
          });
      } else {
        if (this.currentState !== 4) { // not in the follwip tab
          this.memberWizard.errorState(4); // going to the followup tab
        }
      }
    } else {
      UsErrorService.validateAllFormFields(this.memberForm, this.formErrors, this.validationMessages);
      if (this.registerMember.Role === 'MEM') {
        this.errorStateHandle();
      }
      // UsErrorService.validateAllFormFields(this.memberForm, this.formErrors, this.validationMessages);
    }
  }

  onCompleteWithContract() {
    if (this.memberForm.get('Age').value < 18 && this.memberForm.get('GuardianCustId').value === null) {
      this.MSN.openMessageBox('WARNING',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.MsgCannotSignUp'
      });
    } else {
      this.iscontractsave = true;
      this.onComplete()
    }
  }

  closeAddMemberView() {
    this.newMemberRegisterView.close();
  }

  private errorStateHandle() {
    if (this.currentState !== 0 && (this.memberForm.controls['FirstName'].invalid || this.memberForm.controls['LastName'].invalid || this.memberForm.controls['Email'].invalid
      || this.memberForm.controls['Mobile'].invalid || this.memberForm.controls['PrivateTeleNo'].invalid || this.memberForm.controls['WorkTeleNo'].invalid
      || this.memberForm.controls['Address1'].invalid || this.memberForm.controls['PostCode'].invalid
      || this.memberForm.controls['Gender'].invalid || this.memberForm.controls['Age'].invalid)) {

    } else if (this.currentState !== 1 && this.memberForm.controls['BankAccount'].invalid) {
      this.memberWizard.errorState(1);
    }
  }

  private emailValidator(control: AbstractControl): ValidationErrors {
    if (!control.value) {
      return null;
    }
    return Validators.email(control);
  }

  openPostalCodeModal(content: any) {
    this.postalDataView = this.modalService.open(content, { width: '300' });
  }

  closePostalCodeModal() {
    this.postalDataView.close();
  }

  openCountryModal(content: any) {
    this.countryDataView = this.modalService.open(content, { width: '300' });
  }

  openBrisMemberModal(content: any) {
    this.brisMemberView = this.modalService.open(content);
  }

  saveMember() {
    this.myFormSubmited = true;
    const empSponserCategory = this.employeeCategoryList.find(x => x.Id === this.memberForm.value.employeeCategoryForSponserId) // don't use ====
    if (empSponserCategory) {
      this.memberForm.patchValue({ employeeCategoryForSponser: empSponserCategory });
    }
  }

  employeeSelectionModal(content: any) {
    this.translateService.get('MEMBERSHIP.SelectEmployee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
    this.isBranchVisible = false;
    this.isRoleVisible = false;
    this.isStatusVisible = false;
    this.isAddBtnVisible = false;
    this.isDbPagination = false;
    this.isBasicInforVisible = false;
    this.entitySelectionType = EntitySelectionType[EntitySelectionType.INS];
    this.columns = [{ property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'CustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ]
    PreloaderService.showPreLoader();
    this.memberService.getInstructors('', true).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        PreloaderService.hidePreLoader();
        this.items = result.Data;
        this.itemCount = result.Data.length;
        this.newMemberRegisterView = this.modalService.open(content, { width: '800' });
      } else {
        PreloaderService.hidePreLoader();
      }
    }, error => {
      PreloaderService.hidePreLoader();
    })
  }

  newMemberModal(content: any, searchType: any) {
    this.translateService.get('COMMON.MemberSelection').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
    this.entitySearchType = searchType;
    this.isAddBtnVisible = true;
    this.isBranchVisible = true;
    this.isRoleVisible = true;
    this.isStatusVisible = true;
    this.isAddBtnVisible = true;
    this.isDbPagination = true;
    this.isBasicInforVisible = true;
    this.defaultRole = 'ALL'
    this.multipleRowSelect = false;
    this.items = [];
    this.columns = [];
    this.entitySelectionType = EntitySelectionType[EntitySelectionType.MEM];

    // Column headers translation BEGIN
    this.translateService.get('COMMON.FirstName').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.firstname = tranlstedValue);
    this.translateService.get('COMMON.LastName').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.lastname = tranlstedValue);
    this.translateService.get('COMMON.Number').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.number = tranlstedValue);
    this.translateService.get('COMMON.Age').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.age = tranlstedValue);
    this.translateService.get('COMMON.Mobile').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.mobile = tranlstedValue);
    this.translateService.get('COMMON.Status').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.status = tranlstedValue);
    this.translateService.get('COMMON.GymName').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.gymname = tranlstedValue);
    // End

    if (searchType === MemberSearchType[MemberSearchType.INTRODUCEDBY] || searchType === MemberSearchType[MemberSearchType.GUARDIAN] ||
      searchType === MemberSearchType[MemberSearchType.GROUP] || searchType === MemberSearchType[MemberSearchType.SPONSORS]
      || searchType === MemberSearchType[MemberSearchType.INTRODUCER] || searchType === MemberSearchType[MemberSearchType.CONTACTPERSON]
      || searchType === MemberSearchType[MemberSearchType.LINKCUSTOMER]) {
      this.columns = [{ property: 'FirstName', header: this.firstname, sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'LastName', header: this.lastname, sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'IntCustId', header: this.number, sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'Age', header: this.age, sortable: false, resizable: false, width: '150px', type: ColumnDataType.DATE },
      { property: 'Mobile', header: this.mobile, sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'StatusName', header: this.status, sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'BranchName', header: this.gymname, sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
      ]
    }
    if (searchType === MemberSearchType[MemberSearchType.SPONSORS]) {
      this.defaultRole = 'SPO'
      this.isDisabledRole = true;
    }
    if (searchType === MemberSearchType[MemberSearchType.INTRODUCER] || searchType === MemberSearchType[MemberSearchType.LINKCUSTOMER]) {
      this.multipleRowSelect = true;
    }

    this.newMemberRegisterView = this.modalService.open(content, {});
    // this.items
    // const searchType = MemberSearchType[MemberSearchType.LINKCUSTOMER];
    // this.memberService.getMembers( this.loggedBranchId,'', 9,searchType,'MEM',1,false,
    //   false, ''
    // ).pipe(takeUntil(this.destroy$)).subscribe
    //   (result => {
    //     if (result) {
    //       this.items = result.Data;

    //       this.newMemberRegisterView = this.modalService.open(content, {});
    //       // this.reloadInActiveDataGrid(inActiveResult.Data);
    //       // this.updateAllMember();
    //     } else {
    //     }
    //   }
    //   )
  }

  searchDeatail(val: any) {
    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.MEM]:
        PreloaderService.showPreLoader();
        this.memberService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
          this.entitySearchType, val.roleSelected.id, val.hit, false,
          false, ''
        ).pipe(takeUntil(this.destroy$)).subscribe(result => {
          if (result) {
            PreloaderService.hidePreLoader();
            this.items = result.Data;
            this.itemCount = result.Data.length;
          } else {
            PreloaderService.hidePreLoader();
          }
        });
        break;
      case EntitySelectionType[EntitySelectionType.INS]:
        PreloaderService.showPreLoader();
        this.memberService.getInstructors(val.searchVal, true
        ).pipe(takeUntil(this.destroy$)).subscribe(result => {
          if (result) {
            PreloaderService.hidePreLoader();
            this.items = result.Data;
          } else {
            PreloaderService.hidePreLoader();
          }
        });
        break;
    }
  }

  addEntity(item: any) {
    const name = item.FirstName + ' ' + item.LastName;
    switch (this.entitySearchType) {
      case MemberSearchType[MemberSearchType.INTRODUCEDBY]:
        this.memberForm.patchValue({ IntroducedById: item.ID })
        this.memberForm.patchValue({ IntroducedByCustId: item.CustId })
        this.memberForm.patchValue({ IntroducedByName: name })
        break;

      case MemberSearchType[MemberSearchType.GUARDIAN]:
        this.memberForm.patchValue({ GuardianId: item.Id })
        this.memberForm.patchValue({ GuardianCustId: item.CustId })
        this.addMemberView.close();
        break;

      case MemberSearchType[MemberSearchType.GROUP]:
        this.memberForm.patchValue({ GroupId: item.Id })
        this.memberForm.patchValue({ GroupCustId: item.CustId })
        this.memberForm.patchValue({ GroupNameForMember: item.Name })
        this.addMemberView.close();
        break;

      case MemberSearchType[MemberSearchType.SPONSORS]:
        this.memberForm.patchValue({ SponserId: item.Id })
        this.memberForm.patchValue({ SponserCustId: item.CustId })
        this.memberForm.patchValue({ SponserName: item.Name })
        this.addMemberView.close();
        break;

      case MemberSearchType[MemberSearchType.CONTACTPERSON]:
        this.memberForm.patchValue({ ContactPersonId: item.Id })
        this.memberForm.patchValue({ ContactPersonName: item.Name })
        this.addMemberView.close();
        break;

      case MemberSearchType[MemberSearchType.INTRODUCER]:
        this.introduceMemberList.push(item);
        this.addMemberView.close();
        break;

      case MemberSearchType[MemberSearchType.LINKCUSTOMER]:
        this.groupMemberList.push(item);
        this.addMemberView.close();
        break;
    }
    // this.addMemberView.close();
  }

  deleteIntroduceMember(member: any) {
    const index: number = this.introduceMemberList.indexOf(member);
    if (index !== -1) {
      this.introduceMemberList.splice(index, 1);
    } else {
    }
  }

  deleteLinkMember(member: any) {
    const index: number = this.groupMemberList.indexOf(member);
    if (index !== -1) {
      this.groupMemberList.splice(index, 1);
    } else {
    }

  }

  getMemberRole() {

  }

  getMemberSearcType() {

  }

  selectedRowItem(item) {
    if (this.entitySelectionType === EntitySelectionType[EntitySelectionType.MEM]) {
      switch (this.entitySearchType) {
        case MemberSearchType[MemberSearchType.INTRODUCEDBY]:
          this.memberForm.patchValue({ IntroducedById: item.Id })
          this.memberForm.patchValue({ IntroducedByCustId: item.CustId })
          this.memberForm.patchValue({ IntroducedByName: item.Name })
          break;

        case MemberSearchType[MemberSearchType.GUARDIAN]:
          this.memberForm.patchValue({ GuardianId: item.Id })
          this.memberForm.patchValue({ GuardianCustId: item.CustId })
          this.memberForm.patchValue({ GuardianName: item.Name })
          this.guardianName = item.Name;
          this.isGuardianSelected = true;
          break;

        case MemberSearchType[MemberSearchType.GROUP]:
          this.memberForm.patchValue({ GroupId: item.Id })
          this.memberForm.patchValue({ GroupCustId: item.CustId })
          this.memberForm.patchValue({ GroupNameForMember: item.Name })
          break;

        case MemberSearchType[MemberSearchType.SPONSORS]:
          this.memberForm.patchValue({ SponserId: item.Id })
          this.memberForm.patchValue({ SponserCustId: item.CustId })
          this.memberForm.patchValue({ SponserName: item.Name })
          // debugger
          this.memberService.getEmployeeCategoryBySponsorId(this.loggedBranchId, item.Id
          ).pipe(takeUntil(this.destroy$)).subscribe(result => {
            if (result) {
              result.Data.EmployeeCategoryList.forEach(catItem => {
                this.employeeCategoryList.push(catItem)
              });
            } else {
            }
          });

          break;

        case MemberSearchType[MemberSearchType.CONTACTPERSON]:
          this.memberForm.patchValue({ ContactPersonId: item.Id })
          this.memberForm.patchValue({ ContactPersonName: item.Name })
          break;
      }
    } else if (this.entitySelectionType === EntitySelectionType[EntitySelectionType.INS]) {
      this.memberForm.patchValue({ InstructorId: item.Id })
      this.memberForm.patchValue({ InstructorCustId: item.CustId })
      this.memberForm.patchValue({ InstructorName: item.Name })
    }
    this.newMemberRegisterView.close();
  }

  selectedRowItems(item) {
    if (this.entitySelectionType === EntitySelectionType[EntitySelectionType.MEM]) {
      switch (this.entitySearchType) {
        case MemberSearchType[MemberSearchType.INTRODUCER]:
          item.forEach(i => {
            this.introduceMemberList.push(i);
          })
          break;

        case MemberSearchType[MemberSearchType.LINKCUSTOMER]:
          item.forEach(i => {
            this.groupMemberList.push(i);
          })
          break;
      }
    }
    this.newMemberRegisterView.close();
  }

  deleteContactPerson() {
    this.memberForm.patchValue({ ContactPersonId: null })
    this.memberForm.patchValue({ ContactPersonName: null })
  }

  addMemberModal(content: any) {
    this.addMemberView = this.modalService.open(content, { width: '800' });
    this.newMemberRegisterView.close();
  }

  getSelectedCountry(selectCountry, initialLoad: boolean) {
    if (this.countries) {
      let country: any;
      if (initialLoad === true) {
        country = this.countries.filter(x => x.Id === selectCountry);
      } else {
        country = this.countries.filter(x => x.Name === selectCountry);
      }

      if (country && country.length > 0) {
        this.registerMember.CountryId = country[0].Id;
        this.registerMember.CountryName = country[0].Name;
        // this.registerMember.MobilePrefix = country[0].CountryCode;
        this.registerMember.PrivateTeleNoPrefix = country[0].CountryCode;
        this.registerMember.WorkTeleNoPrefix = country[0].CountryCode;
        // this.memberForm.patchValue({ MobilePrefix: country[0].CountryCode })
        // this.memberForm.patchValue({ PrivateTeleNoPrefix: country[0].CountryCode })
        // this.memberForm.patchValue({ WorkTeleNoPrefix: country[0].CountryCode })
      }
    }
  }

  defaultPrefixSet(type) {
    switch (type) {
      case 'MobilePrefix':
      this.memberForm.patchValue({ MobilePrefix: '+47'})
      break;
      case 'PrivateTeleNoPrefix':
      this.memberForm.patchValue({ PrivateTeleNoPrefix: '+47'})
      break;
      case 'WorkTeleNoPrefix':
      this.memberForm.patchValue({ WorkTeleNoPrefix: '+47'})
      break;
    }
  }

  prefixChange(val, type) {
    if (val.length > 0) {
      const index = val.indexOf('+')
      let valu = val;
      let length = 0;
      if (index === 0) {
        if (val.split('+').length === 2) {
          // nothin to see here, move along
        } else {
          this.defaultPrefixSet(type);
          this.translateService.get('MEMBERSHIP.PreFixError').pipe(takeUntil(this.destroy$)).subscribe(trans => this.aniMessage = trans);
          this.type = 'DANGER';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
        }
        length = val.split('+')[1];
        valu = index;
      } else if (index > 0) {
        this.defaultPrefixSet(type);
        this.translateService.get('MEMBERSHIP.PreFixError').pipe(takeUntil(this.destroy$)).subscribe(trans => this.aniMessage = trans);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      } else {
        // missing +
        // this.memberForm.patchValue({ MobilePrefix: '+' + val})
        switch (type) {
          case 'MobilePrefix':
          this.memberForm.patchValue({ MobilePrefix: '+' + val})
          break;
          case 'PrivateTeleNoPrefix':
          this.memberForm.patchValue({ PrivateTeleNoPrefix: '+' + val})
          break;
          case 'WorkTeleNo':
          this.memberForm.patchValue({ WorkTeleNo: '+' + val})
          break;
        }
        this.translateService.get('MEMBERSHIP.PreFixError').pipe(takeUntil(this.destroy$)).subscribe(trans => this.aniMessage = trans);
        this.type = 'WARNING';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);

      }
      if (length > 0) {

      }
    } else {
      this.memberForm.patchValue({ MobilePrefix: '+47'})
    }
  }

  isSponserSelectAvailable() {
    // Disable SponsorCategory selector in html if no sponsor is selected.
    if ( (this.memberForm.get('SponserName').value === null) && (this.memberForm.get('SponserCustId').value === null)) {
      this.isSponsorCategorySelectionAvailable = true;
    } else {
      this.isSponsorCategorySelectionAvailable = false;
    }
  }

  removeSelectedSponsor() {
    // Clear selected sponsor
    this.memberForm.patchValue({
      'SponserName': '',
      'SponserCustId': ''
      });
      this.isSponsorCategorySelectionAvailable = true;
      this.employeeCategoryList.length = 0;
  }
  getCityforPostalCode(postalCode: any) {
    this.postalCodeChange = false;
    if (postalCode && postalCode.length >= 4) {
      this.memberForm.controls['PostCode'].setErrors({
      'postCodeToShort': false
      });
      this.memberService.getCityForPostalCode(postalCode).pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          if (result) {
            this.memberForm.patchValue({ PostCode: postalCode, PostPlace: result.Data });
            if (result.Data === '') {
              this.postalCodeChange = true;
              this.memberForm.patchValue({ PostCode: '' });
              this.memberForm.controls['PostCode'].setErrors({
                'postCodeNotFound': true
              });
            }
          }
        })
    } else {
      this.memberForm.patchValue({ PostPlace: '' });
      this.memberForm.controls['PostCode'].setErrors({
        'postCodeToShort': true
      });
    }
  }

  addPostalCode(postalCodeItem: any) {
    this.memberForm.patchValue({ PostCode: postalCodeItem.postalCode })
    this.memberForm.patchValue({ PostPlace: postalCodeItem.postalName })
    this.postalDataView.close();
  }

  generatePinCode() {
    const val = Math.floor(1000 + Math.random() * 9000);
    this.memberForm.patchValue({ PinCode: val });
  }

  createGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      // tslint:disable-next-line:no-bitwise
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  getGatCardNo() {
    const sessionKey = this.createGuid();
    if (sessionKey) {
      // this.addSession('GATCARDNO', '', sessionKey, 'alpha/exceadmin');
      this.exceSessionService.AddSession('GATCARDNO', '', sessionKey, 'alpha/exceadmin');
      this.timer = timer(0, 2000);
      this.sub = this.timer.pipe(takeUntil(this.destroy$)).subscribe(t => this.tickerFunc(t, sessionKey));
    }
  }

  tickerFunc(tick: number, sessionKey: string) {
    this.memberService.getSessionValue(sessionKey).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result.Data) {
          this.sub.unsubscribe();
          if (result.Data === 'ERROR') {
            // Error while uploading - add message
          } else {
            this.memberService.validateGatCardNo(result.Data).pipe(takeUntil(this.destroy$)).subscribe(
              getCardResult => {
                if (getCardResult.Data) {
                  if (!getCardResult.Data) {
                    this.memberForm.controls['GatCardNo'].setErrors({
                      'gatCardTaken': true
                    });
                  } else {
                    this.memberForm.patchValue({ GatCardNo: result.Data });
                  }
                }
              })
          }
        } else if (tick === 9) {
          this.sub.unsubscribe();
        }
      });

    // if (tick === 9) {
    //   this.sub.unsubscribe();
    // }
    //  this.ticks = tick
  }

  // SessionError() {
  //         alert('Communication manager is not running');
  //     }

  setSmsEmailInvoice(type: number) {
    if (type === 1) {
      this.registerMember.SmsInvoice = true;
      this.memberForm.patchValue({ SmsInvoice: true });
    } else {
      this.registerMember.SmsInvoice = false;
      this.memberForm.patchValue({ SmsInvoice: false });
    }
  }

  setInvoiceCharge(type: number) {
    if (type === 1) {
      this.registerMember.InvoiceChage = true;
      this.memberForm.patchValue({ InvoiceChage: true });
    } else {
      this.registerMember.InvoiceChage = false;
      this.memberForm.patchValue({ InvoiceChage: false });
    }
  }

  setMemberType(memberType: string) {
    this.myFormSubmited = false;
    this.registerMember.Role = memberType;
    this.setValidation(memberType)
    if (memberType === 'COM') {
      this.memberWizard.setOneStepOnly = true;
      this.memberWizard.backStepVisible = false;
      this.memberWizard.nextStepVisible = false;
      this.registerMember.MemberCategory = this.memberCategoies.find(x => x.Code === 'COMPANY');
      this.memberForm.get('Mobile').clearValidators();
      this.memberForm.get('Mobile').updateValueAndValidity();
      // this.memberWizard.hasNextStep = false;
      // **  Hiding red sign over mobile --> required **
      this.isMobileReq = false;
    } else if (memberType === 'MEM') {
      this.memberWizard.setOneStepOnly = false;
      this.isMember = true;
      this.memberWizard.backStepVisible = true;
      this.memberWizard.nextStepVisible = true;
      // this.memberWizard.hasNextStep = true;
      if (this.gymSettings.ReqMobileRequired === true) {
        this.memberForm.get('Mobile').setValidators(Validators.required);
        this.memberForm.get('Mobile').updateValueAndValidity();
        this.isMobileReq = true;
      }
      this.registerMember.MemberCategory = this.memberCategoies.find(x => x.Code === 'MEMBER');
    }
  }

  setValidation(roleType: string) {
    if (roleType === 'MEM') {
      this.memberForm.get('Age').setValidators(Validators.required);
      this.memberForm.get('Age').updateValueAndValidity();
      this.memberForm.get('Gender').setValidators(Validators.required);
      this.memberForm.get('Gender').updateValueAndValidity();
      this.memberForm.get('LastName').setValidators(Validators.required);
      this.memberForm.get('LastName').updateValueAndValidity();
      this.memberForm.get('MemberCardNo').setValidators(Validators.required);
      this.memberForm.get('MemberCardNo').updateValueAndValidity();
      this.memberForm.get('FirstName').setValidators(Validators.required);
      this.memberForm.get('FirstName').updateValueAndValidity();
      this.memberForm.get('Email').setValidators(this.emailValidator.bind(this));
      this.memberForm.get('Email').updateValueAndValidity();
      this.memberForm.get('CompanyName').setValidators(null);
      this.memberForm.get('CompanyName').updateValueAndValidity();
      this.memberForm.get('AccountingEmail').setValidators(null);
      this.memberForm.get('AccountingEmail').updateValueAndValidity();
      this.memberForm.get('CompanyEmail').setValidators(null);
      this.memberForm.get('CompanyEmail').updateValueAndValidity();
    } if (roleType === 'COM') {
      this.memberForm.get('LastName').setValidators(null);
      this.memberForm.get('LastName').updateValueAndValidity();
      this.memberForm.get('FirstName').setValidators(null);
      this.memberForm.get('FirstName').updateValueAndValidity();
      this.memberForm.get('Email').setValidators(null);
      this.memberForm.get('Email').updateValueAndValidity();
      this.memberForm.get('CompanyName').setValidators(Validators.required);
      this.memberForm.get('CompanyName').updateValueAndValidity();
      this.memberForm.get('AccountingEmail').setValidators(this.emailValidator.bind(this));
      this.memberForm.get('AccountingEmail').updateValueAndValidity();
      this.memberForm.get('CompanyEmail').setValidators(this.emailValidator.bind(this));
      this.memberForm.get('CompanyEmail').updateValueAndValidity();
    }
    this.memberForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => UsErrorService.onValueChanged(this.memberForm, this.formErrors, this.validationMessages));
  }

  // tslint:disable-next-line:no-shadowed-variable
  onbirthdayDateChange(value) {
    this.registerMember.BirthDate = value.date.year + '-' + value.date.month + '-' + value.date.day;
    // const bDate = new Date(this.selectedMember.BirthDate);
    this.memberBirthdate = new Date(value.date.year + '-' + value.date.month + '-' + value.date.day);
    let age = this.basicInfoService.getMemberAge(this.memberBirthdate);
    if (isNaN(age)) {
      age = null;
    } else if (age < 0) {
      age = 0
    }
    this.memberForm.patchValue({ Age: age })
    // this.selectedMember.BirthDate = value.date.year + '-' + value.date.month + '-' + value.date.day;
  }

  // savePostalData() {
  //   const postalData = this.postalDataForm.value;
  //   this.memberService.savePostalArea(postalData).pipe(takeUntil(this.destroy$)).subscribe(
  //     result => {
  //       if (result) {
  //         if (result.Data > 0) {
  //           this.memberForm.patchValue({ PostPlace: postalData.postalCode });
  //           this.memberForm.patchValue(postalData.postalName);
  //           this.postalDataView.close();
  //         }
  //       }
  //     }
  //   )
  // }

  saveCountryData() {
    const countryData = this.countryDataForm.value;
    this.memberService.addCountryDetails(countryData).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.countries.push({ Id: countryData.Id, Name: countryData.Name, CountryCode: countryData.CountryCode })
          const selectedCountry = this.countries.filter(x => x.Id === countryData.Id);
          if (selectedCountry.length > 0) {
            this.getSelectedCountry(selectedCountry[0].Name, false);
            this.basicInfoService.setCountries(this.countries);
          }
          this.countryDataView.close();
        }
      });
  }


  setGender(genderType: number) {
    this.genderSelected = true;
    this.registerMember.Gender = genderType;
    this.memberForm.patchValue({
      Gender: genderType
    })
  }

  emailBlur(val: any, type: string) {
    // Do nothing if emailField is empty
    if (val !== '') {
      switch (type) {
        case 'EMAIL':
          this.isEmailBlur = true;
          break;
        case 'COMEMAIL':
          this.isCompanyEmailBlur = true;
          break;
        case 'ACCEMAIL':
          this.isAccountEmailBlur = true;
          break;
      }
    }
  }

  emailFocus(type: string) {
    switch (type) {
      case 'EMAIL':
        this.isEmailBlur = false;
        break;
      case 'COMEMAIL':
        this.isCompanyEmailBlur = false;
        break;
      case 'ACCEMAIL':
        this.isAccountEmailBlur = false;
        break;
    }
  }

  memberCardBlur(val: any) {
    if (val === '') {
      this.isMemberCardBlur = false;
    } else {
      this.isMemberCardBlur = true;
      if (!this.memberForm.controls['MemberCardNo'].invalid) {
        this.removeleadingzeroes();
        // if (this.memberForm.controls['Email'].valid) { // email is exits check after valid email
        this.memberService.validateMemberCard(val, -1).pipe(takeUntil(this.destroy$)).subscribe(
          result => {
            if (result) {
              if (!result.Data) {
                this.memberForm.controls['MemberCardNo'].setErrors({
                  'memberCardTaken': true
                });
              } else {
              }
            }
          }
        )
      } else {
        this.memberForm.controls['MemberCardNo'].setErrors({
          'pattern': true
        });
      }
    }
  }

  guestCardBlur(val: any) {
    if (val === '') {
      this.isGuestCardBlur = false;
    } else {
      this.isGuestCardBlur = true;
      if (this.memberForm.controls['GuestCardNo'].invalid) {
        this.memberForm.controls['GuestCardNo'].setErrors({
          'pattern': true
        });
      }
    }
  }

  removeleadingzeroes() {
    const k = (this.memberForm.get('MemberCardNo').value);
    let p: string;
   for (let i = 0; i < k.length; i++) {
    if (k[i] !== '0') {
      p = k.slice(i, k.length);
      i = k.length;
    }
   }
   this.memberForm.patchValue({MemberCardNo: p});
  }

  followUpChange(e) {
    this.isSaveFollowup = e.target.checked;
    this.followUpProfile.setValue(false)
    this.followUpProfile.setDisabledField(e.target.checked);
  }

  memberCardFocus() {
    this.isMemberCardBlur = false
  }

  onStep1Next() {
    this.currentState = this.currentState + 1;
    if (this.steupValidation(1)) {
      this.myFormSubmited = true;
      this.memberWizard.nextStepDisabled = true;
      if (!this.memberForm.valid) {
        UsErrorService.validateAllFormFields(this.memberForm, this.formErrors, this.validationMessages);
      }
    } else {
      this.wizardNumber = 1;
      this.myFormSubmited = true;
      this.memberWizard.nextStepDisabled = false;
    }
  }

  steupValidation(steup: number) {
    switch (steup) {
      case 1:
        return (this.memberForm.controls['FirstName'].invalid || this.memberForm.controls['LastName'].invalid || this.memberForm.controls['Email'].invalid
          || this.memberForm.controls['Mobile'].invalid || this.memberForm.controls['PrivateTeleNo'].invalid || this.memberForm.controls['WorkTeleNo'].invalid
          || this.memberForm.controls['Address1'].invalid || this.memberForm.controls['PostCode'].invalid
          || this.memberForm.controls['Gender'].invalid || this.memberForm.controls['Age'].invalid || this.memberForm.controls['MemberCardNo'].invalid)
      case 2:
        return this.memberForm.controls['BankAccount'].invalid
    }
  }

  onStep2Back() {
    if (this.currentState > 0) {
      this.currentState = this.currentState - 1;
    }
  }

  onStep3Back() {
    if (this.currentState > 0) {
      this.currentState = this.currentState - 1;
    }
  }

  onStep4Back() {
    if (this.currentState > 0) {
      this.currentState = this.currentState - 1;
    }
  }

  onStep5Back() {
    if (this.currentState > 0) {
      this.currentState = this.currentState - 1;
    }
  }

  onStep2Next() {
    this.currentState = this.currentState + 1;
    if (this.steupValidation(2)) {
      this.myFormSubmited = true;
      this.memberWizard.nextStepDisabled = true;
      UsErrorService.validateAllFormFields(this.memberForm, this.formErrors, this.validationMessages);
    } else {
      this.memberWizard.nextStepDisabled = false;
    }
  }

  onStep3Next() {
    this.currentState = this.currentState + 1;
  }

  onStep4Next() {
    this.currentState = this.currentState + 1;
    this.followUpProfile.initView();
    this.followUpProfile.setMemberValue(this.memberForm.value);
  }

  cancelRegister() {
    this.canceled.emit()
  }

  importSIOMember(rowevent) {
    let ImportApproved, InfoImported, InvalidMember, NotSioMember;
    this.translateService.get('MEMBERSHIP.ImportApproved').pipe(takeUntil(this.destroy$)).subscribe(answer => ImportApproved = answer);
    this.translateService.get('MEMBERSHIP.InfoImported').pipe(takeUntil(this.destroy$)).subscribe(answer => InfoImported = answer);
    this.translateService.get('MEMBERSHIP.InvalidMember').pipe(takeUntil(this.destroy$)).subscribe(answer => InvalidMember = answer);
    this.translateService.get('MEMBERSHIP.NotSioMember').pipe(takeUntil(this.destroy$)).subscribe(answer => NotSioMember = answer);

    if (rowevent.row.item.Email.search('@siotemp.no')  > 0 ) {
    this.MSN.openMessageBox('ERROR',
    {
      messageTitle: InvalidMember,
      messageBody: NotSioMember
    });
    } else if (rowevent.row.item.Email.search('@bristemp.no') > 0) {
     this.MSN.openMessageBox('ERROR',
      {
        messageTitle: InvalidMember,
       messageBody: NotSioMember
     });
    }else {
      this.MSN.openMessageBox('SUCCSESS',
     {
       messageTitle: ImportApproved,
       messageBody: InfoImported
     });
      const date = new Date(rowevent.row.item.BirthDay);
      const age = this.basicInfoService.getMemberAge(new Date(rowevent.row.item.BirthDay.slice(0, 10)));

      this.memberForm.patchValue({
        Email: rowevent.row.item.Email,
        Mobile: rowevent.row.item.crMobile,
        BirthDate: {
                    date: {
                      year: date.getFullYear(),
                      month: date.getMonth() + 1,
                      day: date.getDate()}
                    },
        Age: age
      })
    }
  }

  trimName(name, event) {
    let patchName = event.target.value.trim();
    const list = patchName.split(' ');
    if (list.length === 1) {
      patchName = patchName.charAt(0).toUpperCase() + patchName.slice(1);
    } else if (list.length > 1) {
      let newPName = '';
      list.forEach(namepart => {
        newPName = newPName + namepart.charAt(0).toUpperCase() + namepart.slice(1) + ' ';
      });
      patchName = newPName.trim()
    }
    if (name === 'lastname') {
       this.memberForm.patchValue({'LastName': patchName})
    } else if (name === 'firstname') {
      this.memberForm.patchValue({'FirstName': patchName})
    }
  }

  openIntroducedBy(content) {
    this.introducedByView = this.modalService.open(content, { width: '1000' });
    this.entitySearchType = 'INTRODUCEDBY';
  }

  introducedBySelected(event) {
    this.addEntity(event.data)
    this.introducedByAdded = true;
    this.introducedByView.close();
  }

  removeIntroducer() {
    this.memberForm.patchValue({ IntroducedById: null });
    this.memberForm.patchValue({ IntroducedByCustId: null });
    this.memberForm.patchValue({ IntroducedByName: null });
    this.introducedByAdded = false;
  }

  removeGuardian() {
    this.memberForm.patchValue({ GuardianId: null })
    this.memberForm.patchValue({ GuardianCustId: null })
    this.memberForm.patchValue({ GuardianName: null })
    this.guardianName = '';
    this.isGuardianSelected = false;
  }

  openIntroducer(content) {
    this.newMemberRegisterView = this.modalService.open(content, {});
  }

  closeIntroducer() {
    this.newMemberRegisterView.close();
  }

  selectedIntroducedMembers(event, isList) {
    if (isList) {
      event.forEach(i => {
        this.introduceMemberList.push(i);
      })
    } else {
      this.introduceMemberList.push(event.data)
    }
    this.closeIntroducer();
  }

}

  //  addSession(command, objectvalue, key, user) {
  //           window.location.href = 'javascript:var server="localhost:60024";var maxreqlength=1500;var selectedText= "' + command + '" ' +
  //           ' if(selectedText){_speakText()}void 0;function _formatCommand(b,a) ' +
  //           '{return"http://"+server+"/"+b+"/dummy.gif"+a+"&timestamp="+new Date().getTime()+"&key="+"' + key + '|' + user + '|' + command + '"} ' +
  //           'function _speakText(){var a=new Image(1,1);a.onerror=function(){_showerror()}; ' +
  //           'a.src=_formatCommand("operation","?value="+"' + objectvalue + '")}function _bufferText(f) ' +
  //           '{var c="true";var b=Math.floor((f.length+maxreqlength-1)/maxreqlength);for(var d=0;d<b;d++) ' +
  //           '{var g=d*maxreqlength;var a=Math.min(f.length,g+maxreqlength);var e=new Image(1,1);e.onerror=function(){_showerror()}; ' +
  //           'e.src=_formatCommand("buffertext","?totalreqs="+b+"&req="+(d+1)+"&text="+f.substring(g,a)+"&clear="+c);c="false"}} ' +
  //           'function _showerror(){SessionError();}';
  //   }
