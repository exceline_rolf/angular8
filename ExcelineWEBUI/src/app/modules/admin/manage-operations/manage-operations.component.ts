import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { AdminService } from '../services/admin.service';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';
import { saveAs } from 'file-saver/FileSaver';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-manage-operations',
  templateUrl: './manage-operations.component.html',
  styleUrls: ['./manage-operations.component.scss']
})

export class ManageOperationsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  currentUser: any;
  branchId: any;

  constructor(
    private exceBreadcrumbService: ExceBreadcrumbService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.AccessC',
      url: '/adminstrator/manage-operations'
    });
  }

  ngOnInit() {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.currentUser = this.exceLoginService.CurrentUser.username;
  }

  download(filename, text) {
    text = '<?xml version="1.0" encoding="ISO-8859-1"?>' + text;
    const blob = new Blob([text], { type: 'text/xml'});
    saveAs(blob, filename);
  }

  getArxMembers() {
    this.adminService.getARXMembers(true, this.branchId).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        const text = result.Data
        this.download('ARXMembers.xml', text)
      }
    }, null, null);
  }

  getArxChanged() {
    this.adminService.getARXMembers(false, this.branchId).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        const text = result.Data
        this.download('ARXChanged.xml', text)
      }
    }, null, null);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
