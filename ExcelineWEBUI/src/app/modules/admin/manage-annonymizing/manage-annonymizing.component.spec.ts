import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageAnnonymizingComponent } from './manage-annonymizing.component';

describe('ManageAnnonymizingComponent', () => {
  let component: ManageAnnonymizingComponent;
  let fixture: ComponentFixture<ManageAnnonymizingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageAnnonymizingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageAnnonymizingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
