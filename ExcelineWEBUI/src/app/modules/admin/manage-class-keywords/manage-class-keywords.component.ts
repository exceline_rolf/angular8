import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { ExceAddmemberService } from 'app/shared/components/exce-addmember/exce-addmember.service';
import { AdminService } from '../services/admin.service';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import { takeUntil } from 'rxjs/operators';
import { take } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-manage-class-keywords',
  templateUrl: './manage-class-keywords.component.html',
  styleUrls: ['./manage-class-keywords.component.scss']
})
export class ManageClassKeywordsComponent implements OnInit, OnDestroy {
  newCategoryName = '';
  branchId: number;
  categoryType: any;
  keyWords = [];
  private destroy$ = new Subject<void>();

  constructor(
    private exceBreadcrumbService: ExceBreadcrumbService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.CLASSKEYWORD',
      url: '/adminstrator/manage-class-keywords'
    });
  }

  ngOnInit() {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    // get keyword categorytypeid
    this.adminService.getCategoryTypes(this.branchId, '').pipe(
      take(1),
      takeUntil(this.destroy$)
    ).subscribe(res => {
      res.Data.forEach(catType => {
        if (catType.Code === 'KEYWORD') {
          // Id, Code, Description, Name
          this.categoryType = catType;
        }
      });
    }, null, null);
    this.getKeywordCategories();
  }

  addCategory() {
    let code = '';

    if (this.newCategoryName.length < 1) {
      return;
    }

    for (let i = 0; i < this.newCategoryName.length; i++) {
      if (this.newCategoryName[i] !== ' ') {
        code = code + this.newCategoryName[i]
      }
    }
    code = code + Math.floor(Math.random() * 100000000);
    const category = {
      TypeId: this.categoryType.Id,
      BranchId: this.branchId,
      Code: code,
      Name: this.newCategoryName,
      CreatedUser: this.exceLoginService.CurrentUser.username,
      LastModifiedUser: this.exceLoginService.CurrentUser.username,
      ActiveStatus: true
    }
    this.adminService.saveCategory(category).pipe(
      takeUntil(this.destroy$)
    ).subscribe(feedback => {
      this.getKeywordCategories();
    }, null, null);
    this.newCategoryName = '';
  }

  deleteCategory(category) {
    this.adminService.deleteCategory(category).pipe(
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.getKeywordCategories();
    }, null, null);
  }

  getKeywordCategories() {
    this.adminService.getCategories('KEYWORD').pipe(
      takeUntil(this.destroy$)
    ).subscribe(categories => {
      this.keyWords = categories.Data;
    }, null, null);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
