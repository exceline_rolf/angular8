import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageClassKeywordsComponent } from './manage-class-keywords.component';

describe('ManageClassKeywordsComponent', () => {
  let component: ManageClassKeywordsComponent;
  let fixture: ComponentFixture<ManageClassKeywordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageClassKeywordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageClassKeywordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
