import { Injectable } from '@angular/core';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import { ConfigService } from '@ngx-config/core';
import { CommonHttpService } from 'app/shared/services/common-http.service';
import { Observable } from 'rxjs';

@Injectable()
export class NotificationService {

  private notificationServiceUrl: string;
  private branchId: number;

  constructor(
    private commonHttpService: CommonHttpService,
    private config: ConfigService,
    private exceLoginService: ExceLoginService) {
    this.notificationServiceUrl = this.config.getSettings('EXCE_API.NOTIFICATION');
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  GetChangeLogNotificationList(params: any): Observable<any> {
    const url = this.notificationServiceUrl + 'GetChangeLogNotificationList?branchId=' + this.branchId
      + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&notifyMethod=' + params.notifyMethod;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetAccessDeniedList(params: any): Observable<any> {
    const url = this.notificationServiceUrl + 'Notification/GetAccessDeniedList?fromDate=' + params.fromDate +
      '&toDate=' + params.toDate + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetNotificationByIdAction(notificationId: any): Observable<any> {
    const url = this.notificationServiceUrl + 'GetNotificationByIdAction?notificationId=' + notificationId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetNotificationSummaryList(branchId: number, status: any): Observable<any> {
    const url = this.notificationServiceUrl + 'GetNotificationSummaryList?branchId=' + branchId + '&status=' + status

    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }


  GetNotificationSearchResult(searchN: any): Observable<any> {
    const url = this.notificationServiceUrl + 'GetNotificationSearchResult'

    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: searchN
    })
  }


  GetEntityList(): Observable<any> {
    const url = this.notificationServiceUrl + 'GetEntityList'

    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetCategoriesByType(branchId: number, type: string, code): Observable<any> {
    const url = this.notificationServiceUrl + 'GetCategoriesByType?type=' + type + '&branchId=' + branchId + '&code=' + code

    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }


  GetTemplateTextKeyByEntity(entity: number, type: string): Observable<any> {
    const url = this.notificationServiceUrl + 'GetTemplateTextKeyByEntity?entity=' + entity + '&type=' + type

    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetNotificationTemplateText(methodCode: string, typeCode: string): Observable<any> {
    const url = this.notificationServiceUrl + 'GetNotificationTemplateText?methodCode=' + methodCode + '&typeCode=' + typeCode

    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }



  SaveNotificationTextTemplate(SaveNotificationTemplateText: any): Observable<any> {
    const url = this.notificationServiceUrl + 'SaveNotificationTextTemplate'

    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: SaveNotificationTemplateText
    })
  }
}
