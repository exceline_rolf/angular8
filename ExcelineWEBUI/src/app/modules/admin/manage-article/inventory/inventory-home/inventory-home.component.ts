import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { AdminService } from '../../../services/admin.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PreloaderService } from '../../../../../shared/services/preloader.service';
import { UsErrorService } from '../../../../../shared/directives/us-error/us-error.service';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { ExceLoginService } from '../../../../login/exce-login/exce-login.service';
import { ExceBreadcrumbService } from '../../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { ExceToolbarService } from '../../../../common/exce-toolbar/exce-toolbar.service';

@Component({
  selector: 'app-inventory-home',
  templateUrl: './inventory-home.component.html',
  styleUrls: ['./inventory-home.component.scss']
})
export class InventoryHomeComponent implements OnInit, OnDestroy {

  locale: string;
  addCountingList: any;
  inventoryList: any[];
  private inventoryItems = [];
  itemCount = 0;
  private itemResource?: DataTableResource<any>;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  inventoryForm: FormGroup;
  formSubmited = false;
  private addEditMode = 'ADD';
  isDisableAppDate = false;
  yesHandlerSubscription: any;
  branchId: number;
  isApproved = true;

  formErrors = {
    'Name': ''
  };
  validationMessages = {
    'Name': {
      'required': 'ADMIN.PleaseSpecify'
    }
  };

  constructor(
    private toolbarService: ExceToolbarService,
    private modalService: UsbModal,
    private adminService: AdminService,
    private fb: FormBuilder,
    private exceMessageService: ExceMessageService,
    private router: Router,
    private translate: TranslateService,
    private loginservice: ExceLoginService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.CountingListC',
      url: '/adminstrator/inventory-home'
    });
  }

  ngOnInit() {
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );

    this.branchId = this.loginservice.SelectedBranch.BranchId;
    this.inventoryForm = this.fb.group({
      'Id': [],
      'Name': [null, Validators.required],
      'StartDate': [],
      'ApprovedDate': [null],
      'NetValue': [{ value: null, disabled: true }]
    });
    this.adminService.GetInventory().subscribe(
      res => {
        this.inventoryList = res.Data;
        this.inventoryItems = this.inventoryList;
        this.itemResource = new DataTableResource(this.inventoryList);
        this.itemResource.count().then(count => this.itemCount = count);
        this.reloadInventoryList({ offset: 0, limit: 50 });
        this.approveCheck();
      }, err => {
        console.log(err);
      });

    this.inventoryForm.statusChanges.subscribe(_ => {
      UsErrorService.onValueChanged(this.inventoryForm, this.formErrors, this.validationMessages)
    });
    this.yesHandlerSubscription = this.exceMessageService.yesCalledHandle.subscribe((value) => {
      if (value.id === 'DELETE_INVENTORY') {
        this.deleteInventoryConfirm(value.optionalData);
      }
    });
  }

  approveCheck() {
    for (var i in this.inventoryList) {
      if (!this.inventoryList[i].ApprovedDate) {
        this.isApproved = false
      } else {
        this.isApproved = true
      }
    }
  }

  deleteInventoryConfirm(inventoryId) {
    PreloaderService.showPreLoader();
    this.adminService.DeleteInventory(inventoryId).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {
          const indx = this.inventoryList.findIndex(x => x.Id === inventoryId);
          if (indx !== -1) {
            this.inventoryList.splice(indx, 1);
          }
          this.inventoryItems = this.inventoryList;
          this.itemResource = new DataTableResource(this.inventoryList);
          this.reloadInventoryList(this.inventoryList);
        }
      },
      err => {
        PreloaderService.hidePreLoader();
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorDeletingInventory' });
      });
  }

  ngOnDestroy() {
    if (this.yesHandlerSubscription) {
      this.yesHandlerSubscription.unsubscribe();
    }
    if (this.addCountingList) {
      this.addCountingList.close();
    }
  }

  reloadInventoryList(params) {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.inventoryList = items);
    }
    this.approveCheck();
  }
  filterInventory(nameValue?: any) {
    let filteredInventorybyname = this.inventoryItems;
    filteredInventorybyname = this.filterpipe.transform('Name', 'NAMEFILTER', filteredInventorybyname, nameValue);
    this.itemResource = new DataTableResource(filteredInventorybyname);
    this.reloadInventoryList(filteredInventorybyname);
  }
  openNewCountingList(content: any) {

    this.inventoryForm.reset();
    this.isDisableAppDate = false;
    let currentDate = new Date();
    let stDate = { date: { year: currentDate.getFullYear(), month: currentDate.getMonth() + 1, day: currentDate.getDate() } }
    this.inventoryForm.patchValue({
      StartDate: stDate
    })
    this.addCountingList = this.modalService.open(content, { width: '400' });
  }

  editInventory(content: any, item: any) {
    const appDate = this.getDisplayDate(item.ApprovedDate);
    if (appDate !== null) {
      this.isDisableAppDate = true;
    } else {
      this.isDisableAppDate = false;
    }
    this.inventoryForm.patchValue({
      Id: item.Id,
      Name: item.Name,
      NetValue: item.NetValue,
      StartDate: this.getDisplayDate(item.StartDate),
      ApprovedDate: appDate
    })
    this.addCountingList = this.modalService.open(content, { width: '400' });
  }

  getDisplayDate(val) {
    val = new Date(val);
    let result;
    if ((val.getFullYear() === 1970 || val.getFullYear() === 1900) && val.getMonth() === 0 &&
      val.getDate() === 1) {
      result = null;
    } else {
      result = {
        date: {
          year: val.getFullYear(),
          month: val.getMonth() + 1,
          day: val.getDate()
        }
      };
    }
    return result;
  }

  saveInventory(value) {
    if (value.Name == null || value.Name == '') {
      this.inventoryForm.controls['Name'].setErrors({ 'required': true });
    } else {
      this.inventoryForm.controls['Name'].setErrors(null);
    }

    this.formSubmited = true;

    if (this.inventoryForm.valid) {
      let inventory: any = {};
      inventory = Object.assign({}, value);
      inventory.BranchId = this.branchId;
      //  inventory = this.inventoryForm.getRawValue().Id;
      if (inventory.StartDate !== null) {
        //  const stDate = inventory.StartDate.date.year + '-' + inventory.StartDate.date.month + '-' + inventory.StartDate.date.day;
        inventory.StartDate = this.manipulatedate(inventory.StartDate); // moment(dueDate).format('YYYY-MM-DD').toString(); // stDate;
      }
      if (inventory.ApprovedDate !== null) {
        // const appDate = new Date(inventory.ApprovedDate.date.year + '-' + inventory.ApprovedDate.date.month + '-' + (inventory.ApprovedDate.date.day + 1));
        inventory.ApprovedDate = this.manipulatedate(inventory.ApprovedDate); // appDate;
      }

      PreloaderService.showPreLoader();

      this.adminService.SaveInventory(inventory).subscribe(
        res => {
          PreloaderService.hidePreLoader();
          if (res.Data > 0) {
            if (inventory.Id <= 0) {
              inventory.Id = res.Data;
              this.inventoryList.push(inventory);
              this.inventoryItems = this.inventoryList;
              this.reloadInventoryList(this.inventoryList);
            } else {
              const editItem = this.inventoryList.find(x => x.Id === res.Data);
              if (editItem) {
                editItem.Name = value.Name;
                editItem.StartDate = inventory.StartDate;
                if (value.ApprovedDate !== null) {
                  editItem.ApprovedDate = this.getDateConverter(value.ApprovedDate.date);
                }
              }
            }
            this.itemResource = new DataTableResource(this.inventoryList);
            this.reloadInventoryList(this.inventoryList);
            this.addCountingList.close()
          }
        },
        err => {
          PreloaderService.hidePreLoader();
          this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorSavingArticle' });
        });
    } else {
      UsErrorService.validateAllFormFields(this.inventoryForm, this.formErrors, this.validationMessages);
    }
  }

  // convert date js object in to string
  getDateConverter(datee: any) {
    const dd = (datee.day < 10 ? '0' : '') + datee.day;
    const MM = ((datee.month + 1) < 10 ? '0' : '') + datee.month;
    const yyyy = datee.year;
    return (dd + '/' + MM + '/' + yyyy);
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date != undefined) {
      let tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  GetFormatteredDate(date: any): any {
    if (date) {
      const selectedDate = new Date(date);
      let formatteredDate: any;
      formatteredDate = { year: selectedDate.getFullYear(), month: selectedDate.getMonth() + 1, day: selectedDate.getDate() }
      return formatteredDate;
    } else {
      return;
    }
  }

  deleteInventory(item) {
    let msgType = '';
    let msg = '';
    this.translate.get('COMMON.Confirm').subscribe(translatedValue => msgType = translatedValue);
    this.translate.get('ADMIN.ConfirmMessage').subscribe(translatedValue => msg = translatedValue);
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: msgType,
        messageBody: msg,
        msgBoxId: 'DELETE_INVENTORY',
        optionalData: item.Id
      }
    );
  }

  inventoryDetails(inventory) {
    this.router.navigate(['adminstrator/inventory-home/inventory-detail/' + inventory.Id]);
  }
}
