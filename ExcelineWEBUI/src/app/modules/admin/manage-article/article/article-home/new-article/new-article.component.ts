import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { UsbModal } from '../../../../../../shared//components/us-modal/us-modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminService } from '../../../../services/admin.service';
import { DataTableResource } from '../../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { ExceLoginService } from '../../../../../login/exce-login/exce-login.service';
import { PreloaderService } from '../../../../../../shared/services/preloader.service';
import { ExceMessageService } from '../../../../../../shared/components/exce-message/exce-message.service';
import { ColumnDataType } from '../../../../../../shared/enums/us-enum';
import { UsErrorService } from '../../../../../../shared/directives/us-error/us-error.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-new-article',
  templateUrl: './new-article.component.html',
  styleUrls: ['./new-article.component.scss']
})
export class NewArticleComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private modelRefUnit: any;
  private modelRefvendor: any;
  private modelRefrevAcc: any;
  private modelRefvCode: any;
  private revenueList: any;
  private vatCodeItems = [];
  private revenueItemsSource: DataTableResource<any[]>;
  private vatCodeItemResource: any;
  private selectedRevAccount: any;
  private categoryTypeCode: string;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private categoryRegisterView: any;
  private branchId: number;
  private loginUseRoleId: number;
  private columns: any;
  private itemCount: number;
  private addMemberView: any;
  private isStockItem = false;
  private stockLevel: number;
  private reOrderLevel: number;

  model: void;
  selectedAccount: any;
  selectedVatCode: any;
  articleType = 'SERVICE';
  articleForm: FormGroup;
  unitCategoryList: any;
  selectedUnityCategory: any;
  categoryList: any;
  selectedCategory: any;
  acrivityList: any;
  selectedActivity: any;
  @Input() Mode: string;
  @Input() serviceCategoryList: any;
  @Input() itemCategoryList: any;
  @Input() activityCategoryList: any;
  @Input() selectedArticle: any;
  @Input() ArticleType: any;
  @Output() savedCategoryEvent = new EventEmitter();
  @Output() savedArticleEvent = new EventEmitter();
  @Output() closeArticleEvent = new EventEmitter();
  public selectRevAccountForm: FormGroup;
  revenueItems = [];
  vatCodeItemCount: number;
  branches = [];
  branchCount = 0;
  limit = 20;
  items = [];
  formSubmited = false;
  accountName: any;
  accountNo: any;
  auotocompleteItems = [{ id: 'NAME', name: 'NAMEE :', isNumber: false }, { id: 'ID', name: 'Idd :', isNumber: true }]
  public selected: any;
  public isbookingselected = false;

  formErrors = {
    'Description': '',
    'CategoryId': '',
    'VatCodeName': '',
    'RevenueAccountName': ''

  };
  validationMessages = {
    'Description': {
      'required': 'ADMIN.PleaseSpecify'
    },
    'CategoryId': {
      'required': 'ADMIN.PleaseSpecify'
    },
    'VatCodeName': {
      'required': 'ADMIN.VatReq'
    },
    'RevenueAccountName': {
      'required': 'ADMIN.Required'
    }
  };

  constructor(
    private translateService: TranslateService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private adminService: AdminService,
    private loginservice: ExceLoginService,
    private exceMessageService: ExceMessageService
  ) { }

  ngOnInit() {
    this.articleForm = this.fb.group({
      'Id': [],
      'ArticleNo': [],
      'ArticleSettingId': [],
      'SortCutKey': [],
      'Description': [null, Validators.required],
      'BarCode': [],
      'UnitId': [],
      'ActivityId': [],
      'CategoryId': [null, Validators.required],
      'RevenueAccount': [],
      'RevenueAccountId': [],
      'RevenueAccountNo': [],
      'RevenueAccountName': [null, Validators.required],
      'DefaultPrice': [],
      'PurchasePriceWithoutVat': [],
      'PurchasedPrice': [{ value: null, disabled: true }],
      'EmployeePrice': [],
      'Category': [],
      'NoOfMinutes': [],
      'VatCodeId': [],
      'VatCodeName': [null, Validators.required],
      'VatRate': [],
      'VatCode': [],
      'IsPunchCard': [],
      'IsContractBooking': [],
      'BranchIdList': [],
      'VenderId': [],
      'VendorName': [],
      'StockStatus': [],
      'ObsoleteStatus': [],
      'IsVoucher': [],
      'StockLevel': [],
      'ReOrderLevel': []
    });

    if (this.articleForm.getRawValue().CategoryId == 167) {
      this.isbookingselected = true;
    } else {
      this.isbookingselected = false;
    }

    this.selectRevAccountForm = this.fb.group({
      'AccountNo': [null, Validators.required],
      'AccountName': [null, Validators.required],
      'Id': [null]
    });


    this.categoryList = this.serviceCategoryList;
    this.acrivityList = this.activityCategoryList;
    this.branchId = this.loginservice.SelectedBranch.BranchId;
    this.loginUseRoleId = this.loginservice.LoggedUserDetails.RoleId;
    this.adminService.getCategories('UNIT').pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      res => {
        this.unitCategoryList = res.Data;
      }, err => {
        console.log(err);
      });

    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
    ).subscribe(branch => {
      if (branch.Data) {
        this.branches = branch.Data;
        this.branchCount = this.branches.length;
        if (this.Mode === 'ADD') {
          const loginBranch = this.branches.find(x => x.Id === this.branchId);
          loginBranch.IsCheck = true;
          this.branches.forEach(b => {
            if (this.loginUseRoleId === 1) {
              b.IsEnabled = true;
            } else {
              b.IsEnabled = false;
            }
          });
        } else {
          this.branches.forEach(b => {
            if (this.loginUseRoleId === 1) {
              b.IsEnabled = true;
            } else {
              if (b.Id !== this.branchId) {
                b.IsEnabled = false;
              } else {
                b.IsEnabled = true;
              }
            }
          });
          this.selectedArticle.BranchIdList.forEach(i => {
            this.branches.forEach(b => {
              if (b.Id === i) {
                b.IsCheck = true;
              }
            });
          });
        }
      }
    });

    this.adminService.getCategories('UNIT').pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      resItem => {
        this.unitCategoryList = resItem.Data;
      }, err => {
        console.log(err);
      });

    if (this.Mode === 'EDIT') {
      this.articleType = this.selectedArticle.ArticleType;

      if (this.articleType === 'SERVICE') {
        this.categoryList = this.serviceCategoryList;
      } else if (this.articleType === 'ITEM') {
        this.categoryList = this.itemCategoryList;
      }

      this.patchArticleDetails(this.selectedArticle);
    }

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((val) => {
      if (val.id === 'SAVE_ARTICLE') {
        this.saveArticleConfirm(val.optionalData);
      } else if (val.id === 'DELETE_ACCOUNT') {
        this.removeRevenueAccount();
      }
    });

    this.articleForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => {
      UsErrorService.onValueChanged(this.articleForm, this.formErrors, this.validationMessages)
    });

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
      if (value.id === 'DELETE_ARTICLE') {
        this.deleteArticleConfirm(value.optionalData);
      }
    });
  }

  deleteArticleConfirm(articleId) {
    const delObj: any = {};
    delObj.ArticleId = articleId;
    delObj.BranchId = this.branchId;
    delObj.IsAdminUser = this.loginUseRoleId === 1;
    PreloaderService.showPreLoader();
    this.adminService.deleteArticle(delObj).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        this.cancelArticle();
      },
      err => {
        PreloaderService.hidePreLoader();
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorDeletingArticle' });
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();

    if (this.addMemberView) {
      this.addMemberView.close();
    }
    if (this.categoryRegisterView) {
      this.categoryRegisterView.close();
    }
    if (this.modelRefvendor) {
      this.modelRefvendor.close();
    }
    if (this.modelRefUnit) {
      this.modelRefUnit.close();
    }
  }

  patchArticleDetails(article: any) {
    this.articleForm.patchValue({
      Id: article.Id,
      ArticleNo: article.ArticleNo,
      ArticleSettingId: article.ArticleSettingId,
      SortCutKey: article.SortCutKey,
      Description: article.Description,
      BarCode: article.BarCode,
      UnitId: article.UnitId,
      ActivityId: article.ActivityId,
      CategoryId: article.CategoryId,
      RevenueAccount: article.RevenueAccount,
      RevenueAccountId: article.RevenueAccountId,
      RevenueAccountNo: article.RevenueAccountNo,
      RevenueAccountName: article.RevenueAccountName,
      DefaultPrice: article.DefaultPrice,
      PurchasePriceWithoutVat: article.PurchasePriceWithoutVat,
      PurchasedPrice: article.PurchasedPrice,
      EmployeePrice: article.EmployeePrice,
      Category: article.Category,
      NoOfMinutes: article.NoOfMinutes,
      VatCodeId: article.VatCodeID,
      VatCodeName: article.VatCodeName,
      VatRate: article.VatRate,
      VatCode: article.VatCode,
      IsPunchCard: article.IsPunchCard,
      IsContractBooking: article.IsContractBooking,
      BranchIdList: article.BranchIdList,
      VenderId: article.VenderId,
      VendorName: article.VendorName,
      StockStatus: article.StockStatus,
      ObsoleteStatus: article.ObsoleteStatus,
      IsVoucher: article.IsVoucher,
      StockLevel: (article.StockLevel === '') ? 0 : article.StockLevel,
      ReOrderLevel: article.ReOrderLevel
    });
  }

  saveArticle(val) {
    this.formSubmited = true;
    if (this.articleForm.valid) {
      let article: any = {};
      article = val;
      article.BranchIdList = [];
      if (this.loginUseRoleId === 1) {
        article.IsAdminUser = true;
      } else {
        article.IsAdminUser = false;
      }
      this.branches.forEach(x => {
        if (x.IsCheck) {
          article.BranchIdList.push(x.Id);
        }
      });

      article.ActiveStatus = true;
      article.IsSave = true;
      article.IsUpdate = false;
      article.BranchId = this.branchId;
      article.ArticleType = this.articleType;
      if (this.Mode === 'ADD') {
        article.IsNotSystemArticle = true;
      }
      this.saveProcess(article);
    } else {
      UsErrorService.validateAllFormFields(this.articleForm, this.formErrors, this.validationMessages);
    }
  }

  updateArticle(val) {
    this.formSubmited = true;
    if (this.articleForm.valid) {
      let article: any = {};
      article = val;
      article.ArticleType = this.articleType;
      article.IsSave = false;
      article.IsUpdate = true;
      if (this.loginUseRoleId === 1) {
        article.IsAdminUser = true;
      } else {
        article.IsAdminUser = false;
      }
      article.ActiveStatus = true;
      article.BranchId = this.branchId;
      // let brIdList: any = [];
      // this.branches.filter(x => x.IsCheck).forEach(x => brIdList.push(x.Id))

      // if (brIdList.Except(this.selectedArticle.BranchIdList.any())) {
      //    USMessage.Show(USGMSAdminResources.MsgNewGymWithSave, MessageTypes.WARNING);
      //   const removeBranchList = this.selectedArticle.BranchIdList.ToList().Except(brIdList).ToList();
      //   removeBranchList.forEach(b => {
      //     let removedBranch = this.branches.find(x => x.Id === b);
      //     if (removedBranch) {
      //       removedBranch.IsCheck = true;
      //     }
      //   });
      //   return;
      // }
      article.BranchIdList = [];
      this.branches.forEach(x => {
        if (x.IsCheck) {
          article.BranchIdList.push(x.Id);
        }
      })

      if (this.loginUseRoleId === 1) {
        const confirmMessage = this.getConfirmMessage(article);
        if (confirmMessage !== '') {
          let messageTitle = '';

          (this.translateService.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
          this.exceMessageService.openMessageBox('CONFIRM',
            {
              messageTitle: messageTitle,
              messageBody: confirmMessage,
              msgBoxId: 'SAVE_ARTICLE',
              optionalData: article
            });
        } else {
          this.saveProcess(article);
        }
      }
    } else {
      UsErrorService.validateAllFormFields(this.articleForm, this.formErrors, this.validationMessages);
    }
  }

  saveArticleConfirm(article) {
    this.saveProcess(article);
  }

  saveProcess(article) {
    PreloaderService.showPreLoader();
    this.adminService.SaveArticle(article).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {
          this.savedArticleEvent.emit(res.Data);
        }
        this.formSubmited = false;
      },
      err => {
        PreloaderService.hidePreLoader();
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorSavingArticle' });
      });
  }

  getConfirmMessage(article) {
    // let defaultPriceC: string;
    // let employeePriceC: string;
    // let stockItem: string;
    // let notStockItem: string;
    // let obsoleteC: string;
    // let notObsolete: string;
    // let updateAllGym: string;


    let message = 'Default Price : ' + article.DefaultPrice + '\n';
    message = message + '\n' + 'Employee Price : ' + article.EmployeePrice;

    if (article.ArticleType === 'ITEM') {
      if (article.StockStatus) {
        message = message + 'stock item' + '\n';
      } else {
        message = message + 'not Stock item' + '\n';
      }
      if (article.ObsoleteStatus) {
        message = message + 'obsolete' + '\n';
      } else {
        message = message + 'not Obsolete' + '\n';
      }
    }
    if (message !== '') {
      message = message + '\n' + 'Do you want to update in all selected gym ?';
    }
    return message;
  }

  changeArticleType(type) {
    this.articleType = type;
    if (type === 'SERVICE') {
      this.categoryList = this.serviceCategoryList;
    } else if (type === 'ITEM') {
      this.categoryList = this.itemCategoryList;
    }
  }

  revenueAccountOpen(content) {
    this.adminService.GetRevenueAccounts().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.revenueList = result.Data;
        this.revenueItems = this.revenueList;
      } else {
      }
    },
      error => {
      });
    this.modelRefrevAcc = this.modalService.open(content, { width: '1000' });
  }

  reloadRevAccItems(params) {
    if (this.revenueItemsSource) {
      this.revenueItemsSource.query(params).then(items => this.revenueItems = items);
    }
  }

  rowClickAccount(rowEvent: any, clickType: any) {
    this.selectedRevAccount = rowEvent.row.item;
  }

  rowDoubleClickAccount(rowEvent: any) {
    this.selectedRevAccount = rowEvent.row.item;
    this.revAccountSelected();
  }

  editRevenueAccount(account: any) {
    this.selectedRevAccount = account;
    this.selectRevAccountForm.patchValue({
      AccountNo: this.selectedRevAccount.AccountNo,
      AccountName: this.selectedRevAccount.Name,
      Id: this.selectedRevAccount.ID,
    });
  }

  filterInput(accNameValue?: any, accNoValue?: any) {
    let filteredAccountsbyName = this.revenueList;
    filteredAccountsbyName = this.filterpipe.transform('Name', 'NAMEFILTER', filteredAccountsbyName, accNameValue);
    let filteredAccounts = filteredAccountsbyName;
    filteredAccounts = this.filterpipe.transform('AccountNo', 'NAMEFILTER', filteredAccounts, accNoValue);
    this.revenueItemsSource = new DataTableResource(filteredAccounts);
    this.reloadRevAccItems(filteredAccounts);
  }

  revAccountSelected() {
    if (this.selectedRevAccount) {
      this.articleForm.patchValue({
        RevenueAccount: this.selectedRevAccount,
        RevenueAccountNo: this.selectedRevAccount.AccountNo,
        RevenueAccountName: this.selectedRevAccount.Name,
        RevenueAccountId: this.selectedRevAccount.ID
      });
      this.modelRefrevAcc.close();
    }
  }

  selectRevenueAccount(item) {
    this.selectedRevAccount = item;
  }

  selectRevenueAccount2(event) {
    this.selectedRevAccount = event.row.item;
    this.revAccountSelected()
  }

  cancelUpdateRevAccount() {
    this.selectRevAccountForm.reset();
  }

  unitOpen(Content) {
    this.modelRefUnit = this.modalService.open(Content, { width: '900' });
  }

  vendorNameOpen(Content) {
    this.columns = [{ property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'CustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'Address1', header: 'Address', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'Mobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ]
    this.modelRefvendor = this.modalService.open(Content);
  }

  closeModal() {
    this.modelRefvendor.close();
  }

  vatCodeOpen(Content) {
    this.adminService.GetGymCompanySettings('VAT').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.vatCodeItemResource = new DataTableResource(result.Data);
        this.vatCodeItemResource.count().then(count => this.vatCodeItemCount = count);
        // this.reloadVatCodeList({ offset: 0, limit: 20 })
        this.modelRefvCode = this.modalService.open(Content, { width: '1024' });
      }
    });
  }

  reloadVatCodeList(params) {
    if (this.vatCodeItemResource) {
      this.vatCodeItemResource.query(params).then(items => this.vatCodeItems = items);
    }
  }

  vatCodeDoubleClick(rowEvent) {
    const purchasePriceWithoutVat = this.articleForm.getRawValue().PurchasePriceWithoutVat;
    this.articleForm.patchValue({
      VatCode: rowEvent.row.item.VatCode,
      VatRate: rowEvent.row.item.Rate,
      VatCodeName: rowEvent.row.item.Name,
      VatCodeId: rowEvent.row.item.Id
    });
    if (purchasePriceWithoutVat > 0) {
      const purchasedPrice = purchasePriceWithoutVat * ((rowEvent.row.item.Rate + 100) / 100);
      this.articleForm.patchValue({ PurchasedPrice: purchasedPrice });
    }
    this.modelRefvCode.close();
  }

  vatCoderowClick(event) {
    this.selectedVatCode = event.row.item;
  }

  selectVatCode() {
    const purchasePriceWithoutVat = this.articleForm.getRawValue().PurchasePriceWithoutVat;
    this.articleForm.patchValue({
      VatCode: this.selectedVatCode.VatCode,
      VatRate: this.selectedVatCode.Rate,
      VatCodeName: this.selectedVatCode.Name,
      VatCodeId: this.selectedVatCode.Id
    });
    if (purchasePriceWithoutVat > 0) {
      const purchasedPrice = purchasePriceWithoutVat * ((this.selectedVatCode.Rate + 100) / 100);
      this.articleForm.patchValue({ PurchasedPrice: purchasedPrice });
    }
    this.modelRefvCode.close();
  }

  openCategoryModal(categoryType: any, content: any) {
    this.categoryTypeCode = categoryType;
    this.categoryRegisterView = this.modalService.open(content, { width: '800' });
  }

  closeView() {
    this.categoryRegisterView.close();
  }

  saveCategory(category) {
    switch (this.categoryTypeCode) {
      case 'UNIT':
        this.unitCategoryList.push(category);
        this.articleForm.patchValue({ 'UnitId': category.Id });
        break;
      case 'SERVICE':
        // this.serviceCategoryList.push(category);
        // this.categoryList = this.serviceCategoryList;
        // this.articleForm.patchValue({ 'CategoryId': category.Id });
        this.savedCategoryEvent.emit({ category: category, type: this.categoryTypeCode })
        break;
      case 'ITEM':
        // this.itemCategoryList.push(category);
        // this.categoryList = this.itemCategoryList;
        // this.articleForm.patchValue({ 'CategoryId': category.Id });
        this.savedCategoryEvent.emit({ category: category, type: this.categoryTypeCode })
        break;
    }
    this.categoryRegisterView.close();
  }

  searchDeatail(val: any) {
    PreloaderService.showPreLoader();
    this.adminService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
      'SEARCH', val.roleSelected.id, val.hit, false,
      false, ''
    ).pipe(takeUntil(this.destroy$)).subscribe
      (result => {
        if (result) {
          PreloaderService.hidePreLoader();

          const venderId = this.articleForm.getRawValue().VenderId;
          const memIndex: number = result.Data.findIndex(item => item.Id === venderId);
          if (memIndex !== -1) {
            result.Data.splice(memIndex, 1);
          }
          this.items = result.Data;
          this.itemCount = result.Data.length;
        } else {
          PreloaderService.hidePreLoader();
        }
      });
  }

  selectedRowItem(item) {
    this.articleForm.patchValue({ VenderId: item.Id })
    this.articleForm.patchValue({ VendorName: item.Name })
    this.modelRefvendor.close();
  }

  addMemberModal(content: any) {
    this.addMemberView = this.modalService.open(content, { width: '900' });
    this.modelRefvendor.close();
  }

  addEntity(item: any) {
    this.articleForm.patchValue({ VenderId: item.Id })
    this.articleForm.patchValue({ VendorName: item.CompanyName })
    this.addMemberView.close();
  }

  stockItemChange(e) {
    const articleSettingid = this.articleForm.getRawValue().articleSettingid;
    if (articleSettingid <= 0) {
      if (e.target.checked) {
        this.isStockItem = true;
      } else {
        this.isStockItem = false;
        this.articleForm.patchValue({ StockLevel: 0 })
        this.articleForm.patchValue({ ReOrderLevel: 0 })
      }
    } else if (e.target.checked) {
      if (this.Mode === 'ADD') {
        this.isStockItem = true;
      }
      this.articleForm.patchValue({ StockLevel: this.stockLevel })
      this.articleForm.patchValue({ ReOrderLevel: this.reOrderLevel })
    } else {
      this.stockLevel = this.articleForm.getRawValue().StockLevel;
      this.reOrderLevel = this.articleForm.getRawValue().ReOrderLevel;
      this.articleForm.patchValue({ StockLevel: 0 })
      this.articleForm.patchValue({ ReOrderLevel: 0 })
      this.isStockItem = false;
    }
  }

  cancelArticle() {
    this.closeArticleEvent.emit();
  }

  closeRevAccountForm() {
    if (this.modelRefrevAcc) {
      this.modelRefrevAcc.close();
    }
  }

  addRevenueAccount() {
    if (this.selectRevAccountForm.valid) {
      const account: any = {
        Id: this.selectRevAccountForm.value.Id,
        AccountNo: this.selectRevAccountForm.value.AccountNo,
        Name: this.selectRevAccountForm.value.AccountName
      }

      this.adminService.addRevenueAccount(account).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        result => {
          if (result) {
            if (result.Data > 0) {
              this.adminService.GetRevenueAccounts().pipe(
                takeUntil(this.destroy$)
              ).subscribe(res => {
                if (res) {
                  this.revenueList = res.Data;
                  this.revenueItems = this.revenueList;
                  this.selectRevAccountForm.reset();
                } else {
                }
              },
                error => {
                });
            }
          }
        }
      );
    }
  }

  deleteRevenueAccount(account: any) {
    this.selectedAccount = account;
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'ADMIN.ConfirmMessage',
        msgBoxId: 'DELETE_ACCOUNT',
      });
  }

  removeRevenueAccount() {
    this.adminService.deleteRevenueAccount(this.selectedAccount).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          if (result.Data > 0) {
            const accountIndex = this.revenueItems.indexOf(this.selectedAccount);
            this.revenueItems.splice(accountIndex, 1);
          } else {
            this.exceMessageService.openMessageBox('WARNING',
              {
                messageTitle: 'Exceline',
                messageBody: 'ADMIN.AccountCannotDelete'
              });
          }
        }
      }
    );
  }

  deleteConfirmMsg() {
    // this.paymentId = rowEvent.PaymentArItemNo;
    let msgType = '';
    let msg = '';
    this.translateService.get('COMMON.Confirm').pipe(
      takeUntil(this.destroy$)
    ).subscribe(translatedValue => msgType = translatedValue);
    this.translateService.get('ADMIN.ConfirmMessage').pipe(
      takeUntil(this.destroy$)
    ).subscribe(translatedValue => msg = translatedValue);
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: msgType,
        messageBody: msg,
        msgBoxId: 'DELETE_ARTICLE',
        optionalData: this.selectedArticle.Id
      }
    );
  }


  categoryChange() {
    //console.log(this.articleForm.getRawValue().CategoryId)
    if (this.articleForm.getRawValue().CategoryId == 167) {
      this.isbookingselected = true;
    } else {
      this.isbookingselected = false;
    }
  }
}
