import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymEmployeeTimesComponent } from './gym-employee-times.component';

describe('GymEmployeeTimesComponent', () => {
  let component: GymEmployeeTimesComponent;
  let fixture: ComponentFixture<GymEmployeeTimesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymEmployeeTimesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymEmployeeTimesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
