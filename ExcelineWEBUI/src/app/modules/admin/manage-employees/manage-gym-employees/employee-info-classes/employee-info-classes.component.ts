
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AdminService } from '../../../services/admin.service'
import { EmployeeBasicInfoService } from '../employee-basic-info/employee-basic-info.service';
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { error } from 'util';
import { PreloaderService } from '../../../../../shared/services/preloader.service'
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-employee-info-classes',
  templateUrl: './employee-info-classes.component.html',
  styleUrls: ['./employee-info-classes.component.scss']
})

export class EmployeeInfoClassesComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId = 1;
  private itemResource: any = 0;
  private classItems = []
  private jobCategories: any
  private filteredEvents = []
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private currentEmployee: any;

  public items = [];
  public itemCount = 0;
  public branches = []
  public Resource: any;
  public rowTooltip: any;

  // @ViewChild('followUpProfile') public followUpProfile;
  // @ViewChild('updateMember') public updateMember;

  constructor(
    private employeeBasicInfoService: EmployeeBasicInfoService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;

      });
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
      ).subscribe(branch => {
      if (branch.Data) {
        this.branches = branch.Data;
      }
    })
  }

  ngOnInit() {
    // load the data to the table
    this.employeeBasicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
      ).subscribe((employee) => {
        this.currentEmployee = employee
        if (this.currentEmployee) {
          this.getClasses()
        }
      });
  }

getClasses() {
  PreloaderService.showPreLoader();
  this.adminService.GetEmployeeClasses(this.currentEmployee.Id).pipe(
    takeUntil(this.destroy$)
    ).subscribe(event => {
    if (event.Data) {
      this.classItems = event.Data
      this.itemResource = new DataTableResource(this.classItems);
      this.itemResource.count().then(count => this.itemCount = count);
      this.items = this.classItems;
      this.itemCount = Number(this.classItems.length);
      PreloaderService.hidePreLoader();
    } else {
      PreloaderService.hidePreLoader();
    }
  }, Error => {
    PreloaderService.hidePreLoader();
  }
  );
}

  filterInput(typeVal: string, approveVal: string, branchValue: any) {
    this.filteredEvents = this.classItems
    if (branchValue && branchValue !== 'ALL') {
      this.filteredEvents = this.filterpipe.transform('BranchName', 'NAMEFILTER', this.filteredEvents, branchValue.split('_')[1]);

    }

    if (typeVal && typeVal !== '') {
      this.filteredEvents = this.filterpipe.transform('ClassType', 'NAMEFILTER', this.filteredEvents, typeVal);
    }

    if (approveVal && approveVal !== 'ALL') {

      this.filteredEvents = this.filterpipe.transform('IsApproved', 'SELECT', this.filteredEvents, approveVal);

    }

    this.itemResource = new DataTableResource(this.filteredEvents);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredEvents);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  changeApprovalStatus(isApproved, EntityActiveTimeID) {
    this.adminService.UpdateApproveStatus({ 'IsApproved': isApproved, 'EntityActiveTimeID': EntityActiveTimeID }).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.getClasses()
      }
    },Error=>{
        window.alert('Error')
    }
  );
  }

ngOnDestroy(): void {
  this.destroy$.next();
  this.destroy$.complete();
}

}





