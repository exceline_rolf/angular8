

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AdminService } from '../../../services/admin.service'
import { EmployeeBasicInfoService } from '../employee-basic-info/employee-basic-info.service';
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { ExcelineMember } from 'app/modules/membership/models/ExcelineMember';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-employee-info-roles',
  templateUrl: './employee-info-roles.component.html',
  styleUrls: ['./employee-info-roles.component.scss']
})

export class EmployeeInfoRolesComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId = 1;
  private itemResource: any = 0;
  private roleItems = []
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private filteredRoles = []

  public branches = []
  public rowTooltip: any;
  public items = [];
  public itemCount = 0;

  // @ViewChild('followUpProfile') public followUpProfile;
  // @ViewChild('updateMember') public updateMember;

  constructor(
    private employeeBasicInfoService: EmployeeBasicInfoService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      });
    // this.adminService.getBranches(-1).pipe(takeUntil(this.destroy$)).subscribe(branch => {
    //   if (branch.Data) {
    //     this.branches = branch.Data;
    //   }
    // })

  }

  ngOnInit() {
    // load the data to the table
    let currentEmployee: ExcelineMember;
    this.employeeBasicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
      ).subscribe(employee => {currentEmployee = employee});
    this.roleItems = []
    this.adminService.getBranches(-1).pipe(takeUntil(this.destroy$)).subscribe(branchList => {
      if (branchList.Data) {
        this.branches = branchList.Data;
        this.roleItems = [];
      if (currentEmployee.ExceEmpRoleList) {
        currentEmployee.ExceEmpRoleList.forEach(empRole => {
          empRole.BranchList.forEach(element => {
            const branch = this.branches.filter(x => x.Id == element)[0]
            this.roleItems.push({ 'RoleId': empRole.RoleId, 'RoleName': empRole.RoleName, 'BranchName': branch.BranchName, 'BranchId': branch.Id })
        });
      });
      this.itemResource = new DataTableResource(this.roleItems);
      this.itemResource.count().then(count => this.itemCount = count);
      this.items = this.roleItems;
      this.itemCount = Number(this.roleItems.length);
      }
      }
    }, null, null);
  };

  filterInput(roleValue: any) {
    this.filteredRoles = this.roleItems
    if (roleValue && roleValue !== 'ALL') {
      this.filteredRoles = this.filterpipe.transform('BranchId', 'SELECT', this.filteredRoles, roleValue);
    }

    this.itemResource = new DataTableResource(this.filteredRoles);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredRoles);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}




