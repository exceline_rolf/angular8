import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeInfoRolesComponent } from './employee-info-roles.component';

describe('EmployeeInfoRolesComponent', () => {
  let component: EmployeeInfoRolesComponent;
  let fixture: ComponentFixture<EmployeeInfoRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeInfoRolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeInfoRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
