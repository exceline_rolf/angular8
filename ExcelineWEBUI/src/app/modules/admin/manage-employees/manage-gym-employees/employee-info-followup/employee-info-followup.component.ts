
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AdminService } from '../../../services/admin.service'
import { EmployeeBasicInfoService } from '../employee-basic-info/employee-basic-info.service';
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { error } from 'util';
import { PreloaderService } from '../../../../../shared/services/preloader.service'
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { takeUntil, concatMap, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
const now = new Date();

@Component({
  selector: 'app-employee-info-followup',
  templateUrl: './employee-info-followup.component.html',
  styleUrls: ['./employee-info-followup.component.scss']
})

export class EmployeeInfoFollowupComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId = 1;
  private itemResource: any = 0;
  private filteredFollowUps = []
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private followUpItems = []

  public items = [];
  public itemCount = 0;
  public followUpCategories: any
  selectedItem: any;
  public branches = []
  public rowTooltip: any;

  // @ViewChild('followUpProfile') public followUpProfile;
  // @ViewChild('updateMember') public updateMember;

  constructor(
    private employeeBasicInfoService: EmployeeBasicInfoService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private modalService: UsbModal
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;

      });
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
      ).subscribe(branch => {
      if (branch.Data) {
        this.branches = branch.Data;
      }
    })

    this.adminService.getTaskCategories(this.branchId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(category => {
      if (category.Data) {
        this.followUpCategories = category.Data;
      }
    })
  }

  ngOnInit() {
    // load the data to the table
    this.employeeBasicInfoService.currentMember.pipe(
      concatMap(currentEmployee => {
        PreloaderService.showPreLoader();
        return this.adminService.GetFollowUpDetailByEmpId(currentEmployee.Id)
      }),
      map(result => result.Data),
      takeUntil(this.destroy$)
    ).subscribe(employeeEvents => {
      this.followUpItems = employeeEvents;
      this.itemResource = new DataTableResource(this.followUpItems);
      this.itemResource.count().then(count => this.itemCount = count);
      this.items = this.followUpItems;
      this.itemCount = Number(this.followUpItems.length);
      PreloaderService.hidePreLoader();
    }, () => PreloaderService.hidePreLoader(), null);


/*     this.employeeBasicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      employee => {
        if (employee) {
          PreloaderService.showPreLoader();
          this.adminService.GetFollowUpDetailByEmpId(employee.Id).pipe(takeUntil(this.destroy$)).subscribe(event => {
            if (event.Data) {
              this.followUpItems = event.Data
              // const today = now.getFullYear() + '-' + now.getMonth() + 1 + '-' + now.getDate()
              this.itemResource = new DataTableResource(this.followUpItems);
              this.itemResource.count().then(count => this.itemCount = count);
              this.items = this.followUpItems;
              this.itemCount = Number(this.followUpItems.length);
              PreloaderService.hidePreLoader();
            } else {
              PreloaderService.hidePreLoader();
            }
          }, Error => {
            PreloaderService.hidePreLoader();
          }
          );

        }
      }); */
  }

  filterInput(catValue: string, statusValue: string, branchValue: any) {
    this.filteredFollowUps = this.followUpItems
    if (branchValue && branchValue !== 'ALL') {
      this.filteredFollowUps = this.filterpipe.transform('BranchId', 'SELECT', this.filteredFollowUps, branchValue);
    }
    if (statusValue && statusValue !== '') {
      this.filteredFollowUps = this.filterpipe.transform('Status', 'NAMEFILTER', this.filteredFollowUps, statusValue);
    }
    if (catValue && catValue !== 'ALL') {
      this.filteredFollowUps = this.filterpipe.transform('TaskCategoryId', 'SELECT', this.filteredFollowUps, catValue);
    }
    this.itemResource = new DataTableResource(this.filteredFollowUps);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredFollowUps);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }


  openFollowUpModal(content: any, event:any) {
    this.selectedItem = event.row.item
    this.modalService.open(content, { width: '800' });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}







