import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeInfoFollowupComponent } from './employee-info-followup.component';

describe('EmployeeInfoFollowupComponent', () => {
  let component: EmployeeInfoFollowupComponent;
  let fixture: ComponentFixture<EmployeeInfoFollowupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeInfoFollowupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeInfoFollowupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
