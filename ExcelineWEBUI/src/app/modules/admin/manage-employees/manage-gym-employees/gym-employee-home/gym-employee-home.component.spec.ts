import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymEmployeeHomeComponent } from './gym-employee-home.component';

describe('GymEmployeeHomeComponent', () => {
  let component: GymEmployeeHomeComponent;
  let fixture: ComponentFixture<GymEmployeeHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymEmployeeHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymEmployeeHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
