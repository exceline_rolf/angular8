
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../../membership/services/exce-member.service'
import { AdminService } from '../../../services/admin.service'
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GymEmployeeHomeService } from './gym-employee-home.service';
import { ExceBreadcrumbService } from '../../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
const now = new Date();


@Component({
  selector: 'app-gym-employee-home',
  templateUrl: './gym-employee-home.component.html',
  styleUrls: ['./gym-employee-home.component.scss']
})
export class GymEmployeeHomeComponent implements OnInit, OnDestroy {
  private branchId: number;
  private itemResource: any = 0;
  private destroy$ = new Subject<void>();
  private filteredEmployees: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private classTypeForm: any;
  private rowItem: any;
  private model: any;
  private registerTemplateView: any;
  private selectedItem: any;
  private isEdit: boolean;
  private searchText: string;
  private searchVal: string;
  private employees: any;
  private categories: any;
  private selectedBranchId = -2;
  private empRoles: any;
  private rolePopModel: any;
  private deletingItem: any;
  items = [];
  itemCount = 0;
  branches = [];
  activeVal = true;
  searchTypeForm: any;
  openEmpInfoView: any;
  openAddNewEmployeeView: any;
  showRole = false;
  auotocompleteItems = [{ id: 'Name', name: 'Name :', isNumber: false }, { id: 'Id', name: 'id :', isNumber: true }]
  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };
  @ViewChild('autobox', { static: true }) public autocomplete;

  constructor(private exceMemberService: ExceMemberService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private router: Router,
    private gymEmployeeHomeService: GymEmployeeHomeService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.ManageEmployeesC',
      url: '/adminstrator/gym-employee-home'
    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
      if (value.id === 'DELETE_EMPL0YEE') {
        this.validateFollowUp();
      } else if (value.id === 'DELETE_SUCCESS') {
      }
    });
  }

  ngOnInit() {
    this.getEmployees(-2, '', this.activeVal, -1);
    this.getBranches()
    this.getCategories()
    this.searchTypeForm = this.fb.group({
      'searchVal': [this.searchVal]
    });
    this.autocomplete.items = this.auotocompleteItems;
    this.autocomplete.setSearchValue(this.auotocompleteItems);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.openAddNewEmployeeView) {
      this.openAddNewEmployeeView.close();
    }
    if (this.registerTemplateView) {
      this.registerTemplateView.close();
    }
  }

  contractSearch(value: any) {
    const selectedSearchItem = this.autocomplete.getTextValue();
    if (selectedSearchItem.searchVal) {
      this.getEmployees(this.selectedBranchId, selectedSearchItem.searchVal, this.activeVal, -1)
    } else {
      this.getEmployees(this.selectedBranchId, '', this.activeVal, -1)
    }
  }

  onSelect(item: any, value: any) {
    this.searchText = item.searchText; // this.getSearchText(item.name);
    this.searchVal = item.searchVal;
    this.getEmployees(this.selectedBranchId, item.searchText, this.activeVal, -1);
  }

  changeBranch(value) {
    if (value == -2) {
      this.showRole = false;
    } else {
      this.showRole = true;
    }
    this.selectedBranchId = value

    const selectedSearchItem = this.autocomplete.getTextValue();
    if (selectedSearchItem.searchVal) {
      this.getEmployees(this.selectedBranchId, selectedSearchItem.searchVal, this.activeVal, -1)
    } else {
      this.getEmployees(this.selectedBranchId, '', this.activeVal, -1)
    }
  }

  closedSearch() {
    this.autocomplete.textValue = '';
    this.getEmployees(-2, '', this.activeVal, -1);
  }

  getEmployees(branchId: number, searchText: string, isActive: boolean, roleId: number) {
    this.adminService.GetGymEmployees(branchId, searchText, isActive, roleId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.employees = result.Data;
        this.itemResource = new DataTableResource(this.employees);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.employees;
        this.itemCount = Number(result.Data.length);
      }
    })
  }

  getBranches() {
    this.adminService.getBranches(-1).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.branches = result.Data;
      }
    })
  }

  getCategories() {
    this.adminService.getCategoriesByType('EMPLOYEE').pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.categories = result.Data;
      }
    })
  }

  openAddNewEmployeeModel(content: any) {
    this.openAddNewEmployeeView = this.modalService.open(content);
  }

  changeActiveVal(event) {
    if (event.target.checked) {
      this.activeVal = event.target.value;
      // this.getContracts('', this.activeVal, '')

      const selectedSearchItem = this.autocomplete.getTextValue();
      if (selectedSearchItem.searchVal) {
        this.getEmployees(this.selectedBranchId, selectedSearchItem.searchVal, this.activeVal, -1)
      } else {
        this.getEmployees(this.selectedBranchId, '', this.activeVal, -1)
      }
    }
  }

  newTemplateModal(content: any, isEdit: boolean) {
    this.isEdit = isEdit;
    this.registerTemplateView = this.modalService.open(content, {});
  }

  filterInput(nameValue: any) {
    this.filteredEmployees = this.employees
    if (nameValue && nameValue !== '') {
      this.filteredEmployees = this.filterpipe.transform('Name', 'NAMEFILTER', this.filteredEmployees, nameValue);
    }

    this.itemResource = new DataTableResource(this.filteredEmployees);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredEmployees);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  getEmpRoles(item, content) {
    this.adminService.GetGymEmployeeRolesById(this.branchId, item.Id).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.empRoles = result.Data;
        if (this.rolePopModel) {
          this.rolePopModel.close();
        }
        this.rolePopModel = content
        this.rolePopModel.open();
      }
    })
  }

  closeEmpPop() {
    if (this.rolePopModel) {
      this.rolePopModel.close();
    }
  }


  deleteEmp(item) {
    this.deletingItem = item;
    this.openExceDeleteEmployeeMessage('CONFIRM', 'Are you sure you want to delete this?')
  }

  openExceDeleteEmployeeMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'CONFIRM',
        messageBody: body,
        msgBoxId: 'DELETE_EMPL0YEE'

      }
    );
  }

  validateFollowUp() {
    this.adminService.ValidateEmployeeFollowUp({ 'employeeId': this.deletingItem.Id, 'endDate': now }).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.deleteEmployee()
      }
    })
  }

  deleteEmployee() {
    this.adminService.DeleteGymEmployee({ 'memberId': this.deletingItem.Id, 'assignEmpId': -1 }).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.openExceDeleteEmployeeSuccessMessage('CONFIRM', 'EmHomeDeletedMessage')
      }
    })

  }

  openExceDeleteEmployeeSuccessMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'CONFIRM',
        messageBody: body,
        msgBoxId: 'DELETE_SUCCESS'

      }
    );
  }
  rowDoubleClick(rowItem) {
    const url = '/adminstrator/employee-card/' + rowItem.Id + '/' + this.branchId;
    this.router.navigate([url]);

  }

  loadEmployeeCalendar(item) {
    this.gymEmployeeHomeService.$selectedGymEmployee = item;
    this.router.navigate(['/adminstrator/gym-employee-home/calendar/']);
  }
}
