
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AdminService } from '../../../services/admin.service'
import { EmployeeBasicInfoService } from '../employee-basic-info/employee-basic-info.service';
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { error } from 'util';
import { PreloaderService } from '../../../../../shared/services/preloader.service'
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { takeUntil, concatMap, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
const now = new Date();

@Component({
  selector: 'app-employee-info-time',
  templateUrl: './employee-info-time.component.html',
  styleUrls: ['./employee-info-time.component.scss']
})

export class EmployeeInfoTimeComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private currentEmployee: any;
  private selectedDate: any;
  private contractList = []
  private textComment = '';
  private newModal: any;
  private ContractItemList = []
  private selectedCategoryString = ''
  private today;
  private formSubmited = false;
  private branchId = 1;
  private itemResource: any = 0;
  private timeEntries = []
  private filteredTime = []
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private lastFromDate: any;
  private lastToDate: any;

  public rowTooltip: any;
  public items = [];
  public itemCount = 0;
  public branches = []
  public Categories: any
  addNewTimeView: any;
  NewTimeForm: any;
  selectedCategory: string;
  selectedBranch: number

  // @ViewChild('followUpProfile') public followUpProfile;
  // @ViewChild('updateMember') public updateMember;
  formErrors = {
    'WorkDate': '',
    'BranchId': '',
    'TimeCategoryId': '',
    'Comment': '',

  };

  validationMessages = {
    'WorkDate': {
      'required': 'Work date is required',
    },
    'BranchId': {
      'required': 'Branch is Required',
    },
    'TimeCategoryId': {
      'required': 'Category is required',
    },
    'Comment': {
      'required': 'Comment is Required',
    },

  }
  constructor(
    private employeeBasicInfoService: EmployeeBasicInfoService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private modalService: UsbModal,
    private fb: FormBuilder,
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;

      });
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
      ).subscribe(branch => {
      if (branch.Data) {
        this.branches = branch.Data;
      }
    })

    this.adminService.getCategoriesByType('TIME').pipe(
      takeUntil(this.destroy$)
      ).subscribe(category => {
      if (category.Data) {
        this.Categories = category.Data;
      }
    })
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.selectedDate = { date: this.today }
  }


  ngOnInit() {
    // load the data to the table
    this.employeeBasicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      employee => {
        if (employee) {
          this.currentEmployee = employee
          this.getTimeEntries()
        }
      });

    this.NewTimeForm = this.fb.group({
      'WorkDate': [{ date: this.today }, [Validators.required]],
      'StartTime': [['08:00']],
      'EndTime': [['08:00']],
      'BranchId': [null, [Validators.required]],
      'TimeCategoryId': [null, [Validators.required]],
      'Comment': ['', [Validators.required]],
    });
    this.NewTimeForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => UsErrorService.onValueChanged(this.NewTimeForm, this.formErrors, this.validationMessages));
  }

  ngOnDestroy() {
    if (this.addNewTimeView) {
      this.addNewTimeView.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  getTimeEntries() {
    PreloaderService.showPreLoader();
    this.adminService.GetEmployeeTimeEntries(this.currentEmployee.Id).pipe(
      takeUntil(this.destroy$)
      ).subscribe(event => {
      if (event.Data) {
        this.timeEntries = event.Data
        // const today = now.getFullYear() + '-' + now.getMonth() + 1 + '-' + now.getDate()
        this.itemResource = new DataTableResource(this.timeEntries);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.timeEntries;
        this.itemCount = Number(this.timeEntries.length);
        PreloaderService.hidePreLoader();
      } else {
        PreloaderService.hidePreLoader();
      }
    }, Error => {
      PreloaderService.hidePreLoader();
    }
    );
  }

  filterInput(FromDateVal: any, ToDateVal: any, CatValue: string, branchValue: any) {
    if (FromDateVal) {
      this.lastFromDate = JSON.parse(JSON.stringify(FromDateVal));
      FromDateVal = FromDateVal.date.year + '-' + FromDateVal.date.month + '-' + FromDateVal.date.day
      if (this.lastToDate && this.lastToDate.formatted) {
        ToDateVal = this.lastToDate.date.year + '-' + this.lastToDate.date.month + '-' + this.lastToDate.date.day
      } else {
        ToDateVal = ''
      }
    }
    if (ToDateVal) {
      this.lastToDate = JSON.parse(JSON.stringify(ToDateVal));
      ToDateVal = ToDateVal.date.year + '-' + ToDateVal.date.month + '-' + ToDateVal.date.day
      if (this.lastFromDate && this.lastFromDate.formatted) {
        FromDateVal = this.lastFromDate.date.year + '-' + this.lastFromDate.date.month + '-' + this.lastFromDate.date.day
      } else {
        FromDateVal = ''
      }
    }
    this.filteredTime = this.timeEntries
    // FromDateVal='2018-03-15'
    if (branchValue && branchValue !== 'ALL') {
      this.filteredTime = this.filterpipe.transform('BranchName', 'NAMEFILTER', this.filteredTime, branchValue.split('_')[1]);
    }
    if (CatValue && CatValue !== 'ALL') {
      this.filteredTime = this.filterpipe.transform('TimeCategoryId', 'SELECT', this.filteredTime, CatValue);
    }
    if (FromDateVal && FromDateVal !== '' && FromDateVal != '0-0-0') {
      this.filteredTime = this.filterpipe.transform('WorkDate', 'FROM_DATE', this.filteredTime, FromDateVal);
    }
    if (ToDateVal && ToDateVal !== '' && ToDateVal != '0-0-0') {
      this.filteredTime = this.filterpipe.transform('WorkDate', 'TO_DATE', this.filteredTime, ToDateVal);
    }

    this.itemResource = new DataTableResource(this.filteredTime);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredTime);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  openaddNewTimeView(content: any) {
    this.addNewTimeView = this.modalService.open(content, { width: '600' });
  }

  changeCategory(value, createdDate) {
    this.contractList = []
    this.selectedCategory = value;

    if (this.selectedCategory != '') {

      for (let cat of this.Categories) {
        this.selectedCategoryString = cat.Code
        if (cat.Code == 'CONTRACT' && this.selectedBranch !== -1) {

          this.getContractsList()
        }
      }
    }
    // if (this.selectedCategory == 'CONTRACT' && this.selectedBranch !== -1) {
    //   this.getContractsList()
    // }
  }

  changeBranch(value, ) {
    this.contractList = []
    this.selectedBranch = value
    if (this.selectedCategory == 'CONTRACT' && this.selectedBranch !== -1) {
      this.getContractsList()
    }
  }

  changeDate(value) {
    this.selectedDate = value;
  }

  getContractsList() {
    if (this.selectedDate.date) {
      let createdDate = this.selectedDate.date.year + '-' + this.selectedDate.date.month + '-' + this.selectedDate.date.day
      this.adminService.GetContractSummariesByEmployee(this.currentEmployee.Id, this.branchId, createdDate).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result.Data) {
          this.contractList = result.Data;
        }
      })
    }
  }


  addClick() {
    let tempComment = '';
    let tempContractList = []
    this.contractList.forEach(contract => {
      if (contract.IsGroup) {
        tempComment = tempComment + ' ' + contract.ContractNo;
        tempContractList.push(contract.Id);
      }
    })
    this.textComment = this.textComment + 'Contract No :' + tempComment
    this.ContractItemList = tempContractList
  }


  submitForm(value) {
    this.formSubmited = true;
    if (this.NewTimeForm.valid) {
      this.saveTime(value)
      this.formSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.NewTimeForm, this.formErrors, this.validationMessages);
    }
  }
  saveTime(value) {
    value.WorkDate = this.selectedDate.date.year + '-' + this.selectedDate.date.month + '-' + this.selectedDate.date.day
    value.StartTime = value.StartTime + ':00'
    value.EndTime = value.EndTime + ':00'
    value.ContractItemList = this.ContractItemList
    value.EmployeeId = this.currentEmployee.Id
    const fromTime = value.WorkDate + ' ' + value.StartTime
    const endTime = value.WorkDate + ' ' + value.EndTime
    value.TimeInMinutes = this.getMins(fromTime, endTime)
    this.adminService.AddEmployeeTimeEntry(value).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result.Data) {
        this.getTimeEntries()
        this.closeModal()
      }
    })
  }

  closeModal() {
    this.formSubmited = false
    this.contractList = []
    this.textComment = '';
    this.selectedCategoryString = ''
    this.NewTimeForm.reset();
    this.NewTimeForm.patchValue(
      {
        'WorkDate': { date: this.today },
        'StartTime': '08:00',
        'EndTime': '08:00',
        'Comment': ''
      }
    )
    this.addNewTimeView.close()
  }

  getMins(fromTime, toTime) {
    var timeDiff = Date.parse(toTime) - Date.parse(fromTime);
    const difference = Math.abs(timeDiff) / 60000;
    return difference
  }
}

