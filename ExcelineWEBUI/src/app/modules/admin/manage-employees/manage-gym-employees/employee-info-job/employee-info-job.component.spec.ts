import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeInfoJobComponent } from './employee-info-job.component';

describe('EmployeeInfoJobComponent', () => {
  let component: EmployeeInfoJobComponent;
  let fixture: ComponentFixture<EmployeeInfoJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeInfoJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeInfoJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
