
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';

import { Component, OnInit, ElementRef, ComponentFactoryResolver, ViewContainerRef, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { ExceMemberService } from '../../../membership/services/exce-member.service';
import { EmployeeBasicInfoService } from '../manage-gym-employees/employee-basic-info/employee-basic-info.service';
import { ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UsBreadcrumbService } from '../../../../shared/components/us-breadcrumb/us-breadcrumb.service';
import { ISlimScrollOptions } from '../../../../shared/directives/us-scroll/classes/slimscroll-options.class';
// import { McContractsComponent } from './mc-contract/mc-contracts/mc-contracts.component';
// import { McInfoWithNotesComponent } from './mc-info-with-notes/mc-info-with-notes.component';
// import { McBookingComponent } from './mc-booking/mc-booking.component';
// import { McClassesComponent } from './mc-classes/mc-classes.component';
// import { McXtraLogComponent } from './mc-xtra-log/mc-xtra-log.component';
// import { McIntroducedMembersComponent } from './mc-introduced-members/mc-introduced-members.component';
// import { McEventLogComponent } from './mc-event-log/mc-event-log.component'
// import { McCommunicationLogComponent } from './mc-communication-log/mc-communication-log.component';
// import { McPaymentsComponent } from 'app/modules/membership/member-card/mc-payments/mc-payments.component';
// import { McInterestsComponent } from './mc-interests/mc-interests.component';
// import { McSponsoringComponent } from './mc-sponsoring/mc-sponsoring.component';
// import { McVisitComponent } from './mc-visit/mc-visit.component'
// import { McContractRegComponent } from './mc-contract/mc-contract-reg/mc-contract-reg.component';
// import { McContractDetailsComponent } from './mc-contract/mc-contract-details/mc-contract-details.component';
// import { McOrdersDetailsComponent } from './mc-orders/mc-orders-details/mc-orders-details.component';
// import { McOrdersHomeComponent } from './mc-orders/mc-orders-home/mc-orders-home.component';
// import { McDocumentsComponent } from './mc-documents/mc-documents.component';
// // import { McFollowUpComponent } from './mc-follow-up/mc-follow-up.component';
// import { McResignComponent } from './mc-resign/mc-resign.component';
import { EmployeeCardService } from './employee-card.service';
// import { McTrainingProgramComponent } from '../member-card/mc-training-program/mc-training-program.component';
// import { McEconomyComponent } from 'app/modules/membership/member-card/mc-economy/mc-economy.component';
// import { McFreezeHomeComponent } from '../member-card/mc-freeze/mc-freeze-home/mc-freeze-home.component';
import { TranslateService } from '@ngx-translate/core';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { CropperSettings } from '../../../../shared/components/us-crop-image/cropperSettings';
import { UsCropImageComponent } from '../../../../shared/components/us-crop-image/us-crop-image.component';
import { Bounds } from '../../../../shared/components/us-crop-image/model/bounds';

@Component({
  selector: 'app-employee-card',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.scss']
})
export class EmployeeCardComponent implements OnInit {
  private _employeeID: number;
  private _branchID: number;
  private _memberRole: string;
  public _editMode = false;
  private _isMenuOpen = false;
  state = 'INFOWITHNOTE';
  title: string;
  selectedMember?: any;
  countries?: any;
  private options: ISlimScrollOptions;
  private tabContentHeight: number;
  // private contractComponent: McContractsComponent;
  public isleftPanelCollapsed: boolean;
  private loadedInfo = false;
  public adminImage = "assets/images/member-card-avatar.svg";
  @Input() MemberId;
  @Input() BranchId;
  @Input() Role;

  @ViewChild('mainContainer', { read: ViewContainerRef, static: false }) mainContainer;

  /* **********************************************************************
   * variables for cropping image
   * Fri Apr 06 2018 08:42:41 GMT+0530 (Sri Lanka Standard Time)
  **********************************************************************/
  atDecition = true;
  useWebcam = false;
  useFileUpload = false;
  reader = new FileReader();
  name: string;
  data1: any;
  cropperSettings1: CropperSettings;
  croppedWidth: number;
  croppedHeight: number;
  @ViewChild('cropper', {static: false}) cropper: UsCropImageComponent;
  /////////////////////////////////////


  constructor(
    private elRef: ElementRef,
    private memberService: ExceMemberService,
    private employeeInfoService: EmployeeBasicInfoService,
    private route: ActivatedRoute,
    private breadcrumbService: UsBreadcrumbService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private viewContainerRef: ViewContainerRef,
    private translateService: TranslateService,
    private employeeCardService: EmployeeCardService,
    private modalService: UsbModal,
    private router: Router,
    private adminService: AdminService

  ) {
    this.isleftPanelCollapsed = false;
    this._employeeID = this.route.snapshot.params['Id'];
    this._branchID = this.route.snapshot.params['BranchId'];

    this.options = {
      barBackground: '#C9C9C9',
      gridBackground: '#D9D9D9',
      barBorderRadius: '10',
      barWidth: '6',
      gridWidth: '2'
    };

    breadcrumbService.hideRoute('/adminstrator/employee-card');
    breadcrumbService.hideRoute('/adminstrator/employee-card/' + this.route.snapshot.params['Id']);
    breadcrumbService.hideRoute('/adminstrator/employee-card/' + this.route.snapshot.params['Id'] + '/' + this.route.snapshot.params['BranchId']);


    this.route.url.subscribe(result => {
      if (result) {

        this.setTitle()

      }
    });

    /***********************************************************************
    * initialize values for cropping image
    * Fri Apr 06 2018 08:42:41 GMT+0530 (Sri Lanka Standard Time)
    **********************************************************************/
    this.cropperSettings1 = new CropperSettings();
    this.cropperSettings1.width = 100;
    this.cropperSettings1.height = 100;

    this.cropperSettings1.croppedWidth = 200;
    this.cropperSettings1.croppedHeight = 200;

    this.cropperSettings1.canvasWidth = 200;
    this.cropperSettings1.canvasHeight = 200;

    this.cropperSettings1.minWidth = 10;
    this.cropperSettings1.minHeight = 10;

    this.cropperSettings1.rounded = false;
    this.cropperSettings1.keepAspect = false;

    this.cropperSettings1.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings1.cropperDrawSettings.strokeWidth = 2;

    this.data1 = {};
    //////////////////////////////////////////////////////////////////////

  }

  ngOnInit() {
    this.adminService.GetGymEmployeeByEmployeeId(this._branchID, this._employeeID).subscribe
      (
      result => {
        if (result) {
          this.employeeInfoService.setSelectedMember(result.Data);
          if (result.Data.ImagePath !== "" && result.Data.ImagePath !== null) {
            this.adminImage = result.Data.ImageURLDomain;
          } else {
            this.adminImage = "assets/images/member-card-avatar.svg";
          }
          this.breadcrumbService.addFriendlyNameForRoute('/adminstrator/employee-card/' + this.route.snapshot.params['Id'] + '/' + this.route.snapshot.params['BranchId'], this.selectedMember.Name);
        } else {

        }
      }
      );
    this.employeeInfoService.currentMember.subscribe(
      member => this.selectedMember = member
    );
    this.employeeInfoService.getEditMode().subscribe(editMode => this._editMode = editMode);
    this.translateService.get('ADMIN.EmRoles').subscribe(translatedValue => this.title = translatedValue);
    this.setTitle();
    // this.loadedInfo = true
    this.setCardState('employee-info-roles');
  }

  saveEditedPic() {

    this.adminImage = '';

    if (this.selectedMember.BirthDay && this.selectedMember.BirthDay.date) {
      this.selectedMember.BirthDay = this.selectedMember.BirthDay.date.year + '-' + this.selectedMember.BirthDay.date.month + '-' + this.selectedMember.BirthDay.date.day
    }
    if (this.selectedMember.StartDate && this.selectedMember.StartDate.date) {
      this.selectedMember.StartDate = this.selectedMember.StartDate.date.year + '-' + this.selectedMember.StartDate.date.month + '-' + this.selectedMember.StartDate.date.day
    }
    if (this.selectedMember.EndDate && this.selectedMember.EndDate.date) {
      this.selectedMember.EndDate = this.selectedMember.EndDate.date.year + '-' + this.selectedMember.EndDate.date.month + '-' + this.selectedMember.EndDate.date.day
    }
    this.selectedMember.Image = this.data1.image;
    if (this.selectedMember.Image !== null && this.selectedMember.Image !== "") {

      this.adminService.SaveGymEmployee(this.selectedMember).subscribe(
        result => {
          this.adminImage = '';

          if (result.Status === "OK") {
            this.employeeInfoService.setSelectedMember(this.selectedMember);
            // this.adminImage = this.selectedMember.ImageURLDomain;
            this.adminImage = this.data1.image;
            this.cancelAction();
          }
        }
      );

    }
  }

  /***********************************************************************
   * functions for cropping image
   * Fri Apr 06 2018 08:42:41 GMT+0530 (Sri Lanka Standard Time)
   **********************************************************************/
  wantToUploadFile() {
    this.atDecition = false;
    this.useFileUpload = true;
    this.useWebcam = false;
  }

  wantToUseWebCam() {
    this.atDecition = false;
    this.useFileUpload = false;
    this.useWebcam = true;
  }

  cropped(bounds: Bounds) {
    this.croppedHeight = bounds.bottom - bounds.top;
    this.croppedWidth = bounds.right - bounds.left;
  }

  fileChangeListener($event) {
    var image: any = new Image();
    var file: File = $event.target.files[0];
    var myReader: FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);
    };
    myReader.readAsDataURL(file);
  }

  cancelAction() {
    this.atDecition = true;
    this.useFileUpload = false;
    this.useWebcam = false;
    this.data1 = {};
  }

  doesImageExists(url: string): boolean { // will check if the image exists at a perticualr url
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.withCredentials = true;
    http.send();
    var returnVal = http.status != 404
    debugger;
    return http.status != 404;
  }
  ////////////////////////////////////////////////////////////////////////

  setTitle() {
    const currentComponent = this.router.url.split('/')[this.router.url.split('/').length - 1]
    switch (currentComponent) {
      case 'employee-info-roles': {
        this.translateService.get('ADMIN.EmRoles').subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'employee-info-work': {
        this.translateService.get('ADMIN.WorkC').subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'employee-info-bookings': {
        this.translateService.get('ADMIN.Bookings').subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'employee-info-classes': {
        this.translateService.get('ADMIN.TitleClasses').subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'employee-info-followup': {
        this.translateService.get('ADMIN.Followup').subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'employee-info-job': {
        this.translateService.get('ADMIN.Job').subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'employee-info-events': {
        this.translateService.get('ADMIN.Events').subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'employee-info-time': {
        this.translateService.get('ADMIN.Time').subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      default: {
      }
        this.closeNav();
    }

  }
  editCustomer() {
    this.employeeInfoService.updateEditMode(true, 'CARD');
    this._editMode = true;
  }

  editImage(content) {
    this.modalService.open(content, { width: '800' });
    this.employeeInfoService.updateEditMode(true, 'IMAGE');
  }

  openNav() {
    if (!this._isMenuOpen) {
      this._isMenuOpen = true;
      document.getElementById('mc-menu-slide').style.width = '200px';
      this.elRef.nativeElement.querySelector('.off-canvas-panel-ani').style.marginRight = '200px';
    } else {
      this.closeNav();
    }
  }

  closeNav() {
    this._isMenuOpen = false;
    if (document.getElementById('mc-menu-slide')) {
      document.getElementById('mc-menu-slide').style.width = '0';
    }
    if (this.elRef.nativeElement.querySelector('.off-canvas-panel-ani')) {
      this.elRef.nativeElement.querySelector('.off-canvas-panel-ani').style.marginRight = '0';
    }
  }

  setCardState(status: string) {

    const parentRoute = '/adminstrator/employee-card/' +
      this.route.snapshot.params['Id'] + '/' +
      this.route.snapshot.params['BranchId']




    if (this.loadedInfo) {
      this.employeeCardService.$isleftPanelCollapsed = true;
      this.isleftPanelCollapsed = true;
    } else {
      this.employeeCardService.$isleftPanelCollapsed = false;
      this.isleftPanelCollapsed = false;
      this.loadedInfo = true;
    }

    this._editMode = false;
    switch (status) {
      case 'employee-info-roles': {
        this.router.navigate([parentRoute + '/employee-info-roles']);
        this.translateService.get('ADMIN.EmRoles').subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'employee-info-work': {
        this.router.navigate([parentRoute + '/employee-info-work']);
        this.translateService.get('ADMIN.WorkC').subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'employee-info-bookings': {
        this.router.navigate([parentRoute + '/employee-info-bookings']);
        this.translateService.get('ADMIN.Bookings').subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'employee-info-classes': {
        this.router.navigate([parentRoute + '/employee-info-classes']);
        this.translateService.get('ADMIN.TitleClasses').subscribe(translatedValue => this.title = translatedValue);
        break;
      }
      case 'employee-info-followup': {
        this.router.navigate([parentRoute + '/employee-info-followup']);
        this.translateService.get('ADMIN.Followup').subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'employee-info-job': {
        this.router.navigate([parentRoute + '/employee-info-job']);
        this.translateService.get('ADMIN.Job').subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'employee-info-events': {
        this.router.navigate([parentRoute + '/employee-info-events']);
        this.translateService.get('ADMIN.Events').subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      case 'employee-info-time': {
        this.router.navigate([parentRoute + '/employee-info-time']);
        this.translateService.get('ADMIN.Time').subscribe(translatedValue => this.title = translatedValue);
        break;
      }

      default: {
      }

    }
    this.closeNav();
  }

  goToMemberCard() {
    const url = '/membership/card/' + this.selectedMember.MemberId + '/' + this._branchID + '/' + 'EMP';
    this.router.navigate([url]);
  }

}
