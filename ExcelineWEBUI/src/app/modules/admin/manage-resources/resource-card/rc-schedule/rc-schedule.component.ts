import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { RcBasicInfoService } from '../rc-basic-info/rc-basic-info.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { AdminService } from '../../../services/admin.service';
import { TranslateService } from '@ngx-translate/core';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EntitySelectionType, ColumnDataType, MemberSearchType, MemberRole } from '../../../../../shared/enums/us-enum';
import { ExceLoginService } from '../../../../login/exce-login/exce-login.service';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import * as moment from 'moment';
import { Dictionary } from 'lodash';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'app-rc-schedule',
  templateUrl: './rc-schedule.component.html',
  styleUrls: ['./rc-schedule.component.scss']
})
export class RcScheduleComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private _user: any;
  private scheduleItemCount = 0;
  private scheduleItemResource: any = 0;
  private branchId: number;
  private entitySelectionViewTitle: string;
  beforeUpdate: any;
  enDate: { date: { year: number; month: number; day: number; }; };
  stDate: { date: { year: number; month: number; day: number; }; };
  selectedDay: any;
  IsUpdate: boolean;
  existingEntry = false
  subCurrentRes: any;
  additionalResource: any;
  selectedEmp: any;
  msgBoxModal: any;
  formSubmited = false;
  entitySearchType = 'RES';
  selectedResource: any;
  entityItemCount: any;
  entityItems: any;
  entitySelectionView: any;
  defaultRole = 'ALL'
  employeeSelectionView: any;
  items = [];
  columns: any;
  entitySelectionType: string;
  isDbPagination = true;
  dayList?: any[] = [];
  public scheduleForm: FormGroup;
  deletedItemId: any;
  deletedItem: any;
  msgBoxDel: any;
  newScheduleView: any;
  itemCount = 0;
  resourcesList?: any[] = [];
  itemResource: DataTableResource<any>;
  today: { year: number; month: number; day: number; };
  updateStatus: boolean;
  planTitle: string;

 @ViewChild('scheduleItems', { static: true }) scheduleItemsModal: ElementRef;

  auotocompleteItemsForEmpSelection = [{ id: 'NAME', name: 'Name :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }];
  auotocompleteItems = [{ id: 'Name', name: 'Name :', isNumber: false }, { id: 'Id', name: 'Id :', isNumber: true }];
  ResourceColumns = [{ property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }];

  // fromDatePickerOptions: IMyDpOptions = {
  //   disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 }
  // };

  toDatePickerOptions: IMyDpOptions = {
    disableUntil: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() - 1 }
  };

  formErrors = {
    'StartTime': '',
    'EndTime': ''
  };

  validationMessages = {
    'StartTime': {
      'required': 'ADMIN.ErrorEnterStartTime',
    },
    'EndTime': {
      'required': 'ADMIN.ErrorEnterEndTime'
    }
  }
  scheduleItemsForScheduleItem: any;

  constructor(
    private exceLoginService: ExceLoginService,
    private _fb: FormBuilder,
    private _exceMessageService: ExceMessageService,
    private translateService: TranslateService,
    private adminService: AdminService,
    private modalService: UsbModal,
    private resourceBasicInfoService: RcBasicInfoService
  ) {
    this._exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      if (value.id === 'DELETE_SCHEDULE') {
        this.ScheduleDelYesHandler();
      }
      // if (value.id === 'UPDATE_SCHEDULE') {
      //   this.ScheduleUpdateYesHandler();
      // }
    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      });

    // this.subCurrentRes = this.resourceBasicInfoService.resourceSource.pipe(takeUntil(this.destroy$)).subscribe(result => {
    //   if (result) {
    //     this.selectedResource = result;
    //   }
    // });

    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() - 1 };

    this.scheduleForm = this._fb.group({
      'Id': [null],
      'StartDate': [null],
      'EndDate': [null],
      'EmpName': [null],
      'EmpId': [null],
      'StartTime': [null, [Validators.required]],
      'EndTime': [null, [Validators.required]],
      'ResourcesName': [null],
      'ResourcesId': [null],
      'WeekType': ['2'],
      'IsMonday': [null],
      'IsTuesday': [null],
      'IsWednesday': [null],
      'IsThursday': [null],
      'IsFriday': [null],
      'IsSaturday': [null],
      'IsSunday': [null],
      'Day': [null],
      'UpdateStatus': [0]
    });

  }

  ngOnInit() {
    this._user = this.exceLoginService.CurrentUser.username;
    const date = new Date();
    this.scheduleForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      UsErrorService.onValueChanged(this.scheduleForm, this.formErrors, this.validationMessages)
    });
    this.getScheduleData();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.newScheduleView) {
      this.newScheduleView.close();
    }
    if (this.msgBoxModal) {
      this.msgBoxModal.close();
    }
    if (this.employeeSelectionView) {
      this.employeeSelectionView.close();
    }
    if (this.entitySelectionView) {
      this.entitySelectionView.close();
    }
  }

  getScheduleData() {
    this.selectedResource = this.resourceBasicInfoService.getSelectedResource();
    if (!this.selectedResource.Schedule) {
      this.selectedResource.Schedule = {};
    }
    this.getResourceScheduleItems();
    // this.scheduleItemList = this.selectedSchedule.SheduleItemList;
  }

  openNewScheduleView(content: any) {
    this.planTitle = 'ADMIN.NewSchedule';
    this.IsUpdate = false;
    const today = new Date();
    const sDate = {
      date: {
        year: today.getFullYear(),
        month: today.getMonth() + 1,
        day: today.getDate()
      }
    }
    this.scheduleForm.patchValue({ StartDate: sDate });
    this.newScheduleView = this.modalService.open(content, { width: '800' });
    this.IsUpdate = true
  }

  openNewScheduleViewUpdate(content: any) {
    this.planTitle = 'ADMIN.EditSchedule';
    this.IsUpdate = true;
    this.newScheduleView = this.modalService.open(content, { width: '800' });
    this.IsUpdate = false;
  }

  // get from date change
  fromDateChange(event, toDate) {
    this.toDatePickerOptions = {
      disableUntil: { year: event.date.year, month: event.date.month, day: event.date.day - 1 }
    };
    toDate.showSelector = true;
  }

  deleteScheduleItem(item) {
    this.deletedItem = item;
    this.deletedItemId = item.Id;
    let hasFutureBookings;
    const rescourceList = [];
    rescourceList.push(this.selectedResource.Id)
    const entityActiveTime = {
      'Branchid': this.branchId,
      'StartDate': new Date(),
      // Enddate so that we get every future bookings
      'EndDate': new Date(2099, 11, 31),
      'EntityList': rescourceList,
      'EntityRoleType': 'RES'
    }
    this.adminService.GetEntityActiveTimes(entityActiveTime).pipe(takeUntil(this.destroy$)).subscribe(
      bookings => {
        if (bookings) {
          this.scheduleItemsForScheduleItem = bookings.Data
          const newList = [];
          this.scheduleItemsForScheduleItem.forEach(booking => {
            if (Number(booking.ScheduleParentId) === Number(this.deletedItemId)) {
              newList.push(booking);
            }
          });
          this.scheduleItemsForScheduleItem = newList;
          this.scheduleItemsForScheduleItem.forEach(element => {
            let members = '';
            element.BookingMemberList.forEach(memb => {
              members += memb.Name + '; '
              element.members = members;
            });
          });
          hasFutureBookings =  this.scheduleItemsForScheduleItem.length;
          if (hasFutureBookings >= 1) {
            this.msgBoxModal = this.modalService.open(this.scheduleItemsModal, { width: '1200' })
          } else {
            let messageTitle = '';
            let messageBody = '';
            (this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
            (this.translateService.get('ADMIN.HasBookingDoYouWantToDelete').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
            this.msgBoxDel = this._exceMessageService.openMessageBox('CONFIRM',
              {
                messageTitle: messageTitle,
                messageBody: messageBody,
                msgBoxId: 'DELETE_SCHEDULE'
              });
          }
        }
      }
    )
  }

  closeScheduleItems() {
    this.msgBoxModal.close();
  }

  deleteConfirm() {
    this.ScheduleDelYesHandler();
    this.closeScheduleItems();
  }

  ScheduleDelYesHandler() {
    this.adminService.DeleteResourcesScheduleItem(this.deletedItemId).pipe(takeUntil(this.destroy$)).subscribe(res => {
      const index: number = this.selectedResource.Schedule.SheduleItemList.indexOf(this.deletedItem);
      if (index !== -1) {
        this.selectedResource.Schedule.SheduleItemList.splice(index, 1);
      }
    }, null, null);
  }

  // Row Double click for update event
  getSelectedSchedule(event) {
    this.IsUpdate = true;
    this.beforeUpdate = event.row.item;

    if (event.row.item.StartDate) {
      const toDate = new Date(event.row.item.StartDate);
      this.stDate = {
        date: {
          year: toDate.getFullYear(),
          month: toDate.getMonth() + 1,
          day: toDate.getDate()
        }
      }
    }

    if (event.row.item.EndDate) {
      const fromDate = new Date(event.row.item.EndDate);
      this.enDate = {
        date: {
          year: fromDate.getFullYear(),
          month: fromDate.getMonth() + 1,
          day: fromDate.getDate()
        }
      }
    }

    this.scheduleForm.patchValue({
      Id: event.row.item.Id,
      StartDate: this.stDate,
      EndDate: this.enDate,
      EmpName: event.row.item.EmpName,
      EmpId: event.row.item.Id,
      StartTime: [moment(new Date(event.row.item.StartTime)).format('HH:mm')],
      EndTime: [moment(new Date(event.row.item.EndTime)).format('HH:mm')],
      ResourcesName: event.row.item.ResourcesName,
      ResourcesId: event.row.item.ResourceId,
      WeekType: event.row.item.WeekType,
      Day: event.row.item.Day,
      UpdateStatus: 1,
      IsBooking: event.row.item.IsBooking
    });
  }

  SaveSchedule(value) {
    // this.dayList = [];
    this.existingEntry = false;
    let validity = true;
    if (value.Day !== null) {
      this.dayList.push(value.Day);
    }
    if (value.IsBooking) {
      if (value.IsBooking === true) {
        let messageTitle = '';
        let messageBody = '';
        (this.translateService.get('Exceline').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
        (this.translateService.get('ADMIN.MsgUpdateScheduleConfirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
        this.msgBoxModal = this._exceMessageService.openMessageBox('CONFIRM',
          {
            messageTitle: messageTitle,
            messageBody: messageBody,
            msgBoxId: 'UPDATE_SCHEDULE'
          });
      }
    }
    this.dateValidator(value);
    this.formSubmited = true;
    if (this.scheduleForm.valid) {


      if (value.StartTime >= value.EndTime) {
        validity = false;
        let messageTitle = '';
        let messageBody = '';
        (this.translateService.get('Exceline').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
        (this.translateService.get('ADMIN.EndTimeShouldBeGreaterThenStartTime').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
        this.msgBoxModal = this._exceMessageService.openMessageBox('ERROR',
          {
            messageTitle: messageTitle,
            messageBody: messageBody,
            msgBoxId: 'VALIDATE_TIME'
          });
      }

      if (this.dayList.length === 0) {
        validity = false;
        let messageTitle = '';
        let messageBody = '';
        (this.translateService.get('Exceline').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
        (this.translateService.get('ADMIN.SelectTheDay').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
        this.msgBoxModal = this._exceMessageService.openMessageBox('ERROR',
          {
            messageTitle: messageTitle,
            messageBody: messageBody,
            msgBoxId: 'VALIDATE_DAY'
          });
      }

      if (value.EmpId === null || value.EmpName === '') {
        this.selectedEmp = null;
      } else {
        this.selectedEmp.Id = value.EmpId;
      }
      if (value.ResourcesId === null || value.ResourcesId === '') {
        value.ResourcesId = 0;
      }
      // if (value.ResourcesName === '') {
      //   this.additionalResource = null;
      // }

      // if (!this.additionalResource) {
      //   this.additionalResource.Id = value.ResourceId;
      // }
      const addedscheduleItem = {
        ActiveStatus: true,
        StartDate: this.getDateConverter(value.StartDate),
        StartTime: '1900-01-01T' + value.StartTime + ':00',
        EndDate: this.getDateConverter(value.EndDate),
        EndTime: '1900-01-01T' + value.EndTime + ':00',
        WeekType: value.WeekType,
        EmpId: (this.selectedEmp === null) ? 0 : this.selectedEmp.Id,
        // ResourcesName: this.selectedResource.Name,
        // ResourceId: (this.additionalResource === null) ? 0 : this.additionalResource.Id,
        ResourceId: value.ResourcesId,
        CreatedUser: this._user,
        LastModifiedUser: this._user,
        DayList: this.dayList,
        InstructorIdList: this.additionalResource,
        Occurrence: 'WEEKLY',
        UpdateStatus: value.UpdateStatus,
        Id: (value.UpdateStatus === 1) ? value.Id : -1,
        Day: (value.UpdateStatus === 1) ? value.Day : ''
      }
      const scheduleItemListObj = {
        scheduleItem: addedscheduleItem,
        resId: this.selectedResource.Id,
        branchId: this.branchId
      }

      if (this.existingEntry) {
        let messageTitle = '';
        let messageBody = '';
        (this.translateService.get('Exceline').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
        (this.translateService.get('ADMIN.ExistScheduledItem').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
        this.msgBoxModal = this._exceMessageService.openMessageBox('ERROR',
          {
            messageTitle: messageTitle,
            messageBody: messageBody,
            msgBoxId: 'VALIDATE_EXIST'
          });
      } else {
        if (validity) {
          this.adminService.SaveScheduleItem(scheduleItemListObj).pipe(takeUntil(this.destroy$)).subscribe(obj => {
            if (obj) {
              addedscheduleItem.Id = obj.Data;
              this.selectedResource.Schedule.SheduleItemList.push(addedscheduleItem);
              this.resetForm();
              this.newScheduleView.close();
              this.getScheduleData();
              this.dayList = [];
            }
          }, null, null);
        }
      }
    } else {
      UsErrorService.validateAllFormFields(this.scheduleForm, this.formErrors, this.validationMessages);
    }
  }

  // check whether existing date
  dateValidator(value) {
    if (value.IsMonday) {
      this.timeValidator('Monday', value.StartTime, value.EndTime)
    } if (value.IsTuesday) {
      this.timeValidator('Tuesday', value.StartTime, value.EndTime)
    }
    if (value.IsWednesday) {
      this.timeValidator('Wednesday', value.StartTime, value.EndTime)
    }
    if (value.IsThursday) {
      this.timeValidator('Thursday', value.StartTime, value.EndTime)
    }
    if (value.IsFriday) {
      this.timeValidator('Friday', value.StartTime, value.EndTime)
    }
    if (value.IsSaturday) {
      this.timeValidator('Saturday', value.StartTime, value.EndTime)
    }
    if (value.IsSunday) {
      this.timeValidator('Sunday', value.StartTime, value.EndTime)
    }
  }

  timeValidator(day, fromTime, toTime) {
    this.selectedResource.Schedule.SheduleItemList.forEach(element => {
      if (element.Day === day) {
        const existingFromTime = this.getTime(element.StartTime);
        const existingToTime = this.getTime(element.EndTime);
        const newFromTime = fromTime + ':00'
        const newToTime = toTime + ':00'

        if (!((Date.parse('01/01/2017 ' + newFromTime) > Date.parse('01/01/2017 ' + existingToTime))
          || (Date.parse('01/01/2017 ' + existingFromTime) > Date.parse('01/01/2017 ' + newToTime)))) {

          this.existingEntry = true;
        }
        // return true;
      }
    });
  }

  getTime(dateTimeObje: string) {
    return dateTimeObje.split('T')[1]
  }

  // convert date js object in to string
  getDateConverter(datee: any) {
    if(datee){
    const dd = (datee.date.day < 10 ? '0' : '') + datee.date.day;
    const MM = ((datee.date.month + 1) < 10 ? '0' : '') + datee.date.month;
    const yyyy = datee.date.year;
    return (yyyy + '-' + MM + '-' + dd + 'T00:00:00');
    }else {
      return null;
    }
  }

  selectDay(event) {
    if (event.target.checked) {
      this.dayList.push(event.target.value)
    } else {
      this.dayList = this.dayList.filter(item => item !== event.target.value)
    }
  }

  employeeSelectionModal(content: any) {
    this.translateService.get('MEMBERSHIP.SelectEmployee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
    this.isDbPagination = true;
    this.entitySelectionType = EntitySelectionType[EntitySelectionType.EMP];
    this.columns = [
      { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'CreatedDate', header: 'Introduce Date', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];
    this.getGymEmployees('');
    this.employeeSelectionView = this.modalService.open(content, { width: '990' });
  }

  getGymEmployees(searchText) {
    this.adminService.GetGymEmployees(this.branchId, searchText, true, -1).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.items = result.Data;
          this.items.forEach(date => {
            date.CreatedDate = date.CreatedDate === '0001-01-01T00:00:00' ? ' ' : date.CreatedDate;
          });
          this.itemCount = result.Data.length;
        } else {
        }
      });
  }

  closeEmployeeSelectionView() {
    this.employeeSelectionView.close();
  }

  selectedRowItems(event) {
    this.scheduleForm.controls['EmpId'].setValue(event.Id);
  }

  // Add employees to the EmpName field
  selectedEmployee(item: any) {
    this.selectedEmp = item;
    if (item) {
      this.scheduleForm.controls['EmpName'].setValue(item.Name);
      this.scheduleForm.controls['EmpId'].setValue(item.Id);
    }
    this.closeEmployeeSelectionView();
  }

  // Search an employee from employee selection
  searchEmployee(val: any): void {
    this.getGymEmployees(val.searchVal);
  }

  openEntitySelectionView(content: any) {
    this.entitySelectionView = this.modalService.open(content, { width: '990' });
  }

  searchDetail(val: any) {
    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.RES]:
        this.adminService.GetResources(this.branchId, val.searchVal, -1, -1, 2, true
        ).pipe(takeUntil(this.destroy$)).subscribe
          (result => {
            this.entityItems = result.Data;
            this.entityItemCount = result.Data.length;
          }, err => {
          });
        break;
    }
  }

  selectResource(resource) {
    this.additionalResource = resource
    this.entitySelectionView.close();
    if (resource) {
      this.scheduleForm.controls['ResourcesName'].setValue(this.additionalResource.Name);
      this.scheduleForm.controls['ResourcesId'].setValue(this.additionalResource.Id);
    }
  }

  getResourceScheduleItems() {
    this.adminService.GetResourceScheduleItems(this.selectedResource.Id, '').pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.selectedResource.Schedule = result.Data;
        this.scheduleItemCount = Number(this.selectedResource.Schedule.SheduleItemList.length);
        this.selectedResource.Schedule.SheduleItemList.forEach(item => {
          item.isSelected = false;
        });
        if (this.selectedResource.Schedule.SheduleItemList.length > 0) {
          this.selectedResource.Schedule.SheduleItemList[0].isSelected = true
        }

        this.selectedResource.Schedule.SheduleItemList.forEach(element => {
          element.EndDate = (element.EndDate === '9999-12-31T00:00:00') ? null : element.EndDate;
        });

        this.itemCount = Number(this.selectedResource.Schedule.SheduleItemList.length);
      }
    });
  }

  resetForm() {
    this.scheduleForm.reset();
  }

}
