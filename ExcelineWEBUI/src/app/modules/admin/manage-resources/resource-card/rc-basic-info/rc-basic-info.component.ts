import { Component, OnInit, OnDestroy } from '@angular/core';
import { RcBasicInfoService } from './rc-basic-info.service';
import { Dictionary } from 'lodash';
import { ExceLoginService } from '../../../../login/exce-login/exce-login.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import { AdminService } from '../../../services/admin.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { TranslateService } from '@ngx-translate/core';
import { EntitySelectionType, ColumnDataType, MemberSearchType, MemberRole } from '../../../../../shared/enums/us-enum';
import { invalid } from 'moment';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'app-rc-basic-info',
  templateUrl: './rc-basic-info.component.html',
  styleUrls: ['./rc-basic-info.component.scss']
})
export class RcBasicInfoComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId: number;
  private entitySelectionViewTitle: string;
  mDate: any;
  pDate: any;
  updateResValues: any;
  IsValidate: boolean;
  msgBoxModal: any;
  subGetArticles: any;
  currentArticle: any;
  IsDelBtnVisible = false;
  defaultRole = 'ALL';
  subUpdateRes: any;
  subGetEditMode: any;
  subCurrentRes: any;
  selectedResMDate: any;
  selectedResPDate: any;
  selectedRes: any;
  selectedEmpList = [];
  objectKeys = Object.keys;
  isEditMode: boolean;
  public EditResInfoForm: FormGroup;
  myFormSubmited = false;
  categoryTypes = [];
  timeCategory = [];
  activityTypes = [];
  selectedActivity: any;
  articleList?: any[] = [];
  itemResource: DataTableResource<any>;
  itemCount = 0;
  articles = [];
  articleSelectionView: any;
  employeesList?: any[] = [];
  employeeSelectionView: any;
  isDbPagination = true;
  entitySelectionType: string;
  columns: any;
  items = [];
  articleSettingId: any;
  selectedArticle: any;
  activityVal = ''
  auotocompleteItemsForEmpSelection = [{ id: 'NAME', name: 'Name :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }];

  fromDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 }
  };

  toDatePickerOptions: IMyDpOptions = {
  };

  formErrors = {
    'Name': ''
  };

  validationMessages = {
    'Name': {
      'required': 'ADMIN.Nameisrequired'
    }
  };

  constructor(
    private _exceMessageService: ExceMessageService,
    private translateService: TranslateService,
    private modalService: UsbModal,
    private adminService: AdminService,
    private _fb: FormBuilder,
    private exceLoginService: ExceLoginService,
    private resourceBasicInfoService: RcBasicInfoService
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe((branch) => {
        this.branchId = branch.BranchId;
      });

    this.subCurrentRes = this.resourceBasicInfoService.resourceSource.pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.selectedRes = result;
      }
    });

    this.EditResInfoForm = this._fb.group({
      'Name': [null, [Validators.required]],
      'PurchasedDate': [null],
      'MaintenanceDate': [null],
      'IsEquipment': [null],
      'ActiveStatus': [null],
      'Description': [null],
      'ResourceCategory': [null],
      'TimeCategory': [null],
      'IsSalaryPerBooking': [null],
      'ActivityId': [null],
      'ArticleName': [null],
      'ArticleId': [null],
      'EmpList': [null]
    });
  }

  ngOnInit() {
    this.EditResInfoForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => {
      UsErrorService.onValueChanged(this.EditResInfoForm, this.formErrors, this.validationMessages)
    });

    this.EditResInfoForm.controls['ActivityId'].valueChanges.pipe(takeUntil(this.destroy$)).subscribe(res => {
      this.selectedActivity = res;
    });

    this.getCategories();
    this.getCategoriesByType();
    this.getActivities();
    this.selectedRes = this.resourceBasicInfoService.getSelectedResource();
    if (this.selectedRes) {
      this.selectedRes.EquipmentText = this.selectedRes.IsEquipment ? 'YES' : 'NO';
      this.selectedActivity = this.selectedRes.ActivityId;
      this.selectedEmpList = this.selectedRes.EmpList;
    }
    this.subGetEditMode = this.resourceBasicInfoService.getEditMode().pipe(
      takeUntil(this.destroy$)
      ).subscribe(mode => {
      this.isEditMode = mode;
      if (this.isEditMode) {
        this.resourceBasicInfoService.setEditTitle(this.isEditMode);
        this.EditResInfoForm.controls['ResourceCategory'].setValue(this.selectedRes.ResourceCategory);
        this.mDate = new Date(this.selectedRes.MaintenanceDate);
        this.pDate = new Date(this.selectedRes.PurchasedDate);
        this.EditResInfoForm.patchValue({
          Name: this.selectedRes.Name,
          PurchasedDate: {
            date: {
              year: this.pDate.getFullYear(),
              month: this.pDate.getMonth() + 1,
              day: this.pDate.getDate()
            }
          },
          MaintenanceDate: {
            date: {
              year: this.mDate.getFullYear(),
              month: this.mDate.getMonth() + 1,
              day: this.mDate.getDate()
            }
          },
          IsEquipment: this.selectedRes.EquipmentId,
          ActiveStatus: this.selectedRes.ActiveStatus,
          Description: this.selectedRes.Description,
          ResourceCategory: this.selectedRes.ResourceCategory.Id,
          TimeCategory: this.selectedRes.TimeCategory,
          IsSalaryPerBooking: this.selectedRes.IsSalaryPerBooking,
          ActivityId: this.selectedRes.ActivityId,
          ArticleName: this.selectedRes.ArticleName,
          ArticleId: this.selectedRes.ArticleId,
          EmpList: this.selectedEmpList
        });
        this.activityVal = this.selectedRes.ArticleName
        this.selectedActivity = this.selectedRes.ActivityId;
        if (this.selectedRes.MaintenanceDate === null || this.selectedRes.MaintenanceDate === '1900-01-01T00:00:00') {
          this.EditResInfoForm.controls['MaintenanceDate'].setValue('');
        }
        if (this.selectedRes.PurchasedDate === null || this.selectedRes.PurchasedDate === '1900-01-01T00:00:00') {
          this.EditResInfoForm.controls['PurchasedDate'].setValue('');
        }
      }
    });
  }

  // convert date js object in to string
  getDateConverter(datee: any) {
    const dd = (datee.date.day < 10 ? '0' : '') + datee.date.day;
    const MM = ((datee.date.month + 1) < 10 ? '0' : '') + datee.date.month;
    const yyyy = datee.date.year;
    return (yyyy + '-' + MM + '-' + dd + 'T00:00:00');
  }

  cancelUpdate() {
    this.resourceBasicInfoService.setEditTitle(!this.isEditMode);
    this.resourceBasicInfoService.updateEditMode(false, 'CARD');
  }

  // get from date change
  fromDateChange(event, toDate) {
    this.toDatePickerOptions = {
      disableUntil: event.date
    };
    toDate.showSelector = true;
  }

  getCategories() {
    this.adminService.getCategories('RESOURCE').pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.categoryTypes = result.Data;
      }
    }, null, null);
  }

  getCategoriesByType() {
    this.adminService.getCategoriesByType('TIME').pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.timeCategory = result.Data;
      }
    }, null, null);
  }

  getActivities() {
    this.adminService.getActivities(this.branchId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        result.Data.forEach(element => {
          if (element.ActivitySetting.IsBookingActivity === true) {
            this.activityTypes.push(element);
          }
        });
      }
    }, null, null);
  }

  getArticleForResource(selectedActivity) {
    if (this.selectedActivity) {
      this.subGetArticles = this.adminService.GetArticleForResouce(this.selectedActivity).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.articleList = result.Data;
          this.itemResource = new DataTableResource(result.Data);
          this.itemResource.count().then(count => this.itemCount = count);
          this.articles = this.articleList;
          this.itemCount = Number(result.Data.length);
        }
      });
    }
  }

  employeeSelectionModal(content: any) {
    this.translateService.get('MEMBERSHIP.SelectEmployee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
    this.isDbPagination = true;
    this.entitySelectionType = EntitySelectionType[EntitySelectionType.EMP];
    this.columns = [
      { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'CreatedDate', header: 'Introduce Date', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];
    this.getGymEmployees('');
    this.employeeSelectionView = this.modalService.open(content, { width: '990' });
  }

  articleSelectionModal(content: any) {
    if (this.selectedActivity) {
      this.getArticleForResource(this.selectedActivity);
      this.articleSelectionView = this.modalService.open(content, { width: '800' });
    }
  }

  getGymEmployees(searchText) {
    this.adminService.GetGymEmployees(this.branchId, searchText, true, -1).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.items = result.Data;
          this.items.forEach(date => {
            date.CreatedDate = date.CreatedDate === '0001-01-01T00:00:00' ? ' ' : date.CreatedDate;
          });
          this.itemCount = result.Data.length;
        } else {
        }
      });
  }

  // Search an employee from employee selection
  searchEmployee(val: any): void {
    this.getGymEmployees(val.searchVal);
  }

  selectedRowItems(event) {
  }

  // Add employees to the table
  selectedEmployee(item: any) {
    this.selectedEmpList[item.Id] = item.Name;
    this.closeEmployeeSelectionView();
  }

  closeEmployeeSelectionView() {
    this.employeeSelectionView.close();
  }

  // update Resource Details
  updateResource(value) {
    this.updateResValues = value;

    if (value.Name === '') {
      this.EditResInfoForm.controls['Name'].setErrors({ 'required': true });
    }
    this.myFormSubmited = true;

    if (this.EditResInfoForm.valid) {
      if (value.ActivityId > 0 && value.ArticleId > 0) {
        this.adminService.ValidateArticleByActivityId(value.ActivityId, value.ArticleId).pipe(takeUntil(this.destroy$)).subscribe(val => {
          this.IsValidate = val.Data;
          if (this.IsValidate === false) {
            let messageTitle = '';
            let messageBody = '';
            (this.translateService.get('Exceline').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
            (this.translateService.get('ADMIN.CheckActivityAndArticle').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
            this.msgBoxModal = this._exceMessageService.openMessageBox('ERROR',
              {
                messageTitle: messageTitle,
                messageBody: messageBody,
                msgBoxId: 'VALIDATE'
              });
          } else {
            this.SaveResourceEvent(this.updateResValues);
          }
        });
      } else {
        this.SaveResourceEvent(this.updateResValues);
      }
    } else {
      UsErrorService.validateAllFormFields(this.EditResInfoForm, this.formErrors, this.validationMessages);
    }
  }

  SaveResourceEvent(value) {
    if (this.EditResInfoForm.value.MaintenanceDate === '') {
      value.MaintenanceDate = null;
    }
    if (this.EditResInfoForm.value.PurchasedDate === '') {
      value.PurchasedDate = null;
    }
    value.ResourceCategory = this.categoryTypes.find(x => x.Id == value.ResourceCategory);
    value.ArticleName = (value.ArticleName === undefined) ? '' : value.ArticleName;
    if (value.PurchasedDate) {
      if (value.PurchasedDate.formatted) {
        value.PurchasedDate = this.dateConverter(JSON.parse(JSON.stringify(value.PurchasedDate)));
      } else {
        value.PurchasedDate = this.getDateConverter(value.PurchasedDate);
      }
    }
    if (value.MaintenanceDate) {
      if (value.MaintenanceDate.formatted) {
        value.MaintenanceDate = this.dateConverter(JSON.parse(JSON.stringify(value.MaintenanceDate)));
      } else {
        value.MaintenanceDate = this.getDateConverter(value.MaintenanceDate);
      }
    }
    const resource = {
      Id: this.selectedRes.Id,
      BranchId: this.branchId,
      Name: value.Name,
      Description: value.Description === undefined ? '' : value.Description,
      EquipmentId: value.IsEquipment === true ? 1 : 0,
      ActiveStatus: value.ActiveStatus,
      ArticleId: value.ArticleId === undefined ? 0 : value.ArticleId,
      ArticleName: value.ArticleName,
      ArticleSettingId: this.articleSettingId === undefined ? 0 : this.articleSettingId,
      PurchasedDate: value.PurchasedDate,
      MaintenanceDate: value.MaintenanceDate,
      ResourceCategory: value.ResourceCategory,
      TimeCategory: value.TimeCategory,
      IsSalaryPerBooking: value.IsSalaryPerBooking,
      ActivityId: value.ActivityId,
      EmpList: value.EmpList
    }
    this.adminService.SaveResources(resource).pipe(takeUntil(this.destroy$)).subscribe(result => {
      this.selectedRes = resource;
      this.selectedRes.ResourceCategory.Name = resource.ResourceCategory.Name;
      this.selectedRes.EquipmentText = resource.EquipmentId === 1 ? 'YES' : 'NO';
      if (this.selectedRes.ActivityId) {
        this.selectedRes.ActivityName = this.activityTypes.find(x => x.Id == this.selectedRes.ActivityId).Name;
      }
      this.selectedRes.ArticleName = resource.ArticleName;
      this.resourceBasicInfoService.setSelectedResource(this.selectedRes);
      this.resourceBasicInfoService.setEditTitle(!this.isEditMode);
      this.resourceBasicInfoService.updateEditMode(false, 'CARD');
      this.resourceBasicInfoService.setTitle(resource.Name);
    });
  }

  // convert date in to string
  dateConverter(dateObj: any) {
    let dateS = '';
    if (dateObj) {
      if (dateObj.formatted) {
        const splited = dateObj.formatted.split('/')
        if (splited.length > 1) {
          dateS = splited[2] + '-' + splited[1] + '-' + splited[0] + 'T00:00:00'
        }
      }
    }
    return dateS
  }

  // select Article by double click
  rowDoubleClickSelectArticle(rowEvent: any) {
    if (rowEvent.row.item !== undefined) {
      // Article Id
      // this.articleId = rowEvent.row.item.Id;
      this.EditResInfoForm.controls['ArticleId'].setValue(rowEvent.row.item.Id);
      // Article ArticleSettingId
      this.articleSettingId = rowEvent.row.item.ArticleSettingId;
      this.activityVal = rowEvent.row.item.Description
      this.EditResInfoForm.controls['ArticleName'].setValue(rowEvent.row.item.Description);
      this.articleSelectionView.close();
    }
  }

  // remove an employee from the table
  removeArticle() {
    this.EditResInfoForm.controls['ArticleId'].setValue(0);
    this.EditResInfoForm.controls['ArticleName'].setValue('');
    this.activityVal = '';
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.employeeSelectionView) {
      this.employeeSelectionView.close();
    }
    if (this.articleSelectionView) {
      this.articleSelectionView.close();
    }
  }

}
