import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { AdminService } from '../../services/admin.service';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { TranslateService } from '@ngx-translate/core';
import { ExceMemberService } from 'app/modules/membership/services/exce-member.service';
import { EntitySelectionType, ColumnDataType, MemberSearchType, MemberRole } from '../../../../shared/enums/us-enum';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import { Dictionary } from 'lodash';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RcBasicInfoService } from '../resource-card/rc-basic-info/rc-basic-info.service';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'app-resource-home',
  templateUrl: './resource-home.component.html',
  styleUrls: ['./resource-home.component.scss']
})
export class ResourceHomeComponent implements OnInit, OnDestroy {
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private _newResourceView: any;
  private searchText: string;
  private branchId: number;
  private popOver: any;
  private entitySelectionViewTitle: string;
  private destroy$ = new Subject<void>();
  IsActivityValidate = false;
  CreatedUser: any;
  articleSettingId: any;
  articleId: any;
  equipmentId = 2;
  activityId = -1;
  categoryId = -1;
  IsHasEmp: boolean;
  IsViewEmployees: boolean;
  activeStatus = true;
  resourceCategoryName = [];
  categoryTypes = [];
  activityTypes = [];
  timeCategory = [];
  items = [];
  itemCount = 0;
  resourcesList?: any[] = [];
  itemResource: DataTableResource<any>;
  searchForm: FormGroup;
  objectKeys = Object.keys;
  empList = [];
  defaultRole = 'ALL'
  isDbPagination = true;
  entitySelectionType: string;
  entitySearchType: string;
  columns: any;
  employeeSelectionView: any;
  articleSelectionView: any;
  entityItems = [];
  employeesList?: any[] = [];
  articleList?: any[] = [];
  articles = [];
  selectedArticle: any;
  selectedActivity: any;
  val: number;
  myFormSubmited = false;
  fromDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 }
  };
  toDatePickerOptions: IMyDpOptions = {
  };

  public NewResourceForm: FormGroup;
  auotocompleteItemsForEmpSelection = [{ id: 'NAME', name: 'Name :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }];
  auotocompleteItems = [{ id: 'Name', name: 'Name :', isNumber: false }];
  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };
  @ViewChild('autobox', { static: true }) public autocomplete;

  formErrors = {
    'Name': '',
    'ResourceCategory': '',
    'ActivityId': ''
  };

  validationMessages = {
    'Name': {
      'required': 'ADMIN.Nameisrequired'
    },
    'ResourceCategory': {
      'required': 'ADMIN.CategoryIsRequired'
    },
    'ActivityId': {
      'required': 'ADMIN.MsgActivityNeeded'
    }
  };

  constructor(
    private modalService: UsbModal,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private _fb: FormBuilder,
    private translateService: TranslateService,
    private exceMemberService: ExceMemberService,
    private router: Router,
    private resourceBasicInfoService: RcBasicInfoService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.AdministrateResourcesC',
      url: '/adminstrator/resource-home'
    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      });

    this.NewResourceForm = this._fb.group({
      'Name': [null],
      'Description': [null],
      'ResourceCategory': [null],
      'IsEquipment': [null],
      'TimeCategory': [null],
      'IsSalaryPerBooking': [null],
      'ActivityId': [null],
      'PurchasedDate': [null],
      'MaintenanceDate': [null],
      'EmpList': [null],
      'ArticleName': [this.selectedArticle],
      'ActiveStatus': [{ checked: true }],
    });
  }

  ngOnInit() {
    this.NewResourceForm.controls['ActivityId'].valueChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(data => {
      this.selectedActivity = data;
    });
    this.NewResourceForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => {
      UsErrorService.onValueChanged(this.NewResourceForm, this.formErrors, this.validationMessages)
    });
    this.getResource('', this.categoryId, this.activityId, this.equipmentId, this.activeStatus);
    this.autocomplete.items = this.auotocompleteItems;
    this.autocomplete.setSearchValue(this.auotocompleteItems);
    this.getCategories();
    this.getActivities();
    this.getCategoriesByType();
    this.getCategoriesByType();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this._newResourceView) {
      this._newResourceView.close();
    }
    if (this.employeeSelectionView) {
      this.employeeSelectionView.close();
    }
    if (this.articleSelectionView) {
      this.articleSelectionView.close();
    }
  }

  // Equipment checkbox change event
  equipmentChange(event) {
  }

  // get from date change
  fromDateChange(event, toDate) {
    this.toDatePickerOptions = {
      disableUntil: event.date
    };
    toDate.showSelector = true;
  }

  getCategories() {
    this.adminService.getCategories('RESOURCE').pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.categoryTypes = result.Data;
      }
    }, null, null);
  }

  getActivities() {
    this.adminService.getActivities(this.branchId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        result.Data.forEach(element => {
          if (element.ActivitySetting.IsBookingActivity === true) {
            this.activityTypes.push(element);
          }
        });
      }
    }, null, null);
  }

  getArticleForResource(selectedActivity) {
    if (this.selectedActivity) {
      this.adminService.GetArticleForResouce(this.selectedActivity).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result) {
          this.articleList = result.Data;
          this.itemResource = new DataTableResource(result.Data);
          this.itemResource.count().then(count => this.itemCount = count);
          this.articles = this.articleList;
          this.itemCount = Number(result.Data.length);
        }
      });
    }
  }

  // select Article by double click
  rowDoubleClickSelectArticle(rowEvent: any) {
    if (rowEvent.row.item !== undefined) {
      // Article Id
      this.articleId = rowEvent.row.item.Id;
      // Article ArticleSettingId
      this.articleSettingId = rowEvent.row.item.ArticleSettingId;
      this.selectedArticle = rowEvent.row.item.Description;
      this.articleSelectionView.close();
    }
  }

  getCategoriesByType() {
    this.adminService.getCategoriesByType('TIME').pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.timeCategory = result.Data;
      }
    }, null, null);
  }

  getResource(searchText: string, categoryId: number, activityId: number, equipmentId: number, isActive: boolean) {
    this.adminService.GetResources(this.branchId, searchText, categoryId, activityId, equipmentId, isActive).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.items = result.Data;
        this.items.forEach(ele => {
          ele.EquipmentText = ele.IsEquipment ? 'YES' : 'NO';
          this.equipmentId = ele.EquipmentId;
        });
        this.resourcesList = this.items;
        this.itemResource = new DataTableResource(this.resourcesList);
        this.itemResource.count().then(count => this.itemCount = count);
        this.itemCount = Number(result.Data.length);
      }
    }, null, null);
  }

  // auto complete
  resourceSearch() {
    const selectedSearchItem = this.autocomplete.getTextValue();
    if (selectedSearchItem.searchText) {
      this.getResource(selectedSearchItem.searchText, this.categoryId, this.activityId, this.equipmentId, this.activeStatus);
    } else {
      this.getResource('', this.categoryId, this.activityId, this.equipmentId, this.activeStatus);
    }
  }

  // for autocomplete
  onSelect(item: any) {
    this.searchText = item.searchText;
    this.getResource(item.searchText, this.categoryId, this.activityId, this.equipmentId, this.activeStatus);
  }

  closeSearch() {
    this.autocomplete.textValue = '';
    this.getResource('', this.categoryId, this.activityId, this.equipmentId, this.activeStatus);
  }

  changeActiveStatus(event) {
    if (event.target.checked) {
      this.activeStatus = event.target.value;
      const selectedSearchItem = this.autocomplete.getTextValue();
      if (selectedSearchItem.searchText) {
        this.getResource(selectedSearchItem.searchText, this.categoryId, this.activityId, this.equipmentId, this.activeStatus);
      } else {
        this.getResource('', this.categoryId, this.activityId, this.equipmentId, this.activeStatus);
      }
    }
  }

  changeEquipmentStatus(event) {
    this.equipmentId = event.target.checked ? 1 : 0;
    const selectedSearchItem = this.autocomplete.getTextValue();
    if (selectedSearchItem.searchText) {
      this.getResource(selectedSearchItem.searchText, this.categoryId, this.activityId, this.equipmentId, this.activeStatus);
    } else {
      this.getResource('', this.categoryId, this.activityId, this.equipmentId, this.activeStatus);
    }
  }

  // filter by name (search field)
  filterInput(param?: any) {
    this.resourcesList = this.filterpipe.transform('Name', 'NAMEFILTER', this.items, param);
  }

  // filter by Category type
  filterInputByCategory(catValue?: any) {
    if (catValue === 'ALL') {
      this.getResource('', this.categoryId, this.activityId, this.equipmentId, this.activeStatus);
    } else {
      this.getResource('', catValue, this.activityId, this.equipmentId, this.activeStatus);
    }
  }

  // filter by Activity type
  filterInputByActivity(actValue?: any) {
    if (actValue === 'ALL') {
      this.getResource('', this.categoryId, this.activityId, this.equipmentId, this.activeStatus);
    } else {
      this.getResource('', this.categoryId, actValue, this.equipmentId, this.activeStatus);
    }
  }

  // Open Add resource view
  openNewResource(content: any) {
    this.NewResourceForm.reset({ 'ActiveStatus': true });
    this.employeesList = [];
    this._newResourceView = this.modalService.open(content, { width: '900' });
  }

  // double click resource
  rowDoubleClick(event) {
    this.resourceBasicInfoService.setSelectedResource(event.row.item);

    const url = '/adminstrator/resource-card/' + event.row.item.Id;
    this.router.navigate([url]);
  }

  // for view employees
  viewLoad(rowItem, popOver) {
    if (this.popOver) {
      this.popOver.close();
    }
    this.popOver = popOver;
    this.popOver.open();
    this.empList = [];
    this.empList.push(rowItem.EmpList);
  }

  closePopOver() {
    this.popOver.close();
  }

  employeeSelectionModal(content: any) {
    this.translateService.get('MEMBERSHIP.SelectEmployee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
    this.isDbPagination = true;
    this.entitySelectionType = EntitySelectionType[EntitySelectionType.EMP];
    this.columns = [
      { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'CreatedDate', header: 'Introduce Date', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];
    this.getGymEmployees('');
    this.employeeSelectionView = this.modalService.open(content, { width: '990' });
  }

  articleSelectionModal(content: any) {
    if (this.selectedActivity) {
      this.getArticleForResource(this.selectedActivity);
      this.articleSelectionView = this.modalService.open(content, { width: '800' });
    }
  }

  getGymEmployees(searchText) {
    this.adminService.GetGymEmployees(this.branchId, searchText, true, -1).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.items = result.Data;
          this.items.forEach(date => {
            date.CreatedDate = date.CreatedDate === '0001-01-01T00:00:00' ? ' ' : date.CreatedDate;
          });
          this.itemCount = result.Data.length;
        }
      });
  }

  closeEmployeeSelectionView() {
    this.employeeSelectionView.close();
  }

  // Search an employee from employee selection
  searchEmployee(val: any): void {
    this.getGymEmployees(val.searchVal);
  }

  // Add employees to the table
  selectedEmployee(item: any) {

    if (item) {
      this.employeesList.push(item);
    }
    this.closeEmployeeSelectionView();
  }

  // remove an employee from the table
  removeEmployee(employee: any) {
    const index: number = this.employeesList.indexOf(employee);
    if (index !== -1) {
      this.employeesList.splice(index, 1);
    }
  }

  selectedRowItems(event) {
  }

  saveResource(value) {

    if (value.Name === null) {
      this.NewResourceForm.controls['Name'].setErrors({ 'required': true });
    }

    if (value.ResourceCategory === null) {
      this.NewResourceForm.controls['ResourceCategory'].setErrors({ 'required': true });
    }


    if (value.IsEquipment === true && this.selectedActivity == null) {
      this.NewResourceForm.controls['ActivityId'].setErrors({ 'required': true });
    } else {
      this.NewResourceForm.controls['ActivityId'].setErrors(null);
    }

    this.myFormSubmited = true;
    const EmpList: Dictionary<string> = {};
    for (const emp of this.employeesList) {
      EmpList[emp.Id + ''] = emp.Name;
    }

    if (value.PurchasedDate === '') {
      value.PurchasedDate = null;
    }
    if (value.MaintenanceDate === '') {
      value.MaintenanceDate = null;
    }

    const resource = {
      BranchId: this.branchId,
      Name: value.Name,
      Description: value.Description,
      EquipmentId: value.IsEquipment === true ? 1 : 0,
      ActiveStatus: value.ActiveStatus,
      ArticleId: this.articleId,
      ArticleSettingId: this.articleSettingId,
      PurchasedDate: this.dateConverter(value.PurchasedDate),
      MaintenanceDate: this.dateConverter(value.MaintenanceDate),
      ResourceCategory: value.ResourceCategory,
      TimeCategory: value.TimeCategory,
      IsSalaryPerBooking: value.IsSalaryPerBooking,
      ActivityId: value.ActivityId,
      EmpCount: this.employeesList.length,
      EmpList: EmpList
    }

    if (this.NewResourceForm.valid) {
      this.adminService.SaveResources(resource).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          const currentResourceId = result.Data;
          this._newResourceView.close();
          if (resource.ActivityId === null) {
            resource.ActivityId = -1;
          }
          if (resource.EquipmentId === 0) {
            resource.EquipmentId = 2;
          }
          // call to the db and get the just saved member and load the resource card
          this.adminService.GetResources(this.branchId, resource.Name, resource.ResourceCategory.Id, resource.ActivityId, resource.EquipmentId, resource.ActiveStatus).pipe(
            takeUntil(this.destroy$)
            ).subscribe(res => {
              res.Data.forEach(element => {
                if (currentResourceId === element.Id) {
                  const url = '/adminstrator/resource-card/' + currentResourceId;
                  this.router.navigate([url]);
                }
              });
            });
        }
      });
    } else {
      UsErrorService.validateAllFormFields(this.NewResourceForm, this.formErrors, this.validationMessages);
    }
  }

  // convert date in to string
  dateConverter(dateObj: any) {
    let dateS = '';
    if (dateObj) {
      if (dateObj.formatted) {
        const splited = dateObj.formatted.split('/')
        if (splited.length > 1) {
          dateS = splited[2] + '-' + splited[1] + '-' + splited[0] + 'T00:00:00'
        }
      }
    }
    return dateS
  }

  goToResourceBooking() {
    const url = 'adminstrator/my-calandar-home'
    this.router.navigate([url]);
  }

}

