import { Component, OnInit } from '@angular/core';
import { ExceClassService } from 'app/modules/class/services/exce-class.service';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { DataTableResource } from 'app/shared/components/us-data-table/tools/data-table-resource';
import * as _ from 'lodash';
import * as moment from 'moment';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { UsbModalRef } from 'app/shared/components/us-modal/modal-ref';
import { OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { PreloaderService } from 'app/shared/services/preloader.service';
import { ManageSeasonsService } from 'app/modules/admin/manage-seasons/manage-seasons.service';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import { takeUntil, take, mergeMap } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
const now = new Date();

@Component({
  selector: 'manage-seasons',
  templateUrl: './manage-seasons.component.html',
  styleUrls: ['./manage-seasons.component.scss']
})
export class ManageSeasonsComponent implements OnInit, OnDestroy {
  savingMode: string;
  IsCopyClass: boolean;
  IsClassDeleted: any;
  selectedDeleteingSeason: any;
  deleteConfModel: void;
  toDateinvalid: boolean;
  fromDateInvalid: boolean;
  isDateRangeValid: boolean;
  dateTimes: any[];
  isCopySheduleItem: boolean;
  selectedSeason: any;
  ListOfSeasonDates: any[] = [];

  today: { year: number; month: number; day: number; };
  addSeasonForm: FormGroup;

  addSeasonModel: UsbModalRef;
  itemResource: DataTableResource<any>;
  classTypeList: any[];
  classList: any[] = [];
  addSeasonFormSubmited = false;
  itemCount: number;
  maxDate: any;

  isDisplaySave: boolean;
  isDisplayDelete: boolean;
  isDisplayCopyOk: boolean;

  private destroy$ = new Subject<void>();

  formErrors = {
    'name': '',
    'fromDate': '',
    'toDate': ''
  };

  validationMessages = {
    'name': {
      'required': 'CLASS.NameReq'
    },
    'fromDate': {
      'required': 'CLASS.StartDateReq',
      'fromDateRangeInvalid': 'CLASS.DateRangeInvalid'
    },
    'toDate': {
      'required': 'CLASS.EndDateReq',
      'toDateInvalid': 'CLASS.ToDateInvalid',
      'dateRangeInvalid': 'CLASS.DateRangeInvalid',
      'startDateLowerThanMinClassesStartDate': 'CLASS.StartDateLowerThanMinClassesStartDate'

    }
  };

  private fromDatePickerOptions: IMyDpOptions = {
    showClearDateBtn: false,
    disableUntil: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() },
  };

  private toDatePickerOptions: IMyDpOptions = {
    showClearDateBtn: false,
    disableUntil: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }
  };
  scheduleItems = [];

  constructor(
    private exceClassService: ExceClassService,
    private modalService: UsbModal,
    private router: Router,
    private fb: FormBuilder,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private manageSeasonsService: ManageSeasonsService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {

    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.SEASONAL',
      url: '/adminstrator/manage-seasons'
    });

    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.addSeasonForm = this.fb.group({
      name: [null, [Validators.required]],
      fromDate: [{ date: this.today }, [Validators.required], [this.fromDateRangeInvalidValidator.bind(this)]],
      toDate: [{ date: this.today }, [Validators.required], [this.toDateRangeInvalidValidator.bind(this)]]
    });

    this.isDisplaySave = false;
    this.isDisplayDelete = false;
    this.isDisplayCopyOk = false;

    this.exceMessageService.yesCalledHandle.pipe(
      mergeMap(value => this.exceClassService.DeleteSchedule(this.selectedDeleteingSeason)),
      take(1),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (this.addSeasonModel) {
        this.addSeasonModel.close()
      }
      this.getClasses();
    }, null, null);
  }

  ngOnInit() {
    this.exceClassService.GetClassTypes().pipe(
      takeUntil(this.destroy$)
    ).subscribe(classTypes => {
      this.classTypeList = classTypes.Data;
      if (this.classTypeList.length > 0) {
        this.getClasses();
      }
    }, null, null);

    this.addSeasonForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(__ => {
      UsErrorService.onValueChanged(this.addSeasonForm, this.formErrors, this.validationMessages)
    }, null, null);
  }

  private getClasses() {
    let counter = 0;
    PreloaderService.showPreLoader();
    this.exceClassService.GetClasses({ className: '' }).pipe(
      takeUntil(this.destroy$)
    ).subscribe(classes => {
      this.classList = classes.Data;
      this.classList.forEach(cls => {
        const scheduledates = {
          Number: counter,
          Id: cls.Schedule.Id,
          StartDate: { year: Number(moment(cls.Schedule.StartDate).format('YYYY')) ,
          month: Number(moment(cls.Schedule.StartDate).format('M')) , day:  Number(moment(cls.Schedule.StartDate).format('D'))} ,
          EndDate: { year: Number(moment(cls.Schedule.EndDate).format('YYYY')) , month: Number(moment(cls.Schedule.EndDate).format('M')) , day:  Number(moment(cls.Schedule.EndDate).format('D'))},
          IsCurrentSeason: ((cls.Schedule.StartDate < moment().format('YYYY-MM-DDTHH:mm:SS')) && ( moment().format('YYYY-MM-DDTHH:mm:SS') < cls.Schedule.EndDate )) ? true : false
        }
        this.ListOfSeasonDates.push(scheduledates)
        counter++;
        cls.ScheduleStartDate = cls.Schedule.StartDate;
        cls.ScheduleEndDate = cls.Schedule.EndDate;
      });
      this.classList = _.orderBy(this.classList, ['ScheduleStartDate'], ['desc']);
      this.manageSeasonsService.ClassList = this.classList;
      // this.classList = classes.Data;
      this.itemCount = this.classList.length;
      this.itemResource = new DataTableResource(this.classList);
      if (this.classList.length > 0) {
        this.maxDate = this.getNewScheduleStartDate(this.classList);
      } else {
        this.maxDate = moment();
      }
      const maxDateNext = moment(this.maxDate).add(1, 'days');
      this.addSeasonForm.patchValue({
        name: '',
        fromDate: { date: { year: this.maxDate.format('YYYY'), month: this.maxDate.format('M'), day: this.maxDate.format('D') } },
        toDate: { date: { year: maxDateNext.format('YYYY'), month: maxDateNext.format('M'), day: maxDateNext.format('D') } }
      });
      this.toDatePickerOptions = {
        showClearDateBtn: false,
        disableUntil: { year: this.maxDate.format('YYYY'), month: this.maxDate.format('M'), day: this.maxDate.format('D') }
      };
      PreloaderService.hidePreLoader();
    }, null, null);
  }

  reloadItems(params) {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.classList = items);
    }
  }

  openWeekViewCalandar(exceClass) {
    this.exceClassService.GetGymSettings({ gymSettingType: 'OTHER' }).pipe(
      takeUntil(this.destroy$)
    ).subscribe(res => {
      if (res.Data.length > 0) {
        if (exceClass.Schedule) {
          exceClass.IsCalendarPriority = res.Data[0].IsCalendarPriority;
          this.manageSeasonsService.SelectedSeason = exceClass;
          this.router.navigate(['adminstrator/manage-seasons/season-calandar/' + exceClass.Id
          ]);
        }
      }
    }, null, null);
  }

  openSessoionModal(item, content, type) {
    this.addSeasonFormSubmited = false;
    this.selectedSeason = item;
    this.fromDateInvalid = false;
    this.toDateinvalid = false;
    const lastSeason = this.ListOfSeasonDates[this.ListOfSeasonDates.length - 1];
    switch (type) {
      case 'NEW':
        this.toDateinvalid = false;
        this.fromDateInvalid = false;
        this.isDateRangeValid = false;
        const maxDateNext = moment(this.maxDate).add(1, 'days');
        const maxDatePrv = moment(this.maxDate).add(-1, 'days');
        this.fromDatePickerOptions = {
          showClearDateBtn: false,
          disableUntil: lastSeason.EndDate,
          disableSince: { year: Number(3000), month: Number(1), day: Number(1) }
        };
        this.toDatePickerOptions = {
          showClearDateBtn: false,
          disableUntil: lastSeason.EndDate,
          disableSince: { year: Number(3000), month: Number(1), day: Number(1) }
        };
        this.addSeasonForm.patchValue({
          name: '',
          fromDate: { date: { year: Number(this.maxDate.format('YYYY')), month: Number(this.maxDate.format('M')), day: Number(this.maxDate.format('D')) } },
          toDate: { date: { year: Number(maxDateNext.format('YYYY')), month: Number(maxDateNext.format('M')), day: Number(maxDateNext.format('D')) } }
        });
        this.toDatePickerOptions = {
          showClearDateBtn: false,
          disableUntil: { year: Number(this.maxDate.format('YYYY')), month: Number(this.maxDate.format('M')), day: Number(this.maxDate.format('D')) },
          // disableSince: { year: Number(maxDateNext.format('YYYY')), month: Number(maxDateNext.format('M')), day: Number(maxDateNext.format('D')) }
        };
        this.isDisplaySave = true;
        this.isDisplayDelete = false;
        this.isDisplayCopyOk = false;
        this.IsCopyClass = false;
        this.savingMode = null;
        break;
      case 'COPY':
        this.exceClassService.GetScheduleItemsByClass({classId: item.Id}).pipe(
          takeUntil(this.destroy$)
        ).subscribe(res => {
          this.scheduleItems = res.Data
        }, null, null);
        this.fromDatePickerOptions = {
          showClearDateBtn: false
        }
        const fromDtCopy = moment(this.selectedSeason.ScheduleStartDate);
        const toDateCopy = moment(this.selectedSeason.ScheduleEndDate);
        const duration = moment.duration(toDateCopy.diff(fromDtCopy));
        const days = duration.asDays();
        const toNewDate = moment(this.maxDate).add(Number(days), 'days');
        const maxDateNext1 = moment(this.maxDate).add(1, 'days');
        const maxDatePrv1 = moment(this.maxDate).add(-1, 'days');
        this.fromDatePickerOptions = {
          showClearDateBtn: false,
          disableUntil: { year: Number(maxDatePrv1.format('YYYY')), month: Number(maxDatePrv1.format('M')), day: Number(maxDatePrv1.format('D')) },
          disableSince: { year: Number(maxDateNext1.format('YYYY')), month: Number(maxDateNext1.format('M')), day: Number(maxDateNext1.format('D')) }
        };

        this.checkDateRangeValid(moment(this.maxDate), toNewDate);
        this.addSeasonForm.patchValue({
          name: this.selectedSeason.Name + '_' + this.maxDate.format('YYYY'),
          fromDate: { date: { year: Number(this.maxDate.format('YYYY')), month: Number(this.maxDate.format('M')), day: Number(this.maxDate.format('D')) } },
          toDate: { date: { year: toNewDate.format('YYYY'), month: toNewDate.format('M'), day: toNewDate.format('D') } }
        });
        this.IsCopyClass = true;

        this.isDisplaySave = false;
        this.isDisplayDelete = false;
        this.isDisplayCopyOk = true;
        this.isCopySheduleItem = true;
        this.savingMode = null;
        break;
      case 'DETAIL':
      const currentSeason = this.ListOfSeasonDates.find(x => x.IsCurrentSeason === true)
      const currentDay = new Date();
        if ( Number(this.selectedSeason.Schedule.Id) < Number(currentSeason.Id) ) {
        this.fromDatePickerOptions = {
          showClearDateBtn: true,
          disableUntil: { year: Number(3000), month: Number(1), day: Number(1) },
          disableSince: { year: Number(1900), month: Number(1), day: Number(1) }
        };
        this.toDatePickerOptions = {
          showClearDateBtn: false,
          disableUntil: { year: Number(3000), month: Number(1), day: Number(1) },
          disableSince: { year: Number(1900), month: Number(1), day: Number(1) }
        };
      }
        if (Number(this.selectedSeason.Schedule.Id) === Number(currentSeason.Id)) {
        this.fromDatePickerOptions = {
          showClearDateBtn: true,
          disableUntil: { year: Number(3000), month: Number(1), day: Number(1) },
          disableSince: { year: Number(1900), month: Number(1), day: Number(1) }
        };
        this.toDatePickerOptions = {
          showClearDateBtn: false,
          disableUntil: { year: currentDay.getFullYear(), month: currentDay.getMonth() + 1, day: currentDay.getDate() - 1},
          disableSince:
          this.ListOfSeasonDates.find(y => y.Number === (currentSeason.Number + 1)) === undefined ?
          { year: Number(3000), month: Number(1), day: Number(1) } : this.ListOfSeasonDates.find(y => y.Number === (currentSeason.Number + 1)).StartDate
        };
        }
        if (Number(this.selectedSeason.Schedule.Id) > Number(currentSeason.Id)) {
        const selSeason = this.ListOfSeasonDates.find(z => z.Id === this.selectedSeason.Schedule.Id)
        this.fromDatePickerOptions = {
          showClearDateBtn: true,
          disableUntil: this.ListOfSeasonDates.find(y => y.Number === (selSeason.Number - 1)).EndDate,
          disableSince: this.ListOfSeasonDates.find(y => y.Number === (selSeason.Number + 1)) === undefined ?
          { year: Number(3000), month: Number(1), day: Number(1) } : this.ListOfSeasonDates.find(y => y.Number === (selSeason.Number + 1)).StartDate
        };
        this.toDatePickerOptions = {
          showClearDateBtn: false,
          disableUntil: this.ListOfSeasonDates.find(y => y.Number === (selSeason.Number - 1)).EndDate,
          disableSince: this.ListOfSeasonDates.find(y => y.Number === (selSeason.Number + 1)) === undefined ?
          { year: Number(3000), month: Number(1), day: Number(1) } : this.ListOfSeasonDates.find(y => y.Number === (selSeason.Number + 1)).StartDate
        };
        }
        const fromDtDetail = moment(this.selectedSeason.ScheduleStartDate);
        const toDateDetail = moment(this.selectedSeason.ScheduleEndDate);
        const toDate = toDateDetail.toDate();
        const toDay = new Date();
        if (toDate < toDay) {
          const beforeDate = fromDtDetail.add(1, 'days');
          const afterDate = fromDtDetail.add(-1, 'days');
          const beforeDate2 = toDateDetail.add(1, 'days');
          const afterDate2 = toDateDetail.add(-1, 'days');
          this.checkDateRangeValid(fromDtDetail, toDateDetail);
          this.addSeasonForm.patchValue({
            name: this.selectedSeason.Name,
            fromDate: { date: { year: fromDtDetail.format('YYYY'), month: fromDtDetail.format('M'), day: fromDtDetail.format('D') } },
            toDate: { date: { year: toDateDetail.format('YYYY'), month: toDateDetail.format('M'), day: toDateDetail.format('D') } }
          });
          // disable oportunity to pick date - season is done
          this.fromDatePickerOptions = {
            showClearDateBtn: false,
            disableUntil: { year: Number(beforeDate.format('YYYY')), month: Number(beforeDate.format('M')), day: Number(beforeDate.format('D')) },
            disableSince: { year: Number(afterDate.format('YYYY')), month: Number(afterDate.format('M')), day: Number(afterDate.format('D')) }
          };
          this.toDatePickerOptions = {
            showClearDateBtn: false,
            disableUntil: { year: Number(beforeDate2.format('YYYY')), month: Number(beforeDate2.format('M')), day: Number(beforeDate2.format('D')) },
            disableSince: { year: Number(afterDate2.format('YYYY')), month: Number(afterDate2.format('M')), day: Number(afterDate2.format('D')) }
          };
        } else {
          const beforeDate = fromDtDetail.add(1, 'days');
          const afterDate = fromDtDetail.add(-1, 'days');
          this.checkDateRangeValid(fromDtDetail, toDateDetail);
          this.addSeasonForm.patchValue({
            name: this.selectedSeason.Name,
            fromDate: { date: { year: fromDtDetail.format('YYYY'), month: fromDtDetail.format('M'), day: fromDtDetail.format('D') } },
            toDate: { date: { year: toDateDetail.format('YYYY'), month: toDateDetail.format('M'), day: toDateDetail.format('D') } }
          });
          this.fromDatePickerOptions = {
            showClearDateBtn: false,
            disableUntil: { year: Number(beforeDate.format('YYYY')), month: Number(beforeDate.format('M')), day: Number(beforeDate.format('D')) },
            disableSince: { year: Number(afterDate.format('YYYY')), month: Number(afterDate.format('M')), day: Number(afterDate.format('D')) }
          };
          this.toDatePickerOptions = {
            showClearDateBtn: false,
            disableUntil: { year: Number(fromDtDetail.format('YYYY')), month: Number(fromDtDetail.format('M')), day: Number(fromDtDetail.format('D')) },
            // disableSince: { year: Number(maxDateNext.format('YYYY')), month: Number(maxDateNext.format('M')), day: Number(maxDateNext.format('D')) }
          };
        }

        this.isDisplaySave = true;
        this.isDisplayDelete = item.IsClassDeleted;
        this.IsClassDeleted = item.IsClassDeleted;
        this.IsCopyClass = false;
        this.isDisplayCopyOk = false;
        this.isCopySheduleItem = false;
        this.savingMode = 'Edit';
        break;
    }
    this.addSeasonModel = this.modalService.open(content, { width: '500' });

  }

  private geserateDateSubmitFormat(date): string {
    return date.date.year + '-' + date.date.month + '-' + date.date.day;
  }

  saveNewSeasons(value) {
    this.addSeasonFormSubmited = true;
    if (this.addSeasonForm.valid) {
      PreloaderService.showPreLoader();
      const body = {
        ExcelineClass: {
          Id: this.selectedSeason.Id,
          Name: value.name,
          IsClassDeleted: this.IsClassDeleted,
          ClassCategory: null,
          ClassType: null,
          Description: '',
          CreatedUser: this.exceLoginService.CurrentUser.username,
          ModifiedUser: this.exceLoginService.CurrentUser.username,
          Mode: this.savingMode,
          Schedule: {
            Id: (this.selectedSeason.Id === 0) ? 0 : this.selectedSeason.Schedule.Id,
            Occurrence: 'NONE',
            StartDate: this.geserateDateSubmitFormat(value.fromDate),
            EndDate: this.geserateDateSubmitFormat(value.toDate),
            SheduleItemList: (this.isCopySheduleItem) ? this.scheduleItems : null
          },
          IsCopyClass: this.IsCopyClass
        }
      };
      this.exceClassService.SaveClass(body).pipe(
        takeUntil(this.destroy$)
        ).subscribe(res => {
          const ccResult = res.Data.split(' ');
          if (ccResult.length > 0 && Number(ccResult[0])) {
            body.ExcelineClass.Id = Number(ccResult[0]);
          }
          if (ccResult.length > 1) {
            body.ExcelineClass.Schedule.Id = Number(ccResult[1]);
          }
          this.addSeasonModel.close();
          this.getClasses();
          PreloaderService.hidePreLoader();
        },
        err => {
          PreloaderService.hidePreLoader();
        }, null
      );
    } else {
      UsErrorService.validateAllFormFields(this.addSeasonForm, this.formErrors, this.validationMessages);
    }
  }

  onStartDateChange(date, fromDateController, toDateController) {
    const jsDate = date.jsdate;
    const nextJsDate = moment(jsDate).add(1, 'days');
    this.toDatePickerOptions = {
      disableUntil: date.date,
      showClearDateBtn: false
    };
    toDateController.showSelector = true;
    this.addSeasonForm.patchValue({
      toDate: { date: { year: nextJsDate.format('YYYY'), month: nextJsDate.format('M'), day: nextJsDate.format('D') } }
    });
    this.checkDateRangeValid(moment(jsDate), nextJsDate);
  }

  private getNewScheduleStartDate(classList) {
    this.dateTimes = [];
    classList.forEach(classItem => {
      const scheduleDateListTemp = [];
      scheduleDateListTemp.push(classItem.Schedule.StartDate);
      if (classItem.Schedule.EndDate) {
        scheduleDateListTemp.push(classItem.Schedule.EndDate);
      }
      this.dateTimes.push({ classItemId: classItem.Id, scheduleDateList: scheduleDateListTemp });
    });
    let maxEndDate = moment();
    this.dateTimes.forEach(dtItem => {
      const dt = moment(dtItem.scheduleDateList[1]);
      if (dt > maxEndDate) {
        maxEndDate = moment(dt).add(1, 'days');
      }
    });
    return maxEndDate;
  }

  fromDateRangeInvalidValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (this.fromDateInvalid) {
          resolve({
            'fromDateRangeInvalid': true
          })
        } else {
          resolve(null);
        }
      }, 100);
    });
  }

  toDateRangeInvalidValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (this.toDateinvalid) {
          resolve({
            'dateRangeInvalid': true
          })
        } else {
          resolve(null);
        }
      }, 100);
    });
  }

  checkDateRangeValid(fromDate, toDate) {
    this.isDateRangeValid = (fromDate < toDate) ? true : false;
    if (this.isDateRangeValid) {
      if (this.isCopySheduleItem) {
        this.dateTimes.forEach(item => {
          const stDate = item.scheduleDateList[0];
          const endDate = item.scheduleDateList[1];

          if (!(fromDate < moment(stDate)) || !(toDate < moment(stDate))) {
            if (!(!toDate || toDate > moment(endDate)) || !(fromDate > moment(endDate))) {
              if (!(fromDate < moment(stDate)) && !(fromDate > moment(endDate))) {
                this.fromDateInvalid = true;
              } else {
                this.toDateinvalid = true;
              }
              return false;
            }
          }
        });
      } else {
        this.dateTimes = this.dateTimes.filter(x => x.classItemId !== this.selectedSeason.Id);
        this.dateTimes.forEach(item => {
          const stDate = item.scheduleDateList[0];
          const endDate = item.scheduleDateList[1];
          if (!(fromDate < moment(stDate)) || !(toDate < moment(stDate))) {
            if (!(!toDate || toDate > moment(endDate)) || !(fromDate > moment(endDate))) {
              if (!(fromDate < moment(stDate)) && !(fromDate > moment(endDate))) {
                this.fromDateInvalid = true;
              } else {
                this.toDateinvalid = true;
              }
              return false;
            }
          }
        });
      }
    } else {

    }

  }



  deleteSeasonFromDetail() {
    this.deleteSeason(this.selectedSeason);
  }

  deleteSeason(item) {
    this.selectedDeleteingSeason = item;
    this.deleteConfModel = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Confirm',
        messageBody: 'CLASS.DeleteConfirm'
      }
    );
  }

  /**
   * @deprecated
   */
  deleteYesHandler() {
    this.exceClassService.DeleteSchedule(this.selectedDeleteingSeason).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      res => {
        if (this.addSeasonModel) {
          this.addSeasonModel.close();
        }
        this.getClasses();
      }, null
    )
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.addSeasonModel) {
      this.addSeasonModel.close();
    }
  }

}
