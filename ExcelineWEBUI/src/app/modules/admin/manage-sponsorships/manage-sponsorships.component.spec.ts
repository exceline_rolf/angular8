import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageSponsorshipsComponent } from './manage-sponsorships.component';

describe('ManageSponsorshipsComponent', () => {
  let component: ManageSponsorshipsComponent;
  let fixture: ComponentFixture<ManageSponsorshipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageSponsorshipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSponsorshipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
