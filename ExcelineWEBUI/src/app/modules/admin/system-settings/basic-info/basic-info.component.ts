import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.scss']
})

export class BasicInfoComponent implements OnInit, OnDestroy {
  private basicInfoSetting: any;
  public basicInfoForm: FormGroup;
  private destroy$ = new Subject<void>();

  constructor(private adminService: AdminService,
    private fb: FormBuilder) { }

  ngOnInit() {

    this.adminService.GetGymCompanySettings('INFO').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      this.basicInfoSetting = result.Data;
      this.basicInfoForm.patchValue({
        Id: this.basicInfoSetting.Id,
        GymCompanyID: this.basicInfoSetting.GymCompanyID,
        GymCompanyName: this.basicInfoSetting.GymCompanyName,
        Address1: this.basicInfoSetting.Address1,
        Address2: this.basicInfoSetting.Address2,
        Address3: this.basicInfoSetting.Address3,
        PostalCode: this.basicInfoSetting.PostalCode,
        PostalPlace: this.basicInfoSetting.PostalPlace,
        ContactPerson: this.basicInfoSetting.ContactPerson,
        Email: this.basicInfoSetting.Email,
        Mobile: this.basicInfoSetting.Mobile,
        AccountNo: this.basicInfoSetting.AccountNo,
        OtherAdminMobile: this.basicInfoSetting.OtherAdminMobile,
        OtherAdminEmail: this.basicInfoSetting.OtherAdminEmail
      });
    }, null, null)

    this.basicInfoForm = this.fb.group({
      'Id': [],
      'GymCompanyID': [],
      'GymCompanyName': [],
      'Address1': [],
      'Address2': [],
      'Address3': [],
      'PostalCode': [],
      'PostalPlace': [],
      'ContactPerson': [],
      'Email': [],
      'Mobile': [],
      'AccountNo': [],
      'OtherAdminMobile': [],
      'OtherAdminEmail': [],
    })
  }

  saveBasicInfo() {
    let basicInfoId: any;
    this.adminService.saveGymCompanyBasicSettings(this.basicInfoForm.value).subscribe(result => {
      basicInfoId = result.Data;
    }, null, null);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
