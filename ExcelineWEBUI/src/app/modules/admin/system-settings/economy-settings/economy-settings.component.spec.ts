import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EconomySettingsComponent } from './economy-settings.component';

describe('EconomySettingsComponent', () => {
  let component: EconomySettingsComponent;
  let fixture: ComponentFixture<EconomySettingsComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EconomySettingsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EconomySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
