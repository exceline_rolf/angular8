import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminService } from "app/modules/admin/services/admin.service";
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'economy-settings',
  templateUrl: './economy-settings.component.html',
  styleUrls: ['./economy-settings.component.scss']
})
export class EconomySettingsComponent implements OnInit, OnDestroy {

  private gymEconmySetting: any;
  public economySettingForm: FormGroup;
  private destroy$ = new Subject<void>();

  constructor(private adminService: AdminService,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.adminService.GetGymCompanySettings('Economy').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      this.gymEconmySetting = result.Data;
      this.economySettingForm.patchValue({
        Id: this.gymEconmySetting.ID,
        creditPeriod: this.gymEconmySetting.CreditPeriod,
        oneDueDateAtDate: this.gymEconmySetting.OneDueDateAtDate,
        fixedDueDay: this.gymEconmySetting.FixedDueDay,
        sponsoringDay: this.gymEconmySetting.SponsoringDay,
        sponsoredDueDate: this.gymEconmySetting.SponsoredDueDate,
        sponsorDueDay: this.gymEconmySetting.SponsorDueDay,
        sponsorInvoiceText: this.gymEconmySetting.SponsorInvoiceText,
        contractAutoRenewalDays: this.gymEconmySetting.ContractAutoRenewalDays,
        isSMSInvoice: this.gymEconmySetting.IsSMSInvoice,
        deviationFollowup: this.gymEconmySetting.DeviationFollowup,
        deviationFollowupCompleted: this.gymEconmySetting.DeviationFollowupCompleted,
        reminderDueDays: this.gymEconmySetting.ReminderDueDays
      });
    }, null, null)

    this.economySettingForm = this.fb.group({
      'Id': [],
      'creditPeriod': [],
      'oneDueDateAtDate': [],
      'fixedDueDay': [],
      'sponsoringDay': [],
      'sponsoredDueDate': [],
      'sponsorDueDay': [],
      'sponsorInvoiceText': [],
      'contractAutoRenewalDays': [],
      'isSMSInvoice': [],
      'deviationFollowup': [],
      'deviationFollowupCompleted': [],
      'reminderDueDays': [],
    })
  }
  submitForm(value: any) {
  }

  saveEconomySettings() {
    let economyId: any;
    this.adminService.saveGymCompanyEconomySettings(this.economySettingForm.value).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      economyId = result.Data;
    }, null, null);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}


