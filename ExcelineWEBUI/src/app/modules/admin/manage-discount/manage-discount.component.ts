import { Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import { AdminService } from '../services/admin.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';
import { DataTableFilterPipe } from '../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { DataTableResource } from '../../../shared/components/us-data-table/tools/data-table-resource';
import { ExceToolbarService } from '../../common/exce-toolbar/exce-toolbar.service';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { TranslateService } from '@ngx-translate/core';
import { EntitySelectionType, ColumnDataType } from '../../../shared/enums/us-enum';
import { Observable } from 'rxjs';
import { TreeNode } from 'app/shared/components/us-tree-table/tree-node';
import * as moment from 'moment';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'app-manage-discount',
  templateUrl: './manage-discount.component.html',
  styleUrls: ['./manage-discount.component.scss']
})
export class ManageDiscountComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private itemResourceSpon: DataTableResource<any>;
  private _user: string;
  private itemResource: DataTableResource<any>;
  private popOver: any;
  private branchId: number;
  private filteredEvents = [];
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  selectedTypeObj: any;
  disTitle: any;
  selectedShopId: number;
  editDiscountView: any;
  files: TreeNode[] = [];
  objectKeys = Object.keys;
  sponItems = [];
  filterSponDisCate?: any[] = [];
  selectedSponDisType: any;
  CategoryCode: any;
  selectedVendorId: number;
  selectedDiscountMode: any;
  selectedItemList?: any[] = [];
  selectItemsView: any;
  selectItems?: any[] = [];
  itemCount = 0;
  defaultRole = 'ALL'
  venderSelectionView: any;
  columns: any;
  entitySelectionViewTitle: string;
  entitySelectionType: string;
  isDbPagination = true;
  vendersList?: any[] = [];
  discountModeList?: any[] = [];
  atricleCategoryList?: any[] = [];
  isSponsorContent: boolean;
  isShopContent: boolean;
  locale: string;
  discountTypeList?: any[] = [];
  discountCategoryList?: any[] = [];
  filteredItems: any;
  filteredBranches: any;
  items = [];
  newDiscountView: any;
  status = 'ALL'
  itemList = [];
  itemCountGroup = 0;
  itemCountShop = 0;
  groupDiscountList?: any[] = [];
  shopDiscountList?: any[] = [];
  title: any;
  isSponsor = true;
  isShop = false;
  SponsorNewDiscountForm: FormGroup;
  ShopNewDiscountForm: FormGroup;
  myFormSubmited = false;

  fromDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 }
  };

  toDatePickerOptions: IMyDpOptions = {
  };

  auotocompleteItemsForEmpSelection = [{ id: 'NAME', name: 'Name :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }];
  auotocompleteItems = [{ id: 'Name', name: 'Name :', isNumber: false }];
  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };
  @ViewChild('autobox', { static: false }) public autocomplete;

  formErrors = {
    'Name': '',
    'SelectVender': '',
    'MinNoOfArticles': ''
  };
  validationMessages = {
    'Name': {
      'required': 'ADMIN.Nameisrequired'
    },
    'SelectVender': {
      'required': 'ADMIN.Nameisrequired'
    },
    'MinNoOfArticles': {
      'required': 'ADMIN.InvalidMinArticles'
    }
  };

  constructor(
    private modalService: UsbModal,
    private adminService: AdminService,
    private fb: FormBuilder,
    private exceLoginService: ExceLoginService,
    private toolbarService: ExceToolbarService,
    private translateService: TranslateService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.DiscountsC',
      url: '/adminstrator/manage-discount'
    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this._user = this.exceLoginService.CurrentUser.username;

    this.SponsorNewDiscountForm = this.fb.group({
      'DiscountCategory': [null],
      'DiscountType': [this.selectedSponDisType]
    });

    this.ShopNewDiscountForm = this.fb.group({
      'DiscountCategory': [null],
      'Name': [null, [Validators.required]],
      'Description': [null],
      'SelectVender': [null, [Validators.required]],
      'ArticleCategory': [null],
      'MinNoOfArticles': [null, [Validators.required]],
      'SelectedItemCount': [null],
      'DiscountType': [this.selectedDiscountMode],
      'Discounts': [null],
      'StartDate': [null],
      'EndDate': [null]
    });
  }

  ngOnInit() {
    this.ShopNewDiscountForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(_ => {
      UsErrorService.onValueChanged(this.ShopNewDiscountForm, this.formErrors, this.validationMessages)
    });

    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );

    if (this.isSponsor) {
      this.title = 'ADMIN.GroupDiscounts';
      this.GetGroupDiscounts();
    }
    if (this.isShop) {
      this.title = 'ADMIN.SHOPDISCOUNTS';
      this.GetShopDiscounts();
    }
    this.GetDiscountTypes();
    this.GetDiscountModes();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.venderSelectionView) {
      this.venderSelectionView.close();
    }
    if (this.selectItemsView) {
      this.selectItemsView.close();
    }
    if (this.newDiscountView) {
      this.newDiscountView.close();
    }
  }
  // get from date change
    fromDateChange(event, toDate) {
    this.toDatePickerOptions = {
      disableUntil: event.date
    };
    toDate.showSelector = true;
  }

  // Get Group (Sponsor) Discounts
  GetGroupDiscounts() {
    this.files = [];
    this.adminService.GetDiscountList('GROUP').pipe(
      takeUntil(this.destroy$)
      ).subscribe(gD => {
      if (gD) {
        this.sponItems = gD.Data;
        this.groupDiscountList = this.sponItems;
        this.filterSponDisCate = this.sponItems;
        this.prepareDataForTreeTable(this.groupDiscountList);
      }
    });
  }

  prepareDataForTreeTable(data) {
    const sortedArr = (data.reduce(function (res, current) {
      res[current.TypeName] = res[current.TypeName] || [];
      res[current.TypeName].push(current);
      return res;
    }, {}));

    // tslint:disable-next-line:prefer-const
    let files = []
    this.files = []
    // tslint:disable-next-line:prefer-const
    // tslint:disable-next-line:forin
    for (let key in sortedArr) {
      const children = []
      const constArr = sortedArr[key]
      constArr.forEach(element => {
        children.push({ 'data': element })
      });
      this.files.push({
        'data': { 'Id': key },
        'children': children
      });
    }
  }

  // Get Shop Discounts
  GetShopDiscounts() {
    this.adminService.GetDiscountList('SHOP').pipe(
      takeUntil(this.destroy$)
      ).subscribe(sD => {
      if (sD) {
        this.items = sD.Data;
        this.shopDiscountList = this.items;
        this.filteredItems = this.items;
        this.itemCountShop = this.shopDiscountList.length;
        this.itemResource = new DataTableResource(this.shopDiscountList);
        this.itemResource.count().then(count => this.itemCountShop = count);
        this.shopDiscountList.forEach(element => {
          if (Date.now() > Date.parse(element.EndDate)) {
            element.Status = 'OLD';
          } else if (Date.now() < Date.parse(element.StartDate)) {
            element.Status = 'PENDING';
          } else if (Date.now() <= Date.parse(element.EndDate) && Date.now() >= Date.parse(element.StartDate)) {
            element.Status = 'VALID';
          } else {
            element.Status = 'ALL';
          }
        });
      }
    });
  }

  // get discount categories
  GetDiscountCategories() {
    this.adminService.getCategories('DISCOUNT').pipe(
      takeUntil(this.destroy$)
      ).subscribe(discountCate => {
      if (discountCate) {
        this.discountCategoryList = discountCate.Data;
      }
    });
  }

  // get discount types
  GetDiscountTypes() {
    this.adminService.getCategories('GROUPDISCOUNT').pipe(
      takeUntil(this.destroy$)
      ).subscribe(discountType => {
      if (discountType) {
        this.discountTypeList = discountType.Data;
        this.selectedSponDisType = this.discountTypeList[0];
      }
    });
  }

  onChange(event) {
    if (event) {
      this.filterSponDisCate = this.sponItems;
      this.filterSponDisCate = this.sponItems.filter(x => x.TypeId == event);
    }
  }

  // get discount modes
  GetDiscountModes() {
    this.adminService.getCategories('DISCOUNTMODE').pipe(
      takeUntil(this.destroy$)
      ).subscribe(discountMode => {
      if (discountMode) {
        this.discountModeList = discountMode.Data;
        this.selectedDiscountMode = this.discountModeList[0];
      }
    });
  }

  // get Article Categories
  GetArticleCategories() {
    this.adminService.getCategories('ITEM').pipe(
      takeUntil(this.destroy$)
      ).subscribe(articleCate => {
      if (articleCate) {
        this.atricleCategoryList = articleCate.Data;
      }
    });
  }

  // get article names (select items in new discount shop)
  GetArticles() {
    this.adminService.getArticles('ITEM', '', null, -1, true, true, this.branchId, false).pipe(
      takeUntil(this.destroy$)
      ).subscribe(article => {
      if (article) {
        this.selectItems = article.Data
      }
    });
  }

  // get vender list
  GetVenderList(searchText) {
    this.adminService.GetVenderList(searchText).pipe(
      takeUntil(this.destroy$)
      ).subscribe(venders => {
      if (venders) {
        this.vendersList = venders.Data
        this.vendersList.forEach(date => {
          date.CreatedDate = date.CreatedDate === '0001-01-01T00:00:00' ? ' ' : date.CreatedDate;
        });
        this.itemCount = venders.Data.length;
      }
    });
  }

  venderSelectionModal(content: any) {
    this.translateService.get('MEMBERSHIP.SelectEmployee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
    this.isDbPagination = true;
    this.entitySelectionType = EntitySelectionType[EntitySelectionType.COM]; // select vendors
    this.columns = [
      { property: 'Name', header: 'ADMIN.Name | translate', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'CreatedDate', header: 'Introduce Date', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];
    this.GetVenderList('');
    this.venderSelectionView = this.modalService.open(content, { width: '990' });
  }

  closeVenderSelectionView() {
    this.venderSelectionView.close();
  }

  selectedRowItems(event) {
  }

  // Add employees to the table
  selectedVender(item: any) {
    this.selectedVendorId = item.Id;
    if (item.IsSelected == false) {
      this.selectItems = [];
    }

    if (item) {
      this.ShopNewDiscountForm.controls['SelectVender'].setValue(item.Name);
    }
    this.closeVenderSelectionView();
  }

  // Search an employee from employee selection
  searchEmployee(val: any): void {
    this.GetVenderList(val.searchVal);
  }

  // select Items view open (new discount shop)
  selectItemsModal(content: any) {
    this.selectItemsView = this.modalService.open(content, { width: '550' });
  }

  // to get selected item count
  checkBoxChange(item) {
    this.selectedItemList.push(item);
    if (this.selectedItemList.length > 0) {
      this.ShopNewDiscountForm.controls['SelectVender'].setValue(null);
    }
  }

  changeType(event) {
    if (event.target.value == 'sponsor') {
      this.title = 'ADMIN.GroupDiscounts';
      this.isSponsor = true;
      this.isShop = false;
    }
    if (event.target.value == 'shop') {
      this.title = 'ADMIN.SHOPDISCOUNTS';
      this.isShop = true;
      this.isSponsor = false;
      this.GetShopDiscounts();
    }
  }

  // delete selected Group Discount row
  deleteSelectedGroupDiscount(item) {
    const deleteSponsorDiscount = {
      discountId: item.Id,
      type: 'sponsor'
    }
    this.adminService.DeleteShopSponsorDiscount(deleteSponsorDiscount).pipe(
      takeUntil(this.destroy$)
      ).subscribe(del => {
        const index: number = this.groupDiscountList.indexOf(item);
        if (index !== -1) {
          this.groupDiscountList.splice(index, 1);
        }
    });
  }

  // for view employees
  viewLoad(rowItem, popOver) {
    if (this.popOver) {
      this.popOver.close();
    }
    this.popOver = popOver;
    this.popOver.open();
    this.itemList = [];
    this.itemList.push(rowItem.ItemNames);
  }

  closePopOver() {
    this.popOver.close();
  }

  // delete selected Shop Discount row
  deleteSelectedShopDiscount(item) {
    const deleteShopDiscount = {
      discountId: item.Id,
      type: 'shop'
    }
    this.adminService.DeleteShopSponsorDiscount(deleteShopDiscount).pipe(
      takeUntil(this.destroy$)
      ).subscribe(del => {
      const index: number = this.shopDiscountList.indexOf(item);
      if (index !== -1) {
        this.shopDiscountList.splice(index, 1);
      }
    });
  }

  // get status change
  statusChange(value) {
    this.filteredItems = this.items;
    if (value && value !== 'ALL') {
      this.filteredItems = this.items.filter(x => x.Status === value);
    }
    this.itemResource = new DataTableResource(this.filteredItems);
    this.itemResource.count().then(count => this.itemCountShop = count);
  }

  openNewDiscountModal(content: any, IsEdit) {
    if (IsEdit == true) {
      this.disTitle = 'ADMIN.EditGroupDiscountsC';
    }
    if (IsEdit == false) {
      this.disTitle = 'ADMIN.NewDiscount';
    }
    this.GetDiscountTypes();
    this.GetDiscountCategories();
    this.GetVenderList('');
    this.GetArticles();
    this.GetArticleCategories();
    this.GetDiscountModes();

    if (this.isSponsor == true) {
      this.isShopContent = false;
      this.isSponsorContent = true;
      this.SponsorNewDiscountForm.get('DiscountCategory').enable();
      this.SponsorNewDiscountForm.controls['DiscountCategory'].setValue('Sponsor');
      this.SponsorNewDiscountForm.patchValue({ DiscountType: this.discountTypeList[0].Id })
      this.filterSponDisCate = this.sponItems.filter(x => x.TypeId == this.discountTypeList[0].Id);
      this.newDiscountView = this.modalService.open(content, { width: '990' });
    }

    if (this.isShop == true) {
      this.isSponsorContent = false;
      this.isShopContent = true;
      this.ShopNewDiscountForm.get('DiscountCategory').enable();
      this.ShopNewDiscountForm.controls['Name'].setValue(null);
      this.ShopNewDiscountForm.controls['Description'].setValue(null);
      this.ShopNewDiscountForm.controls['MinNoOfArticles'].setValue(1);
      this.ShopNewDiscountForm.controls['Discounts'].setValue(0);
      this.ShopNewDiscountForm.controls['SelectVender'].setValue(null);
      this.selectedItemList.length = 0;
      this.ShopNewDiscountForm.controls['DiscountCategory'].setValue('Shop');
      const today = new Date();
      const sDate = {
        date: {
          year: today.getFullYear(),
          month: today.getMonth() + 1,
          day: today.getDate()
        }
      }
      this.ShopNewDiscountForm.patchValue({
        StartDate: sDate,
        EndDate: sDate
      });
      this.newDiscountView = this.modalService.open(content, { width: '990' });
    }
  }

  // row double click event for Shop
  rowDoubleClickShop(rowEvent: any, content: any) {
    this.disTitle = 'ADMIN.EditShopDiscount';
    this.GetDiscountTypes();
    this.GetDiscountCategories();
    this.GetVenderList('');
    this.GetArticles();
    this.GetArticleCategories();
    this.isSponsorContent = false;
    this.isShopContent = true;
    this.ShopNewDiscountForm.get('DiscountCategory').disable();
    this.ShopNewDiscountForm.controls['DiscountCategory'].setValue('Shop');
    this.selectedItemList.length = rowEvent.row.item.ItemList.length;
    this.selectedVendorId = rowEvent.row.item.VendorId;
    this.selectedShopId = rowEvent.row.item.Id;
    this.selectedDiscountMode = this.discountModeList.filter(x => x.Id == rowEvent.row.item.TypeId)[0];
    this.ShopNewDiscountForm.controls['DiscountType'].setValue(this.selectedDiscountMode);
    const sDate = new Date(rowEvent.row.item.StartDate);
    const eDate = new Date(rowEvent.row.item.EndDate);
    this.ShopNewDiscountForm.patchValue({
      Name: rowEvent.row.item.Name,
      Description: rowEvent.row.item.Description,
      SelectVender: rowEvent.row.item.VendorName,
      ArticleCategory: (rowEvent.row.item.ArticleCategoryId == 0) ? null : rowEvent.row.item.ArticleCategoryId,
      MinNoOfArticles: rowEvent.row.item.MinNumberOfArticles,
      Discounts: rowEvent.row.item.Discount,
      StartDate: {
        date: {
          year: sDate.getFullYear(),
          month: sDate.getMonth() + 1,
          day: sDate.getDate()
        }
      },
      EndDate: {
        date: {
          year: eDate.getFullYear(),
          month: eDate.getMonth() + 1,
          day: eDate.getDate()
        }
      }
    });
    this.newDiscountView = this.modalService.open(content, { width: '990' });
  }

  // row double click for Group
  rowDoubleClickGroup(content, rowEvent) {
    if (rowEvent) {
      this.selectedTypeObj = rowEvent.node.data;
    }
    this.disTitle = 'ADMIN.EditGroupDiscountsC';
    this.GetDiscountTypes();
    this.GetDiscountCategories();
    this.GetVenderList('');
    this.GetArticles();
    this.GetArticleCategories();
    this.GetDiscountModes();
    if (this.isSponsor == true) {
      this.isShopContent = false;
      this.isSponsorContent = true;
      this.SponsorNewDiscountForm.get('DiscountCategory').disable();
      this.SponsorNewDiscountForm.controls['DiscountCategory'].setValue('Sponsor');
      this.filterSponDisCate = this.sponItems.filter(x => x.TypeId == this.selectedTypeObj.TypeId);
      this.newDiscountView = this.modalService.open(content, { width: '990' });
      this.SponsorNewDiscountForm.patchValue({ DiscountType: this.selectedTypeObj.TypeId })
    }
  }

  // get Discount Category Change
  ChangeDiscountCategory(value) {
    this.CategoryCode = value;
    if (value == 'Sponsor') {
      this.isShopContent = false;
      this.isSponsorContent = true;
      this.SponsorNewDiscountForm.controls['DiscountCategory'].setValue('Sponsor');
      this.SponsorNewDiscountForm.patchValue({
        DiscountType: this.selectedSponDisType
      });
      this.GetDiscountTypes();
    }
    if (value == 'Shop') {
      this.ShopNewDiscountForm.controls['Name'].setValue(null);
      this.ShopNewDiscountForm.controls['Description'].setValue(null);
      this.ShopNewDiscountForm.controls['MinNoOfArticles'].setValue(1);
      this.ShopNewDiscountForm.controls['Discounts'].setValue(0);
      this.ShopNewDiscountForm.controls['SelectVender'].setValue(null);
      this.ShopNewDiscountForm.controls['DiscountCategory'].setValue('Shop');
      this.selectedItemList.length = 0;
      this.ShopNewDiscountForm.patchValue({
        DiscountType: this.selectedDiscountMode
      });
      const today = new Date();
      const sDate = {
        date: {
          year: today.getFullYear(),
          month: today.getMonth() + 1,
          day: today.getDate()
        }
      }
      this.ShopNewDiscountForm.patchValue({
        StartDate: sDate,
        EndDate: sDate
      });
      this.isSponsorContent = false;
      this.isShopContent = true;
    }
  }

  // Add discount category
  addDiscountCategory() {
    let _sponosrDiscount = {
      'Id': -1,
      'Name': '',
      'MinMemberNo': 0,
      'HighAmount': 0,
      'HighPercentage': 0,
      'LowAmount': 0,
      'LowPercentage': 0,
    };
    this.filterSponDisCate.push(_sponosrDiscount);
  }

  nameChanged(value: any, discount: any) {
    discount.GroupDiscountName = value;
  }

  minMemberNoChanged(value: any, discount: any) {
    discount.MinMemberNo = Number(value);
  }

  highAmountChanged(value: any, discount: any) {
    discount.HighAmount = Number(value);
    if (discount.HighAmount > 0) {
      discount.HighPercentage = 0;
    }
  }

  highPercentageChanged(value: any, discount: any) {
    discount.HighPercentage = Number(value);
    if (discount.HighPercentage > 0) {
      discount.HighAmount = 0;
    }
  }

  lowAmountChanged(value: any, discount: any) {
    discount.LowAmount = Number(value);
    if (discount.LowAmount > 0) {
      discount.LowPercentage = 0;
    }
  }

  lowPercentageChanged(value: any, discount: any) {
    discount.LowPercentage = Number(value);
    if (discount.LowPercentage > 0) {
      discount.LowAmount = 0;
    }
  }

  // save sponsor discount data
  saveSponsorDiscount(list, value) {
    list.forEach(element => {
      const discountListGrp = {
        Id: element.Id,
        DiscountType: 'GROUP',
        GroupDiscountName: element.GroupDiscountName,
        MinMemberNo: element.MinMemberNo,
        MinPayment: (element.MinPayment != undefined) ? element.MinPayment : 0,
        DiscountAmount: (element.DiscountAmount != undefined) ? element.DiscountAmount : 0,
        TypeId: value.DiscountType.Id,
        HighAmount: element.HighAmount,
        HighPercentage: element.HighPercentage,
        LowAmount: element.LowAmount,
        LowPercentage: element.LowPercentage,
        MinVisitsNo: (element.MinVisitsNo != undefined) ? element.MinVisitsNo : 0,
        BranchId: this.branchId,
        IsActive: true,
        CreatedUser: this._user
      }

      const saveSponsorDiscount = {
        discountList: [discountListGrp],
        branchId: this.branchId,
        contractSettingID: -1
      }

      this.adminService.SaveDiscount(saveSponsorDiscount).pipe(
        takeUntil(this.destroy$)
        ).subscribe(data => {
        this.GetGroupDiscounts();
        this.newDiscountView.close();
      });
    });
  }

  // save shop discount data
  saveShopDiscount(value) {
    if (value.Name == null) {
      this.ShopNewDiscountForm.controls['Name'].setErrors({ 'required': true });
    } else {
      this.ShopNewDiscountForm.controls['Name'].setErrors(null);
    }

    if (value.SelectVender == null) {
      this.ShopNewDiscountForm.controls['SelectVender'].setErrors({ 'required': true });
    } else {
      this.ShopNewDiscountForm.controls['SelectVender'].setErrors(null);
    }

    if (value.MinNoOfArticles == null && value.MinNoOfArticles > 0) {
      this.ShopNewDiscountForm.controls['MinNoOfArticles'].setErrors({ 'required': true });
    } else {
      this.ShopNewDiscountForm.controls['MinNoOfArticles'].setErrors(null);
    }

    this.myFormSubmited = true;

    if (this.ShopNewDiscountForm.valid) {
      const discountListObj = {
        Id: (this.selectedShopId > 0) ? this.selectedShopId : -1,
        DiscountType: 'SHOP',
        Name: value.Name,
        GroupDiscountName: value.Name,
        Description: value.Description,
        DiscountAmount: value.Discounts,
        StartDate: this.getDateConverter(value.StartDate.date),
        EndDate: this.getDateConverter(value.EndDate.date),
        BranchId: this.branchId,
        IsActive: true,
        CreatedUser: this._user,
        TypeId: value.DiscountType.Id,
        NoOfFreeItems: (value.SelectedItemCount == null) ? 0 : value.SelectedItemCount,
        MinNoOfArticles: value.MinNoOfArticles,
        VendorId: this.selectedVendorId,
        ArticleCategoryId: (value.ArticleCategory == null) ? 0 : value.ArticleCategory
      }

      const saveShopDiscount = {
        discountList: [discountListObj],
        branchId: this.branchId,
        contractSettingID: -1
      }

      this.adminService.SaveDiscount(saveShopDiscount).pipe(
        takeUntil(this.destroy$)
        ).subscribe(data => {
        this.GetShopDiscounts();
        this.ShopNewDiscountForm.reset();
        this.newDiscountView.close();
      });
    } else {
      UsErrorService.validateAllFormFields(this.ShopNewDiscountForm, this.formErrors, this.validationMessages);
    }
  }

  // convert date js object in to string
  getDateConverter(datee: any) {
    const dd = (datee.day < 10 ? '0' : '') + datee.day;
    const MM = ((datee.month + 1) < 10 ? '0' : '') + datee.month;
    const yyyy = datee.year;
    return (yyyy + '-' + MM + '-' + dd);
  }

}
