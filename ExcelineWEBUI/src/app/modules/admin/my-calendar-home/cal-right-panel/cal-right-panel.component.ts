import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { GymEmployeeHomeService } from '../../manage-employees/manage-gym-employees/gym-employee-home/gym-employee-home.service';
import { AdminService } from '../../services/admin.service';
import * as moment from 'moment';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-cal-right-panel',
  templateUrl: './cal-right-panel.component.html',
  styleUrls: ['./cal-right-panel.component.scss']
})
export class CalRightPanelComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  selectedTask: any;
  model: void;
  itemCountJob: number;
  itemResourceJob: DataTableResource<any>;
  @Input() entityId: number;

  itemCount: number;
  itemResource: DataTableResource<{}>;
  followUpTaskList: any[] = [];
  jobTaskList: any[] = [];
  selectedTab: number;
  constructor(
    private router: Router,
    private adminService: AdminService,
    private exceMessageService: ExceMessageService,
    private gymEmployeeHomeService: GymEmployeeHomeService
  ) { }

  ngOnInit() {
    this.selectedTab = 0;
    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
      if (value.id === 'ASSIN_JOB') {
        this.assignJobToEmployeeConfirm();
      } else if (value.id === 'ASSIN_FLOWWUP') {
        this.assignFollowupToEmployeeConfirm();
      }
    });

    this.adminService.GetFollowUpByEmployeeId({
        employeeId: this.entityId,
        hit: 1
      }).pipe(takeUntil(this.destroy$)).subscribe(
        res => {
          res.Data.forEach(task => {
            this.followUpTaskList.push({
                TaskId: task.Id,
                Title: task.Text,
                StartTime: task.StartTime,
                EndTime: task.EndTime,
                IsFollowUp: true,
                Date: task.PlanDate,
                Day: moment(new Date(task.PlanDate)).format('dddd'),
                FollowUpMemberName:
                  task.FollowupMemName,
                FollowUpMemberID: task.FollowUpMemberId,
                FollowUpMemberRole:
                  task.FollowupMemRole,
                TaskTempleteId: task.TaskTemplateId,
                CategoryName: task.Category,
                EntityRoleType: task.EntityRoleType,
                IsEmployee: task.IsEmployee,
                CategoryColor: '#4FFF33'
              });
            this.itemResource = new DataTableResource(this.followUpTaskList);
            this.itemCount = Number(this.followUpTaskList.length);
            this.itemResource.count().then(count => this.itemCount = count);
          });

          this.adminService.GetJobsByEmployeeId({
            employeeId: this.entityId,
            hit: 1
          }).pipe(
            takeUntil(this.destroy$)
            ).subscribe(jobs => {
                jobs.Data.forEach(task => {
                  this.jobTaskList.push({
                      TaskId: task.Id,
                      Title: task.Text,
                      StartTime: task.StartTime,
                      EndTime: task.EndTime,
                      IsFollowUp: task.IsFollowUp,
                      Date: task.StartTime,
                      Day: moment(new Date(task.StartTime)).format('dddd'),
                      CategoryName: task.Category,
                      EntityRoleType: task.EntityRoleType,
                      IsEmployee: task.IsEmployee,
                      CategoryColor: '#4FFF33'
                    });
                });
                this.itemResourceJob = new DataTableResource(this.jobTaskList);
                this.itemCountJob = Number(this.jobTaskList.length);
                this.itemResourceJob.count().then(count => this.itemCountJob = count);
              }
            )
        }, null, null);
  }

  openFollowUpPage(item) {
    this.router.navigate(['adminstrator/gym-employee-home/calendar/' + 'update fixed followup to ' + item.FollowUpMemberName]);
  }

  assignJobToEmployee(task) {
    this.selectedTask = task;
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Confirm',
        messageBody: 'COMMON.wanttoassignjob',
        msgBoxId: 'ASSIN_JOB'
      }
    );
  }

  assignFollowUpToEmployee(task) {
    this.selectedTask = task;
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Confirm',
        messageBody: 'COMMON.wanttoassign',
        msgBoxId: 'ASSIN_FLOWWUP'
      }
    );
  }

  assignJobToEmployeeConfirm() {
    this.selectedTask.EntityId = this.entityId;
    this.adminService.AssignTaskToEmployee({}).pipe(takeUntil(this.destroy$)).subscribe(
      null, null, null
    );
  }

  assignFollowupToEmployeeConfirm() {
    this.selectedTask.EntityId = this.entityId;
    this.adminService.AssignTaskToEmployee({}).pipe(takeUntil(this.destroy$)).subscribe(
      null, null, null
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
