import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcconomySettingsComponent } from './ecconomy-settings.component';

describe('EcconomySettingsComponent', () => {
  let component: EcconomySettingsComponent;
  let fixture: ComponentFixture<EcconomySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcconomySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcconomySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
