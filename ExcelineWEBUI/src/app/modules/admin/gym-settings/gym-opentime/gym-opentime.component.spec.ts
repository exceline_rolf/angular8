import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymOpentimeComponent } from './gym-opentime.component';

describe('GymOpentimeComponent', () => {
  let component: GymOpentimeComponent;
  let fixture: ComponentFixture<GymOpentimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymOpentimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymOpentimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
