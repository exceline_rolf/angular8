
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../gym-settings.service'
import { AdminService } from '../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'access-settings',
  templateUrl: './access-settings.component.html',
  styleUrls: ['./access-settings.component.scss']
})

export class AccessSettingsComponent implements OnInit, OnDestroy {
  private branch: any;
  private accessSettings: any;
  public accessSettingsFrom: any;
  private destroy$ = new Subject<void>();

  message: string;
  type: string;
  isShowInfo: boolean;

  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private gymListService: GymSettingsService, private fb: FormBuilder) {
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }
    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.fetchData(value.some)
    }, null, null);
  }

  ngOnInit() {
    this.accessSettingsFrom = this.fb.group({
      'CountVisitAfter': [null],
      'MinutesBetweenAccess': [null],
      'BlockAccessNoOfUnpaidInvoices': [null],
      'ComPort': [null],
      'BlockAccessDueBalance': [null],
      'SecondsViewCustomerScreen': [null],
      'IsMemCardExportAsGuestCard': [null],
      'CheckFingerPrint': [null],
      'IsSendSms': [null],
      'BlockAccessOnAccountBalance': [null],
      'IsReceptionAllowCustomerScreen': [null],
      'IsExtraActivity': [null],
      'AccIsDisplayName': [null],
      'AccIsDisplayCustomerNumber': [null],
      'AccIsDisplayContractTemplate': [null],
      'AccIsDisplayHomeGym': [null],
      'AccIsDisplayContractEndDate': [null],
      'AccIsDisplayBirthDate': [null],
      'AccIsDisplayAge': [null],
      'AccIsDisplayLastVisit': [null],
      'AccIsDisplayCreditBalance': [null],
      'AccIsDisplayPicture': [null],
      'AccIsDisplayLastAccess': [null],
      'AccIsDisplayNotification': [null],
    });
  }

  fetchData(branch) {
    this.branch = branch;
    this.adminService.getGymSettings('ACCESSTIME', null, this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.accessSettings = result.Data;
          this.accessSettingsFrom.patchValue(this.accessSettings[0])
        }
      }, null, null)
  }

  saveSettings(value) {
    // tslint:disable-next-line:forin
    for (const key in value) {
      this.accessSettings[0][key] = value[key]
    }
    this.adminService.SaveGymAccessSettings(this.accessSettings[0]).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          if (result.Status === 'OK') {
            this.translateService.get('ADMIN.SaveSuccess').pipe(
              takeUntil(this.destroy$)
            ).subscribe(translatedValue => this.message = translatedValue);
            this.type = 'SUCCESS';
            this.isShowInfo = true;
            setTimeout(() => {
              this.isShowInfo = false;
            }, 4000);
          }
        }
      }, null, null);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
