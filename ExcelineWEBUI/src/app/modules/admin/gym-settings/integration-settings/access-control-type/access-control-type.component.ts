import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AccessControlTypeService } from '../access-control-type-service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'access-control-type',
  templateUrl: './access-control-type.component.html',
  styleUrls: ['./access-control-type.component.scss']
})
export class AccessControlTypeComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  isArx = false;
  isGAT = false;
  isExc = false;

  @Input('cpToggle') cpToggle: boolean;
  @Input() private selectedTypes = false

  constructor(private accessControlTypeService: AccessControlTypeService) {
    this.accessControlTypeService.accessProfileTypeLoaded.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      res => {
        const types = res.split(',')
        types.forEach(element => {
          if (element === 'ARX') {
            this.isArx = true
          } else if (element === 'GAT') {
            this.isGAT = true
          } else if (element === 'EXCELINE') {
            this.isExc = true
          }
        });
      }
    );
  }

  ngOnInit() {

  }

  changeTypes(gat, exc, arx) {
    let typeString = ''
    if (gat) {
      typeString += 'GAT'
    }
    if (exc) {
      if (typeString === '') {
        typeString = 'EXCELINE'
      } else if (typeString !== '') {
        typeString += ',EXCELINE'
      }
    }
    if (arx) {
      if (typeString === '') {
        typeString = 'ARX'
      } else if (typeString !== '') {
        typeString += ',ARX'
      }
    }
    this.accessControlTypeService.SelectedString = typeString
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
