import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExorliveComponent } from './exorlive.component';

describe('ExorliveComponent', () => {
  let component: ExorliveComponent;
  let fixture: ComponentFixture<ExorliveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExorliveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExorliveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
