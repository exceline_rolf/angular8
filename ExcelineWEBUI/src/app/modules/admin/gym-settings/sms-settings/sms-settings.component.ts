
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ExceService } from '../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import {GymSettingsService } from '../gym-settings.service'
import { AdminService } from '../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil, take } from 'rxjs/operators';
import { Subject } from 'rxjs';
@Component({
  selector: 'sms-settings',
  templateUrl: './sms-settings.component.html',
  styleUrls: ['./sms-settings.component.scss']
})
export class SmsSettingsComponent implements OnInit, OnDestroy {
  private branch: any;
  private smsSettings: any;
  public smsSettingsForm: any;
  public currentId = -1;
  public currentName = '';
  public sendingTimesItems = [];
  public sendingTimesCount = 0;
  private sendingTimesResource: any;
  public prohibitedTimesItems = [];
  public prohibitedTimesCount = 0;
  private prohibitedTimesResource: any;
  private isDuplicateDay= false
  private model;

  private message: string;
  private type: string;
  private isShowInfo: boolean;
  private destroy$ = new Subject<void>();

  @ViewChild('senderName', { static: true }) senderName: ElementRef;

  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private translateService: TranslateService,
    private gymListService: GymSettingsService,
    private fb: FormBuilder,
    private exceMessageService: ExceMessageService) {

    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }

    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.fetchData(value.some)
    }, error => {

    }, () => {

    });
  }

  ngOnInit() {
    this.smsSettingsForm = this.fb.group({
      'IsAccessInfoInformTrainer': [null],
      'IsAccessInfoIsFollowUpPopUp': [null],
      'IsAccessInfoContractEnds': [null],
      'IsAccessInfoAntiDoping': [null],
      'IsAccessRecentlyVisit': [null],
      'IsAccessDueOnAccount': [null],
      'IsAccessDueUnpaid': [null],
      'IsAccessDuebalance': [null],
      'IsAccessClosedOutsideOpenHours': [null],
      'IsAccessClosedPeriod': [null],
      'IsAccessOutsideProfile': [null],
      'IsAccessFreeze': [null],
      'IsReorderingShop': [null],
      'IsPriceChange': [null],
      'DaysBeforeFreezeEnds':  [null, [Validators.pattern('^[0-9]*$')]],
      'NovisitsIntervalMessage2':  [null, [Validators.pattern('^[0-9]*$')]],
      'NovisitsIntervalMessage1':  [null, [Validators.pattern('^[0-9]*$')]],
      'ReminderDaysInterval':  [null, [Validators.pattern('^[0-9]*$')]],
      'ContractEnding': [null, [Validators.pattern('^[0-9]*$')]],
      'ATGNotApprovedInterval3':  [null, [Validators.pattern('^[0-9]*$')]],
      'ATGNotApprovedInterval2':  [null, [Validators.pattern('^[0-9]*$')]],
      'ATGNotApprovedInterval1':  [null, [Validators.pattern('^[0-9]*$')]],
      'IsBookingReminder': [null],
      'IsBirthday': [null],
      'LastvisitInterval':  [null, [Validators.pattern('^[0-9]*$')]],
      'AccessInfoPunchesLowestNo':  [null, [Validators.pattern('^[0-9]*$')]]
    });
  }


  fetchData(branch) {
    this.branch = branch;
    this.adminService.getGymSettings('SMS', null, this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {

          this.smsSettings = result.Data;
          this.currentId = this.smsSettings[0].Id
          this.currentName = this.smsSettings[0].SenderName
          this.smsSettingsForm.patchValue(this.smsSettings[0])
          this.sendingTimesResource = new DataTableResource(this.smsSettings[0].SendingTimes);
          this.sendingTimesResource.count().then(count => this.sendingTimesCount = count);
          this.sendingTimesItems = this.smsSettings[0].SendingTimes;
          this.sendingTimesCount = Number(this.smsSettings[0].SendingTimes.length);
          this.prohibitedTimesResource = new DataTableResource(this.smsSettings[0].ProhibittedTimes);
          this.prohibitedTimesResource.count().then(count => this.sendingTimesCount = count);
          this.prohibitedTimesItems = this.smsSettings[0].ProhibittedTimes;
          this.prohibitedTimesCount = Number(this.smsSettings[0].ProhibittedTimes.length);

        } else {

        }
      }, error => {

      }, () => {

      })
  }

  addSendingItem() {

    const newItem = {
      'BranchId': this.branch.Id,
      'Days': '',
      'Id': '',
      'IsFriday': false,
      'IsMonday': false,
      'IsSaturday': false,
      'IsSunday': false,
      'IsThursday': false,
      'IsTuesday': false,
      'IsWednesday': false,
      'SendAfterTime': '09:00'
    }
    this.sendingTimesItems.push(newItem)
  }

  deleteSendingItem(rowItem) {
    this.sendingTimesItems = this.sendingTimesItems.filter(item => item !== rowItem);
  }

  addProhibitedItem() {
    const newItem = {
      'BranchId': this.branch.Id,
      'Comment': '',
      'Id': '',
      'EndDate': '1900-01-01T09:00:00',
      'StartDate': '1900-01-01T09:00:00'
    }
    this.prohibitedTimesItems.push(newItem)
  }

  deleteProhibitedItem(rowItem) {
    this.prohibitedTimesItems = this.prohibitedTimesItems.filter(item => item !== rowItem);
  }


  openExceMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('ERROR',
      {
        messageTitle: title,
        messageBody: body
      }
    );
  }

  senderNameChange(event) {
    if (event.target.value.length < 12) {
      this.smsSettings[0].SenderName = event.target.value;
    } else {
      let messageTitle = '';
      let messageBody = '';
      (this.translateService.get('ADMIN.SmsSettingsError').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageTitle = tranlstedValue));
      (this.translateService.get('ADMIN.SenderNameLengthError').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageBody = tranlstedValue));
      this.exceMessageService.openMessageBox('ERROR',
      {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'MSG_EMAIL'
      });
      this.senderName.nativeElement.value = this.currentName
    }
  }

  saveSettings(value) {
    this.isDuplicateDay = false
    // tslint:disable-next-line:forin
    for (const key in value) {
      this.smsSettings[0][key] = value[key]
    }
    let mondayC = 0
    let tuesdayC = 0
    let wednesdayC = 0
    let thursdayC = 0
    let saturdayC = 0
    let sundayC = 0
    let fridayC = 0;
    this.sendingTimesItems.forEach(element => {
      if (element.IsMonday) {
        mondayC++
        if (mondayC > 1 ) {
          this.isDuplicateDay = true
        }
      }
      if (element.IsTuesday) {
        tuesdayC++
        if (tuesdayC > 1 ) {
          this.isDuplicateDay = true
        }
      }
      if (element.IsWednesday) {
        wednesdayC++
        if (wednesdayC > 1 ) {
          this.isDuplicateDay = true
        }
      }
      if (element.IsThursday) {
        thursdayC++
        if (thursdayC > 1 ) {
          this.isDuplicateDay = true
        }
      }
      if (element.IsFriday) {
        fridayC++
        if (fridayC > 1 ) {
          this.isDuplicateDay = true
        }
      }
      if (element.IsSunday) {
        sundayC++
        if (sundayC > 1 ) {
          this.isDuplicateDay = true
        }
      }
      if (element.IsSaturday) {
        saturdayC++
        if (saturdayC > 1 ) {
          this.isDuplicateDay = true
        }
      }
    });

    if (!this.isDuplicateDay) {
      this.smsSettings[0].SendingTimes = this.sendingTimesItems
      this.smsSettings[0].ProhibittedTimes = this.prohibitedTimesItems

      this.adminService.SaveGymSmsSettings(this.smsSettings[0]).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        result => {
          if (result) {
            if (result.Status === 'OK') {
              this.translateService.get('ADMIN.SaveSuccess').pipe(
                takeUntil(this.destroy$)
              ).subscribe(translatedValue => this.message = translatedValue);
              this.type = 'SUCCESS';
              this.isShowInfo = true;
              setTimeout(() => {
                this.isShowInfo = false;
              }, 4000);
            }
          }
        }, error => {
          this.translateService.get('ADMIN.SaveNotSuccess').pipe(
            takeUntil(this.destroy$)
          ).subscribe(translatedValue => this.message = translatedValue);
          this.type = 'DANGER';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
      }, () => {

      })
    } else {
      this.openExceMessage('Exceline', 'Duplicate Entry')
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
