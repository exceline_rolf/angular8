import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../gym-settings.service'
import { AdminService } from '../../services/admin.service'
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'search-settings',
  templateUrl: './search-settings.component.html',
  styleUrls: ['./search-settings.component.scss']
})
export class SearchSettingsComponent implements OnDestroy {

  private branch: any;
  public categoryItems = [];
  private categoryItemsBase = [];
  public categoryCount = 0;
  private categoryResource: any;
  public memberSearchOrder = ''
  private searchMemberSetting: any;
  private memberSearchText: string;
  private categories: any;
  private settingsArray = [];
  public isDisabled = true
  private invokeEventSubscribe: any;
  private sortedCategories: any;
  private codeArray: any;
  public rowTooltip: any;
  private destroy$ = new Subject<void>();

  private message: string;
  private type: string;
  private isShowInfo: boolean;

  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private gymListService: GymSettingsService) {

    this.adminService.getCategoriesByType('SEARCHMEMBERSETTING').pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.categoryItems = result.Data;
          this.categoryItemsBase = this.categoryItems;
          this.categoryResource = new DataTableResource(this.categoryItems);
          this.categoryResource.count().then(count => this.categoryCount = count);
          this.categoryCount = Number(this.categoryItems.length);

          if (this.gymListService.SelectedGym) {
            this.fetchData(this.gymListService.SelectedGym);
          }

        } else {
        }
      }, null, null);

    this.invokeEventSubscribe = this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      if (value.some) {
        this.fetchData(value.some)
      }
    }, null, null);
  }


fetchData(branch) {
    this.branch = branch;
    this.adminService.getGymSettings('MEMBERSEARCH', null, this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      settingsResult => {
        if (settingsResult) {
          this.searchMemberSetting = settingsResult.Data[0]
          // if (this.searchMemberSetting.MemberSearchText){
          this.codeArray = this.searchMemberSetting.MemberSearchText.replace(/(-\w*[Wæøå]*\s*\w*[Wæøå]*[\/])/ig, ",").split(",")
          this.codeArray = this.codeArray.slice(0, this.codeArray.length - 1)
          this.SetMemberSearchString(this.searchMemberSetting.MemberSearchText)
          // this.codeArray=this.searchMemberSetting.MemberSearchText.split(/(-\w*\s*\w*[\/])/)

          // }
        }
      }, null, null);
  }

  saveSettings() {
    this.searchMemberSetting.MemberSearchText = this.memberSearchText
    this.adminService.SaveGymMemberSearchSettings(this.searchMemberSetting).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.isDisabled = true;
          if (result.Status === 'OK') {
            this.translateService.get('ADMIN.SaveSuccess').pipe(
              takeUntil(this.destroy$)
            ).subscribe(translatedValue => this.message = translatedValue);
            this.type = 'SUCCESS';
            this.isShowInfo = true;
            setTimeout(() => {
              this.isShowInfo = false;
            }, 4000);
          }
        }
      }, error => {
        this.translateService.get('ADMIN.SaveNotSuccess').pipe(
          takeUntil(this.destroy$)
        ).subscribe(translatedValue => this.message = translatedValue);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      }, null);
  }

  SetMemberSearchString(membserSearchText: string) {

    this.memberSearchOrder = '';
    this.settingsArray = [];
    let tempArray = []
    tempArray = membserSearchText.split(/[-/]/);
    let i = 1
    tempArray.forEach(element => {
      if (i % 3 === 0) {
        this.memberSearchOrder += element + ','
      }
      i++
    });
    this.settingsArray = this.memberSearchOrder.split(',')
    this.sortCategories(true);
  }

  sortCategories(isAddNew?) {
    if (!isAddNew) {
      this.isDisabled = false
      this.memberSearchText = ''
    }

    this.sortedCategories = []
    // this.settingsArray.forEach(element => {
    //   this.categoryItems.forEach(catItem => {
    //     // tslint:disable-next-line:one-line
    //     if (catItem.Name === element) {
    //       this.sortedCategories.push(catItem)
    //       if (!isAddNew) {
    //         this.memberSearchText += catItem.Code + '-' + catItem.Name + '/'
    //       }
    //     }
    //   })
    // })
    this.codeArray.forEach(element => {
      this.categoryItems.forEach(catItem => {
        // tslint:disable-next-line:one-line
        if (catItem.Code === element) {
          this.sortedCategories.push(catItem)
          if (!isAddNew) {
            this.memberSearchText += catItem.Code + '-' + catItem.Name + '/'
          }
        }
      })
    })
    if (this.sortedCategories.length > 0) {
      this.categoryItems = this.sortedCategories
    }
  }

  moveUp(item) {
    const itemIndexSystem = this.settingsArray.indexOf(item.Name)
    const itemIndex = this.codeArray.indexOf(item.Code)
    if (itemIndex !== 0) {
      this.arrayMove(this.codeArray, itemIndex, itemIndex - 1)
      this.arrayMove(this.settingsArray, itemIndex, itemIndex - 1)
      this.sortCategories()
      this.setSearchOrderString()
    }
  }

  moveDown(item) {
    const itemIndexSystem = this.settingsArray.indexOf(item.Name)
    const itemIndex = this.codeArray.indexOf(item.Code)
    if (itemIndex + 1 !== this.codeArray.length) {
      this.arrayMove(this.codeArray, itemIndex, itemIndex + 1)
      this.arrayMove(this.settingsArray, itemIndex, itemIndex + 1)
      this.sortCategories()
      this.setSearchOrderString()
    }
  }

  setSearchOrderString() {
    this.memberSearchOrder = ''
    this.settingsArray.forEach(setting => {
      this.memberSearchOrder += setting + ','
    })
  }

  arrayMove(array, fromIndex, toIndex) {
    const element = array[fromIndex];
    array.splice(fromIndex, 1);
    array.splice(toIndex, 0, element);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
