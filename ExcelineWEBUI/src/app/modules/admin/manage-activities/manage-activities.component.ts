import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { DataTableResource } from '../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { AdminService } from '../services/admin.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ExceError } from '../../../shared/directives/exce-error/exce-error';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-manage-activities',
  templateUrl: './manage-activities.component.html',
  styleUrls: ['./manage-activities.component.scss'],
  providers: []
})

export class ManageActivitiesComponent implements OnInit, OnDestroy {

  private itemResource: DataTableResource<any>;
  private items = [];
  itemCount = 0;
  manageActivities?: any[] = [];
  private categoryTypes: any;
  private selectedCategoryType: any;
  private savedId;
  private activityId;
  private activityForm: FormGroup;
  private categories?: any[];
  private modalReference?: any;
  private addNew: any;
  private generatedCode: any;
  private filteredCategories?: any[];
  private param;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  branchId: number;
  private actSettings: any;
  private actSettingsData?: any[];
  public activitySettingsForm: FormGroup;
  private dataList;
  private destroy$ = new Subject<void>();

  constructor(
    private adminService: AdminService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private exceLoginService: ExceLoginService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }, error => {

      }, () => {

      }
    );

    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.ActivitiesC',
      url: '/adminstrator/manage-activities'
    });

    this.activitySettingsForm = this.fb.group({
      'bookDay': [null, [Validators.required]],
      'activityName': [null, [Validators.required]],
      'contractTimeLimited': [null],
      'punchCardLimited': [null],
      'contractbooking': [null],
      'trail': [null],
      'smsReminder': [null],
      'bookViaIbooking': [null],
      'bookingActivity': [null],
      'basicActivity': [null],
      'activityId': [null]
    });
  }

  openActivityModel(content) {
    this.modalReference = this.modalService.open(content, { width: '800' });
  }

  closeView() {
    this.modalReference.close();
  }

  ngOnInit() {
    this.GetActivities();
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }

    this.destroy$.next();
    this.destroy$.complete();
  }

  // get the Activity names for the data table
  GetActivities() {
    this.adminService.getActivities().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
        if (result) {
          this.items = result.Data;
          this.manageActivities = this.items;
          this.itemCount = this.manageActivities.length;
          this.itemResource = new DataTableResource(this.manageActivities);
          this.itemResource.count().then(count => this.itemCount = count);
        }
      },
        error => {
          console.log(error);
        }, () => {

        });
  }

  // filter by name (search field)
  filterInput(param?: any) {
    this.manageActivities = this.filterpipe.transform('Name', 'NAMEFILTER', this.items, param);
  }

  closeModel() {
    this.resetForm();
    this.modalReference.close();
  }

  // special properties:
  rowDoubleClick(rowEvent) {
    this.actSettings = rowEvent.row.item;
    this.activitySettingsForm.controls['activityId'].setValue(this.actSettings.ActivitySetting.ActivityId);
    this.activitySettingsForm.controls['activityName'].setValue(this.actSettings.ActivitySetting.ActivityName);
    this.activitySettingsForm.controls['bookDay'].setValue(this.actSettings.ActivitySetting.DaysInAdvanceForBooking);
    this.activitySettingsForm.controls['contractTimeLimited'].setValue(this.actSettings.ActivitySetting.IsContractTimeLimited);
    this.activitySettingsForm.controls['punchCardLimited'].setValue(this.actSettings.ActivitySetting.IsPunchCardLimited);
    this.activitySettingsForm.controls['contractbooking'].setValue(this.actSettings.ActivitySetting.IsContractBooking);
    this.activitySettingsForm.controls['trail'].setValue(this.actSettings.ActivitySetting.IsTrial);
    this.activitySettingsForm.controls['smsReminder'].setValue(this.actSettings.ActivitySetting.IsSMSRemindered);
    this.activitySettingsForm.controls['bookViaIbooking'].setValue(this.actSettings.ActivitySetting.IsBookViaIbooking);
    this.activitySettingsForm.controls['bookingActivity'].setValue(this.actSettings.ActivitySetting.IsBookingActivity);
    this.activitySettingsForm.controls['basicActivity'].setValue(this.actSettings.ActivitySetting.IsBasicActivity);
  }

  resetForm() {
    this.activitySettingsForm.reset();
  }

  addActivitySettings(activitySettingsData) {
    this.actSettings.ActivitySetting.ActivityId = activitySettingsData.activityId;
    this.actSettings.ActivitySetting.ActivityName = activitySettingsData.activityName;
    this.actSettings.ActivitySetting.DaysInAdvanceForBooking = activitySettingsData.bookDay; // daysInAdvanceForBooking in ActivitySettingsDc
    this.actSettings.ActivitySetting.IsContractTimeLimited = activitySettingsData.contractTimeLimited;
    this.actSettings.ActivitySetting.IsPunchCardLimited = activitySettingsData.punchCardLimited;
    this.actSettings.ActivitySetting.IsContractBooking = activitySettingsData.contractbooking;
    this.actSettings.ActivitySetting.IsTrial = activitySettingsData.trail;
    this.actSettings.ActivitySetting.IsSMSRemindered = activitySettingsData.smsReminder;
    this.actSettings.ActivitySetting.IsBookViaIbooking = activitySettingsData.bookViaIbooking;
    this.actSettings.ActivitySetting.IsBookingActivity = activitySettingsData.bookingActivity;
    this.actSettings.ActivitySetting.IsBasicActivity = activitySettingsData.basicActivity;
    this.dataList = activitySettingsData;

    this.adminService.AddActivitySettings(this.actSettings.ActivitySetting).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
    },
      error => {
        console.log(error);
      }, () => {

      });
    this.closeModel();
  }

  getData(value) {
    this.GetActivities();
  }

  reloadActivityItems(params) {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.manageActivities = items);
    }
  }

}
