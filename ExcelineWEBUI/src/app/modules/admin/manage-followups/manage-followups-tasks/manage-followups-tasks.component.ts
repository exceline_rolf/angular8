import { Component, OnInit, Type, OnDestroy } from '@angular/core';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { AdminService } from '../../services/admin.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { CommonUITypes } from 'app/shared/enums/us-enum';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-manage-followups-tasks',
  templateUrl: './manage-followups-tasks.component.html',
  styleUrls: ['./manage-followups-tasks.component.scss']
})

export class ManageTasksComponent implements OnInit, OnDestroy {
  private _user: any;
  private items = [];
  private itemResource: DataTableResource<any>;
  private branchId: number = JSON.parse(Cookie.get('selectedBranch')).BranchId;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private editTaskCategories: any;
  private uiTypes = CommonUITypes;
  private modalReference?: any;
  private deletedItem: any;
  private modal: any;
  private deletedItemId: any;
  private categoryId: any;
  private categoryType: any;
  private extendedFieldsList;
  private extFields: any;
  private destroy$ = new Subject<void>();
  private searchValue = '';
  itemCount = 0;
  taskCategories?: any[] = [];
  editTaskCategoriesForm: FormGroup;
  extendedFieldForm: FormGroup;
  extendedFields = [];
  myFormSubmited = false;
  mFormSubmited = false;

  formErrors = {
    'Name': '',
    'Title': ''
  };
  validationMessages = {
    'Name': {
      'required': 'ADMIN.Required'
    },
    'Title': {
      'required': 'ADMIN.FieldNameRequired'
    }
  };

  constructor(
    private loginService: ExceLoginService,
    private modalService: UsbModal,
    private adminService: AdminService,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.TaskCategoriesC',
      url: '/adminstrator/manage-followups-tasks'
    });

    this._user = this.loginService.CurrentUser.username;

    this.editTaskCategoriesForm = fb.group({
      'Id': [-1],
      'Name': [null, [Validators.pattern('.*\\S.*'), Validators.required]],
      'Text': [null],
      'IsStartDate': [false],
      'IsEndDate': [false],
      'IsStartTime': [false],
      'IsEndTime': [false],
      'IsPhoneNo': [false],

      'Description': [null],
      'IsFoundDate': [false],
      'IsReturnDate': [false],
      'IsDueDate': [false],
      'IsDueTime': [false],
      'IsNoOfDays': [null],

      'IsAssignToEmp': [null],
      'IsAssignToRole': [null],
      'IsAutomatedEmail': [false],
      'IsAutomatedSMS': [false],
      'IsNextFollowUp': [false]
    });

    this.extendedFieldForm = fb.group({
      'Title': [null, [Validators.pattern('.*\\S.*'), Validators.required]],
      'FieldType': [null]
    });

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.yesHandler();
    });
    this.exceMessageService.noCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.noHandler();
    });

    // this.keys = Object.keys(this.uiTypes).filter(f => !isNaN(Number(f)));
  }

  keys(): Array<string> {
    const key = Object.keys(this.uiTypes);
    return key.slice(key.length / 2);
  }

  ngOnInit() {
    this.getTaskCategories();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterContentInit() {
    this.editTaskCategoriesForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(_ => {
      UsErrorService.onValueChanged(this.editTaskCategoriesForm, this.formErrors, this.validationMessages)
    });
  }

  // get Task Categories data for the data table
  getTaskCategories() {
    this.adminService.getTaskCategories(this.branchId).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.items = result.Data;
        this.taskCategories = this.items;
        this.itemResource = new DataTableResource(this.taskCategories);
        this.itemResource.count().then(count => this.itemCount = count);
        this.itemCount = Number(result.Data.length);
      }
    }, error => {
        console.log(error);
    });
  }

  // to open the New Task Modal
  openNewTaskModal(content) {
    this.extendedFields = [];
    this.modalReference = this.modalService.open(content);
  }

  // filter by name (search field)
  filterInput(param?: any) {
    if (param.length >= 1) {
      this.taskCategories = this.filterpipe.transform('Name', 'NAMEFILTER', this.items, param);
    } else {
      this.getTaskCategories();
    }
  }

  // row double click event
  rowDoubleClick(rowEvent, value) {
    this.editTaskCategories = rowEvent.row.item;
    this.editTaskCategoryDataBind(rowEvent.row.item);
    this.categoryId = rowEvent.row.item.Id;

    // get extended fields for the data table
    this.adminService.getExtFieldsByCategory(this.categoryId, 'FOLLOWUP').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.items = result.Data;
        this.extendedFields = this.items;
      }
    });
  }

  // Load table row data to the form (Edit Button)
  rowItemLoad(rowItem, field) {
    this.editTaskCategories = rowItem;
    this.editTaskCategoryDataBind(rowItem);
    this.categoryId = rowItem.Id;

    // get extended fields for the data table
    this.adminService.getExtFieldsByCategory(this.categoryId, 'FOLLOWUP').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.items = result.Data;
        this.extendedFields = this.items;
      }
    });
  }

  // to patch values to the editTaskCategoriesForm
  private editTaskCategoryDataBind(item) {
    item.ActiveStatus = true;
    item.CreatedUser = this._user;
    this.editTaskCategories.Text = true;
    this.editTaskCategories.Description = true;
    this.editTaskCategories.IsNoOfDays = true;
    this.editTaskCategories.IsAssignToEmp = true;
    this.editTaskCategories.IsAssignToRole = true;

    this.editTaskCategoriesForm.patchValue(item);
  }

  // to save and update new task category to the database
  saveTaskCategory(value): any {
    value.CreatedUser = this._user;
    value.ExtendedFieldsList = this.extendedFields;
    value.ActiveStatus = true;
    value.Text = true;
    value.Description = true;
    value.IsAssignToEmp = true;
    value.IsAssignToRole = true;
    value.IsNoOfDays = true;

    // isEdit = true => edit / isEdit = false => Add
    if (value.Id >= 0) {
      value.isEdit = true;
    } else {
      value.isEdit = false;
    }

    if (value.Name !== null) {
      value.Name = value.Name.trim();
    }

    if (value.Name == null || value.Name == '') {
      this.editTaskCategoriesForm.controls['Name'].setErrors({ 'required': true });
    } else {
      this.editTaskCategoriesForm.controls['Name'].setErrors(null);
    }

    const taskCategoryObject = {
      taskCategoryList: value,
      isEdit: value.isEdit
    }

    this.myFormSubmited = true;
    if (this.editTaskCategoriesForm.valid) {
      this.adminService.saveTaskCategory(taskCategoryObject).pipe(
        takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          this.getTaskCategories();
        }
      }, error => {
          console.log(error);
      });
      this.closeModel();
      this.extendedFieldForm.reset();
      this.myFormSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.editTaskCategoriesForm, this.formErrors, this.validationMessages);
    }

    // Empty search field
    this.searchValue = '';
  }

  // delete unavailable times from the db
  deleteTaskCategory(item) {
    this.deletedItem = item;
    this.deletedItemId = item.Id;
    let messageTitle = '';
    let messageBody = '';
    (this.translateService.get('Confirm').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => messageTitle = tranlstedValue));
    (this.translateService.get('MEMBERSHIP.ConfirmDelete').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => messageBody = tranlstedValue));
    this.modal = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: messageTitle,
        messageBody: messageBody
      }
    );
    this.taskCategories = this.items;
  }

  // delete Yes button
  yesHandler() {
    const index: number = this.items.indexOf(this.deletedItem);

    if (index !== -1) {
      this.items.splice(index, 1);
    } else {
    }

    this.adminService.deleteTaskCategory(this.deletedItemId).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {

    }, error => {
        console.log(error);
    });
  }

  noHandler() {
  }

  // reset the editTaskCategories Form
  resetEditTaskCategoriesForm() {
    this.editTaskCategoriesForm.reset();
    this.myFormSubmited = false;
    this.mFormSubmited = false;
  }

  // to reset editTaskCategoriesForm and close the NewTaskModal
  closeModel() {
    this.resetEditTaskCategoriesForm();
    this.extendedFieldForm.reset();
    this.modalReference.close();
    this.myFormSubmited = false;
  }

  // to reload the datatable items
  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.taskCategories = items);
    }
  }

  // to add extended fields
  addExtendedFields(value) {
    // when click add button fill the tabel not to the database;
    if (value.Title !== null) {
      value.Title = value.Title.trim();
    }

    if (value.Title == null || value.Title == '') {
      this.extendedFieldForm.controls['Title'].setErrors({ 'required': true });
    } else {
      this.extendedFieldForm.controls['Title'].setErrors(null);
    }

    const newField = {
      'Title': value.Title,
      'FieldType': value.FieldType
    }

    this.mFormSubmited = true;
    if (this.extendedFieldForm.valid) {
      this.extendedFields.push(newField);
      this.extendedFieldForm.reset();
      this.mFormSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.extendedFieldForm, this.formErrors, this.validationMessages);
    }
  }

  // delete fields from the data table
  deleteField(field) {
    this.extendedFields.splice(this.extendedFields.indexOf(field), 1);
  }

  ngOnDestroy(): void {
    if (this.modal) {
      this.modal.unsubscribe();
    }
    if (this.modalReference) {
      this.modalReference.close();
    }

    this.destroy$.next();
    this.destroy$.complete();
  }
}
