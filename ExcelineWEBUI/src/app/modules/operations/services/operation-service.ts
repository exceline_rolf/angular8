import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';

@Injectable()
export class OperationService {

  private operationServiceUrl: string;
  private branchId: number;
  constructor(private commonHttpService: CommonHttpService, private config: ConfigService, private exceLoginService: ExceLoginService) {
    this.operationServiceUrl = this.config.getSettings('EXCE_API.MEMBER');
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;

      }
    );

  }

  getARXList(SystemId): Observable<any> {
    const url = this.operationServiceUrl + 'Members/ARX/' + SystemId + '/' + this.branchId + '/0/ExcelineAcc123@/xsw2zaq1'
    // return this.commonHttpService.makeHttpCall(url, {
    //   method: 'GET',
    //   auth: true
    // })
    return of( 'ARX list Q,W,W,E,R,T A, B, C , D, E, F ,G ,H ,I,J').pipe(
      map(o => JSON.stringify(o))
    )
  }

  getChangedList(SystemId): Observable<any> {
    const url = this.operationServiceUrl + 'Members/ARX/' + SystemId + '/' + this.branchId + '/0/ExcelineAcc123@/xsw2zaq1'
    // return this.commonHttpService.makeHttpCall(url, {
    //   method: 'GET',
    //   auth: true
    // })
    return of( {'Data': 'CHanged list A, B, C , D, E, F ,G ,H ,I,J'}).pipe(
      map(o => JSON.stringify(o))
    )
    // return {'Data': 'CHanged list A, B, C , D, E, F ,G ,H ,I,J'}
  }

  getSystemId(): Observable<any> {
    const url = this.operationServiceUrl
    // return this.commonHttpService.makeHttpCall(url, {
    //   method: 'GET',
    //   auth: true
    // })
    return of({'Data': '1'}).pipe(
      map(o => JSON.stringify(o))
    )
    // return {'Data': '1'}
  }


}
