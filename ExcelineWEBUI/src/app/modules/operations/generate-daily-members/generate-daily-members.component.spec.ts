import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateDailyMembersComponent } from './generate-daily-members.component';

describe('GenerateDailyMembersComponent', () => {
  let component: GenerateDailyMembersComponent;
  let fixture: ComponentFixture<GenerateDailyMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateDailyMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateDailyMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
