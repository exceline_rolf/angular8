import { Component, Injectable, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../../membership/services/exce-member.service'
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { USSAdminService } from '../../../services/uss-admin-service'
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { TreeNode } from 'app/shared/components/us-tree-table/tree-node';
import { UsErrorService } from '../../../../../shared/directives/us-error/us-error.service'
import { TreeviewItem, TreeviewConfig, DownlineTreeviewItem, TreeviewHelper } from 'app/shared/components/us-checkbox-tree';
import { CommonEncryptionService } from 'app/shared/services/common-Encryptor-Decryptor';
import { ColumnDataType, MemberSearchType } from 'app/shared/enums/us-enum';
import * as _ from 'lodash';
import { Dictionary } from 'lodash';
import * as CryptoJS from 'crypto-js';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
const now = new Date();

@Injectable()
export class ProductTreeviewConfig extends TreeviewConfig {
  isShowAllCheckBox = true;
  isShowFilter = true;
  isShowCollapseExpand = false;
  maxHeight = 500;
}

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})

export class UserDetailsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId: number;
  private itemResource: any = 0;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private modalReference: any;
  formSubmited = false;
  private subModellReference: any;
  private emp = [];
  private branchItems = [];
  private branchCount = 0;
  private branchResource: any = 0;
  subItems = [];
  subCount = 0;
  private subResource: any = 0;
  private branchesList = []
  hwProfiles = []
  roles = []
  private selectedBranches = []
  private username: string;
  userDetailsForm: any;
  selectedBranchCount = 0;
  private popRef: any;
  private searchVal = '';
  private filteredEmp: any;
  private moduleList: any;
  private subOpList: any;
  private rowSubItem: any;
  private OperationField: any;
  moduleFeatureItems = []
  private defaultRoles: any;
  private selectedRole: any;
  private isRoleChanged = false;
  private selectedOpList: any = [];
  private uniqModTrue = []
  private uniqModFalse = []
  private uniqFeatTrue = []
  private uniqFeatFalse = []
  private uniqOperTrue = []
  private uniqOperFalse = []
  private gymCode = ''
  private employeeId: number;
  private pwModelRef: any;
  private userSettingsForm: any;
  private isSubmitChangePw = false;
  private isPwResetDisabled = false;
  private rows = []
  private uniqFeatList: Dictionary<number> = {};
  private uniqOperList: Dictionary<number> = {};
  private uniqMOdList: Dictionary<number> = {};
  private allBranchesSelected = false;



  @Input() selectedItem: any;
  @Input() isAddNew: true;
  @Output() canceled = new EventEmitter();
  @Output() saveSuccess = new EventEmitter();

  formErrors = {
    'CardNumber': '',
    'DisplayName': '',
    'email': '',
    'HardwareProfileId': '',
    'passowrd': '',
    'RoleId': '',
    'IsActive': '',
    'ConfirmPassowrd': '',
    'UserName': '',
    'Branch': '',
  };

  validationMessages = {

    'CardNumber': { 'required': 'value required.', 'cardNoNotAvailable': 'Card No is not available' },
    'DisplayName': { 'required': 'value required.', },
    'email': { 'required': 'value required.', 'pattern': 'Enter a valid email address.' },
    'HardwareProfileId': { 'required': 'value required.', },
    'passowrd': { 'required': 'value required.', },
    'RoleId': { 'required': 'value required.', },
    'IsActive': { 'required': 'value required.', },
    'ConfirmPassowrd': { 'required': 'value required.', 'pwDontMatch': 'Please retype password correctly' },
    'UserName': { 'required': 'value required.', },
    'Branch' : { 'required': 'USS-ADMIN.MinOneBranch', },
  };

  auotocompleteItems = [{ id: 'Name', name: 'Name :', isNumber: false }, { id: 'Id', name: 'Id :', isNumber: false }]

  columns = [{ property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'IntCustId', header: 'Number', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DEFAULT },
  ]

  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };

  constructor(private exceMemberService: ExceMemberService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private ussAdminService: USSAdminService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private commonEncryptionService: CommonEncryptionService,
  ) {

    this.username = this.exceLoginService.CurrentUser.username
    this.gymCode = this.username.split('/')[0]
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  ngOnInit() {
    if (this.isAddNew) {
      this.userDetailsForm = this.fb.group({
        'CardNumber': [null,],
        'DisplayName': [null, [Validators.required]],
        'email': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]],
        'HardwareProfileId': [null],
        'passowrd': [null, [Validators.required]],
        'RoleId': [null, [Validators.required]],
        'IsActive': [true],
        'ConfirmPassowrd': [null, [Validators.required]],
        'UserName': [null, [Validators.required]],
        'Branch': [null, [Validators.required] ],
      });
    } else {
      this.userDetailsForm = this.fb.group({
        'CardNumber': [null, ],
        'DisplayName': [null, [Validators.required]],
        'email': [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')]],
        'HardwareProfileId': [null],
        'passowrd': [null, ],
        'RoleId': [null, [Validators.required]],
        'IsActive': [null],
        'ConfirmPassowrd': [null, ],
        'UserName': [null, [Validators.required]],
        'Branch': [null, [Validators.required] ],
      });
    }

    // if (!this.isAddNew) {
    //   console.log(this.username.toUpperCase(), this.exceLoginService.CurrentUser)
    //   if (this.username.toUpperCase() !== this.selectedItem.UserName.toUpperCase()) {
    //     this.isPwResetDisabled = true;
    //   }
    // }

    if (!this.isAddNew) {
      for (const key in this.selectedItem.UniqueModuleList) {
        if (this.selectedItem.UniqueModuleList[key] === 1) {
          this.uniqModTrue.push(+key)
        } else {
          this.uniqModFalse.push(+key)
        }
      }
      for (const key in this.selectedItem.UniqueFeatureList) {
        if (this.selectedItem.UniqueFeatureList[key] === 1) {
          this.uniqFeatTrue.push(+key)
        } else {
          this.uniqFeatFalse.push(+key)
        }
      }


      for (let key in this.selectedItem.UniqueOperationIdList) {
        if (this.selectedItem.UniqueOperationIdList[key] === 1) {
          this.uniqOperTrue.push(+key)
        } else {
          this.uniqOperFalse.push(+key)
        }
      }


      this.ussAdminService.GetDefaultUspRole(this.selectedItem.UserName).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result) {
          this.defaultRoles = result.Data;
        }
      });
    }

    this.ussAdminService.GetBranches().pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.branchesList = result.Data
        if (!this.isAddNew) {
          this.ussAdminService.GetUserSelectedBranches(this.selectedItem.UserName).pipe(
            takeUntil(this.destroy$)
            ).subscribe(resultS => {
            if (resultS) {
              this.selectedBranches = resultS.Data
              this.branchesList.forEach(element => {
                this.selectedBranches.forEach(elementS => {
                  if (elementS.BranchName) {
                    if (element.Id.toString() === elementS.BranchName.split('_')[0]) {
                      element.IsCheck = true
                      this.selectedBranchCount++;
                    }
                  }
                });
              })
            }
          })
        }
      }
      this.branchResource = new DataTableResource(this.branchesList);
      this.branchResource.count().then(count => this.branchCount = count);
      this.branchItems = this.branchesList;
      this.branchCount = Number(this.branchesList.length);
    })
    this.ussAdminService.GetHardwareProfiles(this.branchId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.hwProfiles = result.Data
        this.ussAdminService.GetUSPRoles('', true).pipe(
          takeUntil(this.destroy$)
          ).subscribe(resultRoles => {
          if (resultRoles) {
            this.roles = resultRoles.Data;
            for (let i = 0; i < this.roles.length; i++) {
              if (!this.isAddNew) {
                if (this.roles[i].Id === this.selectedItem.RoleId) {
                  this.selectedRole = this.roles[i]
                  this.selectedRole.OperationList.forEach(element => {
                    this.selectedOpList.push(element.ID)
                  });
                  break;
                }
              }
            }
            this.getModuleFetures(this.roles[0].Id)
            this.subOperations(this.roles[0].Id);
            if ((!this.isAddNew) && (this.selectedItem)) {
              this.userDetailsForm.patchValue(this.selectedItem)
            }
          }
        })
      }
    });

    this.ussAdminService.isUserAdmin().pipe(
      takeUntil(this.destroy$)
      ).subscribe( isAdminRes => {
      if (!this.isAddNew && isAdminRes === 'ADMIN') {
        this.isPwResetDisabled = true;
      }
    })
  }

  subOperations(value?) {
    if (this.isAddNew) {
      this.ussAdminService.GetSubOperationsForRole(value).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        this.subOpList = result.Data
        this.subResource = new DataTableResource(this.subOpList);
        this.subResource.count().then(count => this.subCount = count);
        this.subItems = this.subOpList;
        this.subCount = Number(result.Data.length);
      });
    } else {
      this.ussAdminService.GetSubOperationsForUser(this.selectedItem.UserName).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result) {
          this.subOpList = result.Data
          this.subResource = new DataTableResource(this.subOpList);
          this.subResource.count().then(count => this.subCount = count);
          this.subItems = this.subOpList;
          this.subCount = Number(result.Data.length);
        }
      });
    }
  }

  getModuleFetures(value?) {
    if (this.isAddNew) {
      this.ussAdminService.GetModulesFeaturesOperationsConstrained(this.username).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result) {
          this.moduleList = result.Data
        }
      })
    } else {
      this.ussAdminService.GetModulesFeaturesOperationsConstrained(this.username).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result) {
          this.moduleList = result.Data
          this.loadTree()
        }
      });
    }
  }

  loadTree() {
    this.moduleFeatureItems = []
    this.moduleList.forEach(element => {
      let jsm
      if (this.isRoleChanged) {
        if (this.selectedRole.ModuleList.filter(x => x === element.ModuleId).length > 0) {
          jsm = new TreeviewItem({ text: element.DisplayName, value: element.ModuleId, checked: true, propertyVal: 'MODULE' })
        } else {
          jsm = new TreeviewItem({ text: element.DisplayName, value: element.ModuleId, checked: false, propertyVal: 'MODULE' })
        }
      } else {
        if (this.selectedRole.ModuleList.filter(x => x === element.ModuleId).length > 0) {
          if (this.uniqModFalse.filter(x => x === element.ModuleId).length > 0) {
            jsm = new TreeviewItem({ text: element.DisplayName, value: element.ModuleId, checked: false, propertyVal: 'MODULE' })
          } else {
            jsm = new TreeviewItem({ text: element.DisplayName, value: element.ModuleId, checked: true, propertyVal: 'MODULE' })
          }
        } else if (this.uniqModTrue.filter(x => x === element.ModuleId).length > 0) {
          jsm = new TreeviewItem({ text: element.DisplayName, value: element.ModuleId, checked: true, propertyVal: 'MODULE' })
        } else {
          jsm = new TreeviewItem({ text: element.DisplayName, value: element.ModuleId, checked: false, propertyVal: 'MODULE' })
        }
      }
      let childMod = []
      let childModOp = []

      element.Operations.forEach(elementOp => {
        if (this.isRoleChanged) {
          if (this.selectedOpList.filter(x => x === elementOp.OperationId).length > 0) {
            childModOp.push(new TreeviewItem({ text: elementOp.DisplayName, value: elementOp.OperationId, checked: true, propertyVal: 'OPERATION' }))
          } else {
            childModOp.push(new TreeviewItem({ text: elementOp.DisplayName, value: elementOp.OperationId, checked: false, propertyVal: 'OPERATION' }))
          }
        } else {
          if (this.selectedOpList.filter(x => x === elementOp.OperationId).length > 0) {
            if (this.uniqOperFalse.filter(x => x === elementOp.OperationId).length > 0) {
              childModOp.push(new TreeviewItem({ text: elementOp.DisplayName, value: elementOp.OperationId, checked: false, propertyVal: 'OPERATION' }))
            } else {
              childModOp.push(new TreeviewItem({ text: elementOp.DisplayName, value: elementOp.OperationId, checked: true, propertyVal: 'OPERATION' }))
            }
          } else if (this.uniqOperTrue.filter(x => x === elementOp.OperationId).length > 0) {
            childModOp.push(new TreeviewItem({ text: elementOp.DisplayName, value: elementOp.OperationId, checked: true, propertyVal: 'OPERATION' }))
          } else {
            childModOp.push(new TreeviewItem({ text: elementOp.DisplayName, value: elementOp.OperationId, checked: false, propertyVal: 'OPERATION' }))
          }
        }
      });

      element.Features.forEach(elementf => {
        let jsf
        if (this.isRoleChanged) {
          if (this.selectedRole.FeatureList.filter(x => x === elementf.FeatureId).length > 0) {
            jsf = new TreeviewItem({ text: elementf.DisplayName, value: elementf.FeatureId, checked: true, propertyVal: 'FEATURE' })
          } else {
            jsf = new TreeviewItem({ text: elementf.DisplayName, value: elementf.FeatureId, checked: false, propertyVal: 'FEATURE' })
          }

        } else {
          if (this.selectedRole.FeatureList.filter(x => x === elementf.FeatureId).length > 0) {
            if (this.uniqFeatFalse.filter(x => x === elementf.FeatureId).length > 0) {
              jsf = new TreeviewItem({ text: elementf.DisplayName, value: elementf.FeatureId, checked: false, propertyVal: 'FEATURE' })
            } else {
              jsf = new TreeviewItem({ text: elementf.DisplayName, value: elementf.FeatureId, checked: true, propertyVal: 'FEATURE' })
            }
          } else if (this.uniqFeatTrue.filter(x => x === elementf.FeatureId).length > 0) {
            jsf = new TreeviewItem({ text: elementf.DisplayName, value: elementf.FeatureId, checked: true, propertyVal: 'FEATURE' })
          } else {
            jsf = new TreeviewItem({ text: elementf.DisplayName, value: elementf.FeatureId, checked: false, propertyVal: 'FEATURE' })
          }
        }
        let childF = []
        elementf.Operations.forEach(elementO => {
          if (this.isRoleChanged) {
            if (this.selectedOpList.filter(x => x === elementO.OperationId).length > 0) {
              childF.push(new TreeviewItem({ text: elementO.DisplayName, value: elementO.OperationId, checked: true, propertyVal: 'OPERATION' }))
            } else {
              childF.push(new TreeviewItem({ text: elementO.DisplayName, value: elementO.OperationId, checked: false, propertyVal: 'OPERATION' }))
            }
          } else {
            if (this.selectedOpList.filter(x => x === elementO.OperationId).length > 0) {
              if (this.uniqOperFalse.filter(x => x === elementO.OperationId).length > 0) {
                childF.push(new TreeviewItem({ text: elementO.DisplayName, value: elementO.OperationId, checked: false, propertyVal: 'OPERATION' }))
              } else {
                childF.push(new TreeviewItem({ text: elementO.DisplayName, value: elementO.OperationId, checked: true, propertyVal: 'OPERATION' }))
              }
            } else if (this.uniqOperTrue.filter(x => x === elementO.OperationId).length > 0) {
              childF.push(new TreeviewItem({ text: elementO.DisplayName, value: elementO.OperationId, checked: true, propertyVal: 'OPERATION' }))
            } else {
              childF.push(new TreeviewItem({ text: elementO.DisplayName, value: elementO.OperationId, checked: false, propertyVal: 'OPERATION' }))
            }
          }
        });
        if (childF.length > 0) {
          jsf.children = childF
        }
        childMod.push(jsf)
      });
      if (childModOp.length > 0) {
        childMod.push(new TreeviewItem({ text: '', value: '', children: childModOp, propertyVal: '' }))
      }
      if (childMod.length > 0) {
        jsm.children = childMod
      }
      this.moduleFeatureItems.push(jsm)
    });
  }

  branchSelection(event) {
    if (event.target.checked) {
      this.selectedBranchCount++
    } else {
      this.selectedBranchCount--
    }
  }

  openPopOver(popOver) {
    this.popRef = popOver
    this.popRef.open()
  }

  closePop() {
    if (this.popRef) {
      this.popRef.close();
  }
  }

  openSubModel(subModel: any) {
    this.empSearch('');
    this.subModellReference = this.modalService.open(subModel);
  }

  closePage() {
    this.canceled.emit()
  }


  onSelect(item: any) {
    this.searchVal = item.searchVal;
  }

  onDefaultSearch(item: any) {
    this.searchVal = item.searchVal;
  }

  reloadItems(params): void {
  }

  empSearch(val) {
    this.ussAdminService.SearchUSPEmployees(val, true).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.emp = result.Data;
      }
    });
  }

  openSubOp(content, item) {
    this.rowSubItem = item;
    this.OperationField = item.ExtendedInfo.OperationFieldList[0]
    this.modalReference = this.modalService.open(content, { width: '800' });
  }

  roleChange(value) {
    this.selectedOpList = []
    this.subOperations(value);
    this.roles.forEach(ele => {

      if (ele.Id === value) {
        this.selectedRole = ele
        this.selectedRole.OperationList.forEach(element => {
          this.selectedOpList.push(element.ID)
        });
        this.isRoleChanged = true
        this.loadTree()
      }
    });
  }


  onSelectedChange(downlineItems) {
    this.rows = [];
  }

  treeChange(event) {
    if (event.value && event.propertyVal) {
      if (event.propertyVal === 'MODULE') {
        if (!(event.value in this.selectedRole.ModuleList)) {
          if (this.uniqMOdList[event.value]) {
            delete this.uniqMOdList[event.value];
          }
          if (event.checked) {
            this.uniqMOdList[event.value + ''] = 1;
          } else {
            this.uniqMOdList[event.value + ''] = 0;
          }
        }

      } else if (event.propertyVal === 'FEATURE') {
        if (!(event.value in this.selectedRole.FeatureList)) {
          if (this.uniqFeatList[event.value]) {
            delete this.uniqFeatList[event.value];
          }
          if (event.checked) {
            this.uniqFeatList[event.value + ''] = 1;
          } else {
            this.uniqFeatList[event.value + ''] = 0;
          }
        }

      } else {
        if (!(event.value.OperationId in this.selectedOpList)) {
          if (this.uniqOperList[event.value.OperationId]) {
            delete this.uniqOperList[event.value.OperationId];
          }
          if (event.checked) {
            this.uniqOperList[event.value + ''] = 1;
          } else {
            this.uniqOperList[event.value + ''] = 0;
          }
        }
      }
    }
  }

  validateUserForm(value) {
    if (this.isAddNew) {
      if (value.ConfirmPassowrd !== value.passowrd) {
        this.userDetailsForm.controls['ConfirmPassowrd'].setErrors({
          'pwDontMatch': false,
        });
      }
      if (value.CardNumber) {
        this.ussAdminService.CheckCardNumberAvilability(value.CardNumber).pipe(
          takeUntil(this.destroy$))
          .subscribe(res => {
          if (res.Data) {
            this.userDetailsForm.controls['CardNumber'].setErrors({
              'cardNoNotAvailable': false,
            });
          }
          this.submitUserForm(value);
        });
      }else {
        this.submitUserForm(value);
      }
    } else {
      this.submitUserForm(value);
    }
  }

  submitUserForm(value) {
    this.formSubmited = true;
    if (this.selectedBranchCount > 0 ) {
      this.userDetailsForm.controls['Branch'].setValue(1)
    }
    UsErrorService.validateAllFormFields(this.userDetailsForm, this.formErrors, this.validationMessages);
    if (this.userDetailsForm.valid) {
      this.saveUsp(value)
    }
  }

  saveUsp(value) {
    // tslint:disable-next-line:prefer-const
    let selectedBranchesToSave = []
    this.branchesList.forEach(element => {
      if (element.IsCheck) {
        selectedBranchesToSave.push({
          'BranchId': element.Id,
          'BranchName': element.Id + '_' + element.BranchName,
          'IsDeleted': false,
          'UserId': this.exceLoginService.CurrentUser.Id
        });
      }
    });

    if (this.isAddNew) {
      value.EmployeeID = this.employeeId;
      value.UserName = this.gymCode + '/' + value.UserName;
      value.UniqueFeatureList = this.uniqFeatList
      value.UniqueOperationIdList = this.uniqOperList
      value.UniqueModuleList = this.uniqMOdList
      value.SubOperationList = this.subOpList

      this.ussAdminService.AddEditUSPUsers({ uspUser: value, branchesList: selectedBranchesToSave }).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result.Data) {
          this.saveSuccess.emit();
        }
      });
    } else {
      if (value) {
        // tslint:disable-next-line:forin
        for (const key in value) {
          this.selectedItem[key] = value[key]
        }
      }
      this.selectedItem.EmployeeID = this.employeeId;
      this.selectedItem.UniqueFeatureList = this.uniqFeatList
      this.selectedItem.UniqueOperationIdList = this.uniqOperList
      this.selectedItem.UniqueModuleList = this.uniqMOdList
      this.selectedItem.SubOperationList = this.subOpList
      this.ussAdminService.AddEditUSPUsers({ uspUser: this.selectedItem, branchesList: selectedBranchesToSave }).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result) {
          this.saveSuccess.emit();
        }
      });
    }
  }

  setEmployee(event) {
    this.userDetailsForm.controls['DisplayName'].setValue(event.Name);
    this.employeeId = event.Id;
    this.subModellReference.close()
  }

  openPasswordReset(content) {
    this.isSubmitChangePw = false
    this.pwModelRef = this.modalService.open(content, { width: '500' });
  }

  searchDeatail(val: any) {
    this.empSearch(val.searchVal)
  }

  pickAll() {
  this.allBranchesSelected = ( this.allBranchesSelected ? false : true) ;
  if (this.allBranchesSelected) {
this.branchesList.forEach(element => {element.IsCheck = true});
  }
  if (this.allBranchesSelected) {
    this.selectedBranchCount = this.branchesList.length}
  if (!this.allBranchesSelected) {
    this.selectedBranchCount = 0;
    this.branchesList.forEach(element => {element.IsCheck = false});
    }
  }

  closePasswordReset() {
    this.pwModelRef.close();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}


