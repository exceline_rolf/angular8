import { Inject, Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExceError } from '../../../shared/directives/exce-error/exce-error';

import { IAppConfig } from '../../../app-config/app-config.interface';
import { APP_CONFIG } from '../../../app-config/app-config.constants';
import { BankAccountValidator } from '../../../shared/directives/exce-error/util/validators/bank-account-validator';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { ColumnDataType } from 'app/shared/enums/us-enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-form-validation',
  templateUrl: './form-validation.component.html',
  styleUrls: ['./form-validation.component.scss']
})
export class FormValidationComponent implements OnInit, OnDestroy {

  @ViewChild('autocomplete', { static: false }) public autocomplete;
auotocompleteItems = [{ id: 'NAME', name: 'NAME :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }]

columns = [{ property: 'name', header: 'NAME', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
   { property: 'date', header: 'Date', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DATE },
    { property: 'phoneNumber', header: 'phone Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'jobTitle', header: 'job Title', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
   ]


  @ViewChild('cdire', { static: false }) public autocomplete2;
  items = [];
   private newMemberRegisterView: any;
  private addMemberView: any;

  selectedItem: any = '';
  inputChanged: any = '';
  wikiItems: any[] = [];

  items2: any[] = [{ id: 0, payload: { label: 'Tom' } },
  { id: 1, payload: { label: 'John' } },
  { id: 2, payload: { label: 'Lisa' } },
  { id: 3, payload: { label: 'Js' } },
  { id: 4, payload: { label: 'Java' } },
  { id: 5, payload: { label: 'c' } },
  { id: 6, payload: { label: 'vc' } }
  ];

  private persons: any[] =
[
    { 'name': "Aaron 2Moore", "email": "Heath44@hotmail.com", "jobTitle": "Regional Configuration Producer", "active": true, "phoneNumber": "611-898-6201", "date": "2015-11-06T07:21:25.510Z" },
    { "name": "Yvonne Conroy Mrs.", 'email': 'Gideon9@yahoo.com', 'jobTitle': 'Global Mobility Orchestrator', 'active': false, 'phoneNumber': '115-850-0969', 'date': '2014-12-20T00:48:40.276Z' },
    { 'name': 'Laron Padberg', 'email': 'Laney_Huels@hotmail.com', 'jobTitle': 'Senior Directives Supervisor', 'active': false, 'phoneNumber': '632-654-3034', 'date': '2015-09-29T04:33:38.544Z' },
    { 'name': 'Dr. Maryam Spinka', 'email': 'Aletha.Labadie@hotmail.com', 'jobTitle': 'Dynamic Mobility Associate', 'active': true, 'phoneNumber': '547-345-0067', 'date': '2015-09-23T01:13:39.320Z' },
    { 'name': 'Kiley Baumbach', 'email': 'Rogelio24@hotmail.com', 'jobTitle': 'Principal Metrics Orchestrator', 'active': true, 'phoneNumber': '958-524-5164', 'date': '2014-12-05T23:39:27.340Z' },
    { 'name': 'Hollis MacGyver', 'email': 'Yazmin.Heidenreich97@gmail.com', 'jobTitle': 'Direct Markets Assistant', 'active': true, 'phoneNumber': '603-607-3241', 'date': '2015-02-12T10:40:52.977Z' },
    { 'name': 'Axel McLaughlin', 'email': 'Deon_Heaney@gmail.com', 'jobTitle': 'Forward Mobility Architect', 'active': false, 'phoneNumber': '983-639-0705', 'date': '2015-03-01T02:28:26.030Z' },
    { 'name': 'Ricardo Botsford', 'email': 'Melisa73@yahoo.com', 'jobTitle': 'Direct Quality Consultant', 'active': true, 'phoneNumber': '408-082-9480', 'date': '2015-01-31T03:41:54.611Z' },
    { 'name': 'Corbin Funk Mrs.', 'email': 'Marjory.Morissette51@gmail.com', 'jobTitle': 'Human Configuration Manager', 'active': true, 'phoneNumber': '386-937-8683', 'date': '2014-12-05T15:07:36.843Z' },
    { 'name': 'Rosalind Paucek', 'email': 'Ivy_Stanton@gmail.com', 'jobTitle': 'Future Creative Supervisor', 'active': true, 'phoneNumber': '977-661-7403', 'date': '2015-06-10T17:42:38.644Z' },
    { 'name': 'Henderson Moore', 'email': 'Randi_Corkery@hotmail.com', 'jobTitle': 'Internal Accountability Director', 'active': true, 'phoneNumber': '078-101-6377', 'date': '2015-09-26T05:14:34.913Z' },
    { 'name': 'Kelli Schoen', 'email': 'Reva.Kiehn54@yahoo.com', 'jobTitle': 'National Accountability Architect', 'active': false, 'phoneNumber': '654-591-6561', 'date': '2015-05-04T06:50:37.482Z' },
    { 'name': 'Kenna Fritsch', 'email': 'Wilburn2@gmail.com', 'jobTitle': 'Legacy Response Administrator', 'active': true, 'phoneNumber': '790-480-2859', 'date': '2015-10-10T23:37:05.867Z' },
    { 'name': 'Judge Marquardt', 'email': 'Letha_Champlin69@hotmail.com', 'jobTitle': 'Human Program Specialist', 'active': true, 'phoneNumber': '100-494-1787', 'date': '2015-04-04T23:29:48.588Z' },
    { 'name': 'Kurtis Hane', 'email': 'Mona.Gaylord47@yahoo.com', 'jobTitle': 'International Optimization Director', 'active': false, 'phoneNumber': '008-800-2959', 'date': '2014-12-04T21:09:50.722Z' },
    { 'name': 'Nicolette Lind', 'email': 'Thurman30@yahoo.com', 'jobTitle': 'Legacy Marketing Facilitator', 'active': true, 'phoneNumber': '007-908-2460', 'date': '2015-06-22T08:11:57.381Z' },
    { 'name': 'Idella Green', 'email': 'Fernando_Ward@yahoo.com', 'jobTitle': 'Dynamic Division Orchestrator', 'active': false, 'phoneNumber': '147-865-1578', 'date': '2015-02-12T23:00:31.283Z' },
    { 'name': 'Mackenzie Bartell', 'email': 'Price25@yahoo.com', 'jobTitle': 'National Directives Associate', 'active': false, 'phoneNumber': '235-649-0980', 'date': '2015-06-24T20:21:51.356Z' },
    { 'name': 'Mose Kohler', 'email': 'Malika56@hotmail.com', 'jobTitle': 'Lead Implementation Executive', 'active': true, 'phoneNumber': '614-886-4868', 'date': '2015-03-04T13:05:23.698Z' },
    { 'name': 'Cielo Kuphal', 'email': 'Jude_Terry24@gmail.com', 'jobTitle': 'Dynamic Division Analyst', 'active': false, 'phoneNumber': '590-976-7492', 'date': '2015-06-02T20:52:32.664Z' },
    { 'name': 'Haleigh Stokes', 'email': 'Belle_Herman64@yahoo.com', 'jobTitle': 'Global Intranet Executive', 'active': false, 'phoneNumber': '418-255-9365', 'date': '2015-04-10T00:32:10.283Z' },
    { 'name': 'Tyrese Walter', 'email': 'Garland.Veum52@hotmail.com', 'jobTitle': 'Senior Web Liason', 'active': false, 'phoneNumber': '041-555-9831', 'date': '2015-08-18T20:05:08.839Z' },
    { 'name': 'Barney Shields', 'email': 'Anika27@gmail.com', 'jobTitle': 'District Web Administrator', 'active': true, 'phoneNumber': '379-438-0217', 'date': '2015-06-01T09:28:46.778Z' },
    { 'name': 'Favian Abbott Miss', 'email': 'Palma_Little@hotmail.com', 'jobTitle': 'Lead Implementation Facilitator', 'active': false, 'phoneNumber': '642-808-5400', 'date': '2015-08-09T07:38:06.588Z' },
    { 'name': 'Carissa Kunze', 'email': 'Merl_Frami@yahoo.com', 'jobTitle': 'Regional Division Technician', 'active': true, 'phoneNumber': '949-983-0342', 'date': '2015-11-05T08:09:09.463Z' }
]


  items3: any[] = [];
  //  [{ id: "NAME", name: "Name :",isNumber: false }, { id: "ADDRESS", name: "Address :",isNumber: false },
  //    { id: "MOBILE", name: "Mobile :" ,isNumber: true}, { id: "EMAIL", name: "Email :" ,isNumber: true}];

  config2: any = { 'placeholder': 'test', 'sourceField': ['payload', 'label'] };

  config3: any = { 'placeholder': '', 'sourceField': ['name'] };

  onSelect(item: any) {
    alert(item.name);
    this.selectedItem = item;
  }

  onDefultSelect(name: string) {
    alert(name);
  }

  onInputChangedEvent(val: string) {
    this.inputChanged = val;
  }

  search(term: string) {
    // this.service.search(term).subscribe(e => this.wikiItems = e, error => console.log(error));
  }

  protected searchStr: string;
  protected selectedColor: string;
  protected searchData = [
    { color: 'red', value: '#f00', isNumber: true },
    { color: 'green', value: '#0f0', isNumber: true },
    { color: 'blue', value: '#00f', isNumber: true },
    { color: 'cyan', value: '#0ff', isNumber: true },
    { color: 'magenta', value: '#f0f', isNumber: true },
    { color: 'yellow', value: '#ff0', isNumber: true },
    { color: 'black', value: '#000', isNumber: true }
  ];


  fruitName: string;
  fruits: any[] = [
    {
      id: 1,
      name: "Apple",
      searchText: "apple"
    },
    {
      id: 2,
      name: "Orange",
      searchText: "orange"
    },
    {
      id: 3,
      name: "Banana",
      searchText: "banana"
    }
  ];

  selectedFruit: any = this.fruits[0];

  public fruitSelected(fruit) {
    this.fruitName = fruit ? fruit.name : 'none';
  }


  greeting = {};
  name = 'World';
  @ViewChild('p', { static: false }) public popover: ExceError;

  public complexForm: FormGroup;

  constructor(
    private modalService: UsbModal,
    private fb: FormBuilder,
    @Inject(APP_CONFIG) private config: IAppConfig
  ) {

    // translate.addLangs(['en','no']);
    // translate.setDefaultLang('en');

    // let browserLang = translate.getBrowserLang();
    // translate.use(browserLang.match(/en|no/) ? browserLang : 'en');

    this.complexForm = fb.group({
      'firstName': [null, Validators.required],
      'lastName': [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10)])],
      // 'bankAccount': ['', Validators.compose([Validators.required, BankAccountValidator.BankAccount('test')])],
      // 'email': [null, Validators.compose([Validators.required, Validators.pattern(this.config.EMAIL_REGEXP)])],
      'email': [null, [Validators.pattern(this.config.EMAIL_REGEXP)]],
      'postalCode': [null, [Validators.pattern('^[1-9][0-9]{4}$')]],
      'gender': [null, Validators.required],
      'hiking': false,
      'running': false,
      'swimming': false,
      'timepicker': [null]
    })

    // this.complexForm = fb.group({
    //   'firstName': [null],
    //   'lastName': [null],
    //   'bankAccount': [null],
    //   'email': [null],
    //   'postalCode': [null],
    //   'gender': [null],
    //   'hiking': false,
    //   'running': false,
    //   'swimming': false,
    //   'timepicker': [null, Validators.required]


    // })


  }

searchDeatail(val: any) {
    this.persons =
    [
        { 'name': "Aaron 2Moore", "email": "Heath44@hotmail.com", "jobTitle": "Regional Configuration Producer", "active": true, "phoneNumber": "611-898-6201", "date": "2015-11-06T07:21:25.510Z" },
        { "name": "Yvonne Conroy Mrs.", 'email': 'Gideon9@yahoo.com', 'jobTitle': 'Global Mobility Orchestrator', 'active': false, 'phoneNumber': '115-850-0969', 'date': '2014-12-20T00:48:40.276Z' }
    ]

     this.items = this.persons;

}

 newMemberModal(content: any) {
    this.newMemberRegisterView = this.modalService.open(content, {});
  }

  addMemberModal(content: any) {
  this.addMemberView = this.modalService.open(content, {width: '800' });
  this.newMemberRegisterView.close();
  }

  selectedRowItem(item) {
    this.newMemberRegisterView.close();
  }


  submitForm(value: any) {
  }

  ngOnInit() {
    this.items = this.persons;
    //  this.items3 =[{ id: "NAME", name: "NAME :",isNumber: false }, { id: "ADDRESS", name: "ADDRESS :",isNumber: false },
    //  { id: "MOBILE", name: "MOBILE :" ,isNumber: false},{ id: "MOBILE", name: "MOBILE :" ,isNumber: false},  { id: "EMAIL", name: "EMAIL :" ,isNumber: false}]
    //    this.autocomplete2.items = this.items3;
  }

  ngOnDestroy() {
    if (this.newMemberRegisterView) {
      this.newMemberRegisterView.close();
    }
    if (this.addMemberView) {
      this.addMemberView.close();
    }
  }

  log(value) {
  }
  ccc() {
  }

}

 // TODO
 // time picker, date picker, drop down, email , bank account number, mobile number , data grid,
