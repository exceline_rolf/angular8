import { Injectable, EventEmitter } from '@angular/core';
import { ConfigService } from '@ngx-config/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Observable } from 'rxjs';

@Injectable()
export class ExceToolbarService {
  langUpdated: EventEmitter<any> = new EventEmitter();
  branchSelected: EventEmitter<any> = new EventEmitter();
  private userBranches: any;
  private lang: any;
  private branch: any;
  private commonApiUrl: string;

  constructor(
    private config: ConfigService,
  ) {
  }

  setSelectedLanguage(lang) {
    this.lang = lang;
    this.langUpdated.emit(this.lang);
  }

  setSelectedBranch(branch) {
    this.branch = branch;
    this.branchSelected.emit(this.branch);
  }

  getSelectedLanguage(): any {
    return this.lang;
  }

  getUserBranches(): any {
    if (this.config.getSettings('ENV') === 'DEV') {
      const branches = Cookie.get('userBranches');
      if (branches === 'undefined') {
        Cookie.deleteAll();
        location.reload();
      }
      return JSON.parse(branches);
    } else {
      return this.userBranches;
    }

  }

  setUserBranches(userBranches): void {
    this.userBranches = userBranches;
    Cookie.set('userBranches', JSON.stringify(userBranches), this.config.getSettings('COOKIE_EXPIRATION.DAYS'));
  }

}
