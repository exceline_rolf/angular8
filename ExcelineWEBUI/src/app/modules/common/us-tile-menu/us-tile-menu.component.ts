import { Component, OnInit, OnChanges, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { UsTileMenuService } from './us-tile-menu.service';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';

@Component({
  selector: 'app-us-tile-menu',
  templateUrl: './us-tile-menu.component.html',
  styleUrls: ['./us-tile-menu.component.scss'],
})
export class UsTileMenuComponent implements OnChanges {

  modules: any[];
  private errorMsg: string;

  constructor(
    private usTileMenuService: UsTileMenuService,
    private exceLoginService: ExceLoginService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.usTileMenuService.userModulesLoaded.subscribe(modules => {
      this.modules = modules;
    })
    if (!this.modules) {
      this.modules = this.usTileMenuService.getUserModules();
    }
  }

  ngOnChanges(changes: any): void {
  }

  moduleselected(module) {
    this.usTileMenuService.SelectedModule = module;
    let url = '';
    if (module.Features.length > 0) {
      if (module.ID === 'ExceAdmin') {
        url = '/adminstrator/home';
      } else if (module.ID === 'ExceEconomy') {
        url = '/economy/home';
      } else if (module.ID === 'ReportsModule') {
        url = '/reporting/home';
      }else if (module.ID === 'Gym') {
        url = '/gym/home';
      } else {
        url = '/';
      }
      this.router.navigate([url])

    } else {
      this.router.navigate([module.ModuleHomeURL])
    }

  }

  go(url) {
    this.router.navigate([url]);
  }
}
