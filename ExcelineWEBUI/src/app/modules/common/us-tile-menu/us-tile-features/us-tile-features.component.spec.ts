import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsTileFeaturesComponent } from './us-tile-features.component';

describe('UsTileFeaturesComponent', () => {
  let component: UsTileFeaturesComponent;
  let fixture: ComponentFixture<UsTileFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsTileFeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsTileFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
