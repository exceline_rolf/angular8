import { TestBed, inject } from '@angular/core/testing';

import { UsTileMenuService } from './us-tile-menu.service';

describe('UsTileMenuService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsTileMenuService]
    });
  });

  it('should be created', inject([UsTileMenuService], (service: UsTileMenuService) => {
    expect(service).toBeTruthy();
  }));
});
