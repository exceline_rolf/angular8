import { Inject, Injectable, OnInit, EventEmitter } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ConfigService } from '@ngx-config/core';
import { CommonHttpService } from '../../../shared/services/common-http.service';

@Injectable()
export class UsTileMenuService {

  private commonApiUrl: string;
  private modules: any[];
  userModulesLoaded = new Subject<any>();

  private selectedModule;
  constructor(
    private config: ConfigService,
    private commonHttpService: CommonHttpService
  ) {
    this.commonApiUrl = this.config.getSettings('EXCE_API.COMMON');
  }

  setUserModules(modules): void {
    // this.userModulesLoaded.next(modules);
    this.modules = modules;
  }

  getUserModules(): any[] {
    return this.modules;
  }

  set SelectedModule(selectedModule) {
    this.selectedModule = selectedModule;
  }

  get SelectedModule() {
    return this.selectedModule;
  }

  getModuleById(moduleId) {
    if (this.modules) {
      return this.modules.filter(x => x.ModuleId === Number(moduleId))[0];
    } else {
      return null;
    }

  }

  getUserInfo(userName: string): Observable<any> {
    const url = this.commonApiUrl + 'api/User/GetUserInfo?userName=' + userName;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  editUserInfo(body: any): Observable<any> {
    const url = this.commonApiUrl + 'api/User/EditUserInfo';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    });
  }

}
