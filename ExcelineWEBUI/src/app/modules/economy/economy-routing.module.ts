import { NgModule, Inject } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IAppConfig } from '../../app-config/app-config.interface';
import { APP_CONFIG } from '../../app-config/app-config.constants';
import { ExceToolbarService } from '../common/exce-toolbar/exce-toolbar.service';
import { TranslateService } from '@ngx-translate/core';
import { MapPaymentsComponent } from './map-payments/map-payments.component';
import { ClassSalaryComponent } from './class-salary/class-salary.component';
import { InvoicePrintHomeComponent } from './economy-invoice-print/invoice-print-home/invoice-print-home.component';
import { BulkPdfPrintHomeComponent } from './economy-invoice-print/bulk-pdf-print-home/bulk-pdf-print-home.component';
import { ImportExportComponent } from './import-export/import-export.component';
import { EconomyLogSummaryComponent } from './economy-log-summary/economy-log-summary.component';
import { PriceUpdateComponent } from './price-update/price-update.component';
import { MemberFeeGenerationComponent } from './member-fee-generation/member-fee-generation.component';
import { EconomyHomeComponent } from './economy-home/economy-home.component';
import { EconomyExportComponent } from './economy-export/economy-export.component';
import { LedgerComponent } from './kasselov/Ledger/Ledger.component';
import { KeyManagementComponent } from './kasselov/KeyManagement/KeyManagement.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: EconomyHomeComponent },
  { path: 'economy-log-summary', component: EconomyLogSummaryComponent },
  { path: 'map-payments', component: MapPaymentsComponent },
  { path: 'class-salary', component: ClassSalaryComponent },
  { path: 'invoice-print-home', component: InvoicePrintHomeComponent },
  { path: 'bulk-pdf-print-home', component: BulkPdfPrintHomeComponent },
  { path: 'import-export', component: ImportExportComponent },
  { path: 'price-update', component: PriceUpdateComponent },
  { path: 'member-fee-generation', component: MemberFeeGenerationComponent },
  { path: 'economy-export', component: EconomyExportComponent},
  { path: 'Ledger', component: LedgerComponent},
  { path: 'KeyManagement', component: KeyManagementComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EconomyRoutingModule {
  constructor(
    private translate: TranslateService,
    private exceToolbarService: ExceToolbarService,
    @Inject(APP_CONFIG) private config: IAppConfig
  ) {
    const langs = [];
    this.config.LANGUAGES.map(ln => {
      langs.push(ln.ID);
    })
    this.translate.addLangs(langs);
    translate.setDefaultLang(this.config.LANGUAGE_DEFAULT.ID);

    const lang = this.exceToolbarService.getSelectedLanguage();
    this.translate.use(lang.id);
    this.exceToolbarService.langUpdated.subscribe(
      (res) => {
        this.translate.use(res.id);
      }
    );
  }
}

export const EconomyRoutingComponents = [
  MapPaymentsComponent,
  ClassSalaryComponent,
  InvoicePrintHomeComponent,
  BulkPdfPrintHomeComponent,
  ImportExportComponent,
  EconomyLogSummaryComponent,
  PriceUpdateComponent,
  MemberFeeGenerationComponent,
  EconomyExportComponent,
  LedgerComponent,
  KeyManagementComponent
]
