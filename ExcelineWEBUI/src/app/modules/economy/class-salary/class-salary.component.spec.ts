import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassSalaryComponent } from './class-salary.component';

describe('ClassSalaryComponent', () => {
  let component: ClassSalaryComponent;
  let fixture: ComponentFixture<ClassSalaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassSalaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassSalaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
