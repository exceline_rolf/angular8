import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { ExceMessageService } from './../../shared/components/exce-message/exce-message.service';
import { ExceEconomyService } from './services/exce-economy.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { EconomyRoutingModule } from './economy-routing.module';
import { EconomyRoutingComponents } from './economy-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader } from '@ngx-translate/core';
import { MapPaymentsComponent } from './map-payments/map-payments.component';
import { ClassSalaryComponent } from './class-salary/class-salary.component';
import { InvoicePrintHomeComponent } from './economy-invoice-print/invoice-print-home/invoice-print-home.component';
import { BulkPdfPrintHomeComponent } from './economy-invoice-print/bulk-pdf-print-home/bulk-pdf-print-home.component';
import { ImportExportComponent } from './import-export/import-export.component';
import { EconomyLogSummaryComponent } from './economy-log-summary/economy-log-summary.component';
import { PriceUpdateComponent } from './price-update/price-update.component';
import { MemberFeeGenerationComponent } from './member-fee-generation/member-fee-generation.component';
import { createTranslateLoader, ExceCommonModule } from 'app/modules/common/exce-common.module';
import { EconomyHomeComponent } from './economy-home/economy-home.component';
import { EconomyExportComponent } from './economy-export/economy-export.component';
import {LedgerComponent} from './kasselov/Ledger/Ledger.component';
import {KeyManagementComponent} from './kasselov/KeyManagement/KeyManagement.component';

export function translateLoader(http: HttpClient) {

  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/', suffix: '.json' },
    { prefix: './assets/i18n/modules/economy/', suffix: '.json' }
  ]);
}

export class MultiTranslateHttpLoader implements TranslateLoader {

  constructor(private http: HttpClient,
    public resources: { prefix: string, suffix: string }[] = [
      {
        prefix: '/assets/i18n/',
        suffix: '.json'
      }
    ]) { }

  /**
   * Gets the translations from the server
   * @param lang
   * @returns {any}
   */
  public getTranslation(lang: string): any {

    return forkJoin(this.resources.map(config => {
      return this.http.get(`${config.prefix}${lang}${config.suffix}`);
    })).pipe(
      map(response => {
        return response.reduce((a, b) => {
          return Object.assign(a, b);
        });
      })
    )
  }
}

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ExceCommonModule,
    EconomyRoutingModule,
    ReactiveFormsModule,
     TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (translateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    ExceEconomyService
  ],
  declarations: [
    EconomyRoutingComponents,
    MapPaymentsComponent,
    ClassSalaryComponent,
    InvoicePrintHomeComponent,
    BulkPdfPrintHomeComponent,
    ImportExportComponent,
    EconomyLogSummaryComponent,
    PriceUpdateComponent,
    MemberFeeGenerationComponent,
    EconomyHomeComponent,
    EconomyExportComponent,
    LedgerComponent,
    KeyManagementComponent
  ]
})
export class EconomyModule { }
