import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EconomyExportComponent } from './economy-export.component';

describe('EconomyExportComponent', () => {
  let component: EconomyExportComponent;
  let fixture: ComponentFixture<EconomyExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EconomyExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EconomyExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
