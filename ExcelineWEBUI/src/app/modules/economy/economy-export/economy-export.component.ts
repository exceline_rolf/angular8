import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormsModule, ReactiveFormsModule, FormControl} from '@angular/forms';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { ExceEconomyService } from './../services/exce-economy.service';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PreloaderService } from '../../../shared/services/preloader.service';
import { ExceLoginService } from './../../login/exce-login/exce-login.service';
import { Extension } from 'app/shared/Utills/Extensions';
import * as moment from 'moment';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { UsbModalRef } from '../../../shared/components/us-modal/modal-ref';
import { saveAs } from 'file-saver/FileSaver';

@Component({
  selector: 'app-economy-export',
  templateUrl: './economy-export.component.html',
  styleUrls: ['./economy-export.component.scss'],
})
export class EconomyExportComponent implements OnInit, OnDestroy {
  branchId: number;
  accountingSystem: any;
  accountnumbers: any;
  accountnames: any;
  description: any;
  currentnumber: number;
  currentname: any;
  datalist: any;
  kasselist: any;
  templist: any;
  name = new FormControl('');
  number = new FormControl('');
  desc = new FormControl('');
  searchForm: FormGroup;
  exportForm: FormGroup;
  startDate: any;
  endDate: any;
  generateExportView: UsbModalRef;
  generateAccountView: UsbModalRef;
  generateConfirmationView: UsbModalRef;
  fileName = new FormControl('');
  fileType: any;
  showMessage: boolean;
  selectedAccount = new FormControl('');

  @Output() accountnameEvent: EventEmitter<any> = new EventEmitter();
  constructor(
    private economyService: ExceEconomyService,
    private exceBreadcrumbService: ExceBreadcrumbService,
    private exceLoginService: ExceLoginService,
    private fb: FormBuilder,
    private modalService: UsbModal
  ) {
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );

    // breadcrumbService.hideRoute('/economy/home');
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.EconomyExportC',
      url: '/economy/economy-export'
    });
  }



  accountNameFunc() {
    this.economyService.getaccountnames(this.branchId).subscribe(
      result => {
        if (result && result.Data) {
          this.datalist = result.Data;
          this.accountnames = this.datalist.map(a => a.Name)
          this.accountnumbers = this.datalist.map(b => b.Account)
          this.description = this.datalist.map(c => c.Description)
        }
        this.datalist.unsubscribe();
      });
  }

  kassefil() {
  const fromDate: any = this.manipulatedate(this.searchForm.value.startDate);
  const toDate: any = this.manipulatedate(this.searchForm.value.endDate);
  this.economyService.runaccountfile(fromDate, toDate , 1 , false ).subscribe(
    result => {
      if (result && result.Data) {
        this.kasselist = result.Data;
      }
    });
  }

  ngOnDestroy() {
    if (this.generateExportView) {
      this.generateExportView.close();
    }
    if (this.generateAccountView) {
      this.generateAccountView.close();
    }
    if (this.generateConfirmationView) {
      this.generateConfirmationView.close();
    }
  }

  addaccount(num) {
  this.economyService.addaccountnumber(this.desc.value, num).subscribe();
  this.accountNameFunc();
  }

  dateAdd(startDate: any, count: any, interval: string): Date {

    return moment(startDate).add(count, interval).toDate()
  }

  manipulatedate(dateInput: any) {

    if (dateInput.date !== undefined) {
      const tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  openGenerateExportModal(content: any) {
    this.showMessage = false;
    this.generateExportView = this.modalService.open(content, { width: '800' });
  }

  openGenerateAccountModal(content: any) {
    this.showMessage = false;
    this.generateAccountView = this.modalService.open(content, { width: '500' });
  }

  openGenerateConfirmModal(content: any, input) {
    this.desc.setValue(input);
    this.showMessage = false;
    this.generateConfirmationView = this.modalService.open(content, {width: '250'});
  }


exportToFileFunc() {
  switch (this.selectedAccount.value) {
    case 'Tripletex':
    this.TripletexExport();
    break;
    case 'Agresso':
    this.AgressoExport();
    break;
    case 'Visma':
    this.VismaExport();
    break
  }
}

VismaExport() {
  const somelist = this.VismaBuilder();
  const header = Object.keys(somelist[0]);
  const replacer = (key, value) => value === null ? '' : value;
  const csv = somelist.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(';'));
  const header1 = '@WaBnd(Descr, ValDt, SrcTp)';
  const header2 = '"Kassefil" ' + '"' + moment().format('YYYYMMDD') + '"' + ' "12"';
  const header3 = '@Wavo(' + header.join(',') + ')';
  csv.unshift(header3);
  csv.unshift(header2);
  csv.unshift(header1);
  const csvArrayOld = csv.join('\r\n');
  const csvArray = csvArrayOld.toString().replace(/;/g, ' ');
  const blob = new Blob([csvArray], { type: 'text/plain'});
  saveAs(blob, this.fileName.value);
}

VismaBuilder() {
  const keys = Object.keys(this.kasselist);
  const len = keys.length;
  this.templist = [];
  for ( let a = 0 ; a < len; a++) {
    const abc = {
      VoNo: moment().format('YYYYMMDDHHmm'),
      VoDt: this.kasselist.map(w => w.VoucherDate)[a],
      ValDt: this.kasselist.map(w => w.VoucherDate)[a],
      Votp: 5,
      DbAcNo: this.kasselist.map(x => x.AccountNo)[a],
      DbTxCd: '',
      DbAcC1: 3,
      CrAcNo: this.kasselist.map(x => x.AccountNo)[a],
      CrTxCd: this.kasselist.map(v => v.VATRate)[a],
      CrAcC1: 3,
      R7: '',
      R8: this.kasselist.map(j => j.BranchId)[a],
      VatAm: '',
      Am: this.kasselist.map(z => z.TotalSum)[a],
      InvoNo: '',
      DueDt: '',
      AgRef: '',
    }
    this.templist.push(abc);
  }
  return this.templist;
}

TripletexExport() {
  const somelist = this.TripletexBuilder();
  const header = Object.keys(somelist[0]);
  const replacer = (key, value) => value === null ? '' : value;
  const csv = somelist.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(';'));
 //csv.unshift(header.join(',')); // Exclude when headers are unwanted
  const csvArray = csv.join('\r\n');
  const blob = new Blob([csvArray], { type: 'text/plain'});
  saveAs(blob, this.fileName.value);
}

TripletexBuilder() {
  const keys = Object.keys(this.kasselist);
  const len = keys.length;
  this.templist = [];
  for ( let a = 0 ; a < len; a++) {
    const abc = {
      Identifikasjon: 'GBAT10',
      Bilagsnummer: moment().format('YYYYMMDDHHmm'),
      Bilagsdato: this.kasselist.map(w => w.VoucherDate)[a] ,
      Bilagsart: '1',
      Periode: this.kasselist.map(w => w.VoucherDate)[a].substring(4, 2),
      Regnskapsår: this.kasselist.map(w => w.VoucherDate)[a].substring(0, 4),
      Konto: this.kasselist.map(x => x.AccountNo)[a],
      Mva_kode: '',
      Saldo: this.kasselist.map(z => z.TotalSum)[a],
      Kundenummer: '',
      Leverandørnummer: '',
      Kontaktnavn: '',
      Adresse: '',
      Postnummer: '',
      Poststed: '',
      Fakturanummer: '',
      KID: '',
      Forfallsdato: '',
      Ikke_i_bruk: '',
      Bankkonto: '',
      Beskrivelse_Hovedbok: '',
      Beskrivelse_Reskontro: '',
      Rentefakturering: '',
      Prosjekt: '',
      Avdeling: this.kasselist.map(j => j.BranchId)[a],
      Betalingsbetingelse: '',
      Brutto: 'F',
      BruttoSum: '',
      IBAN: '',
      SWIFT_BIC: ''
    }
    this.templist.push(abc);
  }
  return this.templist;
}

AgressoExport() {
  const somelist = this.AgressoBuilder();
  const header = Object.keys(somelist[0]);
  const replacer = (key, value) => value === null ? '' : value;
  const csv = somelist.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
 // csv.unshift(header.join(',')); // Exclude when headers are unwanted
  const csvArrayOld = csv.join('\r\n');
  const csvArrayOld2 = csvArrayOld.toString().replace(/"/g, '');
  const csvArray = csvArrayOld2.toString().replace(/,/g, '');
  const blob = new Blob([csvArray], { type: 'text/plain'});
  saveAs(blob, this.fileName.value);
  }

AgressoBuilder() {
  const keys = Object.keys(this.kasselist);
  const len = keys.length;
  this.templist = [];
  for ( let a = 0 ; a < len; a++) {
  const abc = {
    batch_id: this.fillMe(moment().format('YYYYMMDDHHmm'), 25 ),
    stringerface1: this.fillMe('KT', 25),
    voucher_type: this.fillMe('AK', 25),
    trans_type: this.fillMe('GL', 25),
    client: this.fillMe('70', 25),
    account: this.fillMe(this.kasselist.map(x => x.AccountNo)[a], 25),
    dim_1: this.fillMe(this.kasselist.map(y => y.CompanyIdSalary)[a], 25),
    dim_2: this.fillMe('', 25),
    dim_3: this.fillMe('', 25),
    dim_4: this.fillMe('', 25),
    dim_5: this.fillMe('', 25),
    dim_6: this.fillMe('', 25),
    dim_7: this.fillMe('', 25),
    tax_code: this.fillMe(this.kasselist.map(v => v.VATRate)[a], 25),
    tax_system: this.fillMe('', 25),
    currency: this.fillMe('NOK', 25),
    dc_flag: this.fillMe('', 2),
    cur_amount: this.fillMe(this.kasselist.map(z => z.TotalSum)[a], 20),
    amount: this.fillMe(this.kasselist.map(z => z.TotalSum)[a], 20),
    number_1: this.fillMe('', 11),
    value_1: this.fillMe('', 20),
    value_2: this.fillMe('', 20),
    value_3: this.fillMe('', 20),
    description: this.fillMe('Kasseoppgjør', 255),
    trans_date: this.fillMe(this.kasselist.map(w => w.VoucherDate)[a], 8),
    voucher_date: this.fillMe(this.kasselist.map(w => w.VoucherDate)[a], 8),
    voucher_no: this.fillMe('', 15),
    period: this.fillMe(this.kasselist.map(w => w.VoucherDate)[a].substring(0, 6), 6),
    tax_id: this.fillMe('', 1),
    ext_inv_ref: this.fillMe('', 100),
    ext_ref: this.fillMe('', 255),
    due_date: this.fillMe('', 8),
    disc_date: this.fillMe('', 8),
    discount: this.fillMe('', 20),
    commitment: this.fillMe('', 25),
    order_id: this.fillMe('', 15),
    kid: this.fillMe('', 27),
    pay_transfer: this.fillMe('', 2),
    status: this.fillMe('', 1),
    apar_type: this.fillMe('R', 1),
    apar_id: this.fillMe('', 25),
    pay_flag: this.fillMe('', 1),
    voucher_ref: this.fillMe('', 15),
    sequence_ref: this.fillMe('', 9),
    stringrule_id: this.fillMe('', 25),
    factor_short: this.fillMe('', 25),
    responsible: this.fillMe('', 25),
    apar_name: this.fillMe('', 255),
    address: this.fillMe('', 160),
    province: this.fillMe('', 40),
    place: this.fillMe('', 40),
    bank_account: this.fillMe('', 35),
    pay_method: this.fillMe('', 2),
    vat_reg_no: this.fillMe('', 25),
    zip_code: this.fillMe('', 15),
    curr_licence: this.fillMe('', 3),
    account2: this.fillMe('', 25),
    base_amount: this.fillMe('', 20),
    base_curr: this.fillMe('', 20),
    pay_temp_id: this.fillMe('', 4),
    allocation_key: this.fillMe('', 3),
    period_no: this.fillMe('', 2),
    clearing_code: this.fillMe('', 13),
    swift: this.fillMe('', 11),
    arrive_id: this.fillMe('', 15),
    bank_acc_type: this.fillMe('', 2),
    pay_currency: this.fillMe('', 25),
    arrival_date: this.fillMe('', 8),
    orig_reference: this.fillMe('', 15),
    complastring: this.fillMe('', 25),
    compl_delay: this.fillMe('', 8),
  };
  this.templist.push(abc);
  }
  return this.templist;
}


fillMe(txt, num) {
const len = txt.length;
const char = ' ';
let result = txt;
for ( let i = 0 ; i < (num - len); i++) {
result = result + char;
}
return result;
}
  ngOnInit() {
    this.selectedAccount.setValue('Tripletex');
    this.accountingSystem = [
      {name: 'Tripletex', value: 1},
      {name: 'Agresso', value: 2} ,
      {name: 'Visma', value: 3}];
    this.accountNameFunc();
    this.searchForm = this.fb.group({
      'startDate': [null],
      'endDate': [null]
    });
    this.searchForm.patchValue({
      startDate: { date: Extension.GetFormatteredDate(this.dateAdd(new Date(), -1, 'days')) },
      endDate: { date: Extension.GetFormatteredDate(new Date()) }
    });
  }


}
