import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberFeeGenerationComponent } from './member-fee-generation.component';

describe('MemberFeeGenetationComponent', () => {
  let component: MemberFeeGenerationComponent;
  let fixture: ComponentFixture<MemberFeeGenerationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberFeeGenerationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberFeeGenerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
