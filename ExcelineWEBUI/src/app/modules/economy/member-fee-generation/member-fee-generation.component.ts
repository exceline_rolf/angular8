import { ExceEconomyService } from './../services/exce-economy.service';
import { Subscription } from 'rxjs';
import { OnDestroy } from '@angular/core';
import { error } from 'util';
import { ExceMessageService } from './../../../shared/components/exce-message/exce-message.service';
import { ExceToolbarService } from './../../common/exce-toolbar/exce-toolbar.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-member-fee-generation',
  templateUrl: './member-fee-generation.component.html',
  styleUrls: ['./member-fee-generation.component.scss']
})
export class MemberFeeGenerationComponent implements OnInit, OnDestroy {
  model: void;
  messageSubscription: Subscription;
  _yesHandlerSubcription: any;
  memberFeeDetails: { gyms: any[]; month: Number; };
  selectedGyms: any[];
   months: any[] = [
    { Id: 1, Name: 'ECONOMY.January' },
    { Id: 2, Name: 'ECONOMY.February' },
    { Id: 3, Name: 'ECONOMY.March' },
    { Id: 4, Name: 'ECONOMY.April' },
    { Id: 5, Name: 'ECONOMY.May' },
    { Id: 6, Name: 'ECONOMY.June' },
    { Id: 7, Name: 'ECONOMY.July' },
    { Id: 8, Name: 'ECONOMY.August' },
    { Id: 9, Name: 'ECONOMY.September' },
    { Id: 10, Name: 'ECONOMY.October' },
    { Id: 11, Name: 'ECONOMY.November' },
    { Id: 12, Name: 'ECONOMY.December' }
  ];

  branches: any[] = [];
  selectedMonth: Number;

  constructor(
    private toolBarService: ExceToolbarService,
    private messageService: ExceMessageService,
    private economyService: ExceEconomyService,
    private translateService: TranslateService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.AnnualSubscriptionC',
      url: '/economy/member-fee-generation'
    });
  }

  ngOnInit() {
    this.economyService.getBranches().subscribe(
      result => {
        if (result) {
          this.branches = result.Data;
        }
      }
    );

    this._yesHandlerSubcription = this.messageService.yesCalledHandle.subscribe((value) => {
      switch (value.id) {
        case 'MEMFEECONFIRM':
          this.generatememberFee();
          break;
      }
    });
  }

  changeMemberFeeMonth(selectedMonth: any) {
    if (selectedMonth) {
      this.selectedMonth = selectedMonth;
    }
  }

  rowsSelected(event: any) {
    if (event.selected) {
      event.item.IsSelected = true;
    } else {
      event.item.IsSelected = false;
    }
  }

  generateMemberFeeEvent() {

    this.selectedGyms = this.branches.filter(X => X.IsSelected).map(B => B.BranchId);
    if (this.selectedGyms.length > 0) {

      this.memberFeeDetails = {
        gyms: this.selectedGyms,
        month: this.selectedMonth ? this.selectedMonth : -1
      }
      let message1: string
      let message2: string
      let message3: string
      if (this.selectedMonth > 0) {
        this.economyService.memberFeeConfirmation(this.memberFeeDetails).subscribe(
          result => {
            if (result) {

              this.translateService.get('ECONOMY.MemberFeeConMgs1').subscribe(value => message1 = value);
              this.translateService.get('ECONOMY.MemberFeeConMgs2').subscribe(value => message2 = value);
              this.translateService.get('ECONOMY.MemberFeeConMgs3').subscribe(value => message3 = value);
              let message = message1 + " " + result.Data[1] + " " + message2 + " " + result.Data[0] + " " + message3;

              this.messageService.openMessageBox('CONFIRM',
                {
                  messageTitle: 'Exceline',
                  messageBody: message,
                  msgBoxId: 'MEMFEECONFIRM'
                });
            }
          },
          error => {
            this.messageService.openMessageBox('WARNING', {
              messageTitle: 'Exceline',
              messageBody: 'ECONOMY.Error'
            });
          }
        );
      } else {
        this.economyService.validateMemberFee(this.memberFeeDetails).subscribe(
          result => {
            if (result) {
              if (result.Data) {
                this.model = this.messageService.openMessageBox('WARNING',
                  {
                    messageTitle: 'Exceline',
                    messageBody: 'ECONOMY.plsSelectMonth'
                  }
                );
              } else {
                this.economyService.memberFeeConfirmation(this.memberFeeDetails).subscribe(
                  result => {
                    if (result) {

                      this.translateService.get('ECONOMY.MemberFeeConMgs1').subscribe(value => message1 = value);
                      this.translateService.get('ECONOMY.MemberFeeConMgs2').subscribe(value => message2 = value);
                      this.translateService.get('ECONOMY.MemberFeeConMgs3').subscribe(value => message3 = value);
                      let message = message1 + " " + result.Data[1] + " " + message2 + " " + result.Data[0] + " " + message3;

                      this.messageService.openMessageBox('CONFIRM',
                        {
                          messageTitle: 'Exceline',
                          messageBody: message,
                          msgBoxId: 'MEMFEECONFIRM'
                        });
                    }
                  },
                  error => {

                    this.messageService.openMessageBox('WARNING', {
                      messageTitle: 'Exceline',
                      messageBody: 'ECONOMY.Error'
                    });
                  }
                );
              }
            }
          },
          error => {
            this.messageService.openMessageBox('WARNING', {
              messageTitle: 'Exceline',
              messageBody: 'ECONOMY.Error'
            });
          });
      }
    } else {
      let message: string;
      this.messageService.openMessageBox('WARNING', {
        messageTitle: 'Exceline',
        messageBody: 'ECONOMY.plsSelectGym'
      });
    }
  }

  generatememberFee() {
    this.messageSubscription = this.economyService.generateMemberfee(this.memberFeeDetails).subscribe(
      result => {
        if (result) {
          if (result.Data) {
            this.messageService.openMessageBox('WARNING', {
              messageTitle: 'Exceline',
              messageBody: 'ECONOMY.MsgMemberFeeCompleted',
            });
          } else {
            this.messageService.openMessageBox('WARNING', {
              messageTitle: 'Exceline',
              messageBody: 'ECONOMY.ErrorOccurred',
            });
          }
        }
      });
  }

  ngOnDestroy() {
    if (this._yesHandlerSubcription != undefined) {
      this._yesHandlerSubcription.unsubscribe();
    }

    if (this.messageSubscription) {
      this.messageSubscription.unsubscribe();
    }
  }
}
