import { TestBed, inject } from '@angular/core/testing';

import { ExceEconomyService } from './exce-economy.service';

describe('ExceEconomyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceEconomyService]
    });
  });

  it('should be created', inject([ExceEconomyService], (service: ExceEconomyService) => {
    expect(service).toBeTruthy();
  }));
});
