import { UsbModalRef } from 'app/shared/components/us-modal/modal-ref';
import { ExcePdfViewerService } from 'app/shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { TranslateService } from '@ngx-translate/core';
import { DataTableFilterPipe } from './../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { Subscription } from 'rxjs';
import { ExceMessageService } from './../../../shared/components/exce-message/exce-message.service';
import { ExceLoginService } from './../../login/exce-login/exce-login.service';
import { ExceEconomyService } from './../services/exce-economy.service';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Extension } from './../../../shared/Utills/Extensions';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { ExceToolbarService } from '../../common/exce-toolbar/exce-toolbar.service';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { error } from 'util';

@Component({
  selector: 'app-map-payments',
  templateUrl: './map-payments.component.html',
  styleUrls: ['./map-payments.component.scss']
})
export class MapPaymentsComponent implements OnInit, OnDestroy, AfterViewInit {
  searchVal: any;
  searchText: any;
  sourceInvoices: any;
  selectInvoiceRef: UsbModalRef;
  fieldType: string;
  mappedErrorPayments: any[] = [];
  matchedArItems: any[] = [];
  selectInvoiceView: any;
  selectedPayment: any;
  aritemList: any[] = [];
  invoiceList: any[] = [];
  ApportionListForNotAppPayment: any[] = [];
  movingPayment: any;
  deletingPayment: any;
  _yesHandlerSubcription: Subscription;
  locale: string;
  _user: any;
  _branchId: any;
  searchForm: FormGroup;
  startDate: any
  endDate: any
  selectedType: any = 'UnknownExceeded';
  ErrorPayments: any[] = [];
  SourcePaymentList: any[] = [];
  IsEnableCommitButton: boolean;
  lock: boolean;
  auotocompleteItems = [
    { id: 'DEBNAME', name: 'Debtor Name :', isNumber: false }, { id: 'ADDRESS', name: 'Address :', isNumber: false },
    { id: 'KID', name: 'KID :', isNumber: false }, { id: 'CUSTID', name: 'CustomerId :', isNumber: false },
    { id: 'MOBILE', name: 'Telephone :', isNumber: false }, { id: 'INVNO', name: 'InvoiceNumber :', isNumber: false }
  ]

  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };
  @ViewChild('autobox', { static: false }) public autocomplete;
  private _filterPipe: DataTableFilterPipe = new DataTableFilterPipe();
  constructor(
    private fb: FormBuilder,
    private economyService: ExceEconomyService,
    private loginservice: ExceLoginService,
    private toolBarService: ExceToolbarService,
    private translateService: TranslateService,
    private modalService: UsbModal,
    private excePdfViewerService: ExcePdfViewerService,
    private messageService: ExceMessageService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.ToleranceHandlingC',
      url: '/economy/map-payments'
    });

    const language = this.toolBarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolBarService.langUpdated.subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
  }

  ngOnInit() {
    this.lock = false;
    this._branchId = this.loginservice.SelectedBranch.BranchId;
    this._user = this.loginservice.CurrentUser.username;
    this.searchForm = this.fb.group({
      'startDate': [null],
      'endDate': [null]
    });

    this.searchForm.patchValue({
      startDate: { date: Extension.GetFormatteredDate(this.dateAdd(new Date(), -1, 'days')) },
      endDate: { date: Extension.GetFormatteredDate(new Date()) }
    });

    // this.autocomplete.items = this.auotocompleteItems;

    // this.autocomplete.setSearchValue(this.auotocompleteItems);

    // this._yesHandlerSubcription =
    this.messageService.yesCalled.subscribe( value  => {
      switch (value.id) {
        case 'DELETE':
          this.removeErrorPayment();
          break;
        case 'SHOP':
          this.movePayment('SHOP');
          break;
        case 'PRE':
          this.movePayment('PRE');
          break;
        case 'RESET':
          this.resetMappings();
          break;
      }
    });
  }

  ngAfterViewInit() {
    if (this.lock === false) {
      this.getUnknownPayments();
      this.lock = true;
    }
  }

  getUnknownPayments() {
    this.aritemList = [];
    const fromDate: any = this.manipulatedate(this.searchForm.value.startDate);
    const toDate: any = this.manipulatedate(this.searchForm.value.endDate);
    this.economyService.getErrorPaymentsByType(fromDate, toDate, this.selectedType, this._branchId, '1').subscribe(
      result => {
        if (result) {
          this.ErrorPayments = result.Data;
          this.SourcePaymentList = result.Data;

        }
      }
    );
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date !== undefined) {
      const tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  deleteErrorPayment(payment: any) {
    this.deletingPayment = payment;
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'ECONOMY.MsgRemovepayment',
        msgBoxId: 'DELETE'
      });
  }

  removeErrorPayment() {
    if (this.deletingPayment) {
      const removingDetails = {
        paymentID: this.deletingPayment.ID,
        aritemNo: this.deletingPayment.ExceedPaymentAritemNo,
        user: this._user,
        type: this.deletingPayment.RecordType
      }

      this.economyService.removeErrorPayment(removingDetails).subscribe(
        result => {
          if (result) {
            if (result.Data) {
              this.getUnknownPayments();
            }
          }
        }
      );
    }
  }

  moveToShopEvent(payment: any, type: any) {
    this.movingPayment = payment;
    console.log(payment, type)
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: type === 'SHOP' ? 'ECONOMY.MsgConfirmShopMove' : 'ECONOMY.MsgConfirmShopPrepaid',
        msgBoxId: type === 'SHOP' ? 'SHOP' : 'PRE'
      });
  }

  movePayment(moveType: any) {
    const moveDetails = {
      paymentID: this.movingPayment.ID,
      aritemno: this.movingPayment.ExceedPaymentAritemNo,
      type: moveType
    }
    this.economyService.movePayment(moveDetails).subscribe(
      result => {
        if (result) {
          if (result.Data === 1) {
            this.getUnknownPayments();
          } else if (result.Data === -2) {
            this.messageService.openMessageBox('WARNING', {
              messageTitle: 'Exceline',
              messageBody: 'ECONOMY.MsgAccNotMatch'
            });
          } else if (result.Data === -1) {
            this.messageService.openMessageBox('WARNING', {
              messageTitle: 'Exceline',
              messageBody: 'ECONOMY.MsgErrorMove'
            });
          }
        }
      }
    )
  }

  filterPayments(custId: any, ref: any, Kid: any, amount: any, voucherDate: any, type: any) {
    if (type === 'CUSTID') {
      if (custId) {
        this.ErrorPayments = this._filterPipe.transform('CustId', 'TEXT', this.SourcePaymentList, custId);
      } else {
        this.ErrorPayments = this.SourcePaymentList;
      }
      if (ref) {
        this.ErrorPayments = this._filterPipe.transform('Ref', 'TEXT', this.ErrorPayments, ref);
      }

      if (Kid) {
        this.ErrorPayments = this._filterPipe.transform('KID', 'TEXT', this.ErrorPayments, Kid);
      }

      if (amount) {
        this.ErrorPayments = this._filterPipe.transform('Amount', 'TEXT', this.ErrorPayments, amount);
      }

      if (voucherDate) {
        this.ErrorPayments = this._filterPipe.transform('VoucherDate', 'TEXT', this.ErrorPayments, voucherDate);
      }
    } else if (type === 'REF') {
      if (ref) {
        this.ErrorPayments = this._filterPipe.transform('Ref', 'TEXT', this.SourcePaymentList, ref);
      } else {
        this.ErrorPayments = this.SourcePaymentList;
      }
      if (custId) {
        this.ErrorPayments = this._filterPipe.transform('CustId', 'TEXT', this.ErrorPayments, custId);
      }
      if (Kid) {
        this.ErrorPayments = this._filterPipe.transform('KID', 'TEXT', this.ErrorPayments, Kid);
      }

      if (amount) {
        this.ErrorPayments = this._filterPipe.transform('Amount', 'TEXT', this.ErrorPayments, amount);
      }

      if (voucherDate) {
        this.ErrorPayments = this._filterPipe.transform('VoucherDate', 'TEXT', this.ErrorPayments, voucherDate);
      }
    } else if (type === 'KID') {
      if (Kid) {
        this.ErrorPayments = this._filterPipe.transform('KID', 'TEXT', this.SourcePaymentList, Kid);
      } else {
        this.ErrorPayments = this.SourcePaymentList;
      }
      if (custId) {
        this.ErrorPayments = this._filterPipe.transform('CustId', 'TEXT', this.ErrorPayments, custId);
      }
      if (ref) {
        this.ErrorPayments = this._filterPipe.transform('Ref', 'TEXT', this.ErrorPayments, ref);
      }

      if (amount) {
        this.ErrorPayments = this._filterPipe.transform('Amount', 'TEXT', this.ErrorPayments, amount);
      }

      if (voucherDate) {
        this.ErrorPayments = this._filterPipe.transform('VoucherDate', 'TEXT', this.ErrorPayments, voucherDate);
      }
    } else if (type === 'AMT') {
      if (amount) {
        this.ErrorPayments = this._filterPipe.transform('Amount', 'TEXT', this.SourcePaymentList, amount);
      } else {
        this.ErrorPayments = this.SourcePaymentList;
      }
      if (Kid) {
        this.ErrorPayments = this._filterPipe.transform('KID', 'TEXT', this.ErrorPayments, Kid);
      }
      if (custId) {
        this.ErrorPayments = this._filterPipe.transform('CustId', 'TEXT', this.ErrorPayments, custId);
      }
      if (ref) {
        this.ErrorPayments = this._filterPipe.transform('Ref', 'TEXT', this.ErrorPayments, ref);
      }
      if (voucherDate) {
        this.ErrorPayments = this._filterPipe.transform('VoucherDate', 'TEXT', this.ErrorPayments, voucherDate);
      }
    } else if (type === 'VOU') {
      if (voucherDate) {
        this.ErrorPayments = this._filterPipe.transform('VoucherDate', 'TEXT', this.SourcePaymentList, voucherDate);
      } else {
        this.ErrorPayments = this.SourcePaymentList;
      }
      if (amount) {
        this.ErrorPayments = this._filterPipe.transform('Amount', 'TEXT', this.ErrorPayments, amount);
      }
      if (Kid) {
        this.ErrorPayments = this._filterPipe.transform('KID', 'TEXT', this.ErrorPayments, Kid);
      }
      if (custId) {
        this.ErrorPayments = this._filterPipe.transform('CustId', 'TEXT', this.ErrorPayments, custId);
      }
      if (ref) {
        this.ErrorPayments = this._filterPipe.transform('Ref', 'TEXT', this.ErrorPayments, ref);
      }
    }

    // reset all
    if (!custId && !ref && !Kid && !amount && !voucherDate) {
      this.ErrorPayments = this.SourcePaymentList;
    }
  }

  resetEvent() {
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'ECONOMY.ErrorpaymentResettMessage',
        msgBoxId: 'RESET'
      });
  }

  resetMappings() {
    this.ErrorPayments.forEach(payment => {
      payment.ARItemMatched = '';
      payment.IsMatched = false;
      payment.IsEnabledViewButton = true;
      payment.IsEnabledViewButton = true;
      payment.Ref = '';
    });
    this.aritemList = [];
    this.IsEnableCommitButton = false;
  }

  selectInvoice(content: any, payment: any) {
    this.selectedPayment = payment;
    const searchCriteria: any = {};
    let constValue: any;
    let searchvalue: any;
    this.fieldType = 'CustomerId';
    if (this.selectedPayment.ErrorPaymentType === 'Skredet' || this.selectedPayment.ErrorPaymentType === 'Exceeded') {
      this.fieldType = 'AR';
      searchCriteria.ARNumber = this.selectedPayment.ArNo;
    }

    searchCriteria.KID = this.selectedPayment.KID;
    searchCriteria.CustomerNumber = this.selectedPayment.CustId;
    searchCriteria.CreditorNumber = this.selectedPayment.InkassoId;
    searchCriteria.CreditorName = this.selectedPayment.CreditorName;
    searchCriteria.Amount = this.selectedPayment.Amount;
    searchCriteria.InvoiceNumber = this.selectedPayment.Ref;

    if (this.fieldType === 'AR') {
      constValue = searchCriteria.ARNumber;
      searchvalue = searchCriteria.ARNumber;
    } else {
      searchvalue = this.getCustomerReferenceFromKID(searchCriteria.KID)
      constValue = searchvalue;
    }

    this.economyService.searchInvoices(searchvalue, 0, this.fieldType, 'AR', constValue, this.selectedPayment.ID).subscribe(
      result => {
        if (result) {
          this.invoiceList = result.Data;
          this.sourceInvoices = result.Data;
          this.selectInvoiceRef = this.modalService.open(content);
        }
      }
    )
  }

  getCustomerReferenceFromKID(KID: string) {
    let referenceNo: Number = 1;
    if (KID.length === 13) {
      referenceNo = Number(KID.substr(0, 8));
      this.fieldType = 'CustomerId'
    } else if (KID.length === 11) {
      referenceNo = Number(KID.substr(0, 7));
      this.fieldType = 'ContractNo'
    }
    return referenceNo;
  }

  invoiceSelected(event: any) {
    const selectedInvoice = event.row.item;
    this.selectInvoiceRef.close();
    if (selectedInvoice) {
      selectedInvoice.IsSelected = true;
      if (this.aritemList.filter(X => X.SubCaseNo === selectedInvoice.SubCaseNo).length === 0) {
        // getting apportion data
        this.economyService.getApportionments('', '', this.selectedPayment.Amount, '-1', selectedInvoice.SubCaseNo).subscribe(
          result => {
            if (result) {
              const tempRemoveList: any[] = [];
              this.ApportionListForNotAppPayment.forEach(item => {
                if (item.PaymentID === this.selectedPayment.ID) {
                  tempRemoveList.push(item);
                }
              });

              tempRemoveList.forEach(removingItem => {
                const index = this.ApportionListForNotAppPayment.indexOf(removingItem);
                this.ApportionListForNotAppPayment.splice(index, 1);
              });

              // apply the apportionments list
              result.Data.forEach(item => {
                item.PaymentID = this.selectedPayment.ID;
                item.SubCaseNo = selectedInvoice.SubCaseNo;
                item.ErrorType = this.selectedPayment.ErrorPaymentType;
                item.ExceedPaymentApportionId = this.selectedPayment.ExceedPaymentApportionId;
                item.ExceedPaymentAritemNo = this.selectedPayment.ExceedPaymentAritemNo;
                this.ApportionListForNotAppPayment.push(item);
              });
            }
          }
        );
        this.aritemList.push(selectedInvoice);
        const epm = {
          ErrorPaymentId: this.selectedPayment.ID,
          ARItemNumber: selectedInvoice.ARItemNo
        }

        this.mappedErrorPayments.push(epm);
        this.selectedPayment.IsMatched = true;
        this.selectedPayment.Ref = selectedInvoice.InvoiceNumber;
        let message: string;
        this.translateService.get('ECONOMY.MatchedWith').subscribe(value => message = value);
        this.selectedPayment.ARItemMatched = message + selectedInvoice.InvoiceNumber;
        this.IsEnableCommitButton = true;
      }
    }
  }

  printDeviation() {
    const fromDate: any = this.manipulatedate(this.searchForm.value.startDate);
    const toDate: any = this.manipulatedate(this.searchForm.value.endDate);
    this.economyService.printDeviations(fromDate, toDate, this._branchId).subscribe(
      result => {
        if (result) {
          if (result.Data.FileParth.trim() !== '') {
            this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
          }
        }
      }, _error => {
        if (_error) {
          console.log(_error);
        }
      }
    );
  }

  filterInvoices(ref: any, KID: any, amount: any, invoiceDate: any, dueDate: any, type: string) {
    if (type === 'REF') {
      if (ref) {
        this.invoiceList = this._filterPipe.transform('InvoiceNumber', 'TEXT', this.sourceInvoices, ref);
      } else {
        this.invoiceList = this.sourceInvoices;
      }
      if (KID) {
        this.invoiceList = this._filterPipe.transform('KID', 'TEXT', this.invoiceList, KID);
      }

      if (amount) {
        this.invoiceList = this._filterPipe.transform('Amount', 'TEXT', this.invoiceList, amount);
      }

      if (invoiceDate) {
        this.invoiceList = this._filterPipe.transform('InvoicedDate', 'TEXT', this.invoiceList, invoiceDate);
      }

      if (dueDate) {
        this.invoiceList = this._filterPipe.transform('DueDate', 'TEXT', this.invoiceList, dueDate);
      }
    }else if (type === 'KID') {
      if (KID) {
        this.invoiceList = this._filterPipe.transform('KID', 'TEXT', this.sourceInvoices, KID);
      } else {
        this.invoiceList = this.sourceInvoices;
      }

      if (ref) {
        this.invoiceList = this._filterPipe.transform('InvoiceNumber', 'TEXT', this.invoiceList, ref);
      }

      if (amount) {
        this.invoiceList = this._filterPipe.transform('Amount', 'TEXT', this.invoiceList, amount);
      }

      if (invoiceDate) {
        this.invoiceList = this._filterPipe.transform('InvoicedDate', 'TEXT', this.invoiceList, invoiceDate);
      }

      if (dueDate) {
        this.invoiceList = this._filterPipe.transform('DueDate', 'TEXT', this.invoiceList, dueDate);
      }
    } else if (type === 'AMT') {
      if (amount) {
        this.invoiceList = this._filterPipe.transform('Amount', 'TEXT', this.sourceInvoices, amount);
      } else {
        this.invoiceList = this.sourceInvoices;
      }

      if (ref) {
        this.invoiceList = this._filterPipe.transform('InvoiceNumber', 'TEXT', this.invoiceList, ref);
      }
      if (KID) {
        this.invoiceList = this._filterPipe.transform('KID', 'TEXT', this.invoiceList, KID);
      }

      if (amount) {
        this.invoiceList = this._filterPipe.transform('Amount', 'TEXT', this.invoiceList, amount);
      }

      if (invoiceDate) {
        this.invoiceList = this._filterPipe.transform('InvoicedDate', 'TEXT', this.invoiceList, invoiceDate);
      }

      if (dueDate) {
        this.invoiceList = this._filterPipe.transform('DueDate', 'TEXT', this.invoiceList, dueDate);
      }
    } else if (type === 'INVDATE') {
      if (invoiceDate) {
        this.invoiceList = this._filterPipe.transform('InvoicedDate', 'TEXT', this.sourceInvoices, invoiceDate);
      } else {
        this.invoiceList = this.sourceInvoices;
      }

      if (ref) {
        this.invoiceList = this._filterPipe.transform('InvoiceNumber', 'TEXT', this.invoiceList, ref);
      }
      if (KID) {
        this.invoiceList = this._filterPipe.transform('KID', 'TEXT', this.invoiceList, KID);
      }

      if (amount) {
        this.invoiceList = this._filterPipe.transform('Amount', 'TEXT', this.invoiceList, amount);
      }

      if (invoiceDate) {
        this.invoiceList = this._filterPipe.transform('InvoicedDate', 'TEXT', this.invoiceList, invoiceDate);
      }

      if (dueDate) {
        this.invoiceList = this._filterPipe.transform('DueDate', 'TEXT', this.invoiceList, dueDate);
      }
    } else if (type === 'DUEDATE') {
      if (invoiceDate) {
        this.invoiceList = this._filterPipe.transform('InvoicedDate', 'TEXT', this.sourceInvoices, invoiceDate);
      } else {
        this.invoiceList = this.sourceInvoices;
      }

      if (ref) {
        this.invoiceList = this._filterPipe.transform('InvoiceNumber', 'TEXT', this.invoiceList, ref);
      }
      if (KID) {
        this.invoiceList = this._filterPipe.transform('KID', 'TEXT', this.invoiceList, KID);
      }

      if (amount) {
        this.invoiceList = this._filterPipe.transform('Amount', 'TEXT', this.invoiceList, amount);
      }



      if (dueDate) {
        this.invoiceList = this._filterPipe.transform('DueDate', 'TEXT', this.invoiceList, dueDate);
      }
    }

    // reset all
    if (!ref && !KID && !amount && !invoiceDate && !dueDate) {
      this.invoiceList = this.sourceInvoices;
    }
  }

  closeInvoiceView() {
    this.selectInvoiceRef.close();
  }

  commitRecordsEvent() {
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'ECONOMY.ErrorpaymentCommitMessage',
        msgBoxId: 'COMMIT'
      });
  }

  commitRecords() {
    if (this.ApportionListForNotAppPayment.length > 0) {
      this.economyService.addApportionments(this.ApportionListForNotAppPayment).subscribe(
        result => {
          if (result) {
            if (result.Data > 0) {
              this.messageService.openMessageBox('WARNING', {
                messageTitle: 'Exceline',
                messageBody: 'ECONOMY.MatchPaymentsuccessMsg'
              });
              this.ApportionListForNotAppPayment = [];
              this.mappedErrorPayments = [];
              this.aritemList = [];
              this.getUnknownPayments();
              this.IsEnableCommitButton = false;
            } else if (result.Data === -1) {
              this.messageService.openMessageBox('WARNING', {
                messageTitle: 'Exceline',
                messageBody: 'ECONOMY.MatchPaymentErrorMsg'
              });
            }
          }
        }
      );
    } else {
      this.messageService.openMessageBox('WARNING', {
        messageTitle: 'Exceline',
        messageBody: 'ECONOMY.SelectedErrorPaymentEmptyMsg'
      });
    }
  }

  onSelect(item: any) {
    this.searchText = item.searchText;
    this.searchVal = item.searchVal;
  }

  searchInvoice() {

  }

  ngOnDestroy() {
    if (this._yesHandlerSubcription) {
      this._yesHandlerSubcription.unsubscribe();
    }

    if (this.selectInvoiceRef) {
      this.selectInvoiceRef.close();
    }
  }
}
