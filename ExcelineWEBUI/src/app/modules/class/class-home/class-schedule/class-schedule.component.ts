import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import * as moment from 'moment';
import { EntitySelectionType, ColumnDataType, DayOfWeek } from '../../../../shared/enums/us-enum';
import { ExceClassService } from 'app/modules/class/services/exce-class.service';
import { ClassHomeService } from 'app/modules/class/class-home/class-home.service';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { UsbModalRef } from 'app/shared/components/us-modal/modal-ref';
import { TranslateService } from '@ngx-translate/core';
import { ClassListComponent } from '../class-list/class-list.component';
import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces/my-options.interface';


@Component({
  selector: 'class-schedule',
  templateUrl: './class-schedule.component.html',
  styleUrls: ['./class-schedule.component.scss']
})

export class ClassScheduleComponent implements OnInit, OnDestroy {
  startDate: any;
  translatedMsgs: any;
  model: any;
  resourceIdList: any[] = [];
  instructorIdList: any[] = [];
  updateClassActiveTimeHelper: any = {
    AddedINSIDList: [],
    DeletedINSIDList: [],
    AddedRESIDList: [],
    DeletedRESIDList: [],
    AllINSIDList: [],
    AllRESIDList: []
  };
  startTime: string;
  endTime: string;
  day: any;
  scheduleForm: FormGroup;
  timeDuration = 0;

  disableDate = new Date()
  limitDate = new Date()

  @Input() schedule: any;
  @Input() isDisableToTimeChange: boolean;
  @Input() isFixed: boolean;
  @Output() updateClassEvent = new EventEmitter();
  @Output() cancelUpdatingCls = new EventEmitter();

  classTypeList: any[];
  resourceRecords: any;
  selectedLocations: any[] = [];
  selectedInstructors: any[] = [];
  InstructorList: any[];
  entityItems: any[];
  entitySelectionModel: UsbModalRef;
  multipleSelect: boolean;
  entitySearchType: string;
  itemCount: any;
  startDateDisable = false;
  selectedClassType: any;
  TheDays: number[]= [ 1, 2, 3, 4, 5, 6, 7];
  classTypeData: any;
  classInfo: any;

  columnsToShow = ['LastName', 'FirstName', 'CustId'];

  message: string;
  type: string;

  columns = [
    { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
  ];

  auotocompleteItems = [
    { id: 'Id', name: 'Id :', isNumber: true },
    { id: 'Name', name: 'Name :', isNumber: false }
  ];

  disableDatePicker: IMyDpOptions = {
    disableSince: { year: this.disableDate.getFullYear() - 100, month: this.disableDate.getMonth() + 1, day: this.disableDate.getDay() }
  };

  restrictToDateToSeasonEndDate: IMyDpOptions = {
    disableSince: {year: this.limitDate.getFullYear() , month: this.limitDate.getMonth() + 1, day: this.limitDate.getDay()}
  }

  constructor(
    private exceClassService: ExceClassService,
    private classHomeService: ClassHomeService,
    private fb: FormBuilder,
    private exceMessageService: ExceMessageService,
    private modalService: UsbModal,
    private translate: TranslateService,
    private classList: ClassListComponent
  ) {
    this.entitySearchType = 'INS';
    this.multipleSelect = false;
    translate.get([
      'CLASS.Instructor',
      'CLASS.Resource',
      'CLASS.ScheduleOverlapWithInstructor',
      'CLASS.ScheduleOverlapWithResource',
      'CLASS.ClassEntityProcessMsg'
    ]).subscribe((res) => {
      this.translatedMsgs = res;
    });

  }

  ngOnInit() {
    if (this.isFixed === false) {
      this.classInfo = this.retriveClassList().ClassList;
      this.limitDate = new Date(this.retriveClassList().EndDate)
      this.restrictToDateToSeasonEndDate.disableSince = {year: this.limitDate.getFullYear() , month: this.limitDate.getMonth() + 1, day: this.limitDate.getDate() + 1}
    }

    this.schedule.ResourceLst.forEach(res => {
      this.selectedLocations.push({ Id: res.Id, Name: res.DisplayName });
    });
    this.schedule.InstructorList.forEach(ins => {
      this.selectedInstructors.push({ Id: ins.Id, Name: ins.DisplayName });
    });
    const stDate =  new Date(this.schedule.StartDate);
    const enDate = new Date(this.schedule.EndDate)

    this.timeDuration = this.schedule.ClassType.TimeDuration;

    this.scheduleForm = this.fb.group({
      ClassType: [this.schedule.ClassType.Name],
      ClassGroup: [null],
      StartDate: [{ date: { year: stDate.getFullYear(), month: stDate.getMonth() + 1, day: stDate.getDate() } }],
      EndDate: [{ date: { year: enDate.getFullYear(), month: enDate.getMonth() + 1, day: enDate.getDate() } }],
      StartTime: [moment(new Date(this.schedule.StartDateTime)).format('HH:mm')],
      EndTime: [{ value: moment(new Date(this.schedule.EndDateTime)).format('HH:mm'), disabled: this.isDisableToTimeChange }],
      MaxNoOfBooking: [this.schedule.MaxNoOfBookings],
      NumberOfMembers: [this.schedule.NumberOfMembers],
      DayOfTheWeek: [this.TheDays[this.schedule.Day - 1 ]]
    });

    const thisDate = new Date();
    if (stDate.getFullYear() <= thisDate.getFullYear() && stDate.getMonth() <= thisDate.getMonth() && stDate.getDate() <= thisDate.getDate()) {
      const schTime = this.schedule.StartDateTime.split('T')[1];
      if (Number(schTime.split(':')[0]) < thisDate.getHours()
      || (Number(schTime.split(':')[0]) === thisDate.getHours() && Number(schTime.split(':')[1]) <= thisDate.getMinutes())) {
        this.startDateDisable = true;
      } else {
        // editable
      }
    } else {
      // editable
    }
  }

  OpenLocationSelection(content) {
    this.multipleSelect = true;
    this.entitySelectionModel = this.modalService.open(content, { width: '1000' });
    this.columns = [
      { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];

    this.auotocompleteItems = [
      { id: 'Id', name: 'Id :', isNumber: true },
      { id: 'Name', name: 'Name :', isNumber: false }
    ];
    this.entitySearchType = 'RES';
  }

  openInstructorSelection(content) {
    // this.multipleSelect = true;
    this.entitySelectionModel = this.modalService.open(content, { width: '1000' });
    // this.columns = [
    //   { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    //   { property: 'CustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    // ];

    // this.auotocompleteItems = [
    //   { id: 'Id', name: 'Id :', isNumber: true },
    //   { id: 'Name', name: 'Name :', isNumber: false }
    // ];
    this.entitySearchType = 'INS';
  }


  searchDeatail(val: any) {
    this.entityItems = [];
    const searchArray = val.searchVal.split(':');

    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.CLA]:
        this.classTypeList = this.classHomeService.ClassTypeList
        if (searchArray.length > 1) {
          const searchType = searchArray[0].trim();
          switch (searchType) {
            case 'Name':
              this.entityItems = this.classTypeList.filter(x => x.Name.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));
              break;
            case 'ClassGroup':
              this.entityItems = this.classTypeList.filter(x => x.ClassGroup.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));
              break;
            case 'ClassLevel':
              this.entityItems = this.classTypeList.filter(x => x.ClassLevel.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));

              break;
            case 'ClassCategory':
              this.classTypeList.forEach(categoryType => {
                const items = categoryType.ClassCategoryStringList.filter(x => x.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));
                if (items.length > 0) {
                  this.entityItems.push(categoryType);
                }
              });
              break;
            case 'ClassKeywords':
              this.classTypeList.forEach(categoryType => {
                if (categoryType.ClassKeywordString) {
                  const items = categoryType.ClassKeywordString.split(',').filter(x => x.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()))
                  if (items.length > 0) {
                    this.entityItems.push(categoryType);
                  }
                }
              });
              break;
            default:
              this.entityItems = this.classTypeList;
              break;
          }
        } else {
          this.entityItems = this.classTypeList;
        }
        this.entityItems = _.orderBy(this.entityItems, 'Name');
        break;
      case EntitySelectionType[EntitySelectionType.INS]:
        this.InstructorList = this.classHomeService.InstructorList;
        if (searchArray.length > 1) {
          const searchType = searchArray[0].trim();
          switch (searchType) {
            case 'Name':
              this.entityItems = this.InstructorList.filter(x => x.Name.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));
              break;
            case 'Id':
              this.entityItems = this.InstructorList.filter(x => x.CustId.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));
              break;
            default:
              this.entityItems = this.InstructorList;
              break;
          }
        } else {
          this.entityItems = this.InstructorList;
        }
        this.entityItems = _.orderBy(this.entityItems, 'Name');
        if (this.selectedInstructors.length > 0) {
          this.selectedInstructors.forEach(ins => {
            this.entityItems = this.entityItems.filter(x => x.Id !== ins.Id);
          });
        }
        break;
      case EntitySelectionType[EntitySelectionType.RES]:
        this.resourceRecords = this.classHomeService.ResourceRecords;
        if (searchArray.length > 1) {
          const searchType = searchArray[0].trim();
          switch (searchType) {
            case 'Name':
              this.entityItems = this.resourceRecords.filter(x => x.Name.toLowerCase().startsWith(searchArray[1].trim().toLowerCase()));
              break;
            case 'Id':
              this.entityItems = this.resourceRecords.filter(x => x.Id === Number(searchArray[1]));
              break;
            default:
              this.entityItems = this.resourceRecords;
              break;
          }
        } else {
          this.entityItems = this.resourceRecords;
        }
        this.entityItems = _.orderBy(this.entityItems, 'Name');
        if (this.selectedLocations.length > 0) {
          this.selectedLocations.forEach(ins => {
            this.entityItems = this.entityItems.filter(x => x.Id !== ins.Id);
          });
        }
        break;
    }
  }

  inputFieldChanged(event) {
    this.setTimes(event, this.timeDuration);
  }

  openClassTypeSelection(content) {
    this.entitySelectionModel = this.modalService.open(content, { width: '1000' });
    this.columns = [
      { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'ClassGroup', header: 'ClassGroup', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'ClassLevel', header: 'ClassLevel', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];

    this.auotocompleteItems = [
      { id: 'Name', name: 'Name :', isNumber: false },
      { id: 'ClassGroup', name: 'Class Group :', isNumber: false },
      { id: 'ClassLevel', name: 'Class Level :', isNumber: false },
      { id: 'ClassCategory', name: 'Class Category :', isNumber: false },
      { id: 'ClassKeywords', name: 'Class Keywords :', isNumber: false }
    ];
    this.multipleSelect = false;
    this.entitySearchType = 'CLA';

  }


  selectedItem(event) {
    this.classTypeData = event
    switch (this.entitySearchType) {
      case 'CLA':
        this.scheduleForm.patchValue({
          ClassType: event.Name,
          Group: event.ClassGroup,
          MaxNoOfBooking: event.MaxNoOfBookings
        });

        this.setTimes(this.startTime, event.TimeDuration);
        this.timeDuration = event.TimeDuration;
        this.selectedClassType = event;
        break;
      default:
        break;
    }
    this.entitySelectionModel.close();
  }

  setTimes(startTime, duration) {
    const date = moment(new Date()).format('YYYY-MM-DD');
    this.endTime = (moment(date + ' ' + startTime).add(duration, 'minute')).format('HH:mm');
    this.scheduleForm.patchValue({
      StartTime: startTime,
      EndTime: this.endTime
    });
  }

  selectMultipleItems(event) {
    switch (this.entitySearchType) {
      case 'INS':
        event.forEach(ins => {
          this.selectedInstructors.push(ins);
          this.updateClassActiveTimeHelper.AddedINSIDList.push(ins.Id);
        });
        break;
      case 'RES':
        event.forEach(loc => {
          this.selectedLocations.push(loc);
          this.updateClassActiveTimeHelper.AddedRESIDList.push(loc.Id);
        });
        break;

      default:
        break;
    }
    this.entitySelectionModel.close();
  }

  closeEntView() {
    this.entitySelectionModel.close();
  }

  removeInstructor(item) {
    this.selectedInstructors = this.selectedInstructors.filter(x => x.Id !== item.Id);
    this.updateClassActiveTimeHelper.DeletedINSIDList.push(item.Id);
  }

  removeLocation(item) {
    this.selectedLocations = this.selectedLocations.filter(x => x.Id !== item.Id);
    this.updateClassActiveTimeHelper.DeletedRESIDList.push(item.Id);
  }

  private getTimePickerDateVal(time) {
    const date = moment(new Date()).format('YYYY-MM-DD');
    return (moment(date + ' ' + time)).format('YYYY-MM-DD HH:mm');
  }

  // getDateChange(event) {
  //   this.startDate = event.date;
  // }

  // convert date js object in to string
  getDateConverter(datee: any) {
    const dd = (datee.day < 10 ? '0' : '') + datee.day;
    const MM = ((datee.month + 1) < 10 ? '0' : '') + datee.month;
    const yyyy = datee.year;
    return (yyyy + '-' + MM + '-' + dd);
  }

  saveClass() {
    if (!this.startDateDisable) {
    this.selectedInstructors.forEach(ins => {
      this.instructorIdList.push(ins.Id);
    });
    this.selectedLocations.forEach(res => {
      this.resourceIdList.push(res.Id);
    });


    // this.updateClassActiveTimeHelper.StartDate = this.getDateConverter(this.startDate);
    // this.updateClassActiveTimeHelper.StartDateTime = this.getTimePickerDateVal(this.scheduleForm.getRawValue().StartTime);
    // this.updateClassActiveTimeHelper.EndDateTime = this.getTimePickerDateVal(this.scheduleForm.getRawValue().EndTime);

    this.updateClassActiveTimeHelper.StartDateTime = this.getDateConverter(this.scheduleForm.getRawValue().StartDate.date) + ' ' + this.scheduleForm.getRawValue().StartTime + ':00';
    this.updateClassActiveTimeHelper.EndDateTime = this.getDateConverter(this.scheduleForm.getRawValue().StartDate.date) + ' ' + this.scheduleForm.getRawValue().EndTime + ':00';
    this.updateClassActiveTimeHelper.ActiveTimeId = this.schedule.Id;
    this.updateClassActiveTimeHelper.AllINSIDList = this.instructorIdList;
    this.updateClassActiveTimeHelper.AllRESIDList = this.resourceIdList;
    this.updateClassActiveTimeHelper.MaxNumberOfBookings = this.scheduleForm.value.MaxNoOfBooking;
    this.updateClassActiveTimeHelper.NoOfParticipants = this.scheduleForm.value.NumberOfMembers;
    PreloaderService.showPreLoader();

    const sheduleItem = {
      Id: this.schedule.SheduleItemId,
      ActiveTimeID: this.schedule.Id,
      ActiveTimes: {
        StartDate: this.schedule.StartDate,
        EndDate: this.schedule.EndDate,
        StartDateTime: this.updateClassActiveTimeHelper.StartDateTime,
        EndDateTime: this.updateClassActiveTimeHelper.EndDateTime
      },
      InstructorIdList: this.instructorIdList,
      ResourceIdList: this.resourceIdList

    };

    if (this.instructorIdList.length > 0 || this.resourceIdList.length > 0) {
      this.exceClassService.CheckActiveTimeOverlapWithClass(sheduleItem).subscribe(
        res => {
          const sTextArray = res.Data.split(':');
          if (sTextArray.length > 0) {
            if (Number(sTextArray[0]) === 1) {
              this.SaveScheduleItem();
            } else if (Number(sTextArray[0]) === -2) {
              let entityMsg = '';
              if (sTextArray.length > 1) {
                const entityArray1 = sTextArray[1].split(',');
                if (entityArray1[0] === 'INS') {
                  entityMsg = this.translatedMsgs['CLASS.Instructor'] + ':' + entityArray1[1];
                } else if (entityArray1[0] === 'RES') {
                  entityMsg = this.translatedMsgs['CLASS.Resource'] + ':' + entityArray1[1];
                }
                if (sTextArray.length > 2) {
                  const entityArray2 = sTextArray[2].split(',');
                  if (entityArray2[0] === 'RES') {
                    entityMsg = entityMsg + ' ' + this.translatedMsgs['CLASS.Resource'] + ':' + entityArray2[1];
                  }
                }
                const confirmMsg = entityMsg + ' ' + this.translatedMsgs['CLASS.ClassEntityProcessMsg'];
                this.model = this.exceMessageService.openMessageBox('CONFIRM',
                  {
                    messageTitle: 'Exceline',
                    messageBody: confirmMsg
                  });
              }
            }
          }
        }, err => {
        });
    } else {
      this.SaveScheduleItem();
    }
  }
  }

  getUpdatedList() {
    this.classList.searchClasses();
  }

  SaveScheduleItem() {
  this.classList.isFixed ? this.saveOnlyOne() : this.saveAll()
  }

  saveOnlyOne() {
    this.exceClassService.UpdateClassActiveTime(this.updateClassActiveTimeHelper).subscribe(
      res => {
        this.getUpdatedList();
        PreloaderService.hidePreLoader();
        if (res.Data === 0) {
         /*  this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.UpdatedSuccessfully'
            }
          ); */
          this.translate.get('CLASS.UpdatedSuccessfully').subscribe(deleteMessage => this.message = deleteMessage);
          this.type = 'SUCCESS';
          this.updateClassEvent.emit({
            type: this.type,
            message: this.message
          });
        } else if (res.Data === -1) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithInstructor'
            }
          );
        } else if (res.Data === -2) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithResource'
            });
        }
      }, err => {
        PreloaderService.hidePreLoader();
      });
  }

  saveAll() {
    PreloaderService.showPreLoader();
    let dotw;
    this.translate.get('DAY.' + (this.scheduleForm.getRawValue().DayOfTheWeek )).subscribe(dow => dotw = dow);
    let testList = this.classInfo;
    testList = testList.find(x => x.ScheduleItemId === this.schedule.SheduleItemId)
    let activeTimes =  testList.ClassInstanceList.filter(y => y.Occurence === 'WEEKLY')
    let activeTimesBody = [];
    activeTimes.forEach(activeTimeElement => {
    let bodyElement = {
    Id: activeTimeElement.Id,
    SheduleItemId: this.schedule.SheduleItemId,
    ScheduleItem: this.schedule.ScheduleId,
    StartDateTime: activeTimeElement.StartTime,
    EndDateTime: activeTimeElement.EndTime,
    StartDate: activeTimeElement.Date,
    EndDate: activeTimeElement.Date,
    MaxNoOfBookings: this.scheduleForm.getRawValue().MaxNoOfBooking,
    ResourceLst: this.selectedLocations,
    InstructorList: this.selectedInstructors
    }
    activeTimesBody.push(bodyElement)
    });
    const scheduleBody = {
      ClassTypeId: this.schedule.ClassTypeId,
      ClassType: this.schedule.ClassType,
      StartDate: this.scheduleForm.getRawValue().StartDate.date.year + '-' + this.scheduleForm.getRawValue().StartDate.date.month + '-' + this.scheduleForm.getRawValue().StartDate.date.day ,
      EndDate: this.scheduleForm.getRawValue().EndDate.date.year + '-' + this.scheduleForm.getRawValue().EndDate.date.month + '-' + this.scheduleForm.getRawValue().EndDate.date.day,
      StartTime: this.getTimePickerDateVal(this.scheduleForm.getRawValue().StartTime),
      EndTime: this.getTimePickerDateVal(this.scheduleForm.getRawValue().EndTime),
      MaxNoOfBooking: this.scheduleForm.getRawValue().MaxNoOfBooking,
      InstructorIdList: this.instructorIdList,
      ResourceIdList: this.resourceIdList,
      IsFixed: false,
      Occurrence: 'WEEKLY',
      Day: dotw, // this.scheduleForm.getRawValue().Day,
      Week: 0,
      Month: 0,
      Year: 0,
      WeekType: 2, // this.scheduleForm.getRawValue().WeekType,
      ScheduleId: this.schedule.ScheduleId,
      Id: this.schedule.SheduleItemId,
      Activetimes: activeTimesBody
    }
    this.exceClassService.SaveScheduleItem({ScheduleItem: scheduleBody}).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {

          this.classList.searchClasses();

        } else if (res.Data === -1) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithInstructor'
            }
          );
        } else if (res.Data === -2) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithResource'
            }
          );
        }
      }, err => {
        PreloaderService.hidePreLoader();

      }, () =>  {PreloaderService.hidePreLoader()
        this.translate.get('CLASS.UpdatedSuccessfully').subscribe(deleteMessage => this.message = deleteMessage);
        this.type = 'SUCCESS';
        this.updateClassEvent.emit({
          type: this.type,
          message: this.message
        });
      }
    );

  }


retriveClassList() {
return this.classList.exportClassList();
}

  cancelUpdatingClass() {
    this.cancelUpdatingCls.emit();
  }

  ngOnDestroy(): void {
    if (this.entitySelectionModel) {
      this.entitySelectionModel.close();
    }
  }

}
