import { Extension } from './../../../../shared/Utills/Extensions';
import { ExcePdfViewerService } from '../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { UsErrorService } from '../../../../shared/directives/us-error/us-error.service';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { Guid } from '../../../../shared/services/guid.service';
import { ExceSessionService } from '../../../../shared/services/exce-session.service';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces/my-options.interface';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ShopHomeService } from '../shop-home.service';
import { CategoryTypes } from '../../../../shared/enums/category-types';
import { Component, OnInit, OnDestroy, Output, EventEmitter, ViewChild, ElementRef, ViewChildren, NgZone } from '@angular/core';
import { DataTableResource } from 'app/shared/components/us-data-table/tools/data-table-resource';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { ExceShopService } from 'app/modules/shop/services/exce-shop.service';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription, timer } from 'rxjs';
import { SaleErrors } from 'app/shared/enums/us-enum';
import { UsAmountInputComponent } from '../../../../shared/components/us-amount-input/us-amount-input/us-amount-input.component';
import { Member } from '../../../../shared/SystemObjects/Common/Member';
import { TouchSequence } from 'selenium-webdriver';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { AdminService } from 'app/modules/admin/services/admin.service';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';

const now = new Date();
const tomorrow = new Date();
tomorrow.setDate(now.getDate() + 1);

@Component({
  selector: 'shop-payment',
  templateUrl: './shop-payment.component.html',
  styleUrls: ['./shop-payment.component.scss']
})
export class ShopPaymentComponent implements OnInit, OnDestroy {

  paymentsList = [];
  itemResource = new DataTableResource(this.paymentsList);

  locale: string;

  selectedShopItemSubscribe: any;
  selectedMemberSubscribe: any;
  totalPriceChangedSubscribe: any;
  ///////////////////////////////////

  custBranchId?: number;
  branchId?: number;
  totalPrice?: number;
  paymentModes?: any[];
  shopAccount?: any = {};
  isTrailItem = false;
  trailArticleId = -1;
  trailItemName = '';
  isDropinItem = false;
  dropinArticleId = -1;
  dropinItemName = '';

  isOnAccountPayment?: boolean;
  isGiftVoucher = false;
  isOCR = false;
  negativeAmount?: number;
  branchName?: string;
  // ShopLogin loginData,
  isCreditNote?: boolean;
  isBookingPayment?: boolean;
  custRoleType?: string;
  giftVoucher: any;
  // ////////////////////
  paymentType: any;
  paymentTypeObject: any;
  isCardPayment: boolean;
  public paymentForm: FormGroup;
  selectedMember?: any = {};
  isHasGuardian = false;
  isWithdrawFromCard = false;
  gymName = '';
  selectedDate: any;

  SeshKey: any;
  PaymentTypes = [];

  salesDefs: any;

  PaymentType: {
    Type: String;
    TypeCode: String;
    TypeId: String;
    Amount: String;
  }

  Cart = [];
  CartItem: {
    CartItemNo: number;
    ArticleId: String;
    ArticleName: String;
    ArticlePrice: String;
    CurrentArticleStock: String;
    DiscountPercentage: String;
    DiscountAmount: String;
    NumberOfItems: String;
    MVA: String;
    UnitPriceExcludingMVA: String;
    TotalPriceExcludingMVA: String;
    TotalPriceIncludingMVA: String;
    DiscountComment: String;
    InstallmentShareId: String;
    ShopSaleItemId: String;
    ARItemNo: String;
    OrderLineId: String;
    ReturnComment: String;
  }

  SaleXMLData: {
    SalePointInfo: {
      BranchId: String;
      MachineName: String;
      SalePointId: String;
      UserName: String;
      SessionsKey: String;
      TerminalActive: String;
      Date: String;
      Time: String;
      CreditorNo: String;
    }
    CustomerInfo: {
      IsDefaultCustomer: String;
      CustomerId: String;
      MemberId: String;
      MemberBranchId: String;
      RoleType: String;
    }
    PaymentTypeContent: {
      PaymentTypes: any[];
    }
    ShopCartContent: {
      CartItems: any[];
    }
    PostSaleInfo: {
      InstallmentId: String;
      ARNo: String;
      ShopSaleId: String;
      PaymentId: String;
    }
    };

  isSelectedMember = false;

  cancleConfirmation: any;
  timer: any;
  timerSub: any;
  itemCount = 0;
  paymentListId = 0;
  dailySettlement?: any;
  inHandBalance: Number;

  yesHandlerSubscription: any;
  paymentFormSubmited: boolean;

  paidTotal = 0;
  remaningBalance = 0;

  type: string;
  isShowInfo: boolean;
  message: string;
  isShowInfo2: boolean;

  @ViewChildren('givenAmount') givenAmount: ElementRef;
  @ViewChild('payAmount', { static: true }) paymentAmount;
  @ViewChild('balance', { static: false }) viewBalance;
  @ViewChild('total', { static: true }) viewTotal;
  @ViewChild('returnCommentModal', { static: true }) returnModal

  paymentDateOptions: IMyDpOptions = {
    disableSince: { year: tomorrow.getFullYear(), month: tomorrow.getMonth() + 1, day: tomorrow.getDate() },
  };

  formErrors = {
    'PayAmount': '',
    'PaymentType': ''
  }

  validationMessages = {
    'PayAmount': {
      'required': 'COMMON.MsgAmountRequired',
      'PayAmountRequired': 'COMMON.MsgAmountRequired',
      'InvalidAmount': 'COMMON.MsgInvalidAmount',
      'NotEnoughBalance': 'COMMON.MsgNotEnoughBalance',
      'AmountLimitation': 'COMMON.MsgPaymentAmountLimitation',
      'PayementAdded': 'COMMON.MsgPayementAdded'
    },
    'PaymentType': {
      'required': 'COMMON.MsgPaymentTypeRequired'
    }
  }

  @Output() closePayment = new EventEmitter<any>();
  isSelectedMemberSubscribe: any;
  isBankTerminalActiveSubscribe: Subscription;
  isBankTerminalActive = true;
  paymentModes2: any[];
  cardtype: any;
  sessionKeySubscribe: Subscription;
  communicationBranch: any;
  paymentSubscriber: Subscription;
  paymentTypeDisplay: any;
  IsCashSalesActivated = false;
  isShopReturn = false;
  clearShopItemSubscribe: Subscription;
  returnModalHandler: any;
  gridApi: any;
  returnSales: any[];
  returnComment = '';
  returnInvoiceNo = '';
  cashdraweractivedsub: Subscription;
  showMessageEvent: any;

  constructor(
    private toolbarService: ExceToolbarService,
    private shopService: ExceShopService,
    private shopHomeService: ShopHomeService,
    private fb: FormBuilder,
    private router: Router,
    private modalService: UsbModal,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private _ngZone: NgZone,
    private exceLoginService: ExceLoginService,
    private excePdfViewerService: ExcePdfViewerService,
    private exceSessionService: ExceSessionService,
    private adminService: AdminService
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.branchName = this.exceLoginService.SelectedBranch.BranchName;

    this.itemResource.count().then(count => this.itemCount = count);
    // this.shopAccount = new ShopAccount();
    this.shopAccount.Balance = 0.0;

    this.paymentForm = this.fb.group({
      'TotalPayable': [0],
      'PaymentType': [null],
      'PayAmount': [0, [Validators.required], [this.payAmountValidator.bind(this)]],
      'GivenAmount': [null],
      'VoucherNumber': [null],
      'PaymentDate': [{ value: now, disabled: true }],
      'GymName': [{ value: this.branchName, disabled: true }],
      'CashBalance': [{ value: null, disabled: true }]
    });
    this.paymentForm.statusChanges.subscribe(_ => {
      UsErrorService.onValueChanged(this.paymentForm, this.formErrors, this.validationMessages);
    });

  }

  ngOnInit() {
    // **Language selecton and subscription - dateformat and amountconverter uses this **
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';
    this.toolbarService.langUpdated.subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
    // **END**

    // this.shopService.refreshArticleListEvent.next(true);
    if (this.shopHomeService.$fromMcOrders) {
    const totalSum = this.shopHomeService.$totalPrice.toFixed(2)
    this.shopHomeService.$totalPrice = totalSum
    }

    if (this.shopHomeService.$shopLoginData) {
      this.negativeAmount = this.shopHomeService.$shopLoginData.GymSetting.OnAccountNegativeVale;
    }

    if (this.shopHomeService.$isSelectedMember) {
      this.isSelectedMember = true;
      this.shopService.GetCategories({ type: CategoryTypes[CategoryTypes.PAYMODE] }).subscribe(
        categories => {
          this.paymentModes = categories.Data;
          this.paymentModes = this.translatePaymentmodes(this.paymentModes);
        }
      )
      // this.shopHomeService.$isSelectedMember = true;
      this.selectedMember = this.shopHomeService.$selectedMember;
      this.isSelectedMember = true;
    }

    this.selectedMemberSubscribe = this.shopHomeService.memberSelectedEvent.subscribe(
      (member) => {
        if (member) {
          let hasValue;
          try {
            hasValue = member.Id
          } catch (err) {
            hasValue = false;
          }
          if (hasValue) {
            this.isSelectedMember = true;
            // this.shopHomeService.$isSelectedMember = true;
            this.selectedMember = member;
            this.shopService.GetCategories({ type: CategoryTypes[CategoryTypes.PAYMODE] }).subscribe(
              categories => {
                this.paymentModes = categories.Data;
                this.paymentModes = this.translatePaymentmodes(this.paymentModes);
              }
            )
          } else {
            this.isSelectedMember = false;
            // this.shopHomeService.$isSelectedMember = false;
            this.selectedMember = null;
          }
        } else {
          this.isSelectedMember = false;
          this.selectedMember = null;
          // this.shopService.GetCategories({ type: CategoryTypes[CategoryTypes.PAYMODE] }).subscribe(
          //   categories => {
          //     this.paymentModes = categories.Data;
          //     this.paymentModes = this.translatePaymentmodes(this.paymentModes);
          //   }
          // )
        }
        if (this.shopHomeService.$isSelectedMember) {
          this.isSelectedMember = true;
        }
        // this.selectedMember = member;
      }
    );

    this.isCreditNote = this.shopHomeService.$isCreditNote;
    this.isOnAccountPayment = this.shopHomeService.$isOnAccountPayment;
    this.isBookingPayment = this.shopHomeService.$isBookingPayment;
    this.giftVoucher = this.shopHomeService.$giftVoucher
    this.selectedMember = this.shopHomeService.$selectedMember;
    // when leaving shop selectedmember is set to empty object
    // so its not null and tru. Added try / catch to solve problem
    if (this.selectedMember) {
      let hasValue;
      try {
        hasValue = this.selectedMember.Id
      } catch (err) {
        hasValue = false;
      }
      if (hasValue) {
        this.isSelectedMember = true;
        // this.shopHomeService.$isSelectedMember = true;
      }
    }
    this.shopAccount = this.shopHomeService.$selectedMember ? this.shopHomeService.$shopAccount : this.shopAccount;
    this.shopService.GetCategories({ type: CategoryTypes[CategoryTypes.PAYMODE] }).subscribe(
      categories => {
        if (categories) {
          this.paymentModes2 = categories.Data.filter(cat => cat.Code === 'CASH' || cat.Code === 'BANKTERMINAL' || cat.Code === 'VIPPS')
          this.paymentModes2 = this.translatePaymentmodes(this.paymentModes2);
          if (this.shopHomeService.$shopMode === 'CREDITNOTE') {
            this.isCreditNote = true;
            this.paymentModes = categories.Data.filter(cat => cat.Code === 'CASH' || cat.Code === 'ONACCOUNT' || cat.Code === 'BANK' || cat.Code === 'PREPAIDBALANCE');
            this.paymentModes = this.translatePaymentmodes(this.paymentModes);
          } else if (this.isOnAccountPayment) {
            this.paymentModes = categories.Data.filter(cat => cat.Code === 'CASH' || cat.Code === 'BANKTERMINAL' || cat.Code === 'CARD' || cat.Code === 'VIPPS');
            this.paymentModes = this.translatePaymentmodes(this.paymentModes);
          } else {
            this.paymentModes = categories.Data;
            this.paymentModes = this.translatePaymentmodes(this.paymentModes);
          }

          if (this.shopHomeService.$selectedMember && (this.branchId === this.shopHomeService.$selectedMember.BranchID)) {
            if (this.shopAccount && this.shopAccount.PrepaidBalance <= 0  && this.shopHomeService.$shopMode !== 'CREDITNOTE') {
              this.paymentModes = this.paymentModes.filter(cat => cat.Code !== 'PREPAIDBALANCE')
            }
          } else {
            this.paymentModes = this.paymentModes.filter(cat => ((cat.Code !== 'PREPAIDBALANCE') && (cat.Code !== 'ONACCOUNT')))
          }

          const bankTerminal = this.paymentModes.filter(cat => cat.Code === 'BANKTERMINAL')
          if (this.shopHomeService.$isCashCustomerSelected && !this.isSelectedMember) {
            if (bankTerminal) {
              this.paymentModes = this.paymentModes.filter(item => {
                if (item.Code === 'BANKTERMINAL' || item.Code === 'CASH' || item.Code === 'VIPPS') {
                  return true;
                }
              });
            } else {
              this.paymentModes = this.paymentModes.filter(cat => cat.Code === 'CASH')
            }
          } else {
            if (this.isHasGuardian) {
              this.paymentModes = this.paymentModes.filter(item => {
                if (item.Code !== 'INVOICE' || item.Code !== 'NEXTORDER' || item.Code !== 'EMAILSMSINVOICE') {
                  return true;
                }
              });
            }
          }

          this.totalPrice = this.shopHomeService.$totalPrice;
          if (this.shopHomeService.$shopMode === 'CREDITNOTE' || this.shopHomeService.$shopMode === 'BOOKINGCANCELLATION') {
            this.paymentType = 'CASH';
            this.paymentForm.patchValue({ 'PaymentType': 'CASH', 'TotalPayable': this.totalPrice, 'PayAmount': this.totalPrice, 'GivenAmount': this.totalPrice });
            this.paymentForm.get('GivenAmount').enable();
            this.paymentForm.get('CashBalance').enable();
            this.isCardPayment = false;
          } else {
            this.paymentForm.patchValue({ 'PaymentType': bankTerminal ? 'BANKTERMINAL' : this.paymentModes[0].Code });
            this.paymentType = bankTerminal ? 'BANKTERMINAL' : this.paymentModes[0].Code;
            this.paymentTypeObject = this.paymentModes.filter(cat => cat.Code === this.paymentType)[0];
            let displayname = '';
            (this.translateService.get(this.paymentTypeObject.Display).subscribe(tranlstedValue => displayname = tranlstedValue));
            this.paymentTypeDisplay = displayname;
            this.isCardPayment = true;
            this.paymentForm.get('GivenAmount').disable();
            this.paymentForm.get('CashBalance').disable();
          }

          if (this.totalPrice && this.totalPrice >= 0) {
            this.paymentForm.patchValue({ 'TotalPayable': this.totalPrice, 'PayAmount': this.totalPrice });
            this.remaningBalance = this.totalPrice;
            if (!this.shopHomeService.$shopLoginData.GymSetting.IsSMSInvoiceShop) {
              this.paymentModes = this.paymentModes.filter(cat => cat.Code !== 'EMAILSMSINVOICE')
            }
          } else if (this.totalPrice && this.totalPrice < 0) {
            this.paymentModes = this.paymentModes.filter(cat => cat.Code === 'CASH');
            this.paymentForm.patchValue({ 'PaymentType': 'CASH', 'TotalPayable': this.totalPrice, 'PayAmount': this.totalPrice });
            this.isCardPayment = false;
            this.paymentType = 'CASH';
          }
        }


      });


    if (this.selectedMember && this.selectedMember.GuardianId && this.selectedMember.GuardianId > 0) {
      this.isHasGuardian = true;
    }

    this.yesHandlerSubscription = this.exceMessageService.yesCalledHandle.subscribe((value) => {
      if (value.id === 'CONFIRM_INHANDBALANCE') {
        this.shopHomeService.$paymentAdded = true;
        this.paymentsList.push({
          Id: this.paymentListId,
          Amount: value.optionalData.inHandBalance,
          PaymentType: 'WITHDRAW',
          PaymentTypeCode: 'CASHWITHDRAWAL',
          IsAbleDelete: value.optionalData.isDeleteAble,
          IsReadOnly: true
        });
        this.paymentListId++;
        this.setBalance();
      } else if (value.id === 'CANCEL_PAYMENT') {
        this.closePayment.emit(false);
        this.shopHomeService.$selectedMember = null;
      }

    });

    this.shopService.GetDailySettlements({ salePointId: this.shopHomeService.$shopLoginData.SalePoint.ID }).subscribe(
      res => {
        this.dailySettlement = res.Data;
        this.inHandBalance = (res.Data.CashIn - res.Data.CashOut);
        if (this.inHandBalance < 0) {
          this.inHandBalance = 0;
        }
      });

      this.totalPriceChangedSubscribe = this.shopHomeService.totalPriceChangeEvent.subscribe(
        (value) => {
          if (value || value === 0) {
            this.totalPrice = value;
            this.paymentForm.patchValue({TotalPayable: value})
            this.paymentForm.patchValue({PayAmount: value.toFixed(2)})
          }
        }
      )

      this.isBankTerminalActiveSubscribe = this.exceSessionService.isBankTerminalActiveEvent.subscribe(
        (value) => {
          this.isBankTerminalActive = value;
          if (!value) {
            this.paymentTypeDisplay = null;
          }
        }
      )

      // necessary?
      // this.isSelectedMemberSubscribe = this.shopHomeService.isSelectedMemberEvent.subscribe(
      //   (value) => {
      //     this.isSelectedMember = value;
      //     if (value === false) {
      //       // this.paymentModes = this.paymentModes.filter(cat => cat.Code === 'CASH' || cat.Code === 'BANKTERMINAL')
      //     }
      //   }
      // );

      this.selectedShopItemSubscribe = this.shopHomeService.shopItemChanged
      .subscribe(
      (item) => {
        this.totalPrice = this.shopHomeService.$totalPrice
        this.paymentForm.patchValue({TotalPayable: this.shopHomeService.$totalPrice})
        this.paymentForm.patchValue({PayAmount: this.shopHomeService.$totalPrice.toFixed(2)})
      });


      this.sessionKeySubscribe = this.shopHomeService.sessionKeyGymCodeEvent.subscribe(
        key => {
          if (key) {
            this.SeshKey = key;
            this.timer = timer(0, 5000);
            this.timerSub = this.timer.subscribe(t => this.tickForSessionKey(t, key));
          }
        }
      )

      // listening for savepayment called from savebutton in shop-home
      this.paymentSubscriber = this.shopHomeService.savePaymentEvent.subscribe(
        call => {
          const test = this.shopHomeService.getShopItems();
          if (this.isShopReturn) {
            const articles = this.shopHomeService.getShopItems();
            if (articles.length !== 1) {
              this.translateService.get('SHOP.OnlyOneArticle').subscribe(deleteMessage => this.message = deleteMessage);
              this.type = 'DANGER';
              this.isShowInfo = true;
              setTimeout(() => {
                this.isShowInfo = false;
              }, 4000);
            } else {
              this.returnModalHandler = this.modalService.open(this.returnModal, { width: '800' });
            }
            return
          } else {
            this.savePayment(this.cardtype ? this.cardtype : '');
          }
        }
      )

      // if (this.shopHomeService.$selectedMember.Id.length !== 0) {
      //   this.shopService.GetCategories({ type: CategoryTypes[CategoryTypes.PAYMODE] }).subscribe(
      //     categories => {
      //       this.paymentModes = categories.Data;
      //       this.paymentModes = this.translatePaymentmodes(this.paymentModes);
      //     }
      //   )
      // }

      // this.adminService.getGymSettings('ECONOMY', null, this.branchId).subscribe(
      //   ecoSetting => {
      //     this.IsCashSalesActivated = ecoSetting.Data[0].IsCashSalesActivated;
      //   }
      // );
      this.cashdraweractivedsub = this.shopHomeService.IsCashSalesActivatedEvent.subscribe(activated => {
        // depends on if cashsales is activated in settings and machieprofil has POS with cashdrawer in hardware. Shop-home setting the value
        this.IsCashSalesActivated = activated;
      })

      this.shopHomeService.shopItemReturnEvent.subscribe( isReturn => {
        // håndtering av retur - kun lov med kontant
        this.isShopReturn = isReturn;
        this.paymentTypeObject = this.paymentModes.filter(cat => cat.Code === 'CASH')[0];
        this.shopHomeService.$isOnAccountPayment = true;

        this.paymentForm.get('VoucherNumber').disable();
        this.paymentForm.get('PaymentDate').disable();
        this.paymentForm.get('GymName').disable();
        this.paymentForm.get('CashBalance').disable();
        this.paymentForm.get('GivenAmount').disable();

        let displayname = '';
        (this.translateService.get(this.paymentTypeObject.Display).subscribe(tranlstedValue => displayname = tranlstedValue));

        this.paymentType = 'CASH';
        this.paymentTypeDisplay = displayname;

        this.paymentForm.get('GivenAmount').enable();
        this.paymentForm.get('CashBalance').enable();
        this.isCardPayment = false;
      })

      this.clearShopItemSubscribe = this.shopHomeService.clearShopItemsEvent.subscribe(
        clear => {
          this.paymentForm.patchValue({
            'PayAmount': 0.00
          })
        });

    this.salesDefs = [
      {headerName: 'InvoiceNo', field: 'InvoiceNo', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: 'ArticleText', field: 'ArticleText', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: 'NoOfItems', field: 'NoOfItems', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: 'UnitPrice', field: 'UnitPrice', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: 'Regdate', field: 'Regdate', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const d = params.value.split('T');
          const dDate = d[0].split('-');
          return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
        } else {
          return
        }}},
      {headerName: 'CreditorInvoiceAmount', field: 'CreditorInvoiceAmount', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return;
        }}},
    ]
  }

  translatePaymentmodes(list) {
    const templist = [];
    list.forEach(element => {
      element.Display = 'COMMON.' + element.Code;
      templist.push(element)
    });
    return templist
  }

  tickForSessionKey(tick: number, sessionKey: string) {
    this.exceSessionService.getSessionValue(sessionKey).subscribe(
      result => {
        if (result.Data.length > 0 && tick < 10) {
          this.timerSub.unsubscribe();
          this.sessionKeySubscribe.unsubscribe();
          if (result.Data !== this.branchId.toString()) {
            this.communicationBranch = -1;
            this.isBankTerminalActive = false;
          } else {
            this.communicationBranch = result.Data;
          }
        } else if (tick === 10) {
          this.timerSub.unsubscribe();
          this.sessionKeySubscribe.unsubscribe();
        }
      },
      err => {
        this.timerSub.unsubscribe();
        this.sessionKeySubscribe.unsubscribe();
      });
  }


  reloadItems(params) {
    this.itemResource.query(params).then(items => this.paymentsList = items);
  }

  payByCard() {
    this.paymentType = 'BANKTERMINAL';
    this.isCardPayment = true;
    this.paymentForm.get('GivenAmount').disable();
    this.paymentForm.get('CashBalance').disable();
    this.paymentForm.patchValue({
      'PayAmount': 0,
      'PaymentType': 'BANKTERMINAL'
    });
    this.paymentFormSubmited = false;
  }

  payByCash(givenAmount) {
    this.paymentType = 'CASH';
    this.isCardPayment = false;
    this.paymentForm.get('GivenAmount').enable();
    this.paymentForm.get('CashBalance').enable();
    const totalPaid = this.paymentsList.reduce((a, b) => Number(a) + Number(b.Amount), 0);
    const balance = this.totalPrice - totalPaid;
    this.paymentForm.patchValue({
      'PayAmount': balance,
      'GivenAmount': 0,
      'PaymentType': 'CASH',
      'CashBalance': 0
    });
    this.paymentFormSubmited = false;

    this._ngZone.runOutsideAngular(() => {
      setTimeout(() => {
        givenAmount.first.input.nativeElement.focus();
        givenAmount.first.input.nativeElement.selectionEnd = 0;
      }, 100);
    });
  }

  addToPaymentList() {
    if (this.isShopReturn) {
      const articles = this.shopHomeService.getShopItems();
      if (articles.length !== 1) {
        this.translateService.get('SHOP.OnlyOneArticle').subscribe(deleteMessage => this.message = deleteMessage);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
        return;
      }
    }

    let nextOrderOrInvoiceAdded = false;

    this.paymentsList.forEach(payment => {
      if (payment.PaymentTypeCode === 'NEXTORDER' || payment.PaymentTypeCode === 'INVOICE' ) {
        nextOrderOrInvoiceAdded = true;
      }
    });

    if (((this.paymentType === 'NEXTORDER' || this.paymentType === 'INVOICE' ) && this.paymentsList.length >= 1) ||
    ((this.paymentType !== 'NEXTORDER' || this.paymentType !== 'INVOICE' ) && nextOrderOrInvoiceAdded) ) {
      // this.translateService.get('SHOP.NextOrderInvoiceMessage').subscribe(deleteMessage => this.message = deleteMessage);
      //   this.type = 'DANGER';
      //   this.isShowInfo = true;
      //   setTimeout(() => {
      //     this.isShowInfo = false;
      //   }, 4000);
      //   return;
      this.showMessageEvent = this.shopHomeService.showMessageEvent.next()
      return;
    }



    if (!(Number(this.branchId) === Number(this.communicationBranch)) && this.paymentType === 'BANKTERMINAL') {
      this.exceMessageService.openMessageBox('ERROR',
      {
        messageTitle: 'Exceline',
        messageBody: 'COMMON.CommunicatorBranchMismatch'
      });
      return
    }

    let allowZero = false;
    const itemsShop = this.shopHomeService.getShopItems();
    itemsShop.forEach( item => {
      if (item.Category === 'TRAIL' && itemsShop.length === 1) {
        allowZero = true;
      }
    })

    const payAm = this.paymentForm.get('PayAmount')
    if ((payAm.value === 0 || payAm.value === '0.00') && !allowZero) {
      this.exceMessageService.openMessageBox('ERROR',
          {
            messageTitle: 'Exceline',
            messageBody: 'COMMON.PaymentErrorInputVal'
          });
          return
    }
    this.paymentFormSubmited = true;
    this.paymentTypeObject = this.paymentModes.filter(cat => cat.Code === this.paymentType)[0];

   //  const paymentExist = this.paymentsList.filter(item => item.PaymentTypeCode === this.paymentForm.value.PaymentType);
    // if (paymentExist && paymentExist.length > 0) {
    //   this.paymentForm.get('PayAmount').setAsyncValidators(this.paymentTypeValidator.bind(this));
    //   this.paymentForm.get('PayAmount').updateValueAndValidity();
    // } else {
      if (this.paymentForm.valid || allowZero) {
        if (this.paymentType && (this.paymentType === 'OCR' || this.paymentType === 'DEBTCOLLECTION' || this.paymentType === 'BANKSTATEMENT')) {
          this.selectedDate = this.paymentForm.value.paymentDate;
          // this.payAmount();
        }
        if (this.paymentType === 'ONACCOUNT') {
          if (!(this.shopAccount.Balance >=  this.paymentForm.value.PayAmount)) {
            UsErrorService.validateAllFormFields(this.paymentForm, this.formErrors, this.validationMessages);
            return;
          }
        }
        if (this.paymentType && this.paymentType === 'GIFTCARD') {
          this.validateGiftVoucherNumber(this.paymentForm.value.VoucherNumber, Number(this.paymentForm.value.PayAmount));
        } else {
          if (!this.shopHomeService.$shopLoginData.GymSetting.IsPrintReceiptInShop && (this.paymentType === 'CASH' || this.paymentType === 'BANKTERMINAL')) {
            let messageTitle = '';
            let messageBody = '';
            (this.translateService.get('SHOP.SettingError').subscribe(tranlstedValue => messageTitle = tranlstedValue));
            (this.translateService.get('SHOP.SettingErrorReciptPrint').subscribe(tranlstedValue => messageBody = tranlstedValue));
            this.exceMessageService.openMessageBox('ERROR',
            {
              messageTitle: messageTitle,
              messageBody: messageBody
            });
            return;
          }
          this.payAmount();
        }
      } else {
        UsErrorService.validateAllFormFields(this.paymentForm, this.formErrors, this.validationMessages);
      }
    // }
  }

  private payAmount() {
    const sessionKey = Extension.createGuid();
    if (this.isOnAccountPayment || this.isCreditNote) {
      if (this.paymentType === 'BANKTERMINAL') {
        this.paymentForm.value.TotalPayable = this.paymentForm.value.PayAmount;
        if (this.shopHomeService.$shopLoginData.HardwareProfileList) {
          if (this.shopHomeService.$shopLoginData.GymSetting.IsBankTerminalIntegrated) {
            PreloaderService.showPreLoader();
            // ------ START LOGGING SALE -------//

            this.exceSessionService.AddSession(
              'TERMINAL',
              this.paymentForm.value.PayAmount,
              sessionKey,
              this.exceLoginService.CurrentUser.username);

            this.timer = timer(0, 5000);
            this.timerSub = this.timer.subscribe(t => this.tickerFunc(t, sessionKey, this.paymentForm.value));
            return;
          } else {
            this.handleTerminalPayment(this.paymentForm.value, 0);
          }
        } else {
          this.handleTerminalPayment(this.paymentForm.value, 0);
        }
      } else {
        if (!this.isCreditNote && this.paymentValid()) {
          this.shopHomeService.$paymentAdded = true;
          this.paymentsList.push({
            Id: this.paymentListId,
            Amount: this.paymentForm.value.PayAmount,
            PaidDate: this.selectedDate ? (this.selectedDate.date.year + '-' + this.selectedDate.date.month + '-' + this.selectedDate.date.day) : new Date(),
            PaymentType: this.paymentTypeObject.Name,
            PaymentTypeId: this.paymentTypeObject.TypeId,
            PaymentTypeCode: this.paymentTypeObject.Code,
            IsAbleDelete: true
          });
          this.paymentListId++;
          // }
        }
        if (this.isCreditNote && this.creditNotePaymentValid()) {
          this.shopHomeService.$paymentAdded = true;
          this.paymentsList.push({
            Id: this.paymentListId,
            Amount: this.paymentForm.value.PayAmount,
            PaidDate: this.selectedDate ? (this.selectedDate.date.year + '-' + this.selectedDate.date.month + '-' + this.selectedDate.date.day) : new Date(),
            PaymentType: this.paymentTypeObject.Name,
            PaymentTypeId: this.paymentTypeObject.TypeId,
            PaymentTypeCode: this.paymentTypeObject.Code,
            IsAbleDelete: true
          });
          this.paymentListId++;
        }

        this.setBalance();
      }
    } else {
      if (!this.isCreditNote && this.paymentValid()) {
        if (this.paymentType === 'BANKTERMINAL') {
          if (this.shopHomeService.$shopLoginData.SelectedHardwareProfile &&
            this.shopHomeService.$shopLoginData.SelectedHardwareProfile.HardwareProfileItemList) {
            if (this.shopHomeService.$shopLoginData.GymSetting.IsBankTerminalIntegrated) {
              PreloaderService.showPreLoader();
              // tslint:disable-next-line:no-shadowed-variable
              const sessionKey = Extension.createGuid();
              this.exceSessionService.AddSession(
                'TERMINAL',
                this.paymentForm.value.PayAmount,
                sessionKey,
                this.exceLoginService.CurrentUser.username);

              this.timer = timer(0, 5000);
              this.timerSub = this.timer.subscribe(t => this.tickerFunc(t, sessionKey, this.paymentForm.value));
              return;
            } else {
              this.handleTerminalPayment(this.paymentForm.value, 0);
            }
          } else {
            this.handleTerminalPayment(this.paymentForm.value, 0);
          }
        } else if (this.paymentType === 'CASH') {
          this.shopHomeService.$paymentAdded = true;
          this.paymentsList.push({
            Id: this.paymentListId,
            Amount: this.paymentForm.value.PayAmount,
            PaidDate: new Date(),
            PaymentType: this.paymentTypeObject.Name,
            PaymentTypeId: this.paymentTypeObject.TypeId,
            PaymentTypeCode: this.paymentTypeObject.Code,
            IsAbleDelete: true
          });
        } else if (this.paymentType === 'VIPPS') {
          this.shopHomeService.$paymentAdded = true;
          this.paymentsList.push({
            Id: this.paymentListId,
            Amount: this.paymentForm.value.PayAmount,
            PaidDate: new Date(),
            PaymentType: this.paymentTypeObject.Name,
            PaymentTypeId: this.paymentTypeObject.TypeId,
            PaymentTypeCode: this.paymentTypeObject.Code,
            IsAbleDelete: true
          });
          this.paymentListId++;
          this.setBalance();
        } else {
          if (this.paymentForm.value.PaymentDate) {
            this.shopHomeService.$paymentAdded = true;
            this.paymentsList.push({
              Id: this.paymentListId,
              Amount: this.paymentForm.value.PayAmount,
              PaidDate: this.paymentForm.value.PaymentDate,
              PaymentType: this.paymentTypeObject.Name,
              PaymentTypeId: this.paymentTypeObject.TypeId,
              PaymentTypeCode: this.paymentTypeObject.Code,
              IsAbleDelete: true
            });
            this.paymentListId++;
            this.setBalance();
          } else {
            this.shopHomeService.$paymentAdded = true;
            this.paymentsList.push({
              Id: this.paymentListId,
              Amount: this.paymentForm.value.PayAmount,
              PaidDate: new Date(),
              PaymentType: this.paymentTypeObject.Name,
              PaymentTypeId: this.paymentTypeObject.TypeId,
              PaymentTypeCode: this.paymentTypeObject.Code,
              IsAbleDelete: true
            });
            this.paymentListId++;
            this.setBalance();
          }
        }
      }
    }
    this.paymentFormSubmited = false;
  }

  private tickerFunc(tick: number, sessionKey: string, paymentData: any) {
    this.exceSessionService.getSessionValue(sessionKey).subscribe(
      result => {
        if (result.Data) {
          const resList = result.Data
          const resList2 = resList.split('|')
          const cardType = resList2[1]
          this.cardtype = cardType
          if (result.Data === 'DUPLICATE') {
            PreloaderService.hidePreLoader();
            this.timerSub.unsubscribe();
            this.setTerminalResponse(3, paymentData);

            // this.setTerminalResponse(3, paymentData);
          } else if (result.Data === 'SUCCESS|' + cardType) {
            PreloaderService.hidePreLoader();
            this.timerSub.unsubscribe();
            this.setTerminalResponse(1, paymentData);
            if (this.remaningBalance <= 0) {
              this.savePayment(cardType);
            }
            // auto save
            // this.shopService.refreshArticleListEvent.next(true);
            return;

          } else if (result.Data === 'ERROR') {
            PreloaderService.hidePreLoader();
            this.timerSub.unsubscribe();
            this.setTerminalResponse(0, paymentData);
          }
        } else if (tick === 60) {
          PreloaderService.hidePreLoader();
          this.timerSub.unsubscribe();
          this.setTerminalResponse(2, paymentData);
        }
      },
      err => {
        PreloaderService.hidePreLoader();
        this.timerSub.unsubscribe();
        this.setTerminalResponse(0, paymentData);
      });
  }

  private setTerminalResponse(status: Number, paymentData: any) {
    switch (status) {
      case 0:
        this.exceMessageService.openMessageBox('ERROR',
          {
            messageTitle: 'Exceline',
            messageBody: 'COMMON.PaymentError'
          });
        PreloaderService.hidePreLoader();
        break;
      case 1:
        this.handleTerminalPayment(paymentData, status);
        break;
      case 2:
        this.exceMessageService.openMessageBox('WARNING',
          {
            messageTitle: 'Exceline',
            messageBody: 'COMMON.MsgTerminalDidNotResponse'
          });
        PreloaderService.hidePreLoader();
        break;
      case 3:
        this.exceMessageService.openMessageBox('WARNING',
          {
            messageTitle: 'Exceline',
            messageBody: 'COMMON.MsgTerminalDidNotResponse'
          });
        PreloaderService.hidePreLoader();
        break;

    }

  }

  private handleTerminalPayment(paymentData: any, status: Number) {
    const isDeleteAble = (this.paymentType === 'BANKTERMINAL' && status === 1) ? false : true;
    if ((paymentData.PayAmount - paymentData.TotalPayable) > 0) {
      this.shopHomeService.$paymentAdded = true;
      this.paymentsList.push({
        Id: this.paymentListId,
        Amount: paymentData.TotalPayable,
        PaymentType: this.paymentTypeObject.Name,
        PaymentTypeId: this.paymentTypeObject.TypeId,
        PaymentTypeCode: this.paymentTypeObject.Code,
        IsAbleDelete: isDeleteAble
      });
      this.paymentListId++;

      if (this.inHandBalance > 0) {
        if (this.inHandBalance < (paymentData.PayAmount - paymentData.TotalPayable)) {
          this.exceMessageService.openMessageBox('CONFIRM',
            {
              messageTitle: 'Exceline',
              messageBody: 'COMMON.ProceedwithCustomizeAmount',
              msgBoxId: 'CONFIRM_INHANDBALANCE',
              optionalData: { inHandBalance: this.inHandBalance }
            });
        } else {
          this.shopHomeService.$paymentAdded = true;
          this.paymentsList.push({
            Id: this.paymentListId,
            Amount: paymentData.PayAmount - paymentData.TotalPayable,
            PaymentType: 'WITHDRAW',
            PaymentTypeCode: 'CASHWITHDRAWAL',
            IsAbleDelete: isDeleteAble,
            IsReadOnly: true
          });
          this.paymentListId++;
        }
      } else {
        this.exceMessageService.openMessageBox('WARNING',
          {
            messageTitle: 'WARNING',
            messageBody: 'COMMON.MsgNotEnoughBalance'
          });
      }
    } else {
      this.shopHomeService.$paymentAdded = true;
      this.paymentsList.push({
        Id: this.paymentListId,
        Amount: this.paymentForm.value.PayAmount,
        PaymentType: this.paymentTypeObject.Name,
        PaymentTypeId: this.paymentTypeObject.TypeId,
        PaymentTypeCode: this.paymentTypeObject.Code,
        IsAbleDelete: isDeleteAble
      });
      this.paymentListId++;
    }
    this.setBalance();
  }

  onPaymentMethodChange(paymentCode) {
    this.paymentTypeObject = this.paymentModes.filter(cat => cat.Code === paymentCode)[0];
    this.shopHomeService.$isOnAccountPayment = true;

    this.paymentForm.get('VoucherNumber').disable();
    this.paymentForm.get('PaymentDate').disable();
    this.paymentForm.get('GymName').disable();
    this.paymentForm.get('CashBalance').disable();
    this.paymentForm.get('GivenAmount').disable();

    let displayname = '';
    (this.translateService.get(this.paymentTypeObject.Display).subscribe(tranlstedValue => displayname = tranlstedValue));

    this.paymentType = paymentCode;
    this.paymentTypeDisplay = displayname;
    this.isGiftVoucher = false;
    this.isOCR = false;

    if (paymentCode === 'ONACCOUNT') {
      // this.isOnAccountPayment = true;
    } else if (paymentCode === 'OCR' || paymentCode === 'DEBTCOLLECTION' || paymentCode === 'BANKSTATEMENT') {
      this.isOCR = true;
      this.paymentForm.get('PaymentDate').enable();
      this.paymentForm.get('GymName').enable();
      this.paymentForm.patchValue(
        { 'GymName': this.branchName }
      )
    } else if (paymentCode === 'CASH') {
      this.paymentForm.get('GivenAmount').enable();
      this.paymentForm.get('CashBalance').enable();
      this.isCardPayment = false;
    }
      else if (paymentCode === 'VIPPS') {
        this.paymentForm.get('GivenAmount').enable();
        this.paymentForm.get('CashBalance').enable();
        this.isCardPayment = false;
    }
      else if (paymentCode === 'GIFTCARD') {
      this.isGiftVoucher = true;
      this.paymentForm.get('VoucherNumber').enable();

    } else {
      this.isCardPayment = true;
      this.paymentForm.get('GivenAmount').disable();
      this.paymentForm.get('CashBalance').disable();
    }

  }

  private setGivenAmountPaymentAmount(amount: number) {
    if (this.paymentType === 'CASH') {
      let currentGivenAmount = !Number.isNaN(Number(this.paymentForm.get('GivenAmount').value)) ? Number(this.paymentForm.get('GivenAmount').value) : 0;
      currentGivenAmount += amount;
      this.paymentForm.patchValue({
        'GivenAmount': currentGivenAmount
      });
      this.paymentForm.get('PayAmount').setAsyncValidators(this.payAmountValidator.bind(this));
      this.paymentForm.get('PayAmount').updateValueAndValidity();

      if ((this.paymentForm.value.GivenAmount - this.paymentForm.value.PayAmount) > 0) {
        this.paymentForm.patchValue({
          'CashBalance': this.paymentForm.value.GivenAmount - this.paymentForm.value.PayAmount
        });
      } else {
        this.paymentForm.patchValue({
          'CashBalance': 0
        });
      }
    } else if (this.paymentType === 'BANKTERMINAL') {
      let paymentAmount = !Number.isNaN(Number(this.paymentForm.get('PayAmount').value)) ? Number(this.paymentForm.get('PayAmount').value) : 0;
      paymentAmount += amount;
      this.paymentForm.patchValue({
        'PayAmount': paymentAmount
      });
    }

  }

  private paymentValid(): boolean {
    if (this.shopHomeService.$isReturn && !this.isCreditNote) {
      return true;
    }
    return true;
  }

  private creditNotePaymentValid(): boolean {

    return true;
  }

  validateGiftVoucherNumber(voucherNumber: string, payment: Number) {
    PreloaderService.showPreLoader();
    this.shopService.ValidateGiftVoucherNumber({ voucherNumber, payment }).subscribe(
      res => {
        if (res.Data === -1) {
          this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'COMMON.VoucherNumberMismatch'
            });
        } else if (res.Data === -2) {
          this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'COMMON.GiftVoucherExpired'
            });
        } else if (res.Data === -3) {
          this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'COMMON.VoucherAmountMismatch'
            });
        } else {
          if (this.paymentForm.value.PayAmount <= this.paymentForm.getRawValue().TotalPayable) {
            this.shopHomeService.$paymentAdded = true;
            this.paymentsList.push({
              Id: this.paymentListId,
              Amount: this.paymentForm.value.PayAmount,
              PaymentType: this.paymentTypeObject.Name,
              PaymentTypeId: this.paymentTypeObject.TypeId,
              PaymentTypeCode: this.paymentTypeObject.Code,
              VoucherNo: voucherNumber,
              IsAbleDelete: true
            });
            this.paymentListId++;
            this.paymentForm.patchValue(
              { 'VoucherNumber': '' }
            )
          }
        }
        PreloaderService.hidePreLoader();
      }, error => {
        PreloaderService.hidePreLoader();
      }
    )
  }

  paymentTypeValidator(control: AbstractControl) {
    return new Promise(resolve => {
      resolve({
        'PayementAdded': true
      })
    });
  }

  payAmountValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (Number(control.value) === 0) {
          resolve({
            'PayAmountRequired': true
          })
        } else if (Number.isNaN(Number(control.value))) {
          resolve({
            'InvalidAmount': true
          })
        } else if (this.paymentType === 'PREPAIDBALANCE') {
          const remainBalance = Number(this.shopAccount.PrepaidBalance) - Number(control.value);
          if (remainBalance < 0) {
            resolve({
              'NotEnoughBalance': true
            })
          } else {
            resolve(null);
          }
        } else if (this.paymentType === 'ONACCOUNT') {
          const remainBalance = Number(this.shopAccount.Balance) - Number(control.value);
          if (remainBalance < 0) {
            resolve({
              'NotEnoughBalance': true
            })
          } else {
            if ((remainBalance * -1) > this.negativeAmount) {
              resolve({
                'NotEnoughBalance': true
              })
            } else {
              resolve(null);
            }
            resolve(null);
          }

        } else if (this.paymentType === 'CASH') {
          if ((Number(this.paymentForm.value.GivenAmount) > 0) && (Number(control.value) > this.paymentForm.value.GivenAmount)) {
            resolve({
              'AmountLimitation': true
            })
            // this.formErrors.PayAmount = 'COMMON.MsgPaymentAmountLimitation';
          } else {
            resolve(null);
          }
        } else {
          resolve(null);
        }
      }, 10);
    })
  }

  givenAmountChange(event) {
    const val = event.replace(',', '.')
    this.paymentForm.patchValue({
      'GivenAmount': val
    })
    this.paymentForm.get('PayAmount').setAsyncValidators(this.payAmountValidator.bind(this));
    this.paymentForm.get('PayAmount').updateValueAndValidity();
    if ((this.paymentForm.value.GivenAmount - this.paymentForm.value.PayAmount) > 0) {
      this.paymentForm.patchValue({
        'CashBalance': this.paymentForm.value.GivenAmount - this.paymentForm.value.PayAmount,
      });
    } else {
      this.paymentForm.patchValue({
        'CashBalance': 0,
      });
    }
  }

  removePayment(item) {
    this.paymentsList = this.paymentsList.filter(payment => payment.Id !== item.Id);
    if (this.paymentsList.length === 0) {
      this.shopHomeService.$paymentAdded = false;
      this.paidTotal = 0;
      this.remaningBalance = 0;
    }

  }

  claerGivenAmount() {
    this.paymentForm.patchValue({
      'CashBalance': 0
    });
  }

  ngOnDestroy() {
    if (this.returnModalHandler) {
      this.returnModalHandler.close();
    }
    if (this.yesHandlerSubscription) {
      this.yesHandlerSubscription.unsubscribe();
    }
    if (this.timerSub) {
      this.timerSub.unsubscribe();
    }
    if (this.paymentSubscriber) {
      this.paymentSubscriber.unsubscribe();
    }
    if (this.isBankTerminalActiveSubscribe) {
      this.isBankTerminalActiveSubscribe.unsubscribe();
    }
    if (this.selectedMemberSubscribe) {
      this.selectedMemberSubscribe.unsubscribe();
    }
    if (this.clearShopItemSubscribe) {
      this.clearShopItemSubscribe.unsubscribe();
    }
    if (this.cashdraweractivedsub) {
      this.cashdraweractivedsub.unsubscribe();
    }
    if (this.showMessageEvent) {
      this.showMessageEvent.unsubscribe();
    }
    if (this.selectedShopItemSubscribe) {
      this.selectedShopItemSubscribe.unsubscribe();
    }
    this.shopHomeService.$isCreditNote = false;
    this.shopHomeService.$isOnAccountPayment = false;
    this.shopHomeService.$isBookingPayment = false;
    this.shopHomeService.$giftVoucher = false;
    // this.shopHomeService.$selectedMember = null;
    this.isSelectedMember = false;
    this.shopHomeService.$isSelectedMember = false;
    this.shopHomeService.$paymentAdded = false;
  }

  logOpenCashdrawer() {

    const bodyOCD = {
      SalesPointId: this.shopHomeService.$shopLoginData.SalePoint.ID,
      EntitiyId: this.selectedMember ? this.selectedMember.Id : -2,
      EntityRoleType: (this.shopHomeService.$priceType === 'MEMBER' || this.shopHomeService.$priceType ===  'MEM') ? 'MEM' : 'EMP',
      UserName: this.exceLoginService.CurrentUser.username,
      BranchId: this.selectedMember ? ((this.selectedMember.BranchID > 0) ? this.selectedMember.BranchID : 1) : 1,
    };

    this.shopService.SaveOpenCashRegister(bodyOCD).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {
        }
      }, err => {
        this.exceMessageService.openMessageBox('ERROR',
          {
            messageTitle: 'Exceline',
            messageBody: 'Problem med lagring av open cash drawer.'
          });
      });

  }

  savePayment(cardType) {
    PreloaderService.showPreLoader();
  //  PreloaderService.showPreLoader();

    let saleSuccess = false;
    // Start logging here!
    this.paymentsList.forEach(payMode => {
      this.PaymentType = {
        Type: payMode.PaymentType.toString(),
        TypeCode: payMode.PaymentTypeCode.toString(),
        TypeId: payMode.PaymentTypeId.toString(),
        Amount: payMode.Amount.toString()
      }
      this.PaymentTypes.push(this.PaymentType)
    });


    let i = 1;
    this.shopHomeService.getShopItems().forEach(article => {
      if (article.Category === 'Gavekort' || article.Description === 'Gavekort') {
        this.CartItem = {
          CartItemNo: i,
          ArticleId: article.ArticleNo.toString(),
          ArticleName: article.Description.toString(),
          ArticlePrice: article.SelectedPrice.toString(),
          CurrentArticleStock: '0',
          DiscountPercentage: (article.DiscountPercentage + '%').toString() ,
          DiscountAmount: article.Discount.toString(),
          NumberOfItems: article.OrderQuantity.toString(),
          MVA: (article.VatRate + '%').toString(),
          UnitPriceExcludingMVA: (article.SelectedPrice - this.calculateVAT(article.Price, article.VatRate)).toString(),
          TotalPriceExcludingMVA: (article.OrderQuantity * (article.SelectedPrice - this.calculateVAT(article.Price, article.VatRate))).toString(),
          TotalPriceIncludingMVA: article.Price.toString(),
          DiscountComment: '',
          InstallmentShareId: '',
          ShopSaleItemId: '',
          ARItemNo: '',
          OrderLineId: '',
          ReturnComment: this.isShopReturn ? article.ReturnComment.toString() : ''
         }
      } else {
        this.CartItem = {
          CartItemNo: i,
          ArticleId: article.ArticleNo.toString(),
          ArticleName: article.Description.toString(),
          ArticlePrice: article.SelectedPrice.toString(),
          CurrentArticleStock: article.StockLevel ? article.StockLevel.toString() : '',
          DiscountPercentage: (article.DiscountPercentage + '%').toString() ,
          DiscountAmount: article.Discount.toString(),
          NumberOfItems: article.OrderQuantity.toString(),
          MVA: (article.VatRate + '%').toString(),
          UnitPriceExcludingMVA: (article.SelectedPrice - this.calculateVAT(article.Price, article.VatRate)).toString(),
          TotalPriceExcludingMVA: (article.OrderQuantity * (article.SelectedPrice - this.calculateVAT(article.Price, article.VatRate))).toString(),
          TotalPriceIncludingMVA: article.Price.toString(),
          DiscountComment: article.Comment ? article.Comment.toString() : '',
          InstallmentShareId: '',
          ShopSaleItemId: '',
          ARItemNo: '',
          OrderLineId: '',
          ReturnComment: this.isShopReturn ? article.ReturnComment.toString() : ''
         }
        }
        this.Cart.push(this.CartItem)
        i++;
    });
    this.SaleXMLData = {
      SalePointInfo: {
        BranchId: this.branchId.toString(),
        MachineName: this.exceLoginService.MachineName.toString(),
        SalePointId: this.shopHomeService.$shopLoginData.SalePoint.ID,
        UserName: this.exceLoginService.CurrentUser.username,
        SessionsKey: this.SeshKey.toString(),
        TerminalActive: this.isBankTerminalActive.toString(),
        Date:  moment().format('DD.MM.YYYY') ,
        Time:  moment().format('hh:mm:ss'),
        CreditorNo: ''
      },
      PaymentTypeContent: {
        PaymentTypes: this.PaymentTypes
      },
      ShopCartContent: {
        CartItems: this.Cart
      },
      CustomerInfo: {
        IsDefaultCustomer: this.shopHomeService.$isCashCustomerSelected.toString(),
        CustomerId: this.selectedMember ? this.selectedMember.CustId.toString() : 'Default customer',
        MemberId: this.selectedMember ? this.selectedMember.Id.toString() : '-2',
        MemberBranchId: this.selectedMember ? this.selectedMember.BranchId.toString() : '-1',
        RoleType: this.selectedMember ? (this.selectedMember.RoleId ? this.selectedMember.RoleId.toString() : '-1') : '-1'
      },
      PostSaleInfo: {
        InstallmentId: '',
        ARNo: '',
        ShopSaleId: '',
        PaymentId: ''
      }
    }

   // this.SaleXMLData.ShopCartContent.CartItems.push(this.CartItem);
   // this.CartItem.ArticleId = '123456';
    // this.SaleXMLData.ShopCartContent.CartItems.push(this.CartItem);
   // PreloaderService.showPreLoader();
    this.shopService.AddShopXMLLog(this.SaleXMLData).subscribe(asd => {}
      )
    if (this.paymentsList.length === 0) {
      this.exceMessageService.openMessageBox('WARNING',
        {
          messageTitle: 'Exceline',
          messageBody: 'COMMON.MsgNoPaymentsFound'
        });
    //  PreloaderService.hidePreLoader();
    } else {
      const total = this.paymentsList.reduce((a, b) => Number(a) + Number(b.Amount), 0);
      if (this.isOnAccountPayment || this.isCreditNote) {
        if (this.isBookingPayment && total < this.totalPrice) {
          this.exceMessageService.openMessageBox('ERROR',
            {
              messageTitle: 'Exceline',
              messageBody: 'COMMON.BookingPaymentValidation'
            });
          return;
        }

        // credit not addd
        const paymentModes = [];
        const creditaccountItemList = [];
        if (this.shopHomeService.$shopMode === 'CREDITNOTE') {
          this.paymentsList.forEach(payMode => {
            paymentModes.push({
              Amount: payMode.Amount,
              PaymentTypeId: payMode.PaymentTypeId,
              PaymentType: payMode.PaymentType,
              PaymentTypeCode: payMode.PaymentTypeCode,
              PaidDate: new Date(payMode.PaidDate),
              CreatedUser: this.exceLoginService.CurrentUser.username,
              CreatedDateTime: new Date()
            });
          });

          const bodyCreditNote = {
            AritemNo: this.shopHomeService.$arItemNo,
            ShopAccountid: this.shopAccount.Id,
            MemberId: this.shopHomeService.$selectedMember.Id,
            paymentModes: paymentModes,
            SalePointId: this.shopHomeService.$shopLoginData.SalePoint.ID,
            loggedBranchId: this.branchId
          }

          this.shopService.PayCreditNote(bodyCreditNote).subscribe(
            res => {
       //       PreloaderService.hidePreLoader();
              if (res.Data === 1) {
                const sessionKey = Extension.createGuid();
                let receiptText = '';
                receiptText = receiptText + 'MEMBER$' + this.shopHomeService.$selectedMember.CustId +
                  '|USER$' + this.exceLoginService.CurrentUser.username +
                  '|ITEM$';

                paymentModes.forEach(pMode => {
                  receiptText = receiptText + pMode.PaymentType + ':' + pMode.Amount.toFixed(2) + ';';
                });
                receiptText = receiptText + '|DISCOUNT$0.0||VAT$0.0|';
                this.shopService.AddSessionValue({ sessionKey: sessionKey, value: receiptText, branchId: this.branchId,
                  salepointId: this.shopHomeService.$shopLoginData.SalePoint.ID }).subscribe(
                  session => {
                    this.exceSessionService.AddSession(
                      'CWREC',
                      'START',
                      sessionKey,
                      this.exceLoginService.CurrentUser.username);
                    // this.timer = Observable.timer(0, 2000);
                    // this.timerSub = this.timer.subscribe(t => this.tickerFunc(t, sessionKey, this.paymentForm.value));
                    this.shopHomeService.savePaymentFromOrdersEvent.next(0)
                  }, error => {
                    this.exceMessageService.openMessageBox('ERROR',
                      {
                        messageTitle: 'Exceline',
                        messageBody: 'COMMON.ErrorInAddToSession'
                      });
                  });
              }
              this.closePayment.emit(true);
            }, err => {
              this.exceMessageService.openMessageBox('ERROR',
                {
                  messageTitle: 'Exceline',
                  messageBody: 'COMMON.MsgCreditNotePayError'
                });
            });

        } if (this.shopHomeService.$shopMode === 'BOOKINGCANCELLATION') {
          this.paymentsList.forEach(payMode => {
            paymentModes.push({
              Amount: payMode.Amount,
              PaymentTypeId: payMode.PaymentTypeId,
              PaymentType: payMode.PaymentType,
              PaymentTypeCode: payMode.PaymentTypeCode,
              PaidDate: new Date(payMode.PaidDate),
              CreatedUser: this.exceLoginService.CurrentUser.username,
              CreatedDateTime: new Date()
            });

          });
          const creditNoteDetail = {
            ArticleId: this.shopHomeService.$articleId,
            ArticleText: this.shopHomeService.$articleName
          }

          const details = [];
          details.push(creditNoteDetail);

          const creditNote = {
            AritemNo: this.shopHomeService.$arItemNo,
            Amount: this.shopHomeService.$totalPrice,
            ShopAccoutId: this.shopAccount.Id,
            SalePointId: this.shopHomeService.$shopLoginData.SalePoint.ID,
            MemberId: this.shopHomeService.$selectedMember.Id,
            Note: 'Booking credit note',
            Details: details
          }

          const bodyCreditNote = {
            CreditNote: creditNote,
            paymentModes: paymentModes,
            BranchId: this.branchId,
            IsReturn: true
          }

          this.shopService.AddCreditNoteForBooking(bodyCreditNote).subscribe(
            res => {
          //    PreloaderService.hidePreLoader();
              if (res.Data > 0) {
                this.closePayment.emit(true);
              } else {
                this.closePayment.emit(false);
              }
            }, err => {
              this.exceMessageService.openMessageBox('ERROR',
                {
                  messageTitle: 'Exceline',
                  messageBody: 'COMMON.Errorpaymentcancel'
                });
            });

        } else {
          // if (this.shopAccount.Id) {
          this.paymentsList.forEach(payMode => {
            creditaccountItemList.push({
              PaymentAmount: payMode.Amount,
              PayModeId: payMode.PaymentTypeId,
              Type: 'CRE',
              PayMode: payMode.PaymentTypeCode,
              CreatedUser: this.exceLoginService.CurrentUser.username,
              CreatedDateTime: new Date()
            });
          });

          const bodyMSA = {
            Id: this.shopAccount.Id,
            MemberId: this.shopHomeService.$selectedMember.Id,
            CreatedUser: this.exceLoginService.CurrentUser.username,
            CreatedDateTime: new Date(),
            Balance: total,
            ActiveStatus: true,
            CreditaccountItemList: creditaccountItemList,
            SalePointId: this.shopHomeService.$shopLoginData.SalePoint.ID,
            Mode: 'ADD'
          }

          this.shopService.ManageMemberShopAccount(bodyMSA).subscribe(
            res => {
          //    PreloaderService.hidePreLoader();
              this.closePayment.emit(true);
            }, err => {
           //   PreloaderService.hidePreLoader();
              this.closePayment.emit(true);
            });
          // }
        }
        return;
      }
      const paidList = this.paymentsList.filter(payment => (payment.PaymentTypeCode !== 'INVOICE' || payment.PaymentTypeCode !== 'EMAILSMSINVOICE'));
      const paidAmount = paidList.reduce((a, b) => Number(a) + Number(b.Amount), 0);

      if (this.isBookingPayment && paidAmount < this.totalPrice) {
        this.exceMessageService.openMessageBox('ERROR',
          {
            messageTitle: 'Exceline',
            messageBody: 'COMMON.BookingPaymentValidation'
          });
        return;
      }

      const shopSalesItemList = [];
      this.shopHomeService.getShopItems().forEach(article => {
        const price = (article.DiscountPercentage ?
          (article.OrderQuantity * article.DefaultPrice -
            (article.DiscountPercentage * article.OrderQuantity * article.DefaultPrice / 100))
          : article.OrderQuantity * article.DefaultPrice);
        shopSalesItemList.push(
          {
            Comment: article.Comment,
            Amount: article.SelectedPrice,
            ArticleId: article.Id,
            Discount: article.Discount,
            ItemName: article.Description,
            Quantity: article.OrderQuantity,
            TotalAmount: article.Price,
            IsCompaign: article.IsCampaign,
            ReturnComment: this.isShopReturn ? article.ReturnComment.toString() : '',
            SaleAmount: article.SelectedPrice,
            VatAmount: this.calculateVAT(article.Price, article.VatRate),
            DefaultAmount: article.DefaultPrice,
            VoucherNo: article.VoucherNumber,
            ExpiryDate: article.ExpiryDate ? (article.ExpiryDate.date.year + '-' + article.ExpiryDate.date.month + '-' + article.ExpiryDate.date.day) : null
          }
        )
      });
      const addonList = [];
      this.shopHomeService.getShopItems().forEach(article => {
        const price = (article.DiscountPercentage ?
          (article.OrderQuantity * article.DefaultPrice -
            (article.DiscountPercentage * article.OrderQuantity * article.DefaultPrice / 100))
          : article.OrderQuantity * article.DefaultPrice);
          if (article.CategoryCode === 'TRAIL') { // Used for exit-handling after payment.
            this.isTrailItem = true;
            this.trailArticleId = article.Id;
            this.trailItemName = article.Description;
          }
          if (article.CategoryCode === 'DROPIN') { // Same
            this.isDropinItem = true;
            this.dropinArticleId = article.Id;
            this.dropinItemName = article.Description;
          }
          addonList.push({
          EmployeePrice: article.EmployeePrice,
          CategoryId: article.CategoryId,
          ArticleId: article.Id,
          ItemName: article.Description,
          UnitPrice: article.SelectedPrice,
          Price: price,
          ReturnComment: this.isShopReturn ? article.ReturnComment.toString() : '',
          Quantity: article.OrderQuantity,
          Discount: article.Discount,
          IsActivityArticle: false,
          StockStatus: article.StockStatus,
          CategoryCode: article.CategoryCode,
          InvoiceNo: article.InvoiceNo
        });
      });
      const payModes = [];
      this.paymentsList.forEach(item => {
        payModes.push({
          Id: item.Id,
          PaymentTypeId: item.PaymentTypeId,
          PaymentType: item.PaymentType,
          PaidDate: new Date(),
          PaymentTypeCode: item.PaymentTypeCode,
          Amount: item.Amount,
          VoucherNo: item.VoucherNo
        });
      });

      const installment = {
        Amount: this.totalPrice,
        Balance: this.totalPrice - paidAmount,
        InvoiceGeneratedDate: new Date(),
        InstallmentDate: new Date(),
        MemberId: this.selectedMember ? this.selectedMember.Id : -2,
        InstallmentType: 'M',
        IsATG: false,
        DueDate: new Date(),
        AddOnList: addonList,
        CreatedUser: null
      };

      if (installment.MemberId == null) {
        installment.MemberId = -2;
      }

      const paymentDetails = {
        PaymentSource: 'INSTALLMENTS',
        PayModes: payModes,
        PaidAmount: paidAmount,
        InvoiceAmount: this.totalPrice
      }
      let paymentDetailsINV: any = {};
      // invoice
      if (this.shopHomeService.$shopMode === 'INVOICE') {
        paymentDetailsINV = {
          PaymentSourceId: this.shopHomeService.$selectedInvoice.ArItemNO,
          PaymentSource: 'INVOICE',
          PayModes: payModes,
          PaidAmount: paidAmount,
          InvoiceAmount: this.totalPrice,
          CustId: this.shopHomeService.$selectedInvoice.CustId,
          Creditorno: this.shopHomeService.$selectedInvoice.CreditorNo,
          PaidMemberId: this.selectedMember.Id,
          SalepointId: this.shopHomeService.$shopLoginData.SalePoint.ID,
          Kid: this.shopHomeService.$selectedInvoice.BasicDetails.KID,
          Ref: this.shopHomeService.$selectedInvoice.InvoiceNo
        }
      }

      const shopSaleDetails = {
        SalesPointId: this.shopHomeService.$shopLoginData.SalePoint.ID,
        EntitiyId: this.selectedMember ? this.selectedMember.Id : -2,
        EntityRoleType: this.shopHomeService.$priceType === ('MEMBER' || 'MEM') ? 'MEM' : 'EMP',
        UserName: this.exceLoginService.CurrentUser.username,
        ShopSalesItemList: shopSalesItemList
      };

      const bodyASS = {
        CardType: this.cardtype,
        ShopSaleDetails: shopSaleDetails,
        Installment: installment,
        PaymentDetails: paymentDetails,
        MemberBranchID: this.selectedMember ? ((this.selectedMember.BranchID > 0) ? this.selectedMember.BranchID : this.branchId) : this.branchId,
        PrintInvoice: true,
        CustRoleType: this.shopHomeService.$priceType === ('MEMBER' || 'MEM') ? 'MEM' : 'EMP',
        SessionKey: this.SeshKey
      };



   //   PreloaderService.showPreLoader();
      if (!this.shopHomeService.$isUpdateInstallments) {
        this.shopService.AddShopSales(bodyASS).subscribe(
          res => {
            this.cardtype = null;
         //   PreloaderService.hidePreLoader();
            if (res.Data.SaleStatus === SaleErrors.NOSTOCK) {
              saleSuccess = false;
              this.exceMessageService.openMessageBox('ERROR',
                {
                  messageTitle: 'Exceline',
                  messageBody: 'SHOP.ErrorQuantityInStock'
                });
            } else if (this.shopHomeService.$shopLoginData.GymSetting.IsPrintInvoice) {
              // TODO show report
              saleSuccess = true;
              // this.excePdfViewerService.openPdfViewer('PDF Viewer', res.Data.PrintResult.FileParth);
            } else {
              switch (res.Data.SaleStatus) {
                case SaleErrors.ERROR:
                  saleSuccess = false;
                  this.exceMessageService.openMessageBox('ERROR',
                    {
                      messageTitle: 'Exceline',
                      messageBody: 'SHOP.MsgErrorInAddingSale'
                    });
                  break;

                case SaleErrors.ORDERLINEADDERROR:
                  saleSuccess = false;
                  this.exceMessageService.openMessageBox('ERROR',
                    {
                      messageTitle: 'Exceline',
                      messageBody: 'SHOP.MsgErrorInAddingSale'
                    });
                  break;

                case SaleErrors.INVOICEADDINGERROR:
                  saleSuccess = false;
                  this.exceMessageService.openMessageBox('ERROR',
                    {
                      messageTitle: 'Exceline',
                      messageBody: 'SHOP.MsgErrorInAddingSale'
                    });
                  break;

                case SaleErrors.NOORDERFOUND:
                  saleSuccess = false;
                  let message;
                  this.translateService.get('SHOP.MsgNoNextOrderFound').subscribe(ordermsg => message = ordermsg);
                  this.exceMessageService.openMessageBox('ERROR',
                    {
                      messageTitle: 'Exceline',
                      messageBody: this.selectedMember.CustId + ' ' + this.selectedMember.FirstName + ' ' + this.selectedMember.LastName + ' ' + message,
                    });
                  break;
                default:
                  if (res.Data.SaleStatus === SaleErrors.SUCCESS) {
                    saleSuccess = true;
                    if (this.shopHomeService.$shopLoginData.GymSetting.IsPrintReceiptInShop) {
                      const sessionKey = Extension.createGuid();
                      let receiptText = '';
                      receiptText = receiptText + 'MEMBER$' + res.Data.CustId +
                        '|INVNO$' + res.Data.InvoiceNo +
                        '|USER$' + this.exceLoginService.CurrentUser.username + '|';
                      const r = receiptText;
                      if (this.shopHomeService.getShopItems() != null) {
                        receiptText = receiptText + 'ITEM$';

                        this.shopHomeService.getShopItems().forEach(article => {
                         // let price = Number(article.SelectedPrice);
                         let price = article.Price;

                          receiptText = receiptText +
                            article.Description + ':' +
                            article.OrderQuantity +
                            ':' +
                            article.SelectedPrice.toFixed(2) + ':' +
                            article.DiscountPercentage.toFixed(2) + ':' +
                            article.VatRate.toFixed(2) +
                            ':' +
                            article.Price.toFixed(2) +
                            ':' +
                            article.VoucherNumber + ';';
                        });

                      }

                      if (this.paymentsList != null) {
                        receiptText = receiptText + '|PAYMENT$';
                        this.paymentsList.forEach(payment1 => {
                          let tempPaymentType = '';
                          switch (payment1.PaymentTypeCode) {
                            case 'CASH':
                              tempPaymentType = 'Kontant';
                              this.shopHomeService.openCashDrawerEvent.next(true)
                              break;
                            case 'CARD':
                              tempPaymentType = 'Bankkort';
                              break;
                            case 'BANKTERMINAL':
                              tempPaymentType = 'Bankkort';
                              break;
                            case 'VIPPS':
                              tempPaymentType = 'Vipps';
                              break;
                            case 'CASHWITHDRAWAL':
                              tempPaymentType = 'Kontantuttak';
                              break;
                            case 'EMAILSMSINVOICE':
                              tempPaymentType = 'Epost/SMS-Faktura';
                              break;
                            case 'OCR':
                              tempPaymentType = 'OCR';
                              break;
                            case 'BANKSTATEMENT':
                              tempPaymentType = 'Melding Bank';
                              break;
                            case 'DEBTCOLLECTION':
                              tempPaymentType = 'Innfordring';
                              break;
                            case 'ONACCOUNT':
                              tempPaymentType = 'Akonto';
                              break;
                            case 'NEXTORDER':
                              if (res.Data.DueDate) {
                                const date = res.Data.DueDate.split('T')[0].split('-');
                                const reciptDate = date[2] + '.' + date[1] + '.' + date[0];
                                tempPaymentType = 'Neste Ordre' + '(' + reciptDate + ')';
                              } else {
                                tempPaymentType = 'Neste Ordre()';
                              }
                              break;
                            case 'GIFTCARD':
                              tempPaymentType = 'Gavekort';
                              break;
                            case 'INVOICE':
                              tempPaymentType = 'Faktura';
                              break;
                            case 'SHOPINVOICE':
                              tempPaymentType = 'shop Faktura';
                              break;
                            case 'PREPAIDBALANCE':
                              tempPaymentType = 'Forhåndsbetalt';
                              break;
                          }
                          payment1.Amount = this.convertPaymentAmount(payment1.Amount);
                          receiptText = receiptText + tempPaymentType +
                            ':' + payment1.Amount +
                            ';';
                        });

                      }
                      receiptText = receiptText + '|';
                      this.shopHomeService.clearShopItems()
                      this.paymentsList.forEach(item => {
                        this.removePayment(item)
                     });
                      this.shopService.AddSessionValue({ sessionKey: sessionKey, value: receiptText, branchId: this.branchId,
                         salepointId: this.shopHomeService.$shopLoginData.SalePoint.ID }).subscribe(
                        session => {
                          this.exceSessionService.AddSession(
                            'REC',
                            'START',
                            sessionKey,
                            this.exceLoginService.CurrentUser.username);
                          // this.timer = Observable.timer(0, 2000);
                          // this.timerSub = this.timer.subscribe(t => this.tickerFunc(t, sessionKey, this.paymentForm.value));
                          this.shopHomeService.savePaymentFromOrdersEvent.next(0)
                        }, error => {
                          this.exceMessageService.openMessageBox('ERROR',
                            {
                              messageTitle: 'Exceline',
                              messageBody: 'COMMON.ErrorInAddToSession'
                            });

                            this.setBalance()
                        });
                    }
                  }
                  this.shopHomeService.clearShopItems()
                  // this.logOpenCashdrawer();
                  break;
              }
              this.shopHomeService.ShopItems = [];
              this.closePayment.emit({ saleStatus: saleSuccess, articlesPaid: addonList, arItemNo: res.Data.AritemNo, totalPrice: this.totalPrice, payModes: payModes });
              if (saleSuccess) {
                this.shopHomeService.$selectedMember = null;
              }
            }
          }, err => {
            PreloaderService.hidePreLoader();
          }, () => PreloaderService.hidePreLoader());
      } else {
        if (this.shopHomeService.$shopMode === 'INVOICE') {
          const regInvoicePayment = {
            MemberBranchID: this.selectedMember ? this.selectedMember.BranchID : this.branchId,
            PaymentDetail: paymentDetailsINV,
            SalePointID: this.shopHomeService.$shopLoginData.SalePoint.ID,
            SalesDetails: shopSaleDetails
          }
          this.shopService.AddInvoicePayment(regInvoicePayment).subscribe(
            res => {
              if (res.Data.SaleStatus === SaleErrors.SUCCESS) {
                saleSuccess = true;
                if (this.shopHomeService.$shopLoginData.GymSetting.IsPrintReceiptInShop) {
                  const sessionKey = Extension.createGuid();
                  let receiptText = '';
                  receiptText = receiptText + 'MEMBER$' + ((res.Data.CustId === '') ? this.selectedMember.CustId : res.Data.CustId) +
                    '|INVNO$' + res.Data.InvoiceNo +
                    '|USER$' + this.exceLoginService.CurrentUser.username + '|';

                  if (this.shopHomeService.getShopItems() != null) {
                    receiptText = receiptText + 'ITEM$';

                    this.shopHomeService.getShopItems().forEach(article => {
                      receiptText = receiptText +
                        article.Description + ':' +
                        article.OrderQuantity +
                        ':' +
                        article.SelectedPrice.toFixed(2) + ':' +
                        article.DiscountPercentage
                          .toFixed(2) + ':' +
                        article.VatRate.toFixed(2) +
                        ':' +
                        article.Price.toFixed(2) +
                        ':' +
                        '' + ';';
                    });

                  }

                  if (this.paymentsList != null) {
                    receiptText = receiptText + '|PAYMENT$';
                    this.paymentsList.forEach(payment1 => {
                      let tempPaymentType = '';
                      switch (payment1.PaymentTypeCode) {
                        case 'CASH':
                          tempPaymentType = 'Kontant';
                          break;
                        case 'CARD':
                          tempPaymentType = 'Bankkort';
                          break;
                        case 'BANKTERMINAL':
                          tempPaymentType = 'Bankkort';
                          break;
                        case 'VIPPS':
                          tempPaymentType = 'Vipps';
                          break;
                        case 'CASHWITHDRAWAL':
                          tempPaymentType = 'Kontantuttak';
                          break;
                        case 'EMAILSMSINVOICE':
                          tempPaymentType = 'Epost/SMS-Faktura';
                          break;
                        case 'OCR':
                          tempPaymentType = 'OCR';
                          break;
                        case 'BANKSTATEMENT':
                          tempPaymentType = 'Melding Bank';
                          break;
                        case 'DEBTCOLLECTION':
                          tempPaymentType = 'Innfordring';
                          break;
                        case 'ONACCOUNT':
                          tempPaymentType = 'Akonto';
                          break;
                        case 'NEXTORDER':
                          if (res.Data.DueDate) {
                            const date = res.Data.DueDate.split('T')[0].split('-');
                            const reciptDate = date[2] + '.' + date[1] + '.' + date[0];
                            tempPaymentType = 'Neste Ordre' + '(' + reciptDate + ')';
                          } else {
                            tempPaymentType = 'Neste Ordre()';
                          }
                          break;
                        case 'GIFTCARD':
                          tempPaymentType = 'Gavekort';
                          break;
                        case 'INVOICE':
                          tempPaymentType = 'Faktura';
                          break;
                        case 'SHOPINVOICE':
                          tempPaymentType = 'shop Faktura';
                          break;
                        case 'PREPAIDBALANCE':
                          tempPaymentType = 'Forhåndsbetalt';
                          break;
                      }
                      payment1.Amount = this.convertPaymentAmount(payment1.Amount);
                      receiptText = receiptText + tempPaymentType +
                        ':' + payment1.Amount +
                        ';';
                    });

                  }

                  receiptText = receiptText + '|';
                  this.shopHomeService.clearShopItems()
                  this.paymentsList.forEach(item => {
                    this.removePayment(item)
                 });
                  this.shopService.AddSessionValue({ sessionKey: sessionKey, value: receiptText, branchId: this.branchId,
                    salepointId: this.shopHomeService.$shopLoginData.SalePoint.ID }).subscribe(
                    session => {
                      this.exceSessionService.AddSession(
                        'REC',
                        'START',
                        sessionKey,
                        this.exceLoginService.CurrentUser.username);
                      // this.timer = Observable.timer(0, 2000);
                      // this.timerSub = this.timer.subscribe(t => this.tickerFunc(t, sessionKey, this.paymentForm.value));
                      this.shopHomeService.savePaymentFromOrdersEvent.next(0)
                    }, error => {
                      this.exceMessageService.openMessageBox('ERROR',
                        {
                          messageTitle: 'Exceline',
                          messageBody: 'COMMON.ErrorInAddToSession'
                        });
                    });
                }
              }

              this.shopHomeService.ShopItems = [];
              this.closePayment.emit({ saleStatus: saleSuccess, articlesPaid: addonList, arItemNo: res.Data.AritemNo });
              this.shopHomeService.$selectedMember = null;
            //  PreloaderService.hidePreLoader();
            },
            error => {
             // PreloaderService.hidePreLoader();
            });

        } else {
          this.shopHomeService.$selectedOder.AddOnList = addonList;
          this.shopHomeService.$selectedOder.DueDate = new Date(this.shopHomeService.$selectedOder.DueDate);
          this.shopHomeService.$selectedOder.EstimatedInvoiceDate = new Date(this.shopHomeService.$selectedOder.EstimatedInvoiceDate);
          this.shopHomeService.$selectedOder.InstallmentDate = new Date(this.shopHomeService.$selectedOder.InstallmentDate);
          this.shopHomeService.$selectedOder.OriginalDueDate = new Date(this.shopHomeService.$selectedOder.OriginalDueDate);
          this.shopHomeService.$selectedOder.TrainingPeriodEnd = new Date(this.shopHomeService.$selectedOder.TrainingPeriodEnd);
          this.shopHomeService.$selectedOder.TrainingPeriodStart = new Date(this.shopHomeService.$selectedOder.TrainingPeriodStart);
          this.shopHomeService.$selectedOder.InvoiceGeneratedDate = new Date();
          this.shopHomeService.$selectedOder.PaidDate = new Date();
          this.shopHomeService.$selectedOder.IsOrderCheckOutvisible = true;
          this.shopHomeService.$selectedOder.IsOrderSMSEmailvisible = true;
          this.shopHomeService.$selectedOder.IsOrderInvoicevisible = true;

          this.shopService.UpdateMemberInstallment(this.shopHomeService.$selectedOder).subscribe(
            res => {
              if (res.Data) {
                const regInstPayment = {
                  MemberBranchID: this.selectedMember ? this.selectedMember.BranchID : this.branchId,
                  Installment: this.shopHomeService.$selectedOder,
                  PaymentDetail: paymentDetails,
                  SalePointID: this.shopHomeService.$shopLoginData.SalePoint.ID,
                  SalesDetails: shopSaleDetails
                }
                this.shopService.RegisterInstallmentPayment(regInstPayment).subscribe(
                  responce => {
                    if (responce.Data.SaleStatus === SaleErrors.SUCCESS) {
                      saleSuccess = true;
                      if (this.shopHomeService.$shopLoginData.GymSetting.IsPrintReceiptInShop) {
                        const sessionKey = Extension.createGuid();
                        let receiptText = '';
                        receiptText = receiptText + 'MEMBER$' + responce.Data.CustId +
                          '|INVNO$' + responce.Data.InvoiceNo +
                          '|USER$' + this.exceLoginService.CurrentUser.username + '|';

                        if (this.shopHomeService.getShopItems() != null) {
                          receiptText = receiptText + 'ITEM$';

                          this.shopHomeService.getShopItems().forEach(article => {
                            receiptText = receiptText +
                              article.Description + ':' +
                              article.OrderQuantity + ':' +
                              article.SelectedPrice.toFixed(2) + ':' +
                              ((article.DiscountPercentage) ? article.DiscountPercentage.toFixed(2) : '0') + ':' +
                              ((article.VatRate) ? article.vatRate.toFixed(2) : '0') + ':' +
                              article.Price.toFixed(2) +
                              ':' +
                              '' + ';';
                          });
                        }

                        if (this.paymentsList != null) {
                          receiptText = receiptText + '|PAYMENT$';
                          this.paymentsList.forEach(payment1 => {
                            let tempPaymentType = '';
                            switch (payment1.PaymentTypeCode) {
                              case 'CASH':
                                tempPaymentType = 'Kontant';
                                break;
                              case 'CARD':
                                tempPaymentType = 'Bankkort';
                                break;
                              case 'BANKTERMINAL':
                                tempPaymentType = 'Bankkort';
                                break;
                              case 'VIPPS':
                                tempPaymentType = 'Vipps';
                                break;
                              case 'CASHWITHDRAWAL':
                                tempPaymentType = 'Kontantuttak';
                                break;
                              case 'EMAILSMSINVOICE':
                                tempPaymentType = 'Epost/SMS-Faktura';
                                break;
                              case 'OCR':
                                tempPaymentType = 'OCR';
                                break;
                              case 'BANKSTATEMENT':
                                tempPaymentType = 'Melding Bank';
                                break;
                              case 'DEBTCOLLECTION':
                                tempPaymentType = 'Innfordring';
                                break;
                              case 'ONACCOUNT':
                                tempPaymentType = 'Akonto';
                                break;
                              case 'NEXTORDER':
                                if (res.Data.DueDate) {
                                  const date = res.Data.DueDate.split('T')[0].split('-');
                                  const reciptDate = date[2] + '.' + date[1] + '.' + date[0];
                                  tempPaymentType = 'Neste Ordre' + '(' + reciptDate + ')';
                                } else {
                                  tempPaymentType = 'Neste Ordre()';
                                }
                                break;
                              case 'GIFTCARD':
                                tempPaymentType = 'Gavekort';
                                break;
                              case 'INVOICE':
                                tempPaymentType = 'Faktura';
                                break;
                              case 'SHOPINVOICE':
                                tempPaymentType = 'shop Faktura';
                                break;
                              case 'PREPAIDBALANCE':
                                tempPaymentType = 'Forhåndsbetalt';
                                break;
                            }
                            if (typeof payment1.Amount === 'number') {
                              payment1.Amount = payment1.Amount.toFixed(2)
                            }
                            payment1.Amount = this.convertPaymentAmount(payment1.Amount);
                            receiptText = receiptText + tempPaymentType +
                              ':' + payment1.Amount +
                              ';';
                          });
                        }
                        receiptText = receiptText + '|';
                        this.shopHomeService.clearShopItems()
                        this.paymentsList.forEach(item => {
                          this.removePayment(item)
                       });
                        this.shopService.AddSessionValue({ sessionKey: sessionKey, value: receiptText, branchId: this.branchId,
                          salepointId: this.shopHomeService.$shopLoginData.SalePoint.ID }).subscribe(
                          session => {
                            this.exceSessionService.AddSession(
                              'REC',
                              'START',
                              sessionKey,
                              this.exceLoginService.CurrentUser.username);
                            this.timer = timer(0, 2000);
                            // this.timerSub = this.timer.subscribe(t => this.tickerFunc(t, sessionKey, this.paymentForm.value));
                            this.shopHomeService.savePaymentFromOrdersEvent.next(0)
                          }, error => {
                            this.exceMessageService.openMessageBox('ERROR',
                              {
                                messageTitle: 'Exceline',
                                messageBody: 'COMMON.ErrorInAddToSession'
                              });
                          });
                      }
                    }
                    this.shopHomeService.ShopItems = [];
                    this.closePayment.emit({ saleStatus: saleSuccess, articlesPaid: addonList, arItemNo: res.Data.AritemNo });
                    this.shopHomeService.$selectedMember = null;
                    this.shopHomeService.$isSelectedMember = false;
                    this.isSelectedMember = false;
                //    PreloaderService.hidePreLoader();

                  },
                  error => {
                //    PreloaderService.hidePreLoader();
                  });
              }
            }, err => {
              PreloaderService.hidePreLoader();
            }, () => PreloaderService.hidePreLoader()
          )
        }
      }
    }
    if (this.paymentForm.get('TotalPayable').value === 0) {
      this.shopHomeService.$isSelectedMember = false;
      this.shopHomeService.$paymentAdded = false;
      // this.shopHomeService.savePaymentFromOrdersEvent.next(0)
   // this.shopService.refreshArticleListEvent.next(true);
    }
    if (this.shopHomeService.$shopOpenedFromVisitRegistration.StatusCode === 1) {
      // ---------- return 2 if article is of type 'TRIAL' signaling confirmation for registration
      // ---------- return 3 if article is of type 'DROPIN' signaling confirmation for registration
      // ---------- return 4 if none of the articles are of the type 'TRIAL' or 'DROPIN'
      if (this.isTrailItem === true && this.isDropinItem === true) {
        // ---- both 'TRIAL' and 'DROPIN' items are present, save both.
        let shopTrialData = {
          StatusCode: 2,
          ArticleId: this.trailArticleId,
          ItemName: this.trailItemName
        }
        this.isTrailItem = false;
        this.shopHomeService.$shopOpenedFromVisitRegistration = shopTrialData;

        shopTrialData = {
          StatusCode: 3,
          ArticleId: this.dropinArticleId,
          ItemName: this.dropinItemName
        }
        this.isDropinItem = false;
        this.shopHomeService.$shopOpenedFromVisitRegistration = shopTrialData;
      } else if (this.isTrailItem === true && this.isDropinItem === false) {
      const shopTrialData = {
        StatusCode: 2,
        ArticleId: this.trailArticleId,
        ItemName: this.trailItemName
      }
      this.isTrailItem = false;
      this.shopHomeService.$shopOpenedFromVisitRegistration = shopTrialData;
      }else if (this.isTrailItem === false && this.isDropinItem === true) {
       const shopTrialData = {
          StatusCode: 3,
          ArticleId: this.dropinArticleId,
          ItemName: this.dropinItemName
        }
        this.isDropinItem = false;
        this.shopHomeService.$shopOpenedFromVisitRegistration = shopTrialData;
      }else if(this.isTrailItem === false && this.isDropinItem === false) {
        const shopTrialData = {
          StatusCode: 4,
          ArticleId: -1,
          ItemName: ''
        }
        this.shopHomeService.$shopOpenedFromVisitRegistration = shopTrialData;
      }
    }
  }

  private calculateVAT(amount: number, vatRate: number): number {
    return amount * vatRate / (vatRate + 100);
  }

  convertPaymentAmount(value) {
    if (value.toString().includes('.')) {
      // do nothing
    } else {
      value = Number(value).toFixed(2)
    }
    return value;
  }

  private setBalance() {
    const totalPaid = this.paymentsList.reduce((a, b) => Number(a) + Number(b.Amount), 0);

    const paidList = this.paymentsList.filter(payment => !payment.IsReadOnly);
    this.paidTotal = paidList.reduce((a, b) => Number(a) + Number(b.Amount), 0);

    const balance = this.totalPrice - totalPaid;

    let stringBalance = balance.toString()
    if (!stringBalance.includes('.') && stringBalance !== 'NaN') {
      stringBalance = stringBalance + '.00';
    }
    this.remaningBalance = balance;
    if (balance > 0) {
      this.paymentForm.patchValue({
        'PayAmount': stringBalance,
        'CashBalance': 0,
        'GivenAmount': 0
      });
    } else {
     //  this.paymentModes = this.paymentModes.filter(cat => cat.Code === 'CASH' || cat.Code === 'BANKTERMINAL' || cat.Code === 'VIPPS')
      this.paymentForm.patchValue({
        'TotalPayable': 0,
        'PayAmount': 0,
        'CashBalance': 0,
        'GivenAmount': 0,
      });
      this.paidTotal = 0;
      this.paymentAmount.clearAmount();
      this.paymentAmount.writeValue(0);
      this.remaningBalance = 0;
      // this.cardtype = null;

    }
  }

  public onChange(event) {
    this.paymentForm.get('PayAmount').setAsyncValidators(this.payAmountValidator.bind(this));
    this.paymentForm.get('PayAmount').updateValueAndValidity();
    const val = event.replace(',', '.')
    this.paymentForm.patchValue({
      'PayAmount': val
    })
  }

  closeReturnModal() {
    this.returnModalHandler.close();
  }

  shopReturnSave() {
    let selectedrows = [];
    selectedrows = this.gridApi.getSelectedRows();
    if (selectedrows.length !== 1 && this.returnComment.length === 0) {
      this.translateService.get('SHOP.MissingArticleOrComment').subscribe(deleteMessage => this.message = deleteMessage);
      this.type = 'DANGER';
      this.isShowInfo2 = true;
      setTimeout(() => {
        this.isShowInfo2 = false;
      }, 3000);
      return;
    }
    this.returnInvoiceNo = selectedrows[0].InvoiceNo;
    this.shopHomeService.getShopItems().forEach(article => {
      article.ReturnComment = this.returnComment;
      article.InvoiceNo = selectedrows[0].InvoiceNo;
    })

    if (selectedrows.length > 0 && this.returnComment.length > 0) {
      this.savePayment(this.cardtype ? this.cardtype : '');
    } else {
    }
    this.returnComment = '';
    this.closeReturnModal();
  }

  commentChange(event) {
    this.returnComment = event.target.value;
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // auto fits columns to fit screen
    this.gridApi.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();

    let articleReturn;

    // get sales with article(s)
    const memberID = this.selectedMember ? this.selectedMember.Id : -2;

    // boolean for procedure is using shopinvoices only or not
    const shopOnly = 1;

    this.shopHomeService.getShopItems().forEach(article => {
      articleReturn = article;
    });
    this.shopService.GetInvoicesForArticleReturn(memberID, this.branchId, articleReturn.ArticleNo, shopOnly).subscribe(res => {
      this.returnSales = res.Data;
    })
    this.returnSales = []
  }
}
