import { TestBed, inject } from '@angular/core/testing';

import { ShopHomeService } from './shop-home.service';

describe('ShopHomeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShopHomeService]
    });
  });

  it('should be created', inject([ShopHomeService], (service: ShopHomeService) => {
    expect(service).toBeTruthy();
  }));
});
