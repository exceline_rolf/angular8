import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { Guid } from '../../../../shared/services/guid.service';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { UsErrorService } from '../../../../shared/directives/us-error/us-error.service';
import { ExceShopService } from '../../services/exce-shop.service';
import { ColumnDataType } from '../../../../shared/enums/us-enum';
import { ExceMemberService } from '../../../membership/services/exce-member.service';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { EntitySelectionType } from 'app/shared/enums/us-enum';
import { ShopHomeService } from 'app/modules/shop/shop-home/shop-home.service';
import { ExceSessionService } from 'app/shared/services/exce-session.service';
import { Observable, timer } from 'rxjs';

@Component({
  selector: 'cash-withdrawal',
  templateUrl: './cash-withdrawal.component.html',
  styleUrls: ['./cash-withdrawal.component.scss']
})
export class CashWithdrawalComponent implements OnInit, OnDestroy {
  translateGerSubscription: any;
  isWithdrawal = true;
  withdrawalType = 'CASHWITHDRAWAL';
  selectedWithdrawalType: string;
  paymentType: string;
  entitySelectionModel: any;
  entitySearchType: string;

  public withdrawalForm: FormGroup;
  public pettyCashForm: FormGroup;
  addMemberView: any;

  entityItems = [];
  itemCount: number;

  isBranchVisible = true;
  isStatusVisible = true;
  isRoleVisible = true;
  isAddBtnVisible = true;
  isBasicInforVisible = true;

  formSubmited = false;
  inHandBalance: Number;
  prePaidBalance: Number;
  onAccountBalance?: Number;
  dailySettlement?: any;
  selectedMember: any;
  memberSelected:boolean = false;

  // memberId: any;
  // custBranchId: any;
  memberNo: any;
  employee: any;

  @Output() closeWithdrowalModel = new EventEmitter();

  columns = [{ property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'TelMobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
  ];

  auotocompleteItems = [{ id: 'NAME', name: 'NAME :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }]

  withdrawalFromList = [
    { value: 'CARD', label: 'COMMON.Card' }
  ];

  timer: any;
  timerSub: any;

  withdrawalFormError = {
    MemberNo: '',
    WithdrawalFrom: '',
    PaymentAmount: ''
  };

  withdrawalValidationMessages = {
    MemberNo: {
      required: 'COMMON.MemberRequired'
    },
    WithdrawalFrom: {
      required: 'COMMON.WithdrawalFromTypeIsRequired'
    },
    PaymentAmount: {
      required: 'COMMON.ValidWithdrawalAmountRequired',
      validInput: 'COMMON.ValidWithdrawalAmountRequired',
      notEnoughCash: 'COMMON.NotEnoughCash',
      notEnoughAmountOnAccount: 'COMMON.NotEnoughAmountOnAccount',
      notEnoughinPrepiad: 'COMMON.NotEnoughinPrepiad'
    }
  }

  pettyCashFormError = {
    EmployeeName: '',
    PaymentAmount: ''
  };

  pettyCashValidationMessages = {
    EmployeeName: {
      required: 'COMMON.EmployeeRequired'
    },
    PaymentAmount: {
      required: 'COMMON.PettyCashAmountRequired',
      validInput: 'COMMON.PettyCashAmountRequired',
      notEnoughCash: 'COMMON.NotEnoughCash'
    },
  }

  constructor(
    private modalService: UsbModal,
    private fb: FormBuilder,
    private memberService: ExceMemberService,
    private shopService: ExceShopService,
    private shopHomeService: ShopHomeService,
    private exceMessageService: ExceMessageService,
    private exceLoginService: ExceLoginService,
    private exceSessionService: ExceSessionService

  ) {
    this.entitySearchType = 'MEM';
  }

  ngOnInit() {
    this.selectedMember = this.shopHomeService.$selectedMember;
    this.withdrawalForm = this.fb.group({
      MemberNo: [null, Validators.required],
      MemberName: [{ value: null, disabled: true }],
      WithdrawalFrom: [null, Validators.required],
      Balance: [{ value: 0, disabled: true }],
      PaymentAmount: [null, [Validators.required], [this.checkPaymentAmount.bind(this)]],
      PaymentType: ['CASH']
    });

    if(this.selectedMember){
      this.selecteMember(this.selectedMember);
      this.memberSelected = true;
    }

    this.pettyCashForm = this.fb.group({
      EmployeeName: [null, [Validators.required]],
      PaymentAmount: [null, [Validators.required], [this.checkPaymentAmount.bind(this)]],
      Comment: [null]
    });

    this.withdrawalForm.statusChanges.subscribe(_ =>
      UsErrorService.onValueChanged(this.withdrawalForm, this.withdrawalFormError, this.withdrawalValidationMessages));
    this.pettyCashForm.statusChanges.subscribe(_ =>
      UsErrorService.onValueChanged(this.pettyCashForm, this.pettyCashFormError, this.pettyCashValidationMessages));

    this.shopService.GetDailySettlements({ salePointId: this.shopHomeService.$shopLoginData.SalePoint.ID }).subscribe(
      res => {
        this.dailySettlement = res.Data;
        this.inHandBalance = (res.Data.CashIn - res.Data.CashOut);
        if (this.inHandBalance < 0) {
          this.inHandBalance = 0;
        }
      });
  }

  ngOnDestroy() {
    if (this.entitySelectionModel)  {
      this.entitySelectionModel.close();
    }
    if (this.addMemberView) {
      this.addMemberView.close();
    }
  }

  withdrowalTypeChange(isWithdrawal) {
    this.isWithdrawal = isWithdrawal;
    this.formSubmited = false;
    if (isWithdrawal) {
      this.withdrawalType = 'CASHWITHDRAWAL';
      this.entitySearchType = 'MEM';
      this.isBranchVisible = true;
      this.isStatusVisible = true;
      this.isRoleVisible = true;
      this.isAddBtnVisible = true;
      this.isBasicInforVisible = true;
      this.columns = [
        { property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'TelMobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
      ];
      this.checkPaymentAmount(this.withdrawalForm.get('PaymentAmount'));
    } else {
      this.withdrawalType = 'PETTYCASH';
      this.entitySearchType = 'EMP';
      this.isBranchVisible = false;
      this.isStatusVisible = false;
      this.isRoleVisible = false;
      this.isAddBtnVisible = false;
      this.isBasicInforVisible = false;
      this.columns = [
        { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
        { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      ];
      this.checkPaymentAmount(this.pettyCashForm.get('PaymentAmount'));
    }
  }

  onWithdrawalFromChange(value) {
    this.selectedWithdrawalType = value;
    this.withdrawalForm.get('PaymentType').enable();
    if (value === 'CARD') {
      this.withdrawalForm.patchValue({
        PaymentType: 'CASH'
      });
      this.withdrawalForm.get('PaymentType').disable();
    } else if (value === 'ONACCOUNT') {
      this.withdrawalForm.patchValue({
        Balance: this.onAccountBalance
      });
    } else if (value === 'PREPAID') {
      this.withdrawalForm.patchValue({
        Balance: this.prePaidBalance
      });
    }

    if ((this.withdrawalForm.get('WithdrawalFrom').value === 'CASH') &&
      (Number(this.withdrawalForm.get('PaymentAmount').value) > Number(this.inHandBalance))) {
      this.withdrawalForm.get('PaymentAmount').setErrors({
        'notEnoughCash': true
      });
    } else {
      if ((this.withdrawalForm.get('WithdrawalFrom').value === 'ONACCOUNT') &&
        Number(this.onAccountBalance) >= 0 && (Number(this.withdrawalForm.get('PaymentAmount').value) > Number(this.onAccountBalance))) {
        this.withdrawalForm.get('PaymentAmount').setErrors({
          'notEnoughAmountOnAccount': true
        });
      } else if ((this.withdrawalForm.get('WithdrawalFrom').value === 'PREPAID') &&
        Number(this.prePaidBalance) >= 0 && (Number(this.withdrawalForm.get('PaymentAmount').value) > Number(this.prePaidBalance))) {
        this.withdrawalForm.get('PaymentAmount').setErrors({
          'notEnoughinPrepiad': true
        });
      }
    }
  }

  saveWithdrawal() {
    this.formSubmited = true;
    if (this.isWithdrawal) {
      if (this.withdrawalForm.valid) {

        const body = {
          MemberId: this.selectedMember.Id,
          WithdrawalFrom: this.withdrawalForm.value.WithdrawalFrom,
          SalePointId: this.shopHomeService.$shopLoginData.SalePoint.ID,
          PaymentAmount: this.withdrawalForm.value.PaymentAmount,
          WithdrawalType: 'CASHWITHDRAWAL',
          CusId: this.memberNo,
          PaymentType: this.withdrawalForm.value.PaymentType
        }

        if (this.withdrawalForm.value.WithdrawalFrom && this.withdrawalForm.value.WithdrawalFrom === 'CARD') {
          if (this.shopHomeService.$shopLoginData.SelectedHardwareProfile.HardwareProfileItemList) {
            if (this.shopHomeService.$shopLoginData.GymSetting && this.shopHomeService.$shopLoginData.GymSetting.IsBankTerminalIntegrated) {
              PreloaderService.showPreLoader();
              const sessionKey = Guid.newGuid();
              this.exceSessionService.AddSession(
                'TERMINAL',
                this.withdrawalForm.value.PaymentAmount,
                sessionKey,
                this.exceLoginService.CurrentUser.username);

              this.timer = timer(0, 5000);
              this.timerSub = this.timer.subscribe(t => this.tickerFunc(t, sessionKey, body));
              return;
            }
          }
        }
        this.saveWithdrawalData(body);
      } else {
        UsErrorService.validateAllFormFields(this.withdrawalForm, this.withdrawalFormError, this.withdrawalValidationMessages)
      }
    } else {
      if (this.pettyCashForm.valid) {
        const body = {
          GymEmployee: this.employee,
          WithdrawalType: 'PETTYCASH',
          PaymentAmount: this.pettyCashForm.value.PaymentAmount,
          Comment: this.pettyCashForm.value.Comment,
          EmployeeId: this.employee.Id,
          SalePointId: this.shopHomeService.$shopLoginData.SalePoint.ID
        }
        this.saveWithdrawalData(body);
      } else {
        UsErrorService.validateAllFormFields(this.pettyCashForm, this.pettyCashFormError, this.pettyCashValidationMessages)
      }
    }
  }

  cancelWithdrawal() {
    this.closeWithdrowalModel.emit();
  }

  searchDeatail(val: any) {
    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.MEM]:
        PreloaderService.showPreLoader();
        this.memberService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
          'SHOP', val.roleSelected.id, val.hit, false,
          false, ''
        ).subscribe
          (result => {
            this.entityItems = result.Data;
            this.itemCount = result.Data.length;
            PreloaderService.hidePreLoader();
          }, err => {
            PreloaderService.hidePreLoader();
          })
        break;
      case EntitySelectionType[EntitySelectionType.EMP]:
        PreloaderService.showPreLoader();
        this.shopService.GetGymEmployees({ searchText: val.searchVal, isActive: true })
          .subscribe
          (result => {
            this.entityItems = result.Data;
            this.itemCount = result.Data.length;
            PreloaderService.hidePreLoader();
          }, err => {
            PreloaderService.hidePreLoader();
          });
        break;
    }

  }

  openEntitySelectionModel(content) {
    this.entitySelectionModel = this.modalService.open(content);
  }

  selecteMember(item) {
    this.selectedMember = item;
    if (this.isWithdrawal) {
      if (this.selectedMember.CustId && this.selectedMember.CustId > 0) {
        this.withdrawalFromList = [
          { value: 'CARD', label: 'COMMON.Card' },
          { value: 'ONACCOUNT', label: 'COMMON.OnAccount' },
          { value: 'PREPAID', label: 'COMMON.PrepaidBalance' }
        ];
      }

     // this.custBranchId = this.selectedMember.BranchId;
      this.memberNo = this.selectedMember.CustId;
      this.withdrawalForm.patchValue({
        MemberNo: this.selectedMember.CustId,
        MemberName: this.selectedMember.Name
      });

      this.shopService.GetMemberEconomyBalances({ memberId: this.selectedMember.Id }).subscribe(
        economy => {
          this.prePaidBalance = economy.Data.PREPAID;
          this.onAccountBalance = economy.Data.ONACCOUNT;
          if (this.withdrawalForm.get('WithdrawalFrom').value === 'ONACCOUNT') {
            this.withdrawalForm.patchValue({
              Balance: this.onAccountBalance
            });
          } else if (this.withdrawalForm.get('WithdrawalFrom').value === 'PREPAID') {
            this.withdrawalForm.patchValue({
              Balance: this.prePaidBalance
            });
          }
        });
    } else {
      this.employee = this.selectedMember;
      this.pettyCashForm.patchValue({
        EmployeeName: this.selectedMember.Name
      });
    }

    this.entitySelectionModel.close();
  }

  closeEntitySelectionModel(){
    this.entitySelectionModel.close()
  }

  addMemberModal(content: any) {
    this.addMemberView = this.modalService.open(content, { width: '800' });
    this.entitySelectionModel.close();
  }


  closeNewMemberModal() {
    this.addMemberView.close();
  }

  addEntity(item: any) {
    this.selecteMember(item);
    this.addMemberView.close();
  }

  checkPaymentAmount(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {

        if (Number.isNaN(Number(control.value)) || Number(control.value) <= 0) {
          resolve({
            'validInput': true
          })
        } else {
          if (this.isWithdrawal) {
            if ((this.withdrawalForm.get('PaymentType').value === 'CASH') &&
              (Number(control.value) > Number(this.inHandBalance))) {
              resolve({
                'notEnoughCash': true
              })
            } else {
              if ((this.withdrawalForm.get('WithdrawalFrom').value === 'ONACCOUNT') &&
                Number(this.onAccountBalance) >= 0 && (Number(control.value) > Number(this.onAccountBalance))) {
                resolve({
                  'notEnoughAmountOnAccount': true
                })
              } else if ((this.withdrawalForm.get('WithdrawalFrom').value === 'PREPAID') &&
                Number(this.prePaidBalance) >= 0 && (Number(control.value) > Number(this.prePaidBalance))) {
                resolve({
                  'notEnoughinPrepiad': true
                })
              } else {
                resolve(null);
              }
            }
          } else {
            if (Number(control.value) > Number(this.inHandBalance)) {
              resolve({
                'notEnoughCash': true
              })
            } else {
              resolve(null);
            }
          }
        }
      }, 10);
    })
  }

  tickerFunc(tick: number, sessionKey: string, withdrawalData: any) {
    this.exceSessionService.getSessionValue(sessionKey).subscribe(
      result => {
        if (result.Data) {
          this.timerSub.unsubscribe();
          if (result.Data === 'ERROR') {
            this.timerSub.unsubscribe();
            PreloaderService.hidePreLoader();
            this.SetTerminalResponse(0, withdrawalData);
          } if (result.Data === 'SUCCESS') {
            this.timerSub.unsubscribe();
            PreloaderService.hidePreLoader();
            this.SetTerminalResponse(1, withdrawalData);
            return;
          } if (result.Data === 'DUPLICATE') {
            this.timerSub.unsubscribe();
            PreloaderService.hidePreLoader();
            this.SetTerminalResponse(3, withdrawalData);
          } else if (tick === 60) {
            this.timerSub.unsubscribe();
            PreloaderService.hidePreLoader();
            this.SetTerminalResponse(2, withdrawalData);
          }
        }
      },
      err => {
        this.timerSub.unsubscribe();
        PreloaderService.hidePreLoader();
        this.SetTerminalResponse(0, withdrawalData);
      });
  }

  private SetTerminalResponse(status: Number, withdrawalData: any) {
    switch (status) {
      case 0:
        this.exceMessageService.openMessageBox('ERROR',
          {
            messageTitle: 'ERROR',
            messageBody: 'COMMON.PaymentError'
          });
        PreloaderService.hidePreLoader();
        break;
      case 1:
        this.saveWithdrawalData(withdrawalData);
        break;
      case 2:
        this.exceMessageService.openMessageBox('WARNING',
          {
            messageTitle: 'WARNING',
            messageBody: 'COMMON.MsgTerminalDidNotResponse'
          });
        PreloaderService.hidePreLoader();
        break;

    }

  }

  private saveWithdrawalData(body) {
    PreloaderService.showPreLoader();
    this.shopService.SaveWithdrawal(body).subscribe(res => {
      this.closeWithdrowalModel.emit();
      PreloaderService.hidePreLoader();
    }, error => {
      PreloaderService.hidePreLoader();
    });
  }
}


