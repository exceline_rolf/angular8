import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopArticlesComponent } from './shop-articles.component';

describe('ShopArticlesComponent', () => {
  let component: ShopArticlesComponent;
  let fixture: ComponentFixture<ShopArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopArticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
