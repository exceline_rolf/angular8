import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShopArticlesService } from '../shop-articles.service';
import { ShopHomeService } from '../../shop-home.service';

@Component({
  selector: 'app-shortcut-key',
  templateUrl: './shortcut-key.component.html',
  styleUrls: ['./shortcut-key.component.scss']
})
export class ShortcutKeyComponent implements OnInit, OnDestroy {

  allArticles: any[];
  shortCutItems: any[];
  shortCutItemsBase: any[];

  articleType = 'ALL';
  articleCategory = 'ALL';
  activity = 'ALL';

  filterSubscription: any;

  constructor(
    private shopArticlesService: ShopArticlesService,
    private shopHomeService: ShopHomeService
  ) {

    this.filterSubscription = this.shopArticlesService.filterArticles.subscribe(
      params => {
        if (this.shortCutItemsBase) {

          this.shortCutItems = this.shortCutItemsBase;

          this.articleType = params.articlelType;
          this.articleCategory = params.artleCategory;
          this.activity = params.activity;

          if (this.articleType === 'ALL') {
            if ((this.articleCategory === 'ALL') && (this.activity === 'ALL')) {
              this.shortCutItems = this.shortCutItemsBase;
            }
            if ((this.articleCategory !== 'ALL') && (this.activity === 'ALL')) {
              this.shortCutItems = this.shortCutItemsBase.filter(
                item => ((item.CategoryCode === this.articleCategory)));
            }
            if ((this.articleCategory === 'ALL') && (this.activity !== 'ALL')) {
              this.shortCutItems = this.shortCutItemsBase.filter(
                item => ((item.ActivityName === this.activity)));
            }
            if ((this.articleCategory !== 'ALL') && (this.activity !== 'ALL')) {
              this.shortCutItems = this.shortCutItemsBase.filter(
                item => ((item.CategoryCode === this.articleCategory) && (item.ActivityName === this.activity)));
            }

          } else {
            if ((this.articleCategory === 'ALL') && (this.activity === 'ALL')) {
              this.shortCutItems = this.shortCutItemsBase.filter(
                item => (item.ArticleType === this.articleType));
            }
            if ((this.articleCategory !== 'ALL') && (this.activity === 'ALL')) {
              this.shortCutItems = this.shortCutItemsBase.filter(
                item => ((item.ArticleType === this.articleType) && (item.CategoryCode === this.articleCategory)));
            }
            if ((this.articleCategory === 'ALL') && (this.activity !== 'ALL')) {
              this.shortCutItems = this.shortCutItemsBase.filter(
                item => ((item.ArticleType === this.articleType) && (item.ActivityName === this.activity)));
            }
            if ((this.articleCategory !== 'ALL') && (this.activity !== 'ALL')) {
              this.shortCutItems = this.shortCutItemsBase.filter(
                item => ((item.ArticleType === this.articleType) && (item.CategoryCode === this.articleCategory) && (item.ActivityName === this.activity)));
            }
          }
          if (params.articleName) {
            this.shortCutItems = this.shortCutItems.filter(o =>
              o.Description.toLowerCase().includes(params.articleName.toLowerCase())
            );
          }

          if (params.vendorName) {
            this.shortCutItems = this.shortCutItems.filter(o =>
              o.VendorName.toLowerCase().includes(params.vendorName.toLowerCase())
            );
          }

        }
      }
    );

  }

  ngOnInit() {
    this.shopArticlesService.allArticles.subscribe(
      articles => {
        if (articles) {
          this.allArticles = articles;
          this.allArticles.forEach(item => {
            if (item.StockLevel < 0) {
              item.StockLevel = 0;
            }
            if (item.SortCutKey > 0) {
              item.ActiveStatus = true;
              item.IsSystemAdded = true;
            }
          });
          this.shortCutItems = this.allArticles.filter(item => (item.SortCutKey > 0));
          this.shortCutItemsBase = this.shortCutItems;
        }
      });

  }

  shortcutKeyDoubleClick(item) {
    this.shopHomeService.addShopItem(item);
  }

  ngOnDestroy(): void {
    this.filterSubscription.unsubscribe();

  }


}
