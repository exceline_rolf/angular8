import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopHomeLayoutComponent } from './shop-home-layout.component';

describe('ShopHomeLayoutComponent', () => {
  let component: ShopHomeLayoutComponent;
  let fixture: ComponentFixture<ShopHomeLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopHomeLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopHomeLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
