import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ExceShopService } from 'app/modules/shop/services/exce-shop.service';
import { ShopHomeService } from 'app/modules/shop/shop-home/shop-home.service';
import { ExceGymSetting } from 'app/shared/SystemObjects/Common/ExceGymSetting';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-shop-home-layout',
  templateUrl: './shop-home-layout.component.html',
  styleUrls: ['./shop-home-layout.component.scss']
})
export class ShopHomeLayoutComponent implements OnInit, OnDestroy {
  SalePointAddedEvent: Subscription;
  closeShopEvent: Subscription;
  loginSuccessEvent: Subscription;
  hardwareEvent: Subscription;
  machineNameEvent: Subscription;
  salePointAdded: any;
  machineName: string;
  addSalePoint = false;
  settingEvent: Subscription;
  isLoginReaderAdded: boolean;
  hardwareProfileList: any;
  shopLoginData: any = {};
  exceGymSetting: ExceGymSetting = new ExceGymSetting();
  selectedGymsettings = [
    'EcoNegativeOnAccount',
    'EcoSMSInvoiceShop',
    'EcoIsPrintInvoiceInShop',
    'EcoIsPrintReceiptInShop',
    'EcoIsShopOnNextOrder',
    'EcoIsOnAccount',
    'EcoIsDefaultCustomerAvailable',
    'EcoIsPayButtonsAvailable',
    'EcoIsPettyCashSameAsUserAllowed',
    'OtherIsShopAvailable',
    'OtherLoginShop',
    'HwIsShopLoginWithCard',
    'ReqMemberListRequired',
    'OtherIsLogoutFromShopAfterSale',
    'EcoIsBankTerminalIntegrated',
    'EcoIsDailySettlementForAll',
    'OtherIsValidateShopGymId'
  ];

  isShopLoginNeed: boolean;
  isShopLoginWithCard: boolean;
  isShopLogged = false;
  @Output() closeShop = new EventEmitter<any>();
  @Output() closeShopAfterSave = new EventEmitter<any>();

  constructor(
    private shopService: ExceShopService,
    private adminService: AdminService,
    private shopHomeService: ShopHomeService,
    private exceBreadcrumbService: ExceBreadcrumbService,
    private loginService: ExceLoginService

  ) {
    this.loginSuccessEvent = shopHomeService.shopLoginSuccessEvent.subscribe(
      res => {
        this.isShopLogged = true;
      });

    this.closeShopEvent = shopHomeService.closeShopEvent.subscribe(
      res => {
        if (res === true) {
          this.closeShopAfterSave.emit(true);
        } else if (res === false) {
          this.closeShop.emit(true);
        }
      }
    );

    this.SalePointAddedEvent = this.shopHomeService.salePointAddedEvent.subscribe(
      res => {
        this.salePointAdded = res;
        if (res > 0) {
          this.shopHomeService.$hasUserLoggedShop = true;
          if (this.isShopLoginNeed) {
            this.isShopLogged = true;
          }
        }
      }
    );

    // exceBreadcrumbService.addBreadCumbItem.next({
    //   name: 'SHOP',
    //   url: '/shop'
    // });
  }

  ngOnInit() {
    this.machineName = this.loginService.MachineName === '' ? 'DEFAULT' : this.loginService.MachineName;
    this.settingEvent = this.shopService.GetSelectedGymSettings({ GymSetColNames: this.selectedGymsettings, IsGymSettings: true }).subscribe(
      res => {
        for (const key in res.Data) {
          if (res.Data.hasOwnProperty(key)) {

            switch (key) {
              case 'EcoNegativeOnAccount':
                this.exceGymSetting.OnAccountNegativeVale = res.Data[key];
                break;
              case 'EcoIsShopOnNextOrder':
                this.exceGymSetting.IsShopOnNextOrder = res.Data[key];
                break;
              case 'EcoIsDailySettlementForAll':
                this.exceGymSetting.IsDailySettlementForAll = res.Data[key];
                break;
              case 'EcoIsOnAccount':
                this.exceGymSetting.IsOnAccount = res.Data[key];
                break;
              case 'EcoIsDefaultCustomerAvailable':
                this.exceGymSetting.IsDefaultCustomerAvailable = res.Data[key];
                break;
              case 'EcoIsPayButtonsAvailable':
                this.exceGymSetting.IsPayButtonsAvailable = res.Data[key];
                break;
              case 'EcoIsPettyCashSameAsUserAllowed':
                this.exceGymSetting.IsPettyCashSameAsUserAllowed = res.Data[key];
                break;
              case 'EcoIsPrintInvoiceInShop':
                this.exceGymSetting.IsPrintInvoiceInShop = res.Data[key];
                break;
              case 'EcoIsPrintReceiptInShop':
                this.exceGymSetting.IsPrintReceiptInShop = res.Data[key];
                break;
              case 'OtherIsShopAvailable':
                this.exceGymSetting.IsShopAvailable = res.Data[key];
                break;
              case 'OtherLoginShop':
                this.exceGymSetting.IsShopLoginNeeded = res.Data[key];
                break;
              case 'HwIsShopLoginWithCard':
                this.exceGymSetting.IsShopLoginWithCard = res.Data[key];
                this.isShopLoginWithCard = res.Data[key];
                break;
              case 'ReqMemberListRequired':
                this.exceGymSetting.MemberRequired = res.Data[key];
                break;
              case 'OtherIsLogoutFromShopAfterSale':
                this.exceGymSetting.IsLogoutFromShopAfterSale = res.Data[key];
                break;
              case 'EcoSMSInvoiceShop':
                this.exceGymSetting.IsSMSInvoiceShop = res.Data[key];
                break;
              case 'EcoIsBankTerminalIntegrated':
                this.exceGymSetting.IsBankTerminalIntegrated = res.Data[key];
                break;
              case 'OtherIsValidateShopGymId':
                this.exceGymSetting.IsValidateShopGymId = res.Data[key];
                break;
            }
          }
        }

        this.shopLoginData.GymSetting = this.exceGymSetting;

        if (this.exceGymSetting.IsShopLoginNeeded) {
          this.isShopLoginNeed = true;
          this.shopHomeService.$isShopLoginNeed = this.isShopLoginNeed;
        } else {
          this.isShopLoginNeed = false;
        }
        if (this.shopHomeService.$shopMode !== 'INVOICE') {
          this.machineNameEvent = this.shopService.GetSalesPointByMachineName(this.machineName).subscribe(
            salePoints => {
              this.shopLoginData.SalePoint = salePoints.Data;
              this.hardwareEvent = this.adminService.getGymSettings('HARDWARE', null).subscribe(
                hwSetting => {
                  this.hardwareProfileList = hwSetting.Data;
                  if (this.shopLoginData.SalePoint) {
                    hwSetting.Data.forEach(hwProfile => {
                      if (this.shopLoginData.SalePoint != null && this.shopLoginData.SalePoint.HardwareProfileId === hwProfile.Id) {
                        this.shopLoginData.SelectedHardwareProfile = hwProfile;
                        this.shopLoginData.HardwareProfileId = hwProfile.Id;
                      }
                      this.shopHomeService.$shopLoginData = this.shopLoginData;
                    });
                  }

                  if (this.shopLoginData.SelectedHardwareProfile) {
                    const profiles = this.shopLoginData.SelectedHardwareProfile.HardwareProfileItemList.filter(x => x.Code === 'LOGINREADER')
                    if (profiles.length > 0) {
                      this.isLoginReaderAdded = true;
                    } else {
                      this.isLoginReaderAdded = false;
                    }
                  } else {
                    this.isLoginReaderAdded = false;
                  }
                  if (!this.shopLoginData.SalePoint || !(this.shopLoginData.SalePoint.ID > 0)) {
                    this.addSalePoint = true;
                  }
                  if (!this.isShopLoginNeed && !this.addSalePoint) {
                    this.isShopLogged = true;
                  }
                },
                error => {
                });
            },
            error => {

            });
        }
        if (this.shopHomeService.$shopMode === 'INVOICE') {
          this.shopLoginData = this.shopHomeService.$shopLoginData;
          if (!this.shopLoginData.SalePoint || !(this.shopLoginData.SalePoint.ID > 0)) {
            this.addSalePoint = true;
          }
          if (!this.isShopLoginNeed && !this.addSalePoint) {
            this.isShopLogged = true;
          }
        }
      },
      error => {

      });
  }

  ngOnDestroy(): void {
    // **** clearing shopitems before purchase is done when paying from mc-orders *****
    // this.shopHomeService.ShopItems = [];
    if (this.settingEvent) {
      this.settingEvent.unsubscribe();
    }

    if (this.SalePointAddedEvent) {
      this.SalePointAddedEvent.unsubscribe();
    }

    if (this.closeShopEvent) {
      this.closeShopEvent.unsubscribe();
    }

    if (this.loginSuccessEvent) {
      this.loginSuccessEvent.unsubscribe();
    }

    if (this.hardwareEvent) {
      this.hardwareEvent.unsubscribe();
    }

    if (this.machineNameEvent) {
      this.machineNameEvent.unsubscribe();
    }
  }
}
