import { TestBed, inject } from '@angular/core/testing';

import { ExceLoginService } from './exce-login.service';

describe('ExceLoginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceLoginService]
    });
  });

  it('should be created', inject([ExceLoginService], (service: ExceLoginService) => {
    expect(service).toBeTruthy();
  }));
});
