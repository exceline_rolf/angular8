import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymHomeComponent } from './gym-home.component';

describe('GymHomeComponent', () => {
  let component: GymHomeComponent;
  let fixture: ComponentFixture<GymHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
