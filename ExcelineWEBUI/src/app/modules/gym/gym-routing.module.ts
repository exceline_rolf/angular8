import { NgModule, Inject } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IAppConfig } from '../../app-config/app-config.interface';
import { APP_CONFIG } from '../../app-config/app-config.constants';

import { ExceToolbarService } from '../common/exce-toolbar/exce-toolbar.service';
import { TranslateService } from '@ngx-translate/core';
import { GymHomeComponent } from './gym-home/gym-home.component';

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'home', component: GymHomeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GymRoutingModule {
  constructor(
    private translate: TranslateService,
    private exceToolbarService: ExceToolbarService,
     @Inject(APP_CONFIG) private config: IAppConfig
  ) {
    const langs = [];
    this.config.LANGUAGES.map(ln => {
      langs.push(ln.ID);
    })
    this.translate.addLangs(langs);
    translate.setDefaultLang(this.config.LANGUAGE_DEFAULT.ID);

    const lang = this.exceToolbarService.getSelectedLanguage();
    this.translate.use(lang.id);
    this.exceToolbarService.langUpdated.subscribe(
      (lang1) => {
        this.translate.use(lang1.id);
      }
    );
  }
}



