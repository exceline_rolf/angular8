import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportingEditQueryComponent } from './reporting-edit-query.component';

describe('ReportingEditQueryComponent', () => {
  let component: ReportingEditQueryComponent;
  let fixture: ComponentFixture<ReportingEditQueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportingEditQueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportingEditQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
