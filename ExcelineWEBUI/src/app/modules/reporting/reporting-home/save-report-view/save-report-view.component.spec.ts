import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveReportViewComponent } from './save-report-view.component';

describe('SaveReportViewComponent', () => {
  let component: SaveReportViewComponent;
  let fixture: ComponentFixture<SaveReportViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveReportViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveReportViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
