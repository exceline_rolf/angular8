import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymUsageReportSettingComponent } from './gym-usage-report-setting.component';

describe('GymUsageReportSettingComponent', () => {
  let component: GymUsageReportSettingComponent;
  let fixture: ComponentFixture<GymUsageReportSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymUsageReportSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymUsageReportSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
