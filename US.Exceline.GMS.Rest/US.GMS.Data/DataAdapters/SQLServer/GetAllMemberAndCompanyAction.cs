﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer
{
    class GetAllMemberAndCompanyAction : USDBActionBase<List<ExcelineMemberDC>>
    {
        private readonly int _branchId;
        private readonly string _searchText = string.Empty;
        private readonly int _status;
        private readonly int _hit;
        public GetAllMemberAndCompanyAction(int branchId, string searchText, int status, int hit)
        {
            _branchId = branchId;
            _searchText = ProcessKeyWord(searchText);
            _status = status;
            _hit = hit;
        }

        protected override List<ExcelineMemberDC> Body(DbConnection connection)
        {
            var ordinaryMemberLst = new List<ExcelineMemberDC>();
            const string storedProcedureName = "USExceGMSGetMemberAndCompany";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                if (!string.IsNullOrEmpty(_searchText))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@seachText", DbType.String, _searchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Int32, _status));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Hit", DbType.Int32, _hit));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var ordineryMember = new ExcelineMemberDC();
                    ordineryMember.Id = Convert.ToInt32(reader["MemberId"]);
                    ordineryMember.Name = reader["Name"].ToString();
                    ordineryMember.FirstName = reader["FirstName"].ToString();
                    ordineryMember.LastName = reader["LastName"].ToString();
                    string roleType = reader["RoleId"].ToString();
                    if (!string.IsNullOrEmpty(roleType.Trim()))
                    {
                        try
                        {
                            ordineryMember.Role = (MemberRole)Enum.Parse(typeof(MemberRole), roleType);
                        }
                        catch
                        {
                            ordineryMember.Role = MemberRole.NONE;
                        }
                    }
                    ordineryMember.Address1 = reader["Address1"].ToString();
                    ordineryMember.Address2 = reader["Address2"].ToString();
                    ordineryMember.Mobile = Convert.ToString(reader["Mobile"]).Trim();
                    ordineryMember.StatusName = Convert.ToString(reader["StatusName"]);
                    ordineryMember.MobilePrefix = Convert.ToString(reader["TelMobilePrefix"]).Trim();
                    ordineryMember.Email = Convert.ToString(reader["Email"]);
                    ordineryMember.ZipCode = Convert.ToString(reader["ZipCode"]);
                    ordineryMember.ZipName = Convert.ToString(reader["ZipName"]);
                    ordineryMember.CustId = Convert.ToString(reader["CustId"]);
                    ordineryMember.BranchID = Convert.ToInt32(reader["BranchId"]);
                    ordineryMember.GuardianId = Convert.ToInt32(reader["GuardianId"]);

                    ordinaryMemberLst.Add(ordineryMember);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ordinaryMemberLst;
        }

        private string ProcessKeyWord(string searchWord)
        {
            if (!string.IsNullOrEmpty(searchWord))
            {
                string[] keywordparts = searchWord.Trim().Split(new[] { ':' }, 2);
                if (keywordparts.Length > 1)
                {
                    string[] wordParts = keywordparts[1].Split(' ');
                    if (wordParts.Length > 1)
                    {
                        string newword = wordParts[0];

                        for (int index = 1; index < wordParts.Length; index++)
                        {
                            newword = string.Format("{0} AND {1}", newword, wordParts[index]);
                        }
                        return string.Format("{0}:{1}", keywordparts[0], newword);
                    }

                    else
                        return string.Format("{0}:{1}", keywordparts[0], wordParts[0]);
                }
                else if (keywordparts.Length == 1)
                {
                    string[] wordParts = keywordparts[0].Split(' ');
                    if (wordParts.Length > 1)
                    {
                        string newword = wordParts[0];
                        for (int index = 1; index < wordParts.Length; index++)
                        {
                            newword = string.Format("{0} AND {1}", newword, wordParts[index]);
                        }

                        return newword;
                    }

                    else
                        return wordParts[0];
                }
                else
                {
                    return string.Empty;
                }

            }
            else
            {
                return string.Empty;
            }
        }

    }
}
