﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/19/2012 3:10:37 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveCategoryAction : USDBActionBase<int>
    {
        private readonly CategoryDC _category;
        public SaveCategoryAction(CategoryDC categoryDC)
        {
            _category = categoryDC;
        }

        protected override int Body(DbConnection connection)
        {
            int categoryId = -1;
            const string storedProcedureName = "USExceGMSAddCategory";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryTypeId", DbType.Int32, _category.TypeId));
                if (_category.CategoryTypeCode != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@typeCode", DbType.String, _category.CategoryTypeCode.Trim()));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _category.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@code", DbType.String, _category.Code));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _category.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@image", DbType.Byte, _category.CategoryImage));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _category.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastModifiedUser", DbType.String, _category.LastModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Boolean, _category.ActiveStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _category.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, 0));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@color", DbType.String, _category.Color));
                object obj = cmd.ExecuteScalar();
                categoryId = Convert.ToInt32(obj);
            }
            catch
            {
                categoryId = -1;
                throw;
            }
            return categoryId;
        }
    }
}
