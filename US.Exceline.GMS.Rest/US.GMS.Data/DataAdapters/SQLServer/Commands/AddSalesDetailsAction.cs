﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;
using System.Linq;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddSalesDetailsAction : USDBActionBase<bool>
    {
        private ShopSalesDC _salesDetails;
        private int _branchID = -1;
        private string _user = string.Empty;
        private int _arItemNo = -1;
        private int _installmentId = -1;
        private string _paymentTypeCode = string.Empty;
             
        public AddSalesDetailsAction(ShopSalesDC salesDetails, int brnachId, string user, int aritemNo, int installmentId, string paymentTypeCode)
        {
            _salesDetails = salesDetails;
            _branchID = brnachId;
            _user = user;
            _arItemNo = aritemNo;
            _installmentId = installmentId;
            _paymentTypeCode = paymentTypeCode;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            return true;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSShopAddSales";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entitiyId", DbType.Int32, _salesDetails.EntitiyId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRoleType", DbType.String, _salesDetails.EntityRoleType));               
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@arItemNo", DbType.Int32, _arItemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _user));
                if (_salesDetails.SalesPointId != -1 && _salesDetails.SalesPointId != 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@salePointID", DbType.Int32, _salesDetails.SalesPointId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentId", DbType.Int32, _installmentId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaymentTypeCode", DbType.String, _paymentTypeCode));

                DataTable salesItemsDT = new DataTable();
                DataColumn col = null;
                col = new DataColumn("ArticleId", typeof(Int32));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Amount", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Discount", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Quantity", typeof(Int32));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("TotalAmount", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Category", typeof(String));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Comment", typeof(String));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("IsCompaign", typeof(bool));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("DefaultPrice", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("SalePrice", typeof(Decimal));
                salesItemsDT.Columns.Add(col);

                if (_salesDetails.ShopSalesItemList != null)
                    foreach (ShopSalesItemDC item in _salesDetails.ShopSalesItemList.ToList().Where(x =>  x.Quantity > 0).ToList())
                    {
                        salesItemsDT.Rows.Add(item.ArticleId, item.Amount, item.Discount, item.Quantity, item.TotalAmount, item.ItemCategory, item.Comment, item.IsCompaign , item.DefaultAmount, item.SaleAmount);
                    }

                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@salesItems";
                parameter.SqlDbType = System.Data.SqlDbType.Structured;
                parameter.Value = salesItemsDT;
                cmd.Parameters.Add(parameter);
                cmd.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
