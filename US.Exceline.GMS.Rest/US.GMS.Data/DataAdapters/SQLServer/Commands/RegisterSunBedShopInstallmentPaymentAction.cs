﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using US.Common.Logging.API;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Exceline.GMS.Modules.Economy.API;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterSunBedShopInstallmentPaymentAction : USDBActionBase<SaleResultDC>
    {
        private GatpurchaseShopItem _gatPuchaseShopItem;
        private int _memberBranchID = -1;
        private int _loggedBranchId = -1;
        private string _user = string.Empty;
        private InstallmentDC _installment = null;
        private PaymentDetailDC _payementDetail = null;
        private string _gymCode = string.Empty;
        private ShopSalesDC _salesDetails = null;
        private bool _isBookingPayment = false;
        private bool _isContainReturn = false;
        private List<ContractItemDC> _contractItemReturnList = new List<ContractItemDC>();

        public RegisterSunBedShopInstallmentPaymentAction(SunBedHelperObject sunbedHelper)
        {
            _gatPuchaseShopItem = sunbedHelper.GatPuchaseShopItem;
            _memberBranchID = sunbedHelper.MemberBranchID;
            _loggedBranchId = sunbedHelper.LoggedbranchID;
            _user = sunbedHelper.User;
            _installment = sunbedHelper.Installments.FirstOrDefault();
            _payementDetail = sunbedHelper.PaymentDetails.FirstOrDefault();
            _gymCode = sunbedHelper.GymCode;
            _salesDetails = sunbedHelper.ShopSaleDetails.FirstOrDefault();
            _isBookingPayment = sunbedHelper.IsBookingPayment;
            _gatPuchaseShopItem.IsNextOrder = false;
        }

        public SaleResultDC RunOnTransaction(DbTransaction transaction)
        {            
            SaleResultDC saleResult = new SaleResultDC();
            int generatedArItemNo = -1;
            string invocieRef = string.Empty;
            string custId = string.Empty;
            int memberId = -1;
            try
            {              

                List<int> orderlineList = new List<int>();
                List<string> orderlineErrors = new List<string>();
                GetClaimInfoForShopInstallmentPaymentAction action = new GetClaimInfoForShopInstallmentPaymentAction(_installment.MemberId, _memberBranchID, _loggedBranchId, _installment.Id);
                IUSPClaim claim = action.RunOnTransaction(transaction);
                claim.InvoiceType = GetInvoiceType(claim.InvoiceTypeId);
                claim.KID = claim.InstallmentKID;
                claim.Amount = _payementDetail.InvoiceAmount;
                claim.BranchNumber = _loggedBranchId.ToString();
                claim.InvoicedDate = DateTime.Now.ToString("yyyy-MM-dd");
                claim.DueDate = DateTime.Now.AddDays(14).ToString("yyyy-MM-dd");
                claim.DueBalance = "0";
                claim.Balance = "0";

                custId = claim.Debtor.DebtorcustumerID;
                memberId = _installment.MemberId;

                if (claim != null)
                {
                    foreach (ContractItemDC contractItem in _installment.AddOnList)
                    {
                        if (contractItem.Quantity > 0)
                        {
                            CreditorOrderLine orderline = new CreditorOrderLine();
                            orderline.Amount = contractItem.Price;
                            orderline.ArticleNo = Convert.ToString(contractItem.ArticleId);
                            orderline.ArticleText = contractItem.ItemName;
                            orderline.Balance = contractItem.Price;
                            orderline.BranchNo = _loggedBranchId;

                            orderline.CoRelationId = Convert.ToString(_installment.Id);
                            orderline.CreatedUser = _user;
                            orderline.CreditorInvoiceType = CreditorInvoiceType.CREDITOR;
                            if (!string.IsNullOrEmpty(claim.Creditor.CreditorInkassoID))
                            {
                                orderline.CreditorNo = Convert.ToInt32(claim.Creditor.CreditorInkassoID);
                            }
                            orderline.CreditorName = claim.Creditor.CreditorName;
                            orderline.CustomerNo = claim.Debtor.DebtorEntityID;
                            orderline.DebtorEntNo = claim.Debtor.DebtorEntityID;
                            orderline.Discount = Convert.ToDecimal(contractItem.Discount);
                            try
                            {
                                orderline.DueDate = Convert.ToDateTime(claim.DueDate);
                            }
                            catch
                            {
                            }
                            orderline.InvoiceNo = claim.InvoiceNumber;
                            orderline.NoOfItems = contractItem.Quantity;
                            orderline.OrderType = "E";
                            orderline.RegDate = DateTime.Now;
                            orderline.ModifiedUser = _user;
                            orderline.Text = contractItem.ItemName;
                            orderline.UnitPrice = contractItem.UnitPrice;

                            OperationResult<int> oResult = Invoice.AddCreditorOrderLine(orderline, _user, transaction);
                            if (oResult.ErrorOccured)
                            {
                                foreach (NotificationMessage message in oResult.Notifications)
                                {
                                    orderlineErrors.Add(message.Message + "Installment : " + _installment.Id.ToString() + " article no : " + orderline.ArticleNo);
                                }

                                transaction.Rollback();
                                saleResult.AritemNo = -1;
                                saleResult.InvoiceNo = string.Empty;
                                saleResult.SaleStatus = SaleErrors.ORDERLINEADDERROR;
                                return saleResult;
                            }
                            else
                            {
                                orderlineList.Add(oResult.OperationReturnValue);
                            }
                        }
                        else
                        {
                            int creditedQuantity = contractItem.Quantity * -1;

                            while (creditedQuantity > 0)
                            {
                                GetShopCreditNoteDetailsAction creditNoteDetailsAction = new GetShopCreditNoteDetailsAction(_installment.MemberId, contractItem.ArticleId, _loggedBranchId);
                                ShopCreditNoteDetail creditNoteDetails = creditNoteDetailsAction.RunOnTransaction(transaction);
                                if (creditNoteDetails != null)
                                {
                                    PaymentProcessResult creditNOteResult;
                                    List<IUSPTransaction> transactions = new List<IUSPTransaction>();

                                    transactions.Add(GetTransaction(creditNoteDetails.CID, creditNoteDetails.PID, creditNoteDetails.KID, creditNoteDetails.Ref, (contractItem.Price * -1), _user, creditNoteDetails.CaseNo));
                                    creditNOteResult = Transaction.RegsiterTransaction(transactions, transaction);

                                    if (!creditNOteResult.ResultStatus)
                                    {
                                        transaction.Rollback();
                                        saleResult.SaleStatus = SaleErrors.ERROR;
                                        return saleResult;
                                    }
                                    else
                                    {
                                        string StoredProcedureName = "USExceGMSManageMembershipAddCreditNote";
                                        DbCommand cmd = new SqlCommand();
                                        cmd.Connection = transaction.Connection;
                                        cmd.Transaction = transaction;
                                        cmd.CommandType = CommandType.StoredProcedure;
                                        cmd.CommandText = StoredProcedureName;
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@custid", DbType.String, creditNoteDetails.CID));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceNo", DbType.String, creditNoteDetails.Ref));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@kid", DbType.String, creditNoteDetails.KID));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Decimal, (contractItem.Price * -1)));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _loggedBranchId));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsReturn", DbType.Boolean, true));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, contractItem.ReturnComment));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@SaleiID", DbType.Int32, creditNoteDetails.SaleId));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@creditNoteDetails", SqlDbType.Structured, GetDetailsTable(contractItem, creditNoteDetails)));
                                        cmd.ExecuteNonQuery();

                                        saleResult.InvoiceNo = creditNoteDetails.Ref;
                                        saleResult.AritemNo = contractItem.ArticleId;
                                        saleResult.CustId = creditNoteDetails.CID;
                                        saleResult.MemberId = memberId;
                                    }

                                    creditedQuantity = creditedQuantity - creditNoteDetails.SumQuanltity;
                                }
                                else
                                {
                                    transaction.Rollback();
                                    saleResult.AritemNo = -1;
                                    saleResult.InvoiceNo = string.Empty;
                                    saleResult.SaleStatus = SaleErrors.ERROR;
                                    return saleResult;
                                }
                            }
                            _isContainReturn = true;
                            //here generatedARItemNo < 0 means there is no matching sales item                           
                            generatedArItemNo = 1;
                        }
                    }

                    ////------------------save shop transations data -------------
                    if (_payementDetail.PaidAmount <= 0 && _isContainReturn)
                    {
                        // _contractItemReturnList.Add(contractItem);
                        ShopTransactionDC _shopTrans = new ShopTransactionDC();
                        _shopTrans.BranchId = _loggedBranchId;
                        _shopTrans.CreatedUser = _user;
                        _shopTrans.CreatedDate = DateTime.Now;
                        _shopTrans.Mode = US.GMS.Core.SystemObjects.TransactionTypes.RETURN;
                        _shopTrans.Amount = _payementDetail.PaidAmount * -1;
                        _shopTrans.SalePointId = _salesDetails.SalesPointId;
                        _shopTrans.EntityId = _salesDetails.EntitiyId;
                        _shopTrans.EntityRoleType = _salesDetails.EntityRoleType;
                        SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                        shopTransactionAction.RunOnTransaction(transaction);
                    }

                    ////--------------------------------------------------------

                    if (orderlineList.Count > 0)
                    {
                        OperationResult<List<IUSPClaim>> Iresult = Invoice.SelectedOrderLineInvoiceGenerator(orderlineList, string.Empty, "E", _user, transaction, _installment.Id, Convert.ToInt32(claim.Creditor.CreditorInkassoID), _gymCode);
                        if (Iresult.ErrorOccured)
                        {
                            transaction.Rollback();
                            saleResult.AritemNo = -1;
                            saleResult.InvoiceNo = string.Empty;
                            saleResult.SaleStatus = SaleErrors.INVOICEADDINGERROR;
                            return saleResult;
                        }
                        else
                        {
                            if (Iresult.OperationReturnValue.Count > 0)
                            {
                                generatedArItemNo = Iresult.OperationReturnValue[0].ARItemNo;
                                invocieRef = Iresult.OperationReturnValue[0].InvoiceNumber;
                                saleResult.AritemNo = generatedArItemNo;
                                saleResult.InvoiceNo = Iresult.OperationReturnValue[0].InvoiceNumber;
                                if (_payementDetail.PaidAmount > 0)
                                {
                                    claim = Iresult.OperationReturnValue[0];
                                    PaymentProcessResult paymentResult = null;
                                    List<IUSPTransaction> transactions = new List<IUSPTransaction>();
                                    foreach (PayModeDC payMode in _payementDetail.PayModes)
                                    {
                                        //------------------save shop transations data -----------------------------------------------------------------
                                        ShopTransactionDC _shopTrans = new ShopTransactionDC();
                                        _shopTrans.BranchId = _loggedBranchId;
                                        _shopTrans.CreatedUser = _user;
                                        _shopTrans.CreatedDate = DateTime.Now;
                                        try
                                        {
                                            _shopTrans.Mode = (US.GMS.Core.SystemObjects.TransactionTypes)Enum.Parse(typeof(US.GMS.Core.SystemObjects.TransactionTypes), payMode.PaymentTypeCode, true);
                                        }
                                        catch
                                        {
                                            _shopTrans.Mode = US.GMS.Core.SystemObjects.TransactionTypes.NONE;
                                            _shopTrans.ModeText = payMode.PaymentTypeCode;
                                        }
                                        _shopTrans.Amount = payMode.Amount;
                                        _shopTrans.SalePointId = _salesDetails.SalesPointId;
                                        _shopTrans.EntityId = _salesDetails.EntitiyId;
                                        _shopTrans.EntityRoleType = _salesDetails.EntityRoleType;
                                        SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                                        shopTransactionAction.RunOnTransaction(transaction);

                                        //---------------------------------------------------------------------------------------------------------------------
                                        if (payMode.PaymentTypeCode != "EMAILSMSINVOICE")
                                        {
                                            transactions.Add(GetTransaction(claim, payMode.Amount, payMode.PaymentTypeCode, payMode.PaidDate));

                                        }

                                        if (payMode.PaymentTypeCode == "BANKTERMINAL" && _installment.Balance < 0)
                                        {
                                            WithdrawalDC withdrawal = new WithdrawalDC();
                                            withdrawal.WithdrawalType = WithdrawalTypes.CASHWITHDRAWAL;
                                            withdrawal.SalePointId = _salesDetails.SalesPointId;
                                            withdrawal.MemberId = _installment.MemberId;
                                            withdrawal.PaymentAmount = _installment.Balance * -1;
                                            SaveWithdrawalAction saveWithdrawalAction = new SaveWithdrawalAction(_loggedBranchId, withdrawal, _user);
                                            saveWithdrawalAction.RunOnTransaction(transaction);
                                        }
                                    }

                                    paymentResult = Transaction.RegsiterTransaction(transactions, transaction);

                                    if (!paymentResult.ResultStatus)
                                    {
                                        foreach (PaymentProcessStepResult step in paymentResult.ProcessStepResultList)
                                        {
                                            foreach (string message in step.MessageList)
                                            {
                                                USLogError.WriteToFile(message, new Exception(), _user);
                                            }
                                        }
                                        transaction.Rollback();
                                        saleResult.SaleStatus = SaleErrors.PAYMENTADDINGERROR;
                                        return saleResult;
                                    }
                                    else
                                    {
                                        var tempEML = string.Empty;
                                        if (_payementDetail.PayModes.Any(mode => mode.PaymentTypeCode == "EMAILSMSINVOICE"))
                                            tempEML = "EML";

                                        AddSalesDetailsAction salesDetailAction = new AddSalesDetailsAction(_salesDetails, _loggedBranchId, _user, generatedArItemNo, -1, tempEML);
                                        salesDetailAction.RunOnTransaction(transaction);

                                        saleResult.AritemNo = generatedArItemNo;
                                        saleResult.InvoiceNo = invocieRef;
                                        _gatPuchaseShopItem.InvoiceCode = saleResult.InvoiceNo;
                                        saleResult.CustId = custId;
                                        saleResult.MemberId = memberId;
                                        saleResult.SaleStatus = SaleErrors.SUCCESS;
                                    }
                                }
                                else
                                {
                                    if (!_isBookingPayment)
                                    {
                                        AddSalesDetailsAction salesDetailAction = new AddSalesDetailsAction(_salesDetails, _loggedBranchId, _user, generatedArItemNo, -1, "SHOPINVOICE");
                                        salesDetailAction.RunOnTransaction(transaction);
                                    }
                                    UpdateOrderInvoiceTypeAction orderTypeAction = new UpdateOrderInvoiceTypeAction(_installment.Id, _installment.SponsoredAmount, _installment.Discount, 4, generatedArItemNo);// set as printed invoice
                                    orderTypeAction.RunOnTransaction(transaction);

                                    saleResult.AritemNo = generatedArItemNo;
                                    saleResult.InvoiceNo = invocieRef;
                                    _gatPuchaseShopItem.InvoiceCode = saleResult.InvoiceNo;
                                    saleResult.CustId = custId;
                                    saleResult.MemberId = memberId;
                                    saleResult.SaleStatus = SaleErrors.SUCCESS;
                                }

                                // add the gat purchase update process.                                
                                UpdateShopAccountItemAndGatPurchaseAction updateGatPurchaseAction = new UpdateShopAccountItemAndGatPurchaseAction(_gatPuchaseShopItem);
                                bool isUpdated = updateGatPurchaseAction.RunOnTransaction(transaction);
                                if (!isUpdated)
                                {
                                    transaction.Rollback();
                                    saleResult.SaleStatus = SaleErrors.ERROR;
                                    return saleResult;
                                }
                            }
                            else
                            {
                                transaction.Rollback();
                                saleResult.SaleStatus = SaleErrors.INVOICEADDINGERROR;
                                return saleResult;
                            }
                        }
                    }
                    else if (!_isContainReturn)
                    {
                        transaction.Rollback();
                        saleResult.SaleStatus = SaleErrors.ORDERLINEADDERROR;
                        return saleResult;
                    }
                }
                else
                {
                    transaction.Rollback();
                    saleResult.AritemNo = -1;
                    saleResult.InvoiceNo = string.Empty;
                    saleResult.SaleStatus = SaleErrors.ERROR;
                    return saleResult;
                }

                saleResult.SaleStatus = SaleErrors.SUCCESS;
                //transaction.Commit();
                return saleResult;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        } 

        private IUSPTransaction GetTransaction(IUSPClaim claim, decimal paymentAmount, string paymentSourceName, DateTime paidDate)
        {
            IUSPTransaction transaction = new USPTransaction();
            transaction.TransType = GetTransType(paymentSourceName);
            if ((transaction.TransType == "OP" || transaction.TransType == "OCR" || transaction.TransType == "BS" || transaction.TransType == "DC") && paidDate != null && paidDate != DateTime.MinValue)
            {
                transaction.VoucherDate = paidDate;
            }
            else
            {
                transaction.VoucherDate = DateTime.Today;
            }
            transaction.PID = claim.Creditor.CreditorInkassoID;
            transaction.CID = Convert.ToString(claim.Debtor.DebtorcustumerID.ToString());
            transaction.DueDate = _installment.DueDate;
            transaction.ReceivedDate = DateTime.Today;
            transaction.RegDate = DateTime.Today;
            transaction.Amount = paymentAmount;
            transaction.Source = paymentSourceName;
            transaction.KID = claim.KID;
            transaction.DebtorAccountNo = "";//TODO
            transaction.InvoiceNo = claim.InvoiceNumber;
            transaction.CreditorAccountNo = claim.Creditor.CreditorAccountNo;
            transaction.User = _user;
            return transaction;
        }

        private string GetTransType(string paymentType)
        {
            switch (paymentType)
            {
                case "CASH":
                    return "CSH";

                case "BANKTERMINAL":
                    return "TMN";

                case "OCR":
                    return "LOP";

                case "BANKSTATEMENT":
                    return "BS";

                case "DEBTCOLLECTION":
                    return "DC";

                case "ONACCOUNT":
                    return "OA";

                case "GIFTCARD":
                    return "GC";

                case "BANK":
                    return "BNK";

                case "PREPAIDBALANCE":
                    return "PB";
            }
            return string.Empty;
        }

        private InvoiceTypes GetInvoiceType(int itemType)
        {
            switch (itemType)
            {
                case 1:
                    return InvoiceTypes.DirectDeduct;

                case 2:
                    return InvoiceTypes.InvoiceForPrint;

                case 3:
                    return InvoiceTypes.Invoice;

                case 4:
                    return InvoiceTypes.DebtWarning;

                case 7:
                    return InvoiceTypes.Sponser;

                case 14:
                    return InvoiceTypes.PaymentDocumentForPrint;

                case 101:
                    return InvoiceTypes.ShopInvoice;

                default:
                    return InvoiceTypes.Invoice;
            }
        }

        private IUSPTransaction GetTransaction(string CID, string PID, string KID, string InvoiceNo, decimal creditNoteAmount, string user, int caseNo)
        {
            IUSPTransaction transaction = new USPTransaction();
            transaction.TransType = "CN";
            transaction.PID = PID;
            transaction.CID = CID;
            transaction.VoucherDate = DateTime.Today;
            transaction.ReceivedDate = DateTime.Today;
            transaction.RegDate = DateTime.Today;
            transaction.Amount = creditNoteAmount;
            transaction.Source = "";
            transaction.KID = KID;
            transaction.DebtorAccountNo = "";//TODO
            transaction.InvoiceNo = InvoiceNo;
            transaction.CreditorAccountNo = "";
            transaction.User = user;
            transaction.SubCaseNo = caseNo;

            return transaction;

        }

        private DataTable GetDetailsTable(ContractItemDC contractItem, ShopCreditNoteDetail creditNoteDetails)
        {
            try
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add(new DataColumn("CreditorNumber", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Date", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("EditUser", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Note", typeof(string)));
                dataTable.Columns.Add(new DataColumn("ARItemNo", typeof(int)));
                dataTable.Columns.Add(new DataColumn("ArticleText", typeof(string)));
                dataTable.Columns.Add(new DataColumn("OrderLineId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("CreditedQuantity", typeof(int)));
                dataTable.Columns.Add(new DataColumn("CreditedAmount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("ArticleID", typeof(int)));

                DataRow dataRow = dataTable.NewRow();
                dataRow["CreditorNumber"] = creditNoteDetails.CID;
                dataRow["Date"] = DateTime.Today;
                dataRow["EditUser"] = _user;
                dataRow["Note"] = contractItem.ReturnComment;
                dataRow["ARItemNo"] = -1;
                dataRow["ArticleText"] = contractItem.ItemName;
                dataRow["OrderLineId"] = creditNoteDetails.OrderlineID;
                dataRow["CreditedQuantity"] = contractItem.Quantity * -1;
                dataRow["CreditedAmount"] = contractItem.Price * -1;
                dataRow["ArticleID"] = contractItem.ArticleId;
                dataTable.Rows.Add(dataRow);

                return dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override SaleResultDC Body(DbConnection connection)
        {
            return null;
        }
    }
}
