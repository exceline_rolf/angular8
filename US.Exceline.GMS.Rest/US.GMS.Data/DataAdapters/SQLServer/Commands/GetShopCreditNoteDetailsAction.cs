﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetShopCreditNoteDetailsAction : USDBActionBase<ShopCreditNoteDetail>
    {
        private int _memberId = -1;
        private int _articleId = -1;
        private int _branchID = -1;
        public GetShopCreditNoteDetailsAction(int memberID, int articleId, int branchID)
        {
            _memberId = memberID;
            _articleId = articleId;
            _branchID = branchID;
        }

        protected override ShopCreditNoteDetail Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSShopGetCreditNoteDetails";
            ShopCreditNoteDetail _detail = null;
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", System.Data.DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArticleID", System.Data.DbType.Int32, _articleId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchID));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    _detail = new ShopCreditNoteDetail();
                    _detail.CID = Convert.ToString(reader["CustId"]);
                    _detail.PID = Convert.ToString(reader["CreditorInkassoID"]);
                    _detail.KID = Convert.ToString(reader["KID"]);
                    _detail.Ref = Convert.ToString(reader["Ref"]);
                    _detail.CaseNo = Convert.ToInt32(reader["CaseNo"]);
                    _detail.OrderlineID = Convert.ToInt32(reader["OrderlineID"]);
                    _detail.SumQuanltity = Convert.ToInt32(reader["SumQuantity"]);
                    _detail.SaleId = Convert.ToInt32(reader["SaleID"]);
                }

                return _detail;

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
        }

        public ShopCreditNoteDetail RunOnTransaction(DbTransaction transaction)
        {

            string spName = "USExceGMSShopGetCreditNoteDetails";
            ShopCreditNoteDetail _detail = null;
            DbDataReader reader = null;
            try
            {
                DbCommand command = new SqlCommand();
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = spName;

                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", System.Data.DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArticleID", System.Data.DbType.Int32, _articleId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchID));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    _detail = new ShopCreditNoteDetail();
                    _detail.CID = Convert.ToString(reader["CustId"]);
                    _detail.PID = Convert.ToString(reader["CreditorInkassoID"]);
                    _detail.KID = Convert.ToString(reader["KID"]);
                    _detail.Ref = Convert.ToString(reader["Ref"]);
                    _detail.CaseNo = Convert.ToInt32(reader["CaseNo"]);
                    _detail.OrderlineID = Convert.ToInt32(reader["OrderlineID"]);
                    _detail.SumQuanltity = Convert.ToInt32(reader["SumQuantity"]);
                    _detail.SaleId = Convert.ToInt32(reader["SaleID"]);
                }

                return _detail;

            }
            catch (Exception)
            {
                return null;
            }
             finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
        }
    }
}
