﻿using System;
using System.Data.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    class ImportNewCancelledStatusAction : USDBActionBase<int>
    {
        protected override int Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string spName = "dbo.USExceGMSCCXImportNewCancelled";
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.ExecuteNonQuery();
                return 1;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
