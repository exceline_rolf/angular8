﻿using System;
using System.Data;
using System.Data.Common;
using System.Globalization;
using US.Payment.Core.BusinessDomainObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class AddPaymentStatusAction : USDBActionBase<int>
    {
        private USPPaymentStatusFileRow _paymentStatus = new USPPaymentStatusFileRow();
        public AddPaymentStatusAction(USPPaymentStatusFileRow paymentStatus)
        {
            _paymentStatus = paymentStatus;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int result = -1;
            try
            {
              
                string spName = "dbo.USExceGMSCCXAddPaymentStatus";
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@RecordType", DbType.String, _paymentStatus.RecordType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PID", DbType.String, _paymentStatus.PID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BNumber", DbType.String, _paymentStatus.BNumber));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CID", DbType.String, _paymentStatus.CID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TNumber", DbType.String, _paymentStatus.TNumber));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ReferenceID", DbType.String, _paymentStatus.ReferenceId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BatchID", DbType.String, _paymentStatus.BatchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DataSource", DbType.String, _paymentStatus.DataSource));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RemitDate", DbType.String, _paymentStatus.RemitDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@KID", DbType.String, _paymentStatus.KID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Decimal, IsValidDecimal(_paymentStatus.Amount)));
                command.ExecuteNonQuery();
                result = 1;
            }
            catch (Exception)
            {
                
                throw;
            }
            return result;
        }

        public static decimal IsValidDecimal(string val)
        {
            string tempval = string.Empty;
            string currentCul = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            if (currentCul.Equals("nb-NO"))
            {
                tempval = val.Replace(".", ",");
            }
            else
            {
                tempval = val;
            }
            try
            {
                decimal decVal = decimal.Parse(tempval);
                return decVal;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
