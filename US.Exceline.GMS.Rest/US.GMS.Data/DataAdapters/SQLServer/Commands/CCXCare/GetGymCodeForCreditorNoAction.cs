﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class GetGymCodeForCreditorNoAction : USDBActionBase<string>
    {
        private int _creditorNo = -1;
        public GetGymCodeForCreditorNoAction(int creditorNo)
        {
            _creditorNo = creditorNo;
        }
        protected override string Body(System.Data.Common.DbConnection connection)
        {
            string gymCode = string.Empty;
            string spName = "ExceWorkStationGetGymCodeForCreditorNo";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@CreditorNo", System.Data.DbType.Int32, _creditorNo));

                gymCode = Convert.ToString(command.ExecuteScalar());
                return gymCode;
            }
            catch (Exception )
            {
                throw;
            }
          
        }
    }
}
