﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{


    public class AddMemberSponsoringOrderLineAction : USDBActionBase<List<ContractItemDC>>
    {
        private InstallmentDC _installment = new InstallmentDC();

        public AddMemberSponsoringOrderLineAction(InstallmentDC installment)
        {
            _installment = installment;
        }

        protected override List<ContractItemDC> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipAddSponsoringOrderLine";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@OrderID", System.Data.DbType.Int32, _installment.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@discountAmount", System.Data.DbType.Int32, _installment.Discount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@sponsoredAmount", System.Data.DbType.Int32, _installment.SponsoredAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _installment.MemberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _installment.CreatedUser));
                command.Parameters.Add(DataAcessUtils.CreateParam("@NoOfVisits", DbType.Int32, _installment.NoOfVisits));
                command.Parameters.Add(DataAcessUtils.CreateParam("@text", DbType.String, _installment.Text));
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractID", DbType.Int32, _installment.MemberContractId));

                DbDataReader reader = command.ExecuteReader();

                List<ContractItemDC> AddOnList = new List<ContractItemDC>();
                while (reader.Read())
                {
                    ContractItemDC addon = new ContractItemDC();
                    addon.Id = Convert.ToInt32(reader["ItemId"]);
                    addon.Price = Convert.ToDecimal(reader["AddOnPrice"]);
                    addon.EmployeePrice = Convert.ToDecimal(reader["EmployeePrice"]);
                    addon.UnitPrice = Convert.ToDecimal(reader["Unitprice"]);
                    addon.ArticleId = Convert.ToInt32(reader["ArticleID"]);
                    addon.ItemName = reader["AddonName"].ToString();
                    addon.IsStartUpItem = Convert.ToBoolean(reader["IsStartUpItem"]);
                    addon.Quantity = Convert.ToInt32(reader["Quantity"]);
                    addon.StockLevel = Convert.ToInt32(reader["StockLevel"]);
                    addon.Discount = Convert.ToDecimal(reader["Discount"]);
                    addon.IsActivityArticle = Convert.ToBoolean(reader["IsActivityArticle"]);
                    addon.Description = Convert.ToString(reader["Description"]);
                    addon.Priority = Convert.ToInt32(reader["Priority"]);
                    addon.IsBookingArticle = Convert.ToBoolean(reader["IsBookingArticle"]);
                    addon.IsSponsored = Convert.ToBoolean(reader["IsSponsored"]);

                    if (addon.IsSponsored && addon.IsActivityArticle)
                        addon.IsReadOnly = true;
                    addon.IsSelected = true;
                    AddOnList.Add(addon);
                }

                reader.Close();
                return AddOnList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ContractItemDC> RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSManageMembershipAddSponsoringOrderLine";
            try
            {
                DbCommand command = new SqlCommand();
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.CommandText = spName;
                command.Parameters.Add(DataAcessUtils.CreateParam("@OrderID", System.Data.DbType.Int32, _installment.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@discountAmount", System.Data.DbType.Int32, _installment.Discount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@sponsoredAmount", System.Data.DbType.Int32, _installment.SponsoredAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _installment.MemberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _installment.CreatedUser));
                command.Parameters.Add(DataAcessUtils.CreateParam("@NoOfVisits", DbType.Int32, _installment.NoOfVisits));
                command.Parameters.Add(DataAcessUtils.CreateParam("@text", DbType.String, _installment.Text));
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractID", DbType.Int32, _installment.MemberContractId));
                DbDataReader reader = command.ExecuteReader();

                List<ContractItemDC> AddOnList = new List<ContractItemDC>();
                while (reader.Read())
                {
                    ContractItemDC addon = new ContractItemDC();
                    addon.Id = Convert.ToInt32(reader["ItemId"]);
                    addon.Price = Convert.ToDecimal(reader["AddOnPrice"]);
                    addon.EmployeePrice = Convert.ToDecimal(reader["EmployeePrice"]);
                    addon.UnitPrice = Convert.ToDecimal(reader["Unitprice"]);
                    addon.ArticleId = Convert.ToInt32(reader["ArticleID"]);
                    addon.ItemName = reader["AddonName"].ToString();
                    addon.IsStartUpItem = Convert.ToBoolean(reader["IsStartUpItem"]);
                    addon.Quantity = Convert.ToInt32(reader["Quantity"]);
                    addon.StockLevel = Convert.ToInt32(reader["StockLevel"]);
                    addon.Discount = Convert.ToDecimal(reader["Discount"]);
                    addon.IsActivityArticle = Convert.ToBoolean(reader["IsActivityArticle"]);
                    addon.Description = Convert.ToString(reader["Description"]);
                    addon.Priority = Convert.ToInt32(reader["Priority"]);
                    addon.IsBookingArticle = Convert.ToBoolean(reader["IsBookingArticle"]);
                    addon.IsSponsored = Convert.ToBoolean(reader["IsSponsored"]);

                    if (addon.IsSponsored && addon.IsActivityArticle)
                        addon.IsReadOnly = true;
                    addon.IsSelected = true;
                    AddOnList.Add(addon);
                }

                reader.Close();
                return AddOnList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

