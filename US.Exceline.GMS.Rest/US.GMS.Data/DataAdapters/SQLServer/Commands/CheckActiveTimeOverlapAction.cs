﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class CheckActiveTimeOverlapAction : USDBActionBase<int>
    {
        private ScheduleItemDC _scheduleItem;
        public CheckActiveTimeOverlapAction(ScheduleItemDC scheduleItem)
        {
            this._scheduleItem = scheduleItem;
        }

        
        protected override int Body(DbConnection connection)
        {
            bool _isOverlapped = false;
            int _overlappedStatus = 0;
            if (_scheduleItem.ActiveTimes != null && _scheduleItem.ActiveTimes.Count > 0)
            {
                const string storedProcedure = "USExceGMSCheckClassActiveTimeOverlap";
                try
                {
                    if (_scheduleItem.InstructorIdList != null && _scheduleItem.InstructorIdList.Count > 0)
                    {
                        foreach (var _insId in this._scheduleItem.InstructorIdList)
                        {
                            if (_isOverlapped)
                                break;
                            foreach (var activeTime in _scheduleItem.ActiveTimes)
                            {
                                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _insId));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleItemId", DbType.Int32, _scheduleItem.Id));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRoleType", DbType.String, "INS"));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeID", DbType.Int32, _scheduleItem.ActiveTimeID)); 
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.Date, activeTime.StartDate.Date));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.Date, activeTime.EndDate.Date));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startTime", DbType.DateTime, activeTime.StartDateTime));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endTime", DbType.DateTime, activeTime.EndDateTime));

                                DbParameter output = new SqlParameter();
                                output.DbType = DbType.Boolean;
                                output.ParameterName = "@OUTPUTVAL";
                                output.Direction = ParameterDirection.Output;
                                cmd.Parameters.Add(output);
                                cmd.ExecuteNonQuery();

                                _isOverlapped = Convert.ToBoolean(output.Value);
                                if (_isOverlapped)
                                {
                                    _overlappedStatus = -1;
                                    break;
                                }
                            }
                        }
                    }
                    if (_overlappedStatus != -1)
                    {
                        if (_scheduleItem.ResourceIdList != null && _scheduleItem.ResourceIdList.Count > 0)
                        {
                            foreach (var _resId in this._scheduleItem.ResourceIdList)
                            {
                                if (_isOverlapped)
                                    break;
                                foreach (var activeTime in _scheduleItem.ActiveTimes)
                                {
                                    DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _resId));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleItemId", DbType.Int32, _scheduleItem.Id));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRoleType", DbType.String, "RES"));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeID", DbType.Int32, _scheduleItem.ActiveTimeID));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.Date, activeTime.StartDate.Date));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.Date, activeTime.EndDate.Date));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@startTime", DbType.DateTime, activeTime.StartDateTime));
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@endTime", DbType.DateTime, activeTime.EndDateTime));

                                    DbParameter output = new SqlParameter();
                                    output.DbType = DbType.Boolean;
                                    output.ParameterName = "@OUTPUTVAL";
                                    output.Direction = ParameterDirection.Output;
                                    cmd.Parameters.Add(output);
                                    cmd.ExecuteNonQuery();

                                    _isOverlapped = Convert.ToBoolean(output.Value);
                                    if (_isOverlapped)
                                    {
                                        _overlappedStatus = -2;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return _overlappedStatus;
        }
    }
}
