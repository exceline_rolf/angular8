﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.Payment.Core.BusinessDomainObjects;
using System.Data.Common;
using US.GMS.Core.SystemObjects;
using US.Payment.Core.Enums;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetClaimInfoForPaymentAction : USDBActionBase<IUSPClaim>
    {
        private int _memberId = -1;
        private int _branchId = -1;
        private PaymentTypes _paymentType = PaymentTypes.CLASS;
        private bool _isMember = true;
        public GetClaimInfoForPaymentAction(int memberId, int branchId, PaymentTypes paymentType, bool isMember)
        {
            _memberId = memberId;
            _branchId = branchId;
            _paymentType = paymentType;
            _isMember = isMember;
        }

        protected override IUSPClaim Body(System.Data.Common.DbConnection connection)
        {
            string spName = string.Empty;
            IUSPClaim claim = new USPClaim();
            IUSPDebtor debtor = new USPDebtor();
            IUSPCreditor creditor = new USPCreditor();
            IUSPAddress debtorAddress = new USPAddress();
            if (_memberId == -1)
                spName = "USExceGMSGetClaimInfoForDefaultUser";
            else
                spName = "USExceGMSGetClaimInfoForPayment";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberID", System.Data.DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchID", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentType", System.Data.DbType.String, _paymentType.ToString()));
                command.Parameters.Add(DataAcessUtils.CreateParam("@isMember", System.Data.DbType.Boolean, _isMember));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    debtor.DebtorEntityID = (reader["DebtorEntNo"] != DBNull.Value)? Convert.ToInt32(reader["DebtorEntNo"]):0;
                    debtor.DebtorcustumerID = Convert.ToString(reader["CustId"]);
                    debtor.DebtorFirstName = Convert.ToString(reader["FirstName"]);
                    debtor.DebtorSecondName = Convert.ToString(reader["LastName"]);
                    debtor.DebtorEntityRoleID = (reader["DebtorEntityRoleID"] != DBNull.Value)? Convert.ToInt32(reader["DebtorEntityRoleID"]):0;
                    
                    if(reader["DebtorBirthDay"] != DBNull.Value)
                      debtor.DebtorBirthDay = Convert.ToDateTime(reader["DebtorBirthDay"]).ToString("MM-dd-yyyy");

                    debtorAddress.Address1 = Convert.ToString(reader["Addr1"]);
                    debtorAddress.Address2 = Convert.ToString(reader["Addr2"]);
                    debtorAddress.Address3 = Convert.ToString(reader["Addr3"]);
                    debtorAddress.Email = Convert.ToString(reader["Email"]);
                    debtorAddress.TelHome = Convert.ToString(reader["TelHome"]);
                    debtorAddress.TelMobile = Convert.ToString(reader["TelMobile"]);
                    debtorAddress.TelWork = Convert.ToString(reader["TelWork"]);

                    debtor.DebtorAddressList.Add(debtorAddress);

                    creditor.CreditorInkassoID = Convert.ToString(reader["CreditorInkassoID"]);
                    creditor.CreditorName = Convert.ToString(reader["CreditorName"]);
                    creditor.CreditorAccountNo = Convert.ToString(reader["CreditorAccountNo"]);
                    if (reader["CreditorRoleId"] != DBNull.Value)
                      creditor.CreditorEntityRoleID = Convert.ToInt32(reader["CreditorRoleId"]);

                    if (reader["InvoiceType"] != DBNull.Value)
                    claim.InvoiceTypeId = Convert.ToInt32(reader["InvoiceType"]);

                    claim.InvoiceRef = Convert.ToString(reader["InvoiceRef"]);
                    claim.Group = Convert.ToString(reader["GroupId"]);
                    claim.InvoiceNumber = Convert.ToString(reader["InvoiceNumber"]);

                    claim.InvoiceType = GetInvoiceType(_paymentType);
                    claim.BranchNumber = _branchId.ToString();
                    claim.Debtor = debtor;
                    claim.Creditor = creditor; 
                }
                return claim;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private InvoiceTypes GetInvoiceType(PaymentTypes paymentType)
        {
            switch (paymentType)
            {
                case PaymentTypes.CLASS:
                    return InvoiceTypes.ClassInvoice;

                case PaymentTypes.DROPINVISIT:
                    return InvoiceTypes.DRPInvoice;

                case PaymentTypes.SHOP:
                    return InvoiceTypes.ShopInvoice;

                default:
                        return InvoiceTypes.Invoice;
            }
        }
    }
}
