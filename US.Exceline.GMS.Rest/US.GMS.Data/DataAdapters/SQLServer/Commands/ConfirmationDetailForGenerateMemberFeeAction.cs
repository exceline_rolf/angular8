﻿using System;
using System.Collections.Generic;
using System.Data;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class ConfirmationDetailForGenerateMemberFeeAction : USDBActionBase<List<string>>
    {
        private List<int> _gyms = new List<int>();
        private int _month = -1;

        public ConfirmationDetailForGenerateMemberFeeAction(List<int> gyms, int month)
        {
            _gyms = gyms;
            _month = month;
        }

        protected override List<string> Body(System.Data.Common.DbConnection connection)
        {
            const string spName = "ExceGMSConfirmationDetailForGenerateMemberFee";
            var result = new List<string>();
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Month", DbType.Int32, _month));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Branches", SqlDbType.Structured, GetIds(_gyms)));
                
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(reader["CountOfMemberFee"].ToString());
                    result.Add(reader["TotalAmount"].ToString());
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private DataTable GetIds(List<int> gyms)
        {
            DataTable branches = new DataTable();
            DataColumn col = new DataColumn("ID", typeof(Int32));
            branches.Columns.Add(col);
            foreach (var id in gyms)
            {
                branches.Rows.Add(id);
            }
            return branches;
        }

    }
}
