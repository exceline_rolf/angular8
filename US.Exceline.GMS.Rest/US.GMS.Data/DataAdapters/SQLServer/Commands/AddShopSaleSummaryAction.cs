﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddShopSaleSummaryAction : USDBActionBase<int>
    {
        private string _installmentData = string.Empty;
        private string _saleData = string.Empty;
        private string _paymentData = string.Empty;
        private int _memberBranchId = -1;
        private int _userBranchId = -1;
        private string _role = string.Empty;
        private string _message = string.Empty;
        private int _tryNumber = 0;
        private int _installmentId = -1;

        public AddShopSaleSummaryAction(string installmentData, string saleData, string paymentData, int memberBranchId, int userBranchId, string role, int tryNumber = 0, int installmentId = -1, string message = "")
        {
            _installmentData = installmentData;
            _saleData = saleData;
            _paymentData = paymentData;
            _memberBranchId = memberBranchId;
            _userBranchId = userBranchId;
            _role = role;
            _message = message;
            _tryNumber = tryNumber;
            _installmentId = installmentId;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSShopAddSaleSummary";
            int Id = -1;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentData", System.Data.DbType.String, _installmentData));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SaleData", System.Data.DbType.String, _saleData));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentData", System.Data.DbType.String, _paymentData));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberBranchId", System.Data.DbType.Int32, _memberBranchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@userBranchId", System.Data.DbType.Int32, _userBranchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Role", System.Data.DbType.String, _role));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Message", System.Data.DbType.String, _message));
                command.Parameters.Add(DataAcessUtils.CreateParam("@tryNumber", System.Data.DbType.Int32, _tryNumber));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentId", System.Data.DbType.Int32, _installmentId));


                Id = Convert.ToInt32(command.ExecuteScalar());
                return Id;
            }
            catch
            {
                return -1;
            }
        }
    }
}
