﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : "5/8/2012 16:04:15
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateActiveTimeAction : USDBActionBase<bool>
    {
        private EntityActiveTimeDC _activeTime;
        private bool _isDeleted;
        private int _branchId;
        private string _user = string.Empty;
        public UpdateActiveTimeAction(EntityActiveTimeDC activeTime,int branchId, bool isDelete,string user)
        {
            _activeTime = activeTime;
            _isDeleted = isDelete;
            _branchId = branchId;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSUpdateScheduleItemActiveTime";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", System.Data.DbType.Int32, _activeTime.ScheduleId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeId", System.Data.DbType.Int32, _activeTime.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", System.Data.DbType.DateTime, _activeTime.StartDateTime));
                command.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", System.Data.DbType.DateTime, _activeTime.EndDateTime));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsDeleted", System.Data.DbType.Boolean, _isDeleted));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
