﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetVistsByDateAction : USDBActionBase<List<EntityVisitDC>>
    {
        private int _branchId;
        private string _system;
        private DateTime _selectedDate;
        private readonly string _user = string.Empty;

        public GetVistsByDateAction(DateTime selectedDate, int branchId, string systemName, string user)
        {
            _branchId = branchId;
            _system = systemName;
            _selectedDate = selectedDate;
            _user = user;
        }

        protected override List<EntityVisitDC> Body(System.Data.Common.DbConnection connection)
        {
            List<EntityVisitDC> _visitList = new List<EntityVisitDC>();

            try
            {
                string storedProcedure = "USExceGMSManageMembershipGetMemberVisits";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@system", System.Data.DbType.String, _system));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@visitDate", System.Data.DbType.DateTime, _selectedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    EntityVisitDC visit = new EntityVisitDC();
                    visit.EntityId = Convert.ToInt32(reader["MemberId"]); ;
                    visit.InTime = Convert.ToDateTime(reader["InTime"]);
                    visit.IntegrationUserId = Convert.ToString(reader["UserId"]);
                    _visitList.Add(visit);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _visitList;
        }
    }
}