﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class DeleteClassTypeAction : USDBActionBase<int>
    {
        private int _classTypeId;

        public DeleteClassTypeAction(int classTypeId)
        {
            _classTypeId = classTypeId;
        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            string StoredProcedureName = "USExceGMSAdminDeleteClassType";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassTypeId", DbType.String, _classTypeId));
                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@Result";
                outputPara.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara);
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(outputPara.Value);
            }
            catch (Exception ex)
            {
                result = -1;
                throw ex;
            }
            return result;
        }


    }
}
