﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetEntitiesByActiveTimeIdAction : USDBActionBase<List<EntityDC>>
    {
        private int _activeTimeId;
        private string _roleId = string.Empty;
        public GetEntitiesByActiveTimeIdAction(int activeTimeId, string roleId)
        {
            _activeTimeId = activeTimeId;
            _roleId = roleId;
        }

        protected override List<EntityDC> Body(DbConnection connection)
        {
            List<EntityDC> entityLst = new List<EntityDC>();
            string spName = "USExceGMSGetEntitiesByActiveTimeId";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeId", DbType.Int32, _activeTimeId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.String, _roleId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {

                    int id = Convert.ToInt32(reader["ID"]);
                    string name = reader["Name"].ToString();
                    entityLst.Add(new EntityDC()
                    {
                        Id = id,
                        DisplayName = name
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return entityLst;
        }
    }
}