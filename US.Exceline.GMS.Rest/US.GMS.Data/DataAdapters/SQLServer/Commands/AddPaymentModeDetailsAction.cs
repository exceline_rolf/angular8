﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddPaymentModeDetailsAction : USDBActionBase<bool>
    {
        private PaymentDetailDC _paymentDetail = null;
        private string _user = string.Empty;
        private int _branchId = -1;
        public AddPaymentModeDetailsAction(PaymentDetailDC paymentDetail, string user, int branchID)
        {
            _paymentDetail = paymentDetail;
            _user = user;
            _branchId = branchID;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, "USExceGMSAddPaymentModeDetails");
                command.Parameters.Add(DataAcessUtils.CreateParam("@paymentModes", SqlDbType.Structured, GetDataTable()));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", SqlDbType.Int, _branchId));

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            try
            {
                DbCommand command = new SqlCommand();
                command.CommandText = "USExceGMSAddPaymentModeDetails";
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@paymentModes", SqlDbType.Structured, GetDataTable()));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", SqlDbType.Int, _branchId));
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        private DataTable GetDataTable()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("PaymodeId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Amount", typeof(decimal)));
            dataTable.Columns.Add(new DataColumn("PaymentSourceId", typeof(string)));
            dataTable.Columns.Add(new DataColumn("PaymentSource", typeof(string)));
            dataTable.Columns.Add(new DataColumn("CreatedUser", typeof(string)));

            foreach (PayModeDC paymentMode in _paymentDetail.PayModes)
            {
                DataRow dataRow = dataTable.NewRow();
                dataRow["PaymodeId"] = paymentMode.PaymentTypeId;
                dataRow["Amount"] = paymentMode.Amount;
                dataRow["PaymentSourceId"] = _paymentDetail.PaymentSourceId;
                dataRow["PaymentSource"] = _paymentDetail.PaymentSource.ToString();
                dataRow["CreatedUser"] = _user;
                dataTable.Rows.Add(dataRow);
            }
            return dataTable;
        }
    }
}
