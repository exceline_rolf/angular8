﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymConnectionAction : USDBActionBase<string>
    {
        private string _gymCode = string.Empty;

        public GetGymConnectionAction(string gymCode)
        {
            _gymCode = gymCode;
        }
        protected override string Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceWorkStationGetGymConnection";
            string conection = string.Empty;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@GymCode", System.Data.DbType.String, _gymCode));
                conection = (string) command.ExecuteScalar();
                return conection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
