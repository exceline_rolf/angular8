﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/18/2012 09:26:32
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;
using US_DataAccess;
using System.Linq;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetArticlesSearchAction : USDBActionBase<List<ArticleDC>>
    {
        private string _keyWord = string.Empty;
        private int _branchId = -1;

        public GetArticlesSearchAction(int branchId, string keyWord)
        {
            _keyWord = keyWord;
            _branchId = branchId;
        }

        protected override List<ArticleDC> Body(DbConnection connection)
        {
            List<ArticleDC> articleList = new List<ArticleDC>();
            const string storedProcedureName = "GetArticlesSearch";
            decimal vatAmount = 0;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                if (!string.IsNullOrEmpty(_keyWord))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@searchKeyWord", DbType.String, _keyWord));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
               
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ArticleDC article = new ArticleDC();
                    article.Id = Convert.ToInt32(reader["ID"]);
                    article.ArticleSettingId = Convert.ToInt32(reader["ArticleSettingId"]);
                    if (article.ArticleSettingId > 0)
                        article.IsBelongToBranch = true;
                    article.ArticleNo = Convert.ToString(reader["ArticleNo"]);
                    article.Description = Convert.ToString(reader["Description"]);
                    article.SortCutKey = Convert.ToInt32(reader["ShortCutKey"]);
                    article.DefaultPrice = Convert.ToDecimal(reader["DefaultPrice"]);
                    article.EmployeePrice = Convert.ToDecimal(reader["EmployeePrice"]);
                    article.PurchasedPrice = Convert.ToDecimal(reader["PurchasedPrice"]);
                    if (reader["VatAmount"] != null)
                        vatAmount = Convert.ToDecimal(reader["VatAmount"]);
                    article.PurchasePriceWithoutVat = article.PurchasedPrice - vatAmount;

                    article.VatCodeID = Convert.ToInt32(reader["VATCodeID"]);
                    if (article.VatCodeID != 0 && article.VatCodeID != null)
                    {
                        article.VatCode = Convert.ToInt32(reader["VatCode"]);
                        article.VatCodeName = Convert.ToString(reader["VatCodeName"]);
                        article.VatRate = Convert.ToDecimal(reader["VatRate"]);
                    }
                    article.RevenueAccountId = Convert.ToInt32(reader["RevenueAccountID"]);
                    if (article.RevenueAccountId != 0 && article.RevenueAccountId != null)
                    {
                        article.RevenueAccountNo = Convert.ToString(reader["RevenueAccountNo"]);
                        article.RevenueAccountName = Convert.ToString(reader["RevenueAccountName"]);
                    }
                    article.ArticleType = Convert.ToString(reader["TypeCode"]);
                    article.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                    article.Category = Convert.ToString(reader["Category"]);
                    article.CategoryCode = Convert.ToString(reader["CategoryCode"]);
                    article.BranchId = Convert.ToInt32(reader["BranchId"]);
                    article.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    article.ActivityName = Convert.ToString(reader["ActivityName"]);
                    article.VenderId = Convert.ToInt32(reader["VendorId"]);
                    article.Color = Convert.ToString(reader["Color"]);
                    article.BarCode = Convert.ToString(reader["BarCode"]);
                    article.LedgerAccount = Convert.ToString(reader["LedgerAccount"]);
                    article.StockLevel = Convert.ToInt32(reader["StockLevel"]);
                    article.ReOrderLevel = Convert.ToInt32(reader["ReOrderLevel"]);
                    article.StockStatus = Convert.ToBoolean(reader["StockStatus"]);
                    article.IsNtstockStatus = !article.StockStatus;
                    article.VendorName = Convert.ToString(reader["vendorName"]);
                    article.Category = Convert.ToString(reader["Category"]);
                    article.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    article.Discount = Convert.ToInt32(reader["Discount"]);
                    article.NoOfMinutes = Convert.ToInt32(reader["NoOfMinutes"]);
                    article.VatStatus = Convert.ToInt32(reader["VatStatus"]);
                    article.UnitId = Convert.ToInt32(reader["UnitId"]);
                    article.IsPunchCard = Convert.ToBoolean(reader["PunchCard"]);
                    article.IsContractBooking = Convert.ToBoolean(reader["ContractBooking"]);
                    article.ObsoleteStatus = Convert.ToBoolean(reader["Obsolete"]);
                    article.IsVoucher = Convert.ToBoolean(reader["Voucher"]);
                    article.IsVatAdded = Convert.ToBoolean(reader["IsVatAdded"]);
                    string branchList = reader["BranchList"].ToString().Trim(',');

                    if (reader["VatAmount"] != null)
                        vatAmount = Convert.ToDecimal(reader["VatAmount"]);
                    article.PurchasePriceWithoutVat = article.PurchasedPrice - vatAmount;

                    branchList.Split(',').ToList().ForEach(x =>
                    {
                        if (article.BranchIdList == null) article.BranchIdList = new List<int>();
                        if (x != null) article.BranchIdList.Add(Convert.ToInt32(x));
                    });
                    articleList.Add(article);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return articleList;
        }
    }
}
