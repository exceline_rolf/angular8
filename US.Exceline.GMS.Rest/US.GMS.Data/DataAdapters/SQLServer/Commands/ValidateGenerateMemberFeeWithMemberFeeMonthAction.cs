﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateGenerateMemberFeeWithMemberFeeMonthAction : USDBActionBase<bool>
    {
        private List<int> _gyms;
        private string _user;

        public ValidateGenerateMemberFeeWithMemberFeeMonthAction(List<int> gyms, string user)
        {
            _gyms = gyms;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSValidateGenerateMemberFeeWithMemberFeeMonth";
            bool result;
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BrancheList", SqlDbType.Structured, GetIds(_gyms)));
                var param = new SqlParameter
                    {
                        ParameterName = "@outId",
                        SqlDbType = SqlDbType.Bit,
                        Direction = ParameterDirection.Output
                    };
                command.Parameters.Add(param);
                command.ExecuteNonQuery();

                result = Convert.ToBoolean(param.Value);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private DataTable GetIds(List<int> gyms)
        {
            DataTable branches = new DataTable();
            DataColumn col = new DataColumn("ID", typeof(Int32));
            branches.Columns.Add(col);
            foreach (var id in gyms)
            {
                branches.Rows.Add(id);
            }
            return branches;
        }
    }
}
