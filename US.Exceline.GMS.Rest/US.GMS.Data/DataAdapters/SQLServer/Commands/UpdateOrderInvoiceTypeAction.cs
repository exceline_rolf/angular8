﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateOrderInvoiceTypeAction : USDBActionBase<bool>
    {
        private int _orderInvoiceType = -1;
        private decimal _sponsoredAmount = 0.00M;
        private decimal _discountAmounnt = 0.00M;
        private int _orderId = -1;
        private int _arItemNo = -1;
        public UpdateOrderInvoiceTypeAction(int orderid, decimal sponsoredAmount, decimal discountAmounnt, int orderInvoiceType, int arItemNo)
        {
            _orderInvoiceType = orderInvoiceType;
            _sponsoredAmount = sponsoredAmount;
            _discountAmounnt = discountAmounnt;
            _orderId = orderid;
            _arItemNo = arItemNo;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipUpdateOrderInvoiceType";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@OrderID", System.Data.DbType.Int32, _orderId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@OrdderInvoiceType", System.Data.DbType.Int32, _orderInvoiceType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArITemNo", System.Data.DbType.Int32, _arItemNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@discountAmount", System.Data.DbType.Int32, _discountAmounnt));
                command.Parameters.Add(DataAcessUtils.CreateParam("@sponsoredAmount", System.Data.DbType.Int32, _sponsoredAmount));
                command.ExecuteNonQuery();
                return true;
            }
            catch(Exception ex)
            {
               throw  ex;
            }
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSManageMembershipUpdateOrderInvoiceType";
            try
            {
                DbCommand command = new SqlCommand();
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.CommandText = spName;
                command.Parameters.Add(DataAcessUtils.CreateParam("@OrderID", System.Data.DbType.Int32, _orderId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@discountAmount", System.Data.DbType.Int32, _discountAmounnt));
                command.Parameters.Add(DataAcessUtils.CreateParam("@sponsoredAmount", System.Data.DbType.Int32, _sponsoredAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@OrdderInvoiceType", System.Data.DbType.Int32, _orderInvoiceType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArITemNo", System.Data.DbType.Int32, _arItemNo));

                command.ExecuteNonQuery();
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

    }
}
