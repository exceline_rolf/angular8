﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data.Common;
using System.Collections.ObjectModel;
using System.Data;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{

    public class GetEntityScheduleAction : USDBActionBase<ScheduleDC>
    {
        private int _entityId;
        private string _entityRole = string.Empty;



        public GetEntityScheduleAction(int entityId, string entityRoleType)
        {
            _entityId = entityId;
            _entityRole = entityRoleType;
            //  _parentId = parentId;
        }

        protected override ScheduleDC Body(DbConnection connection)
        {
            ScheduleDC entitySchedule = new ScheduleDC();
            entitySchedule.SheduleItemList = new ObservableCollection<ScheduleItemDC>();

            string StoredProcedureName = "USExceGMSGetEntitySchedule";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId ", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRole", DbType.String, _entityRole));


                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ScheduleItemDC scheduleItem = new ScheduleItemDC();
                    //entitySchedule.Id = Convert.ToInt32(reader["ID"]);
                    //entitySchedule.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    //entitySchedule.EndDate = Convert.ToDateTime(reader["EndDate"]);
                   // string rowid = reader["RowID"].ToString();
                    //if (string.IsNullOrEmpty(rowid))
                    //{
                        
                    //}
                    int ParentId = Convert.ToInt32(reader["ParentId"]);
                    if (ParentId != -1)
                    {
                        scheduleItem.IsParentable = false;
                    }
                    else
                    {
                        scheduleItem.IsParentable = true;
                    }
                    scheduleItem.Id = Convert.ToInt32(reader["ID"]);


                    string occurrence = reader["Occurrence"].ToString();
                    if (!string.IsNullOrEmpty(occurrence.Trim()))
                    {
                        //  ordineryMember.Gender = (Gender)Enum.Parse(typeof(Gender), gender);
                        scheduleItem.Occurrence = (ScheduleTypes)Enum.Parse(typeof(ScheduleTypes), occurrence, true);
                    }

                    scheduleItem.Day = reader["Day"].ToString();
                    scheduleItem.Week = Convert.ToInt32(reader["Week"]);
                    scheduleItem.Month = reader["Month"].ToString();
                    scheduleItem.Year = Convert.ToInt32(reader["Year"]);
                    scheduleItem.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    scheduleItem.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    scheduleItem.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    scheduleItem.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    scheduleItem.CreatedDate = Convert.ToDateTime(reader["CreatedDateTime"]);
                    scheduleItem.LastModifiedDate = Convert.ToDateTime(reader["LastModifiedDateTime"]);
                    scheduleItem.CreatedUser = reader["CreatedUser"].ToString();
                    scheduleItem.LastModifiedUser = reader["LastModifiedUser"].ToString();
                    scheduleItem.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    scheduleItem.IsFixed = Convert.ToBoolean(reader["IsFixed"]);
                    scheduleItem.LastActiveTimeGeneratedDate = Convert.ToDateTime(reader["LastActivetimeDate"]);

                    entitySchedule.SheduleItemList.Add(scheduleItem);
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return entitySchedule;
        }
    }
}
