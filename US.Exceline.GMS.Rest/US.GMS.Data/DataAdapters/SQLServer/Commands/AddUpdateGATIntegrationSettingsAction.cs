﻿// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : Unicornvwi
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddUpdateGATIntegrationSettingsAction : USDBActionBase<bool>
    {
        private readonly GymIntegrationSettingsDC _gymCompanyGatIntegrationSettingsDetail;
        private readonly int _branchId = -1;
        private DataTable _dataTable;

        public AddUpdateGATIntegrationSettingsAction(int branchId, GymIntegrationSettingsDC gymCompanyGatIntegrationSettingsDetail)
        {
            _branchId = branchId;
            _gymCompanyGatIntegrationSettingsDetail = gymCompanyGatIntegrationSettingsDetail;
            _dataTable = GetAccessProfileLst(_gymCompanyGatIntegrationSettingsDetail.AccessProfileList);
        }

        private DataTable GetAccessProfileLst(List<ExceAccessProfileDC> accessProfiles)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("AccessProfileId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("TerminalId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("DrawPunch", typeof(Boolean)));
            if (accessProfiles != null && accessProfiles.Any())
                foreach (var item in accessProfiles)
                {
                    DataRow dataTableRow = _dataTable.NewRow();
                    dataTableRow["AccessProfileId"] = item.Id;
                    dataTableRow["TerminalId"] = _gymCompanyGatIntegrationSettingsDetail.Id;
                    dataTableRow["DrawPunch"] = item.IsDrawPuncheSelect;
                    _dataTable.Rows.Add(dataTableRow);
                }
            return _dataTable;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "dbo.USExceGMSAdminAddUpdateGATIntegrationSettings";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.String, _gymCompanyGatIntegrationSettingsDetail.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TerminalName", DbType.String, _gymCompanyGatIntegrationSettingsDetail.TerminalName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Port", DbType.Int32, _gymCompanyGatIntegrationSettingsDetail.Port));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Location", DbType.String, _gymCompanyGatIntegrationSettingsDetail.Location));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TypeId", DbType.String, _gymCompanyGatIntegrationSettingsDetail.TerminalType.TypeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsSecondIdentification", DbType.Boolean, _gymCompanyGatIntegrationSettingsDetail.IsSecondIdentification));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsOnline", DbType.Boolean, _gymCompanyGatIntegrationSettingsDetail.IsOnline));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccessControlTypes", DbType.String, _gymCompanyGatIntegrationSettingsDetail.AccessContolTypes));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPaid", DbType.Boolean, _gymCompanyGatIntegrationSettingsDetail.IsPaid));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleNo", DbType.String, _gymCompanyGatIntegrationSettingsDetail.ArticleNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _gymCompanyGatIntegrationSettingsDetail.ActivityId));
                cmd.Parameters.Add(_dataTable != null
                                           ? DataAcessUtils.CreateParam("@ExceAccessProfileTerminal", SqlDbType.Structured,
                                                                        _dataTable)
                                           : DataAcessUtils.CreateParam("@ExceAccessProfileTerminal", SqlDbType.Structured, null));

                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                var outputId = Convert.ToInt32(para1.Value);
                if (outputId > 0)
                {
                    return true;
                }
                return false;
            }

            catch
            {
                throw;
            }
        }
    }
}
