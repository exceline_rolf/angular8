﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveGymOpenTimeAction : USDBActionBase<bool>
    {
        #region private variable
        private readonly int _branchId;
        private bool _returnValue;
        private readonly List<GymOpenTimeDC> _gymOpenTimesList = new List<GymOpenTimeDC>();
        #endregion

        #region Constructor
        public SaveGymOpenTimeAction(int branchId, List<GymOpenTimeDC> gymOpenTimesList)
        {
            _branchId = branchId;
            _gymOpenTimesList = gymOpenTimesList;
        }
        #endregion

        #region Body
        protected override bool Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSAdminUpdateGymOpenTime";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                if (cmd != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@GymOpenTimes", SqlDbType.Structured, GetTimeTable(_gymOpenTimesList)));

                    cmd.ExecuteNonQuery();
                }
                _returnValue = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _returnValue;
        }
        #endregion

        #region GetTimeTable
        private DataTable GetTimeTable(List<GymOpenTimeDC> gymOpenTimes)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("FromTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("ToTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("Monday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Tuesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Wednesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Thursday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Friday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Saturday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Sunday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("BranchId", typeof(int)));

            foreach (GymOpenTimeDC openTime in gymOpenTimes)
            {
                DataRow row = dataTable.NewRow();
                row["FromTime"] = openTime.StartTime;
                row["ToTime"] = openTime.EndTime;
                row["Monday"] = openTime.IsMonday;
                row["Tuesday"] = openTime.IsTuesday;
                row["Wednesday"] = openTime.IsWednesday;
                row["Thursday"] = openTime.IsThursday;
                row["Friday"] = openTime.IsFriday;
                row["Saturday"] = openTime.IsSaturday;
                row["Sunday"] = openTime.IsSunday;
                row["BranchId"] = openTime.BranchId;
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }
        #endregion
    }
}
