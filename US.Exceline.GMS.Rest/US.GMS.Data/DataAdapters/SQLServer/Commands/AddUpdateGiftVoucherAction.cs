﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class AddUpdateGiftVoucherAction : USDBActionBase<int>
    {
        private readonly int _branchId = -1;
        private readonly VoucherDC _voucher;

        public AddUpdateGiftVoucherAction(int branchId, VoucherDC voucher)
        {
            _branchId = branchId;
            _voucher = voucher;
        }

        protected override int Body(DbConnection connection)
        {
            int voucherId;
            const string storedProcedureName = "USExceGMSShopAddUpdateGiftVoucher";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _voucher.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNo", DbType.Int32, _voucher.VoucherNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherPrice", DbType.Decimal, _voucher.VoucherPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaidAmount", DbType.Decimal, _voucher.PayableAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExpiryDate", DbType.DateTime, _voucher.ExpiryDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CustomerNo", DbType.Int32, _voucher.CustomerNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RemainingAmount", DbType.Decimal, _voucher.RemainingAmount));
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@outId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
                voucherId = Convert.ToInt32(param.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return voucherId;
        }
    }
}
