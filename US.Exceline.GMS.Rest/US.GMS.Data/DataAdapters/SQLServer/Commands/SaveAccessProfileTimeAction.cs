﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class SaveAccessProfileTimeAction : USDBActionBase<bool>
    {
        #region private variable
        private readonly List<ExceAccessProfileTimeDC> _profileTimeList;
        private readonly DbTransaction _transaction;
        //private DateTime _from;
        //private DateTime _to;
        private readonly int _savedMsgId;
        #endregion

        #region Constructor
        public SaveAccessProfileTimeAction(List<ExceAccessProfileTimeDC> profileTimeList, int savedMsgId, DbTransaction transaction)
        {
            _profileTimeList = profileTimeList;
            _transaction = transaction;
            _savedMsgId = savedMsgId;
        }

        //public SaveAccessProfileTimeAction(List<ExceAccessProfileTimeDC> profileTimeList, DbTransaction transaction)
        //{
        //    _profileTimeList = profileTimeList;
        //    _transaction = transaction;
        //}
        #endregion

        #region Body
        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSSaveAccessProfile";
            try
            {
                //_from = new DateTime();
                //_from = _profileTime.FromTime.GetValueOrDefault();

                //_to = new DateTime();
                //_to = _profileTime.ToTime.GetValueOrDefault();

                DbCommand cmd1 = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd1.Connection = _transaction.Connection;
                cmd1.Transaction = _transaction;
                cmd1.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _profileTimeList[0].Id));
                cmd1.Parameters.Add(DataAcessUtils.CreateParam("@GymAccessProfileTime", SqlDbType.Structured, GetAccessProfileTime(_profileTimeList)));
                cmd1.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetAccessProfileTimeTable
        private DataTable GetAccessProfileTime(List<ExceAccessProfileTimeDC> profileTimeList)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("AccessProfileId", typeof(Int32)));
            dataTable.Columns.Add(new DataColumn("FromTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("ToTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("Monday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Tuesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Wednesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Thursday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Friday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Saturday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Sunday", typeof(bool)));

            foreach (ExceAccessProfileTimeDC profileTime in profileTimeList)
            {
                DataRow row = dataTable.NewRow();
                row["AccessProfileId"] = _savedMsgId;
                row["FromTime"] = profileTime.FromTime;
                row["ToTime"] = profileTime.ToTime;
                row["Monday"] = profileTime.Monday;
                row["Tuesday"] = profileTime.Tuesday;
                row["Wednesday"] = profileTime.Wednesday;
                row["Thursday"] = profileTime.Thursday;
                row["Friday"] = profileTime.Friday;
                row["Saturday"] = profileTime.Saturday;
                row["Sunday"] = profileTime.Sunday;
                dataTable.Rows.Add(row);
            }

            //  cmd1.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _profileTime.Id));
            //    cmd1.Parameters.Add(DataAcessUtils.CreateParam("@accProfId", DbType.Int32, _profileTime.AccessProfileId));
            //    cmd1.Parameters.Add(DataAcessUtils.CreateParam("@FromTime", DbType.String, from.ToShortTimeString()));
            //    cmd1.Parameters.Add(DataAcessUtils.CreateParam("@ToTime", DbType.String, to.ToShortTimeString()));
            //    cmd1.Parameters.Add(DataAcessUtils.CreateParam("@Monday", DbType.Boolean, _profileTime.Monday));
            //    cmd1.Parameters.Add(DataAcessUtils.CreateParam("@Tuesday", DbType.Boolean, _profileTime.Tuesday));
            //    cmd1.Parameters.Add(DataAcessUtils.CreateParam("@Wednesday", DbType.Boolean, _profileTime.Wednesday));
            //    cmd1.Parameters.Add(DataAcessUtils.CreateParam("@Thursday", DbType.Boolean, _profileTime.Thursday));
            //    cmd1.Parameters.Add(DataAcessUtils.CreateParam("@Friday", DbType.Boolean, _profileTime.Friday));
            //    cmd1.Parameters.Add(DataAcessUtils.CreateParam("@Saturday", DbType.Boolean, _profileTime.Saturday));
            //    cmd1.Parameters.Add(DataAcessUtils.CreateParam("@Sunday", DbType.Boolean, _profileTime.Sunday));
            return dataTable;
        }
        #endregion
    }
}
