﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "27/06/2012 10:09:32
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetActivitiesForCommonBookingAction : USDBActionBase<List<ActivityDC>>
    {
        private int _entityId;
        private string _entityType;
        private int _memberId;

        public GetActivitiesForCommonBookingAction(int entityId, string entityType, int memberId)
        {
            _entityId = entityId;
            _entityType = entityType;
            _memberId = memberId;
        }

        protected override List<ActivityDC> Body(DbConnection connection)
        {
            List<ActivityDC> activityList = new List<ActivityDC>();
            string storedProcedure = "USExceGMSGetActivitiesForCommonBooking ";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityType", DbType.String, _entityType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ActivityDC activity = new ActivityDC();
                    activity.Id = Convert.ToInt32(reader["ID"]);
                    activity.Name = reader["Name"].ToString();
                    activity.Code = reader["Code"].ToString();
                    activity.BranchId = Convert.ToInt32(reader["BranchId"]);
                    activity.IsActive = Convert.ToBoolean(reader["IsActive"]);
                    activityList.Add(activity);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return activityList;
        }
    }
}
