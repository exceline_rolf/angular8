﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetEmployeeAction : USDBActionBase<List<GymEmployeeDC>>
    {
        private readonly int _aciveStatus;
        public GetEmployeeAction(int aciveStatus)
        {
            _aciveStatus = aciveStatus;
        }

        protected override List<GymEmployeeDC> Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSGetGymEmployees";
            var gymEmployeeList = new List<GymEmployeeDC>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeState", DbType.Int32, _aciveStatus));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var gymEmployee = new GymEmployeeDC();
                    gymEmployee.Id = Convert.ToInt32(reader["Id"]);
                    gymEmployee.Name = reader["Name"].ToString();
                    gymEmployee.FirstName = reader["FirstName"].ToString();
                    gymEmployee.LastName = reader["LastName"].ToString();
                    gymEmployee.CustId = reader["CustId"].ToString();

                    gymEmployeeList.Add(gymEmployee);
                }
                reader.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return gymEmployeeList;
        }

    }
}
