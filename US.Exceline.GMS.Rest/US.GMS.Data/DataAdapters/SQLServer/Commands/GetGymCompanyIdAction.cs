﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymCompanyIdAction : USDBActionBase<int>
    {
        private string _gymCode = string.Empty;
        public GetGymCompanyIdAction(string gymCode)
        {
            _gymCode = gymCode;
        }
        protected override int Body(System.Data.Common.DbConnection connection)
        {
            const string spName = "USExceGMSGetGymCompanyId";
            DbDataReader reader = null;
            int result = -1;
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@GymCode", DbType.String, _gymCode));
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    result = Convert.ToInt32(reader["GymID"]);

                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
