﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data.SqlClient;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateInstallmentBalanceForInvoicePaymentsAction : USDBActionBase<bool>
    {
        private string _invoiceNo = string.Empty;
        private decimal _paymentAmount = 0;
        public UpdateInstallmentBalanceForInvoicePaymentsAction(string invoiceNo, decimal paymentAmount)
        {
            _invoiceNo = invoiceNo;
            _paymentAmount = paymentAmount;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            return true;
        }

        public bool RunOntransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSUpdateInstallmentBalanceForInvoicePayment";
            try
            {
                DbCommand command = new SqlCommand();
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.CommandText = spName;
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@InvoiceNo", _invoiceNo));
                command.Parameters.Add(new SqlParameter("@PaymentAmount", _paymentAmount));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
