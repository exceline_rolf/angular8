﻿using System;
using System.Data.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateSettingForUserRoutineAction : USDBActionBase<bool>
    {
        private readonly string _key = string.Empty;
        private readonly string _value = string.Empty;
        private readonly string _user = string.Empty;

        public UpdateSettingForUserRoutineAction(string key, string value, string user)
        {
            _key = key;
            _value = value;
            _user = user;
        }

        protected override bool Body(DbConnection con)
        {
            try
            {
                const string spName = "USExceGMSUpdateSettingForUserRoutine";
                var command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Key", System.Data.DbType.String, _key));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Value", System.Data.DbType.String, _value));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}
