﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.WorkStation
{
    public class GetGymCodesAction : USDBActionBase<List<string>>
    {
        protected override List<string> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceWorkStationGetGymCodes";
            DbDataReader reader = null;
            List<string> gymCodes = new List<string>();
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    gymCodes.Add(Convert.ToString(reader["GymCode"]));
                }   
            }
            catch (Exception)
            {
                throw;
            }
            return gymCodes;
        }
    }
}
