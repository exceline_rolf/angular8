﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class GetNextGiftVoucherNoAction : USDBActionBase<int>
    {
        private readonly int _branchId;

        public GetNextGiftVoucherNoAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override int Body(DbConnection connection)
        {
            int nextGiftVoucherNo;
            const string storedProcedureName = "USExceGMSShopGetNextGiftVoucherNo";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                nextGiftVoucherNo = Convert.ToInt32(cmd.ExecuteScalar());
                nextGiftVoucherNo += 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return nextGiftVoucherNo;
        }
    }
}
