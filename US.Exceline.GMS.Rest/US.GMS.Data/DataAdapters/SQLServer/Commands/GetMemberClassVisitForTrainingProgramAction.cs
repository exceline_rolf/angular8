﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberClassVisitForTrainingProgramAction : USDBActionBase<List<TrainingProgramClassVisitDetailDC>>
    {
        private readonly int _branchId;
        private readonly string _systemName;
        private readonly DateTime _lastUpdatedDateTime;

        public GetMemberClassVisitForTrainingProgramAction(int branchId, string systemName, DateTime lastUpdatedDateTime)
        {
            _branchId = branchId;
            _systemName = systemName;
            _lastUpdatedDateTime = lastUpdatedDateTime;
        }

        protected override List<TrainingProgramClassVisitDetailDC> Body(DbConnection connection)
        {
            List<TrainingProgramClassVisitDetailDC> trainingProgramClassVisitDetailList = new List<TrainingProgramClassVisitDetailDC>();

            const string storedProcedureName = "USExceGMSGetMemberClassVisitDetailForTrainingProgram";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SystemName", DbType.String, _systemName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastUpdatedDateTime", DbType.DateTime, _lastUpdatedDateTime));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    TrainingProgramClassVisitDetailDC trainingProgramClassVisitDetail = new TrainingProgramClassVisitDetailDC();
                    MemberIntegrationSettingDC memberIntegrationSetting = new MemberIntegrationSettingDC();

                    memberIntegrationSetting.UserId = Convert.ToString(reader["UserId"]);
                    trainingProgramClassVisitDetail.MemberIntegrationSetting = memberIntegrationSetting;
                    if (!string.IsNullOrEmpty(reader["ClassNumber"].ToString()))
                    {
                        trainingProgramClassVisitDetail.ClassNo = Convert.ToInt32(reader["ClassNumber"]);
                    }
                    if (!string.IsNullOrEmpty(reader["StartDate"].ToString()))
                    {
                        trainingProgramClassVisitDetail.StartDateTime = Convert.ToDateTime(reader["StartDate"]);
                    }
                    if (!string.IsNullOrEmpty(reader["Duration"].ToString()))
                    {
                        trainingProgramClassVisitDetail.Duration = Convert.ToInt32(reader["Duration"]);
                    }
                    if (!string.IsNullOrEmpty(reader["CurrentUpdatedDateTime"].ToString()))
                    {
                        trainingProgramClassVisitDetail.CurrentUpdatedDateTime = Convert.ToDateTime(reader["CurrentUpdatedDateTime"]);
                    }

                    trainingProgramClassVisitDetailList.Add(trainingProgramClassVisitDetail);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return trainingProgramClassVisitDetailList;
        }
    }
}

