﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateMobileAction : USDBActionBase<Tuple<int, string>>
    {
        private string _mobile = string.Empty;
        private string _mobilePrefix = string.Empty;
        public ValidateMobileAction(string mobile,string mobilePrefix)
        {
            _mobile = mobile;
            _mobilePrefix = mobilePrefix;
        }
        protected override Tuple<int, string> Body(DbConnection connection)
        {
            // Tuple<int, string> result;// = new Tuple<int, string>(-1, string.Empty);
            string spName = "USExceGMSValidateMobile";
            try
            {

                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Mobile", DbType.String, _mobile));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MobilePrefix", DbType.String, _mobilePrefix));

                DbParameter NameOutput = new SqlParameter();
                NameOutput.DbType = DbType.String;
                NameOutput.ParameterName = "@Name";
                NameOutput.Size = 500;
                NameOutput.Direction = ParameterDirection.Output;
                command.Parameters.Add(NameOutput);

                DbParameter MemberIdOutput = new SqlParameter();
                MemberIdOutput.DbType = DbType.Int32;
                MemberIdOutput.ParameterName = "@MemberId";
                MemberIdOutput.Size = 500;
                MemberIdOutput.Direction = ParameterDirection.Output;
                command.Parameters.Add(MemberIdOutput);

                command.ExecuteNonQuery();

                if (MemberIdOutput.Value == DBNull.Value || NameOutput.Value == DBNull.Value)
                {
                    Tuple<int, string> result = new Tuple<int, string>(-1, "OK");
                    return result;
                } else {
                    Tuple<int, string> result = new Tuple<int, string>(Convert.ToInt32(MemberIdOutput.Value), Convert.ToString(NameOutput.Value));
                    return result;
                }
            }
            catch (Exception)
            {
                // Tuple<int, string> result = new Tuple<int, string>(-1, string.Empty);
                // return result;
                throw;
            }
        }
    }
}
