﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetArticlesForContractAction : USDBActionBase<List<ArticleDC>>
    {
        private int _categoryID = -1;
        private int _brnachID = -1;

        public GetArticlesForContractAction(int categoryID, int branchID)
        {
            _categoryID = categoryID;
            _brnachID = branchID;
        }

        protected override List<ArticleDC> Body(DbConnection connection)
        {
            List<ArticleDC> _articleList = new List<ArticleDC>();
            string StoredProcedureName = "USExceGMSAdminGetArticlesForActivity";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryID", DbType.Int32, _categoryID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _brnachID));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ArticleDC article = new ArticleDC();
                    article.Category = reader["Activity"].ToString();
                    article.Id = Convert.ToInt32(reader["ArticleId"]);
                    article.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    article.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                    article.ArticleNo = reader["ArticleNo"].ToString();
                    article.Description = reader["Description"].ToString();
                    article.DefaultPrice = Convert.ToDecimal(reader["UnitPrice"]);
                    _articleList.Add(article);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _articleList;
        }
    }
}
