﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetInvoicePaymentSummaryAction : USDBActionBase<List<InvoicePaymentSummaryDC>>
    {
        private int _subCaseNo = -1;
        public GetInvoicePaymentSummaryAction(int sunCaseNO)
        {
            _subCaseNo = sunCaseNO;
        }
        protected override List<InvoicePaymentSummaryDC> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipGetInvoicePaymentSummary";
            DbDataReader reader = null;
            List<InvoicePaymentSummaryDC> payments = new List<InvoicePaymentSummaryDC>();
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@SubCaseNo", System.Data.DbType.Int32, _subCaseNo));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    InvoicePaymentSummaryDC pSummary = new InvoicePaymentSummaryDC();
                    pSummary.Id = Convert.ToInt32(reader["ARItemNo"]);
                    pSummary.PaymentAmount = Convert.ToDecimal(reader["Amount"]);
                    pSummary.PaymentDate = Convert.ToDateTime(reader["VoucherDate"]);
                    pSummary.PaymentRegDate = Convert.ToDateTime(reader["Regdate"]);
                    pSummary.Employee = Convert.ToString(reader["CreatedBy"]);
                    pSummary.Type = Convert.ToString(reader["PayementType"]);
                    payments.Add(pSummary);
                }

                if(reader != null)
                    reader.Close();
                return payments;
            }
            catch
            {
                if(reader != null)
                    reader.Close();
                throw;
            }
        }
    }
}
