﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class ActGetFileDataAction : USDBActionBase<List<String>>
    {
       public ActGetFileDataAction()
       {
           
       }
       protected override List<String> Body(DbConnection connection)
        {
            List<String> actorLst = new List<String>();
            const string storedProcedure = "USExceActGetFileData";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    String item = reader["ActorString"].ToString();
                    actorLst.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actorLst;
        }
    }
}
