﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SunBedAddShopOrderSaleAction:USDBActionBase<int>
    {
        private ShopSalesDC _salesDetails;
        private int _installmentId;
        private SunBedHelperObject _sunBedHelperObj;
        private decimal _amount;
        

        public SunBedAddShopOrderSaleAction(SunBedHelperObject sunbedHelperObject,int installmentId,ShopSalesDC salesDetails, decimal amount)
        {
            _salesDetails = salesDetails;
            _installmentId = installmentId;
            _sunBedHelperObj = sunbedHelperObject;
            _amount = amount;
        }
        protected override int Body(DbConnection connection)
        {
            return -1;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            int _transId = -1;
            try
            {
                AddSalesDetailsAction salesDetailAction = new AddSalesDetailsAction(_salesDetails, _sunBedHelperObj.LoggedbranchID, _sunBedHelperObj.User, -1, _installmentId, string.Empty);
                salesDetailAction.RunOnTransaction(transaction);

                ShopTransactionDC _shopTrans = new ShopTransactionDC();
                _shopTrans.BranchId = _sunBedHelperObj.LoggedbranchID;
                _shopTrans.CreatedUser = _sunBedHelperObj.User;
                _shopTrans.CreatedDate = DateTime.Now;
                _shopTrans.Mode = TransactionTypes.SHOPORDER;
                _shopTrans.Amount = _amount;
                _shopTrans.SalePointId = _salesDetails.SalesPointId;
                _shopTrans.EntityId = _salesDetails.EntitiyId;
                _shopTrans.EntityRoleType = _salesDetails.EntityRoleType;
                SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                _transId = shopTransactionAction.RunOnTransaction(transaction);

            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            return _transId;
        }
    }
}
