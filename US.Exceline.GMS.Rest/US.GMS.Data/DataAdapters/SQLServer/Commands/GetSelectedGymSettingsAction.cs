﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetSelectedGymSettingsAction : USDBActionBase<Dictionary<string, object>>
    {
        private  List<string> _gymSettingColumnList;
        private  int _branchId;
        private  bool _isGymSettings;
        private bool _isFromAccessControl;

        public GetSelectedGymSettingsAction(List<string> gymSettingColumnList,bool isGymSettings,int branchId, bool isFromAcccessControl)
        {
            _gymSettingColumnList = gymSettingColumnList;
            _branchId = branchId;
            _isGymSettings = isGymSettings;
            _isFromAccessControl = isFromAcccessControl;
        }

        protected override Dictionary<string, object> Body(DbConnection connection)
        {
            const string spName = "USExceGetSelectedGymSettings";
            Dictionary<string, object> gymSettingDictionary = new Dictionary<string, object>();
            string queryText = string.Empty;
            try
            {
                for (int i = 0; i < _gymSettingColumnList.Count; i++)
                {
                    if (i == (_gymSettingColumnList.Count - 1))
                        queryText += "ISNULL(" + _gymSettingColumnList[i] + ",0)";
                    else
                        queryText += "ISNULL("+_gymSettingColumnList[i] + ",0),";
                }
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@queryText", DbType.String, queryText));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.String, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsGymSettings", DbType.Boolean, _isGymSettings));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsFrmAccessCtrl", DbType.Boolean, _isFromAccessControl));
                DbDataReader reader = command.ExecuteReader();
                List<object> columnValueList = new List<object>();
                while (reader.Read())
                {
                    for (int j = 0; j < _gymSettingColumnList.Count; j++)
                    {
                        columnValueList.Add(reader.GetValue(j));

                    }
                }

                for (int i = 0; i < _gymSettingColumnList.Count; i++)
                {
                    gymSettingDictionary.Add(_gymSettingColumnList[i], columnValueList[i]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return gymSettingDictionary;
        }
    }
}
