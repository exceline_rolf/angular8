﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetCityForPostalCodeAction : USDBActionBase<string>
    {
        private string _postalCode = string.Empty;
        public GetCityForPostalCodeAction(string postalCode)
        {
            _postalCode = postalCode;
        }
        protected override string Body(System.Data.Common.DbConnection connection)
        {
            string cityName = string.Empty;
            string spName = "USExceGMSGetCityForPostalCode";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@PostalCode", System.Data.DbType.String, _postalCode));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    cityName = reader["PostPlace"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cityName;
        }
    }
}
