﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UploadArticleAction : USDBActionBase<bool>
    {
        private DataTable _dataTable;
        private readonly string _user = string.Empty;
        private int _branchId;
        public UploadArticleAction(List<ArticleDC> articleList,int branchId, string user)
        {
            _dataTable = GetArticleLst(articleList);
            _user = user;
            _branchId = branchId;
        }

        private DataTable GetArticleLst(List<ArticleDC> articleList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("BarCode", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("UnitName", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("Description", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("DefaultPrice", typeof(Decimal)));
            _dataTable.Columns.Add(new DataColumn("PurchasedPrice", typeof(Decimal)));
            _dataTable.Columns.Add(new DataColumn("EmployeePrice", typeof(Decimal)));
            _dataTable.Columns.Add(new DataColumn("Category", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("VendorNumber", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("VendorName", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("RevenueAccountNo", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("BranchIds", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("VatCode", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("ArticleType", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("ActivityName", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("StockStatus", typeof(Boolean)));
            _dataTable.Columns.Add(new DataColumn("StockLevel", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("ReOrderLevel", typeof(Int32)));
            if (articleList != null && articleList.Any())
                foreach (var item in articleList)
                {
                    DataRow dataTableRow = _dataTable.NewRow();
                    dataTableRow["BarCode"] = item.BarCode;
                    dataTableRow["UnitName"] = item.UnitName;
                    dataTableRow["Description"] = item.Description;
                    dataTableRow["DefaultPrice"] = item.DefaultPrice;
                    dataTableRow["PurchasedPrice"] = item.PurchasedPrice;
                    dataTableRow["EmployeePrice"] = item.EmployeePrice;
                    dataTableRow["Category"] = item.Category;
                    dataTableRow["VendorNumber"] = item.VendorNumber;
                    dataTableRow["VendorName"] = item.VendorName;
                    dataTableRow["RevenueAccountNo"] =item.RevenueAccountNo;
                    string branchIds = string.Empty;
                    if (item.BranchIdList != null)
                    {
                        branchIds = string.Join(",", item.BranchIdList);
                    }
                    dataTableRow["BranchIds"] = branchIds;
                    dataTableRow["VatCode"] = item.VatCode;
                    dataTableRow["ArticleType"] = item.ArticleType;
                    dataTableRow["ActivityName"] = item.ActivityName;
                    dataTableRow["StockStatus"] = item.StockStatus;
                    dataTableRow["StockLevel"] = item.StockLevel;
                    dataTableRow["ReOrderLevel"] = item.ReOrderLevel;
                    _dataTable.Rows.Add(dataTableRow);
                }
            return _dataTable;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result;
            string spName = "USExceGMSUploadArticle";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                command.Parameters.Add(_dataTable != null
                                           ? DataAcessUtils.CreateParam("@ExceArticle", SqlDbType.Structured,
                                                                        _dataTable)
                                           : DataAcessUtils.CreateParam("@ExceArticle", SqlDbType.Structured, null));

               
                command.ExecuteNonQuery();
                result = true;
              
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
    }
}
