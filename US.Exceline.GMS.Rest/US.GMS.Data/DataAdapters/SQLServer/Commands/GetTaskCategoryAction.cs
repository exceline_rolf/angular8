﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : DKA
// Created Timestamp : "10/10/2013 6:06:49 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetTaskCategoryAction : USDBActionBase<List<ExcelineTaskCategoryDC>>
    {
        public GetTaskCategoryAction(int branchId) { }

        protected override List<ExcelineTaskCategoryDC> Body(DbConnection connection)
        {
            List<ExcelineTaskCategoryDC> taskCategoryList = new List<ExcelineTaskCategoryDC>();
            const string storedProcedureName = "USExceGMSAdminGetTaskCategories";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExcelineTaskCategoryDC taskCategory = new ExcelineTaskCategoryDC();
                    taskCategory.Id = Convert.ToInt32(reader["ID"]);
                    taskCategory.Name = reader["Name"].ToString();
                    taskCategory.IsAssignToEmp = Convert.ToBoolean(reader["IsAssignToEmp"]);
                    taskCategory.IsAssignToRole = Convert.ToBoolean(reader["IsAssignToRole"]);
                    taskCategory.IsStartDate = Convert.ToBoolean(reader["IsStartDate"]);
                    taskCategory.IsEndDate = Convert.ToBoolean(reader["IsEndDate"]);
                    taskCategory.IsStartTime = Convert.ToBoolean(reader["IsStartTime"]);
                    taskCategory.IsEndTime = Convert.ToBoolean(reader["IsEndTime"]);
                    taskCategory.IsFoundDate = Convert.ToBoolean(reader["IsFoundDate"]);
                    taskCategory.IsReturnDate = Convert.ToBoolean(reader["IsReturnDate"]);
                    taskCategory.IsNoOfDays = Convert.ToBoolean(reader["IsNoOfDays"]);
                    taskCategory.IsDueTime = Convert.ToBoolean(reader["IsDueTime"]);
                    taskCategory.IsDueDate = Convert.ToBoolean(reader["IsDueDate"]);
                    taskCategory.IsPhoneNo = Convert.ToBoolean(reader["IsPhoneNo"]);
                    taskCategory.IsNextFollowUp = Convert.ToBoolean(reader["IsNextFollowUp"]);
                    taskCategory.IsAutomatedSMS = Convert.ToBoolean(reader["IsAutomatedSMS"]);
                    taskCategory.IsAutomatedEmail = Convert.ToBoolean(reader["IsAutomatedEmail"]);
                    taskCategory.IsDefault = Convert.ToBoolean(reader["IsDefault"]);
                    taskCategoryList.Add(taskCategory);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return taskCategoryList;
        }
    }
}
