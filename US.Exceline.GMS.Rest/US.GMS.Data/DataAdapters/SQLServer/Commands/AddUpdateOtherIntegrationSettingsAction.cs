﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddUpdateOtherIntegrationSettingsAction : USDBActionBase<bool>
    {
        private readonly OtherIntegrationSettingsDC _otherIntegrationSettingsDetail;
        private readonly int _branchId = -1;

        public AddUpdateOtherIntegrationSettingsAction(int branchId, OtherIntegrationSettingsDC otherIntegrationSettingsDetail)
        {
            _branchId = branchId;
            _otherIntegrationSettingsDetail = otherIntegrationSettingsDetail;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "dbo.USExceGMSAddUpdateSystemIntegrationSettings";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.String, _otherIntegrationSettingsDetail.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.String, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SystemName", DbType.String, _otherIntegrationSettingsDetail.SystemName.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ServiceBaseUrl", DbType.String, _otherIntegrationSettingsDetail.ServiceBaseUrl.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _otherIntegrationSettingsDetail.UserName.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Password", DbType.String, _otherIntegrationSettingsDetail.Password.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@APIKey", DbType.String, _otherIntegrationSettingsDetail.ApiKey.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FacilityUrl", DbType.String, _otherIntegrationSettingsDetail.FacilityUrl.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _otherIntegrationSettingsDetail.User));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Guid", DbType.String, _otherIntegrationSettingsDetail.Guid));

                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                var outputId = Convert.ToInt32(para1.Value);
                if (outputId > 0)
                {
                    return true;
                }
                return false;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

