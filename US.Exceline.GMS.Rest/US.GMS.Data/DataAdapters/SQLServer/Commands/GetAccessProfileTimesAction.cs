﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetAccessProfileTimesAction : USDBActionBase<List<ExceAccessProfileTimeDC>>
    {
        private int _accessProfileId = -1;
        public GetAccessProfileTimesAction(int accessProfileId)
        {
            _accessProfileId = accessProfileId;
        }

        protected override List<ExceAccessProfileTimeDC> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSGetAccessProfilesTimes";
            List<ExceAccessProfileTimeDC> prifileTimeList = new List<ExceAccessProfileTimeDC>();
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@profileId", System.Data.DbType.Int32, _accessProfileId));

                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExceAccessProfileTimeDC profileTime = new ExceAccessProfileTimeDC();
                    profileTime.FromTime = Convert.ToDateTime(reader["FromTime"]);
                    profileTime.ToTime = Convert.ToDateTime(reader["ToTime"]);
                    prifileTimeList.Add(profileTime);
                }
                return prifileTimeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
