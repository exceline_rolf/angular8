﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetRegionsForAccessProfileAction : USDBActionBase<List<int>>
    {
        private readonly int _accessProfileId;

        public GetRegionsForAccessProfileAction(int accessProfileId)
        {
            _accessProfileId = accessProfileId;
        }

        protected override List<int> Body(DbConnection connection)
        {
            List<int> regionList = new List<int>();
            const string spGetItems = "USExceGMSAdminGetRegionsByAccessProfileId";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, spGetItems);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@accessProfileId", DbType.Int32, _accessProfileId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int regionId = Convert.ToInt32(reader["ID"]);
                    regionList.Add(regionId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return regionList;
        }
    }
}
