﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/9/2012 2:42:02 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Data.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.GMS.Data.DataAdapters
{
    public class CommonBookingFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static bool CheckCommonBookingAvailability(int activeTimeId, int entityId, int branchId, string entityType, DateTime startDateTime, DateTime endDateTime, string gymCode)
        {
            return GetDataAdapter().CheckCommonBookingAvailability(activeTimeId, entityId, branchId, entityType, startDateTime, endDateTime, gymCode);
        }

        public static bool SaveCommonBookingDetails(CommonBookingDC commonBookingDC, ScheduleDC scheduleDC, int branchId, string user, string scheduleCategoryType, string gymCode)
        {
            return GetDataAdapter().SaveCommonBookingDetails(commonBookingDC, scheduleDC, branchId, user, scheduleCategoryType, gymCode);
        }
    }
}
