﻿using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Data.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;

namespace US.GMS.Data.DataAdapters.ManageSystemSettings
{
    public class GymSettingFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        #region Gym Access Time Settings

        //public static List<GymAccessTimeSettingsDC> GetGymAccessTimeSettings(int branchId, string gymCode, string userName)
        //{
        //    return GetDataAdapter().GetGymAccessTimeSettings(branchId, gymCode, userName);
        //}
        //public static bool SaveGymAccessTimeSettings(int branchId, GymAccessTimeSettingsDC accessTimeSettings, string gymCode)
        //{
        //    return GetDataAdapter().SaveGymAccessTimeSettings(branchId, accessTimeSettings, gymCode);
        //}

        public static List<GymAccessSettingDC> GetGymAccessSettings(int branchId, string gymCode, string userName)
        {
            return GetDataAdapter().GetGymAccessSettings(branchId, gymCode, userName);
        }

        public static bool SaveGymAccessSettings(int branchId, GymAccessSettingDC accessTimeSettings, string gymCode)
        {
            return GetDataAdapter().SaveGymAccessSettings(branchId, accessTimeSettings, gymCode);
        }
        #endregion

        #region Gym Open Time
        public static List<GymOpenTimeDC> GetGymOpenTimes(int branchId, string gymCode, string userName)
        {
            return GetDataAdapter().GetGymOpenTimes(branchId, gymCode, userName);
        }

        public static bool SaveGymOpenTimes(int branchId, List<GymOpenTimeDC> gymOpenTimesList, string gymCode)
        {
            return GetDataAdapter().SaveGymOpenTimes(branchId, gymOpenTimesList, gymCode);
        }
        #endregion

        #region Gym Required Settings
        public static List<GymRequiredSettingsDC> GetGymRequiredSettings(int branchId, string gymCode, string userName)
        {
            return GetDataAdapter().GetGymRequiredSettings(branchId, gymCode, userName);
        }

        public static bool SaveGymRequiredSettings(int branchId, GymRequiredSettingsDC requiredSettings, string gymCode)
        {
            return GetDataAdapter().SaveGymRequiredSettings(branchId, requiredSettings, gymCode);
        }
        #endregion

        #region Gym Member Search Settings
        public static List<GymMemberSearchSettingsDC> GetGymMemberSearchSettings(int branchId, string gymCode, string userName)
        {
            return GetDataAdapter().GetGymMemberSearchSettings(branchId, gymCode, userName);
        }

        public static bool SaveGymMemberSearchSettings(int branchId, GymMemberSearchSettingsDC memberSearchSettings, string gymCode)
        {
            return GetDataAdapter().SaveGymMemberSearchSettings(branchId, memberSearchSettings, gymCode);
        }
        #endregion

        #region Gym Hardware Profiles Settings
        public static List<ExceHardwareProfileDC> GetHardwareProfiles(int branchId, string gymCode, string userName)
        {
            return GetDataAdapter().GetHardwareProfiles(branchId, gymCode, userName);
        }

        public static bool SaveHardwareProfile(int branchId, List<ExceHardwareProfileDC> hardwareProfileList, string gymCode)
        {
            return GetDataAdapter().SaveHardwareProfile(branchId, hardwareProfileList, gymCode);
        }
        #endregion

        #region Gym Economy Settings
        public static List<GymEconomySettingsDC> GetGymEconomySettings(int branchId, string gymCode, string userName)
        {
            return GetDataAdapter().GetGymEconomySettings(branchId, gymCode, userName);
        }

        public static bool SaveGymEconomySettings(int branchId, GymEconomySettingsDC economySettings, string gymCode)
        {
            return GetDataAdapter().SaveGymEconomySettings(branchId, economySettings, gymCode);
        }
        #endregion

        #region Gym Sms Settings
        public static List<GymSmsSettingsDC> GetGymSmsSettings(int branchId, string gymCode, string userName)
        {
            return GetDataAdapter().GetGymSmsSettings(branchId, gymCode, userName);
        }

        public static bool SaveGymSmsSettings(int branchId, GymSmsSettingsDC smsSettings, string gymCode)
        {
            return GetDataAdapter().SaveGymSmsSettings(branchId, smsSettings, gymCode);
        }
        #endregion

        #region Gym Other Settings
        public static List<GymOtherSettingsDC> GetGymOtherSettings(int branchId, string gymCode, string userName)
        {
            return GetDataAdapter().GetGymOtherSettings(branchId, gymCode, userName);
        }

        public static bool SaveGymOtherSettings(int branchId, GymOtherSettingsDC otherSettings, string gymCode)
        {
            return GetDataAdapter().SaveGymOtherSettings(branchId, otherSettings, gymCode);
        }
        #endregion

        #region GetSelectedGymSettings
        public static Dictionary<string, object> GetSelectedGymSettings(List<string> settingColumnList, bool isGymSettings, int branchId, string gymCode)
        {
            return GetDataAdapter().GetSelectedGymSettings(settingColumnList, isGymSettings, branchId, gymCode);
        }
        #endregion

        #region GetGymIntegrationSettings
        public static List<GymIntegrationSettingsDC> GetGymIntegrationSettings(int branchId, string gymCode)
        {
            return GetDataAdapter().GetGymIntegrationSettings(branchId, gymCode);
        }

        public static List<GymIntegrationExcSettingsDC> GetGymIntegrationExcSettings(int branchId, string gymCode)
        {
            return GetDataAdapter().GetGymIntegrationExcSettings(branchId, gymCode);
        }

        public static List<TerminalType> GetTerminalTypes(string gymCode)
        {
            return GetDataAdapter().GetTerminalTypes(gymCode);
        }

        public static bool SaveGymIntegrationSettings(int branchId, GymIntegrationSettingsDC integrationSettings, string gymCode)
        {
            return GetDataAdapter().SaveGymIntegrationSettings(branchId, integrationSettings, gymCode);
        }

        public static bool SaveGymIntegrationExcSettings(int branchId, GymIntegrationExcSettingsDC integrationSettings, string gymCode)
        {
            return GetDataAdapter().SaveGymIntegrationExcSettings(branchId, integrationSettings, gymCode);
        }

        public static bool DeleteIntegrationSettingById(int id, string gymCode)
        {
            return GetDataAdapter().DeleteIntegrationSettingById(id, gymCode);
        }

        public static OtherIntegrationSettingsDC GetOtherIntegrationSettings(int branchId, string gymCode, string systemName)
        {
            return GetDataAdapter().GetOtherIntegrationSettings(branchId, gymCode, systemName);
        }

        public static bool AddUpdateOtherIntegrationSettings(int branchId, OtherIntegrationSettingsDC otherIntegrationSettings, string gymCode)
        {
            return GetDataAdapter().AddUpdateOtherIntegrationSettings(branchId, otherIntegrationSettings, gymCode);
        }
        #endregion
    }
}
