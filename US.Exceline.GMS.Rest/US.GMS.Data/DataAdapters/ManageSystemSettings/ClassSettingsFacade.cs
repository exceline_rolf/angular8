﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Data.SystemObjects;

namespace US.GMS.Data.DataAdapters.ManageSystemSettings
{
    public class ClassSettingsFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static List<ExceClassTypeDC> GetClassTypes(string gymCode)
        {
            return GetDataAdapter().GetClassTypes( gymCode);
        }

        public static List<ExceClassTypeDC> GetClassTypeByBranch(int branchId, string gymCode)
        {
            return GetDataAdapter().GetClassTypesByBranch(branchId,gymCode);
        }

        public static bool AddEditClassType(ExceClassTypeDC classType, string gymCode)
        {
            return GetDataAdapter().AddEditClassType(classType, gymCode);
        }

        public static int DeleteClassType(int classTypeId, string gymCode)
        {
            return GetDataAdapter().DeleteClassType(classTypeId, gymCode);
        }
    }
}
