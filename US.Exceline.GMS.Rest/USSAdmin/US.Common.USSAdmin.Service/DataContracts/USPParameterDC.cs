﻿
// --------------------------------------------------------------------------
// Copyright(c) 2008 UnicornSolutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.Services.DataContracts
// Coding Standard   : US Coding Standards
// Author            : DAM
// Created Timestamp : 27/06/2011 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.CompilerServices;

namespace US.Common.USSAdmin.Service.DataContracts
{

    [DataContract]
    public class USPParameterDC
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string ParameterType { get; set; }
        [DataMember]
        public object Value { get; set; }
    }

}
