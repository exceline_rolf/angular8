﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Common.USSAdmin.Service.DataContracts
{
    [DataContract]
    public class ClaimsAndPaymentSummeryDC
    {
        private int _numberOfClaims = 0;
        [DataMember]
        public int NumberOfClaims
        {
            get { return _numberOfClaims; }
            set { _numberOfClaims = value; }
        }
    }
}
