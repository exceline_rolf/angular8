﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Common.USSAdmin.Service.DataContracts
{
    [DataContract]
    public class OperationFieldDC
    {
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string Status { get; set; }
    }
}
