﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Common.USSAdmin.RestApi.Models
{
    public class ExceCategoryDC
    {
        private int _id = -1;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int typeId = -1;
        public int TypeId
        {
            get { return typeId; }
            set { typeId = value; }
        }

        private string _typeName = string.Empty;
        public string TypeName
        {
            get { return _typeName; }
            set { _typeName = value; }
        }

        private int _branchId = -1;
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _code = string.Empty;
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _name = string.Empty;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _description = string.Empty;
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private byte[] _image;
        public byte[] CategoryImage
        {

            get { return _image; }
            set { _image = value; }
        }

        private DateTime _createdDate;
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private DateTime _lastModifiedDate;
        public DateTime LastModifiedDate
        {
            get { return _lastModifiedDate; }
            set { _lastModifiedDate = value; }
        }

        private string _createdUser = string.Empty;
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _lastModifiedUser;
        public string LastModifiedUser
        {
            get { return _lastModifiedUser; }
            set { _lastModifiedUser = value; }
        }

        private bool _activeStatus;
        public bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private bool _isCheck = false;
        public bool IsCheck
        {
            get { return _isCheck; }
            set { _isCheck = value; }
        }
    }
}