﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.Common.Web.UI.Core.SystemObjects;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;

namespace US.Common.USSAdmin.RestApi.Models
{
    public class AddUspUser
    {
         public UspUsersExtended uspUser { get; set; }
        public List<UserBranchSelected> branchesList { get; set; }
    }
}