﻿using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    class CheckUserNameAvailabilityAction: USDBActionBase<bool>
    {
        private string _userName = string.Empty;

        public CheckUserNameAvailabilityAction(string userName)
        {
            this._userName = userName;
            OverwriteUser(_userName);
        }


        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool reader = false;

            try
            {
                string spName = "USP_AUT_UserNameAvailability";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userName", DbType.String, _userName));

                reader = Convert.ToBoolean(cmd.ExecuteScalar());


            }
            catch
            {
                throw;
            }
            return reader;
        }
    }
}
