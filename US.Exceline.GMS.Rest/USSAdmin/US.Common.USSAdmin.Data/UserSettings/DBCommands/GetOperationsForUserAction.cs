﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects;
using System.Data.Common;
using System.Data;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    class GetOperationsForUserAction : USDBActionBase<List<USPOperation>>
    {
        private string _userName = string.Empty;

        public GetOperationsForUserAction(string user)
        {
            _userName = user;
            OverwriteUser(user);
        }

        protected override List<USPOperation> Body(System.Data.Common.DbConnection connection)
        {
            List<USPOperation> operationList = new List<USPOperation>();

            try
            {
                var subOperationList = new GetSubOperationListForUserAction(_userName).Execute(EnumDatabase.USP);

                string spName = "USP_AUT_GetOperationsForUser";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));

                DbDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    USPOperation operation = new USPOperation();
                    if (!(string.IsNullOrEmpty(reader["DisplayName"].ToString())))
                    {
                        operation.DisplayName = Convert.ToString(reader["DisplayName"]);
                    }
                    if (!(string.IsNullOrEmpty(reader["ID"].ToString())))
                    {
                        operation.ID = Convert.ToInt32(reader["ID"]);
                    }
                    if (reader["Name"] != null)
                    {
                        operation.Name = Convert.ToString(reader["Name"]);
                    }
                    if (reader["ThumbnailImage"] != null)
                    {
                        operation.ThumbnailImage = Convert.ToString(reader["ThumbnailImage"]);
                    }
                    if (reader["COLOR"] != null)
                    {
                        operation.OperationColor = Convert.ToString(reader["COLOR"]);
                    }
                    if (reader["ModuleId"] != null)
                    {
                        operation.ModuleId = Convert.ToInt32(reader["ModuleId"]);
                    }
                    if (!(string.IsNullOrEmpty(reader["FeatureId"].ToString())))
                    {
                        operation.FeatureId = Convert.ToInt32(reader["FeatureId"]);
                    }
                    if (reader["Namespace"] != null)
                    {
                        operation.OperationNameSpace = Convert.ToString(reader["Namespace"]);
                    }
                    if (!string.IsNullOrEmpty(reader["AddToWorkStation"].ToString()))
                    {
                        operation.IsAddedToWorkStation = Convert.ToInt32(reader["AddToWorkStation"]);
                    }
                    if (!string.IsNullOrEmpty(reader["Package"].ToString()))
                    {
                        operation.OperationPackage = Convert.ToString(reader["Package"]);
                    }
                    operation.OperationHomeURL = Convert.ToString(reader["OperationHomeURL"]);

                    operation.SubOperationList = subOperationList;
                    operationList.Add(operation);
                }
            }
            catch
            {
                throw;
            }
            return operationList;
        }


    }
}
