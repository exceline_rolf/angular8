﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects;
using System.Data.Common;
using System.Data;
using US.GMS.Core.Utils;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    class USPUsersSearchAction : USDBActionBase<List<USPUserTemp>>
    {
        private string _searchText;
        private bool _isActive;
        private string _loggedUser = string.Empty;
       
        public USPUsersSearchAction(string searchText, bool isActive, string loggedUser)
        {

            this._isActive = isActive;
            this._searchText = searchText;
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override List<USPUserTemp> Body(System.Data.Common.DbConnection connection)
        {
            List<USPUserTemp> uspUserList = new List<USPUserTemp>();

            try
            {
                string storedProcedure = "USP_AUT_GetUSPUserSearch";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SearchText", DbType.String, _searchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.Boolean, _isActive));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    USPUserTemp uspUser = new USPUserTemp();
                    uspUser.Id = Convert.ToInt32(reader["ID"]);
                    if (!string.IsNullOrEmpty(reader["UserName"].ToString()))
                    {
                        uspUser.UserName = Convert.ToString(reader["UserName"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ExternalUserName"].ToString()))
                    {
                        uspUser.ExternalUser = Convert.ToString(reader["ExternalUserName"]);
                    }
                    if (!string.IsNullOrEmpty(reader["DisplayName"].ToString()))
                    {
                        uspUser.DisplayName = Convert.ToString(reader["DisplayName"]);
                    }
                    uspUser.RoleId = Convert.ToInt32(reader["RoleId"]);
                    if (!string.IsNullOrEmpty(reader["RoleName"].ToString()))
                    {
                        uspUser.RoleName = Convert.ToString(reader["RoleName"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ModuleId"].ToString()))
                    {
                        uspUser.UniqueModuleId = Convert.ToInt32(reader["ModuleId"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ModuleGranted"].ToString()))
                    {
                        uspUser.ModuleGranted = Convert.ToInt32(reader["ModuleGranted"]);
                    }
                    if (!string.IsNullOrEmpty(reader["FeatureId"].ToString()))
                    {
                        uspUser.UniqueFeatureId = Convert.ToInt32(reader["FeatureId"]);
                    }
                    if (!string.IsNullOrEmpty(reader["FeatureGranted"].ToString()))
                    {
                        uspUser.FeatureGranted = Convert.ToInt32(reader["FeatureGranted"]);
                    }
                    if (!string.IsNullOrEmpty(reader["OperationId"].ToString()))
                    {
                        uspUser.UniqueOperationId = Convert.ToInt32(reader["OperationId"]);
                    }
                    if (!string.IsNullOrEmpty(reader["OperationGranted"].ToString()))
                    {
                        uspUser.OperationGranted = Convert.ToInt32(reader["OperationGranted"]);
                    }

                    if (!string.IsNullOrEmpty(reader["Email"].ToString()))
                    {
                        uspUser.Email = reader["Email"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["FieldList"].ToString()))
                    {
                        string feildListXML = Convert.ToString(reader["FieldList"]);
                        uspUser.OperationExInfo = (OperationExtendedInfo)XMLUtils.DesrializeXMLToObject(feildListXML, typeof(OperationExtendedInfo));
                    }
                    if (!string.IsNullOrEmpty(reader["IsActive"].ToString()))
                        uspUser.IsActive = Convert.ToBoolean(reader["IsActive"]);
                    if (!string.IsNullOrEmpty(reader["HardwareProfileId"].ToString()))
                        uspUser.HardwareProfileId = Convert.ToInt32(reader["HardwareProfileId"]);
                    if (!string.IsNullOrEmpty(reader["CardNumber"].ToString()))
                        uspUser.CardNumber = Convert.ToString(reader["CardNumber"]);
                    
                    uspUserList.Add(uspUser);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return uspUserList;
        }
    }
}
