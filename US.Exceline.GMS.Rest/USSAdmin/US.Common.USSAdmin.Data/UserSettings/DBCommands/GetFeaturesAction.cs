﻿#region File Header
// --------------------------------------------------------------------------
// Copyright(c) <2010> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US.Payment.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : 29/12/2011 HH:MM  PM
// --------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;

using US_DataAccess;
using System.Data.Common;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class GetFeaturesAction : USDBActionBase<List<USPFeature>>
    {
        private string _loggedUser = string.Empty;
        public GetFeaturesAction(string loggedUser)
        {
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override List<USPFeature> Body(System.Data.Common.DbConnection connection)
        {
            List<USPFeature> featureList = new List<USPFeature>();

            try
            {
                string spName = "USP_AUT_GetFeatures";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                

                DbDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    USPFeature feature = new USPFeature();
                    feature.ID = Convert.ToInt32(reader["ID"]);
                    feature.DisplayName = Convert.ToString(reader["DisplayName"]);
                    feature.Name = Convert.ToString(reader["Name"]);
                    feature.ThumbnailImage = Convert.ToString(reader["ThumbnailImage"]);
                    feature.HomeUIControl = Convert.ToString(reader["Namespace"]);
                    feature.ModuleId = Convert.ToInt32(reader["ModuleId"]);
                    featureList.Add(feature);
                }
            }
            catch
            {
                throw;
            }
            return featureList;

        }

    }
}
