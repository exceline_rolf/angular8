﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using US.Common.Logging.API;
using US.Payment.Core.Utils;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public  class SaveEditedReportAction : USDBActionBase<bool>
    {
        private readonly string _userName = string.Empty;
        private readonly int _id = -1;
        private readonly USReport _reportToBeSaved;

        public SaveEditedReportAction(int id, USReport reportToBeSaved, string coreSessionUserName)
        {
            _id = id;
            _reportToBeSaved = reportToBeSaved;
            _userName = coreSessionUserName;
        }

        protected override bool Body(DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "USRPT.SaveEditedReport";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ID", DbType.Int32, _id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _reportToBeSaved.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Category", DbType.String, _reportToBeSaved.Category));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.String, string.Join(",", _reportToBeSaved.RoleList.Where(x => x.IsView).Select(role => role.Id))));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleIdForEdit", DbType.String, string.Join(",", _reportToBeSaved.RoleList.Where(x => x.IsEditable).Select(role => role.Id))));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _userName));
                var xmlString = XMLUtils.SerializeObject(_reportToBeSaved);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Report", DbType.Xml, xmlString.Replace("<?xml version=\"1.0\" encoding=\"utf-32\"?>", "")));
                if (_reportToBeSaved.Description == null)
                {
                    _reportToBeSaved.Description = "US Report Module Report";
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _reportToBeSaved.Description));
                cmd.ExecuteNonQuery();

                if (_reportToBeSaved.IsScheduleReport)
                {
                    //implement here to add data to US_NotificationSchedule table..
                    ReportingFacade.SaveReportSchedule(true, _userName, _id, _reportToBeSaved, _reportToBeSaved.ReportScheduleList);
                }
                else
                {
                    ReportingFacade.DeleteExistingSchedules(_userName, _id);
                }
                return true;
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while SaveEditedReportAction " + ex.Message, ex, _userName);
                throw;
            }
        }
    }
}
