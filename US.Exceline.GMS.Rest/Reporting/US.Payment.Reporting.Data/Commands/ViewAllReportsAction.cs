﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using US.Common.Logging.API;
using US.Payment.Core;
using US.Payment.Core.Utils;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    class ViewAllReportsAction : USDBActionBase<List<USReport>>
    {
        /// <summary>
        /// get all reports
        /// </summary>
        ///
        private List<USReport> _resultAllReports = new List<USReport>();
        private string _coreSessionUserName = string.Empty;

        public ViewAllReportsAction(string CoreSessionUserName)
        {
            _coreSessionUserName = CoreSessionUserName;
        }

        protected override List<USReport> Body(DbConnection connection)
        {
            try
            {

                string StoredProcedureName = "USRPT.GetAllReports";
                DbCommand Cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                Cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _coreSessionUserName));

                DbDataReader Reader = Cmd.ExecuteReader();
                while (Reader.Read())
                {
                    USReport _reportInfo = new USReport();
                    try
                    {
                        string reportObjectXMLString = Convert.ToString(Reader["Report"]);
                        int _id = Convert.ToInt32(Reader["ID"]);
                        if (!string.IsNullOrEmpty(reportObjectXMLString.Trim()))
                        {
                            _reportInfo = XMLUtils.DeserializeObject<USReport>(reportObjectXMLString);

                            List<ReportEntity> latestReportEntityList;
                            if (_coreSessionUserName.Contains('/'))
                            {
                                latestReportEntityList = new GetAllEntitiesWithPropertiesAction(_coreSessionUserName).Execute(EnumDatabase.Exceline, _coreSessionUserName.Split('/')[0]);
                            }
                            else
                            {
                                latestReportEntityList = new GetAllEntitiesWithPropertiesAction(_coreSessionUserName).Execute(EnumDatabase.Exceline, _coreSessionUserName);
                            }
                            _reportInfo.EntityList = UpdateEntityProertyWidthValues(_reportInfo, latestReportEntityList);
                            AddPrimaryColumnWithReportEntities(_reportInfo, latestReportEntityList);
                        }
                        _reportInfo.Id = _id;
                        _reportInfo.Description = Convert.ToString(Reader["Description"]);
                        _reportInfo.CreateDate = Convert.ToDateTime(Reader["CreatedDate"]);
                        _reportInfo.CreateUser = Convert.ToString(Reader["CreatedUser"]);
                        _reportInfo.LastModifiedDate = Convert.ToDateTime(Reader["LastModifiedDate"]);
                        _reportInfo.LastModifiedUser = Convert.ToString(Reader["LastModifiedUser"]);
                        _reportInfo.IsEditable = Convert.ToBoolean(Reader["IsEditable"]);

                        List<ReportSchedule> scheduleList = new GetScheduleListByReportIdAction(_id, _coreSessionUserName).Execute(EnumDatabase.Exceline, _coreSessionUserName.Split('/')[0]);
                        _reportInfo.ReportScheduleList = scheduleList;
                        _resultAllReports.Add(_reportInfo);
                    }
                    catch
                    {
                        _reportInfo.Description = "? " + _reportInfo.Description;
                        continue;
                    }
                }

            }
            catch//(Exception ex)
            {
                //USLogEvent.WriteToFile("View All reports exception", ex.Message.ToString());
                //USLogError.WriteToFile("View All reports exception", ex, _coreSessionUserName);

                throw;
            }
            return _resultAllReports;
        }

        private List<ReportEntity> UpdateEntityProertyWidthValues(USReport reportInfo, List<ReportEntity> latestReportEntityList)
        {
            try
            {
                List<ReportEntity> tempEntityList = new List<ReportEntity>();
                foreach (ReportEntity rre in reportInfo.EntityList)
                {
                    ReportEntity entity = UpdateEntityProperty(rre, latestReportEntityList);
                    tempEntityList.Add(entity);
                }
                return tempEntityList;

            }
            catch
            {
                throw;
            }
        }

        private void AddPrimaryColumnWithReportEntities(USReport reportInfo, List<ReportEntity> latestReportEntityList)
        {
            foreach (ReportEntity rre in latestReportEntityList)
            {
                for (int i = 0; i < reportInfo.EntityList.Count; i++)
                {
                    if (rre.DisplayName == reportInfo.EntityList[i].DisplayName)
                    {
                        reportInfo.EntityList[i].PrimaryColumn = rre.PrimaryColumn;
                        reportInfo.EntityList[i].BasedOn = rre.BasedOn;
                    }
                }
            }
        }

        private ReportEntity UpdateEntityProperty(ReportEntity rre, List<ReportEntity> latestReportEntityList)
        {
            ReportEntity tempReportEntity = new ReportEntity();
            tempReportEntity = rre;
            try
            {
                foreach (ReportEntity entity in latestReportEntityList)
                {
                    if (entity.DisplayName.ToUpper().Trim() == rre.DisplayName.ToUpper().Trim())
                    {
                        List<ReportingEntityProperty> old = rre.Properties;
                        HashSet<string> old_1 = new HashSet<string>(old.Select(s => s.DataColumnName));
                        List<ReportingEntityProperty> newL = entity.Properties;
                        HashSet<string> newL_1 = new HashSet<string>(newL.Select(u => u.DataColumnName));

                        List<ReportingEntityProperty> newlyAddedColumns = newL.Where(x => !old_1.Contains(x.DataColumnName)).ToList();
                        List<ReportingEntityProperty> intersectedColumns = old.Where(y => newL_1.Contains(y.DataColumnName)).ToList();

                        tempReportEntity.Properties = intersectedColumns.Union(newlyAddedColumns).ToList();

                        break;
                    }
                }
                return tempReportEntity;
            }
            catch
            {
                throw;
            }
        }
    }
}