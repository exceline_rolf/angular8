﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class SaveReportScheduleAction : USDBActionBase<string>
    {
        private bool _isEditingReport = false;
        private string _userName = string.Empty;
        private List<ReportSchedule> _scheduleList;
        private int _reportId = 0;
        private USReport _report = new USReport();

        public SaveReportScheduleAction(bool isEditingReport, string userName, int reportId, USReport report, List<ReportSchedule> scheduleList)
        {
            _isEditingReport = isEditingReport;
            _userName = userName;
            _reportId = reportId;
            _scheduleList = scheduleList;
            _report = report;
        }
        protected override string Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                foreach (var schedule in _scheduleList)
                {
                    //save schedule
                    string storedProcedureName = "USRPT.SaveReportSchedule";
                    DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsEditingReport", DbType.Boolean, _isEditingReport));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, schedule.ScheduleId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReportId", DbType.Int32, _reportId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReportPrimaryEntityId", DbType.String, _report.EntityList.SingleOrDefault(x => x.DisplayName == schedule.ReportPrimaryEntity).Id));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReportQuery", DbType.String, _report.QueryForReport));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleStartDate", DbType.DateTime, schedule.ScheduleStartDate));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleEnddate", DbType.DateTime, schedule.ScheduleEnddate));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsUntillFurtherNotice", DbType.Boolean, schedule.IsUntillFurtherNotice));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleRecurrence", DbType.Int32, schedule.ScheduleRecurrence.Id));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleMonth", DbType.Int32, schedule.ScheduleMonth != null ? schedule.ScheduleMonth.Id : 0));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleWeek", DbType.Int32, schedule.ScheduleWeek != null ? schedule.ScheduleWeek.Id : 0));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleDay", DbType.Int32, schedule.ScheduleDay != null ? schedule.ScheduleDay.Id : 0));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleDate", DbType.Int32, schedule.ScheduleDate != null ? schedule.ScheduleDate.Id : 0));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleTime", DbType.DateTime, schedule.ScheduleTime));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDeleted", DbType.Boolean, schedule.IsDeleted));

                    if (schedule.ScheduleItemCategory != null)
                    {
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemCategory", DbType.Int32, schedule.ScheduleItemCategory.Id));
                    }
                    
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemTemplateId", DbType.Int32, schedule.ScheduleItemTemplate.TemplateId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleRecipient", DbType.String, schedule.ScheduleItemRecipient));
                    if (schedule.ScheduleItemRecipientRole != null)
                    {
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleRecipientRoleID", DbType.Int32, schedule.ScheduleItemRecipientRole.Id));
                    }
                    if (schedule.ReportScheduleFollowUPItem != null)
                    {
                        if (!string.IsNullOrEmpty(schedule.ReportScheduleFollowUPItem.ScheduleItemFollowupNameText))
                        {
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowupName", DbType.String, schedule.ReportScheduleFollowUPItem.ScheduleItemFollowupNameText));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowUPStartTime", DbType.DateTime, schedule.ReportScheduleFollowUPItem.FollowupStartTime));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowUPEndTime", DbType.DateTime, schedule.ReportScheduleFollowUPItem.FollowupEndTime));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowUPDescription", DbType.String, schedule.ReportScheduleFollowUPItem.FollowupDescription));
                        }
                    }

                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _userName));
                   
                    DbParameter parameter = new SqlParameter();
                    parameter.DbType = DbType.Int32;
                    parameter.Direction = ParameterDirection.Output;
                    parameter.ParameterName = "@ScheduleItemId";
                    cmd.Parameters.Add(parameter);

                    cmd.ExecuteNonQuery();

                    //cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemHours", DbType.Int32, item.ScheduleItemHours));
                    //cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemRecipient", DbType.String, item.ScheduleItemRecipient));
                    //cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemRecipientRole", DbType.String, item.ScheduleItemRecipientRole));
                }
                return "";
            }
            catch(Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while SaveScheduleAction "+ ex.Message, ex, _userName);
                throw;
            }
        }
    }
}
