﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    class SaveReportSettingAction : USDBActionBase<string>
    {
        //private string  _userName;
        private ReportConfigureSetting _settingItem;
        //private string _joinedSettings = string.Empty;

        public SaveReportSettingAction(string userName, ReportConfigureSetting settingItem)
        {
           // _userName = userName;
            _settingItem = settingItem;
        }
        protected override string Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                for (int i = 0; i < _settingItem.ReportSettingList.Count; i++)
                {
                    //_joinedSettings = _joinedSettings + "-" + _settingItem.ReportSettingList[i].Key;
                    string _storedProcedureName = "USRPT.SaveReportSetting";
                    DbCommand cmd = CreateCommand(CommandType.StoredProcedure, _storedProcedureName);
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@KeyString", DbType.String, _settingItem.ReportSettingList[i].Key));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ValueString", DbType.String, _settingItem.ReportSettingList[i].KeyValue));

                    cmd.ExecuteNonQuery();
                }
                return "true";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
