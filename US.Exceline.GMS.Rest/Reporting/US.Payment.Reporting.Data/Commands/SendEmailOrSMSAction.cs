﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class SendEmailOrSMSAction : USDBActionBase<int>
    {
        private string _userName = string.Empty;
        private string _subject = string.Empty;
        private string _body = string.Empty;
        private int _emailOrSMS = -1;
        private List<string> _recipientList = new List<string>();
        private string _primaryColumn = string.Empty;
        private string _basedOn = string.Empty;
        private bool _isSelected = false;
        private bool _reminder = false;
        private bool _addreminderFee = false;
        private DateTime? _reminderDuedate;
        private int _branchID = -1;
        private bool _considerationGDPR;

        public SendEmailOrSMSAction(bool considerationGDPR, string userName, string subject, string body, bool isSelected, int emailOrSMS, List<string> recipientList, string primaryColumn, string basedOn, bool reminder, bool addreminderFee, DateTime? reminderDueDate, int branchID )
        {
            _userName = userName;
            if (!string.IsNullOrEmpty(subject))
            {
                _subject = subject;
            }
            else
            {
                _subject = "";
            }
            _body = body;
            _isSelected = isSelected;
            _emailOrSMS = emailOrSMS;
            _recipientList = recipientList;
            _primaryColumn = primaryColumn;
            _basedOn = basedOn;
            _reminder = reminder;
            _addreminderFee = addreminderFee;
            _reminderDuedate = reminderDueDate;
            _branchID = branchID;
            _considerationGDPR = considerationGDPR;
        }
        protected override int Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "Exceline.SendEmailOrSMS";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Subject", DbType.String, _subject));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Body", DbType.String, _body));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GetReplySelected", DbType.Boolean, _isSelected));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmailOrSMS",DbType.Int32,_emailOrSMS));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PrimaryColumn", DbType.String, _primaryColumn));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BasedOn", DbType.String, _basedOn));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Reminder", DbType.Boolean, _reminder));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AddReminderFee", DbType.Boolean, _addreminderFee));
                if(_reminderDuedate.HasValue)
                   cmd.Parameters.Add(DataAcessUtils.CreateParam("@RmdDueDate", DbType.DateTime, _reminderDuedate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _branchID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _userName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@considerationGDPR", DbType.Boolean, _considerationGDPR));
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                for (int i = 0; i < _recipientList.Count; i++)
                {
                    table.Rows.Add(Convert.ToInt32(_recipientList[i].Trim()));
                }
                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@list";
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.Value = table;

                cmd.Parameters.Add(parameter);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while SendEmailOrSMSAction " + ex.Message, ex, _userName);
                throw;
            }
            return 1;
        }
    }
}
