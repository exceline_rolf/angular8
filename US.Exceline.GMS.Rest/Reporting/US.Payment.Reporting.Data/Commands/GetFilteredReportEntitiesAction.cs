﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.Payment.Reporting.Core.DomainObjects;
using US.Common.Logging.API;
using System.Configuration;

namespace US.Payment.Reporting.Data.Commands
{
    public class GetFilteredReportEntitiesAction : USDBActionBase<ReportEntity>
    {
        string _userName = ConfigurationManager.AppSettings["userName"];
        private string _coreSessionUserName = string.Empty;
        private string _entity = string.Empty;

        public GetFilteredReportEntitiesAction(string entity, string CoreSessionUserName)
        {
            _entity = entity;
            _coreSessionUserName = CoreSessionUserName;
        }
        protected override ReportEntity Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.GetFilteredReportEntities";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FilterReportEntity", DbType.String, _entity));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ReportEntity reportEntityInfo = new ReportEntity();
                    reportEntityInfo.Id = Convert.ToString(reader["ID"]);
                    reportEntityInfo.DisplayName =Convert.ToString(reader["DisplayName"]);
                    reportEntityInfo.TableOrViewName = Convert.ToString(reader["DataTableOrView"]);
                    reportEntityInfo.BasedOn = Convert.ToString(reader["BasedOn"]);
                    List<ReportingEntityProperty> entityPropertyList = new List<ReportingEntityProperty>();
                    entityPropertyList = ReportingFacade.GetEntityPropertyInfo(Convert.ToInt32(reader["ID"]), reportEntityInfo, _coreSessionUserName);
                    reportEntityInfo.Properties = entityPropertyList;

                    return reportEntityInfo;
                }

            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetFilteredReportEntitiesAction " + ex.Message, ex, _userName);
                throw;
            }
            return new ReportEntity ();
        }
    }
}
