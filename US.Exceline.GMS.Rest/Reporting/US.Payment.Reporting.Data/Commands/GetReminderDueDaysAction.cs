﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class GetReminderDueDaysAction : USDBActionBase<int>
    {

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSGetReminderDueDays";
            int reminderDueDays = 0;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                reminderDueDays = Convert.ToInt32(command.ExecuteScalar());
                return reminderDueDays;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
