﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class ExecuteReportScheduleTaskAction : USDBActionBase<bool>
    {
        private string _gymCode = string.Empty;
        public ExecuteReportScheduleTaskAction(string gymCode)
        {
            _gymCode = gymCode;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.GetUS_NotificationSchedule_Today";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report :" + _gymCode + " : Exception Occour while SendEmailOrSMSAction " + ex.Message, ex, "");
                throw;
            }
        }
    }
}
