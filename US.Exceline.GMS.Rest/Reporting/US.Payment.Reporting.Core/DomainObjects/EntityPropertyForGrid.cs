﻿
namespace US.Payment.Reporting.Core.DomainObjects
{
    public class EntityPropertyForGrid
    {
        private int _entityId = 0;

        public int EntityId
        {
            get { return _entityId; }
            set { _entityId = value; }
        }
        private string _entityName = string.Empty;

        public string EntityName
        {
            get { return _entityName; }
            set { _entityName = value; }
        }
        private int _propertyId = 0;

        public int PropertyId
        {
            get { return _propertyId; }
            set { _propertyId = value; }
        }
        private string _propertyName = string.Empty;

        public string PropertyName
        {
            get { return _propertyName; }
            set { _propertyName = value; }
        }
        private string _propertyReturnName = string.Empty;

        public string PropertyReturnName
        {
            get { return _propertyReturnName; }
            set { _propertyReturnName = value; }
        }
        
        private bool _isValid = true;
        public bool IsValid
        {
            get { return _isValid; }
            set { _isValid = value; }
        }
    }
}
