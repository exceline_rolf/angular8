﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class ReportSetting
    {
        private string _key = string.Empty;

        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }
        private string _keyValue = string.Empty;

        public string KeyValue
        {
            get { return _keyValue; }
            set { _keyValue = value; }
        }
    }
}
