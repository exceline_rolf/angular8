﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class WhereCondition
    {
        private Operand _leftOperand = new Operand();

        public Operand LeftOperand
        {
            get { return _leftOperand; }
            set { _leftOperand = value; }
        }
        private string _operator = string.Empty;

        public string Operator
        {
            get { return _operator; }
            set { _operator = value; }
        }
        private Operand _rightOperand = new Operand();

        public Operand RightOperand
        {
            get { return _rightOperand; }
            set { _rightOperand = value; }
        }
        private string _conjuntion = string.Empty;

        public string Conjuntion
        {
            get { return _conjuntion; }
            set { _conjuntion = value; }
        }
    }
}
