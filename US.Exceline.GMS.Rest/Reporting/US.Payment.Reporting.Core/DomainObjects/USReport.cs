﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class USReport
    {
        private int _id = -1;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _category = string.Empty;

        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }
        private string _name = string.Empty;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _description = string.Empty;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _recordCount = string.Empty;

        public string RecordCount
        {
            get { return _recordCount; }
            set { _recordCount = value; }
        }


        private string _assignType = string.Empty;

        public string AssignType
        {
            get { return _assignType; }
            set { _assignType = value; }
        }



        private int _assignToUserId = 0;

        public int AssignToUserId
        {
            get { return _assignToUserId; }
            set { _assignToUserId = value; }
        }


        private int _assignToRoleId = 0;

        public int AssignToRoleId
        {
            get { return _assignToRoleId; }
            set { _assignToRoleId = value; }
        }



        private string _isFollowUp = string.Empty;

        public string IsFollowUp
        {
            get { return _isFollowUp; }
            set { _isFollowUp = value; }
        }

        private string _isOneTimeFollowUpList = string.Empty;

        public string IsOneTimeFollowUpList
        {
            get { return _isOneTimeFollowUpList; }
            set { _isOneTimeFollowUpList = value; }
        }

        private string _entityName = string.Empty;

        public string EntityName
        {
            get { return _entityName; }
            set { _entityName = value; }
        }

        private int _columnCount = 0;

        public int CoulmnCount
        {
            get { return _columnCount; }
            set { _columnCount = value; }
        }

        private int _isCustomQuery = 0;

        public int IsCustomQuery
        {
            get { return _isCustomQuery; }
            set { _isCustomQuery = value; }
        }
        private List<ReportEntity> _entities = new List<ReportEntity>();

        public List<ReportEntity> EntityList
        {
            get { return _entities; }
            set { _entities = value; }
        }
        private List<UserRole> _roleList = new List<UserRole>();

        public List<UserRole> RoleList
        {
            get { return _roleList; }
            set { _roleList = value; }
        }
        private List<ConditionExpressionItem> _whereConditionList = new List<ConditionExpressionItem>();

        public List<ConditionExpressionItem> WhereConditionList
        {
            get { return _whereConditionList; }
            set { _whereConditionList = value; }
        }
        private bool _fixReport = false;

        public bool FixReport
        {
            get { return _fixReport; }
            set { _fixReport = value; }
        }
        public bool EnableQueryEditing
        {
            set
            {
                //Nothing to do
            }
            get
            {
                return !_fixReport;
            }
        }
        public string EditOrFixed
        {
            set { }
            get
            {
                if (_fixReport)
                {
                    return "Re-Upload";
                }
                else
                {
                    return "Edit";
                }
            }
        }
        private string _queryForReport = string.Empty;

        public string QueryForReport
        {
            get { return _queryForReport; }
            set { _queryForReport = value; }
        }
        public string UploadVisible
        {
            set { }
            get
            {
                if (_fixReport)
                {
                    return "Visible";
                }
                else
                {
                    return "Collapsed";
                }
            }
        }
        public string EditVisible
        {
            set { }
            get
            {
                if (!_fixReport)
                {
                    return "Visible";
                }
                else
                {
                    return "Collapsed";
                }
            }
        }

        //Scheduling....

        private bool _isScheduleReport = false;

        public bool IsScheduleReport
        {
            get { return _isScheduleReport; }
            set { _isScheduleReport = value; }
        }

        private string _scheduleVisibility = "Collapsed";

        public string ScheduleVisibility
        {
            get
            {
                if (IsScheduleReport)
                    return "Visible";
                return "Collapsed";
            }
            set { _scheduleVisibility = value; }
        }

        private List<ReportSchedule> _reportScheduleList = null;

        public List<ReportSchedule> ReportScheduleList
        {
            get { return _reportScheduleList; }
            set { _reportScheduleList = value; }
        }

        private string _createUser = string.Empty;
        public string CreateUser
        {
            get { return _createUser; }
            set { _createUser = value; }
        }

        private DateTime _createDate;
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        private string _lastModifiedUser = string.Empty;
        public string LastModifiedUser
        {
            get { return _lastModifiedUser; }
            set { _lastModifiedUser = value; }
        }

        private DateTime _lastModifiedDate;
        public DateTime LastModifiedDate
        {
            get { return _lastModifiedDate; }
            set { _lastModifiedDate = value; }
        }

        private bool _isEditable;
        public bool IsEditable
        {
            get { return _isEditable; }
            set { _isEditable = value; }
        }

    }
}
