﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class ReportScheduleTemplate
    {
        private int _templateId = 0;

        public int TemplateId
        {
            get { return _templateId; }
            set { _templateId = value; }
        }
        private string _templateName = string.Empty;

        public string TemplateName
        {
            get { return _templateName; }
            set { _templateName = value; }
        }
        private string _entity = string.Empty;

        public string Entity
        {
            get { return _entity; }
            set { _entity = value; }
        }
        private string _category = string.Empty;

        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }
        private string templateText = string.Empty;

        public string TemplateText
        {
            get { return templateText; }
            set { templateText = value; }
        }
    }
}
