﻿
namespace US.Payment.Reporting.Core.DomainObjects
{
    public class Operand
    {
        private bool _isFixedValue = false;

        public bool IsFixedValue
        {
            get { return _isFixedValue; }
            set { _isFixedValue = value; }
        }
        private ReportEntity _rEntity = new ReportEntity();

        public ReportEntity REntity
        {
            get { return _rEntity; }
            set { _rEntity = value; }
        }
        private ReportingEntityProperty _valueProperty = new ReportingEntityProperty();

        public ReportingEntityProperty ValueProperty
        {
            get { return _valueProperty; }
            set { _valueProperty = value; }
        }
        private string _fixedValue = string.Empty;

        public string FixedValue
        {
            get { return _fixedValue; }
            set { _fixedValue = value; }
        }
        //private string _conditionDisplay = string.Empty;

        public string ConditionDisplay
        {
            get 
            {
                if (_isFixedValue)
                {
                    return _fixedValue;
                }
                else
                {
                    return _rEntity.DisplayName + "->" + _valueProperty.DisplayName ;
                }
            }
            //set { _conditionDisplay = value; }
        }
       
    }
}
