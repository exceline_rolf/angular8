﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Notification;

namespace US.Common.Notification.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.`
    [ServiceContract]
    public interface IUSNotificationService
    {
        [OperationContract]
        int AddNotification(USNotificationTree notification);

        [OperationContract]
        bool UpdateNotification(USNotification notification);

        [OperationContract]
        bool UpdateNotificationStatus(List<int> selectedIds, int status, string user);

        [OperationContract]
        List<USNotificationSummaryInfo> GetNotificationSummaryList(int branchId, NotificationStatusEnum status, string user);

        [OperationContract]
        List<USNotificationSummaryInfo> GetNotificationSearchResult(int branchId, string user, string title, string assignTo, DateTime? receiveDateFrom, DateTime? receiveDateTo, DateTime? dueDateFrom, DateTime? dueDateTo, List<int> severityIdList, List<int> typeIdList, int statusId);

        [OperationContract]
        USNotificationSummaryInfo GetNotificationByIdAction(int notificationId, string user);

        [OperationContract]
        TemplateFieldEnum GetEnumValue();

        [OperationContract]
        TemplateField GetTemplateFieldValue();


        [OperationContract]
        NotifyMethodType GetMethodEnumValue();

        [OperationContract]
        List<CategoryDC> GetCategories(string type, string user, int branchId);

        [OperationContract]
        List<CategoryDC> GetTemplateTextKeyByEntity(string type, int entity, string user);

        [OperationContract]
        List<EntityDC> GetEntityList(string user);

        [OperationContract]
        TextTemplateEnum GetTemplateTextEnumValue();

        [OperationContract]
        int SaveNotificationTextTemplate(int entityId, NotificationTemplateText templatetText, int branchId, string loggedUser);

        [OperationContract]
        List<NotificationTemplateText> GetNotificationTemplateText(string methodCode, string typeCode, string user);

        [OperationContract]
        NotificationTemplateText GetNotificationTextByType(NotifyMethodType method, TextTemplateEnum type, string user, int branchId);

        [OperationContract]
        List<USNotificationSummaryInfo> GetNotificationByNotifiyMethod(int memberId, string user, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod, int branchId);
        
        [OperationContract]
        List<USNotificationSummaryInfo> GetChangeLogNotificationList(string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod);

         [OperationContract]
        List<InstructorDC> GetInstructors(int branchId, string searchText, string user, bool isActive);

        [OperationContract]
        bool AddNotificationWithReportScheduler(string gymCode, string user);

        [OperationContract]
        bool UpdateNotificationWithReceiveSMS(string gymCompanyRef, string notificationCode, string message, string sender, string user);

        [OperationContract]
        int SaveCategory(CategoryDC category, string user, int branchId);

        [OperationContract]
        List<CategoryTypeDC> GetCategoryTypes(string code, string user, int branchId);

        [OperationContract]
        List<AccessDeniedLog> GetAccessDeniedList(DateTime fromDate, DateTime toDate, string user, int branchId);
    }
}
