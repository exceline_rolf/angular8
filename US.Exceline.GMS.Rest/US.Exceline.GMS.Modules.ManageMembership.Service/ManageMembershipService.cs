﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using US.Common.Web.UI.Core.SystemObjects;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;
using US.Communication.Core.SystemCore.DomainObjects;
using US.Communication.Data.Output;
using US.Communication.Data.ActionAndParameters;
using US.Communication.API;
using US.Exceline.GMS.Modules.Admin.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.API;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.Exceline.GMS.Modules.Admin.API.ManageResources;
using US.Payment.Core.ResultNotifications;
using US.GMS.Core.Utils;
using US.Common.Logging.API;
using US.Common.USSAdmin.API;
using US.Exceline.GMS.Modules.Shop.API.InventoryManagement;
using US.Communication.Core.Data;
using US.Exceline.GMS.Modules.Admin.API.ManageContract;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.API;
using US.Payment.Core.Enums;
using US.Common.Notification.Core.Enums;
using System.Threading;
using System.IO;
using US.Exceline.GMS.Modules.Shop.API.ManageShop;
using US.Exceline.GMS.Modules.Admin.API.ManageMembers;
using SessionManager;
using US.GMS.Core.DomainObjects.Economy;
using US.Exceline.GMS.Modules.Login.API;
using System.Web;
using System.ServiceModel.Activation;
using US.Common.Web.UI.Core.Common;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.ManageMembership.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ManageMembershipService : IManageMembershipService
    {

        public string SaveMember(OrdinaryMemberDC member,  string user, int branchId, string notificationTitle)
        {
            var result = GMSManageMembership.SaveMember(member, GetConfigurations(user), ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return "-1";
            }
            else
            {
                if (member != null)
                {
                    if (member.IntroducedById > 0)
                    {
                        try
                        {
                            USImportResult<NotificationTemplateText> notificationResult = NotificationAPI.GetNotificationTextByType(NotifyMethodType.POPUP, TextTemplateEnum.INTRODUCEDBY, user, branchId);
                            string textTemplate = string.Empty;
                            if (!notificationResult.ErrorOccured && notificationResult.MethodReturnValue != null)
                            {
                                textTemplate = notificationResult.MethodReturnValue.Text;
                            }
                            List<TemplateField> templateFieldList = new List<TemplateField>();
                            templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.MEMBERNO, Value = member.CustId });
                            templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.MEMBERNAME, Value = member.FirstName + " " + member.LastName });
                            templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.INTRODUCEDNO, Value = member.IntroducedByCustId });
                            templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.INTRODUCEDNAME, Value = member.IntroducedByName });
                            templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.DATE, Value = DateTime.Now.ToShortDateString() });
                            templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.CREATEDUSER, Value = user });
                            string text = NotificationAPI.ConvertTemplateToText(textTemplate, templateFieldList);

                            if (!string.IsNullOrEmpty(text.Trim()))
                            {
                                USNotificationTree noitificationTree = new USNotificationTree();
                                noitificationTree.BranchId = member.BranchId;
                                noitificationTree.MemberId = member.IntroducedById;
                                USNotification notification = new USNotification();
                                notification.Module = "Manage MemberShip";
                                if (string.IsNullOrEmpty(notificationTitle.Trim()))
                                {
                                    notificationTitle = "Intoduced By member";
                                }
                                notification.Title = notificationTitle;
                                notification.Description = text;
                                notification.CreatedDate = DateTime.Now;
                                notification.CreatedUser = user;
                                notification.TypeId = 3;
                                notification.SeverityId = 3;
                                notification.CreatedUserRoleId = "MEM";
                                notification.DueDate = DateTime.Now.AddDays(30);
                                notification.StatusId = 1;
                                noitificationTree.Notification = notification;
                                USNotificationAction action = new USNotificationAction();
                                action.Comment = "Notification was saved automatically";
                                action.AssgnId = user;
                                noitificationTree.Action = action;
                                List<USNotificationChannel> channelList = new List<USNotificationChannel>();
                                USNotificationChannel channel = new USNotificationChannel();
                                channel.ChannelDueDate = DateTime.Now.AddDays(30);
                                channel.MessageType = NotifyMethodType.POPUP.ToString();
                                channel.OccuranceType = OccuranceType.DAILY.ToString();
                                channel.ChannelMessage = "New Message";
                                channel.ChannelAssign = user;
                                channel.ChannelStatus = true;
                                channelList.Add(channel);
                                noitificationTree.NotificationMethods = channelList;
                                NotificationAPI.AddNotification(noitificationTree);
                            }
                        }
                        catch
                        {
                        }
                    }
                }
                return result.OperationReturnValue;
            }
        }

        public bool UpdateExternalCustomerId(int memberId, int userId,string user)
        {
            OperationResult<bool> result = GMSManageMembership.UpdateExternalCustomerId(memberId, userId, ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<WithdrawalDC> GetWithdrawalByMemberId(DateTime startDate, DateTime endDate, WithdrawalTypes type, int memberId, string user)
        {
            OperationResult<List<WithdrawalDC>> result = GMSShopManager.GetWithdrawalByMemberId(startDate, endDate, type, memberId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<WithdrawalDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public USPUser ValidateUserWithCard(string cardNumber, string user)
        {
            OperationResultValue<USPUser> result = AdminUserSettings.ValidateUserWithCard(cardNumber, user);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), cardNumber);
                }
                return new USPUser();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ShopBillDetailDC GetShopSaleBillDetail(int saleId, int branchId, string user)
        {
            OperationResult<ShopBillDetailDC> result = GMSShopManager.GetShopSaleBillDetail(saleId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new ShopBillDetailDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public bool ValidateUser(string userName, string password)
        {
            OperationResultValue<bool> result = AdminUserSettings.ValidateUser(userName, password);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), userName);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<OrdinaryMemberDC> GetGroupMemebersByGroup(int branchId, string searchText, bool IsActive, int groupId, string user)
        {
            var result = GMSManageMembership.GetGroupMemebersByGroup(branchId, searchText, IsActive, groupId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<OrdinaryMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public SalePointDC GetSalesPointByMachineName(int branchID, string machineName, string user)
        {
            OperationResult<SalePointDC> result = GMSShopManager.GetSalesPointByMachineName(branchID, machineName, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new SalePointDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public USNotificationSummaryInfo GetNotificationById(int notificationId, string user)
        {
            USImportResult<USNotificationSummaryInfo> result = NotificationAPI.GetNotificationByIdAction(notificationId, user);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return new USNotificationSummaryInfo();

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public int SavePointOfSale(int branchId, SalePointDC pointOfSale, string user)
        {
            OperationResult<int> result = GMSShopManager.SavePointOfSale(branchId, pointOfSale, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateNotificationStatus(List<int> selectedIds, int status, string user)
        {
            USImportResult<bool> result = NotificationAPI.UpdateNotificationStatus(selectedIds, status, user);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                return false;
            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public List<USNotificationSummaryInfo> GetNotificationSearchResult(int memberId, string user, string title, string assignTo, DateTime? receiveDateFrom, DateTime? receiveDateTo, DateTime? dueDateFrom, DateTime? dueDateTo, List<int> severityIdList, List<int> typeIdList, int statusId)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = NotificationAPI.GetMemberNotificationSearchResult(memberId, user, title, assignTo, receiveDateFrom, receiveDateTo, dueDateFrom, dueDateTo, severityIdList, typeIdList, statusId);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return new List<USNotificationSummaryInfo>();
            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public NotificationTypeEnum GetNotificationType()
        {
            return NotificationTypeEnum.Messages;
        }

        public NotificationSeverityEnum GetSeverity()
        {
            return NotificationSeverityEnum.Minor;
        }

        public NotificationStatusEnum GetNotificationStatus()
        {
            return NotificationStatusEnum.New;
        }

        public List<USNotificationSummaryInfo> GetNotificationSummaryList(int memberId, NotificationStatusEnum status, string user)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = NotificationAPI.GetMemberNotificationSummaryList(memberId, status, user);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return new List<USNotificationSummaryInfo>();
            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public List<NotificationTemplateText> GetNotificationTemplateText(string user, int branchId)
        {
            USImportResult<List<NotificationTemplateText>> result = NotificationAPI.GetNotificationTemplateText("","",user);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return new List<NotificationTemplateText>();
            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        private string GetConfigurations(string user)
        {
            var imageSavepath = string.Empty;
            try
            {
                imageSavepath = ConfigurationSettings.AppSettings["ImageFolderPath"].ToString();
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Error in getting Image folder path " + ex.Message, new Exception(), user);
            }
            return imageSavepath;
        }

        public string UpdateMember(OrdinaryMemberDC member,  string user)
        {
            var result = GMSManageMembership.UpdateMember(member, GetConfigurations(user), ExceConnectionManager.GetGymCode(user), user);

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return "-1";
            }
            return result.OperationReturnValue;
        }

        public NotificationTemplateText GetNotificationTextByType(NotifyMethodType method, TextTemplateEnum type, string user, int branchId)
        {
            USImportResult<NotificationTemplateText> result = NotificationAPI.GetNotificationTextByType(method, type, user, branchId);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                return new NotificationTemplateText();
            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public TemplateField GetTemplateFieldValue()
        {
            return new TemplateField();
        }

        public bool DisableMember(int memberId, string user, bool activeState, string comment)
        {
            var result = GMSManageMembership.DisableMember(memberId, activeState, comment, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineMemberDC> GetMembers(int branchId, string user, string searchText, int statuse, MemberSearchType searchType, MemberRole memberRole, int hit, bool isHeaderClick, bool isAscending, string sortName)
        {
            
          //  HttpContext.Current.Session["ExorMemberDetails"] = "Nuwan sanjeewa";
            var result = GMSManageMembership.GetMembers(branchId, searchText, statuse, searchType, memberRole, user, ExceConnectionManager.GetGymCode(user), hit, isHeaderClick, isAscending, sortName);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<PackageDC> GetContracts(int branchId, int contractType, string user)
        {
            var result = GMSManageMembership.GetContracts(branchId, user, contractType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<PackageDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SetContractSequenceID(List<PackageDC> contractList, int branchID, string user)
        {
            var result = GMSManageMembership.SetContractSequenceID(contractList, branchID, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return true;
            }
            return result.OperationReturnValue;
        }

        public bool DeleteCreditNote(int creditNoteId, string user)
        {
            var result = GMSManageMembership.DeleteCreditNote(creditNoteId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string ResignContract(ContractResignDetailsDC resignDetails, string user, int branchId, string notificationMethod, string senderDescription)
        {
            try
            {
                var result = GMSManageMembership.ResignContract(resignDetails, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    foreach (var message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return string.Empty;
                }

                if (notificationMethod == "EVENTLOG")
                {
                    PrintResignDocument(resignDetails, user, branchId);
                    return "SUCCESS";
                }
                if (notificationMethod == "PRINT")
                {
                    return PrintResignDocument(resignDetails, user, branchId);
                }
                else
                {
                    PrintResignDocument(resignDetails, user, branchId);
                }
                SendResignNotification(resignDetails, branchId, user, senderDescription, notificationMethod);
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                return "ERROR";
            }
        }

        private string PrintResignDocument(ContractResignDetailsDC resignDetails, string user, int branchId)
        {
            var memnberContractID = resignDetails.MemberContractId.ToString();
            var contractEndDate = resignDetails.ContractEndDate.Value.ToString("dd.MM.yyyy");
            var numberofOrder = "0";
            var totalOrderAmount = "0";
            var lastDueDate = "";

            if (resignDetails.RemainingInstallments.Count > 0)
            {
                numberofOrder = resignDetails.RemainingInstallments.Count.ToString();
                totalOrderAmount = resignDetails.RemainingInstallments.Sum(X => X.Amount).ToString("N");
                lastDueDate = resignDetails.RemainingInstallments.Max(X => X.DueDate).ToString("dd.MM.yyyy");
            }

            var appUrl = ConfigurationManager.AppSettings["USC_Resign_Print_App_Path"];
            var dataDictionary = new Dictionary<string, string>() { { "MemberContractID", memnberContractID }, { "ContractEndDate", contractEndDate }, { "NumberOfOrder", numberofOrder }, { "TotalOrderAmount", totalOrderAmount }, { "LastDueDate", lastDueDate }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
            var pdfFileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
            SetPDFFileParth(resignDetails.MemberContractId, pdfFileParth, "RESG", branchId, user);

            if (!string.IsNullOrEmpty(pdfFileParth))
                return pdfFileParth;
            return "PRINTERROR";
        }

        private int SendResignNotification(ContractResignDetailsDC resignDetails, int branchID, string user, string senderDescription, string notifyMethod)
        {
            try
            {
                // create parameter string for notification
                var application = string.Empty;
                var outputFormat = string.Empty;

                USCommonNotificationDC commonNotification = null;
                var nMethod = NotificationMethodType.NONE;
                switch (notifyMethod)
                {
                    case "SMS":
                        nMethod = NotificationMethodType.SMS;
                        application = ConfigurationManager.AppSettings["USC_Notification_SMS"];
                        outputFormat = "SMS";
                        break;
                    case "EMAIL":
                        nMethod = NotificationMethodType.EMAIL;
                        application = ConfigurationManager.AppSettings["USC_Notification_Email"];
                        outputFormat = "EMAIL";
                        break;
                }

                var numberofOrder = resignDetails.RemainingInstallments.Count;
                DateTime? lastDueDate = null;
                if (resignDetails.RemainingInstallments.Count > 0)
                {
                    decimal totalOrderAmount = resignDetails.RemainingInstallments.Sum(X => X.Amount);
                    lastDueDate = resignDetails.RemainingInstallments.Max(X => X.DueDate);

                    var paraList = new Dictionary<TemplateFieldEnum, string>
                    {
                        {TemplateFieldEnum.CONTRACTNO, resignDetails.MemberContractNo},
                        {TemplateFieldEnum.CONTRACTENDDATE, resignDetails.ContractEndDate.Value.ToShortDateString()},
                        {TemplateFieldEnum.NOOFORDERS, numberofOrder.ToString()},
                        {TemplateFieldEnum.TOTALORDERAMOUNT, totalOrderAmount.ToString()},
                        {TemplateFieldEnum.DUEDATEFORLASTORDER, lastDueDate.Value.ToShortDateString()}
                    };

                    // create common notification
                    commonNotification = new USCommonNotificationDC
                    {
                        ParameterString = paraList,
                        BranchID = branchID,
                        CreatedUser = user,
                        Role = "MEM",
                        NotificationType = NotificationTypeEnum.Messages,
                        Severity = NotificationSeverityEnum.Minor,
                        MemberID = resignDetails.MemberId,
                        Status = NotificationStatusEnum.New,
                        Method = nMethod,
                        Type = TextTemplateEnum.RESIGNSMS,
                        Title = "RESIGN CONTRACT",
                        SenderDescription = senderDescription,
                        IsTemplateNotification = true
                    };
                }
                else
                {
                    var paraList = new Dictionary<TemplateFieldEnum, string>
                    {
                        {TemplateFieldEnum.CONTRACTNO, resignDetails.MemberContractNo},
                        {TemplateFieldEnum.CONTRACTENDDATE, resignDetails.ContractEndDate.Value.ToShortDateString()},
                    };

                    // create common notification
                    commonNotification = new USCommonNotificationDC
                    {
                        ParameterString = paraList,
                        BranchID = branchID,
                        CreatedUser = user,
                        Role = "MEM",
                        NotificationType = NotificationTypeEnum.Messages,
                        Severity = NotificationSeverityEnum.Minor,
                        MemberID = resignDetails.MemberId,
                        Status = NotificationStatusEnum.New,
                        Method = nMethod,
                        Type = TextTemplateEnum.RESIGNSMSPAID,
                        Title = "RESIGN CONTRACT",
                        SenderDescription = senderDescription,
                        IsTemplateNotification = true
                    };
                }


                var result = NotificationAPI.AddNotification(commonNotification);
                if (result.ErrorOccured)
                {
                    foreach (var mgs in result.NotificationMessages.Where(mgs => mgs.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error))
                    {
                        USLogError.WriteToFile(mgs.Message, new Exception(), user);
                    }
                    return 0;
                }

                if (notifyMethod == "SMS")
                {
                    try
                    {
                        // send SMS through the USC API
                        var smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                        var status = NotificationStatusEnum.Attended;
                        string statusMessage = string.Empty;
                        if (smsStatus != null)
                        {
                            status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                            statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                        }

                        // update status in notification table
                        USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                        if (updateResult.ErrorOccured)
                        {
                            foreach (USNotificationMessage message in updateResult.NotificationMessages)
                            {
                                if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                {
                                    USLogError.WriteToFile(message.Message, new Exception(), user);
                                }
                            }
                            return 0;
                        }
                    }
                    catch
                    {
                        return 0;
                    }
                }
                else if (notifyMethod == "EMAIL")
                {
                    // send EMAIL through the USC API
                    IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                    NotificationStatusEnum status = NotificationStatusEnum.Attended;
                    string statusMessage = string.Empty;
                    if (emailStatus != null)
                    {
                        status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                        statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                    }

                    // update status in notification table
                    USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                    if (updateResult.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in updateResult.NotificationMessages)
                        {
                            if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        return 0;
                    }
                    return 1;
                }
                return 1;
            }
            catch (Exception e)
            {
                USLogError.WriteToFile(e.Message, new Exception(), user);
                throw e;
            }
        }

        public InstallmentDC GetMemberContractLastInstallment(int memberContractId, string user)
        {
            var result = GMSManageMembership.GetMemberContractLastInstallment(memberContractId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<InstallmentDC> GetMemberOrders(int memberId, string type, string user)
        {
            var result = GMSManageMembership.GetMemberOrders(memberId, type, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstallmentDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<InstallmentDC> GetSponsorOrders(List<int> sponsors, string user)
        {
            var result = GMSManageMembership.GetSponsorOrders(sponsors, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstallmentDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ContractSaveResultDC SaveMemberContract(MemberContractDC memberContract, string user, int branchId, string notificationTitle)
        {
            var result = GMSManageMembership.SaveMemberContract(memberContract, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new ContractSaveResultDC() { MemberContractId = -1, MemberContractNo = string.Empty, IsGymChanged = false };
            }
            return result.OperationReturnValue;
        }

        public int UpdateMemberContract(MemberContractDC memberContract, string user, int branchId, string notificationTitle)
        {
            var result = GMSManageMembership.UpdateMemberContract(memberContract, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            return result.OperationReturnValue;
        }

        public MemberContractDC GetMemberContractDetails(int contractId, string user, int branchId)
        {
            var result = GMSManageMembership.GetMemberContractDetails(contractId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int FreezeMemberContract(int memberContractId, string user)
        {
            return 1;
        }

        public List<ContractItemDC> GetContractItems(int contractId, int branchId, string user)
        {
            var result = GMSManageMembership.GetContractTemplateItems(contractId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ContractItemDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ActivityDC> GetActivities(int branchId, string user)
        {
            var result = US.Exceline.GMS.API.GMSContract.GetActivities(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ActivityDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<USNotificationSummaryInfo> GetNotificationByNotifiyMethod(int memberId, string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = NotificationAPI.GetNotificationByNotifiyMethod(memberId, user, branchId, fromDate, toDate, notifyMethod);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                return new List<USNotificationSummaryInfo>();

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public List<InstallmentDC> GetInstallments(int branchId, string user, int memberContractId)
        {
            var result = GMSManageMembership.GetInstallments(memberContractId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstallmentDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public MemberContractDC RenewMemberContract(MemberContractDC renewedContract, List<InstallmentDC> installmenList, string user, int branchId)
        {
            var result = GMSManageMembership.RenewMemberContract(renewedContract, installmenList, ExceConnectionManager.GetGymCode(user), branchId, false, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                var contractResult = GMSManageMembership.GetMemberContractDetails(renewedContract.Id, branchId, ExceConnectionManager.GetGymCode(user));
                if (contractResult.ErrorOccured)
                {
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                else
                {
                    return contractResult.OperationReturnValue;
                }
            }
        }

        public EntityVisitDetailsDC GetMemberVisits(int memberId, DateTime selectedDate, int branchId, string user)
        {
            var result = GMSManageMembership.GetMemberVisits(memberId, selectedDate, branchId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                result.OperationReturnValue.GymList = GMSSystemSettings.GetBranches(user, ExceConnectionManager.GetGymCode(user), -1).OperationReturnValue.ToList();
                return result.OperationReturnValue;
            }
        }

        public int SaveMemberVisit(EntityVisitDC memberVisit, string user)
        {
            memberVisit.IsVisit = true;
            var result = GMSManageMembership.SaveMemberVisit(memberVisit, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                
                return result.OperationReturnValue;
            }
        }

        public bool DeleteMemberVisit(int memberVisitId, int memberContractId, string user)
        {
            var result = GMSManageMembership.DeleteMemberVisit(memberVisitId, memberContractId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }



        public bool UpdateMemberInstallment(bool isEditedFromUI, InstallmentDC installment, string user, int branchId, string notificationTitle)
        {
            var result = GMSManageMembership.UpdateMemberInstallment(installment, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateMemberAddonInstallments(List<InstallmentDC> installments, string user)
        {
            var result = GMSManageMembership.UpdateMemberAddonInstallments(installments, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteInstallment(List<int> installmentIdList, int memberContractId, string user)
        {
            var result = GMSManageMembership.DeleteInstallment(installmentIdList, memberContractId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SwitchPayer(int memberId, int payerId, DateTime activatedDate, bool isSave, string user)
        {
            var result = GMSManageMembership.SwitchPayer(memberId, payerId, activatedDate, isSave, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return 0;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool IntroducePayer(int memberId, int introduceId, string user)
        {
            var result = GMSManageMembership.IntroducePayer(memberId, introduceId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool CancelMemberContract(int branchId, string user, int memberContractId, string canceledBy, string comment, List<int> installmentIds, int minNumber, int tergetInstallmentId)
        {
            var result = GMSManageMembership.CancelMemberContract(memberContractId, canceledBy, comment, installmentIds, minNumber, tergetInstallmentId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<MemberContractDC> GetMemberContractsForActivity(int memberID, int activityId, int branchId, string user)
        {
            var result = GMSManageMembership.GetMemberContractsForActivity(memberID, activityId, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<MemberContractDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ClassDetailDC> GetClassByMemberId(int branchId, string user, int memberId, DateTime? classDate)
        {
            var result = GMSManageMembership.GetClassByMemberId(branchId, memberId, classDate, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ClassDetailDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool AddFamilyMember(int memberId, int familyMemberId, int memberContractId, string user)
        {
            var result = GMSManageMembership.AddFamilyMember(memberId, familyMemberId, memberContractId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<InstallmentDC> UpdateInstallmentsWithMemberContract(string user, decimal installmentAmount, int updatingInstallmentNo, MemberContractDC memberContract)
        {
            var result = GMSManageMembership.UpdateInstallmentWithMemberContract(installmentAmount, updatingInstallmentNo, memberContract, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstallmentDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveSponsor(int memberId, int sponsorId, int branchId, string user)
        {
            var result = GMSManageMembership.SaveSponsor(memberId, sponsorId, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string SaveMemberParent(OrdinaryMemberDC memberParent, int branchId, string user)
        {
            var result = GMSManageMembership.SaveMemberParent(memberParent, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return "error";
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public OrdinaryMemberDC GetMemberDetailsByMemberId(int branchId, string user, int memberId, string memberRole)
        {
            var result = GMSManageMembership.GetMemberDetailsByMemberId(branchId, user, memberId, memberRole, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {

                //if (result.OperationReturnValue != null)
                //{
                //    USImportResult<List<USNotificationSummaryInfo>> notifcationrResult = new USImportResult<List<USNotificationSummaryInfo>>();
                //    notifcationrResult = NotificationAPI.GetNotificationsByMemberId(memberId, user);
                //    if (!notifcationrResult.ErrorOccured)
                //    {
                //        result.OperationReturnValue.NotificationList = notifcationrResult.MethodReturnValue;
                //        if (result.OperationReturnValue.NotificationList.Count > 0)
                //        {
                //            result.OperationReturnValue.HasNotifications = true;
                //        }
                //    }

                //}
                return result.OperationReturnValue;
            }
        }

        public List<CategoryDC> GetInterestCategoryByMember(string user, int memberId, int branchId)
        {
            var result = GMSManageMembership.GetInterestCategoryByMember(memberId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveInterestCategoryByMember(string user, int memberId, int branchId, List<CategoryDC> interestCategoryList)
        {
            var result = GMSManageMembership.SaveInterestCategoryByMember(memberId, branchId, interestCategoryList, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage massage in result.Notifications)
                {
                    USLogError.WriteToFile(massage.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<OrdinaryMemberDC> GetSwitchPayers(string user, int branchId, int memberId)
        {
            var result = GMSManageMembership.GetSwitchPayers(branchId, memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<OrdinaryMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineMemberDC> GetGroupMembers(int groupId, string user)
        {
            var result = GMSManageMembership.GetGroupMembers(groupId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ContractBookingDC> GetContractBookings(int branchId, string user, int membercontractId)
        {
            var result = GMSManageMembership.GetContractBookings(membercontractId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ContractBookingDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineMemberDC> GetFamilyMembers(int memberId, string user)
        {
            var result = GMSManageMembership.GetFamilyMembers(memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<FollowUpDC> GetFollowUps(int memberId, int followUpId, string user)
        {
            var result = GMSManageMembership.GetFollowUps(memberId, followUpId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<FollowUpDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<FollowUpTemplateTaskDC> GetFollowUpTask(string user)
        {
            var result = GMSManageMembership.GetFollowUpTask(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<FollowUpTemplateTaskDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveFollowUp(List<FollowUpDC> followUpList, string user)
        {
            var result = GMSManageMembership.SaveFollowUp(followUpList, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<USPRole> GetEmpRole(string user)
        {
            var result = AdminUserSettings.GetUSPRoles("", true, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }

                return new List<USPRole>();
            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public OrdinaryMemberDC GetEconomyDetails(int memberId, string user)
        {
            var result = GMSManageMembership.GetEconomyDetails(memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new OrdinaryMemberDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineMemberDC> GetListOfGuardian(int guardianId, string user)
        {
            var result = GMSManageMembership.GetListOfGuardian(guardianId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveEconomyDetails(OrdinaryMemberDC member, string user)
        {
            var result = GMSManageMembership.SaveEconomyDetails(member, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public OrdinaryMemberDC GetMemberInfoWithNotes(int branchId, int memberId, string user)
        {
            var result = GMSManageMembership.GetMemberInfoWithNotes(branchId, memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new OrdinaryMemberDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string SaveMemberInfoWithNotes(OrdinaryMemberDC member, string user)
        {
            member.ModifiedUser = user;
            var result = GMSManageMembership.SaveMemberInfoWithNotes(member, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<GymEmployeeDC> GetGymEmployees(int branchId, string searchText, string user, int roleId)
        {

            var result = GMSGymEmployee.GetGymEmployees(branchId, searchText, true,roleId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<GymEmployeeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public GymEmployeeDC GetGymEmployeeById(int branchId, int employeeId, string user)
        {
            var result = GMSGymEmployee.GetGymEmployeeById(branchId, employeeId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new GymEmployeeDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryDC> GetCategories(string type, string user, int branchId)
        {
            var result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId)
        {
            var result = GMSCategory.GetCategoryTypes(name, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryTypeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveCategory(CategoryDC category, string user, int branchId)
        {
            var result = GMSCategory.SaveCategory(category, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public SponsorSettingDC GetSponsorSetting(int branchId, string user, int sponsorId)
        {
            OperationResult<SponsorSettingDC> result = GMSManageMembership.GetSponsorSetting(branchId, user, sponsorId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new SponsorSettingDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<DiscountDC> GetGroupDiscountByType(int branchId, string user, int discountTypeId, int sponsorId)
        {
            OperationResult<List<DiscountDC>> result = GMSManageMembership.GetGroupDiscountByType(branchId, user, discountTypeId, ExceConnectionManager.GetGymCode(user), sponsorId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<DiscountDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveSposorSetting(SponsorSettingDC sponsorSetting, int branchId, string user)
        {
            OperationResult<bool> result = GMSManageMembership.SaveSposorSetting(sponsorSetting, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteDiscountCategory(int categoryID, string user)
        {
            var discountList = new List<int>();
            discountList.Add(categoryID);
            var result = GMSManageMembership.DeleteDiscount(discountList, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteEmployeeCategory(int categoryID, string user)
        {
            var result = GMSManageMembership.DeleteEmployeeCategory(categoryID, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool AddSponserShipForMember(int memContractId, int sponserContractId, int employeeCategotyID, string user, int actionType, string employeeRef)
        {
            var result = GMSManageMembership.AddSponserShipForMember(memContractId, sponserContractId, employeeCategotyID, actionType, employeeRef, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public SaleResultDC AddShopSales(InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberBranchID, int loggedBranchID, string user, bool printInvoice)
        {
            OperationResult<SaleResultDC> result = new OperationResult<SaleResultDC>();
             result = GMSShop.AddShopSales(installment, shopSaleDetails, paymentDetails, memberBranchID, loggedBranchID, user, ExceConnectionManager.GetGymCode(user), false,"");
            if (result.OperationReturnValue.SaleStatus == SaleErrors.NOSTOCK) //not enough  stock level in article
            {
                return result.OperationReturnValue;
            }
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return result.OperationReturnValue;
            }
            else
            {
                string PDFFileParth = string.Empty;
                try
                {
                    if (paymentDetails != null && paymentDetails.PayModes != null && result.OperationReturnValue.AritemNo > 0)
                    {
                        if (paymentDetails.PayModes.Where(x => x.PaymentTypeCode == "EMAILSMSINVOICE").FirstOrDefault() != null)
                        {
                            try
                            {
                                var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"];
                                var dataDictionary = new Dictionary<string, string>() { { "itemID", result.OperationReturnValue.AritemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                                PDFFileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                                SetPDFFileParth(result.OperationReturnValue.AritemNo, PDFFileParth, "INV", memberBranchID, user);

                                var InvoiceDetails = GMSInvoice.GetInvoiceDetails(result.OperationReturnValue.AritemNo, memberBranchID, ExceConnectionManager.GetGymCode(user));

                                if (InvoiceDetails.ErrorOccured)
                                {
                                    foreach (NotificationMessage message in InvoiceDetails.Notifications)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                    return null;
                                }
                                else
                                {
                                    ExcelineInvoiceDetailDC invoice = InvoiceDetails.OperationReturnValue;

                                    // create parameter string for notification
                                    Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                                    paraList.Add(TemplateFieldEnum.INVOICENO, invoice.InvoiceNo.ToString());
                                    paraList.Add(TemplateFieldEnum.DUEDATE, invoice.InvoiceDueDate.ToString());
                                    paraList.Add(TemplateFieldEnum.AMOUNT, invoice.InvoiceAmount.ToString());
                                    paraList.Add(TemplateFieldEnum.KID, invoice.BasicDetails.KID.ToString());
                                    paraList.Add(TemplateFieldEnum.GYMACCOUNT, invoice.OtherDetails.BranchAccountNo);
                                    paraList.Add(TemplateFieldEnum.GYMNAME, invoice.OtherDetails.BranchName.ToString());
                                    paraList.Add(TemplateFieldEnum.MEMBERNAME, invoice.CustomerName);

                                    // create common notification
                                    USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                                    commonNotification.IsTemplateNotification = true;
                                    commonNotification.ParameterString = paraList;
                                    commonNotification.BranchID = memberBranchID;
                                    commonNotification.CreatedUser = user;
                                    commonNotification.Role = "MEM";
                                    commonNotification.NotificationType = NotificationTypeEnum.Messages;
                                    commonNotification.Severity = NotificationSeverityEnum.Minor;
                                    commonNotification.MemberID = installment.MemberId;
                                    commonNotification.Status = NotificationStatusEnum.New;

                                    // FOR SMS **************************************************************
                                    commonNotification.Type = TextTemplateEnum.SMSINVOICE;
                                    commonNotification.Method = NotificationMethodType.SMS;
                                    commonNotification.Title = "INVOICE SMS";

                                    //add SMS notification into notification tables
                                    USImportResult<int> SMSresult = NotificationAPI.AddNotification(commonNotification);

                                    if (result.ErrorOccured)
                                    {
                                        foreach (USNotificationMessage message in SMSresult.NotificationMessages)
                                        {
                                            if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                            {
                                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                                        string outputFormat = "SMS";

                                        try
                                        {
                                            // send SMS through the USC API
                                            IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), SMSresult.MethodReturnValue.ToString());

                                            NotificationStatusEnum status = NotificationStatusEnum.Attended;
                                            if (smsStatus != null)
                                                status = ((from st in smsStatus.Messages where st.Header == "StatusMessage" select st.Message).ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                            var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                                            // update status in notification table
                                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(SMSresult.MethodReturnValue, user, status, statusMessage);

                                            if (updateResult.ErrorOccured)
                                            {
                                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                                {
                                                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                                    {
                                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            USLogError.WriteToFile(e.Message, new Exception(), user);
                                        }
                                    }

                                    // FOR EMAIL **************************************************************
                                    commonNotification.Type = TextTemplateEnum.EMAILINVOICE;
                                    commonNotification.Method = NotificationMethodType.EMAIL;
                                    commonNotification.AttachmentFileParth = PDFFileParth;
                                    commonNotification.Title = "INVOICE EMAIL";

                                    //add EMAIL notification into notification tables
                                    USImportResult<int> EMAILresult = NotificationAPI.AddNotification(commonNotification);

                                    if (result.ErrorOccured)
                                    {
                                        foreach (USNotificationMessage message in EMAILresult.NotificationMessages)
                                        {
                                            if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                            {
                                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
                                        string outputFormat = "EMAIL";

                                        try
                                        {
                                            // send EMAIL through the USC API
                                            IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), EMAILresult.MethodReturnValue.ToString());

                                            NotificationStatusEnum status = NotificationStatusEnum.Attended;

                                            if (smsStatus != null)
                                                status = ((from st in smsStatus.Messages where st.Header == "StatusMessage" select st.Message).ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                            var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                                            // update status in notification table
                                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(EMAILresult.MethodReturnValue, user, status, statusMessage);

                                            if (updateResult.ErrorOccured)
                                            {
                                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                                {
                                                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                                    {
                                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            USLogError.WriteToFile(e.Message, new Exception(), user);
                                        }
                                    }
                                }
                            }
                            catch
                            {
                            }
                        }

                        if (paymentDetails.PayModes.Where(x => x.PaymentTypeCode == "INVOICE").FirstOrDefault() != null && printInvoice)
                        {
                            var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"];
                            var dataDictionary = new Dictionary<string, string>() { { "itemID", result.OperationReturnValue.AritemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                            PDFFileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                            SetPDFFileParth(result.OperationReturnValue.AritemNo, PDFFileParth, "INV", memberBranchID, user);
                            PDFPrintResultDC printResult = new PDFPrintResultDC();
                            printResult.FileParth = PDFFileParth;
                            result.OperationReturnValue.PrintResult = printResult;
                            return result.OperationReturnValue;
                        }
                    }
                }
                catch
                {
                    result.OperationReturnValue.SaleStatus = SaleErrors.ERROR;
                    return result.OperationReturnValue;
                }
                return result.OperationReturnValue;
            }
        }

        public bool RemoveSponsorshipOfMember(int sponsoredRecordId, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = GMSManageMembership.RemoveSponsorshipOfMember(sponsoredRecordId, ExceConnectionManager.GetGymCode(user), user);

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<OrdinaryMemberDC> GetMembersByRoleType(int branchId, string user, int status, MemberRole roleType, string searchText)
        {
            var result = GMSManageMembership.GetMembersByRoleType(branchId, user, status, roleType, searchText, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<OrdinaryMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<EmployeeCategoryDC> GetSponsorEmployeeCategoryList(int branchId, string user, int sponsorId)
        {
            OperationResult<List<EmployeeCategoryDC>> result = GMSManageMembership.GetSponsorEmployeeCategoryList(branchId, ExceConnectionManager.GetGymCode(user), sponsorId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EmployeeCategoryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public OrdinaryMemberDC GetEmployeeCategoryBySponsorId(int branchId, string user, int sponsorId)
        {
            OperationResult<OrdinaryMemberDC> result = GMSManageMembership.GetEmployeeCategoryBySponsorId(branchId, ExceConnectionManager.GetGymCode(user), sponsorId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new OrdinaryMemberDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineMemberDC> GetMembersBySponsorId(int sponsorId, int branchId, string user)
        {
            OperationResult<List<ExcelineMemberDC>> result = GMSManageMembership.GetMembersBySponsorId(sponsorId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<int> GetSponsoredMemberListByTimePeriod(int sponsorId, Dictionary<int, List<DateTime>> sponsorMembers, string user)
        {
            OperationResult<List<int>> result = GMSManageMembership.GetSponsoredMemberListByTimePeriod(sponsorId, sponsorMembers, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<int>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ArticleDC> GetArticles(int branchId, string user, ArticleTypes categpryType, string keyword, CategoryDC category, bool isActive, bool filterByGym)
        {
            OperationResult<List<ArticleDC>> result = GMSArticle.GetArticles(branchId, user, categpryType, keyword, category, -1, false, isActive, ExceConnectionManager.GetGymCode(user).Trim(),filterByGym);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ArticleDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ActivityDC> GetActivitiesForEntity(int entityId, string entityType, string user, int branchId)
        {
            var result = GMSActivity.GetActivitiesForEntity(entityId, entityType, ExceConnectionManager.GetGymCode(user), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ActivityDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<EntityActiveTimeDC> GetEntityActiveTimes(int branchid, DateTime startDate, DateTime endDate, List<int> entityList, string entityRoleType, string user)
        {
            var result = US.GMS.API.GMSSchedule.GetEntityActiveTimes(branchid, startDate, endDate, entityList, entityRoleType, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EntityActiveTimeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateSheduleItems(ScheduleDC schedule, string user)
        {
            var result = US.GMS.API.GMSSchedule.UpdateSheduleItems(schedule, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateActiveTime(EntityActiveTimeDC activeTime, string user, int branchId, bool isDelete)
        {
            var result = US.GMS.API.GMSSchedule.UpdateActiveTime(activeTime, isDelete, branchId, user,ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ScheduleItemDC> GetScheduleItemsByScheduleId(int scheduleId, string user)
        {
            var result = US.GMS.API.GMSSchedule.GetScheduleItemsByScheduleId(scheduleId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ScheduleItemDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveTaskTemplate(TaskTemplateDC smsTemplate, bool isEdit, string user)
        {
            var result = GMSManageTask.SaveTaskTemplate(smsTemplate, isEdit, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateTaskTemplate(TaskTemplateDC smsTemplate, string user)
        {
            var result = GMSManageTask.UpdateTaskTemplate(smsTemplate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteTaskTemplate(int smsTemplateId, string user)
        {
            var result = GMSManageTask.DeleteTaskTemplate(smsTemplateId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeActivateTaskTemplate(int smsTemplateId, string user)
        {
            var result = GMSManageTask.DeActivateTaskTemplate(smsTemplateId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<TaskTemplateDC> GetTaskTemplates(TaskTemplateType templateType, int branchId, int templateId, string user)
        {
            var result = GMSManageTask.GetTaskTemplates(templateType, branchId, templateId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<TaskTemplateDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<FollowUpTemplateDC> GetFollowUpTemplates(string user, int branchId)
        {
            OperationResult<List<FollowUpTemplateDC>> result = GMSSystemSettings.GetFollowupTemplates(ExceConnectionManager.GetGymCode(user), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<FollowUpTemplateDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExtendedTaskTemplateDC> GetTaskTemplateExtFields(int extFieldId, string user)
        {
            var result = GMSManageTask.GetTaskTemplateExtFields(extFieldId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExtendedTaskTemplateDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveExcelineTask(ExcelineTaskDC excelineTask, string user)
        {
            var result = GMSManageTask.SaveExcelineTask(excelineTask, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                if (result.OperationReturnValue > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<ExcelineTaskDC> GetExcelineTasks(int branchId, bool isFxied, int templateId, int assignTo, string roleType, int followupMemberId, string user)
        {
            var result = GMSManageTask.GetExcelineTasks(branchId, isFxied, templateId, assignTo, roleType, followupMemberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineTaskDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<InstructorDC> GetInstructors(int branchId, string searchText, string user, bool isActive)
        {
            var instructorId = -1;
            var result = GMSInstructor.GetInstructors(branchId, searchText, isActive, instructorId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstructorDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<TrainerDC> GetTrainers(int branchId, string searchText, string user, bool isActive)
        {
            var trainerId = -1;
            var result = GMSTrainer.GetTrainers(branchId, searchText, isActive, trainerId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<TrainerDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateExcelineTask(ExcelineTaskDC excelineTask, string user)
        {
            var result = GMSManageTask.EditExcelineTask(excelineTask, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                if (result.OperationReturnValue > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<ExcelineTaskDC> SearchExcelineTask(string searchText, TaskTemplateType type, int followUpMemId, int branchId, bool isFxied, string user)
        {
            var result = GMSManageTask.SearchExcelineTask(searchText, type, followUpMemId, branchId, isFxied, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineTaskDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool SaveTaskWithOutSchedule(ExcelineTaskDC excelineTask, string user)
        {
            var result = GMSManageTask.SaveTaskWithOutSchedule(excelineTask, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool AddMemberToGroup(List<int> memberIdList, int groupId, string user)
        {
            var result = GMSManageMembership.AddMemberToGroup(memberIdList, groupId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool RemoveMemberFromGroup(List<int> memberList, int groupId, string user)
        {
            var result = GMSManageMembership.RemoveMemberFromGroup(memberList, groupId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<DiscountDC> GetDiscountList(int branchId, DiscountType discountType, string user)
        {
            var result = GMSManageMembership.GetDiscountList(branchId, discountType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<DiscountDC> GetDiscountsForActivity(int branchId, int activityId, string user)
        {
            var result = GMSManageMembership.GetDiscountsForActivity(branchId, activityId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ShopSalesDC GetMemberPurchaseHistory(int memberId, DateTime fromDate, DateTime toDate, int branchId, string user)
        {
            var result = GMSManageMembership.GetMemberPerchaseHistory(memberId, fromDate, toDate, ExceConnectionManager.GetGymCode(user), branchId, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string SaveContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, int branchId, string notificationTitle, string notifyMethod, string senderDescription, string gymName )
        {
            var result = GMSManageMembership.SaveContractFreezeItem(freezeItem, freezeInstallments, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return "ERROR";
            }
            return SendSMSForFreeze(freezeItem.MemberContractNo, freezeItem.MemberId, branchId, user, senderDescription, notifyMethod, freezeItem, gymName);
        }

        public string SendSMSForFreeze(string contractNo, int memberID, int branchId, string user, string senderDescription, string notifyMethod, ContractFreezeItemDC freezeItem, string gymName)
        {
            // create parameter string for notification
            string application = string.Empty;
            string outputFormat = string.Empty;
            NotificationMethodType nMethod = NotificationMethodType.NONE;
            if (notifyMethod == "SMS")
            {
                nMethod = NotificationMethodType.SMS;
                application = ConfigurationManager.AppSettings["USC_Notification_SMS"];
                outputFormat = "SMS";
            }
            else if (notifyMethod == "EMAIL")
            {
                nMethod = NotificationMethodType.EMAIL;
                application = ConfigurationManager.AppSettings["USC_Notification_Email"];
                outputFormat = "EMAIL";
            }

            var paraList = new Dictionary<TemplateFieldEnum, string>
                {
                    {TemplateFieldEnum.CONTRACTNO, contractNo},
                    {TemplateFieldEnum.STARTDATE, freezeItem.FromDate.ToShortDateString()},
                    {TemplateFieldEnum.ENDDATE, freezeItem.ToDate.ToShortDateString()},
                    {TemplateFieldEnum.GYMNAME, gymName.Trim()},
                };

            // create common notification
            var commonNotification = new USCommonNotificationDC
            {
                ParameterString = paraList,
                BranchID = branchId,
                CreatedUser = user,
                Role = "MEM",
                NotificationType = NotificationTypeEnum.Messages,
                Severity = NotificationSeverityEnum.Minor,
                MemberID = memberID,
                Status = NotificationStatusEnum.New,
                Method = nMethod,
                Type = TextTemplateEnum.FREEZESMS,
                Title = "FREEZE CONTRACT",
                SenderDescription = senderDescription,
                IsTemplateNotification = true
            };

            var result = NotificationAPI.AddNotification(commonNotification);
            if (result.ErrorOccured)
            {
                foreach (var mgs in result.NotificationMessages.Where(mgs => mgs.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error))
                {
                    USLogError.WriteToFile(mgs.Message, new Exception(), user);
                }
                return "ERROR";
            }

            try
            {
                if (notifyMethod == "SMS")
                {
                    // send SMS through the USC API
                    var smsStatus = USCAPI.ExecuteTemplate(application, outputFormat,
                                                           ExceConnectionManager.GetGymCode(user),
                                                           result.MethodReturnValue.ToString());
                    var status = NotificationStatusEnum.Attended;
                    string statusMessage = string.Empty;
                    if (smsStatus != null)
                    {
                        status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() ==
                                  "1")
                                     ? NotificationStatusEnum.Success
                                     : NotificationStatusEnum.Fail;
                        statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                    }

                    // update status in notification table
                    USImportResult<bool> updateResult =
                        NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                    if (updateResult.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in updateResult.NotificationMessages)
                        {
                            if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        return "ERROR";
                    }
                    return "SUCCESS";
                }
                if (notifyMethod == "EMAIL")
                {
                    // send EMAIL through the USC API
                    IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                    NotificationStatusEnum status = NotificationStatusEnum.Attended;
                    string statusMessage = string.Empty;
                    if (emailStatus != null)
                    {
                        status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() ==
                                  "1")
                                     ? NotificationStatusEnum.Success
                                     : NotificationStatusEnum.Fail;
                        statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                    }

                    // update status in notification table
                    USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                    if (updateResult.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in updateResult.NotificationMessages)
                        {
                            if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        return "ERROR";
                    }
                    return "SUCCESS";
                }
                if (notifyMethod == "PRINT")
                {
                    var memnberContractId = freezeItem.MemberContractid.ToString();
                    var startDate = freezeItem.FromDate.ToString("dd.MM.yyyy");
                    var endDate = freezeItem.ToDate.ToString("dd.MM.yyyy");

                    var appUrl = ConfigurationManager.AppSettings["USC_Freez_Print_App_Path"];
                    var dataDictionary = new Dictionary<string, string>() { { "MemberContractID", memnberContractId }, { "StartDate", startDate }, { "EndDate", endDate }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                    var pdfFileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                    if (!string.IsNullOrEmpty(pdfFileParth))
                        return pdfFileParth;
                    return "PDFERROR";
                }
            }
            catch (Exception)
            {
                return "ERROR";
            }
            return "SUCCESS";
        }

        public List<ContractFreezeItemDC> GetContractFreezeItems(int memberContractId, string user)
        {
            var result = GMSManageMembership.GetContractFreezeItems(memberContractId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ContractFreezeInstallmentDC> GetContractFreezeInstallments(int freezeItemId, string user)
        {
            var result = GMSManageMembership.GetContractFreezeInstallments(freezeItemId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<InstallmentDC> GetInstallmentsToFreeze(int memberContractId, string user)
        {
            var result = GMSManageMembership.GetInstallmentsToFreeze(memberContractId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int ExtendContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, int branchId, string notificationTitle)
        {

            var result = GMSManageMembership.ExtendContractFreezeItem(freezeItem, freezeInstallments, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            return result.OperationReturnValue;
        }

        public int UnfreezeContract(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> unfreezeInstallments, string user, int branchId, string notificationTitle)
        {
            var result = GMSManageMembership.UnfreezeContract(freezeItem, unfreezeInstallments, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {

                try
                {
                    USImportResult<NotificationTemplateText> notificationResult = NotificationAPI.GetNotificationTextByType(NotifyMethodType.EVENTLOG, TextTemplateEnum.UNFREEZCONTRACT, user, branchId);
                    string textTemplate = string.Empty;
                    NotificationTemplateText notificationTemplateText = new NotificationTemplateText();
                    if (!notificationResult.ErrorOccured && notificationResult.MethodReturnValue != null)
                    {
                        notificationTemplateText = notificationResult.MethodReturnValue;
                        textTemplate = notificationTemplateText.Text;
                    }
                    List<TemplateField> templateFieldList = new List<TemplateField>();
                    string memberNo = string.Empty;
                    try
                    {
                        OrdinaryMemberDC memberSummary = (GMSMember.GetMembersById(branchId, freezeItem.MemberId, ExceConnectionManager.GetGymCode(user))).OperationReturnValue;
                        memberNo = memberSummary.CustId;
                    }
                    catch
                    {
                    }
                    try
                    {
                        templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.MEMBERNO, Value = memberNo });
                        templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.CONTRACTNO, Value = freezeItem.MemberContractNo });
                        templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.DATE, Value = DateTime.Now.ToShortDateString() });
                        templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.CREATEDUSER, Value = user });
                    }
                    catch
                    {
                    }

                    string text = NotificationAPI.ConvertTemplateToText(textTemplate, templateFieldList);
                    if (!string.IsNullOrEmpty(text.Trim()))
                    {
                        USNotificationTree noitificationTree = new USNotificationTree();
                        noitificationTree.BranchId = branchId;
                        noitificationTree.MemberId = freezeItem.MemberId;
                        USNotification notification = new USNotification();
                        notification.Module = "Manage MemberShip";
                        notification.Title = notificationTitle;
                        notification.Description = text;
                        notification.CreatedDate = DateTime.Now;
                        notification.CreatedUser = user;
                        notification.TypeId = 3;
                        notification.SeverityId = 3;
                        notification.StatusId = 1;
                        notification.CreatedUserRoleId = "MEM";
                        noitificationTree.Notification = notification;
                        USNotificationAction action = new USNotificationAction();
                        action.Comment = "Notification was saved automatically";
                        action.AssgnId = user;
                        noitificationTree.Action = action;
                        List<USNotificationChannel> channelList = new List<USNotificationChannel>();
                        USNotificationChannel channel = new USNotificationChannel();
                        channel.ChannelDueDate = DateTime.Now.AddDays(30);
                        channel.MessageType = NotifyMethodType.EVENTLOG.ToString();
                        channel.OccuranceType = OccuranceType.ONCE.ToString();

                        channel.ChannelMessage = "New Message";
                        channel.ChannelAssign = user;
                        channel.ChannelStatus = true;
                        channelList.Add(channel);
                        noitificationTree.NotificationMethods = channelList;
                        NotificationAPI.AddNotification(noitificationTree);
                    }
                }
                catch
                {
                }
            }
            return result.OperationReturnValue;
        }

        public string RegisterPayment(int branchId, int memberId, int articleNo, decimal paymentAmount, decimal discount, string createdUser, PaymentTypes paymentType)
        {
            var result = GMSPayment.RegisterPayment(branchId, memberId, articleNo, paymentAmount, discount, createdUser, paymentAmount, paymentType, ExceConnectionManager.GetGymCode(createdUser));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), createdUser);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int width { get; set; }

        public SaleResultDC RegisterInstallmentPayment(int memberBranchID, int loggedBranchID, string user, InstallmentDC installment, PaymentDetailDC paymentDetail, int salePointID, ShopSalesDC salesDetails)
        {
            

        OperationResult<SaleResultDC> result = GMSPayment.RegisterInstallmentPayment(memberBranchID, loggedBranchID, user, installment, paymentDetail, ExceConnectionManager.GetGymCode(user), salePointID, salesDetails);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new SaleResultDC() { AritemNo = -1, SaleStatus = SaleErrors.ERROR };
            }

            foreach (var payMode in paymentDetail.PayModes)
            {
                if (payMode.PaymentTypeCode == "PREPAIDBALANCE" || payMode.PaymentTypeCode == "INVOICE" || payMode.PaymentTypeCode == "ONACCOUNT" || payMode.PaymentTypeCode == "NEXTORDER")
                {
                    // create parameter string for notification
                    Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                    paraList.Add(TemplateFieldEnum.DATE, DateTime.Now.ToString());
                    paraList.Add(TemplateFieldEnum.MEMBERNAME, "");
                    paraList.Add(TemplateFieldEnum.MEMBERNO, installment.MemberName.ToString());
                    paraList.Add(TemplateFieldEnum.AMOUNT, payMode.Amount.ToString());
                    paraList.Add(TemplateFieldEnum.PAYMENTMETHOD, payMode.PaymentType.ToString());


                    // create common notification
                    USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                    commonNotification.IsTemplateNotification = true;
                    commonNotification.ParameterString = paraList;
                    commonNotification.BranchID = memberBranchID;
                    commonNotification.CreatedUser = user;
                    commonNotification.Role = "MEM";
                    commonNotification.NotificationType = NotificationTypeEnum.Messages;
                    commonNotification.Severity = NotificationSeverityEnum.Minor;
                    commonNotification.MemberID = installment.MemberId;
                    commonNotification.Status = NotificationStatusEnum.New;

                    // FOR SMS **************************************************************
                    commonNotification.Type = TextTemplateEnum.PAYMENTNOTIFICATION;
                    commonNotification.Method = NotificationMethodType.SMS;
                    commonNotification.Title = "SHOP PAYMENT NOTIFICATION";

                    //add SMS notification into notification tables

                    USImportResult<int> notification = US.Common.Notification.API.NotificationAPI.AddNotification(commonNotification);

                    if (!result.ErrorOccured)
                    {
                        string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                        string outputFormat = "SMS";

                        try
                        {
                            // send SMS through the USC API
                            IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), notification.MethodReturnValue.ToString());

                            NotificationStatusEnum status = NotificationStatusEnum.Attended;

                            if (smsStatus != null)
                                status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                            var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                            // update status in notification table
                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(notification.MethodReturnValue, user, status, statusMessage);

                            if (updateResult.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                {
                                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            USLogError.WriteToFile(e.Message, new Exception(), user);
                        }
                    }
                }
            }

            PayModeDC invoiceSMSEmail = paymentDetail.PayModes.FirstOrDefault(X => X.PaymentTypeCode == "EMAILSMSINVOICE");
            if (invoiceSMSEmail != null)
            {
                PDFPrintResultDC pdfResult = new PDFPrintResultDC();
                try
                {
                    var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"].ToString();
                    var dataDictionary = new Dictionary<string, string>() { { "itemID", result.OperationReturnValue.AritemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                    pdfResult = GetFilePathForPdf(appUrl, "PDF", user, memberBranchID, dataDictionary);
                    SetPDFFileParth(result.OperationReturnValue.AritemNo, pdfResult.FileParth, "INV", memberBranchID, user);

                    var InvoiceDetails = GMSInvoice.GetInvoiceDetails(result.OperationReturnValue.AritemNo, memberBranchID, ExceConnectionManager.GetGymCode(user));

                    if (InvoiceDetails.ErrorOccured)
                    {
                        foreach (NotificationMessage message in InvoiceDetails.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                        pdfResult.ErrorStateId = -4;
                        result.OperationReturnValue.PrintResult = pdfResult;
                        return result.OperationReturnValue;
                    }
                    ExcelineInvoiceDetailDC invoice = InvoiceDetails.OperationReturnValue;

                    // create parameter string for notification
                    Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                    paraList.Add(TemplateFieldEnum.INVOICENO, invoice.InvoiceNo.ToString());
                    paraList.Add(TemplateFieldEnum.DUEDATE, invoice.InvoiceDueDate.ToString());
                    paraList.Add(TemplateFieldEnum.AMOUNT, invoice.InvoiceAmount.ToString());
                    paraList.Add(TemplateFieldEnum.KID, invoice.BasicDetails.KID.ToString());
                    paraList.Add(TemplateFieldEnum.GYMACCOUNT, invoice.OtherDetails.BranchAccountNo);
                    paraList.Add(TemplateFieldEnum.GYMNAME, invoice.OtherDetails.BranchName.ToString());

                    // create common notification
                    USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                    commonNotification.IsTemplateNotification = true;
                    commonNotification.ParameterString = paraList;
                    commonNotification.BranchID = memberBranchID;
                    commonNotification.CreatedUser = user;
                    commonNotification.Role = "MEM";
                    commonNotification.NotificationType = NotificationTypeEnum.Messages;
                    commonNotification.Severity = NotificationSeverityEnum.Minor;
                    commonNotification.MemberID = paymentDetail.PaidMemberId;
                    commonNotification.Status = NotificationStatusEnum.New;

                    // FOR SMS **************************************************************
                    commonNotification.Type = TextTemplateEnum.SMSINVOICE;
                    commonNotification.Method = NotificationMethodType.SMS;
                    commonNotification.Title = "INVOICE SMS";

                    //add SMS notification into notification tables
                    USImportResult<int> SMSresult = NotificationAPI.AddNotification(commonNotification);

                    if (result.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in SMSresult.NotificationMessages)
                        {
                            if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        pdfResult.ErrorStateId = -4;
                    }
                    else
                    {
                        string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                        string outputFormat = "SMS";

                        try
                        {
                            // send SMS through the USC API
                            IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), SMSresult.MethodReturnValue.ToString());

                            NotificationStatusEnum status = NotificationStatusEnum.Attended;
                            if (smsStatus != null)
                                status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                            var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                            // update status in notification table
                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(SMSresult.MethodReturnValue, user, status, statusMessage);

                            if (updateResult.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                {
                                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                }
                                pdfResult.ErrorStateId = -4;
                            }
                        }
                        catch (Exception e)
                        {
                            USLogError.WriteToFile(e.Message, new Exception(), user);
                            pdfResult.ErrorStateId = -4;
                        }
                    }

                    // FOR EMAIL **************************************************************
                    commonNotification.Type = TextTemplateEnum.EMAILINVOICE;
                    commonNotification.Method = NotificationMethodType.EMAIL;
                    commonNotification.AttachmentFileParth = pdfResult.FileParth;
                    commonNotification.Title = "INVOICE EMAIL";

                    //add EMAIL notification into notification tables
                    USImportResult<int> EMAILresult = NotificationAPI.AddNotification(commonNotification);

                    if (EMAILresult.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in EMAILresult.NotificationMessages)
                        {
                            if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        pdfResult.ErrorStateId = -4;
                    }
                    else
                    {
                        string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
                        string outputFormat = "EMAIL";

                        try
                        {
                            // send EMAIL through the USC API
                            IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), EMAILresult.MethodReturnValue.ToString());

                            NotificationStatusEnum status = NotificationStatusEnum.Attended;

                            if (emailStatus != null)
                                status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                            var statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                            // update status in notification table
                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(EMAILresult.MethodReturnValue, user, status, statusMessage);

                            if (updateResult.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                {
                                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                }
                                pdfResult.ErrorStateId = -4;
                            }
                        }
                        catch (Exception e)
                        {
                            USLogError.WriteToFile(e.Message, new Exception(), user);
                            pdfResult.ErrorStateId = -4;
                        }
                    }
                }
                catch (Exception e)
                {
                    USLogError.WriteToFile(e.Message, new Exception(), user);
                    pdfResult.ErrorStateId = -4;
                }
            }
            return result.OperationReturnValue;
        }

        private PDFPrintResultDC GetFilePathForPdf(string application, string OutputPlugging, string user, int branchId, Dictionary<string, string> dataDictionary)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var list = ExcecuteWithUSC(application, OutputPlugging, dataDictionary, ExceConnectionManager.GetGymCode(user), user);
                foreach (var item in list)
                {
                    var Message = item.Messages.Where(mssge => mssge.Header.ToLower().Equals("filepath")).First();
                    if (Message != null)
                    {
                        result.FileParth = Message.Message;
                        break;
                    }
                }

                // for get visibility on browser.
                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return result;
        }

        public SaleResultDC AddInvoicePayment(PaymentDetailDC paymentDetails, string user, int brnachID, ShopSalesDC salesDetails)
        {
            OperationResult<SaleResultDC> result = GMSPayment.RegisterInvoicePayment(paymentDetails, ExceConnectionManager.GetGymCode(user), brnachID, salesDetails, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new SaleResultDC() {  AritemNo = -1, InvoiceNo = string.Empty, SaleStatus = SaleErrors.ERROR};
            }

            foreach (var payMode in paymentDetails.PayModes)
            {
                if (payMode.PaymentTypeCode == "PREPAIDBALANCE" || payMode.PaymentTypeCode == "INVOICE" || payMode.PaymentTypeCode == "ONACCOUNT" || payMode.PaymentTypeCode == "NEXTORDER")
                {
                    // create parameter string for notification
                    Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                    paraList.Add(TemplateFieldEnum.DATE, DateTime.Now.ToString());
                    paraList.Add(TemplateFieldEnum.MEMBERNAME, "");
                    paraList.Add(TemplateFieldEnum.MEMBERNO, paymentDetails.CustId.ToString());
                    paraList.Add(TemplateFieldEnum.AMOUNT, payMode.Amount.ToString());
                    paraList.Add(TemplateFieldEnum.PAYMENTMETHOD, payMode.PaymentType.ToString());


                    // create common notification
                    USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                    commonNotification.IsTemplateNotification = true;
                    commonNotification.ParameterString = paraList;
                    commonNotification.BranchID = brnachID;
                    commonNotification.CreatedUser = user;
                    commonNotification.Role = "MEM";
                    commonNotification.NotificationType = NotificationTypeEnum.Messages;
                    commonNotification.Severity = NotificationSeverityEnum.Minor;
                    commonNotification.MemberID = paymentDetails.PaidMemberId;
                    commonNotification.Status = NotificationStatusEnum.New;

                    // FOR SMS **************************************************************
                    commonNotification.Type = TextTemplateEnum.PAYMENTNOTIFICATION;
                    commonNotification.Method = NotificationMethodType.SMS;
                    commonNotification.Title = "SHOP PAYMENT NOTIFICATION";

                    //add SMS notification into notification tables

                    USImportResult<int> notification = US.Common.Notification.API.NotificationAPI.AddNotification(commonNotification);

                    if (!result.ErrorOccured)
                    {
                        string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                        string outputFormat = "SMS";

                        try
                        {
                            // send SMS through the USC API
                            IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), notification.MethodReturnValue.ToString());

                            NotificationStatusEnum status = NotificationStatusEnum.Attended;

                            if (smsStatus != null)
                                status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                            var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                            // update status in notification table
                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(notification.MethodReturnValue, user, status, statusMessage);

                            if (updateResult.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                {
                                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            USLogError.WriteToFile(e.Message, new Exception(), user);
                        }
                    }
                }
            }
            return result.OperationReturnValue;
        }

        public string GetCityForPostalCode(string postalCode, string user)
        {
            var result = GMSUtility.GetCityByPostalCode(postalCode, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CalendarHoliday> GetHolidays(DateTime calendarDate, string user, int branchId)
        {
            var result = GMSUtility.GetHolidays(calendarDate, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CalendarHoliday>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string GetGymSettings(int branchId, string user, GymSettingType gymSettingType)
        {
            OperationResult<string> result = GMSManageGymSetting.GetGymSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool ManageMemberShopAccount(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePointId)
        {
            var result = GMSMember.ManageMemberShopAccount(memberShopAccount, mode, user, salePointId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public MemberShopAccountDC GetMemberShopAccounts(int memberId, int hit, bool itemsNeeded, string user)
        {
            var result = GMSMember.GetMemberShopAccounts(memberId, hit, itemsNeeded, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new MemberShopAccountDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<MemberShopAccountItemDC> GetMemberShopAccountItems(int shopAccountId, string user, int branchId)
        {
            var result = GMSMember.GetMemberShopAccountItems(shopAccountId, user, ExceConnectionManager.GetGymCode(user), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<MemberShopAccountItemDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcePaymentInfoDC> GetMemberPaymentsHistory(int branchId, string user, int hit, int memberId, string paymentType)
        {
            var result = GMSManageMembership.GetMemberPaymentsHistory(memberId, paymentType, ExceConnectionManager.GetGymCode(user), hit, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcePaymentInfoDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<USNotificationSummaryInfo> GetNotificationsByMemberId(int branchId, string user, int memberId)
        {

            var result = NotificationAPI.GetNotificationsByMemberId(memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<USNotificationSummaryInfo>();
            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public List<string> GetInstallmentInvoice(int branchId, string user, string installmentId)
        {
            var createdHtml = new List<string>();
            try
            {
                var plugin = OutputFacade.GetSystemOutputPluginByID("PDF", "ExcelineInstallmentInvoice");
                var adapter = ActionAndParameteFacade.GetNewDataAdapter("ExcelineInstallmentInvoice");
                TemplateManager.GetTemplateByCategoryAndId("PDF", "PDFInvoice", "ExcelineInstallmentInvoice");
                var data = adapter.GetDataProvider().GetData(plugin, ExceConnectionManager.GetGymCode(user), installmentId);
                if (data != null && data.Count > 0)
                {
                }
            }
            catch
            {
            }
            return createdHtml;
        }

        public int SaveNote(int branchId, string user, int memberId, string note)
        {
            var result = GMSManageMembership.SaveNote(branchId, user, memberId, note, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public OrdinaryMemberDC GetNote(int branchId, string user, int memberId)
        {
            var result = GMSManageMembership.GetNote(branchId, user, memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new OrdinaryMemberDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int AddCreditNote(CreditNoteDC creditNote, string user, int branchId)
        {
            var result = GMSInvoice.AddCreditNote(creditNote, branchId, false, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public CreditNoteDC GetCreditNoteDetails(int arItemno, string user)
        {
            var result = GMSManageMembership.GetCreditNoteDetails(arItemno, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #region GetSelectedGymSettings
        public Dictionary<string, object> GetSelectedGymSettings(List<string> gymSetColNames, bool isGymSettings, int branchId, string user)
        {
            OperationResult<Dictionary<string, object>> result = GMSManageGymSetting.GetSelectedGymSettings(gymSetColNames, isGymSettings, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        #region DummyMethodForPassDomainObject
        public void DummyMethodForPassDomainObject(ExceGymSettingDC gymSetting)
        {
        }
        #endregion

        public string SaveGymEmployee(GymEmployeeDC employee, string user)
        {
            var result = GMSGymEmployee.SaveGymEmployee(employee, GetConfigurations(user), ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return "-1";
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string UpdateEmployees(EmployeeDC employeeDc, string user)
        {
            var result = GMSEmployee.UpdateEmployees(employeeDc, GetConfigurations(user), user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ResourceDC> GetResources(int branchId, string searchText, int categoryId,int activityId, bool isActive, string user)
        {
            OperationResult<List<ResourceDC>> result = GMSResources.GetResources(branchId, searchText, -1, activityId, 2, true, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ResourceDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CountryDC> GetCountryDetails(string user)
        {
            var result = GMSMember.GetCountryDetails(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CountryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<LanguageDC> GetLangugeDetails(string user)
        {
            var result = GMSMember.GetLangugeDetails(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<LanguageDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SavePostalArea(string user, string postalCode, string postalArea, long population, long houseHold)
        {
            var result = GMSMember.SavePostalArea(user, postalCode, postalArea, population, houseHold, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExceUserBranchDC> GetUserSelectedBranches(string userName, string user)
        {
            OperationResultValue<List<ExceUserBranchDC>> result = ManageExcelineLogin.GetUserSelectedBranches(userName, user);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExceUserBranchDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ExcelineMemberDC GetMem()
        {
            return null;
        }

        public bool CancelInvoice(int arItemNo, string user)
        {
            var result = GMSInvoice.CancelInvoice(arItemNo, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateInvoice(int arItemNo, string user, DateTime DueDate, string comment)
        {
            var result = GMSInvoice.UpdateInvoice(arItemNo, comment, DueDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string GetMemberInvoices(int memberId, int hit, string user)
        {
            var result = GMSInvoice.GetMemberInvoices(memberId, hit, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return XMLUtils.SerializeDataContractObjectToXML(new List<MemberInvoiceDC>());
            }
            else
            {
                return XMLUtils.SerializeDataContractObjectToXML(result.OperationReturnValue);
            }
        }

        public MemberInvoiceDC GetInvoices()
        {
            return null;
        }

        public ExcelineInvoiceDetailDC GetInvoiceDetails(int invoiceID, int branchId, string user)
        {
            var result = GMSInvoice.GetInvoiceDetails(invoiceID, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }

                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public SaleResultDC GenerateInvoice(int invoiceBranchId, string user, InstallmentDC installment, PaymentDetailDC paymentDetail, string invoiceType, string notificationTitle, int memberBranchID)
        {
            //handle Email and SMS
            PDFPrintResultDC printResult = new PDFPrintResultDC();
            OperationResult<SaleResultDC> result = GMSPayment.GenerateInvoice(invoiceBranchId, user, installment, paymentDetail, invoiceType, ExceConnectionManager.GetGymCode(user), memberBranchID);
            
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }

                result.OperationReturnValue.AritemNo = -1;
                result.OperationReturnValue.SaleStatus = SaleErrors.ERROR;
                result.OperationReturnValue.PrintResult = printResult;
            }
            else
            {
                if (result.OperationReturnValue.AritemNo > 0)
                {
                    if (invoiceType == "SMS" || invoiceType == "EmailSMS")
                    {
                        try
                        {
                            #region Save Notification
                            USImportResult<NotificationTemplateText> notificationResult = NotificationAPI.GetNotificationTextByType(NotifyMethodType.SMS, TextTemplateEnum.SMSINVOICE, user, invoiceBranchId);
                            string textTemplate = string.Empty;
                            NotificationTemplateText notificationTemplateText = new NotificationTemplateText();
                            if (!notificationResult.ErrorOccured && notificationResult.MethodReturnValue != null)
                            {
                                notificationTemplateText = notificationResult.MethodReturnValue;
                                textTemplate = notificationTemplateText.Text;
                            }

                            ExcelineInvoiceDetailDC invoiceDetails = new ExcelineInvoiceDetailDC();
                            var invoiceDetailsVar = GMSInvoice.GetInvoiceDetails(result.OperationReturnValue.AritemNo, invoiceBranchId, ExceConnectionManager.GetGymCode(user));
                            if (!invoiceDetailsVar.ErrorOccured && invoiceDetailsVar != null)
                            {
                                invoiceDetails = invoiceDetailsVar.OperationReturnValue;
                            }
                            List<TemplateField> templateFieldList = new List<TemplateField>();
                            try
                            {
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.AMOUNT, Value = invoiceDetails.InvoiceAmount.ToString() });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.KID, Value = invoiceDetails.BasicDetails.KID });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.DUEDATE, Value = invoiceDetails.BasicDetails.DueDate.ToString() });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.INVOICENO, Value = invoiceDetails.InvoiceNo });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.GYMACCOUNT, Value = invoiceDetails.OtherDetails.BranchAccountNo });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.DATE, Value = DateTime.Now.ToShortDateString() });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.CREATEDUSER, Value = user });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.MEMBERNAME, Value = invoiceDetails.CustomerName });
                            }
                            catch
                            {
                            }

                            string text = NotificationAPI.ConvertTemplateToText(textTemplate, templateFieldList);
                            if (!string.IsNullOrEmpty(text))
                            {

                                USNotificationTree noitificationTree = new USNotificationTree();
                                noitificationTree.BranchId = invoiceBranchId;
                                noitificationTree.MemberId = installment.MemberId;
                                USNotification notification = new USNotification();
                                notification.Module = "Manage MemberShip";
                                notification.Title = notificationTitle;
                                notification.Description = text;
                                notification.CreatedDate = DateTime.Now;
                                notification.CreatedUser = user;
                                notification.TypeId = 3;
                                notification.SeverityId = 3;
                                notification.CreatedUserRoleId = "MEM";
                                notification.DueDate = DateTime.Now.AddDays(30);
                                notification.StatusId = 1;
                                noitificationTree.Notification = notification;
                                USNotificationAction action = new USNotificationAction();
                                action.Comment = "Notification was saved automatically";
                                action.AssgnId = user;
                                noitificationTree.Action = action;
                                List<USNotificationChannel> channelList = new List<USNotificationChannel>();
                                USNotificationChannel channel = new USNotificationChannel();
                                channel.ChannelDueDate = DateTime.Now.AddDays(30);
                                channel.MessageType = NotifyMethodType.SMS.ToString();
                                channel.OccuranceType = OccuranceType.ONCE.ToString();
                                channel.ChannelMessage = "New Message";
                                channel.ChannelAssign = user;
                                channel.ChannelStatus = true;
                                channelList.Add(channel);
                                noitificationTree.NotificationMethods = channelList;
                                NotificationAPI.AddNotification(noitificationTree);
                            }
                            #endregion

                        }
                        catch
                        {
                        }
                    }

                    if (invoiceType == "SMS")
                    {

                        SendSMSInvoice(invoiceBranchId, user, result.OperationReturnValue.AritemNo);
                        printResult.FileParth = "done";
                        result.OperationReturnValue.PrintResult = printResult;
                    }
                    else if (invoiceType == "EmailSMS")
                    {
                        SendSMSInvoice(invoiceBranchId, user, result.OperationReturnValue.AritemNo);
                        printResult.FileParth = "done";
                        result.OperationReturnValue.PrintResult = printResult;
                    }
                    else if (invoiceType == "PRINT")
                    {
                        printResult = ViewInvoiceDetails(invoiceBranchId, user, result.OperationReturnValue.AritemNo);
                        if (string.IsNullOrEmpty(printResult.FileParth))
                            printResult.FileParth = "INV";
                        result.OperationReturnValue.PrintResult = printResult;
                    }
                    }
                else
                {
                    result.OperationReturnValue.AritemNo = -1;
                    result.OperationReturnValue.SaleStatus = SaleErrors.ERROR;
                    result.OperationReturnValue.PrintResult = printResult;
                }
            }
            return result.OperationReturnValue;
        }

        public ArticleDC GetInvoiceFeeArticle(int branchId, string user)
        {
            var result = GMSArticle.GetInvoiceFee(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string GetContractMembers(int memberContractId, string user)
        {
            var result = GMSManageMembership.GetContractMembers(memberContractId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return XMLUtils.SerializeDataContractObjectToXML(new List<ExcelineMemberDC>());
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        // Added to the RestAPI 
        public List<ExcelineMemberDC> GetIntroducedMembers(int memberId, int branchId, string user)
        {
            var result = GMSManageMembership.GetIntroducedMembers(memberId, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        // Added to the RestAPI 
        public bool UpdateIntroducedMembers(int memberId, DateTime? creditedDate, string creditedText, bool isDelete, string username)
        {
            OperationResult<bool> result = GMSManageMembership.UpdateIntroducedMembers(memberId, creditedDate, creditedText, isDelete, ExceConnectionManager.GetGymCode(username));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), username);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<MemberStatus> GetMemberStatus(string user)
        {
            var result = GMSManageMembership.GetMemberStatus(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<MemberStatus>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateContractOrder(List<InstallmentDC> installmentList, int branchId, string user)
        {
            OperationResult<bool> result = GMSManageMembership.UpdateContractOrder(installmentList, branchId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ExcePaymentInfoDC getTest()
        {
            ExcePaymentInfoDC hh = new ExcePaymentInfoDC();
            return hh;
        }

        public int SaveContractGroupMembers(int groupId, List<ExcelineMemberDC> groupMemberList, int contractId, string user)
        {
            OperationResult<int> result = GMSManageMembership.SaveContractGroupMembers(groupId, groupMemberList, contractId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int UpdateContractPriority(Dictionary<int, int> packages, string user)
        {
            OperationResult<int> result = GMSContract.UpdateContractPriority(packages, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool CheckBookingAvailability(int resourceID, string day, DateTime startTime, DateTime endTime, string user)
        {
            OperationResult<bool> result = GMSManageMembership.CheckBookingAvailability(resourceID, day, startTime, endTime, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int AddInstallment(InstallmentDC installment, string user)
        {
            OperationResult<int> result = GMSManageMembership.AddInstallment(installment, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string GetMemberSearchCategory(int branchId, string user)
        {
            OperationResult<string> result = GMSManageMembership.GetMemberSearchCategory(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return string.Empty;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int IsFreezeAllowed(int memberContractId, int memberId, int branchId, string user)
        {
            var result = GMSManageMembership.IsFreezeAllowed(memberContractId, memberId, ExceConnectionManager.GetGymCode(user), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int AddMemberNotification(USNotificationTree notification, string text)
        {
            USImportResult<int> result = NotificationAPI.AddNotification(notification);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return -1;

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public List<ImportStatusDC> GetClaimExportStatusForMember(string user, int memberID, int hit, ImportStatusDC status, DateTime? transferDate, string type)
        {
            OperationResult<List<ImportStatusDC>> result = GMSCcx.GetClaimExportStatusForMember(ExceConnectionManager.GetGymCode(user), memberID, hit, transferDate, type, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ImportStatusDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool AttachFile(string fileName, string custId, string companyCode, string username, int branchId)
        {
            string fullFileName = ConfigurationManager.AppSettings["UploadScanDocRoot"];
            int maxRetryTime = Convert.ToInt32(ConfigurationManager.AppSettings["DocumentUploadRetryTime"]);
            fullFileName = fullFileName + @"\" + companyCode + @"\" + branchId + @"\" + custId + @"\" + fileName;
            int i = 0;
            bool isUploaded = false;
            while (i < (maxRetryTime / 5))
            {
                Thread.Sleep(5000);
                if (File.Exists(fullFileName))
                {
                    isUploaded = true;
                    break;
                }
                i++;
            }
            return isUploaded;
        }

        public bool SaveDocumentData(DocumentDC document, string username, int branchId)
        {
            if (!document.IsScanDoc)
                document.FilePath = ConfigurationManager.AppSettings["UploadScanDocRoot"] + document.FilePath;

            OperationResult<bool> result = GMSManageMembership.SaveDocumentData(document, ExceConnectionManager.GetGymCode(username), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), username);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<DocumentDC> GetDocumentData(bool isActive, string custId, string searchText, int documentType, string username, int branchId)
        {
            OperationResult<List<DocumentDC>> result = GMSManageMembership.GetDocumentData(isActive, custId, searchText, documentType, ExceConnectionManager.GetGymCode(username), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), username);
                }
                return new List<DocumentDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteDocumentData(int docId, string username, int branchId)
        {
            OperationResult<bool> result = GMSManageMembership.DeleteDocumentData(docId, ExceConnectionManager.GetGymCode(username), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), username);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ArticleDC> GetVisitArticles(string userName, int branchID)
        {
            OperationResult<List<ArticleDC>> result = US.Exceline.GMS.API.GMSContract.GetArticlesForCommonUse(-1, ExceConnectionManager.GetGymCode(userName), branchID);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), userName);
                }
                return new List<ArticleDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        //***********************************************************************************************
        #region USC

        public List<ExceNotificationTepmlateDC> GetSMSNotificationTemplateByEntity(string user)
        {
            var result = NotificationAPI.GetSMSNotificationTemplateByEntity("MEM",ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (var mgs in result.NotificationMessages.Where(mgs => mgs.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error))
                {
                    USLogError.WriteToFile(mgs.Message, new Exception(), user);
                }
                return new List<ExceNotificationTepmlateDC>();
            }
            return result.MethodReturnValue;
        }

        public bool SendSMSFromMemberCard(int memberID, int branchId, string user, string message, string mobileNo)
        {
            var result = NotificationAPI.AddSMSNotification("MEMBER CARD SMS", message, mobileNo, "MEM", memberID, memberID, branchId, user, ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (var mgs in result.NotificationMessages.Where(mgs => mgs.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error))
                {
                    USLogError.WriteToFile(mgs.Message, new Exception(), user);
                }
                return false;
            }

            var application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
            const string outputFormat = "SMS";

            try
            {
                // send SMS through the USC API
                var smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                var status = NotificationStatusEnum.Attended;
                if (smsStatus != null)
                    status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                // update status in notification table
                var updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                if (updateResult.ErrorOccured)
                {
                    foreach (var mgs in updateResult.NotificationMessages.Where(mgs => mgs.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error))
                    {
                        USLogError.WriteToFile(mgs.Message, new Exception(), user);
                    }
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                USLogError.WriteToFile(e.Message, new Exception(), user);
                return false;
            }
        }

        public bool SendEmailFromMemberCard(int memberID, int branchId, string user, string message, string email, string subject)
        {
            // create common notification
            USCommonNotificationDC commonNotification = new USCommonNotificationDC();
            commonNotification.IsTemplateNotification = false;
            commonNotification.Description = message;
            commonNotification.BranchID = branchId;
            commonNotification.CreatedUser = user;
            commonNotification.Role = "MEM";
            commonNotification.NotificationType = NotificationTypeEnum.Messages;
            commonNotification.Severity = NotificationSeverityEnum.Minor;
            commonNotification.MemberID = memberID;
            commonNotification.Status = NotificationStatusEnum.New;
            commonNotification.Method = NotificationMethodType.EMAIL;
            commonNotification.Title = subject;
            commonNotification.SenderDescription = email;

            USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage mgs in result.NotificationMessages)
                {
                    if (mgs.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(mgs.Message, new Exception(), user);
                    }
                }
                return false;
            }
            string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
            string outputFormat = "EMAIL";

            try
            {
                // send Email through the USC API
                IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                NotificationStatusEnum status = NotificationStatusEnum.Attended;
                    
                string statusMessage = string.Empty;
                if (emailStatus != null)
                {
                    status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                    statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                }

                // update status in notification table
                USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                if (updateResult.ErrorOccured)
                {
                    foreach (USNotificationMessage mgs in updateResult.NotificationMessages)
                    {
                        if (mgs.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                        {
                            USLogError.WriteToFile(mgs.Message, new Exception(), user);
                        }
                    }
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                USLogError.WriteToFile(e.Message, new Exception(), user);
                return false;
            }
        }

        public bool GenerateAndSendInvoiceSMSANDEmail(int invoiceBranchId, string user, InstallmentDC selectedOrder, PaymentDetailDC paymentDetails, Dictionary<string, bool> isSelected, Dictionary<string, string> text, Dictionary<string, string> senderDetail, int memberBranchID)
        {
            string invoiceType = string.Empty;
            if (isSelected["SMS"])
                invoiceType = "SMS";
            else if (isSelected["EMAIL"])
                invoiceType = "EMAIL";
            else
                invoiceType = "SMS";

            // Generate Invoice
            var newInvoice = GMSPayment.GenerateInvoice(invoiceBranchId, user, selectedOrder, paymentDetails, invoiceType, ExceConnectionManager.GetGymCode(user), memberBranchID);
            if (newInvoice.ErrorOccured)
            {
                foreach (NotificationMessage message in newInvoice.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            return SendInvoiceSMSANDEmail(invoiceBranchId, user, newInvoice.OperationReturnValue.AritemNo, selectedOrder.MemberId, selectedOrder.PayerId, isSelected, text, senderDetail);
        }

        public bool SendInvoiceSMSANDEmail(int branchId, string user, int arItemNo, int memberID, int guardianId, Dictionary<string, bool> isSelected, Dictionary<string, string> text, Dictionary<string, string> senderDetail)
        {
            ExcelineInvoiceDetailDC InvoiceDetails = GetInvoiceDetails(arItemNo, branchId, user);
            if (InvoiceDetails != null)
            {
                // create parameter string for notification
                Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                paraList.Add(TemplateFieldEnum.INVOICENO, InvoiceDetails.InvoiceNo.ToString());
                paraList.Add(TemplateFieldEnum.DUEDATE, InvoiceDetails.InvoiceDueDate.Value.ToShortDateString());
                paraList.Add(TemplateFieldEnum.AMOUNT, InvoiceDetails.InvoiceBalance.ToString());
                paraList.Add(TemplateFieldEnum.BALANCE, InvoiceDetails.InvoiceBalance.ToString());
                paraList.Add(TemplateFieldEnum.KID, InvoiceDetails.BasicDetails.KID.ToString());
                paraList.Add(TemplateFieldEnum.GYMACCOUNT, InvoiceDetails.OtherDetails.BranchAccountNo);
                paraList.Add(TemplateFieldEnum.GYMNAME, InvoiceDetails.OtherDetails.BranchName.ToString());
                paraList.Add(TemplateFieldEnum.CREATEDUSER, user.ToString());
                paraList.Add(TemplateFieldEnum.DATE, DateTime.Now.ToShortDateString());
                paraList.Add(TemplateFieldEnum.MEMBERNAME, InvoiceDetails.CustomerName);

                // create common notification
                USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                //commonNotification.IsTemplateNotification = true;
                commonNotification.ParameterString = paraList;
                commonNotification.BranchID = branchId;
                commonNotification.CreatedUser = user;
                commonNotification.Role = "MEM";
                commonNotification.NotificationType = NotificationTypeEnum.Messages;
                commonNotification.Severity = NotificationSeverityEnum.Minor;
                if (guardianId > 0)
                    commonNotification.MemberID = guardianId;
                else
                    commonNotification.MemberID = memberID;

                commonNotification.Status = NotificationStatusEnum.New;

                bool sendedresult = true;

                // if wants to send sms
                if (isSelected["SMS"])
                {
                    commonNotification.Type = TextTemplateEnum.SMSINVOICE;
                    commonNotification.Method = NotificationMethodType.SMS;
                    commonNotification.Title = "INVOICE SMS";
                    commonNotification.TempateText = text["SMS"];
                    commonNotification.SenderDescription = senderDetail["SMS"];

                    //add SMS notification into notification tables
                    USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

                    if (result.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in result.NotificationMessages)
                        {
                            if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        sendedresult = false;
                    }
                    else
                    {
                        string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                        string outputFormat = "SMS";

                        try
                        {
                            // send SMS through the USC API
                            IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());

                            NotificationStatusEnum status = NotificationStatusEnum.Attended;
                            string statusMessage = string.Empty;
                            if (smsStatus != null)
                            {
                                status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                             statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                                }
                            // update status in notification table
                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                            if (updateResult.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                {
                                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                }
                                sendedresult = false;
                            }
                            else
                            {
                                sendedresult = true;
                            }
                        }
                        catch (Exception e)
                        {
                            USLogError.WriteToFile(e.Message, new Exception(), user);
                            sendedresult = false;
                        }
                    }
                }

                // if wants to send email
                if (isSelected["EMAIL"])
                {
                    var filePath = ViewInvoiceDetails(branchId, user, arItemNo); // to print invoice pdf useing USC

                    commonNotification.Type = TextTemplateEnum.EMAILINVOICE;
                    commonNotification.Method = NotificationMethodType.EMAIL;
                    commonNotification.AttachmentFileParth = filePath.FileParth;
                    commonNotification.Title = "INVOICE EMAIL";
                    commonNotification.TempateText = text["EMAIL"];
                    commonNotification.SenderDescription = senderDetail["EMAIL"];

                    //add EMAIL notification into notification tables
                    USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

                    if (result.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in result.NotificationMessages)
                        {
                            if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        sendedresult = false;
                    }
                    else
                    {
                        string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
                        string outputFormat = "EMAIL";

                        try
                        {
                            // send EMAIL through the USC API
                            IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                            NotificationStatusEnum status = NotificationStatusEnum.Attended;
                            string statusMessage = string.Empty;
                            if (emailStatus != null)
                            {
                                status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                            }

                            // update status in notification table
                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                            if (updateResult.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                {
                                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                }
                                sendedresult = false;
                            }
                            else
                            {
                                if (!sendedresult)
                                    sendedresult = false;
                            }
                        }
                        catch (Exception e)
                        {
                            USLogError.WriteToFile(e.Message, new Exception(), user);
                            sendedresult = false;
                        }
                    }
                }

                return sendedresult;
            }
            return false;
        }

        public int SendSMSInvoice(int brnachId, string user, int arItemNo)
        {
            var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"];
            return SendSMS(appUrl, "SMS", arItemNo.ToString(), user);
        }

        public PDFPrintResultDC PrintOrder(int orderId, int branchId, string user)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_Order_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "itemID", orderId.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC PrintGroupContractMemberList(int branchId, string user, string contractID, string memberType)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_GroupContractMemberList_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "groupContractID", contractID },{ "memberType", contractID }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(Convert.ToInt32(contractID), result.FileParth, "GCM", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }
        
        public PDFPrintResultDC PrintGroupMemberListByMember(int branchId, string user, string memberID)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_GroupMemberList_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "memberID", memberID }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(Convert.ToInt32(memberID), result.FileParth, "GML", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC PrintVisitsListByMember(int branchId, string user, int memberId, DateTime fromDate, DateTime toDate)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_MemberVisits_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "MemberID", memberId.ToString() }, { "fromDate", fromDate.ToString("yyyy-MM-dd") }, { "toDate", toDate.ToString("yyyy-MM-dd") }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(memberId, result.FileParth, "VST", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC PrintClassListByMember(int branchId, string user, int memberId, DateTime fromDate, DateTime toDate)
        {
            var result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_MemberClass_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "MemberID", memberId.ToString() }, { "branchId", branchId.ToString() }, { "fromDate", fromDate.ToString("yyyy-MM-dd") }, { "toDate", toDate.ToString("yyyy-MM-dd") }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC PrintPaymentHistoryByMember(int branchId, string user, int memberId, DateTime fromDate, DateTime toDate)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_MemberPaymentHistory_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "MemberID", memberId.ToString() }, { "fromDate", fromDate.ToString("yyyy-MM-dd") }, { "toDate", toDate.ToString("yyyy-MM-dd") }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(memberId, result.FileParth, "PHP", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC ViewInvoiceDetails(int branchId, string user, int arItemNo)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "itemID", arItemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(arItemNo, result.FileParth, "INV", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC ViewCreditNoteDetails(int branchId, string user, int CreditNoteID)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_CreditNote_Print_App_Path"].ToString();
                var dataDictionary = new Dictionary<string, string>() { { "itemID", CreditNoteID.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(CreditNoteID, result.FileParth, "CNT", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC PrintInvoicePaidReceipt(int branchId, string user, int arItemNo)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_Invoice_PrintReceipt_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "itemID", arItemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC ViewMemberContractDetail(int branchId, string user, int memberId)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_MemberContract_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "itemID", memberId.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(memberId, result.FileParth, "CONT", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC ViewMemberATGContract(int branchId, string user, int memberId)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_Contract_ATG_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "itemID", memberId.ToString() },{ "branchID", branchId.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(memberId, result.FileParth, "ATG", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC ViewSponsorInvoiceDetails(int branchId, string user, int arItemNo)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_SponsorInvoiceDetails_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "itemID", arItemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(arItemNo, result.FileParth, "SPINV", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC ViewSponsorOrderDetails(int branchId, string user, int orderNo)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_SponsorInvoiceDetails_Print_App_Path"].ToString();
                var dataDictionary = new Dictionary<string, string>() { { "itemID", orderNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public PDFPrintResultDC ViewInstallmentInvoice(int branchId, string user, string installmentId)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"].ToString();
                var dataDictionary = new Dictionary<string, string>() { { "itemID", installmentId }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
            }

        public PDFPrintResultDC PrintMemberBooking(int memberId, int branchId, string user, DateTime fromDate, DateTime toDate)
        {
            var result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_MemberBooking_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "MemberID", memberId.ToString() }, { "fromDate", fromDate.ToString("yyyy-MM-dd") }, { "toDate", toDate.ToString("yyyy-MM-dd") }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(memberId, result.FileParth, "MBO", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        private int SendSMS(string application, string outputPlugging, string dataId, string user)
        {
            int result = 0;
            try
            {
                var dataDictionary = new Dictionary<string, string>() { { "itemID", dataId } };
                var list = ExcecuteWithUSC(application, outputPlugging, dataDictionary, ExceConnectionManager.GetGymCode(user), user);
                if (list[0].Messages[2] != null)
                    result = Convert.ToInt32(list[0].Messages[2].Message);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return result;
        }
        private string GetFilePathForPdf(string application, string outputPlugging, string user, Dictionary<string, string> dataDictionary)
        {
            var filePath = string.Empty;
            try
            {
                var list = ExcecuteWithUSC(application, outputPlugging, dataDictionary, ExceConnectionManager.GetGymCode(user), user);
                foreach (var item in list)
                {
                    if (item == null) continue;
                    var message = item.Messages.First(mssge => mssge.Header.ToLower().Equals("filepath"));
                    if (message != null)
                    {
                        filePath = message.Message;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return filePath;
        }
        private List<IProcessingStatus> ExcecuteWithUSC(string application, string outputPlugging, Dictionary<string, string> dataDictionary, string gymCode, string user)
        {
            List<IProcessingStatus> lists = null;
            try
            {
                lists = TemplateManager.ExecuteTemplate(dataDictionary,  application, outputPlugging, gymCode);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                throw ex;
            }
            return lists;
        }

        private void SetPDFFileParth(int id, string fileParth, string flag, int branchId, string user)
        {
            GMSManageMembership.SetPDFFileParth(id, fileParth, flag, branchId, ExceConnectionManager.GetGymCode(user), user);
        }

        public string EmailContractDetail(int branchId, string user, int memberId)
        {
            var mailMsg = string.Empty;
            //var appUrl = ConfigurationManager.AppSettings["USC_MemberContract_Print_App_Path"].ToString();
            //var pdfTemplate = ConfigurationManager.AppSettings["USC_MemberContract_PDF_Template"].ToString();
            //mailMsg = GetFilePathForEmail(appUrl, ExceConnectionManager.GetGymCode(user), pdfTemplate, "Mail", memberId.ToString(), user, "Contract Details");
            return mailMsg;
        }
        //private string GetFilePathForEmail(string application, string dataId, string user, string mailSubject)
        //{
        //    var filePath = string.Empty;
        //    var mailStatus = string.Empty;
        //    try
        //    {
        //        var dataDictionary = new Dictionary<string, string>() { { "itemID", dataId } };
        //        var list = ExcecuteWithUSC(application,  "PDF", dataDictionary, ExceConnectionManager.GetGymCode(user), user);

        //        foreach (var item in list)
        //        {
        //            var Message = item.Messages.Where(mssge => mssge.Header.ToLower().Equals("filepath")).First();
        //            if (Message != null)
        //            {
        //                filePath = Message.Message;
        //                var emailList = EmailExcecuteWithUSC(application, "EMAIL", dataId, ExceConnectionManager.GetGymCode(user), user, filePath, mailSubject);
        //                var mailMessage = emailList.Messages.Where(mssge => mssge.Header.ToLower().Equals("status")).First();
        //                if (mailMessage != null)
        //                {
        //                    mailStatus = mailMessage.Message;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        USLogError.WriteToFile(ex.Message, new Exception(), user);
        //    }
        //    return mailStatus;
        //}

        //private IProcessingStatus EmailExcecuteWithUSC(string application, string outputPlugging, string dataId, string gymCode, string user, string path, string msgSubject)
        //{
        //    IProcessingStatus list = null;
        //    try
        //    {
        //        var plugin = OutputFacade.GetSystemOutputPluginByID(outputPlugging, application);
        //        var adapter = ActionAndParameteFacade.GetNewDataAdapter(application);
        //        var data = adapter.GetDataProvider().GetData(plugin, gymCode, dataId);
        //        var dataVal = new USDataValue();
        //        dataVal.IsRepeatingDataValue = false;
        //        dataVal.Key = "Attachment Path";
        //        dataVal.Type = typeof(string);
        //        dataVal.Value = path;

        //        var subjectVal = new USDataValue();
        //        subjectVal.IsRepeatingDataValue = false;
        //        subjectVal.Key = "Subject Text FilePath";
        //        subjectVal.Type = typeof(string);
        //        subjectVal.Value = msgSubject;

        //        if (data != null & data.Count > 0)
        //        {
        //            data[0].DataValues.Add(dataVal);
        //            data[0].DataValues.Add(subjectVal);
        //            list = TemplateManager.ExecuteTemplate(data[0], application);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        USLogError.WriteToFile(ex.Message, new Exception(), user);
        //    }
        //    return list;
        //}
        #endregion
        //***********************************************************************************************

        public string GetConfigSettings(string type)
        {
            switch (type)
            {
                case "RPT":
                    return ConfigurationManager.AppSettings["ReportViewerURL"].ToString();
                case "EXL":
                    return ConfigurationManager.AppSettings["ExorLiveURL"].ToString();
            }
            return string.Empty;
        }

        public CustomerBasicInfoDC GetMemberBasicInfo(int memberId, string user)
        {
            OperationResult<CustomerBasicInfoDC> result = GMSManageMembership.GetMemberBasicInfo(memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ContractSummaryDC> GetContractSummaries(int memberId, int branchId, string user, bool isFreezDetailNeeded, bool isFreezdView)
        {
            OperationResult<List<ContractSummaryDC>> result = GMSManageMembership.GetContractSummaries(memberId, branchId, ExceConnectionManager.GetGymCode(user), isFreezDetailNeeded, user, isFreezdView);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ContractSummaryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int GetVisitCount(int memberId, DateTime startDate, DateTime endDate, string user)
        {
            OperationResult<int> result = GMSManageMembership.GetVisitCount(memberId, startDate, endDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return 0;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        //shop Login
        public List<SalePointDC> GetSalesPointList(int branchID, string user)
        {
            OperationResult<List<SalePointDC>> result = GMSShopManager.GetSalesPointList(branchID, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<SalePointDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int GetSelectedHardwareProfile(int branchId, string user)
        {
            OperationResult<int> result = GMSShopManager.GetSelectedHardwareProfile(branchId, user, ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<InstallmentDC> MergeOrders(List<InstallmentDC> updatedOrders, List<int> removedOrder, string removedOrderNo, int branchID, int memberContractId, string user)
        {
            OperationResult<List<InstallmentDC>> result = GMSManageMembership.MergeOrders(updatedOrders, removedOrder, memberContractId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstallmentDC>();
            }
            else
            {
                var orderNoList = (updatedOrders.Aggregate(string.Empty, (current, order) => current + order.OrderNo) + ", " + removedOrderNo).Trim();

                // create parameter string for notification
                var paraList = new Dictionary<TemplateFieldEnum, string>
                {
                    {TemplateFieldEnum.CONTRACTNO, updatedOrders.First().MemberContractNo},
                    {TemplateFieldEnum.NOOFORDERS, (removedOrder.Count + 1).ToString()},
                    {TemplateFieldEnum.ORDERNOLIST, orderNoList},
                    {TemplateFieldEnum.CREATEDUSER, user},
                    {TemplateFieldEnum.DATE, DateTime.Now.ToShortDateString()}
                };

                // create common notification
                var commonNotification = new USCommonNotificationDC();
                commonNotification.IsTemplateNotification = true;
                commonNotification.ParameterString = paraList;
                commonNotification.BranchID = branchID;
                commonNotification.CreatedUser = user;
                commonNotification.Role = "MEM";
                commonNotification.NotificationType = NotificationTypeEnum.Messages;
                commonNotification.Severity = NotificationSeverityEnum.Minor;
                commonNotification.MemberID = updatedOrders.First().MemberId;
                commonNotification.Status = NotificationStatusEnum.New;
                commonNotification.Type = TextTemplateEnum.MERGEORDERS;
                commonNotification.Method = NotificationMethodType.EVENTLOG;
                commonNotification.Title = "MERGE ORDERS";
                commonNotification.TempateText = string.Empty;

                //add SMS notification into notification tables
                var Logresult = NotificationAPI.AddNotification(commonNotification);
                if (Logresult.ErrorOccured)
                {
                    foreach (USNotificationMessage message in Logresult.NotificationMessages)
                    {
                        if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                    }
                }
                return result.OperationReturnValue;
            }
        }

        public List<FollowUpTemplateTaskDC> GetFollowUpTaskByTemplateId(int followUpTemplateId, string user)
        {
            OperationResult<List<FollowUpTemplateTaskDC>> result = GMSSystemSettings.GetFollowUpTaskByTemplateId(followUpTemplateId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<FollowUpTemplateTaskDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExceAccessProfileDC> GetAccessProfiles(string user, Gender gender)
        {
            OperationResult<List<ExceAccessProfileDC>> result = GMSSystem.GetAccessProfiles(ExceConnectionManager.GetGymCode(user), gender);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExceAccessProfileDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<MemberBookingDC> GetMemberBooking(int memberId, string user)
        {
            OperationResult<List<MemberBookingDC>> result = GMSManageMembership.GetMemberBookings(memberId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<MemberBookingDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineMemberDC> GetOtherMembersForBooking(int scheduleId, string user)
        {
            OperationResult<List<ExcelineMemberDC>> result = GMSManageMembership.GetOtherMembersForBooking(scheduleId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string GetSponsorsforSponsoring(int branchId, DateTime startDate, DateTime endDate, string user)
        {
            OperationResult<string> result = GMSMembers.GetSponsorsforSponsoring(branchId, startDate, endDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return US.GMS.Core.Utils.XMLUtils.SerializeDataContractObjectToXML(new List<ExcelineMemberDC>());
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool GenerateSponsorOrders(SponsorShipGenerationDetailsDC sponsorDetails, string user)
        {
            OperationResult<bool> result = GMSMembers.GenerateSponsorOrders(sponsorDetails, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool GetSponsoringProcessStatus(string guiID, string user)
        {
            OperationResult<bool> result = GMSMembers.GetSponsoringProcessStatus(guiID, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<ExceNotificationTepmlateDC> GetNotificationTemplateListByMethod(NotificationMethodType method, TextTemplateEnum temp, string user, int branchId)
        {
            USImportResult<List<ExceNotificationTepmlateDC>> result = NotificationAPI.GetNotificationTemplateListByMethod(method, temp, user, branchId);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                return new List<ExceNotificationTepmlateDC>();
            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public string GetSessionValue(string sessionKey, string user)
        {
            return Session.GetSession(sessionKey, ExceConnectionManager.GetGymCode(user));
        }

        public int AddSessionValue(string sessionKey, string value, string user)
        {
            return Session.AddSessionValue(sessionKey, value, ExceConnectionManager.GetGymCode(user));
        }

        public List<USPRole> GetEmpRoles(string user)
        {
            var result = AdminUserSettings.GetUSPRoles("", true, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }

                return new List<USPRole>();
            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public List<MemberIntegrationSettingDC> GetMemberIntegrationSettings(int branchId, int memberId, string user)
        {
            OperationResult<List<MemberIntegrationSettingDC>> result = GMSManageMembership.GetMemberIntegrationSettings(branchId, memberId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<MemberIntegrationSettingDC>();
            }
            return result.OperationReturnValue;
        }

        public bool WellnessIntegrationOperation(OrdinaryMemberDC member, MemberIntegrationSettingDC wellnessIntegrationSetting, WellnessOperationType wellnessOperationType, string user, int branchId)
        {
            var result = GMSManageMembership.WellnessIntegrationOperation(member, wellnessIntegrationSetting, wellnessOperationType, ExceConnectionManager.GetGymCode(user), branchId, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            return result.OperationReturnValue;
        }

        public PackageDC GetRenewTemplate(int templateID, string user, string type)
        {
            OperationResult<PackageDC> result = GMSManageMembership.GetContractTemplateDetails(type, templateID, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineBranchDC> GetBranches(string user, int branchId)
        {
            OperationResult<List<ExcelineBranchDC>> result = GMSSystemSettings.GetBranches(user, ExceConnectionManager.GetGymCode(user), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineBranchDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int PayCreditNote(int aritemNo, List<PayModeDC> paymentModes, int memberId, int shopAccountid, string user, int salePointId, int loggedBranchID)
        {
            OperationResult<int> result = GMSInvoice.PayCreditNote(aritemNo, paymentModes, memberId, shopAccountid, ExceConnectionManager.GetGymCode(user), user, salePointId, loggedBranchID);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public DailySettlementDC GetDailySettlements(int branchId, int salePointId, string user)
        {
            OperationResult<DailySettlementDC> result = GMSShop.GetDailySettlements(salePointId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public Dictionary<string, decimal> GetMemberEconomyBalances(string user, int memberId)
        {
            var result = GMSShopManager.GetMemberEconomyBalances(ExceConnectionManager.GetGymCode(user), memberId);

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new Dictionary<string, decimal>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveWithdrawal(int branchId, WithdrawalDC withDrawal, string user)
        {
            OperationResult<int> result = GMSShopManager.SaveWithdrawal(branchId, withDrawal, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool AddCountryDetails(CountryDC country, string user)
        {
            var result = GMSMember.AddCountryDetails(country, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return true;
            }
            return result.OperationReturnValue;
        }

        public int CheckContractGymChange(int memberID, int memberContractID, int oldBranchID, int newBranchID, string user)
        {
            OperationResult<int> result = GMSManageMembership.CheckContractGymChange(memberID, memberContractID, oldBranchID, newBranchID, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return 0;
            }
            return result.OperationReturnValue;
        }

        public int ValidateChangeGymInMember(int currentBranchID, int newBranchID, int branchID, int memberID, string user)
        {
            var result = GMSManageMembership.ValidateChangeGymInMember(currentBranchID, newBranchID, branchID, memberID, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return 0;
            }
            return result.OperationReturnValue;
        }

        public List<ExceUserBranchDC> GetGymsforAccountNumber(string accountNumber, string user)
        {
            OperationResult<List<ExceUserBranchDC>> result = GMSGymDetail.GetGymsforAccountNumber(accountNumber, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExceUserBranchDC>();
            }
            return result.OperationReturnValue;
        }

        public MemberFeeGenerationResultDC GenerateMemberFee(List<int> gyms, int month, string user)
        {
            OperationResult<MemberFeeGenerationResultDC> result = GMSMember.GenerateMemberFee(gyms, month, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new MemberFeeGenerationResultDC() { Status = false };
            }
            return result.OperationReturnValue;
        }

        public List<string> ConfirmationDetailForGenerateMemberFee(List<int> gyms, int month, string user)
        {
            OperationResult<List<string>> result = GMSMember.ConfirmationDetailForGenerateMemberFee(gyms, month, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<string>();
            }
            return result.OperationReturnValue;
        }

        public bool ValidateGenerateMemberFeeWithMemberFeeMonth(List<int> gyms, string user)
        {
            OperationResult<bool> result = GMSMember.ValidateGenerateMemberFeeWithMemberFeeMonth(gyms, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            return result.OperationReturnValue;
        }

        public List<ExcelineRoleDc> GetUserRole(string user)
        {
            OperationResult<List<ExcelineRoleDc>> result = GMSGymDetail.GetUserRole(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineRoleDc>();
            }
            return result.OperationReturnValue;
        }

        public int ValidateMemberWithStatus(int memberId, int statusId, string user)
        {
            var result = GMSManageMembership.ValidateMemberWithStatus(memberId, statusId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return 0;
            }
            return result.OperationReturnValue;
        }

        public string GetGymCompanySettings(string user, GymCompanySettingType gymCompanySettingType)
        {
            OperationResult<string> result = new OperationResult<string>();
                //GMSSystemSettings.GetGymCompanySettings(ExceConnectionManager.GetGymCode(user), gymCompanySettingType);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public string GetExorLiveClientAppUrl()
        {
            return ConfigurationSettings.AppSettings["ExorLiveClientAppUrl"].ToString(CultureInfo.InvariantCulture);
        }

        public Dictionary<string,string> GetEncryptKey(string gymCode,string custId)
        {
            var result = new Dictionary<string, string> {{"gymCode", Encrypt(gymCode)}, {"custId", Encrypt(custId)}};
            return result;
        }

        private string Encrypt(string text)
        {
            const string encryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(text);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    text = Convert.ToBase64String(ms.ToArray());
                }
            }
            return text;
        }
        
        //------------------Dummy Method--------------------
        public SystemSettingEconomySettingDC DummyGetEconomySystemSetting()
        {
            SystemSettingEconomySettingDC setting = new SystemSettingEconomySettingDC();
            return setting;
        }

        public int CancelPayment(int paymentID, bool keepthePayment, string user, string comment)
        {
            var result = GMSPayment.CancelPayment(paymentID, keepthePayment,user,comment, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int GetNextGiftVoucherNumber(string seqId, string subSeqId, string user)
        {
            var result = GMSShopManager.GetNextGiftVoucherNumber(seqId, subSeqId, ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int ValidateGiftVoucherNumber(string voucherNumber, decimal payment, string user)
        {
            var result = GMSShopManager.ValidateGiftVoucherNumber(voucherNumber, payment, ExceConnectionManager.GetGymCode(user));

            if (!result.ErrorOccured)
            {
                return result.OperationReturnValue;
            }
            else
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -3;
            }
        }

        public Dictionary<string, string> GetGiftVoucherDetail(string user)
        {
            var result = GMSShopManager.GetGiftVoucherDetail(ExceConnectionManager.GetGymCode(user));
            if (!result.ErrorOccured)
            {
                return result.OperationReturnValue;
            }
            else
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
        }

        public int GetPriceChangeContractCount(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string user, bool changeToNextTemplate)
        {
            var result = GMSManageMembership.GetPriceChangeContractCount(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, ExceConnectionManager.GetGymCode(user), changeToNextTemplate);
            if (!result.ErrorOccured)
            {
                return result.OperationReturnValue;
            }
            else
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
        }

        public bool UpdatePrice(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string user, bool changeToNextTemplate)
        {
            var result = GMSManageMembership.UpdatePrice(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, ExceConnectionManager.GetGymCode(user), changeToNextTemplate, user);
            if (!result.ErrorOccured)
            {
                return result.OperationReturnValue;
            }
            else
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
        }

        public string GetPriceUpdateContracts(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string user, bool changeToNextTemplate, int hit)
        {
            var result = GMSManageMembership.GetPriceUpdateContracts(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, ExceConnectionManager.GetGymCode(user),changeToNextTemplate, hit);
            if (!result.ErrorOccured)
            {
                return result.OperationReturnValue;
               
            }
            else
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return XMLUtils.SerializeDataContractObjectToXML(new List<ExcelineContractDC>());
            }
        }

        public ExcelineContractDC getList()
        {
            return  new ExcelineContractDC();
        }

        public List<PackageDC> GetTemplatesForPriceUpdate(string user)
        {
            var result = GMSManageMembership.GetTemplatesForPriceUpdate(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<PackageDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #region InfoGym
        public string InfoGymGetAccessToken(string scope, string user)
        {
            try
            {
                var clientId = ConfigurationManager.AppSettings["InfoGym_ClientId"];
                var clientSecret = ConfigurationManager.AppSettings["InfoGym_ClientSecret"];
                var host = ConfigurationManager.AppSettings["InfoGym_TokenEndpoint"];
                
                var grantType = ConfigurationManager.AppSettings["InfoGym_GrantType"];
                var org = ConfigurationManager.AppSettings["InfoGym_Org"];
                var data = string.Format("grant_type={0}&org={1}&scope={2}", grantType, org, scope);

                return GMSManageMembership.InfoGymGetAccessToken(clientId, clientSecret, host, data);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, ex, user);
                return string.Empty;
            }
        }

        public string InfoGymRegisterMember(InfoGymMember member, int memberID, string user)
        {
            try
            {
                var host = ConfigurationManager.AppSettings["InfoGym_ApiEndpoint"];
                var org = ConfigurationManager.AppSettings["InfoGym_Org"];
                var url = string.Format("https://{0}/orgs/{1}/members", host, org);
                var scope = string.Format("orgs:{0}:members:write", org);

                var accessToken = InfoGymGetAccessToken(scope, user);
                var infoId = GMSManageMembership.InfoGymRegisterMember(member, url, host, accessToken);

                GMSManageMembership.InfoGymUpdateExceMember(infoId, memberID, user);
                return infoId;
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, ex, user);
                return string.Empty;
            }
        }

        public string InfoGymUpdateMember(InfoGymMember member, string inforGymId, string user)
        {
            try
            {
                var host = ConfigurationManager.AppSettings["InfoGym_ApiEndpoint"];
                var org = ConfigurationManager.AppSettings["InfoGym_Org"];
                var url = string.Format("https://{0}/orgs/{1}/members/{2}", host, org, inforGymId);
                var scope = string.Format("orgs:{0}:members:write", org);

                var accessToken = InfoGymGetAccessToken(scope, user);
                return GMSManageMembership.InfoGymUpdateMember(member, url, host, accessToken);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, ex, user);
                return string.Empty;
            }
        }


        public DateTime? ValidateSponsoringGeneration(string user)
        {
            var result = GMSManageMembership.ValidateSponsoringGeneration(ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion


        public bool UpdateSettingForUserRoutine(string key, string value, string user)
        {

            var result = GMSUser.UpdateSettingForUserRoutine(key, value, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }

          
        }

        public PDFPrintResultDC PrintMemberSales(int memberID, DateTime? startdate, DateTime? endDate, string user, int branchID)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var appUrl = ConfigurationManager.AppSettings["USC_MemberSales_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "MemberID", memberID.ToString() }, { "fromDate", startdate.Value.ToString("yyyy-MM-dd") }, { "toDate", endDate.Value.ToString("yyyy-MM-dd") }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(memberID, result.FileParth, "MSE", branchID, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchID, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public string GetMemberInvoicesForContract(int memberId, int contractId, int hit, string user)
        {
            var result = GMSInvoice.GetMemberInvoicesForContract(memberId, contractId, hit, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return XMLUtils.SerializeDataContractObjectToXML(new List<MemberInvoiceDC>());
            }
            else
            {
                return XMLUtils.SerializeDataContractObjectToXML(result.OperationReturnValue);
            }
        }

       

        public bool IsBrisIntegration()
        {
            var brisIntegration = ConfigurationManager.AppSettings["BrisIntegration"];
            return brisIntegration == "1";
        }

        public ContractCondition TempContractCondition()
        {
            throw new NotImplementedException();
        }

        public List<OrdinaryMemberDC> SearchBrisMember(string userId, string firstName, string lastName, string email, string mobile, string birthDay, string cardNumber, string ssn, string user)
        {
            var brisIntegrationApiKey = ConfigurationManager.AppSettings["Bris_Integration_ApiKey"];
           // File.AppendAllText(@"C:\Exceline\Logs\Test.txt", "Accessed " + Environment.NewLine);
            var result = new OperationResult<List<OrdinaryMemberDC>>();
            var brisServiceIntegration = new BrisServiceIntegration();
            try
            {
                result.OperationReturnValue = brisServiceIntegration.SearchBrisMember(brisIntegrationApiKey, userId, firstName, lastName, email, mobile, birthDay, cardNumber, ssn);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting bris member" + ex.Message, MessageTypes.ERROR);

                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
            }
            }
            return result.OperationReturnValue;
        }

        public OrdinaryMemberDC SaveBrisMember(OrdinaryMemberDC member)
        {
            var brisIntegrationApiKey = ConfigurationManager.AppSettings["Bris_Integration_ApiKey"];
            var result = new OperationResult<OrdinaryMemberDC>();
            var brisServiceIntegration = new BrisServiceIntegration();
            try
            {
                result.OperationReturnValue = brisServiceIntegration.SaveBrisMember(brisIntegrationApiKey, member);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in saving bris member" + ex.Message, MessageTypes.ERROR);

                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), "");
                }
            }
            return result.OperationReturnValue;
        }



       
    }
}
