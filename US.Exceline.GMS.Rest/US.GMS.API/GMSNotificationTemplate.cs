﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.ResultNotifications;
using US.GMS.BusinessLogic;

namespace US.GMS.API
{
   public class GMSNotificationTemplate
    {
       public static OperationResult<List<NotificationTemplateDC>> GetNotificationTemplatesByType(int templateTypeId, NotifyMethodType notifyMethod, int branchId, string gymCode)
       {
           return NotificationTemplateManager.GetNotificationTemplatesByType(templateTypeId, notifyMethod, branchId, gymCode);
       }
    }
}
