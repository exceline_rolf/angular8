﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : "8/16/2012 6:25:16 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using US.Common.Logging.API;
using US.GMS.BusinessLogic;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Payment.Core.BusinessDomainObjects;
using US.USDF.Core.DomainObjects;

namespace US.GMS.API
{
    public class GMSCcx
    {
        public static OperationResult<string> GetAllInvoices(int chunkSize, int creditorNo, int batchsequenceNo, List<string> invoiceTypes)
        {
            USLogEvent.WriteToFile("*********************GetAllInvoices-Begin*******************",Convert.ToString(creditorNo));
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                string gymCode = GetGymCodeForCreditor(creditorNo).OperationReturnValue;
                USLogEvent.WriteToFile("Gym Code :" + gymCode, Convert.ToString(creditorNo));
                OperationResult<USDFInvoiceInfo> invoiceResult = CCXManager.GetAllInvoices(chunkSize, gymCode, creditorNo, batchsequenceNo, invoiceTypes);
                USLogEvent.WriteToFile("Case count (" + invoiceResult.OperationReturnValue.Cases.Count + ")", Convert.ToString(creditorNo));
                foreach (var caseRecord in invoiceResult.OperationReturnValue.Cases)
                {
                    caseRecord.CaseNo = "0";
                    foreach (var transRecord in caseRecord.Transactions)
                    {
                        transRecord.CaseNo = "0";
                    }

                }
                result.OperationReturnValue = ConvertToString(invoiceResult.OperationReturnValue);
                foreach (NotificationMessage message in invoiceResult.Notifications)
                {
                    result.CreateMessage(message.Message, MessageTypes.ERROR);
                }
            }
            catch (Exception ex)
            {
                result.OperationReturnValue = ConvertToString(new USDFInvoiceInfo());
                USLogError.WriteToFile("Error In GetAllInvoices(API):" + ex.Message, ex, Convert.ToString(creditorNo));
                if(ex.StackTrace != null)
                    USLogError.WriteToFile("Error In GetAllInvoices(API):" + ex.StackTrace.ToString(), ex, Convert.ToString(creditorNo));
            }
            USLogEvent.WriteToFile("*********************GetAllInvoices-End*******************", Convert.ToString(creditorNo));
           
            return result;
        }

        public static OperationResult<string> GetDebtWarningInvoices(int chunkSize, int creditorNo, int batchsequenceNo)
        {
            USLogEvent.WriteToFile("*********************GetDebtWarningInvoices-Begin*******************", Convert.ToString(creditorNo));
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                string gymCode = GetGymCodeForCreditor(creditorNo).OperationReturnValue;
                USLogEvent.WriteToFile("Gym Code :" + gymCode, Convert.ToString(creditorNo)); 
                OperationResult<USDFInvoiceInfo> invoiceResult = CCXManager.GetDebtWarningInvoices(chunkSize, gymCode, creditorNo, batchsequenceNo);
                USLogEvent.WriteToFile("Case count (" + invoiceResult.OperationReturnValue.Cases.Count + ")", Convert.ToString(creditorNo));
                foreach (var caseRecord in invoiceResult.OperationReturnValue.Cases)
                {
                    caseRecord.CaseNo = "0";
                    foreach (var transRecord in caseRecord.Transactions)
                    {
                        transRecord.CaseNo = "0";
                    }

                }
                result.OperationReturnValue = ConvertToString(invoiceResult.OperationReturnValue);
                foreach (NotificationMessage message in invoiceResult.Notifications)
                {
                    result.CreateMessage(message.Message, MessageTypes.ERROR);
                }
            }
            catch (Exception ex)
            {
                result.OperationReturnValue = ConvertToString(new USDFInvoiceInfo());
                USLogError.WriteToFile("Error In GetDebtWarningInvoices(API):" + ex.Message, ex, Convert.ToString(creditorNo));
                if (ex.StackTrace != null)
                    USLogError.WriteToFile("Error In GetAllInvoices(API):" + ex.StackTrace.ToString(), ex, Convert.ToString(creditorNo));
            }
            USLogEvent.WriteToFile("*********************GetDebtWarningInvoices-End*******************", Convert.ToString(creditorNo));
            return result;
        }

        public static OperationResult<string> GetDirectPayments(int chunkSize,int creditorNo,int batchSequenceNo)
        {
            USLogEvent.WriteToFile("*********************GetDirectPayments-Begin*******************", Convert.ToString(creditorNo));
            
            OperationResult<string> result = new OperationResult<string>();
            try
            { 
                string gymCode = GetGymCodeForCreditor(creditorNo).OperationReturnValue;
                USLogEvent.WriteToFile("Gym Code :" + gymCode, Convert.ToString(creditorNo)); 
                OperationResult<USDFInvoiceInfo> paymentResult = CCXManager.GetDirectPayments(chunkSize, gymCode,creditorNo,batchSequenceNo);
                USLogEvent.WriteToFile("Payment count (" + paymentResult.OperationReturnValue.Cases.Count + ")", Convert.ToString(creditorNo));
                result.OperationReturnValue = ConvertToString(paymentResult.OperationReturnValue);
            }
            catch (Exception ex)
            {
                result.OperationReturnValue = ConvertToString(new USDFInvoiceInfo());
                USLogError.WriteToFile("Error In GetDirectPayments(API):" + ex.Message, ex, Convert.ToString(creditorNo));
                if (ex.StackTrace != null)
                    USLogError.WriteToFile("Error In GetAllInvoices(API):" + ex.StackTrace.ToString(), ex, Convert.ToString(creditorNo));
            }
            USLogEvent.WriteToFile("*********************GetDirectPayments-End*******************", Convert.ToString(creditorNo));
            return result;
        }

        public static OperationResult<int> AddPaymentStatus(string paymentStatusData, int creditorNo)
        {
            USLogEvent.WriteToFile("*********************AddPaymentStatus-Begin*******************", Convert.ToString(creditorNo));
            USPPaymentStatusFile paymentFile = new USPPaymentStatusFile();
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                paymentFile = (USPPaymentStatusFile)XMLUtils.DeserializeObject<USPPaymentStatusFile>(paymentStatusData);                
                string gymCode = GetGymCodeForCreditor(creditorNo).OperationReturnValue;
                USLogEvent.WriteToFile("Gym Code :" + gymCode, Convert.ToString(creditorNo));
                USLogEvent.WriteToFile("Payment status count (" + paymentFile.FileRowList.Count + ")", Convert.ToString(creditorNo));
                foreach (var fileRow in paymentFile.FileRowList)
                {
                    CCXManager.AddPaymentStatus(fileRow, gymCode);
                }

                result.OperationReturnValue = 1;
            }
            catch (Exception  ex)
            {
                result.OperationReturnValue = -1;
                USLogError.WriteToFile("Error In AddPaymentStatus(API):" + ex.Message, ex, Convert.ToString(creditorNo));
                if (ex.StackTrace != null)
                    USLogError.WriteToFile("Error In GetAllInvoices(API):" + ex.StackTrace.ToString(), ex, Convert.ToString(creditorNo));
            }
            USLogEvent.WriteToFile("*********************AddPaymentStatus-End*******************", Convert.ToString(creditorNo));
            return result;
        }

        public static OperationResult<int> AddNewCancelledStatus(string newCancelledStatusData, int creditorNo)
        {
            USLogEvent.WriteToFile("*********************AddNewCancelledStatus-Begin*******************", Convert.ToString(creditorNo));
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPNewOrCancelledFile newCancelledFile = new US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPNewOrCancelledFile();
                newCancelledFile = (US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPNewOrCancelledFile)XMLUtils.DeserializeObject<US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPNewOrCancelledFile>(newCancelledStatusData);             
                string gymCode = GetGymCodeForCreditor(creditorNo).OperationReturnValue;
                USLogEvent.WriteToFile("Gym Code :" + gymCode, Convert.ToString(creditorNo));
                USLogEvent.WriteToFile("New cancel status count (" + newCancelledFile.FileRowList.Count + ")", Convert.ToString(creditorNo));
                foreach (var fileRow in newCancelledFile.FileRowList)
                {
                    CCXManager.AddNewCancelledStatus(fileRow, gymCode);                   
                }
                result.OperationReturnValue = 1;
            }
            catch (Exception ex)
            {
                result.OperationReturnValue = -1;
                USLogError.WriteToFile("Error In AddNewCancelledStatus(API):" + ex.Message, ex, Convert.ToString(creditorNo));
                if (ex.StackTrace != null)
                    USLogError.WriteToFile("Error In GetAllInvoices(API):" + ex.StackTrace.ToString(), ex, Convert.ToString(creditorNo));
            }
            USLogEvent.WriteToFile("*********************AddNewCancelledStatus-End*******************", Convert.ToString(creditorNo));
            return result;
        }

        public static OperationResult<int> AddNewCancelToDB(US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPNewOrCancelledFileRow newCancelledStatus, string gymCode, int creditor)
        {

            OperationResult<int> result = new OperationResult<int>();

            try
            {
                result = CCXManager.AddNewCancelledStatus(newCancelledStatus, gymCode); 
            }
            catch (Exception ex)
            {
                
                result.OperationReturnValue = -1;
                USLogError.WriteToFile("Error In AddNewCancelToDB(API):" + ex.Message, ex, Convert.ToString(creditor));
            }
            return result;
        
        }


        public static OperationResult<int> AddDataImportReceipt(string dataImportReceiptData, int creditorNo)
        {
            USLogEvent.WriteToFile("*********************AddDataImportReceipt-Begin*******************", Convert.ToString(creditorNo));
            OperationResult<int> result = new OperationResult<int>();
         
            try
            {
                DataImportReceipt dataImportReceipt = new DataImportReceipt();
                dataImportReceipt = (DataImportReceipt)XMLUtils.DeserializeObject<DataImportReceipt>(dataImportReceiptData);
                string gymCode = GetGymCodeForCreditor(creditorNo).OperationReturnValue;

                USLogEvent.WriteToFile("Gym Code :" + gymCode, Convert.ToString(creditorNo));
                USLogEvent.WriteToFile("Import receipt count (" + dataImportReceipt.DetailRowList.Count + ")", Convert.ToString(creditorNo));
                foreach (var detailRow in dataImportReceipt.DetailRowList)
                {
                    CCXManager.AddDataImportReceipt(detailRow, gymCode);
                }
            }
            catch (Exception ex)
            {
                result.OperationReturnValue = -1;
                USLogError.WriteToFile("Error In AddNewCancelledStatus(API):" + ex.Message, ex, Convert.ToString(creditorNo));
                if (ex.StackTrace != null)
                    USLogError.WriteToFile("Error In GetAllInvoices(API):" + ex.StackTrace.ToString(), ex, Convert.ToString(creditorNo));
            }
            USLogEvent.WriteToFile("*********************AddDataImportReceipt-End*******************", Convert.ToString(creditorNo));
            return result;
        }


        public static OperationResult<int> ImportDataImportReceipt(string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                //string gymCode = GetGymCodeForCreditor(creditorNo).OperationReturnValue;
               result= CCXManager.ImportDataImportReceipt(gymCode);
               
            }
            catch (Exception)
            {
                result.OperationReturnValue = -1;
                throw;
            }
            return result;
        }

        public static OperationResult<int> ImportPaymentStatus( int creditorNo,string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                //string gymCode = GetGymCodeForCreditor(creditorNo).OperationReturnValue;
                result = CCXManager.ImportPaymentStatus(gymCode);

            }
            catch (Exception ex)
            {
                result.OperationReturnValue = -1;
                USLogError.WriteToFile("Error In ImportPaymentStatus(API):" + ex.Message, ex, Convert.ToString(creditorNo));
                if (ex.StackTrace != null)
                    USLogError.WriteToFile("Error In GetAllInvoices(API):" + ex.StackTrace.ToString(), ex, Convert.ToString(creditorNo));
            }
            return result;
        }

        public static OperationResult<int> ImportNewCancelledStatus(string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result = CCXManager.ImportNewCancelledStatus(gymCode);
            }
            catch (Exception)
            {
                result.OperationReturnValue = -1;
                throw;
            }
            return result;
        }


        public static string GetInvoices(DateTime startDate, DateTime endDate, int chunkSize)
        {
            return string.Empty;
        }

        public static OperationResult<string> GetGymCodeForCreditor(int creditorNo)
        {
            return CCXManager.GetGymCodeForCreditor(creditorNo);
        }


        private static string ConvertToString(USDFInvoiceInfo invoiceInfo)
        {
            if (invoiceInfo == null)
                return string.Empty;
            else
                return XMLUtils.SerializeToXMLString(invoiceInfo);           
        }

        public static OperationResult<int> UpdateDWStatus(string gymCode)
        {
            return CCXManager.UpdateDWStatus(gymCode);
        }

        public static OperationResult<int> UpdateTemplateStatus(string gymCode)
        {
            return CCXManager.UpdateTemplateStatus(gymCode);
        }

        public static OperationResult<List<GymCompanyDC>> GetGymCompanies()
        {
            return CCXManager.GetGymCompanies();
        }

        public static OperationResult<bool> UpdateExportStatus(string gymCode)
        {
            return CCXManager.UpdateExportStatus(gymCode);
        }

        public static OperationResult<string> GetClaimStatus(string gymCode, int hit, int status, DateTime date, ImportStatusDC advancedData)
        {
            return CCXManager.GetClaimStatus(gymCode, hit, status, date, advancedData);
        }

        public static OperationResult<bool> ReScheduleClaim(string gymCode, int itemNo)
        {
            return CCXManager.ReScheduleClaim(gymCode, itemNo);
        }

        public static OperationResult<string> GetPaymentImportStatusXML( string gymCode, int hit, DateTime regDate)
        {
            return CCXManager.GetPaymentImportStatusXML(gymCode, hit, regDate);
        }

        public static OperationResult<List<ImportStatusDC>> GetClaimExportStatusForMember(string gymCode, int memberID, int hit, DateTime? transferDate, string type, string user)
        {
            return CCXManager.GetClaimExportStatusForMember(gymCode, memberID, hit, transferDate, type, user);
        }

        public static OperationResult<bool> MoveToShopAccount(int statusID, bool isImportStatus, string gymCode)
        {
            return CCXManager.MoveToShopAccount(statusID, isImportStatus, gymCode);
        }

        public static OperationResult<int> UpdateAddress(string addressData, int creditorNo)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                List<USPAddress> address = new List<USPAddress>();
                address = XMLUtils.DeserializeObject<List<USPAddress>>(addressData);
                string gymCode = GetGymCodeForCreditor(creditorNo).OperationReturnValue;

                CCXManager.UpdateAddress(address);
                
            }
            catch (Exception ex)
            {
                result.OperationReturnValue = -1;
                USLogError.WriteToFile("Error In GetDirectPayments(API):" + ex.Message, ex, Convert.ToString(creditorNo));
                if (ex.StackTrace != null)
                    USLogError.WriteToFile("Error In GetAllInvoices(API):" + ex.StackTrace.ToString(), ex, Convert.ToString(creditorNo));
            }
            return result;
        }
    }
}
