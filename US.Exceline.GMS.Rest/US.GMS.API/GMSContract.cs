﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.BusinessLogic;

namespace US.Exceline.GMS.API
{
    public class GMSContract
    {

        public static OperationResult<List<PackageDC>> GetContracts(int branchId, string searchText, string gymCode)
        {
            return ContractManager.GetContracts(branchId, searchText, gymCode);
        }

        public static OperationResult<List<ActivityDC>> GetActivities(int branchId, string gymCode)
        {
            return ContractManager.GetActivities(branchId, gymCode);
        }

        public static OperationResult<List<ArticleDC>> GetArticlesForCommonUse(int categoryID, string gymCode, int branchID)
        {
            return ContractManager.GetArticlesForCommonUse(categoryID, gymCode, branchID);
        }

        public static OperationResult<bool> ManageTemplate(string gymCode)
        {
            return ContractManager.ManageTemplate(gymCode);
        }
    }
}
