﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.BusinessLogic.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.GMS.Core.ResultNotifications;

namespace US.GMS.API.ManageSystemSettings
{
    public class ManageClassTypeSetting
    {
        public static OperationResult<List<ExceClassTypeDC>> GetClassTypes(string gymCode)
        {
            return ClassTypesSettingManager.GetClassTypes(gymCode);
        }

        public static OperationResult<List<ExceClassTypeDC>> GetClassTypesByBranch(int branchId, string gymCode)
        {
            return ClassTypesSettingManager.GetClassTypesByBranch(branchId,gymCode);
        }
        
        public static OperationResult<bool> AddEditClassType(ExceClassTypeDC classType, string gymCode)
        {
            return ClassTypesSettingManager.AddEditClassType(classType, gymCode);
        }

        public static OperationResult<int> DeleteClassType(int classTypeId, string gymCode)
        {
            return ClassTypesSettingManager.DeleteClassType(classTypeId, gymCode);
        }

    }
}
