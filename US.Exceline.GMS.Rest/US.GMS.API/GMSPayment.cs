﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.BusinessLogic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageShop;

namespace US.GMS.API
{
    public class GMSPayment
    {
        public static OperationResult<string> RegisterPayment(int branchId, int memberId, int articleNo, decimal paymentAmount, decimal discount, string createdUser, decimal invoiceAmount, PaymentTypes paymentType, string gymCode)
        {
            return PaymentsManager.RegisterPayment(branchId, memberId, articleNo, paymentAmount, discount, createdUser, invoiceAmount, paymentType, gymCode);
        }

        public static OperationResult<SaleResultDC> RegisterInstallmentPayment(int memberBranchID, int loggedbranchID, string user, InstallmentDC installment, PaymentDetailDC paymentDetail, string gymCode, int salePointID, ShopSalesDC salesDetails)
        {
            return PaymentsManager.RegisterInstallmentPayment(memberBranchID, loggedbranchID, user, installment, paymentDetail, gymCode, salePointID, salesDetails);
        }

        public static OperationResult<SaleResultDC> GenerateInvoice(int invoiceBranchID, string user, Core.DomainObjects.ManageMemberships.InstallmentDC installment, PaymentDetailDC paymentDetail, string invoiceType, string gymCode, int memberBranchID)
        {
            return PaymentsManager.GenerateInvoice(invoiceBranchID, user, installment, paymentDetail, invoiceType, gymCode, memberBranchID);
        }

        public static OperationResult<SaleResultDC> RegisterInvoicePayment(PaymentDetailDC paymentDetails, string gymCode, int branchId, ShopSalesDC salesDetails, string user)
        {
            return PaymentsManager.RegisterInvoicePayment(paymentDetails, gymCode, branchId,salesDetails, user);
        }

        public static OperationResult<int> CancelPayment(int paymentID, bool keepthePayment, string user, string comment, string gymCode)
        {
            return PaymentsManager.CancelPayment(paymentID, keepthePayment, user, comment,gymCode);
        }
    }
}
