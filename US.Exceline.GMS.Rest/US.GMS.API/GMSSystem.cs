﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.BusinessLogic;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;

namespace US.GMS.API
{
    public class GMSSystem
    {
        public static OperationResult<List<ExceAccessProfileDC>> GetAccessProfiles(string gymCode, Gender gender)
        {
            return SystemManager.GetAccessProfiles(gymCode, gender);
        }

        public static OperationResult<int> UpdateAccessProfiles(ExceAccessProfileDC AccessProfile, string user, string gymCode)
        {
            return SystemManager.UpdateAccessProfiles(AccessProfile, user, gymCode);
        }

        public static OperationResult<List<String>> ActGetFileData(string gymCode)
        {
            return SystemManager.ActGetFileData(gymCode);
        }

        public static OperationResult<bool> SaveTaskStatus(string taskName, string description, int branchId, int status, string gymCode)
        {
            return SystemManager.SaveTaskStatus(taskName, description, branchId, status, gymCode);
        }

        public static OperationResult<bool> ValidateEmail(string email, string gymCode)
        {
            return SystemManager.ValidateEmail(email, gymCode);
        }

        public static OperationResult<bool> ValidateMemberCard(string memberCard, int memberId, string gymCode)
        {
            return SystemManager.ValidateMemberCard(memberCard, memberId, gymCode);
        }

        public static OperationResult<bool> ValidateGatCardNo(string gatCardNo, string gymCode)
        {
            return SystemManager.ValidateGatCardNo(gatCardNo, gymCode);
        }
        public static OperationResult<Tuple<int, string>> ValidateMobile(string mobile, string mobilePrefix, string gymCode)
        {
            return SystemManager.ValidateMobile(mobile, mobilePrefix, gymCode);
        }
    }
}
