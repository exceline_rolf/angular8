﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/19/2012 3:24:24 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.ResultNotifications;
using US.GMS.BusinessLogic;

namespace US.GMS.API
{
    public class GMSCategory
    {
        public static OperationResult<List<CategoryTypeDC>> GetCategoryTypes(string name, string user, int branchId, string gymCode)
        {
            return CategoryManager.GetCategoryTypes(name, user, branchId, gymCode);
        }

        public static OperationResult<int> SaveCategory(CategoryDC category, string user, int branchId, string gymCode)
        {
            return CategoryManager.SaveCategory(category, user, branchId, gymCode);
        }


        public static OperationResult<int> SaveRegion(RegionDC region, string user, int branchId, string gymCode)
        {
            return CategoryManager.SaveRegion(region, user, branchId, gymCode);
        }

        public static OperationResult<List<CategoryDC>> GetCategoriesByType(string type,string user,string gymCode)
        {
            return CategoryManager.GetCategoriesByType(type, user, gymCode);
        }

        public static OperationResult<bool> UpdateCategory(CategoryDC category, string user, int branchId, string gymCode)
        {
            return CategoryManager.UpdateCategory(category, user, branchId, gymCode);
        }

        public static OperationResult<int> DeleteCategory(int categoryId, string user, int branchId, string gymCode)
        {
            return CategoryManager.DeleteCategory(categoryId, user, branchId, gymCode);
        }

        public static OperationResult<List<CategoryDC>> SearchCategories(string searchText, string searchType, string user, int branchId, string gymCode)
        {
            return CategoryManager.SearchCategories(searchText, searchType, user, branchId, gymCode);
        }

        public static OperationResult<List<RegionDC>> GetRegions(int branchId, string gymCode, string countryId)
        {
            return CategoryManager.GetRegions(branchId, gymCode,countryId);
        }

    }
}
