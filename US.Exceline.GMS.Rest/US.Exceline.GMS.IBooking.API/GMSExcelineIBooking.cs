﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.IBooking.BusinessLogic;
using US.Exceline.GMS.IBooking.Core;
using US.GMS.Core.DomainObjects.API;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.IBooking.API
{
    public class GMSExcelineIBooking
    {
        public static OperationResult<List<ExceIBookingMember>> GetIBookingMembers(string gymCode, int branchId, int changedSinceDays, int systemId)
        {
            return ExcelineIBookingManager.GetMembers(gymCode, branchId, changedSinceDays, systemId);
        }

        public static OperationResult<List<ExceIBookingMember>> GetIBookingMembers(string gymCode, int branchId, int systemId)
        {
            return ExcelineIBookingManager.GetMembers(gymCode, branchId, systemId);
        }

        public static OperationResult<List<ExceIBookingMember>> GetIBookingMemberById(string gymCode, int branchId, string customerNo, int systemId)
        {
            return ExcelineIBookingManager.GetMemberById(gymCode, branchId, customerNo, systemId);
        }

        public static OperationResult<ExceIBookingSystem> GetIBookingSystemDetails(string gymCode, int systemId)
        {
            return ExcelineIBookingManager.GetSystemDetails(gymCode, systemId);
        }

        public static OperationResult<List<ExceIBookingGym>> GetIBookingGyms(string gymCode)
        {
            return ExcelineIBookingManager.GetGyms(gymCode);
        }

        public static OperationResult<List<ExceIBookingWebOffering>> GetIBookingWebOfferings(string gymCode)
        {
            return ExcelineIBookingManager.GetWebOfferings(gymCode);
        }

        public static OperationResult<List<ExceIBookingInstructor>> GetIBookingInstructors(string gymCode)
        {
            return ExcelineIBookingManager.GetInstructors(gymCode);
        }

        public static OperationResult<List<ExceIBookingClassCategory>> GetIBookingClassCategories(string gymCode)
        {
            return ExcelineIBookingManager.GetIBookingClassCategories(gymCode);
        }

        public static OperationResult<List<ExceIBookingClassKeyword>> GetIBookingClassKeyword(string gymCode)
        {
            return ExcelineIBookingManager.GetIBookingClassKeyword(gymCode);
        }

        public static OperationResult<List<CategoryDC>> GetCategoriesByType(string type, string gymCode)
        {
            return ExcelineIBookingManager.GetCategoriesByType(type, gymCode);
        }

        public static OperationResult<ContractResignDetails> CancelIBookingContract(int systemId, int gymId, int customerId, string gymCode)
        {
            return ExcelineIBookingManager.CancelIBookingContract(systemId, gymId, customerId, gymCode);
        }

        public static OperationResult<int> DeleteIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string gymCode)
        {
            return ExcelineIBookingManager.DeleteIBookingClassBooking(systemId, gymId, classId, customerId, gymCode);
        }


        public static OperationResult<int> AddIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string gymCode)
        {
            return ExcelineIBookingManager.AddIBookingClassBooking( systemId,  gymId,  classId,  customerId,  gymCode);
        }

        public static OperationResult<List<ExceIBookingStartReason>> GetIBookingStartReasons(string gymCode)
        {
            return ExcelineIBookingManager.GetIBookingStartReasons(gymCode);
        }

        public static OperationResult<List<ExceIBookingClassType>> GetIBookingClassTypes(string gymCode)
        {
            return ExcelineIBookingManager.GetClassTypes(gymCode);
        }

        public static OperationResult<List<ExceIBookingClassCalendar>> GetIBookingClassSchedules(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            return ExcelineIBookingManager.GetClassSchedules(gymCode, branchId, fromDate, toDate);
        }

        public static OperationResult<List<ExceIBookingShowUp>> GetIBookingExcelineShowUps(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            return ExcelineIBookingManager.GetExcelineShowUps(gymCode, branchId, fromDate, toDate);
        }

        public static OperationResult<List<ExceIBookingPayment>> GetIBookingPayments(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            return ExcelineIBookingManager.GetPayments(gymCode, branchId, fromDate, toDate);
        }

        public static OperationResult<string> RegisterNewIBookingMember(string gymCode, ExceIBookingNewMember member, string user)
        {
            return ExcelineIBookingManager.RegisterNewIBookingMember(gymCode, member, user);
        }

        public static OperationResult<int> UpdateIBookingClass(string gymCode, ExceIBookingUpdateClass exceClass)
        {
            return ExcelineIBookingManager.UpdateIBookingClass(gymCode, exceClass);
        }

        public static OperationResult<int> UpdateIBookingMemberClassVisit(string gymCode, ExceIBookingMemberClassVisit visit)
        {
            return ExcelineIBookingManager.UpdateIBookingMemberClassVisit(gymCode, visit);
        }

        public static OperationResult<int> UpdateIBookingMemberVisit(string gymCode, ExceIBookingMemberVisit visit)
        {
            return ExcelineIBookingManager.UpdateIBookingMemberVisit(gymCode, visit);
        }

        public static OperationResult<int> UpdateIBookingMember(string gymCode, ExceIBookingUpdateMember member)
        {
            return ExcelineIBookingManager.UpdateIBookingMember(gymCode, member);
        }

        public static OperationResult<List<ExceIBookingResourceCalendar>> GetResourceCalendar(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            // return ExcelineIBookingManager.GetExcelineShowUps(gymCode, branchId, fromDate, toDate);
            return null;
        }

        public static OperationResult<List<ExceIBookingResourceBookingType>> GetResourceBookingTypes(string gymCode, int branchId)
        {
            // return ExcelineIBookingManager.GetIBookingStartReasons(gymCode, branchId);
            return null;
        }



        public static OperationResult<string> GetGymCodeByCompanyId(string systemId)
        {
            return ExcelineIBookingManager.GetGymCodeByCompanyId(systemId);
        }

        public static OperationResult<bool> GetIBookingEntityValid(int entityId, string entityType, int branchId, string gymCode)
        {
            return ExcelineIBookingManager.GetEntityValid(entityId, entityType, branchId, gymCode);
        }

        public static ExceIBookingNotificationDetails GetTemplateDetails(string gymCode, string templateNo, int branchID, string type)
        {
            return ExcelineIBookingManager.GetTemplateDetails(gymCode, templateNo, branchID, type);
        }

        public static List<InstallmentDC> GetTodayOrder(string gymCode, int savedMemberID, int memberContractID)
        {
            return ExcelineIBookingManager.GetTodayOrder(gymCode, savedMemberID, memberContractID);
        }

        public static OperationResult<List<ExceIBookingResource>> GetResources(string gymCode, int branchId)
        {
            return ExcelineIBookingManager.GetResources(gymCode, branchId);
        }

        public static OperationResult<List<ExceIBookingResourceBooking>> GetResourceBooking(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            return ExcelineIBookingManager.GetResourceBooking(gymCode, branchId, fromDate, toDate);
        }
        public static OperationResult<int> AddResourceBooking(string gymCode, ExceIBookingResourceBooking resourceBooking)
        {
            return ExcelineIBookingManager.AddResourceBooking(gymCode, resourceBooking);
        }

        public static OperationResult<int> DeleteResourceBooking(string gymCode, int bookingId, int branchId)
        {
            return ExcelineIBookingManager.DeleteResourceBooking(gymCode, bookingId, branchId);
        }

        public static OperationResult<Dictionary<int, int>> ValidateAvailableForBooking(string gymCode, List<int> resourceIdList, DateTime startDateTime, DateTime endDateTime)
        {
            return ExcelineIBookingManager.ValidateAvailableForBooking(gymCode, resourceIdList, startDateTime, endDateTime);
        }
        public static OperationResult<List<ExceIBookingResourceSchedule>> GetResourceSchedules(string gymCode, int branchId, int resourceId, DateTime fromDate, DateTime toDate)
        {
            return ExcelineIBookingManager.GetResourceSchedules(gymCode, branchId, resourceId, fromDate, toDate);
        }

        public static OperationResult<List<ExceIBookingResourceAvailableTime>> GetResourceAvailableTimes(string gymCode, int branchId, int resourceId)
        {
            return ExcelineIBookingManager.GetResourceAvailableTimes(gymCode, branchId, resourceId);
        }

        public static OperationResult<List<ExceIBookingInvoice>> GetInvoices(int branchID, string gymCode, DateTime startDate, DateTime endDate, string documentPath, string fileFolderPath)
        {
            return ExcelineIBookingManager.GetInvoices(branchID, gymCode, startDate, endDate, documentPath, fileFolderPath);
        }

        public static void AddStatus(ExceIBookingNewMember NewMember, int status, string description, string gymCode)
        {
            ExcelineIBookingManager.AddStatus(NewMember, status, description, gymCode);
        }

        public static OperationResult<List<ExceIBookingInterest>> GetIBookingInterests(string gymCode)
        {
            return ExcelineIBookingManager.GetInterests(gymCode);
        }

        public static OperationResult<ARXSetting> GetARXSettings(int systemId)
        {
            return ExcelineIBookingManager.GetARXSettings(systemId);
        }

        public static void UploadARXData(int savedMemberID, int memberContractID, string gymCode, int systemID, string fileLocation, string CustId, ARXSetting settings)
        {
            ExcelineIBookingManager.UploadARXData(savedMemberID, memberContractID, gymCode, systemID, fileLocation, CustId, settings);
        }

        public static int AddClassVisit(string gymID, string classID, List<MemberVisitStatus> visits, string gymCode)
        {
            return ExcelineIBookingManager.AddClassVisit(gymID, classID, visits, gymCode);
        }

        public static void SetPDFFileParth(int id, string fileParth, string flag, int branchId, string gymCode, string user)
        {
            ExcelineIBookingManager.SetPDFFileParth(id, fileParth, flag, branchId, gymCode, user);
        }

        public static OperationResult<int> SetResourceBookingAsPaid(ExceIBookingResourceBookingPaid resourceBookingPaid, string gymCode)
        {
            return ExcelineIBookingManager.SetResourceBookingAsPaid(resourceBookingPaid, gymCode); 
        }

        public static OperationResult<bool> UpdateBRISData(OrdinaryMemberDC member, string gymCode)
        {
            return ExcelineIBookingManager.UpdateBRISData(member, gymCode);
        }

        public static OperationResult<List<ExceIBookingMemberGymDetail>> GetIBookingMemberGyms(string gymCode )
        {
            return ExcelineIBookingManager.GetMemberGyms(gymCode);
        }

        public static OperationResult<List<ExceIBookingModifiedMember>> GetMembersUpdatedByDate(string date, string systemId, string branchId, string gymCode)
        {
            return ExcelineIBookingManager.GetMembersUpdatedByDate(date, systemId, branchId, gymCode);
        }


        public static OperationResult<List<ExceIBookingAllContracts>> GetAllContracts(string systemId, string gymCode)
        {
            return ExcelineIBookingManager.GetAllContracts(systemId, gymCode);
        }


        public static OperationResult<int> RegisterNewFreeze(string gymCode, ExceIBookingNewFreeze freeze)
        {
            return ExcelineIBookingManager.RegisterNewFreeze(gymCode, freeze);
        }

        public static OperationResult<int> RegisterResign(string gymCode, ExceIBookingNewResign resign)
        {
            return ExcelineIBookingManager.RegisterResign(gymCode, resign);
        }

        public static OperationResult<List<ExceIBookingContractFreeze>> GetContractByMemberId(string gymCode, string customerNo, int systemId)
        {
            return ExcelineIBookingManager.GetContractByMemberId(gymCode, customerNo, systemId);
        }

        public static OperationResult<List<ExceIBookingListFreeze>> GetFreezeByCustId(string gymCode, string custId, int systemId)
        {
            return ExcelineIBookingManager.GetFreezeByCustId(gymCode, custId, systemId);
        }

        public static OperationResult<List<ExceAPIMember>> GetAPIMembers(string gymCode, int branchId, int changedSinceDays, int systemId)
        {
            return ExcelineIBookingManager.GetAPIMembers(gymCode, branchId, changedSinceDays, systemId);
        }

        public static OperationResult<List<ExceIBookingFreezeResignCategories>> GetFreezeResignCategories(string gymCode)
        {
            return ExcelineIBookingManager.GetFreezeResignCategories(gymCode);
        }

        public static OperationResult<List<int?>> GetBrisIdActiveFreezed(string gymCode)
        {
            return ExcelineIBookingManager.GetBrisIdActiveFreezed(gymCode);
        }

        public static OperationResult<List<ExceIBookingMember>> NTNUIGetIBookingMembers(string gymCode, int changedSinceDays, int systemId)
        {
            return ExcelineIBookingManager.NTNUIGetMembers(gymCode, changedSinceDays, systemId);
        }

        public static OperationResult<List<ExceIBookingVisitCount>> GetVisitCount(string gymCode, int branchId, int year, int systemId)
        {
            return ExcelineIBookingManager.GetVisitCount(gymCode, branchId, year, systemId);
        }

    }
}
