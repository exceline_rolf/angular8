﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Common.Notification.RestApi.Models;
using US.GMS.API;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Payment.Core.ResultNotifications;

namespace US.Common.Notification.RestApi.Controllers
{
    [RoutePrefix("api/Notification")]
    public class NotificationController : ApiController
    {
        [HttpGet]
        [Route("GetEntityList")]
        [Authorize]
        public HttpResponseMessage GetEntityList()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = NotificationAPI.GetEntityList(ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (var message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<EntityDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<EntityDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetChangeLogNotificationList")]
        [Authorize]
        public HttpResponseMessage GetChangeLogNotificationList(int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USImportResult<List<USNotificationSummaryInfo>> result = NotificationAPI.GetChangeLogNotificationList(ExceConnectionManager.GetGymCode(user), branchId, fromDate, toDate, notifyMethod);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<USNotificationSummaryInfo>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<USNotificationSummaryInfo>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetAccessDeniedList")]
        [Authorize]
        public HttpResponseMessage GetAccessDeniedList(DateTime fromDate, DateTime toDate, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USImportResult<List<AccessDeniedLog>> result = NotificationAPI.GetAccessDeniedList(fromDate, toDate, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<AccessDeniedLog>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<AccessDeniedLog>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetNotificationSummaryList")]
        [Authorize]
        public HttpResponseMessage GetNotificationSummaryList(int branchId, NotificationStatusEnum status)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = NotificationAPI.GetNotificationSummaryList(branchId, status, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<USNotificationSummaryInfo>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<USNotificationSummaryInfo>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("GetNotificationSearchResult")]
        [Authorize]
        public HttpResponseMessage GetNotificationSearchResult(SearchNotifications searchN)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = NotificationAPI.GetNotificationSearchResult(searchN.branchId, user, searchN.title, searchN.assignTo, searchN.receiveDateFrom, searchN.receiveDateTo, searchN.dueDateFrom, searchN.dueDateTo, searchN.severityIdList, searchN.typeIdList, searchN.statusId);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<USNotificationSummaryInfo>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<USNotificationSummaryInfo>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpGet]
        [Route("GetNotificationByIdAction")]
        [Authorize]
        public HttpResponseMessage GetNotificationByIdAction(int notificationId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USImportResult<USNotificationSummaryInfo> result = NotificationAPI.GetNotificationByIdAction(notificationId, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new USNotificationSummaryInfo(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new USNotificationSummaryInfo(), ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpGet]
        [Route("GetCategoriesByType")]
        [Authorize]
        public HttpResponseMessage GetCategoryTypes(string type, int branchId,string code)
        {
            try
            {
               string user = Request.Headers.GetValues("UserName").First();
                var result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryTypeDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryTypeDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpGet]
        [Route("GetTemplateTextKeyByEntity")]
        [Authorize]
        public HttpResponseMessage GetTemplateTextKeyByEntity(int entity,string type)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result =  NotificationAPI.GetTemplateTextKeyByEntity(type, entity, ExceConnectionManager.GetGymCode(user)); 
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryTypeDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryTypeDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpGet]
        [Route("GetNotificationTemplateText")]
        [Authorize]
        public HttpResponseMessage GetNotificationTemplateText(string methodCode, string typeCode)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = NotificationAPI.GetNotificationTemplateText(methodCode, typeCode, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<NotificationTemplateText>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<NotificationTemplateText>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpPost]
        [Route("SaveNotificationTextTemplate")]
        [Authorize]
        public HttpResponseMessage SaveNotificationTextTemplate(SaveNotificationTemplateText saveNTT)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = NotificationAPI.SaveNotificationTextTemplate(saveNTT.entityId, saveNTT.templatetText, saveNTT.branchId, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }


    }
}
