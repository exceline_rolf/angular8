﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.IBooking.Core;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.API;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.IBooking.Data.SystemObjects
{
    public interface IBookingDataAdapter
    {
        List<ExceIBookingMember> GetIBookingMemberChanges(string gymCode, int branchId, int changedSinceDays, int systemId);
        List<ExceIBookingMember> GetIBookingMembers(string gymCode, int branchId, int systemId);
        List<ExceIBookingMember> GetIBookingMemberById(string gymCode, int branchId, string customerNo, int systemId);
        ExceIBookingSystem GetIBookingSystemDetails(string gymCode, int systemId);
        List<ExceIBookingGym> GetIBookingGyms(string gymCode);
        List<ExceIBookingWebOffering> GetIBookingWebOfferings(string gymCode);
        List<ExceIBookingInstructor> GetIBookingInstructors(string gymCode);
        List<ExceIBookingClassCategory> GetIBookingClassCategories(string gymCode);
        List<ExceIBookingClassKeyword> GetIBookingClassKeyword(string gymCode);
        List<CategoryDC> GetCategoriesByType(string type, string gymCode);
        List<ExceIBookingStartReason> GetIBookingStartReasons(string gymCode);
        List<ExceIBookingClassType> GetIBookingClassTypes(string gymCode);
        List<ExceIBookingClassCalendar> GetIBookingClassSchedules(string gymCode, int branchId, DateTime fromDate, DateTime toDate);
        List<ExceIBookingShowUp> GetIBookingExcelineShowUps(string gymCode, int branchId, DateTime fromDate, DateTime toDate);
        List<ExceIBookingPayment> GetIBookingPayments(string gymCode, int branchId, DateTime fromDate, DateTime toDate);
        string RegisterNewIBookingMember(string gymCode, ExceIBookingNewMember member);
        int RegisterNewFreeze(string gymCode, ExceIBookingNewFreeze freeze);
        int RegisterResign(string gymCode, ExceIBookingNewResign resign);
        List<ExceIBookingContractFreeze> GetContractByMemberId(string gymCode, string customerNo, int systemId);
        List<ExceIBookingListFreeze> GetFreezeByCustId(string gymCode, string custId, int systemId);
        List<ExceIBookingVisitCount> GetVisitCount(string gymCode, int branchId, int year, int systemId);
        List<ExceIBookingFreezeResignCategories> GetFreezeResignCategories(string gymCode);
        List<int?> GetBrisIdActiveFreezed(string gymCode);
        int UpdateIBookingClass(string gymCode, ExceIBookingUpdateClass exceClass);
        int UpdateIBookingMemberClassVisit(string gymCode, ExceIBookingMemberClassVisit visit);
        int UpdateIBookingMemberVisit(string gymCode, EntityVisitDC visit);
        int UpdateIBookingMember(string gymCode, ExceIBookingUpdateMember member);
        string GetGymCodeByCompanyId(string systemId);
        bool GetIBookingEntityValid(int entityId, string entityType, int branchId, string gymCode);
        PackageDC GetContractTemplate(string templateNo, int branchId, string gymCode, int memberID);
        List<ContractSummaryDC> GetContractSummaries(string customerNo, string gymCode);
        ExceIBookingNotificationDetails GetNotificationDetails(string gymCode, string templateNo, int branchID, string type);
        List<InstallmentDC> GetTodayOrder(string gymCode, int savedMemberID, int memberContractID);
        ContractResignDetails CancelIBookingContract(int systemId, int gymId, int customerId, string gymCode);
        int DeleteIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string gymCode);
        int AddIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string gymCode);
        List<ExceIBookingResource> GetResources(string gymCode, int branchId);

        List<ExceIBookingResourceBooking> GetResourceBooking(string gymCode, int branchId, DateTime fromDate,
                                                                   DateTime toDate);

        int AddResourceBooking(string gymCode, ExceIBookingResourceBooking resourceBooking);

        int DeleteResourceBooking(string gymCode, int bookingId, int branchId);

        Dictionary<int, int> ValidateAvailableForBooking(string gymCode, List<int> resourceIdList, DateTime startDateTime, DateTime endDateTime);

        List<ExceIBookingResourceSchedule> GetResourceSchedules(string gymCode, int branchId, int resourceId, DateTime fromDate, DateTime toDate);

        List<ExceIBookingResourceAvailableTime> GetResourceAvailableTimes(string gymCode, int branchId, int resourceId);
        List<ExceIBookingInvoice> GetInvoices(int branchID, string gymCode, DateTime startDate, DateTime endDate, string documentService, string folderPath);
        List<ExceIBookingInterest> GetIBookingInterests(string gymCode);
        void AddStatus(ExceIBookingNewMember NewMember, int status, string description, string gymCode);
        ARXSetting GetARXSettings(int systemId);
        person GetARXData(int savedMemberID, int memberContractID, string gymCode);
        int AddClassVisit(string gymID, string classID, List<MemberVisitStatus> visits, string gymCode);
        void SetPDFFileParth(int id, string fileParth, string flag, int branchId, string gymCode, string user);
        BookingDetails GetBookingDetails(int bookingID, string gymCode);
        int AddShopOrder(InstallmentDC installment, int branchID, int memberBranchID, string gymCode);
        OperationResult<SaleResultDC> PayIBookingInvoice(int branchID, string user, InstallmentDC installment, PaymentDetailDC paymentDetails, string gymCode);
        bool UpdateBookingPaymentDetails(int bookingID, int memberID, int arItemNO, decimal amount, string refID, string gymCode);
        bool CancelPaymentOrder(int bookingID, int orderID, string notificationMessage, string gymCode);
        bool UpdateBRISData(OrdinaryMemberDC member, string gymCode);
        List<ExceIBookingMemberGymDetail> GetIBookingMemberGyms(string gymCode );
        List<ExceIBookingModifiedMember> GetMembersUpdated(DateTime date, int systemId, int branchId, string gymCode);
        List<ExceIBookingAllContracts> GetAllContracts(int systemId, string gymCode);
        List<ExceAPIMember> GetAPIMembers(string gymCode, int branchId, int changedSinceDays, int systemId);
        List<ExceIBookingMember> NTNUIGetIBookingMemberChanges(string gymCode, int changedSinceDays, int systemId);
    }
}
