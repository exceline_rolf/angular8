﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.IBooking.Data.SystemObjects;
using US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Exceline.GMS.IBooking.Core;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.API;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer
{
    public class SQLServerIBookingDataAdapter : IBookingDataAdapter
    {
        public List<ExceIBookingMember> GetIBookingMemberChanges(string gymCode, int branchId, int changedSinceDays, int systemId)
        {
            GetIBookingMemberChangesAction action = new GetIBookingMemberChangesAction(branchId, changedSinceDays, systemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingMember> GetIBookingMembers(string gymCode, int branchId, int systemId)
        {
            GetIBookingMembersAction action = new GetIBookingMembersAction(branchId, systemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ContractResignDetails CancelIBookingContract(int systemId, int gymId, int customerId, string gymCode)
        {
            CancelIBookingContractAction action = new CancelIBookingContractAction( systemId,  gymId,  customerId );
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int DeleteIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string gymCode)
        {
            DeleteIBookingClassBookingAction action = new DeleteIBookingClassBookingAction( systemId,  gymId,  classId,  customerId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
          
        }

        public int AddIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string gymCode)
        {
            AddIBookingClassBookingAction action = new AddIBookingClassBookingAction(systemId, gymId, classId, customerId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
            
        }

        public List<ExceIBookingMember> GetIBookingMemberById(string gymCode, int branchId, string customerNo, int systemId)
        {
            GetIBookingMemberByIdAction action = new GetIBookingMemberByIdAction(branchId, customerNo, systemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ExceIBookingSystem GetIBookingSystemDetails(string gymCode, int systemId)
        {
            GetIBookingSystemDetailsAction action = new GetIBookingSystemDetailsAction(systemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingGym> GetIBookingGyms(string gymCode)
        {
            GetIBookingGymsAction action = new GetIBookingGymsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingWebOffering> GetIBookingWebOfferings(string gymCode)
        {
            GetIBookingWebOfferingsAction action = new GetIBookingWebOfferingsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingInstructor> GetIBookingInstructors(string gymCode)
        {
            GetIBookingInstructorsAction action = new GetIBookingInstructorsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingClassCategory> GetIBookingClassCategories(string gymCode)
        {
            GetIBookingClassCategoriesAction action = new GetIBookingClassCategoriesAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingClassKeyword> GetIBookingClassKeyword(string gymCode)
        {
            GetIBookingClassKeywordsAction action = new GetIBookingClassKeywordsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<CategoryDC> GetCategoriesByType(string type, string gymCode)
        {
            GetIBookingCategoriesByTypeAction action = new GetIBookingCategoriesByTypeAction(type);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingStartReason> GetIBookingStartReasons(string gymCode)
        {
            GetIBookingStartReasonsAction action = new GetIBookingStartReasonsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingClassType> GetIBookingClassTypes(string gymCode)
        {
            GetIBookingClassTypesAction action = new GetIBookingClassTypesAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingClassCalendar> GetIBookingClassSchedules(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            GetIBookingClassSchedulesAction action = new GetIBookingClassSchedulesAction(branchId, fromDate, toDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingShowUp> GetIBookingExcelineShowUps(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            GetIBookingExcelineShowUpsAction action = new GetIBookingExcelineShowUpsAction(branchId, fromDate, toDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingPayment> GetIBookingPayments(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            GetIBookingPaymentsAction action = new GetIBookingPaymentsAction(branchId, fromDate, toDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public string RegisterNewIBookingMember(string gymCode, ExceIBookingNewMember member)
        {
            RegisterNewIBookingMemberAction action = new RegisterNewIBookingMemberAction(member);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int UpdateIBookingClass(string gymCode, ExceIBookingUpdateClass exceClass)
        {
            UpdateIBookingClassAction action = new UpdateIBookingClassAction(exceClass);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int UpdateIBookingMemberClassVisit(string gymCode, ExceIBookingMemberClassVisit visit)
        {
            UpdateIBookingMemberClassVisitAction action = new UpdateIBookingMemberClassVisitAction(visit);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int UpdateIBookingMemberVisit(string gymCode, EntityVisitDC visit)
        {
            UpdateIBookingMemberVisitAction action = new UpdateIBookingMemberVisitAction(visit);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int UpdateIBookingMember(string gymCode, ExceIBookingUpdateMember member)
        {
            UpdateIBookingMemberAction action = new UpdateIBookingMemberAction(member);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public string GetGymCodeByCompanyId(string systemId)
        {
            GetIBookingGymCodeByCompanyIdAction action = new GetIBookingGymCodeByCompanyIdAction(systemId);
            return action.Execute(EnumDatabase.WorkStation);
        }

        public bool GetIBookingEntityValid(int entityId, string entityType, int branchId, string gymCode)
        {
            GetIBookingEntityValidAction action = new GetIBookingEntityValidAction(entityId, entityType, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public PackageDC GetContractTemplate(string templateNo, int branchId, string gymCode, int memberID)
        {
            GetIBookingContractTemplateAction action = new GetIBookingContractTemplateAction(templateNo, branchId, gymCode, memberID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<US.GMS.Core.SystemObjects.ContractSummaryDC> GetContractSummaries(string customerNo, string gymCode)
        {
            GetContractSummariesForIBookingAction action = new GetContractSummariesForIBookingAction(customerNo, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ExceIBookingNotificationDetails GetNotificationDetails(string gymCode, string templateNo, int branchID, string type)
        {
            GetIBookingNotificationDetailsAction action = new GetIBookingNotificationDetailsAction(templateNo, branchID, type);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<InstallmentDC> GetTodayOrder(string gymCode, int savedMemberID, int memberContractID)
        {
            GetTodayOrderAction action = new GetTodayOrderAction(savedMemberID, memberContractID, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        public List<ExceIBookingResource> GetResources(string gymCode, int branchId)
        {
            GetResourcesAction action = new GetResourcesAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingResourceBooking> GetResourceBooking(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            var action = new GetIBookingResourceBookingAction(branchId, fromDate, toDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddResourceBooking(string gymCode, ExceIBookingResourceBooking resourceBooking)
        {
            var action = new AddIBookingResourceBookingAction(resourceBooking);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int DeleteResourceBooking(string gymCode, int bookingId, int branchId)
        {
            var action = new DeleteIBookingResourceBookingAction(bookingId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public Dictionary<int, int> ValidateAvailableForBooking(string gymCode, List<int> resourceIdList, DateTime startDateTime, DateTime endDateTime)
        {
            var action = new ValidateIBookingAvailableForBookingAction(resourceIdList, startDateTime, endDateTime);
            return action.Execute(EnumDatabase.Exceline, gymCode);

        }

        public List<ExceIBookingResourceSchedule> GetResourceSchedules(string gymCode, int branchId, int resourceId, DateTime fromDate, DateTime toDate)
        {
            var action = new GetIBookingResourceSchedulesAction(branchId, resourceId, fromDate, toDate, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);

        }
        public List<ExceIBookingResourceAvailableTime> GetResourceAvailableTimes(string gymCode, int branchId, int resourceId)
        {
            var action = new GetIBookingResourceAvailableTimeAction(branchId, resourceId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingInvoice> GetInvoices(int branchID, string gymCode, DateTime startDate, DateTime endDate, string documentService, string folderpath)
        {
            GetIBookingInvoicesAction action = new GetIBookingInvoicesAction(branchID, startDate, endDate, documentService, folderpath);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingInterest> GetIBookingInterests(string gymCode)
        {
            GetIBookingInterestsAction action = new GetIBookingInterestsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public void AddStatus(ExceIBookingNewMember NewMember, int status, string description, string gymCode)
        {
            AddIBookingStatusAction action = new AddIBookingStatusAction(NewMember, status, description);
            action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ARXSetting GetARXSettings(int systemId)
        {
            GetARXSettingsAction action = new GetARXSettingsAction(systemId);
            return action.Execute(EnumDatabase.WorkStation);
        }


        public person GetARXData(int savedMemberID, int memberContractID, string gymCode)
        {
            GetARXDetailsByMemberIDAction action = new GetARXDetailsByMemberIDAction(savedMemberID, memberContractID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public int AddClassVisit(string gymID, string classID, List<MemberVisitStatus> visits, string gymCode)
        {
            AddClassVisitAction action = new AddClassVisitAction(gymID, classID, visits);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public void SetPDFFileParth(int id, string fileParth, string flag, int branchId, string gymCode, string user)
        {
            
            SetPDFFileParthAction action = new SetPDFFileParthAction(id, fileParth, flag, branchId, user);
            action.Execute(EnumDatabase.Exceline, gymCode);
           
        }

        public BookingDetails GetBookingDetails(int bookingID, string gymCode)
        {
            GetBookingDetailsAction action = new GetBookingDetailsAction(bookingID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddShopOrder(InstallmentDC installment, int branchID, int memberBranchID, string gymCode)
        {
            AddIBookingShopOrderAction action = new AddIBookingShopOrderAction(installment, branchID, memberBranchID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public OperationResult<SaleResultDC> PayIBookingInvoice(int branchID, string user, InstallmentDC installment, PaymentDetailDC paymentDetails, string gymCode)
        {
            AddIBookingInvoicePaymentAction action = new AddIBookingInvoicePaymentAction(branchID, user, installment, paymentDetails, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateBookingPaymentDetails(int bookingID, int memberID, int arItemNO, decimal amount, string refID, string gymCode)
        {
            UpdateIBookingPaymentsDetailsAction action = new UpdateIBookingPaymentsDetailsAction(bookingID, memberID, arItemNO, amount, refID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public bool CancelPaymentOrder(int bookingID, int orderID, string notificationMessage, string gymCode)
        {
            CancelBookingPaymentOrderAction action = new CancelBookingPaymentOrderAction(bookingID, orderID, notificationMessage);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateBRISData(OrdinaryMemberDC member, string gymCode)
        {
            UpdateBRISDataAction action = new UpdateBRISDataAction(member);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingMemberGymDetail> GetIBookingMemberGyms(string gymCode)
        {
            GetIbookingMemberGymsAction action = new GetIbookingMemberGymsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingModifiedMember> GetMembersUpdated(DateTime date, int systemId, int branchId, string gymCode)
        {
            GetMembersUpdatedAction action = new GetMembersUpdatedAction(date, systemId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public List<ExceIBookingAllContracts> GetAllContracts(int systemId, string gymCode)
        {
            GetAllContractsAction action = new GetAllContractsAction(systemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int RegisterNewFreeze(string gymCode, ExceIBookingNewFreeze freeze)
        {
            RegisterNewFreezeAction action = new RegisterNewFreezeAction(freeze);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int RegisterResign(string gymCode, ExceIBookingNewResign resign)
        {
            RegisterResignAction action = new RegisterResignAction(resign);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingContractFreeze> GetContractByMemberId(string gymCode, string customerNo, int systemId)
        {
            GetContractByMemberIdAction action = new GetContractByMemberIdAction(customerNo, systemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingListFreeze> GetFreezeByCustId(string gymCode, string custId, int systemId)
        {
            GetFreezeByCustIdAction action = new GetFreezeByCustIdAction(custId, systemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public List<ExceAPIMember> GetAPIMembers(string gymCode, int branchId, int changedSinceDays, int systemId)
        {
            GetAPIMembersAction action = new GetAPIMembersAction(branchId, changedSinceDays, systemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingFreezeResignCategories> GetFreezeResignCategories(string gymCode)
        {
            GetFreezeResignCategoriesAction action = new GetFreezeResignCategoriesAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<int?> GetBrisIdActiveFreezed(string gymCode)
        {
            GetBrisIdActiveFreezedAction action = new GetBrisIdActiveFreezedAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceIBookingMember> NTNUIGetIBookingMemberChanges(string gymCode, int changedSinceDays, int systemId)
        {
            NTNUIGetIBookingMemberChangesAction action = new NTNUIGetIBookingMemberChangesAction(changedSinceDays, systemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public List<ExceIBookingVisitCount> GetVisitCount(string gymCode, int branchId, int year, int systemId)
        {
            GetVisitCountAction action = new GetVisitCountAction(branchId, year, systemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

    }
}
