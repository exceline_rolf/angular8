﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    class GetARXDetailsByMemberIDAction : USDBActionBase<person>
    {
        private int _memberID = -1;
        private int _memberContractID = -1;
        public GetARXDetailsByMemberIDAction(int memberID, int memberContractID)
        {
            _memberID = memberID;
            _memberContractID = memberContractID;
        }

        protected override person Body(System.Data.Common.DbConnection connection)
        {
            string spName = "GetARXDetailsByMemberID";
            person per = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", System.Data.DbType.Int32, _memberID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractID", System.Data.DbType.Int32, _memberContractID));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    per = new person();
                    per.id = Convert.ToString(reader["CustId"]);
                    per.first_name = Convert.ToString(reader["FirstName"]);
                    per.last_name = Convert.ToString(reader["LastName"]);
                    per.AccessProfileID = Convert.ToInt32(reader["AccessProfileId"]);
                    per.CardNo = Convert.ToString(reader["CardNo"]);
                    per.BranchName = Convert.ToString(reader["BranchName"]);
                    per.FormatName = Convert.ToString(reader["FormatName"]);
                    per.BranchID = Convert.ToInt32(reader["BranchID"]);
                    per.EndDate = Convert.ToDateTime(reader["EndDate"]).ToString("yyyy-MM-dd");
                    per.AccessProfileName = Convert.ToString(reader["AccessProfileName"]);
                }
            }
            catch (Exception)
            {
                return null;
            }
            return per;
        }
    }
}
