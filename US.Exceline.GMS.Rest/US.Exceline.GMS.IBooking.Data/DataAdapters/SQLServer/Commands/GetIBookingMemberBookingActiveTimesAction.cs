﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingMemberBookingActiveTimesAction : USDBActionBase<List<ExceIBookingClassTimeForMember>>
    {
        private string _customerId;
        private int _classId;
        private int _branchId;


        public GetIBookingMemberBookingActiveTimesAction(string customerId, int classId, int branchId)
        {
            _customerId = customerId;
            _classId = classId;
            _branchId = branchId;
        }

        protected override List<ExceIBookingClassTimeForMember> Body(DbConnection connection)
        {
            GetIBookingClassMemberBookingsAction getMemberBookingsAction = new GetIBookingClassMemberBookingsAction(_classId, _branchId);
            List<ExceIBookingClassMemberBooking> classBookingDetails = getMemberBookingsAction.Execute(EnumDatabase.USP);

            int notBookedMembersForClassCount = classBookingDetails.Where(x => x.TimeId == 0).Count();


            List<ExceIBookingClassTime> excelineClassActiveTimeList = new List<ExceIBookingClassTime>();
            string storedProcedureName = "USExceGMSIBookingGetClassActiveTimes";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceIBookingClassTime excelineClassActiveTime = new ExceIBookingClassTime();
                    excelineClassActiveTime.ClassId = _classId;
                    excelineClassActiveTime.TimeId = Convert.ToInt32(reader["ActiveTimeId"]);
                    if (reader["StartDateTime"] != DBNull.Value)
                    {
                        excelineClassActiveTime.ClassDate = Convert.ToDateTime(reader["StartDateTime"]).Date;
                        excelineClassActiveTime.ClassStartTime = Convert.ToDateTime(reader["StartDateTime"]);
                    }
                    if (reader["EndDateTime"] != DBNull.Value)
                    {
                        excelineClassActiveTime.ClassEndTime = Convert.ToDateTime(reader["EndDateTime"]);
                    }
                    int maxNumOfSlots = Convert.ToInt32(reader["ClassMaxParticipants"]);
                    excelineClassActiveTime.TotalAvailableSlots = (maxNumOfSlots) - (notBookedMembersForClassCount + classBookingDetails.Where(x => x.TimeId == excelineClassActiveTime.TimeId).Sum(n => n.Participants));
                    excelineClassActiveTimeList.Add(excelineClassActiveTime);

                }

                var totalBookedCount = 0;
                var paidActiveTimes = classBookingDetails.Where(x => x.Paid && x.CustomerId == _customerId).Select(x => x.TimeId).ToList();

                var list = (from c in excelineClassActiveTimeList
                            where !paidActiveTimes.Contains(c.TimeId)
                            select new ExceIBookingClassTimeForMember
                            {
                                CustomerId = _customerId,
                                ClassId = _classId,
                                TimeId = c.TimeId,
                                ClassDate = c.ClassDate,
                                ClassStartTime = c.ClassStartTime,
                                ClassEndTime = c.ClassEndTime,
                                BookedSlots = totalBookedCount = (classBookingDetails.Where(x => x.CustomerId == _customerId && x.TimeId == c.TimeId).Count() > 0) ? classBookingDetails.Where(x => x.CustomerId == _customerId && x.TimeId == c.TimeId).Sum(n => n.Participants) : 0,
                                AvailableSlots = (totalBookedCount == 0) ? 5 : (5 - totalBookedCount)
                                //AvailableMembersCount = totalAvailCount > 5 ? 5 : ((totalMemberAvlCount = (totalAvailCount + ViewModel.MemberBookingDetailsList.Where(x => x.MemberId == _memberId && x.ActiveTimeId == c.ActiveTimeId).Sum(n => n.NumberOfParticipants))) > 5 ? 5 : totalMemberAvlCount),
                                //ParticipatingMembersCount = (_memberId > 0 && ViewModel.MemberBookingDetailsList.Where(x => x.MemberId.Equals(_memberId) && x.ActiveTimeId == c.ActiveTimeId).Count() > 0) ? ViewModel.MemberBookingDetailsList.Where(x => x.MemberId.Equals(_memberId) && x.ActiveTimeId == c.ActiveTimeId).FirstOrDefault().NumberOfParticipants : 0,
                                //Enabled = ((totalAvailCount == 0 && totalMemberAvlCount == 0) || ViewModel.MemberBookingDetailsList.Where(x => x.ActiveTimeId.Equals(c.ActiveTimeId) && (x.MemberId.Equals(_memberId)) && x.Paid).Count() > 0) ? false : true,
                                //Checked = (_memberId > 0 && ViewModel.MemberBookingDetailsList.Where(x => x.MemberId.Equals(_memberId) && x.ActiveTimeId == c.ActiveTimeId).Count() > 0) ? true : false
                            }).ToList();

                if (list.Count > 0)
                {
                    list = list.Where(x => x.ClassDate >= DateTime.Now.Date).ToList();
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
