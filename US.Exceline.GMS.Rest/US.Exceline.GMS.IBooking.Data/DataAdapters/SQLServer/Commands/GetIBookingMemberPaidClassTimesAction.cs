﻿using System;
using System.Collections.Generic;
using System.Linq;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingMemberPaidClassTimesAction : USDBActionBase<List<ExceIBookingClassTimeForMember>>
    {
        private string _customerId;
        private int _classId;
        private int _branchId;

        public GetIBookingMemberPaidClassTimesAction(string customerId, int classId, int branchId)
        {
            _customerId = customerId;
            _classId = classId;
            _branchId = branchId;
        }

        protected override List<ExceIBookingClassTimeForMember> Body(DbConnection connection)
        {
            GetIBookingClassMemberBookingsAction getMemberBookingsAction = new GetIBookingClassMemberBookingsAction(_classId, _branchId);
            List<ExceIBookingClassMemberBooking> classBookingDetails = getMemberBookingsAction.Execute(EnumDatabase.USP);

            int notBookedMembersForClassCount = classBookingDetails.Where(x => x.TimeId == 0).Count();


            List<ExceIBookingClassTime> excelineClassActiveTimeList = new List<ExceIBookingClassTime>();
            string storedProcedureName = "USExceGMSIBookingGetClassActiveTimes";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceIBookingClassTime excelineClassActiveTime = new ExceIBookingClassTime();
                    excelineClassActiveTime.ClassId = _classId;
                    excelineClassActiveTime.TimeId = Convert.ToInt32(reader["ActiveTimeId"]);
                    if (reader["StartDateTime"] != DBNull.Value)
                    {
                        excelineClassActiveTime.ClassDate = Convert.ToDateTime(reader["StartDateTime"]).Date;
                        excelineClassActiveTime.ClassStartTime = Convert.ToDateTime(reader["StartDateTime"]);
                    }
                    if (reader["EndDateTime"] != DBNull.Value)
                    {
                        excelineClassActiveTime.ClassEndTime = Convert.ToDateTime(reader["EndDateTime"]);
                    }
                    int maxNumOfSlots = Convert.ToInt32(reader["ClassMaxParticipants"]);
                    excelineClassActiveTime.TotalAvailableSlots = (maxNumOfSlots) - (notBookedMembersForClassCount + classBookingDetails.Where(x => x.TimeId == excelineClassActiveTime.TimeId).Sum(n => n.Participants));
                    excelineClassActiveTimeList.Add(excelineClassActiveTime);

                }

                var totalBookedCount = 0;
                var paidActiveTimes = classBookingDetails.Where(x => x.Paid && x.CustomerId == _customerId).Select(x => x.TimeId).ToList();

                var list = (from c in excelineClassActiveTimeList
                            where paidActiveTimes.Contains(c.TimeId)
                            select new ExceIBookingClassTimeForMember
                            {
                                CustomerId = _customerId,
                                ClassId = _classId,
                                TimeId = c.TimeId,
                                ClassDate = c.ClassDate,
                                ClassStartTime = c.ClassStartTime,
                                ClassEndTime = c.ClassEndTime,
                                BookedSlots = totalBookedCount = (classBookingDetails.Where(x => x.CustomerId == _customerId && x.TimeId == c.TimeId).Count() > 0) ? classBookingDetails.Where(x => x.CustomerId == _customerId && x.TimeId == c.TimeId).Sum(n => n.Participants) : 0,
                                AvailableSlots = 0
                            }).ToList();
                return list;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
