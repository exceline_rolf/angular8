﻿using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingMemberDetailsAction : USDBActionBase<ExceIBookingMember>
    {
        private string _customerId;
        private int _branchId;

        public GetIBookingMemberDetailsAction(string customerId, int branchId)
        {
            _customerId = customerId;
            _branchId = branchId;
        }

        protected override ExceIBookingMember Body(DbConnection connection)
        {
            ExceIBookingMember member = new ExceIBookingMember();
            string StoredProcedureName = "USExceGMSIBookingGetMemberDetails";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberCustId", DbType.String, _customerId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                //    member.CustomerId = Convert.ToInt32(_customerId);
                    member.FirstName = reader["FirstName"].ToString();
                    member.LastName = reader["LastName"].ToString();
                    //member.BirthDate =  reader["BirthDate"]!=DBNull.Value?Convert.ToDateTime(reader["BirthDate"]):(Nullable<DateTime>)null;
                    //member.Address1 = reader["Address1"].ToString();
                    //member.Address2 = reader["Address2"].ToString();
                    //member.Address3 = reader["Address3"].ToString();
                    //member.PostCode = reader["ZipCodde"].ToString();
                    //member.PostPlace = reader["ZipName"].ToString();
                    //member.Mobile = Convert.ToString(reader["Mobile"]).Trim();
                    //member.WorkTeleNo = Convert.ToString(reader["TeleWork"]).Trim();
                    //member.PrivateTeleNo = Convert.ToString(reader["TeleHome"]).Trim();
                    //member.Email = reader["Email"].ToString();
                    //member.Gender = reader["Gender"].ToString();
                    //member.Role = reader["Role"].ToString();
                    //member.Active=(Convert.ToBoolean(reader["ActiveStatus"])?"YES":"NO");
                    //member.CardNumber = Convert.ToString(reader["CardNumber"]).Trim();
                    //member.RegisterdDate = reader["RegisterdDatetime"] != DBNull.Value ? Convert.ToDateTime(reader["RegisterdDatetime"]) : (Nullable<DateTime>)null;
                    //member.LastVisitDate = reader["LastVisitDateTime"] != DBNull.Value ? Convert.ToDateTime(reader["LastVisitDateTime"]) : (Nullable<DateTime>)null;
                    //member.UpdatedDateTime = reader["LastUpdatedDateTime"] != DBNull.Value ? Convert.ToDateTime(reader["LastUpdatedDateTime"]) : (Nullable<DateTime>)null;
                }
                return member;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
