﻿using System;
using System.Data.SqlClient;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data;
using System.Globalization;
using System.Collections.Generic;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterNewIBookingMemberAction : USDBActionBase<string>
    {
        private readonly ExceIBookingNewMember _member;
        private DataTable _dtInterestIds;

        public RegisterNewIBookingMemberAction(ExceIBookingNewMember member)
        {
            _member = member;
            _dtInterestIds = GetInterestsDataTable(_member.Interests);
        }

        private DataTable GetInterestsDataTable(List<int> interestList)
        {
            _dtInterestIds = new DataTable();
            _dtInterestIds.Columns.Add(new DataColumn("ID", typeof(int)));
            if (interestList != null)
            {
                foreach (int id in interestList)
                {
                    DataRow dataTableRow = _dtInterestIds.NewRow();
                    dataTableRow["ID"] = id;
                    _dtInterestIds.Rows.Add(dataTableRow);
                }
            }
            return _dtInterestIds;
        }

        protected override string Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingSaveMember";
            string savedMemberID = "-1";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@systemId", DbType.Int32, 39));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _member.GymId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@exportDateTime", DbType.DateTime, DateTime.ParseExact(_member.ExportDateTime,"yyyy.MM.dd",CultureInfo.InvariantCulture)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@registeredDateTime", DbType.DateTime,DateTime.ParseExact(_member.RegisteredDateTime,"yyyy.MM.dd",CultureInfo.InvariantCulture)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstName", DbType.String, _member.FirstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastName", DbType.String, _member.LastName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address1", DbType.String, _member.Address));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address2", DbType.String, _member.Address2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address3", DbType.String, _member.Address3));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postCode", DbType.String, _member.PostCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postPlace", DbType.String, _member.PostPlace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobile", DbType.String, _member.Mobile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@workTeleNo", DbType.String, _member.WorkTeleNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@privateTeleNo", DbType.String, _member.PrivateTeleNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, _member.Email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gender", DbType.String, _member.Gender));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@birthDate", DbType.Date,DateTime.ParseExact(_member.BirthDate,"yyyy.MM.dd",CultureInfo.InvariantCulture))); 
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@accountNumber", DbType.String, _member.AccountNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@offeringId", DbType.Int32, _member.OfferingId));
                if( _member.BelongsTo > 0)
                   cmd.Parameters.Add(DataAcessUtils.CreateParam("@belongsTo", DbType.Int32, _member.BelongsTo));
                if(!string.IsNullOrEmpty(_member.CardNumber))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@cardNumber", DbType.String, _member.CardNumber));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@paidAmount", DbType.Decimal, Convert.ToDecimal(_member.PaidAmount, new CultureInfo("en-US"))));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InterestIdList", SqlDbType.Structured, _dtInterestIds));

                if(!string.IsNullOrEmpty(_member.CountryCode))
                   cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountryCode", DbType.String, _member.CountryCode));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GuardianFirstName", DbType.String, _member.GuardianFirstName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GuardianLastname", DbType.String, _member.GuardianLastName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GuardianAddress", DbType.String, _member.GuardianAddress));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GuardianPostalCode", DbType.String, _member.GuardianPostalCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GuardianPostalPlace", DbType.String, _member.GuardianPostalPlace));
                if (!string.IsNullOrEmpty(_member.GuardianBirthdate))
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GuardianBirthdate", DbType.Date, DateTime.ParseExact(_member.GuardianBirthdate, "yyyy.MM.dd", CultureInfo.InvariantCulture)));
                
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GuardianGender", DbType.String, _member.GuardianGender));

                if (!string.IsNullOrEmpty(_member.GuardianMobile))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@GuardianMobile", DbType.String, _member.GuardianMobile));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GuardianEmail", DbType.String, _member.GuardianEmail));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PinCode", DbType.String, _member.PinCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CustomerId", DbType.Int32, _member.CustomerNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BrisId", DbType.Int32, _member.BrisID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AgressoId", DbType.Int32, _member.AgressoID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SendEmail", DbType.Boolean, _member.SendEmail));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SendSMS", DbType.Boolean, _member.SendSMS));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CustomerClub", DbType.Boolean, _member.CustomerClub));
                savedMemberID = Convert.ToString(cmd.ExecuteScalar());
                return savedMemberID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
