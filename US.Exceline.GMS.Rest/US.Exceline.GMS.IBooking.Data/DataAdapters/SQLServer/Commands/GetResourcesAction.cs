﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetResourcesAction : USDBActionBase<List<ExceIBookingResource>>
    {
        private readonly int _branchId = -1;
        public GetResourcesAction(int branchId)
        {
            _branchId = branchId;
        }
        protected override List<ExceIBookingResource> Body(DbConnection connection)
        {
            var exceIBookingResourceList = new List<ExceIBookingResource>();
            const string storedProcedure = "USExceGMSIBookingGetResources";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var exceIBooking = new ExceIBookingResource
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Name = reader["Name"].ToString(),
                            CategoryId = Convert.ToInt32(reader["CategoryId"]),
                            ArticlId = Convert.ToInt32(reader["ArticleId"]),
                            ArticlePricePerTimeUnit = Convert.ToDecimal(reader["ArticlePrice"]),
                            ArticleTimeUnit = Convert.ToInt32(reader["ArticleTimeUnit"])
                        };

                    exceIBookingResourceList.Add(exceIBooking);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return exceIBookingResourceList;
        }
    }
}
