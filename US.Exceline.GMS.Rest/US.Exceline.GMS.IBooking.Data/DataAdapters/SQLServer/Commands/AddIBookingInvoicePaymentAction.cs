﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.API;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class AddIBookingInvoicePaymentAction : USDBActionBase<OperationResult<SaleResultDC>>
    {
         private int _memberbranchId = -1;
        private int _loggedBranchID = -1;
        private string _user = string.Empty;
        private InstallmentDC _installment = null;
        private PaymentDetailDC _payementDetail = null;
        private string _gymCode = string.Empty;

        public AddIBookingInvoicePaymentAction(int memberBranchID, string user, InstallmentDC installment, PaymentDetailDC paymentDetail, string gymCode)
        {
            _memberbranchId = memberBranchID;
            _loggedBranchID = memberBranchID;
            _user = user;
            _installment = installment;
            _payementDetail = paymentDetail;
            _gymCode = gymCode;
        }

        protected override OperationResult<SaleResultDC> Body(System.Data.Common.DbConnection connection)
        {
            DbTransaction transaction = null;
            OperationResult<SaleResultDC> result = new OperationResult<SaleResultDC>();
            int generatedArItemNo = -1;
            string spName = "ExceGMSGenerateInvoiceForOrder";
            SaleResultDC saleResult = new SaleResultDC();
            try
            {
                transaction = connection.BeginTransaction();
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Transaction = transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentID", System.Data.DbType.Int32, _installment.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsInvoiceFeeAdded", System.Data.DbType.Boolean, false));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceType", System.Data.DbType.String, "PRINT"));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsShopInvoice", System.Data.DbType.Boolean, false));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SMSInvoice", System.Data.DbType.Boolean, false));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberBranchID", System.Data.DbType.Int32, _memberbranchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoicBranchID", System.Data.DbType.Int32, _loggedBranchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SettleWithPrepaid", System.Data.DbType.Boolean, false));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SponsoredAmount", System.Data.DbType.Decimal, _installment.SponsoredAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Discount", System.Data.DbType.Decimal, _installment.Discount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsSponsored", System.Data.DbType.Boolean, _installment.IsSponsored));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", System.Data.DbType.Int32, _installment.MemberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@NofoVisists", System.Data.DbType.Int32, _installment.NoOfVisits));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentText", System.Data.DbType.String, _installment.Text));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractID", System.Data.DbType.Int32, _installment.MemberContractId));

                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    saleResult = new SaleResultDC();
                    saleResult.InvoiceNo = Convert.ToString(reader["Ref"]);
                    saleResult.AritemNo = Convert.ToInt32(reader["Aritemno"]);
                    saleResult.InkassoID = Convert.ToInt32(reader["CreditorNo"]);
                    saleResult.CustId = Convert.ToString(reader["CustID"]);
                    saleResult.KID = Convert.ToString(reader["KID"]);
                }

                reader.Close();
                reader.Dispose();

                if (saleResult.AritemNo > 0)
                {
                    generatedArItemNo = saleResult.AritemNo;
                    saleResult.AritemNo = generatedArItemNo;
                    saleResult.InvoiceNo = saleResult.InvoiceNo;

                    if (generatedArItemNo > 0)
                    {
                        if (_payementDetail.PaidAmount > 0)
                        {
                            PaymentProcessResult paymentResult = null;
                            List<IUSPTransaction> transactions = new List<IUSPTransaction>();
                            foreach (PayModeDC payMode in _payementDetail.PayModes)
                            {
                                transactions.Add(GetTransaction(saleResult, payMode.Amount, payMode.PaymentTypeCode, payMode.PaidDate));
                            }
                            
                            paymentResult = Transaction.RegsiterTransaction(transactions, transaction);
                            if (!paymentResult.ResultStatus)
                            {
                                transaction.Rollback();
                                saleResult.SaleStatus = SaleErrors.PAYMENTADDINGERROR;
                                result.CreateMessage("Error In Adding Payment", MessageTypes.ERROR);
                                result.OperationReturnValue = saleResult;
                                return result;
                            }
                            else
                            {
                                transaction.Commit();
                                saleResult.SaleStatus = SaleErrors.SUCCESS;
                                result.OperationReturnValue = saleResult;
                                return result;
                            }
                        }
                        else
                        {
                            transaction.Rollback();
                            saleResult.SaleStatus = SaleErrors.PAYMENTNOTFOUND;
                            result.CreateMessage("Payment is not found", MessageTypes.ERROR);
                            result.OperationReturnValue = saleResult;
                            return result;
                        }
                    }
                    else
                    {
                        transaction.Rollback();
                        saleResult.AritemNo = -1;
                        saleResult.InvoiceNo = string.Empty;
                        saleResult.SaleStatus = SaleErrors.INVOICEADDINGERROR;
                        result.CreateMessage("Invoice Not added OrderID" + _installment.Id.ToString(), MessageTypes.ERROR);
                        result.OperationReturnValue = saleResult;
                        return result;
                    }
                }
                else
                {
                    transaction.Rollback();
                    saleResult.SaleStatus = SaleErrors.INVOICEADDINGERROR;
                    result.CreateMessage("Invoice Not added OrderID" + _installment.Id.ToString(), MessageTypes.ERROR);
                    result.OperationReturnValue = saleResult;
                    return result;
                }
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();
                saleResult.SaleStatus = SaleErrors.ERROR;
                result.OperationReturnValue = saleResult;
                result.CreateMessage(ex.Message, MessageTypes.ERROR);
                return result;
            }
        }

        private IUSPTransaction GetTransaction(SaleResultDC result, decimal paymentAmount, string paymentSourceName, DateTime paidDate)
        {
            IUSPTransaction transaction = new USPTransaction();
            transaction.TransType = GetTransType(paymentSourceName);
            transaction.PID = result.InkassoID.ToString();
            transaction.CID = result.CustId;
            if ((transaction.TransType == "OP" || transaction.TransType == "LOP" || transaction.TransType == "BS" || transaction.TransType == "DC") && paidDate != null && paidDate != DateTime.MinValue)
            {
                transaction.VoucherDate = paidDate;
            }
            else
            {
                transaction.VoucherDate = DateTime.Today;
            }
            transaction.DueDate = _installment.DueDate;
            transaction.ReceivedDate = DateTime.Today;
            transaction.RegDate = DateTime.Today;
            transaction.Amount = paymentAmount;
            transaction.Source = paymentSourceName;
            transaction.KID = result.KID;
            transaction.DebtorAccountNo = "";//TODO
            transaction.InvoiceNo = result.InvoiceNo;
            transaction.CreditorAccountNo = "";

            transaction.User = _user;
            return transaction;
        }

        private string GetTransType(string paymentType)
        {
            switch (paymentType)
            {
                case "CASH":
                    return "CSH";

                case "BANKTERMINAL":
                    return "TMN";

                case "OCR":
                    return "LOP";

                case "BANKSTATEMENT":
                    return "BS";

                case "DEBTCOLLECTION":
                    return "DC";

                case "ONACCOUNT":
                    return "OA";

                case "GIFTCARD":
                    return "GC";

                case "BANK":
                    return "BNK";

                case "PREPAIDBALANCE":
                    return "PB";
            }
            return string.Empty;
        }
    }
}
