﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    class GetMembersUpdatedAction : USDBActionBase<List<ExceIBookingModifiedMember>>
    {
        private readonly DateTime _startDate;
        private readonly DateTime _endDate;
        private readonly int _systemId;
        private readonly int _branchId;

        public GetMembersUpdatedAction(DateTime date, int systemId, int branchId)
        {
            _startDate = date;
            _systemId = systemId;
            _branchId = branchId;
        }

        protected override List<ExceIBookingModifiedMember> Body(DbConnection connection)
        {
            List<ExceIBookingModifiedMember> memberList = new List<ExceIBookingModifiedMember>();
            const string storedProcedure = "USExceGMSIBookingGetMembersUpdatedByDate";

            string tempValue = string.Empty;
            try
            {
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@date", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@systemId", DbType.Int32, _systemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceIBookingModifiedMember member = new ExceIBookingModifiedMember();

                    member.Name = reader["Name"].ToString();
                    member.CustomerID = Convert.ToInt32(reader["CustomerID"]);
                    member.ParentCustomerID = Convert.ToInt32(reader["ParentCustID"]);
                    member.ContractNo = Convert.ToInt32(reader["ContractNo"]);
                    member.ContractTempNo = Convert.ToInt32(reader["ContractTempNo"]);
                    member.ContractTempName = reader["ContractTempName"].ToString();
                    if (reader["ContractStartDate"] != DBNull.Value)
                        member.ContractStartDate = Convert.ToString(reader["ContractStartDate"]);
                    if (reader["ContractEndDate"] != DBNull.Value)
                        member.ContractEndDate = Convert.ToString(reader["ContractEndDate"]);
                    if (reader["ActiveFreezeStartDate"] != DBNull.Value)
                        member.ActiveFreezeStartDate = Convert.ToString(reader["ActiveFreezeStartDate"]);
                    if (reader["ActiveFreezeEndDate"] != DBNull.Value)
                        member.ActiveFreezeEndDate = Convert.ToString(reader["ActiveFreezeEndDate"]);                    
                    member.ActiveStatus = Convert.ToInt32(reader["ActiveStatus"]);
                    if (reader["LastModifiedDateTime"] != DBNull.Value)
                        member.LastModifiedDateTime = Convert.ToString(reader["LastModifiedDateTime"]);
                    if (reader["CreatedDateTime"] != DBNull.Value)
                        member.CreatedDateTime = Convert.ToString(reader["CreatedDateTime"]);      
                    if (reader["NotifyMethod"] != DBNull.Value)
                        member.NotifyMethod = reader["NotifyMethod"].ToString();

                    memberList.Add(member);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return memberList;
        }
    }
}
