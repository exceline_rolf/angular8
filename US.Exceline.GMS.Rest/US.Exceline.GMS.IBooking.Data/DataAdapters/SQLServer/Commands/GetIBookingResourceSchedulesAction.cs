﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingResourceSchedulesAction : USDBActionBase<List<ExceIBookingResourceSchedule>>
    {
        private readonly int _branchId = -1;
        private readonly int _resourceId = -1;
        private readonly string _gymCode = string.Empty;
        private readonly DateTime _fromDate ;
        private readonly DateTime _toDate;
        public GetIBookingResourceSchedulesAction(int branchId,int resourceId,DateTime fromDate,DateTime toDate,string gymCode)
        {
            _branchId = branchId;
            _resourceId = resourceId;
            _gymCode = gymCode;
            _fromDate = fromDate;
            _toDate = toDate;
        }
        protected override List<ExceIBookingResourceSchedule> Body(DbConnection connection)
        {
            var exceIBookingList = new List<ExceIBookingResourceSchedule>();
            const string storedProcedure = "USExceGMSIBookingGetResourceSchedules";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Resourceid", DbType.Int32, _resourceId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Fromdate", DbType.DateTime, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Todate", DbType.DateTime, _toDate));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var exceIBookingResourceSchedule = new ExceIBookingResourceSchedule();
                    exceIBookingResourceSchedule.Id = Convert.ToInt32(reader["ID"]);
                    exceIBookingResourceSchedule.Day = reader["Day"].ToString();
                    exceIBookingResourceSchedule.FromTime = reader["FromTime"].ToString();
                    exceIBookingResourceSchedule.ToTime = reader["ToTime"].ToString();
                    exceIBookingResourceSchedule.RoleType = reader["RoleType"].ToString();
                    if (exceIBookingResourceSchedule.RoleType.Equals("RESSCHEDULE"))
                    {
                        var action = new GetIBookingResourceSwitchDataRangeAction(exceIBookingResourceSchedule.Id, _fromDate, _toDate);
                        exceIBookingResourceSchedule.UnavailableTime = action.Execute(EnumDatabase.Exceline, _gymCode);
                    }

                    exceIBookingList.Add(exceIBookingResourceSchedule);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return exceIBookingList;
        }
    }
}
