﻿using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingEntityValidAction : USDBActionBase<bool>
    {
        private readonly int _entityId;
        private readonly string _entityType;
        private readonly int _branchId;
        private bool _result;

        public GetIBookingEntityValidAction(int entityId, string entityType, int branchId)
        {
            _entityId = entityId;
            _entityType = entityType;
            _branchId = branchId;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingCheckEntityExists";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.String, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityType", DbType.String, _entityType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbParameter para3 = new SqlParameter();
                para3.DbType = DbType.Int32;
                para3.ParameterName = "@exists";
                para3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para3);
                cmd.ExecuteReader();
                _result = Convert.ToBoolean(para3.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _result;
        }
    }
}
