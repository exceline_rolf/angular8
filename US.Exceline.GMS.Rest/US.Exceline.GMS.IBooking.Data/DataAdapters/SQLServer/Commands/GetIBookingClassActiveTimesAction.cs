﻿using System;
using System.Collections.Generic;
using System.Linq;
using US_DataAccess;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingClassActiveTimesAction : USDBActionBase<List<ExceIBookingClassTime>>
    {
        private int _classId;
        private int _branchId;

        public GetIBookingClassActiveTimesAction(int classId, int branchId)
        {
            _classId = classId;
            _branchId = branchId; ;
        }

        protected override List<ExceIBookingClassTime> Body(DbConnection connection)
        {
            GetIBookingClassMemberBookingsAction getMemberBookingsAction = new GetIBookingClassMemberBookingsAction(_classId, _branchId);
            List<ExceIBookingClassMemberBooking> classBookingDetails = getMemberBookingsAction.Execute(EnumDatabase.USP);

            int notBookedMembersForClassCount = classBookingDetails.Where(x => x.TimeId == 0).Count();

            List<ExceIBookingClassTime> excelineClassActiveTimeList = new List<ExceIBookingClassTime>();
            string storedProcedureName = "USExceGMSIBookingGetClassActiveTimes";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceIBookingClassTime excelineClassActiveTime = new ExceIBookingClassTime();
                    excelineClassActiveTime.ClassId = _classId;
                    excelineClassActiveTime.TimeId = Convert.ToInt32(reader["ActiveTimeId"]);
                    if (reader["StartDateTime"] != DBNull.Value)
                    {
                        excelineClassActiveTime.ClassDate = Convert.ToDateTime(reader["StartDateTime"]).Date;
                        excelineClassActiveTime.ClassStartTime = Convert.ToDateTime(reader["StartDateTime"]);
                    }
                    if (reader["EndDateTime"] != DBNull.Value)
                    {
                        excelineClassActiveTime.ClassEndTime = Convert.ToDateTime(reader["EndDateTime"]);
                    }
                    int maxNumOfSlots = Convert.ToInt32(reader["ClassMaxParticipants"]);
                    excelineClassActiveTime.TotalAvailableSlots = (maxNumOfSlots) - (notBookedMembersForClassCount + classBookingDetails.Where(x => x.TimeId == excelineClassActiveTime.TimeId).Sum(n => n.Participants));
                    excelineClassActiveTimeList.Add(excelineClassActiveTime);
                }
                if (excelineClassActiveTimeList.Count > 0)
                {
                    excelineClassActiveTimeList = excelineClassActiveTimeList.Where(x => x.ClassDate >= DateTime.Now.Date).ToList();
                }
                return excelineClassActiveTimeList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
