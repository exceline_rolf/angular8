﻿


using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIbookingMemberGymsAction : USDBActionBase<List<ExceIBookingMemberGymDetail>>
    {

        protected override List<ExceIBookingMemberGymDetail> Body(DbConnection connection )
        {
            List<ExceIBookingMemberGymDetail> memberGymList = new List<ExceIBookingMemberGymDetail>();
            const string storedProcedure = "USExceGMSIBookingGetAllMemberGyms";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceIBookingMemberGymDetail memberGym = new ExceIBookingMemberGymDetail();
                    memberGym.CustomerNo = Convert.ToString(reader["CustomerNo"]);
                    memberGym.GymID = Convert.ToString(reader["GymId"]);
                    memberGymList.Add(memberGym);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return memberGymList;
        }
    }
}
