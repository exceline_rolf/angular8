﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingInterestsAction : USDBActionBase<List<ExceIBookingInterest>>
    {
        public GetIBookingInterestsAction()
        {
        }
        protected override List<ExceIBookingInterest> Body(DbConnection connection)
        {
            List<ExceIBookingInterest> interestList = new List<ExceIBookingInterest>();
            const string storedProcedure = "USExceGMSIBookingGetInterests";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingInterest interest = new ExceIBookingInterest();
                    interest.InterestId = Convert.ToInt32(reader["InterestId"]);
                    interest.InterestName = reader["InterestName"].ToString();
                    interestList.Add(interest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return interestList;
        }
    }
}
