﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateTerminalStatusAction : USDBActionBase<bool>
    {
        private readonly ExceAccessControlUpdateTerminalStatusDC _terminalStatusDetail;

        public UpdateTerminalStatusAction(ExceAccessControlUpdateTerminalStatusDC terminalStatusDetail)
        {
            _terminalStatusDetail = terminalStatusDetail;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAccessControlUpdateTerminalStatus";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsOnline", DbType.Boolean, _terminalStatusDetail.IsOnlined));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TerminalId", DbType.Int32, _terminalStatusDetail.TerminalId));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Boolean;
                output.ParameterName = "@OutPut";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                return Convert.ToBoolean(output.Value);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
