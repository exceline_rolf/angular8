﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberDetailForSunbedPurchaseAction : USDBActionBase<ExceAccessControlAuthenticationDC>
    {
        private string _gatCardNumber;

        public GetMemberDetailForSunbedPurchaseAction(string gateCardNumber)
        {
            this._gatCardNumber = gateCardNumber;
        }
        protected override ExceAccessControlAuthenticationDC Body(System.Data.Common.DbConnection connection)
        {
            ExceAccessControlAuthenticationDC memberDetail = new ExceAccessControlAuthenticationDC();
            string spName = "USExceGetMemberDetailForGatPurchase";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@cardNo", SqlDbType.VarChar, _gatCardNumber));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    memberDetail.Balance = Convert.ToDecimal(reader["Balance"]);
                    memberDetail.MemberId = Convert.ToInt32(reader["MemberId"]);
                    memberDetail.IsValidMember = reader["ShopAccountId"] == DBNull.Value ? (bool?)null : true;
                    memberDetail.BranchId = Convert.ToInt32(reader["BranchId"]);                   
                    
                }
            }
            catch (Exception)
            {
                throw;
            }
            return memberDetail;
        }
    }
}
