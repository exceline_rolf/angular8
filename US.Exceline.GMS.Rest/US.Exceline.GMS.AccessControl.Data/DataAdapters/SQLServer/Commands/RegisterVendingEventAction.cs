﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterVendingEventAction : USDBActionBase<bool>
    {
        private readonly ExceAccessControlVendingEventDC _vendingEventDetail;

        public RegisterVendingEventAction(ExceAccessControlVendingEventDC vendingEventDetail)
        {
            _vendingEventDetail = vendingEventDetail;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAccessControlRegisterVendingEvent";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CardNo", DbType.String, _vendingEventDetail.CardNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TerminalId", DbType.Int32, _vendingEventDetail.TerminalId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _vendingEventDetail.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Text", DbType.String, _vendingEventDetail.Text));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Boolean;
                output.ParameterName = "@OutPut";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                return Convert.ToBoolean(output.Value);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
