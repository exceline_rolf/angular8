﻿using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer.Commands
{
    public class CheckAuthenticationAction : USDBActionBase<ExceAccessControlAuthenticationDC>
    {
        private readonly int _branchId;
        private readonly string _cardNo;
        private readonly int _terminalId;
        public CheckAuthenticationAction(int branchId, string cardNo,int terminalId)
        {
            _branchId = branchId;
            _cardNo = cardNo;
            _terminalId = terminalId;
        }
        protected override ExceAccessControlAuthenticationDC Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSAccessControlCheckAuthenticationWithFingerPrint";
            ExceAccessControlAuthenticationDC authentication = new ExceAccessControlAuthenticationDC();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@cardNo", DbType.String, _cardNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@terminalId", DbType.Int32, _terminalId));
                DbDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    authentication.Balance = Convert.ToDecimal(reader["Balance"]);
                    authentication.MemberId = Convert.ToInt32(reader["MemberId"]);
                    authentication.IsValidMember = reader["ShopAccountId"] == DBNull.Value ? (bool?) null : true;
                    authentication.BranchId = Convert.ToInt32(reader["BranchId"]);
                    authentication.CheckFingerPrint = Convert.ToBoolean(reader["CheckFingerPrint"]);
                    
                }
                else
                {
                    authentication.Balance = 0.00M;
                    authentication.IsValidMember = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return authentication;
        }
    }
}
