﻿using System.Collections.Generic;
using System.ServiceModel;
//using US.Payment.Modules.Economy.Core.DomainObjects;

namespace US.Exceline.GMS.Modules.Economy.Service.Apportionment
{
    [ServiceContract(Name = "Apportionment", Namespace = "US.Exceline.GMS.Modules.ExcEconomy.Service")]
    public interface IApportionment
    {
        [OperationContract]
        bool Test();

        [OperationContract]
        string GetName();

        [OperationContract]
        US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums.ReverseConditions ReverseApportionments(int paymentId, int arItem, string actionStatus, string user, string dbCode);

        [OperationContract]
        List<US.Exceline.GMS.Modules.Economy.Core.SystemObjects.USPApportionmentTransaction> GetInitialApportionmentData(string KID, string creditoNo, decimal amount, int arNo, int caseNo, string dbCode);

        [OperationContract]
        int AddApportionmentData(List<US.Exceline.GMS.Modules.Economy.Core.SystemObjects.USPApportionmentTransaction> transactionList, string user);
    }
}

