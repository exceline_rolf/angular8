﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18034
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CCXTestApplication.CCXService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="CCXService.ICredicareService")]
    public interface ICredicareService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICredicareService/GetAllInvoices", ReplyAction="http://tempuri.org/ICredicareService/GetAllInvoicesResponse")]
        string GetAllInvoices(int creditorNo, int chunkSize);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICredicareService/GetInvoices", ReplyAction="http://tempuri.org/ICredicareService/GetInvoicesResponse")]
        string GetInvoices(System.DateTime startDate, System.DateTime endDate, int chunkSize);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ICredicareServiceChannel : CCXTestApplication.CCXService.ICredicareService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CredicareServiceClient : System.ServiceModel.ClientBase<CCXTestApplication.CCXService.ICredicareService>, CCXTestApplication.CCXService.ICredicareService {
        
        public CredicareServiceClient() {
        }
        
        public CredicareServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CredicareServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CredicareServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CredicareServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string GetAllInvoices(int creditorNo, int chunkSize) {
            return base.Channel.GetAllInvoices(creditorNo, chunkSize);
        }
        
        public string GetInvoices(System.DateTime startDate, System.DateTime endDate, int chunkSize) {
            return base.Channel.GetInvoices(startDate, endDate, chunkSize);
        }
    }
}
