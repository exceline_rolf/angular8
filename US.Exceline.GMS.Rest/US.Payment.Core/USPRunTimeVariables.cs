﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using US.Payment.Core.Exceptions;
using System.Xml.Serialization;
using US.Payment.Core.Configurations;

namespace US.Payment.Core
{
    public class USPRunTimeVariables
    {
        public static string GetUSPConnectionString()
        {
            //TextWriter tw = File.CreateText("C:\\perl.txt");
            try
            {
                return USPConfigurationReader.GetUSPCongfiuration().USPConnectionString;
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("Failed to read USP Database connection string. " + ex.Message, ex);
            }
        }

        public static string GetLOGConnectionString()
        {
            try
            {
                return USPConfigurationReader.GetUSPCongfiuration().LOGConnectionString;
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("Failed to read LOG Database connection string. " + ex.Message, ex);
            }
        }
        public static string GetProcassoConnectionString()
        {
            USPConfigurations configurations = USPConfigurationReader.GetUSPCongfiuration();
            foreach (ConfigItem c in configurations.ExtendedConfiguratiions.Configurations)
            {
                if (c.Key == "ProcassoConnectionString")
                {
                    return c.Value;
                }
            }
            throw new Exception("procasso connection string is not supplied in USP Configuration file");
        }

        public static string GetAppFabricMonitoringConnectionString()
        {
            USPConfigurations configurations = USPConfigurationReader.GetUSPCongfiuration();
            foreach (ConfigItem c in configurations.ExtendedConfiguratiions.Configurations)
            {
                if (c.Key == "AppFabricMonitoringConnectionString")
                {
                    return c.Value;
                }
            }
            throw new Exception("App Fabric MonitoringConnectionString connection string is not supplied in USP Configuration file");
        }
        public static string GetAppFabricPersistingConnectionString()
        {
            USPConfigurations configurations = USPConfigurationReader.GetUSPCongfiuration();
            foreach (ConfigItem c in configurations.ExtendedConfiguratiions.Configurations)
            {
                if (c.Key == "AppFabricPersistingConnectionString")
                {
                    return c.Value;
                }
            }
            throw new Exception("App Fabric PersistingConnectionString connection string is not supplied in USP Configuration file");
        }

        public static string GetPaymentMachineConnectionString()
        {
            try
            {
                return USPConfigurationReader.GetUSPCongfiuration().PaymentMachineConnectionString;
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("Payment machine connection string is not supplied in USP Configuration file. " + ex.Message, ex);
            }
        }

        public static string GetWorkflowConnectionString()
        {
            USPConfigurations configurations = USPConfigurationReader.GetUSPCongfiuration();
            foreach (ConfigItem c in configurations.ExtendedConfiguratiions.Configurations)
            {
                if (c.Key == "WorkflowConnectionString")
                {
                    return c.Value;
                }
            }
            throw new Exception("Workflow Connection String is not supplied in USP Configuration file");
        }

        private string _excelineConnectionString = string.Empty;

        public string ExcelineConnectionString
        {
            get { return _excelineConnectionString; }
            set { _excelineConnectionString = value; }
        }

        public static string GetReportServerConnectionString()
        {
            try
            {
                return USPConfigurationReader.GetUSPCongfiuration().ReportServerConnectionString;
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("Report Server connection string is not supplied in USP Configuration file. " + ex.Message, ex);
            }
        }

        public static string GetSearchDBConnectionString()
        {
            string connectionString = string.Empty;
            try
            {
                USPConfigurations usConfig = new USPConfigurations();
                usConfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in usConfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key == "SearchDataBaseConnection")
                    {
                        connectionString = item.Value;
                        return connectionString;
                    }

                }
                throw new USPConfigurationReadException("Search DB Server connection string is not supplied in USP Configuration file", new Exception("Error"));
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("Search DB Server connection string is not supplied in USP Configuration file. " + ex.Message, ex);
            }

        }

        public static string GetUSSRoot()
        {
            string ussRoot = string.Empty;
            try
            {
                USPConfigurations bConfig = new USPConfigurations();
                bConfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in bConfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key == "USSRoot")
                    {
                        ussRoot = item.Value;
                        return ussRoot;
                    }

                }
                throw new USPConfigurationReadException("USSRoot is not supplied in USP Configuration file", new Exception("Error"));
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("USSRoot is not supplied in USP Configuration file" + ex.Message, ex);
            }
        }

        public static string GetWinmedDBConnectionString()
        {
            string connectionString = string.Empty;
            try
            {
                USPConfigurations usConfig = new USPConfigurations();
                usConfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in usConfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key == "WinmedAdminConnection")
                    {
                        connectionString = item.Value;
                        return connectionString;
                    }

                }
                throw new USPConfigurationReadException("WinmedAdmin DB Server connection string is not supplied in USP Configuration file", new Exception("Error"));
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("WinmedAdminConnection DB Server connection string is not supplied in USP Configuration file. " + ex.Message, ex);
            }

        }

        public static string GetWorkStationConnectionString()
        {
            string connectionString = string.Empty;
            try
            {
                USPConfigurations usConfig = new USPConfigurations();
                usConfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in usConfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key == "ExcelineWorkStation")
                    {
                        connectionString = item.Value;
                        return connectionString;
                    }

                }
                throw new USPConfigurationReadException("WinmedAdmin DB Server connection string is not supplied in USP Configuration file", new Exception("Error"));
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("WinmedAdminConnection DB Server connection string is not supplied in USP Configuration file. " + ex.Message, ex);
            }

        }


        //Added GetDelayActionNo
        public static string GetDelayActionNo()
        {
            string delayAction = string.Empty;
            try
            {
                USPConfigurations dconfig = new USPConfigurations();
                dconfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in dconfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key == "DelayActionNo")
                    {
                        delayAction = item.Value;
                        return delayAction;
                    }
                }

                throw new USPConfigurationReadException("DelayActionNo is not supplied in USP Configuration file", new Exception("Error"));
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("DelayActionNo is not supplied in USP Configuration file" + ex.Message, ex);
            }
        }

        //added GetBureauActionNo
        public static string GetBureauActionNo()
        {
            string bureauActionNo = string.Empty;
            try
            {
                USPConfigurations bConfig = new USPConfigurations();
                bConfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in bConfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key == "BureauActionNo")
                    {
                        bureauActionNo = item.Value;
                        return bureauActionNo;
                    }

                }
                throw new USPConfigurationReadException("BureauActionNo is not supplied in USP Configuration file", new Exception("Error"));
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("BureauActionNo is not supplied in USP Configuration file" + ex.Message, ex);
           }
        }

        //added GetClientActionNo
        public static string GetClientActionNo()
        {
            string clientActionNo = string.Empty;
            try
            {
                USPConfigurations cconfig = new USPConfigurations();
                cconfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in cconfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key == "ClientActionNo")
                    {
                        clientActionNo = item.Value;
                        return clientActionNo;
                    }
                }
                throw new USPConfigurationReadException("ClientActionNo is not supplied in USP Configuration file", new Exception("Error"));
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("ClientActionNo is not supplied in USP Configuration file" + ex.Message, ex);
            }
        }

        public static string ProcassoInstallationPath()
        {
            string installationPath = string.Empty;
            try
            {
                USPConfigurations cconfig = new USPConfigurations();
                cconfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in cconfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key == "ProcassoInstallationPath")
                    {
                        installationPath = item.Value;
                        return installationPath;
                    }
                }
                throw new USPConfigurationReadException("installationPath is not supplied in USP Configuration file", new Exception("Error"));
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("ClientActionNo is not supplied in USP Configuration file" + ex.Message, ex);
            }
        }
        public static string GetConfigurationByName(string configurationName)
        {
            string configuration = string.Empty;
            try
            {
                USPConfigurations cconfig = new USPConfigurations();
                cconfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in cconfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key.Trim () == configurationName.Trim ())
                    {
                        configuration = item.Value;
                        return configuration;
                    }
                }
                throw new USPConfigurationReadException(configurationName +" is not supplied in USP Configuration file", new Exception("Error"));
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException(configurationName + " is not supplied in USP Configuration file" + ex.Message, ex);
            }
        }
        public static string IncomingCallClinetTriggerTime()
        {
            string installationPath = string.Empty;
            try
            {
                USPConfigurations cconfig = new USPConfigurations();
                cconfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in cconfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key == "IncomingCallClinetTriggerTime")
                    {
                        installationPath = item.Value;
                        return installationPath;
                    }
                }
                throw new USPConfigurationReadException("Incoming call client trigger time is not supplied in USP Configuration file", new Exception("Error"));
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("Incoming call client trigger time is not supplied in USP Configuration file" + ex.Message, ex);
            }
        }


        public static string GetInputFilePluginsPath()
        {
            string fileName = string.Empty;
            fileName = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).Directory.FullName + @"\InputFilePlugins\";
            if (!File.Exists(fileName))
            {
                USPConfigurations bConfig = new USPConfigurations();
                bConfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in bConfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key == "USSRoot")
                    {
                        fileName = item.Value + @"\InputFilePlugins\"; ;
                        return fileName;
                    }

                }
            }
            return fileName;
        }
        public static string GetOutputFilePluginsPath()
        {
            return new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).Directory.FullName + @"\OutputFilePlugins\";
        }

        public static string GetInputClaimFilePluginsPath()
        {
            return new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).Directory.FullName + @"\InputFilePlugins\ClaimPlugins\";
        }

        public static string GetConnectionSource()
        {
            try
            {
                string connectionSource = string.Empty;
                USPConfigurations bConfig = new USPConfigurations();
                bConfig = USPConfigurationReader.GetUSPCongfiuration();
                foreach (var item in bConfig.ExtendedConfiguratiions.Configurations)
                {
                    if (item.Key == "ConnectionSource")
                    {
                        connectionSource = item.Value;
                        return connectionSource;
                    }
                }
                throw new USPConfigurationReadException("ConnectionSource is not supplied in USP Configuration file", new Exception("Error"));
            }
            catch (Exception ex)
            {
                throw new USPConfigurationReadException("Incoming call client trigger time is not supplied in USP Configuration file" + ex.Message, ex);
            }

        }
    }
}
