﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.Configurations;

namespace US.Payment.Core.RunTime
{
    public class ModulesAndFeatures
    {
        private static string _moduleRoot = string.Empty;

        public static string ModuleRoot
        {
            get
            {
                USPConfigurations configurations = USPConfigurationReader.GetUSPCongfiuration();
                foreach (ConfigItem c in configurations.ExtendedConfiguratiions.Configurations)
                {
                    if (c.Key == "ModuleRoot")
                    {
                        return c.Value;
                    }
                }
                throw new Exception("Module Root is not supplied in USP Configuration file");
            }
            set
            {
                ;
            }
        }
       
    }
}
