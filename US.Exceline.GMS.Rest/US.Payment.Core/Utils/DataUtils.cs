﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace US.Payment.Core.Utils
{
    public class DataUtils
    {
        public static string ObjectToString(object obj)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            else
            {
                return obj.ToString();
            }
        }

        public static decimal IsValidDecimal(string val)
        {
            string tempval = string.Empty;
            string currentCul = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            if (currentCul.Equals("nb-NO"))
            {
                tempval = val.Replace(".", ",");
            }
            else
            {
                tempval = val;
            }
            try
            {
                decimal decVal = decimal.Parse(tempval);
                return decVal;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static US.Payment.Core.Enums.InvoiceTypes GetItemTypeEnumValue(string itemType)
        {
            if (itemType == "DD")
            {
                return US.Payment.Core.Enums.InvoiceTypes.DirectDeduct;
            }
            else if (itemType == "IP")
            {
                return US.Payment.Core.Enums.InvoiceTypes.InvoiceForPrint;
            }
            else if (itemType == "IN")
            {
                return US.Payment.Core.Enums.InvoiceTypes.Invoice;
            }
            else if (itemType == "DD")
            {
                return US.Payment.Core.Enums.InvoiceTypes.DirectDeduct;
            }
            else if (itemType == "DW")
            {
                return US.Payment.Core.Enums.InvoiceTypes.DebtWarning;
            }
            else if (itemType == "OP")
            {
                return US.Payment.Core.Enums.InvoiceTypes.OP; ;
            }
            else if (itemType == "SP")
            {
                return US.Payment.Core.Enums.InvoiceTypes.Sponser;
            }
            else if (itemType == "OL")
            {
                return US.Payment.Core.Enums.InvoiceTypes.OL;
            }
            else if (itemType == "DB")
            {
                return US.Payment.Core.Enums.InvoiceTypes.DB;
            }
            else if (itemType == "SM")
            {
                return US.Payment.Core.Enums.InvoiceTypes.SM;
            }
            else if (itemType == "CL")
            {
                return US.Payment.Core.Enums.InvoiceTypes.CL;
            }
            else
            {
                return US.Payment.Core.Enums.InvoiceTypes.Default;
            }
        }
        public static bool IsValiedInteger(string stringValue)
        {
            Regex objIntPattern = new Regex("^-[0-9]+$|^[0-9]+$");
            return objIntPattern.IsMatch(stringValue);
        }
    }
}
