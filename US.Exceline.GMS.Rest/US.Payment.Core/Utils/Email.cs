﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Utils
{
    public class Email
    {
        private string _subject = string.Empty;

        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
        private string _body = string.Empty;

        public string Body
        {
            get { return _body; }
            set { _body = value; }
        }
        private string _from = string.Empty;

        public string From
        {
            get { return _from; }
            set { _from = value; }
        }
    }
}
