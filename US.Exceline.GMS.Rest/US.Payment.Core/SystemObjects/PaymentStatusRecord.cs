﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.SystemObjects
{
    public class PaymentStatusRecord
    {
        private string _usdf_id = string.Empty;
        public string USDF_ID
        {
            get
            {
                return _usdf_id;
            }
            set
            {
                _usdf_id = value;
            }
        }
        private int _caseRecordID = 0;
        public int CaseRecordID
        {
            get
            {
                return _caseRecordID;
            }
            set
            {
                _caseRecordID = value;
            }
        }
            
        private string _registerDate = string.Empty;
        public string RegisterDate
        {
            get
            {
                return _registerDate;
            }
            set
            {
                _registerDate = value;
            }
        }
        private string _creditorNumber = string.Empty;
        public string CreditorNumber
        {
            get
            {
                return _creditorNumber;
            }
            set
            {
                _creditorNumber = value;
            }
        }
        private int _CaseNumberID = 0;
        public int CaseNumberID
        {
            get
            {
                return _CaseNumberID;
            }
            set
            {
                _CaseNumberID = value;
            }
        }
        private string _creditorNumberError = string.Empty;
        public string CreditorNumberError
        {
            get
            {
                return _creditorNumberError;
            }
            set
            {
                _creditorNumberError = value;
            }
        }
        private string _credRef = string.Empty;
        public string CredRef
        {
            get
            {
                return _credRef;
            }
            set
            {
                _credRef = value;
            }
        }
        private string _credRefError = string.Empty;
        public string CredRefError
        {
            get
            {
                return _credRefError;
            }
            set
            {
                _credRefError = value;
            }
        }
        private string _creditorName = string.Empty;
        public string CreditorName
        {
            get
            {
                return _creditorName;
            }
            set
            {
                _creditorName = value;
            }
        }

        private string _creditorNameError = string.Empty;
        public string CreditorNameError
        {
            get
            {
                return _creditorNameError;
            }
            set
            {
                _creditorNameError = value;
            }
        }
        private string _recordType = string.Empty;
        public string RecordType
        {
            get
            {
                return _recordType;
            }
            set
            {
                _recordType = value;
            }
        }
        private string _caseNumber = string.Empty;
        public string CaseNumber
        {
            get
            {
                return _caseNumber;
            }
            set
            {
                _caseNumber = value;
            }
        }

        private string _debtWarningDate = string.Empty;
        public string DebtWarningDate
        {
            get
            {
                return _debtWarningDate;
            }
            set
            {
                _debtWarningDate = value;
            }
        }
        private string _debtWarningDateError = string.Empty;
        public string DebtWarningDateError
        {
            get
            {
                return _debtWarningDateError;
            }
            set
            {
                _debtWarningDateError = value;
            }
        }
        private string _paynoteDate = string.Empty;
        public string PayNoteDate
        {
            get
            {
                return _paynoteDate;
            }
            set
            {
                _paynoteDate = value;
            }
        }
        private string _paynoteDateError = string.Empty;
        public string PayNoteDateError
        {
            get
            {
                return _paynoteDateError;
            }
            set
            {
                _paynoteDateError = value;
            }
        }
        private string _kid = string.Empty;
        public string Kid
        {
            get
            {
                return _kid;
            }
            set
            {
                _kid = value;
            }
        }
        //////////////////////
        private string _TdueDate = string.Empty;
        public string TDueDate
        {
            get
            {
                return _TdueDate;
            }
            set
            {
                _TdueDate = value;
            }
        }
        private string _TdueDateError = string.Empty;
        public string TDueDateError
        {
            get
            {
                return _TdueDateError;
            }
            set
            {
                _TdueDateError = value;
            }
        }
        private Double _amount = 0.0;
        public Double Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }
        private string _amountError = string.Empty;
        public string AmountError
        {
            get
            {
                return _amountError;
            }
            set
            {
                _amountError = value;
            }
        }
        private string _tReference = string.Empty;
        public string TReference
        {
            get
            {
                return _tReference;
            }
            set
            {
                _tReference = value;
            }
        }
        private string _tReferenceError = string.Empty;
        public string TReferenceError
        {
            get
            {
                return _tReferenceError;
            }
            set
            {
                _tReferenceError = value;
            }
        }
        private string _TTreastext = string.Empty;
        public string TTreastext
        {
            get
            {
                return _TTreastext;
            }
            set
            {
                _TTreastext = value;
            }
        }
        private string _TVoucherdate = string.Empty;
        public string TVoucherdate
        {
            get
            {
                return _TVoucherdate;
            }
            set
            {
                _TVoucherdate = value;
            }
        }
        private string _TTkid = string.Empty;
        public string TTkid
        {
            get
            {
                return _TTkid;
            }
            set
            {
                _TTkid = value;
            }
        }
        private string _TRefrence2 = string.Empty;
        public string TReference2
        {
            get
            {
                return _TRefrence2;
            }
            set
            {
                _TRefrence2 = value;
            }
        }
        /////////////////////////////////////////////
        private int _debtorRecordID = 0;
        public int DebtorRecordID
        {
            get
            {
                return _debtorRecordID;
            }
            set
            {
                _debtorRecordID = value;
            }
        }
            
        private string _debtorNumbher = string.Empty;
        public string DebtorNumber
        {
            get
            {
                return _debtorNumbher;
            }
            set
            {
                _debtorNumbher = value;
            }
        }
        private string _debtFirstName = string.Empty;
        public string DebtFirstName
        {
            get
            {
                return _debtFirstName;
            }
            set
            {
                _debtFirstName = value;
            }
        }

        private string _debtorLastNameError = string.Empty;
        public string DebtorLastNameError
        {
            get
            {
                return _debtorLastNameError;
            }
            set
            {
                _debtorLastNameError = value;
            }
        }
        private string _DebtorTitle = string.Empty;
        public string DebtorTitle
        {
            get
            {
                return _DebtorTitle;
            }
            set
            {
                _DebtorTitle = value;
            }
        }
        private string _Debtype = string.Empty;
        public string Debtype
        {
            get
            {
                return _Debtype;
            }
            set
            {
                _Debtype = value;
            }
        }
        private string _DebtorPersonno1 = string.Empty;
        public string DebtorPersonno1
        {
            get
            {
                return _DebtorPersonno1;
            }
            set
            {
                _DebtorPersonno1 = value;
            }
        }
        private string _DebtorPersonno2 = string.Empty;
        public string DebtorPersonno2
        {
            get
            {
                return _DebtorPersonno2;
            }
            set
            {
                _DebtorPersonno2 = value;
            }
        }
        private string _DebtorAddress1 = string.Empty;
        public string DebtorAddress1
        {
            get
            {
                return _DebtorAddress1;
            }
            set
            {
                _DebtorAddress1 = value;
            }
        }
        private string _DebtorAddress2 = string.Empty;
        public string DebtorAddress2
        {
            get
            {
                return _DebtorAddress2;
            }
            set
            {
                _DebtorAddress2 = value;
            }
        }
        private string _DebtorPostalcode = string.Empty;
        public string DebtorPostalcode
        {
            get
            {
                return _DebtorPostalcode;
            }
            set
            {
                _DebtorPostalcode = value;
            }

        }
        private string _DebtorPostalcodeError = string.Empty;
        public string DebtorPostalcodeError
        {
            get
            {
                return _DebtorPostalcodeError;
            }
            set
            {
                _DebtorPostalcodeError = value;
            }

        }
        private string _DebtorPostalarea = string.Empty;
        public string DebtorPostalarea
        {
            get
            {
                return _DebtorPostalarea;
            }
            set
            {
                _DebtorPostalarea = value;
            }
        }
        private string _DebtorBirthdate = string.Empty;
        public string DebtorBirthdate
        {
            get
            {
                return _DebtorBirthdate;
            }
            set
            {
                _DebtorBirthdate = value;
            }
        }
        private string _DebtorBirthdateError = string.Empty;
        public string DebtorBirthdateError
        {
            get
            {
                return _DebtorBirthdateError;
            }
            set
            {
                _DebtorBirthdateError = value;
            }
        }
        private string _DebtorEmail = string.Empty;
        public string DebtorEmail
        {
            get
            {
                return _DebtorEmail;
            }
            set
            {
                _DebtorEmail = value;
            }
        }
        private string _DebtorTelehome = string.Empty;
        public string DebtorTelehome
        {
            get
            {
                return _DebtorTelehome;
            }
            set
            {
                _DebtorTelehome = value;
            }
        }
        private string _DebtorTelemobile = string.Empty;
        public string DebtorTelemobile
        {
            get
            {
                return _DebtorTelemobile;
            }
            set
            {
                _DebtorTelemobile = value;
            }
        }
        private string _DebtorTelework = string.Empty;
        public string DebtorTelework
        {
            get
            {
                return _DebtorTelework;
            }
            set
            {
                _DebtorTelework = value;
            }
        }
            
        /// <summary>
        /// ////////////////////////////////////
        /// </summary>

  
            
           
            
            
           
            
            
            
           
            
           
           
           
            private string _caseNumberAction = string.Empty;
            public string CaseNumberAction
            {
                get
                {
                    return _caseNumberAction;
                }
                set
                {
                    _caseNumberAction = value;
                }
            }
            

            
            private string _isImported = string.Empty;
            public string IsImported
            {
                get
                {
                    return _isImported;
                }
                set
                {
                    _isImported = value;
                }
            }
            private string _isCaseAdded = string.Empty;
            public string IsCaseAdded
            {
                get
                {
                    return _isCaseAdded;
                }
                set
                {
                    _isCaseAdded = value;
                }
            }
            private int _transactionRecordID = 0;
            public int TransactionRecordID
            {
                get
                {
                    return _transactionRecordID;
                }
                set
                {
                    _transactionRecordID = value;
                }
            }
            private string _IsTransactionAdded = string.Empty;
            public string IsTransactionAdded
            {
                get
                {
                    return _IsTransactionAdded;
                }
                set
                {
                    _IsTransactionAdded = value;
                }
            }
           
            
            private string _TVoucherdateError = string.Empty;
            public string TVoucherdateError
            {
                get
                {
                    return _TVoucherdateError;
                }
                set
                {
                    _TVoucherdateError = value;
                }
            }
            private string _TTranstype = string.Empty;
            public string TTranstype
            {
                get
                {
                    return _TTranstype;
                }
                set
                {
                    _TTranstype = value;
                }
            }
            private string _TTranstypeError = string.Empty;
            public string TTranstypeError
            {
                get
                {
                    return _TTranstypeError;
                }
                set
                {
                    _TTranstypeError = value;
                }
            }
           
            
            private string _TCaseno = string.Empty;
            public string TCaseno
            {
                get
                {
                    return _TCaseno;
                }
                set
                {
                    _TCaseno = value;
                }
            }
            
           
           

            //Old Data
            //private string _actionLable = string.Empty;
            //public string Actionlable
            //{
            //    get
            //    {
            //        return _actionLable;
            //    }
            //    set
            //    {
            //        _actionLable = value;
            //    }
            //}
            //private string _action = string.Empty;
            //public string Action
            //{
            //    get
            //    {
            //        return _action;
            //    }
            //    set
            //    {
            //        _action = value;
            //    }
            //}
            //private string _importAllAction = string.Empty;
            //public string ImportAllAction
            //{
            //    get
            //    {
            //        return _importAllAction;
            //    }
            //    set
            //    {
            //        _importAllAction = value;
            //    }
            //}
            //private string _deleteAllAction = string.Empty;
            //public string DeleteAllAction
            //{
            //    get
            //    {
            //        return _deleteAllAction;
            //    }
            //    set
            //    {
            //        _deleteAllAction = value;
            //    }
            //}
            //private string _deleteAllFromProcassAndListAction = string.Empty;
            //public string DeleteAllFromProcassoAndListAction
            //{
            //    get
            //    {
            //        return _deleteAllFromProcassAndListAction;
            //    }
            //    set
            //    {
            //        _deleteAllFromProcassAndListAction = value;
            //    }
            //}
            //private string _procassourl = string.Empty;
            //public string ProcassoURL
            //{
            //    get
            //    {
            //        return _procassourl;
            //    }
            //    set
            //    {
            //        _procassourl = value;
            //    }
            //}
            //private string _caseError = string.Empty;
            //public string CaseError
            //{
            //    get
            //    {
            //        return _caseError;
            //    }
            //    set
            //    {
            //        _caseError = value;
            //    }
            //}
            //private string _transError = string.Empty;
            //public string TransError
            //{
            //    get
            //    {
            //        return _transError;
            //    }
            //    set
            //    {
            //        _transError = value;
            //    }
            //}
            //private Boolean _isExpanded = false;
            //public Boolean IsExpanded
            //{
            //    get
            //    {
            //        return _isExpanded;
            //    }
            //    set
            //    {
            //        _isExpanded = value;
            //    }
            //}
            //private Boolean _isActionEnable = false;
            //public Boolean IsActionEnable
            //{
            //    get
            //    {
            //        return _isActionEnable;
            //    }
            //    set
            //    {
            //        _isActionEnable = value;
            //    }
            //}
            //private int _isFailed = 0;
            //public int IsFailed
            //{
            //    get
            //    {
            //        return _isFailed;
            //    }
            //    set
            //    {
            //        _isFailed = value;
            //    }
            //}
            //private int _isSucceeded = 0;
            //public int IsSucceeded
            //{
            //    get
            //    {
            //        return _isSucceeded;
            //    }
            //    set
            //    {
            //        _isSucceeded = value;
            //    }
            //}
            //private string _fileName = string.Empty;
            //public string FileName
            //{
            //    get
            //    {
            //        return _fileName;
            //    }
            //    set
            //    {
            //        _fileName = value;
            //    }
            //}
            //private string _exactFileName = string.Empty;
            //public string ExactFileName
            //{
            //    get
            //    {
            //        return _exactFileName;
            //    }
            //    set
            //    {
            //        _exactFileName = value;
            //    }
            //}
            //private string _fileNameLabel = string.Empty;
            //public string FileNameLabel
            //{
            //    get
            //    {
            //        return _fileNameLabel;
            //    }
            //    set
            //    {
            //        _fileNameLabel = value;
            //    }
            //}
            //private string _deleteAllLabel = string.Empty;
            //public string DeleteAllLabel
            //{
            //    get
            //    {
            //        return _deleteAllLabel;
            //    }
            //    set
            //    {
            //        _deleteAllLabel = value;
            //    }
            //}
            //private string _deleteAllFromProcassoAndListLabel = string.Empty;
            //public string DeleteAllFromProcassoAndListLabel
            //{
            //    get
            //    {
            //        return _deleteAllFromProcassoAndListLabel;
            //    }
            //    set
            //    {
            //        _deleteAllFromProcassoAndListLabel = value;
            //    }
            //}
            //private int _currentProcessingCaseNumberID = 0;
            //public int CurrentProcessingCaseNumberID
            //{
            //    get
            //    {
            //        return _currentProcessingCaseNumberID;
            //    }
            //    set
            //    {
            //        _currentProcessingCaseNumberID = value;
            //    }
            //}
            
            //private int _numberOfFiles = 0;
            //public int NumberOfFiles
            //{
            //    get
            //    {
            //        return _numberOfFiles;
            //    }
            //    set
            //    {
            //        _numberOfFiles = value;
            //    }
            //}
        
    }
}
