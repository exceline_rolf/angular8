﻿using System;
using System.Runtime.Serialization;

namespace US.Payment.Core.SystemObjects.CustomSetting
{
    [DataContract]
    public class USPCustomSettingMetaData
    {
       
        public USPCustomSettingMetaData()
        {
            ID = -1;
            EntityID = -1;
            EntityName = string.Empty;
            Name = string.Empty;
            DisplayName = string.Empty;
            DataTypeID = -1;
            DefaultValue = string.Empty;
            CreateUserID = string.Empty;
            ModifiedUserID = string.Empty;
            CreateDate = DateTime.MinValue;
            ModifiedDate = DateTime.MinValue;
            Hit = string.Empty;

        }
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int EntityID { get; set; }
        [DataMember]
        public string EntityName { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public int DataTypeID { get; set; }
        [DataMember]
        public string DefaultValue { get; set; }
        [DataMember]
        public string CreateUserID { get; set; }
        [DataMember]
        public string ModifiedUserID { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string Hit { get; set; }
    }
}
