﻿
using System.Runtime.Serialization;
namespace US.Payment.Core.SystemObjects.CustomSetting
{
    [DataContract]
    public class USPCustomSettingEntity
    {
        public USPCustomSettingEntity()
        {
            ID = -1;
            EntityName = string.Empty;
            Description = string.Empty;
        }
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string EntityName { get; set; }
        [DataMember]
        public string Description { get; set; }

    }
}
