﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.SystemObjects
{
    public class USPParameter
    {
        public string ID { get; set; }
        public string DisplayName { get; set; }
        public string ParameterType { get; set; }
        public object Value { get; set; }
    }
}
