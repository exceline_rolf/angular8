﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Exceptions
{
    public class USPInvalidCreditorException : Exception
    {
        public USPInvalidCreditorException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
