﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.Core.Workflows
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 30/06/2011 HH:MM  PM
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.Workflows;

namespace US.Payment.Core.Workflows
{
    public class USActivity
    {
        private string _activityName = string.Empty;
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private string _workflowName = string.Empty;
        public string WorkflowName
        {
            get { return _workflowName; }
            set { _workflowName = value; }
        }

        private string _workflowCategory = string.Empty;
        public string WorkflowCategory
        {
            get { return _workflowCategory; }
            set { _workflowCategory = value; }
        }

        private WorkflowTypes _workflowType;
        public WorkflowTypes WorkflowType
        {
            get { return _workflowType; }
            set { _workflowType = value; }
        }

        private List<string> _entityTypes = new List<string>();
        public List<string> EntityTypes
        {
            get { return _entityTypes; }
            set { _entityTypes = value; }
        }

        private DBSettings _dbSetting = null;
        public DBSettings DbSettings
        {
            get { return _dbSetting; }
            set { _dbSetting = value; }
        }
    }
}
