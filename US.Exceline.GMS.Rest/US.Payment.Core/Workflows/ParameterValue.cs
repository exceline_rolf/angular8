﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.WorkflowController.DomainObjects.Activity
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 24/06/2011 HH:MM  PM
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Workflows
{
    public class ParameterValue
    {
        private string _entityProperty = "None";
        public string EntityProperty
        {
            get { return _entityProperty; }
            set { _entityProperty = value; }
        }

        private string _fixedValue = "None";
        public string FixedValue
        {
            get { return _fixedValue; }
            set { _fixedValue = value; }
        }
    }
}
