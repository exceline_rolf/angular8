﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPOrderLine
    {
        [DataMember]
        int OrderLineID { get; set; }
        [DataMember]
        int ArItemNo{ get; set; }
        [DataMember]
        string InkassoId{ get; set; }
        [DataMember]
        string CutId{ get; set; }
        [DataMember]
        string LastName{ get; set; }
        [DataMember]
        string FirstName{ get; set; }
        [DataMember]
        string NameOnContract{ get; set; }
        [DataMember]
        string Address{ get; set; }
        [DataMember]
        string ZipCode{ get; set; }
        [DataMember]
        string PostPlace{ get; set; }
        [DataMember]
        string DiscountAmount{ get; set; }
        [DataMember]
        string EmployeeNo{ get; set; }
        [DataMember]
        string SponsorRef{ get; set; }
        [DataMember]
        int Visit{ get; set; }
        [DataMember]
        string LastVisit{ get; set; }
        [DataMember]
        string Amount{ get; set; }
        [DataMember]
        string AmountThisOrderLines{ get; set; }
        [DataMember]
        bool IsErrorOccurred{ get; set; }
        [DataMember]
        string ErrorMessage { get; set; }
        


    }
}
