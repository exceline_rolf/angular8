﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPLanguage
    {
        string LanId{ get; set; }
        string LanName { get; set; }
        
    }
}
