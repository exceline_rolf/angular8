﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.DomainObjects.RutineAndActivities
{
    [DataContract]
   public class USPCategory
    {
        [DataMember]
        public string Name { get; set; }
        public override string ToString()
        {
            return this.Name;
        }

        private int _id = -1;
         [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

    }
}
