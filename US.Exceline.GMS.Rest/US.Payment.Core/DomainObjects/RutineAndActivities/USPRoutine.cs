﻿using System.Runtime.Serialization;
using System.Runtime.Serialization;

namespace US.Payment.Core.DomainObjects.RutineAndActivities
{
    [DataContract]
    public class USPRoutine
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        
        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _categoryName = string.Empty;
        [DataMember]
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }
        private string _category = string.Empty;
        [DataMember]
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }
        private string _xamlContent = string.Empty;
        [DataMember]
        public string XamlContent
        {
            get { return _xamlContent; }
            set { _xamlContent = value; }
        }

        private string _workflowType = string.Empty;
        [DataMember]
        public string WorkflowType
        {
            get { return _workflowType; }
            set { _workflowType = value; }
        }

        private bool _status = false;
        [DataMember]
        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }
      
    }
}
