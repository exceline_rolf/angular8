﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPPaymentFromProcasso
    {
        string InkassoID{ get; set; }
       string CustID{ get; set; }
       string Ref{ get; set; }
       string ProcassoTransID{ get; set; }
        decimal Amount{ get; set; }
       string KID{ get; set; }
       string Txt{ get; set; }
        DateTime VoucherDate{ get; set; }
       DateTime DueDate{ get; set; }
       DateTime PayDate{ get; set; }
       bool ErrorOccured{ get; set; }
        string ErrorMessage{ get; set; }
        int SavedARItemNo { get; set; }
    }
}
