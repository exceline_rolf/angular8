﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPDebtorEntity
    {
        string Name{ get; set; }
        List<IUSPAddress> Address{ get; set; }
       string telWork{ get; set; }
       string telMobile{ get; set; }
       int CreditorCount{ get; set; }
        string EntNo{ get; set; }
        string PersonNo { get; set; }
        

    }
}
