﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPARItem
    {
       string InvoiceNumber{ get; set;}
       string Text { get; set; }
       DateTime InvoicedDate { get; set; }
       DateTime DueDate { get; set; }
       decimal Amount { get; set; }
       int ARNo { get; set; }
       string itemType { get; set; }
       string Ref { get; set; }
       DateTime RegDate { get; set; }
       string RFee { get; set; }
       decimal Balance { get; set; }
       string KID { get; set; }
       int Delayed { get; set; }
       string ARRole { get; set; }
       int CreEnt { get; set; }
       int DebEnt { get; set; }
       string Paid { get; set; }
       string incasso { get; set; }
       string cusID { get; set; }
       int subCaseNo { get; set; }
       int PaymentId { get; set; }
       string ExternalTarnsactionNumber { get; set; }
       int CaseNo { get; set; }
       int ARItemNo { get; set; }
       int DStatus { get; set; }
       int ItemTypeID { get; set; }
       int Notified { get; set; }
       string CancelDate { get; set; }
       string payDate { get; set; }
       DateTime TransferDate { get; set; }
       string ExtTransNo { get; set; }
       bool IsErrorOcurred { get; set; }
       string ErrorMessage { get; set; }
       int NoOfTotalInvoices { get; set; }
       int NoOfLostInvoices { get; set; }
       int NoOFNewInvoices { get; set; }
       List<IUSPOrderLine> OrderLineList { get; set; }
    }
}
