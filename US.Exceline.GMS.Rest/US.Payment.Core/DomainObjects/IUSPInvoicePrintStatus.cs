﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPInvoicePrintStatus
    {
        int ARItemNo { get; set; }
        string InvoicePath { get; set; }
    }
}
