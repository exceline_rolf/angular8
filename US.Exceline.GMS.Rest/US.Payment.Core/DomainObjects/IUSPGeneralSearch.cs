﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPGeneralSearch
    {
        string CusId { get; set; }
        string CreditorName { get; set; }
        string DebtorName { get; set; }
        string RoleId { get; set; }
        string KID { get; set; }
        string Amount { get; set; }
        string Hit { get; set; }
        string Ref { get; set; }
        int PID { get; set; }
        int CreditorEntNo { get; set; }
        int DebtorEntNo { get; set; }
    }
}
