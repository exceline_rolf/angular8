﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Enums
{
    public enum CreditorGroupTypes
    {
        ALL,
        CREDIORINVOICE,
        VATINVOICE,
        REMMIT
    }
}
