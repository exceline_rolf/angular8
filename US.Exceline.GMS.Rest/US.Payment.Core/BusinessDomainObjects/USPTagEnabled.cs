﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
   public class USPTagEnabled:ITagEnabled
    {
    #region ITagEnabled Members


private string  _Tag1;
public string  Tag1
{
	  get 
	{ 
		 return _Tag1; 
	}
	  set 
	{ 
		_Tag1 = value;
	}
}


private string  _Tag2;
public string  Tag2
{
	  get 
	{ 
		 return _Tag2; 
	}
	  set 
	{ 
		_Tag2 = value;
	}
}


private  string  _Tag3;
public string  Tag3
{
	  get 
	{
        return _Tag3; 
	}
	  set 
	{
        _Tag3 = value;
	}
}

#endregion
}
}
