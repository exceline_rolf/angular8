﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPNewOrCancelledAssignment : IUSPNewOrCancelledAssignment
    {
    #region IUSPNewOrCancelledAssignment Members


private  int  _ID=-1;
public int  ID
{
	  get 
	{ 
		 return _ID; 
	}
	  set 
	{ 
		_ID = value;
	}
}


private  string  _InkassoID=string.Empty;
public string  InkassoID
{
	  get 
	{ 
		 return _InkassoID; 
	}
	  set 
	{ 
		_InkassoID = value;
	}
}


private  string  _BNumber=string.Empty;
public string  BNumber
{
	  get 
	{ 
		 return _BNumber; 
	}
	  set 
	{ 
		_BNumber = value;
	}
}


private  string  _CID=string.Empty;
public string  CID
{
	  get 
	{ 
		 return _CID; 
	}
	  set 
	{ 
		_CID = value;
	}
}


private  string  _KID=string.Empty;
public string  KID
{
	  get 
	{ 
		 return _KID; 
	}
	  set 
	{ 
		_KID = value;
	}
}


private  string  _Status=string.Empty;
public string  Status
{
	  get 
	{ 
		 return _Status; 
	}
	  set 
	{ 
		_Status = value;
	}
}


private  string  _Date=string.Empty;
public string  Date
{
	  get 
	{ 
		 return _Date; 
	}
	  set 
	{ 
		_Date = value;
	}
}


private  string  _Row=string.Empty;
public string  Row
{
	  get 
	{
        return _Row; 
	}
	  set 
	{
        _Row = value;
	}
}

#endregion
}
}
