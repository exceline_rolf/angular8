﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR
{
   public  interface IUSPOCRRecord
    {

        IUSPOCRStartTransmissionRecord StartSendingReocrd
       {
           get;
           set;
       }

       

         List<IUSPOCRAssignmentRecord> AssignmentRecords
        {
            get ; 
            set ; 
        }

      

         IUSPOCREndTransmissionRecord EndSendingRecord
        {
            get  ; 
            set ;
        }
    }
}
