﻿// --------------------------------------------------------------------------
// Copyright(c) 2008 UnicornSolutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US Payment
// Coding Standard   : US Coding Standards
// Author            : MRA
// Created Timestamp : 25/11/2009 10:30  AM
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.BusinessDomainObjects.OCR;

namespace US.Payment.Core.BusinessDomainObjects.OCR.OCRConcreteObjects
{
    public class OCRStartTransmissionRecord : IUSPOCRStartTransmissionRecord
    {

        #region IUSPOCRStartTransmissionRecord Members

        public string FormatCode
        {
            get;
            set; 
        }

        public string ServiceCode
        {
            get;
            set; 
        }

        public string SendingType
        {
            get;
            set; 
        }

        public US.Payment.Core.Enums.OCRRecordTypes RecordType
        {
            get;
            set; 
        }

        public string DataSender
        {
            get;
            set; 
        }

        public string SendingNumber
        {
            get;
            set; 
        }

        public string DataRecipient
        {
            get;
            set; 
        }

        #endregion
    }
}
