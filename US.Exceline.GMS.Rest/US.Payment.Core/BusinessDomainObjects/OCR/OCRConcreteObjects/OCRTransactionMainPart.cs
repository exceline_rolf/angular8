﻿// --------------------------------------------------------------------------
// Copyright(c) 2008 UnicornSolutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US Payment
// Coding Standard   : US Coding Standards
// Author            : MRA
// Created Timestamp : 25/11/2009 10:30  AM
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.Enums;

namespace US.Payment.Core.BusinessDomainObjects.OCR.OCRConcreteObjects
{
    public class OCRTransactionMainPart
    {
        private string _formatCode;
        /// <summary>
        /// Format Code. Always NY.
        /// Index of OCR file : 1-2
        /// </summary>
        public string FormatCode
        {
            get { return _formatCode; }
            set { _formatCode = value; }
        }

        private string _serviceCode;
        /// <summary>
        /// Service Code. 
        /// Index of OCR file : 3-4
        /// </summary>
        public string ServiceCode
        {
            get { return _serviceCode; }
            set { _serviceCode = value; }
        }
        private string _transType;
        /// <summary>
        /// Sending Type.
        /// Index of OCR file : 5-6
        /// </summary>
        public string TransType
        {
            get { return _transType; }
            set { _transType = value; }
        }

        private OCRRecordTypes _recordType;
        /// <summary>
        /// Record type.
        /// Index of OCR file : 7-8
        /// </summary>
        public OCRRecordTypes RecordType
        {
            get { return _recordType; }
            set { _recordType = value; }
        }

        private string _transNumber;
        /// <summary>
        /// Agreement ID
        /// Index of OCR file : 9-15
        /// </summary>
        public string TransNumber
        {
            get { return _transNumber; }
            set { _transNumber = value; }
        }
    }
}
