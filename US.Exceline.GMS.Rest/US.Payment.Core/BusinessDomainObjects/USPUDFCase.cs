﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPUDFCase : IUSPUDFCase
    {
    #region IUSPUDFCase Members


private  int  _USPSubCaseNo=-1;
public int  USPSubCaseNo
{
	  get 
	{ 
		 return _USPSubCaseNo; 
	}
	  set 
	{ 
		_USPSubCaseNo = value;
	}
}


private  string  _CaseNumber=string.Empty;
public string  CaseNumber
{
	  get 
	{ 
		 return _CaseNumber; 
	}
	  set 
	{ 
		_CaseNumber = value;
	}
}


private  string  _CaseReportGroup=string.Empty;
public string  CaseReportGroup
{
	  get 
	{ 
		 return _CaseReportGroup; 
	}
	  set 
	{ 
		_CaseReportGroup = value;
	}
}


private  string  _CaseStatus=string.Empty;
public string  CaseStatus
{
	  get 
	{ 
		 return _CaseStatus; 
	}
	  set 
	{ 
		_CaseStatus = value;
	}
}


private  string  _CaseType=string.Empty;
public string  CaseType
{
	  get 
	{ 
		 return _CaseType; 
	}
	  set 
	{ 
		_CaseType = value;
	}
}


private  string  _CID=string.Empty;
public string  CID
{
	  get 
	{ 
		 return _CID; 
	}
	  set 
	{ 
		_CID = value;
	}
}


private  IUSPAddress  _Address=new USPAddress();
public IUSPAddress  Address
{
	  get 
	{ 
		 return _Address; 
	}
	  set 
	{ 
		_Address = value;
	}
}


private  string  _CBirthdate=string.Empty;
public string  CBirthdate
{
	  get 
	{ 
		 return _CBirthdate; 
	}
	  set 
	{ 
		_CBirthdate = value;
	}
}


private  string  _CDebtype=string.Empty;
public string  CDebtype
{
	  get 
	{ 
		 return _CDebtype; 
	}
	  set 
	{ 
		_CDebtype = value;
	}
}


private  string  _CFirstName=string.Empty;
public string  CFirstName
{
	  get 
	{ 
		 return _CFirstName; 
	}
	  set 
	{ 
		_CFirstName = value;
	}
}


private  string  _CHome=string.Empty;
public string  CHome
{
	  get 
	{ 
		 return _CHome; 
	}
	  set 
	{ 
		_CHome = value;
	}
}


private  string  _CLastname=string.Empty;
public string  CLastname
{
	  get 
	{ 
		 return _CLastname; 
	}
	  set 
	{ 
		_CLastname = value;
	}
}


private  string  _CMail=string.Empty;
public string  CMail
{
	  get 
	{ 
		 return _CMail; 
	}
	  set 
	{ 
		_CMail = value;
	}
}


private  string  _CMobile=string.Empty;
public string  CMobile
{
	  get 
	{ 
		 return _CMobile; 
	}
	  set 
	{ 
		_CMobile = value;
	}
}


private  string  _COrganisation=string.Empty;
public string  COrganisation
{
	  get 
	{ 
		 return _COrganisation; 
	}
	  set 
	{ 
		_COrganisation = value;
	}
}


private  string  _CSosialsecurity=string.Empty;
public string  CSosialsecurity
{
	  get 
	{ 
		 return _CSosialsecurity; 
	}
	  set 
	{ 
		_CSosialsecurity = value;
	}
}


private  string  _CWork=string.Empty;
public string  CWork
{
	  get 
	{ 
		 return _CWork; 
	}
	  set 
	{ 
		_CWork = value;
	}
}


private  string  _DebtWarningDate=string.Empty;
public string  DebtWarningDate
{
	  get 
	{ 
		 return _DebtWarningDate; 
	}
	  set 
	{ 
		_DebtWarningDate = value;
	}
}


private  bool  _IsSuccess=false;
public bool  IsSuccess
{
	  get 
	{ 
		 return _IsSuccess; 
	}
	  set 
	{ 
		_IsSuccess = value;
	}
}


private  string  _KID=string.Empty;
public string  KID
{
	  get 
	{ 
		 return _KID; 
	}
	  set 
	{ 
		_KID = value;
	}
}


private  string  _PayNoteDate=string.Empty;
public string  PayNoteDate
{
	  get 
	{ 
		 return _PayNoteDate; 
	}
	  set 
	{ 
		_PayNoteDate = value;
	}
}


private  string  _PID=string.Empty;
public string  PID
{
	  get 
	{ 
		 return _PID; 
	}
	  set 
	{ 
		_PID = value;
	}
}


private  List<IUSPUDFTransaction>  _Transactions=new List<IUSPUDFTransaction>();
public List<IUSPUDFTransaction>  Transactions
{
	  get 
	{
        return _Transactions; 
	}
	  set 
	{
        _Transactions = value;
	}
}

#endregion
}
}
