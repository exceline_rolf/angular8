﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USUDFTransferedResult : IUSUDFTransferedResult, ITransactionResult
    {
    #region IUSUDFTransferedResult Members


private  string  _ExternalCaseID=string.Empty;
public string  ExternalCaseID
{
	  get 
	{ 
		 return _ExternalCaseID; 
	}
	  set 
	{ 
		_ExternalCaseID = value;
	}
}


private  List<ITransactionResult>  _TransactionResults=new List<ITransactionResult>();
public List<ITransactionResult>  TransactionResults
{
	  get 
	{
        return _TransactionResults; 
	}
	  set 
	{
        _TransactionResults = value;
	}
}

#endregion

#region ITransactionResult Members


private  int  _ARItemNo=-1;
public int  ARItemNo
{
	  get 
	{ 
		 return _ARItemNo; 
	}
	  set 
	{ 
		_ARItemNo = value;
	}
}


private  string  _ExternalTransID=string.Empty;
public string  ExternalTransID
{
	  get 
	{
        return _ExternalTransID; 
	}
	  set 
	{
        _ExternalTransID = value;
	}
}

#endregion
}
}
