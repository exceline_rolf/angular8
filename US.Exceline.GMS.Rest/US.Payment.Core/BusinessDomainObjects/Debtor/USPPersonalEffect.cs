﻿using System;
using System.Net;
using System.Windows;
using System.Runtime.Serialization;
using US.Payment.Core.BusinessDomainObjects.BasicData;


namespace US.Payment.Core.BusinessDomainObjects.Debtor
{
    [DataContract]
    public class USPPersonalEffect
    {
        private string  _registrationID = string.Empty;
        [DataMember]
        public string RegistrationID
        {
            get { return _registrationID; }
            set { _registrationID = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _address = string.Empty;
        [DataMember]
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        private PostalArea _postArea = new PostalArea();
        [DataMember]
        public PostalArea PostArea
        {
            get { return _postArea; }
            set { _postArea = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private DateTime _valuationDate;
        [DataMember]
        public DateTime ValuationDate
        {
            get { return _valuationDate; }
            set { _valuationDate = value; }
        }

        private decimal _valuationAmount = -1;
        [DataMember]
        public decimal ValuationAmount
        {
            get { return _valuationAmount; }
            set { _valuationAmount = value; }
        }

        private decimal _estimationValue = -1;
        [DataMember]
        public decimal EstimationValue
        {
            get { return _estimationValue; }
            set { _estimationValue = value; }
        }

        private int _debtorNo = -1;
        [DataMember]
        public int DebtorNo
        {
            get { return _debtorNo; }
            set { _debtorNo = value; }
        }

        private Municipality _municipality = new Municipality();
        [DataMember]
        public Municipality Municipality
        {
            get { return _municipality; }
            set { _municipality = value; }
        }

        private int _seriesNo = -1;
        [DataMember]
        public int SeriesNo
        {
            get { return _seriesNo; }
            set { _seriesNo = value; }
        }

        private int _id = -1;
         [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
         private decimal _amount = 0;
         [DataMember]
         public decimal Amount
         {
             get { return _amount; }
             set { _amount = value; }
         }

         private int _priority = -1;
        [DataMember]
         public int Priority
         {
             get { return _priority; }
             set { _priority = value; }
         }


    }
}
