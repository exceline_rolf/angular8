﻿using System;
using System.Runtime.Serialization;
using US.Payment.Core.BusinessDomainObjects.BasicData;

namespace US.Payment.Core.BusinessDomainObjects.Debtor
{
    [DataContract]
    public class USPVehicle
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private decimal _valuationAmount = -1;
        [DataMember]
        public decimal ValuationAmount
        {
            get { return _valuationAmount; }
            set { _valuationAmount = value; }
        }

        private DateTime _valuationDate;
        [DataMember]
        public DateTime ValuationDate
        {
            get { return _valuationDate; }
            set { _valuationDate = value; }
        }

        private decimal _estimationValue = -1;
        [DataMember]
        public decimal EstimationValue
        {
            get { return _estimationValue; }
            set { _estimationValue = value; }
        }

        private string _registrationNo = string.Empty;
        [DataMember]
        public string RegistrationNo
        {
            get { return _registrationNo; }
            set { _registrationNo = value; }
        }

        private DateTime _registrationDate;
        [DataMember]
        public DateTime RegistrationDate
        {
            get { return _registrationDate; }
            set { _registrationDate = value; }
        }

        private string _yearModel = string.Empty;
        [DataMember]
        public string YearModel
        {
            get { return _yearModel; }
            set { _yearModel = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int _debtorEntNo = -1;
        [DataMember]
        public int DebtorEntNo
        {
            get { return _debtorEntNo; }
            set { _debtorEntNo = value; }
        }

        private string _countryCode = string.Empty;
        [DataMember]
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }

        private decimal _amount = 0;

        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private string _debtorName = string.Empty;
        [DataMember]
        public string DebtorName
        {
            get { return _debtorName; }
            set { _debtorName = value; }
        }

        private Municipality _municipality = new Municipality();
        [DataMember]
        public Municipality Municipality
        {
            get { return _municipality; }
            set { _municipality = value; }
        }


    }

}
