﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.Payment.Core.BusinessDomainObjects.BasicData;

namespace US.Payment.Core.BusinessDomainObjects.Debtor
{
    [DataContract]
    public class USPSalary
    {       
        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private DateTime _fromDate = DateTime.MinValue;
        [DataMember]
        public DateTime FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }

        private DateTime _toDate = DateTime.MinValue;
        [DataMember]
        public DateTime ToDate
        {
            get { return _toDate; }
            set { _toDate = value; }
        }

        private int _debtorNo = -1;
        [DataMember]
        public int DebtorNo
        {
            get { return _debtorNo; }
            set { _debtorNo = value; }
        }

        private Municipality _municipality = new Municipality();
        [DataMember]
        public Municipality Municipality
        {
            get { return _municipality; }
            set { _municipality = value; }
        }
 
        private int _happenFrequency = -1;
        [DataMember]
        public int HappenFrequency
        {
            get { return _happenFrequency; }
            set { _happenFrequency = value; }
        }

        private int _employerNo = -1;
        [DataMember]
        public int EmployerNo
        {
            get { return _employerNo; }
            set { _employerNo = value; }
        }

        private string _employerName = string.Empty;
        [DataMember]
        public string EmployerName
        {
            get { return _employerName; }
            set { _employerName = value; }
        }

        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private decimal _amount = 0;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private Int16 _priority = -1;
        [DataMember]
        public Int16 Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        private string _debtorName = string.Empty;
        [DataMember]
        public string DebtorName
        {
            get { return _debtorName; }
            set { _debtorName = value; }
        }
   
    }
}
