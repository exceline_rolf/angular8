﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPDPHistory : IUSPDPHistory
    {
    #region IUSPDPHistory Members


private  string  _FileName=string.Empty;
public string  FileName
{
	  get 
	{ 
		 return _FileName; 
	}
	  set 
	{ 
		_FileName = value;
	}
}


private  int  _SMrecodes=0;
public int  SMrecodes
{
	  get 
	{ 
		 return _SMrecodes; 
	}
	  set 
	{ 
		_SMrecodes = value;
	}
}


private int  _DBrecodes=0;
public int  DBrecodes
{
	  get 
	{ 
		 return _DBrecodes; 
	}
	  set 
	{ 
		_DBrecodes = value;
	}
}


private int  _errorRecodes=0;
public int  errorRecodes
{
	  get 
	{ 
		 return _errorRecodes; 
	}
	  set 
	{ 
		_errorRecodes = value;
	}
}

#endregion

#region ITagEnabled Members


private string  _Tag1=string.Empty;
public string  Tag1
{
	  get 
	{ 
		 return _Tag1; 
	}
	  set 
	{ 
		_Tag1 = value;
	}
}


private string  _Tag2=string.Empty;
public string  Tag2
{
	  get 
	{ 
		 return _Tag2; 
	}
	  set 
	{ 
		_Tag2 = value;
	}
}


private string  _Tag3=string.Empty;
public string  Tag3
{
	  get 
	{
        return _Tag3; 
	}
	  set 
	{
        _Tag3 = value;
	}
}

#endregion
}
}
