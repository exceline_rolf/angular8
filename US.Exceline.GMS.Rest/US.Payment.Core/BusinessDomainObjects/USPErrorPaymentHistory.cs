﻿// --------------------------------------------------------------------------
// Copyright(c) 2008 UnicornSolutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.Data
// Coding Standard   : US Coding Standards
// Author            : AAB
// Created Timestamp : 21/06/2011 
// --------------------------------------------------------------------------
using System.Runtime.Serialization;
using System;

namespace US.Payment.Core.BusinessDomainObjects
{
    [DataContract]
    public class USPErrorPaymentHistory : USPErrorPayment
    {
        public USPErrorPaymentHistory(): base()
        {
            ErrorPaymentID = -1;
            IsDeleted = 0;
        
        }
        /// <summary>
        /// Error Payament reference id
        /// </summary>
        [DataMember]
        public int ErrorPaymentID { get; set; }
        /// <summary>
        /// Error payment history delete state
        /// </summary>
        [DataMember]
        public int IsDeleted { get; set; }
        /// <summary>
        /// Payment due date 
        /// </summary>
        [DataMember]
        public DateTime DueDate { get; set; }
        /// <summary>
        /// Payment text
        /// </summary>
        [DataMember]
        public string Text{get;set;}
        /// <summary>
        /// Payment Source file name
        /// </summary>
        [DataMember]
        public string FileName { get; set; }
        /// <summary>
        /// ARitem no or Error payment id
        /// </summary>
        [DataMember]
        public int ReferenceId { get; set; }

        /// <summary>
        /// If IsError =1 then its Error Payment else its ArItem
        /// </summary>
        [DataMember]
        public int IsError { get; set; }



    }
}
