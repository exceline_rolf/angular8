﻿
// --------------------------------------------------------------------------
// Copyright(c) 2011 UnicornSolutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.Core
// Coding Standard   : US Coding Standards
// Author            : AAB
// Created Timestamp : 03/08/2011 
// --------------------------------------------------------------------------
using System;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects
{
    [DataContract]
    public class USPErrorPayment
    {
        public USPErrorPayment()
        {
            ID = -1;
            Amount = 0.0M;
            RegDate = DateTime.MinValue;
            Source = string.Empty;
            Ref = string.Empty;
            KID = string.Empty;
            Description = string.Empty;
            NotifiedDate = DateTime.MinValue;
            AccountNo = string.Empty;
            InkassoId = string.Empty;
            CustId = string.Empty;
            VoucherDate = DateTime.MinValue;
            IsMatched = false;
            IsEnabledViewButton = true;
            ARItemMatched = string.Empty;
            _isEnabledIgnoreButton = true;
        }
        /// <summary>
        /// Error payment unique id
        /// </summary>
        [DataMember]
        public int ID { get; set; }
        /// <summary>
        /// Error payment amount
        /// </summary>
        [DataMember]
        public decimal Amount { get; set; }
        /// <summary>
        /// Error payment date
        /// </summary>
        [DataMember]
        public DateTime VoucherDate { get; set; }
        /// <summary>
        /// Error payment source e.g. OCR,DP etc
        /// </summary>
        public string Source { get; set; }
        /// <summary>
        /// Invoice Number
        /// </summary>
        [DataMember]
        public string Ref { get; set; }
        /// <summary>
        /// Customer Identification number
        /// </summary>
        [DataMember]
        public string KID { get; set; }
        /// <summary>
        /// Error payment description ( reason which payment not registerd to system)
        /// </summary>
        [DataMember]
        public string Description { get; set; }
        /// <summary>
        /// System will send notifications to source systems regarding error payment. Notification send date.
        /// </summary>
        [DataMember]
        public DateTime NotifiedDate { get; set; }
        /// <summary>
        /// Creditor account number 
        /// </summary>        
        [DataMember]
        public string AccountNo { get; set; }
        /// <summary>
        /// Creditor number
        /// </summary>
        [DataMember]
        public string InkassoId { get; set; }
        /// <summary>
        /// Creditor name
        /// </summary>
        [DataMember]
        public string CreditorName { get; set; }
        /// <summary>
        /// Customer number
        /// </summary>
        [DataMember]
        public string CustId { get; set; }
        /// <summary>
        /// Error payment register date to system.
        /// </summary>
        [DataMember]
        public DateTime RegDate { get; set; }
        /// <summary>
        /// Error message in Error payment object
        /// </summary>
        [DataMember]
        public string ErrorMessage { get; set; }
        /// <summary>
        /// Error payment error state
        /// </summary>
        [DataMember]
        public int IsError { get; set; }
        /// <summary>
        /// Item type
        /// </summary>
        [DataMember]
        public int ItemType { get; set; }
        /// <summary>
        /// Map Status
        /// </summary>
        [DataMember]
        public int MapStatus { get; set; }
        /// <summary>
        /// matched ArItem No
        /// </summary>
        [DataMember]
        public string ARItemMatched { get; set; }
        /// <summary>
        /// Creditor No
        /// </summary>

        private string _creditor = string.Empty;
        [DataMember]
        public string Creditor
        {
            get { return _creditor; }
            set { _creditor = value; }
        }

        private int _subCaseNo = -1;
        [DataMember]
        public int SubCaseNo
        {
            get { return _subCaseNo; }
            set { _subCaseNo = value; }
        }


        #region Operation buttons on grid
        /// <summary>
        /// use for button caption w.r.t errorpayment type
        /// </summary>
        private string _errorPaymentOperationName = string.Empty;
        [DataMember]
        public string ErrorPaymentOperationName
        {
            get { return _errorPaymentOperationName; }
            set { _errorPaymentOperationName = value; }
        }
        /// <summary>
        /// use for groping by errorpayment type
        /// </summary>
        private string _errorPaymentType = string.Empty;
        [DataMember]
        public string ErrorPaymentType
        {
            get { return _errorPaymentType; }
            set { _errorPaymentType = value; }
        }
        /// <summary>
        /// is Matched
        /// </summary>
        private bool _isMatched = false;
        [DataMember]
        public bool IsMatched
        {
            get { return _isMatched; }
            set { _isMatched = value; }
        }
        /// <summary>
        /// Enable View Button
        /// </summary>
        [DataMember]
        public bool IsEnabledViewButton { get; set; }


        /// <summary>
        /// Enable Ignore Button
        /// </summary>
        private bool _isEnabledIgnoreButton = true;
        [DataMember]
        public bool IsEnabledIgnoreButton
        {
            get { return _isEnabledIgnoreButton; }
            set { _isEnabledIgnoreButton = value; }
        }



        [DataMember]
        public bool IsOpButtonEnabled { get; set; }

        [DataMember]
        public bool IsIgnored { get; set; }

        private string _ignoredButtonName = string.Empty;
        [DataMember]
        public string IgnoredButtonName
        {
            get { return _ignoredButtonName; }
            set { _ignoredButtonName = value; }
        }
        #endregion

        #region file details purpose
        private string _fileName = string.Empty;
        [DataMember]
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private DateTime _fileDate = DateTime.MinValue;
        [DataMember]
        public DateTime FileDate
        {
            get { return _fileDate; }
            set { _fileDate = value; }
        }

        private string _fileType = string.Empty;
        [DataMember]
        public string FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }

        private double _totalNoOfRecordsInFile = 0;
        [DataMember]
        public double TotalNoOfRecordsInFile
        {
            get { return _totalNoOfRecordsInFile; }
            set { _totalNoOfRecordsInFile = value; }
        }

        private double _fileTotalAmount = 0;
        [DataMember]
        public double FileTotalAmount
        {
            get { return _fileTotalAmount; }
            set { _fileTotalAmount = value; }
        }

        private double _matchedRecords = 0;
        [DataMember]
        public double MatchedRecords
        {
            get { return _matchedRecords; }
            set { _matchedRecords = value; }
        }

        private double _matchedTotal = 0;
        [DataMember]
        public double MatchedTotal
        {
            get { return _matchedTotal; }
            set { _matchedTotal = value; }
        }

        private double _tooMuchPaidRecords = 0;
        [DataMember]
        public double TooMuchPaidRecords
        {
            get { return _tooMuchPaidRecords; }
            set { _tooMuchPaidRecords = value; }
        }

        private double _tooMuchPaidRecordsTotal = 0;
        [DataMember]
        public double TooMuchPaidRecordsTotal
        {
            get { return _tooMuchPaidRecordsTotal; }
            set { _tooMuchPaidRecordsTotal = value; }
        }

        private double _unKnownReocrds = 0;
        [DataMember]
        public double UnKnownReocrds
        {
            get { return _unKnownReocrds; }
            set { _unKnownReocrds = value; }
        }

        private double _unKnownRecordsTotal = 0;
        [DataMember]
        public double UnKnownRecordsTotal
        {
            get { return _unKnownRecordsTotal; }
            set { _unKnownRecordsTotal = value; }
        }

        private double _mappedErrorPaymentRecords = 0;
        [DataMember]
        public double MappedErrorPaymentRecords
        {
            get { return _mappedErrorPaymentRecords; }
            set { _mappedErrorPaymentRecords = value; }
        }

        private double _mappedErrorPaymentTotal = 0;
        [DataMember]
        public double MappedErrorPaymentTotal
        {
            get { return _mappedErrorPaymentTotal; }
            set { _mappedErrorPaymentTotal = value; }
        }
        private double _ignoredErrorPaymentRecords = 0;

        public double IgnoredErrorPaymentRecords
        {
            get { return _ignoredErrorPaymentRecords; }
            set { _ignoredErrorPaymentRecords = value; }
        }
        private double _ignoredErrorPaymentTotal = 0;

        public double IgnoredErrorPaymentTotal
        {
            get { return _ignoredErrorPaymentTotal; }
            set { _ignoredErrorPaymentTotal = value; }
        }
        #endregion
    }
}
