﻿using System.Runtime.Serialization;
using US.Payment.Core.BusinessDomainObjects.BasicData;

namespace US.Payment.Core.BusinessDomainObjects.Case
{
    [DataContract]
    public class USPOtherPartyType
    {
        private int id = -1;
        [DataMember]
        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        private string typeName = string.Empty;
        [DataMember]
        public string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }
    }
}
