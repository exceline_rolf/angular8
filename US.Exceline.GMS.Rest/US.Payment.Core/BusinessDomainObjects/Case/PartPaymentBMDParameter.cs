﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment 
// Project Name      : US.Payment.PM.Dashboard
// Coding Standard   : US Coding Standards
// Author            : TMA
// Created Timestamp : 3/8/2012 HH:MM  PM
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.Case
{
    [DataContract]
    public class PartPaymentBMDParameter
    {

        private decimal _interest = 0;
        [DataMember]
        public  decimal Interest
        {
            get { return _interest; }
            set { _interest = value; }
        }

        private DateTime _interestCalculationDate = DateTime.MinValue;
        [DataMember]
        public  DateTime InterestCalculationDate
        {
            get { return _interestCalculationDate; }
            set { _interestCalculationDate = value; }
        }

        private decimal _termAmount = 0;
        [DataMember]
        public  decimal TermAmount
        {
            get { return _termAmount; }
            set { _termAmount = value; }
        }

        private decimal _termCharge = 0;
        [DataMember]
        public  decimal TermCharge
        {
            get { return _termCharge; }
            set { _termCharge = value; }
        }

        private DateTime _firstPart = DateTime.MinValue;
        [DataMember]
        public  DateTime FirstPart
        {
            get { return _firstPart; }
            set { _firstPart = value; }
        }

        private decimal _minimum = 0;
        [DataMember]
        public decimal Minimum
        {
            get { return _minimum; }
            set { _minimum = value; }
        }

        private string _settlementProfile = string.Empty;
        [DataMember]
        public  string SettlementProfile
        {
            get { return _settlementProfile; }
            set { _settlementProfile = value; }
        }

        private DateTime _profileToBeStrarted = DateTime.MinValue;
        [DataMember]
        public  DateTime ProfileToBeStrarted
        {
            get { return _profileToBeStrarted; }
            set { _profileToBeStrarted = value; }
        }

        private decimal _partPayment = 0;
        [DataMember]
        public  decimal PartPayment
        {
            get { return _partPayment; }
            set { _partPayment = value; }
        }

        private int _maxNoOfNotPaidInstalment = 0;
        [DataMember]
        public  int MaxNoOfNotPaidInstalment
        {
            get { return _maxNoOfNotPaidInstalment; }
            set { _maxNoOfNotPaidInstalment = value; }
        }

        private int _noOfDatesWaitAfterBreached = 0;
        [DataMember]
        public  int NoOfDatesWaitAfterBreached
        {
            get { return _noOfDatesWaitAfterBreached; }
            set { _noOfDatesWaitAfterBreached = value; }
        }

        private string _header = string.Empty;
        [DataMember]
        public  string Header
        {
            get { return _header; }
            set { _header = value; }
        }

        private string _footer = string.Empty;
        [DataMember]
        public  string Footer
        {
            get { return _footer; }
            set { _footer = value; }
        }

        private int _maxNoOfInstallment = 0;
        [DataMember]
        public int MaxNoOfInstallment
        {
            get { return _maxNoOfInstallment; }
            set { _maxNoOfInstallment = value; }
        }

        private string _balence = string.Empty;
        [DataMember]
        public string Balence
        {
            get { return _balence; }
            set { _balence = value; }
        }        
    }
}
