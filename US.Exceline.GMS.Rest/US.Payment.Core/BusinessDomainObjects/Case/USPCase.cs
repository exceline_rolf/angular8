﻿using System.Runtime.Serialization;
using System;

namespace US.Payment.Core.BusinessDomainObjects.Case
{
    [DataContract]
    public class USPCase
    {
        private int _caseNo = -1;
        [DataMember]
        public int CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }
        private int _ARNo = -1;
        [DataMember]
        public int ARNo
        {
            get { return _ARNo; }
            set { _ARNo = value; }
        }
        private decimal _mainAmount = 0;
        [DataMember]
        public decimal MainAmount
        {
            get { return _mainAmount; }
            set { _mainAmount = value; }
        }
        private decimal _collectionAmount = 0;
        [DataMember]
        public decimal CollectionAmount
        {
            get { return _collectionAmount; }
            set { _collectionAmount = value; }
        }
        private decimal _intrestAmount = 0;
        [DataMember]
        public decimal IntrestAmount
        {
            get { return _intrestAmount; }
            set { _intrestAmount = value; }
        }
        private decimal _courtAmount = 0;
        [DataMember]
        public decimal CourtAmount
        {
            get { return _courtAmount; }
            set { _courtAmount = value; }
        }
        private decimal _otherAmount = 0;
        [DataMember]
        public decimal OtherAmount
        {
            get { return _otherAmount; }
            set { _otherAmount = value; }
        }
        private decimal _paidAmount = 0;
        [DataMember]
        public decimal PaidAmount
        {
            get { return _paidAmount; }
            set { _paidAmount = value; }
        }

        private string _debtorName = string.Empty;
        [DataMember]
        public string DebtorName
        {
            get { return _debtorName; }
            set { _debtorName = value; }
        }
        private int _debtorEntNo = -1;
        [DataMember]
        public int DebtorEntNo
        {
            get { return _debtorEntNo; }
            set { _debtorEntNo = value; }
        }
        private string _debtorCustId = string.Empty;
        [DataMember]
        public string DebtorCustId
        {
            get { return _debtorCustId; }
            set { _debtorCustId = value; }
        }
        private string _creditorNo = string.Empty;
        [DataMember]
        public string CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }
        private string _creditorName = string.Empty;
        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }
        private int _creditorEntNo = -1;
        [DataMember]
        public int CreditorEntNo
        {
            get { return _creditorEntNo; }
            set { _creditorEntNo = value; }
        }

        private decimal _balanace = 0;
        [DataMember]
        public decimal Balanace
        {
            get { return _balanace; }
            set { _balanace = value; }
        }

        private int _state = -1;
        [DataMember]
        public int State
        {
            get { return _state; }
            set { _state = value; }
        }

        private string _profileWorkflow = string.Empty;
        [DataMember]
        public string ProfileWorkflow
        {
            get { return _profileWorkflow; }
            set { _profileWorkflow = value; }
        }
        private string _profileWorkflowcategory = string.Empty;
        [DataMember]
        public string ProfileWorkflowcategory
        {
            get { return _profileWorkflowcategory; }
            set { _profileWorkflowcategory = value; }
        }
        private string _workflowInstnaceId = string.Empty;
        [DataMember]
        public string WorkflowInstnaceId
        {
            get { return _workflowInstnaceId; }
            set { _workflowInstnaceId = value; }
        }

        private string _caseHandler = string.Empty;
        [DataMember]
        public string CaseHandler
        {
            get { return _caseHandler; }
            set { _caseHandler = value; }
        }

        private int _caseHandlerID = -1;
        [DataMember]
        public int CaseHandlerID
        {
            get { return _caseHandlerID; }
            set { _caseHandlerID = value; }
        }
      
        private DateTime _regDate;
        [DataMember]
        public DateTime RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }

        private string _serviceURL = string.Empty;
        [DataMember]
        public string ServiceURL
        {
            get { return _serviceURL; }
            set { _serviceURL = value; }
        }
        private DateTime _lastNoteDate;
        [DataMember]
        public DateTime LastNoteDate
        {
            get { return _lastNoteDate; }
            set { _lastNoteDate = value; }
        }
        private string _lastActivityORRoutine;
        [DataMember]
        public string LastActivityORRoutine
        {
            get { return _lastActivityORRoutine; }
            set { _lastActivityORRoutine = value; }
        }
        private string _nextActivityORRoutine;
        [DataMember]
        public string NextActivityORRoutine
        {
            get { return _nextActivityORRoutine; }
            set { _nextActivityORRoutine = value; }
        }
        private DateTime _lastReminder;
        [DataMember]
        public DateTime LastReminder
        {
            get { return _lastReminder; }
            set { _lastReminder = value; }
        }
        private DateTime _lastInterestCalcDate;
        [DataMember]
        public DateTime LastInterestCalcDate
        {
            get { return _lastInterestCalcDate; }
            set { _lastInterestCalcDate = value; }
        }
 
    }
}
