﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
   public class USPATGFileProcess:IUSPATGFileProcess
    {
    #region IUSPATGFileProcess Members


private List<IUSPCreditor>  _CreditorList=new List<IUSPCreditor>();
public List<IUSPCreditor>  CreditorList
{
	  get 
	{ 
		 return _CreditorList; 
	}
	  set 
	{ 
		_CreditorList = value;
	}
}


private string  _SourceFilePath=string.Empty;
public string  SourceFilePath
{
	  get 
	{ 
		 return _SourceFilePath; 
	}
	  set 
	{ 
		_SourceFilePath = value;
	}
}


private string  _DestinationFilePath=string.Empty;
public string  DestinationFilePath
{
	  get 
	{ 
		 return _DestinationFilePath; 
	}
	  set 
	{ 
		_DestinationFilePath = value;
	}
}


private string  _ProcessPath=string.Empty;
public string  ProcessPath
{
	  get 
	{ 
		 return _ProcessPath; 
	}
	  set 
	{ 
		_ProcessPath = value;
	}
}


private string  _ConnectionString=string.Empty;
public string  ConnectionString
{
	  get 
	{ 
		 return _ConnectionString; 
	}
	  set 
	{ 
		_ConnectionString = value;
	}
}


private string  _FolderPath=string.Empty;
public string  FolderPath
{
	  get 
	{ 
		 return _FolderPath; 
	}
	  set 
	{ 
		_FolderPath = value;
	}
}


private string  _Notificattion=string.Empty;
public string  Notificattion
{
	  get 
	{ 
		 return _Notificattion; 
	}
	  set 
	{ 
		_Notificattion = value;
	}
}


private int  _stateValue=-1;
public int  stateValue
{
	  get 
	{
        return _stateValue; 
	}
	  set 
	{
        _stateValue = value;
	}
}

#endregion
}
}
