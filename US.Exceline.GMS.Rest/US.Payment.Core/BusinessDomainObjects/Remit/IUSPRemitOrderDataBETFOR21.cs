﻿
namespace US.Payment.Core.BusinessDomainObjects.Remit
{
    public interface IUSPRemitOrderDataBETFOR21
    {
        //Positon(Length) 1-40(40)
        //Value =
        string BETFOR21_ApplicationHeader
        {
            get;
            set;
        }

        //Positon(Length) 41-48(8)
        //Value =BETFOR21
        string BETFOR21_TransactionCode
        {
            get;
            set;
        }

        //Positon(Length) 49-59(11)
        //Value =Company Number
        string BETFOR21_EnterpriseNumber
        {
            get;
            set;
        }

        //Positon(Length) 60-70(11)
        //Value =Payer Account 
        string BETFOR21_AccountNumber
        {
            get;
            set;
        }

        //Positon(Length) 71-74(4)
        //Value =auto incremented 1 greater than before record
        string BETFOR21_SequenceControlField
        {
            get;
            set;
        }

        //Positon(Length) 75-80(6)
        //Value =??????
        string BETFOR21_ReferenceNumber
        {
            get;
            set;
        }

        //Positon(Length) 81-86(6)
        //Value =YYDDMM
        string BETFOR21_PaymentDate
        {
            get;
            set;
        }

        //Positon(Length) 87-116(30)
        //Value =Message
        string BETFOR21_OwnRefOrder
        { 
            get; 
            set; 
        }

        //Positon(Length) 117(1)
        //Value =
        string BETFOR21_Reserved
        {
            get;
            set;
        }

        //Positon(Length) 118-128(11)
        //Value =Payee Account if o or empty 00000000019
        string BETFOR21_PayeesAccount
        { 
            get; 
            set;
        }

        //Positon(Length) 129-158(30)
        //Value =Payee Name
        string BETFOR21_PayeesName
        {
            get;
            set;
        }

        //Positon(Length) 159-188(30)
        //Value =addr1
        string BETFOR21_Address1
        {
            get;
            set;
        }

        //Positon(Length) 189-218(30)
        //Value =addr2
        string BETFOR21_Address2
        {
            get;
            set;
        }

        //Positon(Length) 219-222(4)
        //Value =pos
        string BETFOR21_PostalCode
        {
            get;
            set;
        }

        //Positon(Length) 223-248(26)
        //Value =town
        string BETFOR21_Town
        {
            get;
            set;
        }

        //Positon(Length) 249-263(15)
        //Value =Total Amount if trnsfer to own, trans category = 'O'
        string BETFOR21_AmountToOwnAccount
        {
            get;
            set;
        }

        //Positon(Length) 264-266(3)
        //Value =text Code have some Clarifications
        string BETFOR21_TextCode
        {
            get;
            set;
        }

        //Positon(Length) 267(1)
        //Value =F for Invoice Payments /O for to own account -ref page 40
        string BETFOR21_TransactionCategory
        {
            get;
            set;
        }
        
        //Positon(Length) 268(1)
        //Value =Empty
        string BETFOR21_Deletion
        {
            get;
            set;
        }

        //Positon(Length) 269-283(15)
        //Value =TotalAmount
        string BETFOR21_TotalAmount
        {
            get;
            set;
        }

        //Positon(Length) 284-288(5)
        //Value =00000
        string BETFOR21_Reserved1
        {
            get;
            set;
        }

        //Positon(Length) 289-294(6)
        //Value =similar to payment date YYDDMM
        string BETFOR21_ValueDate
        {
            get;
            set;
        }

        //Positon(Length) 295-320(26)
        //Value =Empty
        string BETFOR21_Reserved2
        {
            get;
            set;
        }

    } 

}
