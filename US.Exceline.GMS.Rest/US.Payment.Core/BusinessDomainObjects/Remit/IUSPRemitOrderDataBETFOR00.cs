﻿
namespace US.Payment.Core.BusinessDomainObjects.Remit
{
    public interface IUSPRemitOrderDataBETFOR00
    {
        //Positon(Length) 1-40(40)
        string BETFOR00_ApplicationHeader
        {
            get;
            set;
        }
       
        //41-48(8)
        //Value= BETFOR00
        string BETFOR00_TransactionCode
        {
            get;
            set;
        }

       //49-59(11)
       //Value = company Number
        string BETFOR00_EnterpriseNumber
        {
            get;
            set;
        }

        //60-70(11)
        //Value=Empty
        string BETFOR00_Division
        {
            get;
            set;
        }

        //71-74(4)
        //Value= start with 0001 until 9999 after 0000
        string BETFOR00_SequenceControlField
        { 
            get; 
            set;
        }

        //75-80(6)
        //Value=empty
        string BETFOR00_Reserved1
        {
            get;
            set;
        }

        //81-84(4)
        //Value=MMDD
        string BETFOR00_ProductionDate
        {
            get;
            set;
        }


        //85-94(10)
        //Value=Empty
        string BETFOR00_Password
        {
            get;
            set;
        }

        //95-104(10)
        //Value=VERSION 001/002/003
        string BETFOR00_ProcedureVersion
        { 
            get; 
            set; 
        }

        //105-114(10)                                                                                                         
        //Value=Empty
        string BETFOR00_NewPassword
        {
            get;
            set;
        }

        //115-125(11)
        //Value=Empty
        string BETFOR00_OperatorNumber
        { 
            get;
            set; 
        }

        //126(1)
        //Value=Empty
        string BETFOR00_SealUseSIGILL
        { 
            get;
            set; 
        }

        //127-132(6)
        //Value=Empty
        string BETFOR00_SealDate
        {
            get;
            set;
        }

        //133-152(20)
        //Value=Empty
        string BETFOR00_PartialKey
        {
            get;
            set;
        }

        //153(1)
        //Value=Empty
        string BETFOR00_SealHow
        {
            get;
            set;
        }

        //154-295(142)
        //Value=Empty
        string BETFOR00_Reserved2
        {
            get;
            set;
        }

        //296(1)
        //Value=Empty
        string BETFOR00_SealUseAEGIS
        {
            get;
            set;
        }

        //297-320(24)
        //Value=Empty
        string BETFOR00_Reserved3
        {
            get;
            set;
        }


    }
}
