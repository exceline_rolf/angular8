﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPCreditorReceipt
    {
        private string _itemType = string.Empty;
        public string ItemType
        {
            get { return _itemType; }
            set { _itemType = value; }
        }

        private int _totalNumberOfInvoices = 0;
        public int TotalNumberOfInvoices
        {
            get { return _totalNumberOfInvoices; }
            set { _totalNumberOfInvoices = value; }
        }
        private int _noOfLostInvoices = 0;
        public int NoOfLostInvoices
        {
            get { return _noOfLostInvoices; }
            set { _noOfLostInvoices = value; }
        }
        private int _noOfNewInvoices = 0;
        public int NoOfNewInvoices
        {
            get { return _noOfNewInvoices; }
            set { _noOfNewInvoices = value; }
        }
        private string _cusId = string.Empty;
        public string CusId
        {
            get { return _cusId; }
            set { _cusId = value; }
        }
        private string _debtorName = string.Empty;
        public string DebtorName
        {
            get { return _debtorName; }
            set { _debtorName = value; }
        }
        private string _KID = string.Empty;
        public string KID
        {
            get { return _KID; }
            set { _KID = value; }
        }
        private double _amount = 0;
        public double Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        private int _entNo = 0;
        public int EntNo
        {
            get { return _entNo; }
            set { _entNo = value; }
        }

        
    }
}
