﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.GeneralSearch
{
    [DataContract]
    public class USPCaseSearch
    {
        private int _caseNo = -1;
        [DataMember]
        public int CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }

        private int _caseState = -1;
        [DataMember]
        public int CaseState
        {
            get { return _caseState; }
            set { _caseState = value; }
        }

        private string _kID = string.Empty;
        [DataMember]
        public string KID
        {
            get { return _kID; }
            set { _kID = value; }
        }
        private int _arNo = -1;
        [DataMember]
        public int ArNo
        {
            get { return _arNo; }
            set { _arNo = value; }
        }
        private string _creditorNo = string.Empty;
        [DataMember]
        public string CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }
        private string _creditorName = string.Empty;
        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }
        private string _debtorNo = string.Empty;
        [DataMember]
        public string DebtorNo
        {
            get { return _debtorNo; }
            set { _debtorNo = value; }
        }
        private string _debtorName = string.Empty;
        [DataMember]
        public string DebtorName
        {
            get { return _debtorName; }
            set { _debtorName = value; }
        }
        private string _hit = string.Empty;
        [DataMember]
        public string Hit
        {
            get { return _hit; }
            set { _hit = value; }
        }

        private string _colour;
        [DataMember]
        public string Colour
        {
            get { return _colour; }
            set { _colour = value; }
        }

    }
}


