﻿using System.Runtime.Serialization;
using System.Windows;

namespace US.Payment.Core.BusinessDomainObjects.BasicData
{
    [DataContract]
    public class PostalArea
    {
        private int _id = -1;
        private string _postalPlace = string.Empty;
        private int _countyId = -1;
        private int _cityAreaNo =-1;
        private Municipality _municipality = new Municipality();
        private string _user = string.Empty;
       
        [DataMember]
        public int CountyId
        {
            get { return _countyId; }
            set { _countyId = value; }
        }

        [DataMember]
        public int CityAreaNo
        {
            get { return _cityAreaNo; }
            set { _cityAreaNo = value; }
        }

        private string _postalCode = string.Empty;
        [DataMember]
        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }

        [DataMember]
        public string PostalPlace
        {
            get { return _postalPlace; }
            set { _postalPlace = value; }
        }

        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
         [DataMember]
         public Municipality Municipality
         {
             get { return _municipality; }
             set { _municipality = value; }
         }
         private bool _selected = false;
         [DataMember]
         public bool Selected
         {
             get { return _selected; }
             set { _selected = value; }
         }

        private Visibility _viewEditAndRemoveOption = System.Windows.Visibility.Collapsed;
         [DataMember]
        public Visibility ViewEditAndRemoveOption
        {
            get { return _viewEditAndRemoveOption; }
            set { _viewEditAndRemoveOption = value; }
        }

         [DataMember]
         public string User
         {
             get { return _user; }
             set { _user = value; }
         }
    }
}
