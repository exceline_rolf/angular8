﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment
// Coding Standard   : US Coding Standards
// Author            : GMA
// Created Timestamp : "22/08/2012 17:01:21
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System.Collections.Generic;
using System.Runtime.Serialization;
using System;
using US.Payment.Core.BusinessDomainObjects.BasicData;

namespace US.Payment.Core.BusinessDomainObjects.BasicData
{
    [DataContract]
   public class DepartmentInfo
    {
        private int _id = -1;
        private int _departmentNumber = -1;
        private string _departmentName = string.Empty;
        private string _address1 = string.Empty;
        private string _printerName = string.Empty;
        private string _address2 = string.Empty;
        private PostalArea _postalArea = new PostalArea();
        private Municipality _municipality = new Municipality();
        private string _country = string.Empty;
        private string _feeCentralNumber = string.Empty;
        private string _user = string.Empty;
        private string _telephoneNumber;
        private string _faxNumber = string.Empty;

        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        [DataMember]
        public int DepartmentNumber
        {
            get { return _departmentNumber; }
            set { _departmentNumber = value; }
        }

        [DataMember]
        public string DepartmentName
        {
            get { return _departmentName; }
            set { _departmentName = value; }
        }

        [DataMember]
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }
        
        [DataMember]
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }
        
        [DataMember]
        public PostalArea PostalArea
        {
            get { return _postalArea; }
            set { _postalArea = value; }
        }

        [DataMember]
        public Municipality Municipality
        {
            get { return _municipality; }
            set { _municipality = value; }
        }
       
        [DataMember]
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
    
        [DataMember]
        public string FeeCentralNumber
        {
            get { return _feeCentralNumber; }
            set { _feeCentralNumber = value; }
        }
       
        [DataMember]
        public string PrinterName
        {
            get { return _printerName; }
            set { _printerName = value; }
        }

        [DataMember]
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }
       
        [DataMember]
        public string TelephoneNumber
        {
            get { return _telephoneNumber; }
            set { _telephoneNumber = value; }
        }

        [DataMember]
        public string FaxNumber
        {
            get { return _faxNumber; }
            set { _faxNumber = value; }
        }
    }
}
