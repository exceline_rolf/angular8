﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace US.Payment.Core.ResultNotifications
{
    public enum USMessageTypes
    {
        ERROR,
        WARNING,
        INFO
    }
    public class USOperationResultValue
    {
        private object _tag1 = null;
        public object Tag1
        {
            get { return _tag1; }
            set { _tag1 = value; }
        }

        private object _tag2 = null;
        public object Tag2
        {
            get { return _tag2; }
            set { _tag2 = value; }
        }

        private object _tag3 = null;
        public object Tag3
        {
            get { return _tag3; }
            set { _tag3 = value; }
        }

        private object _tagSavedID = null;
        public object TagSavedID
        {
            get { return _tagSavedID; }
            set { _tagSavedID = value; }
        }

        private bool _errorOccured = false;
        public bool ErrorOccured
        {
            get { return _errorOccured; }
            set { _errorOccured = value; }
        }

        public USOperationResultValue() { }

        private List<USNotificationMessage> _notifications = new List<USNotificationMessage>();
        public ReadOnlyCollection<USNotificationMessage> Notifications
        {
            get { return new ReadOnlyCollection<USNotificationMessage>(_notifications); }
        }

        private void AddNotofications(USNotificationMessage notification)
        {
            if (notification.MessageType == USMessageTypes.ERROR)
            {
                _errorOccured = true;
            }
            _notifications.Add(notification);
        }

        public void AddNotofications(List<USNotificationMessage> notifications)
        {
            foreach (USNotificationMessage notification in notifications)
            {
                if (notification.MessageType == USMessageTypes.ERROR && !(_errorOccured))
                    _errorOccured = true;
                _notifications.Add(notification);
            }
        }

        public void CreateMessage(string message, USMessageTypes messageType)
        {
            if (messageType == USMessageTypes.ERROR)
            {
                _errorOccured = true;
            }
            _notifications.Add(new USNotificationMessage(messageType ,message));
        }
    }
}
