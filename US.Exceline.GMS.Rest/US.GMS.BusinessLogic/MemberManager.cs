﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.Utils;
using US.GMS.Data.DataAdapters;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.BusinessLogic
{
    public class MemberManager
    {
        public static OperationResult<List<OrdinaryMemberDC>> GetMembersForCommonBooking(int branchId, string searchText, string user, string gymCode)
        {
            OperationResult<List<OrdinaryMemberDC>> result = new OperationResult<List<OrdinaryMemberDC>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetMembersForCommonBooking(branchId, searchText, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member list " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<OrdinaryMemberDC> GetMembersById(int branchId, int memberId, string gymCode)
        {
            OperationResult<OrdinaryMemberDC> result = new OperationResult<OrdinaryMemberDC>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetMembersById(branchId, memberId, gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Member " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<List<EntityVisitDC>> GetVistsByDate(DateTime selectedDate, int branchId, string systemName, string gymCode, string user)
        {
            OperationResult<List<EntityVisitDC>> result = new OperationResult<List<EntityVisitDC>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetVistsByDate( selectedDate,  branchId,  systemName,  gymCode, user);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Activities " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }



        public static OperationResult<List<ActivityDC>> GetActivitiesForCommonBooking(int entityId, string entityType, int memberId, string gymCode)
        {
            OperationResult<List<ActivityDC>> result = new OperationResult<List<ActivityDC>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetActivitiesForCommonBooking(entityId, entityType, memberId, gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Activities " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ManageMemberShopAccount(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePointId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = MemberFacade.ManageMemberShopAccount(memberShopAccount, mode, user, branchId, salePointId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in manage member shop account " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<MemberShopAccountDC> GetMemberShopAccounts(int memberId, int hit, string user, bool itemdNeeded, string gymCode)
        {
            OperationResult<MemberShopAccountDC> result = new OperationResult<MemberShopAccountDC>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetMemberShopAccounts(memberId, hit, gymCode, itemdNeeded);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member shop accounts " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<MemberShopAccountItemDC>> GetMemberShopAccountItems(int shopAccountId, string user, string gymCode, int branchId)
        {
            OperationResult<List<MemberShopAccountItemDC>> result = new OperationResult<List<MemberShopAccountItemDC>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetMemberShopAccountItems(shopAccountId, gymCode, branchId);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member shop accounts " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> CheckEmployeeByEntNo(int entNo, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = MemberFacade.CheckEmployeeByEntNo(entNo, gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in checking member as employee ByEntNo " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }



        public static OperationResult<List<CountryDC>> GetCountryDetails(string gymCode)
        {
            OperationResult<List<CountryDC>> result = new OperationResult<List<CountryDC>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetCountryDetails(gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in getting country " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<LanguageDC>> GetLangugeDetails(string gymCode)
        {
            OperationResult<List<LanguageDC>> result = new OperationResult<List<LanguageDC>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetLangugeDetails(gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in getting language " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SavePostalArea(string user, string postalCode, string postalArea, Int64 population, Int64 houseHold, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = MemberFacade.SavePostalArea(user, postalCode, postalArea,population,houseHold, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving PostalArea" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> UpdateMemberStatus(string gymCode, int branchId)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = MemberFacade.UpdateMemberStatus(gymCode, branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Updating Member Status GymCode " + gymCode + " brnach id " + branchId.ToString() + ", " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<TrainingProgramVisitDetailDC>> GetMemberVisitForTrainingProgram(string gymCode, int branchId, string systemName, DateTime lastUpdatedDateTime)
        {
            OperationResult<List<TrainingProgramVisitDetailDC>> result = new OperationResult<List<TrainingProgramVisitDetailDC>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetMemberVisitForTrainingProgram(gymCode, branchId, systemName, lastUpdatedDateTime);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member visit for training program." + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<TrainingProgramClassVisitDetailDC>> GetMemberClassVisitForTrainingProgram(string gymCode, int branchId, string systemName, DateTime lastUpdatedDateTime)
        {
            OperationResult<List<TrainingProgramClassVisitDetailDC>> result = new OperationResult<List<TrainingProgramClassVisitDetailDC>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetMemberClassVisitForTrainingProgram(gymCode, branchId, systemName, lastUpdatedDateTime);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member class visit for training program." + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> AddRegionDetails(RegionDC region, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = MemberFacade.AddRegionDetails(region,gymCode);
            }
            catch(Exception ex)
            {
                result.CreateMessage("Error in Adding Regions " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> AddCountryDetails(CountryDC country, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = MemberFacade.AddCountryDetails(country, gymCode);
            }
            catch(Exception ex)
            {
                result.CreateMessage("Error in Adding country " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<MemberFeeGenerationResultDC> GenerateMemberFee(List<int> gyms, int month, string gymCode, string user)
        {
            OperationResult<MemberFeeGenerationResultDC> result = new OperationResult<MemberFeeGenerationResultDC>();
            try
            {
                result.OperationReturnValue = MemberFacade.GenerateMemberFee(gyms, month, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Generating member fee " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult< List<ExcelineMemberDC>>  GetMembersForShop(int branchId, string searchText, int status, MemberRole memberRole, int hit, string gymCode)
        {
            var result = new OperationResult<List<ExcelineMemberDC>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetMembersForShop(branchId, searchText, status, memberRole, hit, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Generating member for shop " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> GetAllMemberAndCompany(int branchId, string searchText, int status, int hit, string gymCode)
        {

            var result = new OperationResult<string>();
            try
            {
                List<ExcelineMemberDC> members = MemberFacade.GetAllMemberAndCompany(branchId, searchText, status, hit, gymCode);
                result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(members);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member and company" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<bool> ValidateGenerateMemberFeeWithMemberFeeMonth(List<int> gyms, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = MemberFacade.ValidateGenerateMemberFeeWithMemberFeeMonth(gyms, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in validating member fee month " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<string>> ConfirmationDetailForGenerateMemberFee(List<int> gyms, int month, string gymCode, string user)
        {
            OperationResult<List<string>> result = new OperationResult<List<string>>();
            try
            {
                result.OperationReturnValue = MemberFacade.ConfirmationDetailForGenerateMemberFee(gyms, month, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Generating member fee - for confirmation " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<GymEmployeeDC>> GetEmployee(int aciveStatus, string gymCode)
        {
            var result = new OperationResult<List<GymEmployeeDC>>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetEmployee(aciveStatus, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting employee " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<MemberShopAccountDC> GetMemberShopAccounts(int memberId, string entityType, int hit, string user, bool itemsNeeded, string gymCode)
        {
            OperationResult<MemberShopAccountDC> result = new OperationResult<MemberShopAccountDC>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetMemberShopAccounts(memberId, entityType, hit, gymCode, itemsNeeded);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member shop accounts " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
