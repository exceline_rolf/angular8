﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "5/3/2012 5:07:55 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Data.DataAdapters;
using US.GMS.Data.DataAdapters.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Common;
using US.Common.Notification.Core.DomainObjects;

namespace US.GMS.BusinessLogic.ManageSystemSettings
{
    public class TaskManager
    {
        #region Task Templates
        public static OperationResult<bool> SaveTaskTemplate(TaskTemplateDC taskTemplate, bool isEdit, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.SaveTaskTemplate(taskTemplate, isEdit, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Task Template Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateTaskTemplate(TaskTemplateDC taskTemplate, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.UpdateTaskTemplate(taskTemplate, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Task Template updating" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteTaskTemplate(int taskTemplateId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.DeleteTaskTemplate(taskTemplateId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Task Template Deleting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeActivateTaskTemplate(int taskTemplateId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.DeActivateTaskTemplate(taskTemplateId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Task Template Deactivating" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<TaskTemplateDC>> GetTaskTemplates(TaskTemplateType templateType, int branchId, int templateId, string gymCode)
        {
            OperationResult<List<TaskTemplateDC>> result = new OperationResult<List<TaskTemplateDC>>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.GetTaskTemplates(templateType, branchId, templateId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Task Template Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExtendedTaskTemplateDC>> GetTaskTemplateExtFields(int extFieldId, string gymCode)
        {
            OperationResult<List<ExtendedTaskTemplateDC>> result = new OperationResult<List<ExtendedTaskTemplateDC>>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.GetTaskTemplateExtFields(extFieldId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Task Template Extension Field Gatting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExtendedFieldDC>> GetExtFieldsByCategory(int categoryId, string categoryType, string gymCode)
        {
            OperationResult<List<ExtendedFieldDC>> result = new OperationResult<List<ExtendedFieldDC>>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.GetExtFieldsByCategory(categoryId, categoryType, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Category Extesion Field Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveEntityTask(ExcelineTaskDC excelineTask, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.SaveEntityTask(excelineTask,  gymCode);
            }catch (Exception ex)
            {
                result.CreateMessage("Error in Save Entity task" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region Exceline Task
        public static OperationResult<int> SaveExcelineTask(ExcelineTaskDC excelineTask, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.SaveExcelineTask(excelineTask, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Save Entity task" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> AddEventToNotifications(USCommonNotificationDC notification, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.AddEventToNotifications(notification, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in saving notification-event" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> EditExcelineTask(ExcelineTaskDC excelineTask, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.EditExcelineTask(excelineTask, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Edit Entity task" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineTaskDC>> GetExcelineTasks(int branchId, bool isFxied, int templateId, int assignTo, string roleType, int followupMemberId, string gymCode)
        {
            
            OperationResult<List<ExcelineTaskDC>> result = new OperationResult<List<ExcelineTaskDC>>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.GetExcelineTasks(branchId, isFxied, templateId, assignTo, roleType, followupMemberId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Get task list" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineTaskDC>> SearchExcelineTask(string searchText, TaskTemplateType type, int followUpMemId, int branchId, bool isFxied, string gymCode)
        {
            OperationResult<List<ExcelineTaskDC>> result = new OperationResult<List<ExcelineTaskDC>>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.SearchExcelineTask(searchText, type, followUpMemId, branchId, isFxied, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in search task" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SaveTaskWithOutSchedule(ExcelineTaskDC excelineTask, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.SaveTaskWithOutSchedule(excelineTask, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Save Entity task with out schedule" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        #endregion

        #region Setting

        public static OperationResult<decimal> GetSessionTimeOutAfter(int branchId, string gymCode)
        {
            OperationResult<decimal> result = new OperationResult<decimal>();
            try
            {
                result.OperationReturnValue = MemberFacade.GetSessionTimeOutAfter(branchId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting SessionTimeOutAfter" + ex.Message, MessageTypes.ERROR); ;
            }
            return result;
        }

        #endregion

        #region Task Categories
        public static OperationResult<bool> SaveTaskCategory(ExcelineTaskCategoryDC taskCategory, bool isEdit, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.SaveTaskCategory(taskCategory, isEdit, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Task Category Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineTaskCategoryDC>> GetTaskCategories(int branchId, string gymCode)
        {
            OperationResult<List<ExcelineTaskCategoryDC>> result = new OperationResult<List<ExcelineTaskCategoryDC>>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.GetTaskCategories(branchId,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Task Categories Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteTaskCategory(int taskCategoryId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.DeleteTaskCategory(taskCategoryId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Task Category Deleting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region Job Categories
        public static OperationResult<bool> SaveJobCategory(ExcelineJobCategoryDC jobCategory, bool isEdit, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.SaveJobCategory(jobCategory, isEdit, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Job Category Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineJobCategoryDC>> GetJobCategories(int branchId, string gymCode)
        {
            OperationResult<List<ExcelineJobCategoryDC>> result = new OperationResult<List<ExcelineJobCategoryDC>>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.GetJobCategories(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Job Categories Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteJobCategory(int jobCategoryId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = TaskManageFacade.DeleteJobCategory(jobCategoryId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Gym Job Category Deleting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

    }
}
