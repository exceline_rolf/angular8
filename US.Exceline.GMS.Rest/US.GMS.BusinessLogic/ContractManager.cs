﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Data.DataAdapters;

namespace US.GMS.BusinessLogic
{
    public class ContractManager
    {
        public static OperationResult<List<PackageDC>> GetContracts(int branchId, string searchText, string gymCode)
        {
            OperationResult<List<PackageDC>> result = new OperationResult<List<PackageDC>>();
            try
            {
                result.OperationReturnValue = ContractFacade.GetContracts(branchId, searchText,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ActivityDC>> GetActivities(int branchId, string gymCode)
        {
            OperationResult<List<ActivityDC>> result = new OperationResult<List<ActivityDC>>();
            try
            {
                result.OperationReturnValue = ContractFacade.GetActivites(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Activites Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<List<ArticleDC>> GetArticlesForCommonUse(int categoryID, string gymCode, int branchID)
        {
            OperationResult<List<ArticleDC>> result = new OperationResult<List<ArticleDC>>();
            try
            {
                if (categoryID == -1)
                {
                    result.OperationReturnValue = ContractFacade.GetArticlesForVisits(gymCode, branchID);
                }
                else
                {
                    result.OperationReturnValue = ContractFacade.GetArticlesForContract(categoryID, gymCode, branchID);
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Articles" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ManageTemplate(string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ContractFacade.ManageTemplate(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Articles" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
