﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Login;
using US.Exceline.GMS.Modules.Login.BusinessLogic;

namespace US.Exceline.GMS.Modules.Login.API
{
    public class ManageExcelineLogin
    {
       

        public static OperationResultValue<List<ExceUserBranchDC>> GetUserSelectedBranches(string userName,string user)
        {
            return ExcelineLoginManage.GetUserSelectedBranches(userName, user);
        }

        public static OperationResultValue<List<ExecUserDC>> GetLoggedUserDetails(string userName)
        {
            return ExcelineLoginManage.GetLoggedUserDetails(userName);
        }

        public static OperationResultValue<bool> AddEditHardwareProfileId(int userId, int hardwareId, string user)
        {
            return ExcelineLoginManage.AddEditUserHardwareProfile(userId, hardwareId,user);
        }
        public static OperationResultValue<ExcePWRecDC> AnonymousUserPasswordReset(string gymCode, string potentialUserName)
        {
            return ExcelineLoginManage.AnonymousUserPasswordReset(gymCode, potentialUserName);
        }
        public static OperationResultValue<bool> SetNewUserPassword(ExcePWSetRecDC userData)
        {
            return ExcelineLoginManage.SetNewUserPassword(userData);
        }
    }
}
