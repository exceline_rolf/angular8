﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.AccessControl.API;
using US.Exceline.GMS.AccessControl.Service.Response;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.AccessControl.Service
{
    public class AccessControlService : IAccessControlService
    {
        public ResponseAuthentication CheckAuthentication(string branchId, string cardNo, string companyId, string terminalId)
        {
            ResponseAuthentication responseAuthentication;
            try
            {
                string gymCode = GMSExcelineAccessControl.GetGymCodeByCompanyId(companyId).OperationReturnValue;
                ExceAccessControlTerminalDetailDC terminalDetail = GMSExcelineAccessControl.GetTypeIdByTerminalId(Convert.ToInt32(terminalId), gymCode).OperationReturnValue;
                OperationResult<ExceAccessControlAuthenticationDC> result = GMSExcelineAccessControl.CheckAuthentication(Convert.ToInt32(branchId), cardNo.Trim(), gymCode.Trim(), terminalDetail, Convert.ToInt32(terminalId),  "System");
                var authentication = result.OperationReturnValue;

                if (authentication.IsValidMember != null && (bool) authentication.IsValidMember)
                {
                    var status = new ResponseStatus(1, "Successfull");
                    responseAuthentication = new ResponseAuthentication(authentication, status);
                }
                else
                {
                    var status = new ResponseStatus(2, "Member is not valid");
                    responseAuthentication = new ResponseAuthentication(authentication, status);
                    throw new Exception("Member is not valid");
                }
                return responseAuthentication;
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus(4, "Error on processing");
                responseAuthentication = new ResponseAuthentication(null, status);
                return responseAuthentication;
            }
        }

        public ResponseAuthentication CheckAuthenticationForGatPurchase(string branchId, string cardNo, string companyId, string terminalId, string durtaion, string price)
        {
            ResponseAuthentication responseAuthentication;
            try
            {
                string gymCode = GMSExcelineAccessControl.GetGymCodeByCompanyId(companyId).OperationReturnValue;
                OperationResult<ExceAccessControlAuthenticationDC> result = GMSExcelineAccessControl.CheckAuthenticationForGatPurchase(cardNo.Trim(), Convert.ToInt32(branchId), Convert.ToInt32(terminalId), gymCode, durtaion, price);
                ExceAccessControlAuthenticationDC authentication = result.OperationReturnValue;

                if (authentication.IsValidMember != null && (bool)authentication.IsValidMember && authentication.CanPurchase == true)
                {
                    var status = new ResponseStatus(1, "Successfull");
                    responseAuthentication = new ResponseAuthentication(authentication, status);
                }
                else
                {
                    var status = new ResponseStatus(2, "Member is not valid");
                    responseAuthentication = new ResponseAuthentication(authentication, status);
                    throw new Exception("Member is not valid");
                }
                return responseAuthentication;
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus(4, "Error on processing");
                responseAuthentication = new ResponseAuthentication(null, status);
                return responseAuthentication;
            }            
        }

        public ResponseTerminalDetailList GetTerminalDetails(string branchId, string companyId)
        {
            ResponseTerminalDetailList terminalDetailList;
            try
            {
                string gymCode = GMSExcelineAccessControl.GetGymCodeByCompanyId(companyId).OperationReturnValue;
                OperationResult<List<ExceAccessControlTerminalDetailDC>> result = GMSExcelineAccessControl.GetTerminalDetails(Convert.ToInt32(branchId), gymCode.Trim());
                var terminalDetails = result.OperationReturnValue;

                if (terminalDetails.Count > 0)
                {
                    var status = new ResponseStatus(1, "Successfull");
                    terminalDetailList = new ResponseTerminalDetailList(terminalDetails, status);
                }
                else
                {
                    var status = new ResponseStatus(2, "No terminals found");
                    terminalDetailList = new ResponseTerminalDetailList(null, status);
                }
                return terminalDetailList;
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus(4, "Error on processing");
                terminalDetailList = new ResponseTerminalDetailList(null, status);
                return terminalDetailList;
            }
        }

        public ResponseStatus UpdateTerminalStatus(string companyId, ExceAccessControlUpdateTerminalStatusDC terminalStatusDetail)
        {
            try
            {
                string gymCode = GMSExcelineAccessControl.GetGymCodeByCompanyId(companyId).OperationReturnValue;
                bool updateStatus = GMSExcelineAccessControl.UpdateTerminalStatus(gymCode.Trim(), terminalStatusDetail).OperationReturnValue;
                if (updateStatus)
                {
                    var status = new ResponseStatus(1, "Successfull");
                    return status;
                }
                else
                {
                    var status = new ResponseStatus(2, "Error updating terminal status");
                    return status;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus(4, "Error on processing");
                return status;
            }
        }

        public ResponseStatus RegisterPurchase(string companyId, ExceAccessControlRegisterPurchaseDC purchaseDetail)
        {
            try
            {
                string gymCode = GMSExcelineAccessControl.GetGymCodeByCompanyId(companyId).OperationReturnValue;
                bool updateStatus = GMSExcelineAccessControl.RegisterPurchase(gymCode.Trim(), purchaseDetail).OperationReturnValue;
                if (updateStatus)
                {
                    var status = new ResponseStatus(1, "Successfull");
                    return status;
                }
                else
                {
                    var status = new ResponseStatus(2, "Error registering purchase");
                    return status;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus(4, "Error on processing");
                return status;
            }
        }

        public ResponseStatus RegisterVendingEvent(string companyId, ExceAccessControlVendingEventDC vendingEventDetail)
        {
            try
            {
                string gymCode = GMSExcelineAccessControl.GetGymCodeByCompanyId(companyId).OperationReturnValue;
                bool updateStatus = GMSExcelineAccessControl.RegisterVendingEvent(gymCode.Trim(), vendingEventDetail).OperationReturnValue;
                if (updateStatus)
                {
                    var status = new ResponseStatus(1, "Successfull");
                    return status;
                }
                else
                {
                    var status = new ResponseStatus(2, "Error register vending event");
                    return status;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus(4, "Error on processing");
                return status;
            }
        }


        public ResponseStatus RegisterSubBedPurchase(string companyId, ExceAccessControlRegisterPurchaseDC purchaseDetail)
        {
            DateTime startTime = DateTime.Now;
            TimeSpan tDifference;
            try
            {
                
                string gymCode = GMSExcelineAccessControl.GetGymCodeByCompanyId(companyId).OperationReturnValue;
                bool updateStatus = GMSExcelineAccessControl.RegisterSunBedPurchase(gymCode.Trim(), purchaseDetail).OperationReturnValue;
                 tDifference = DateTime.Now - startTime;
                if (updateStatus)
                {
                    
                    var status = new ResponseStatus(1, "Successfull - Duration, Minutes- "+ tDifference.Minutes.ToString() + " Seconds - "+ tDifference.Seconds.ToString()+ " mili seconds - "+ tDifference.Milliseconds.ToString());
                    return status;
                }
                else
                {
                    var status = new ResponseStatus(2, "Error registering purchase - Duration, Minutes- " + tDifference.Minutes.ToString() + " Seconds - " + tDifference.Seconds.ToString() + " mili seconds - " + tDifference.Milliseconds.ToString());
                    throw new Exception("Error registering purchase");
                }
            }
            catch (Exception ex)
            {
                tDifference = DateTime.Now - startTime;
                var status = new ResponseStatus(4, "Error on processing - Duration, Minutes- " + tDifference.Minutes.ToString() + " Seconds - " + tDifference.Seconds.ToString() + " mili seconds - " + tDifference.Milliseconds.ToString());
                return status;
            }
        }

        public ResponseStatus RegisterMemberVisit(string companyId, ExceAccessControlRegisterMemberVisitDC visit)
        {
            try
            {
                string gymCode = GMSExcelineAccessControl.GetGymCodeByCompanyId(companyId).OperationReturnValue;
                bool updateStatus = GMSExcelineAccessControl.RegisterMemberVisit(gymCode.Trim(), visit,"System").OperationReturnValue;
                if (updateStatus)
                {
                    var status = new ResponseStatus(1, "Successfull");
                    return status;
                }
                else
                {
                    var status = new ResponseStatus(2, "Error registering member visit");
                    return status;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus(4, "Error on processing");
                return status;
            }
        }       
    }
}
