﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace US.Exceline.GMS.Access.Service
{
    [DataContract]
    public class ImageResponse
    {
        public ImageResponse() { }
        public ImageResponse(string image, ResponseStatus status)
        {
            MemberImage = image;
            Status = status;
        }

        [DataMember(Order=1)]
        public string MemberImage { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}