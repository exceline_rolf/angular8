﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.Access.Service
{
    [DataContract]
    public class AccessTimesResponse
    {
        public AccessTimesResponse() { }
        public AccessTimesResponse(List<ExceACCAccessProfileTime> accessTimes, ResponseStatus status)
        {
            AcessTimes = accessTimes;
            Status = status;
        }

        [DataMember(Order=1)]
        public List<ExceACCAccessProfileTime> AcessTimes { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}
