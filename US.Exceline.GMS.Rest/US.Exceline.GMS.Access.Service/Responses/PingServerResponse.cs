﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace US.Exceline.GMS.Access.Service.Responses
{
    public class PingServerResponse
    {
          public PingServerResponse() { }
          public PingServerResponse(int result, ResponseStatus status)
        {
            Result = result;
            Status = status;
        }

        [DataMember(Order=1)]
        public int Result { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}