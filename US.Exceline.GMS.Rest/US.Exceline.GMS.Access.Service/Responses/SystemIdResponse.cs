﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace US.Exceline.GMS.Access.Service.Responses
{
    public class SystemIdResponse
    {
        public SystemIdResponse() { }
        public SystemIdResponse(int systemId, ResponseStatus status)
        {
            SystemId = systemId;
            Status = status;
        }

        [DataMember(Order=1)]
        public int SystemId { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}