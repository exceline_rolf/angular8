﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.Access.Service
{
    [DataContract]
    public class ResponseMemberList
    {
        public ResponseMemberList() { }
        public ResponseMemberList(List<ExceACCMember> members, ResponseStatus status)
        {
            Members = members;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceACCMember> Members { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}
