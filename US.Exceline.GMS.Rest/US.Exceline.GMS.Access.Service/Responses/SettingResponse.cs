﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.Access.Service
{
    [DataContract]
    public class SettingResponse
    {
        public SettingResponse() { }
        public SettingResponse(ExceACCSettings settings, ResponseStatus status)
        {
            Settings = settings;
            Status = status;
        }

        [DataMember(Order = 1)]
        public ExceACCSettings Settings { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}
