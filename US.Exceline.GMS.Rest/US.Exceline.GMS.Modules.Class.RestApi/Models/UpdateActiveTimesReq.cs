﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.Exceline.GMS.Modules.Class.RestApi.Models
{
    public class UpdateActiveTimesReq
    {
        public List<EntityActiveTimeDC> ActiveTimeList { get; set; }
        public int BranchId { get; set; }

    }
}