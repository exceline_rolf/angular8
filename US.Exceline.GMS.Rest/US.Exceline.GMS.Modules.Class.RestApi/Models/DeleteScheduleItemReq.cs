﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Class.RestApi.Models
{
    public class DeleteScheduleItemReq
    {
        private List<int> _scheduleItemIdList;

        public List<int> ScheduleItemIdList
        {
            get { return _scheduleItemIdList; }
            set { _scheduleItemIdList = value; }
        }

    }
}