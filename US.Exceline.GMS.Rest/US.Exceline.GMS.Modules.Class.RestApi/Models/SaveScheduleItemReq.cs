﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.Exceline.GMS.Modules.Class.RestApi.Models
{
    public class SaveScheduleItemReq
    {

        private ScheduleItemDC _scheduleItem;

        public ScheduleItemDC ScheduleItem
        {
            get { return _scheduleItem; }
            set { _scheduleItem = value; }
        }

        private int _branchId;

        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _culture;

        public string Culture
        {
            get { return _culture; }
            set { _culture = value; }
        }


    }
}