﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.DomainObjects
{
    [DataContract]
    public class USPErrorPayment
    {
        private int _id = -1;
        [DataMember]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
       
        private decimal _amount = 0;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private DateTime _voucherDate;
        [DataMember]
        public DateTime VoucherDate
        {
            get { return _voucherDate; }
            set { _voucherDate = value; }
        }

        private string _source = string.Empty;
        [DataMember]
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private string _ref = string.Empty;
        [DataMember]
        public string Ref
        {
            get { return _ref; }
            set { _ref = value; }
        }

        private string _kid = string.Empty;
        [DataMember]
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private DateTime _notifiedDate;
        [DataMember]
        public DateTime NotifiedDate
        {
            get { return _notifiedDate; }
            set { _notifiedDate = value; }
        }

        private string _accountNo = string.Empty;
        [DataMember]
        public string AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; }
        }

        private string _inkassoId = string.Empty;
        [DataMember]
        public string InkassoId
        {
            get { return _inkassoId; }
            set { _inkassoId = value; }
        }

        private string _creditorName = string.Empty;
        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }

        private string _custId = string.Empty;
        [DataMember]
        public string CustId
        {
            get { return _custId; }
            set { _custId = value; }
        }

        private DateTime _regDate;
        [DataMember]
        public DateTime RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }

        private string _errorMessage = string.Empty;
        [DataMember]
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        private int _isError = -1;
        [DataMember]
        public int IsError
        {
            get { return _isError; }
            set { _isError = value; }
        }

        private int _itemType = -1;
        [DataMember]
        public int ItemType
        {
            get { return _itemType; }
            set { _itemType = value; }
        }

        private int _mapStatus = -1;
        [DataMember]
        public int MapStatus
        {
            get { return _mapStatus; }
            set { _mapStatus = value; }
        }

        private string _aRItemMatched = string.Empty;
         [DataMember]
        public string ARItemMatched
        {
            get { return _aRItemMatched; }
            set { _aRItemMatched = value; }
        }

        private string _creditor = string.Empty;
        [DataMember]
        public string Creditor
        {
            get { return _creditor; }
            set { _creditor = value; }
        }

        private int _subCaseNo = 0;
        [DataMember]
        public int SubCaseNo
        {
            get { return _subCaseNo; }
            set { _subCaseNo = value; }
        }

        private int _caseNo = 0;
        [DataMember]
        public int CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }

        private int _voucherNo = 0;
        [DataMember]
        public int VoucherNo
        {
            get { return _voucherNo; }
            set { _voucherNo = value; }
        }

        private int _rowID = 0;
        [DataMember]
        public int RowID
        {
            get { return _rowID; }
            set { _rowID = value; }
        }

        private int _arNo = 0;
        [DataMember]
        public int ArNo
        {
            get { return _arNo; }
            set { _arNo = value; }
        }

        private string _errorPaymentOperationName = string.Empty;
        [DataMember]
        public string ErrorPaymentOperationName
        {
            get { return _errorPaymentOperationName; }
            set { _errorPaymentOperationName = value; }
        }
       
        private string _errorPaymentType = string.Empty;
        [DataMember]
        public string ErrorPaymentType
        {
            get { return _errorPaymentType; }
            set { _errorPaymentType = value; }
        }

        private bool _isMatched = false;
        [DataMember]
        public bool IsMatched
        {
            get { return _isMatched; }
            set { _isMatched = value; }
        }

        private bool _isEnabledViewButton = false;
        [DataMember]
        public bool IsEnabledViewButton
        {
            get { return _isEnabledViewButton; }
            set { _isEnabledViewButton = value; }
        }

        private bool _isEnabledIgnoreButton = true;
        [DataMember]
        public bool IsEnabledIgnoreButton
        {
            get { return _isEnabledIgnoreButton; }
            set { _isEnabledIgnoreButton = value; }
        }

        private bool _isEnabledPaymentButton = true;
        [DataMember]
        public bool IsEnabledPaymentButton
        {
            get { return _isEnabledPaymentButton; }
            set { _isEnabledPaymentButton = value; }
        }

        private bool _isEnabledExMatchButton = true;
        [DataMember]
        public bool IsEnabledExMatchButton
        {
            get { return _isEnabledExMatchButton; }
            set { _isEnabledExMatchButton = value; }
        }

        private bool _isOpButtonEnabled = false;
        [DataMember]
        public bool IsOpButtonEnabled
        {
            get { return _isOpButtonEnabled; }
            set { _isOpButtonEnabled = value; }
        }

        private bool _isIgnored = false;
        [DataMember]
        public bool IsIgnored
        {
            get { return _isIgnored; }
            set { _isIgnored = value; }
        }

        private string _ignoredButtonName = string.Empty;
        [DataMember]
        public string IgnoredButtonName
        {
            get { return _ignoredButtonName; }
            set { _ignoredButtonName = value; }
        }

        private string _fileName = string.Empty;
        [DataMember]
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private DateTime _fileDate = DateTime.MinValue;
        [DataMember]
        public DateTime FileDate
        {
            get { return _fileDate; }
            set { _fileDate = value; }
        }

        private string _fileType = string.Empty;
        [DataMember]
        public string FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }

        private double _totalNoOfRecordsInFile = 0;
        [DataMember]
        public double TotalNoOfRecordsInFile
        {
            get { return _totalNoOfRecordsInFile; }
            set { _totalNoOfRecordsInFile = value; }
        }

        private double _fileTotalAmount = 0;
        [DataMember]
        public double FileTotalAmount
        {
            get { return _fileTotalAmount; }
            set { _fileTotalAmount = value; }
        }

        private double _matchedRecords = 0;
        [DataMember]
        public double MatchedRecords
        {
            get { return _matchedRecords; }
            set { _matchedRecords = value; }
        }

        private double _matchedTotal = 0;
        [DataMember]
        public double MatchedTotal
        {
            get { return _matchedTotal; }
            set { _matchedTotal = value; }
        }

        private double _tooMuchPaidRecords = 0;
        [DataMember]
        public double TooMuchPaidRecords
        {
            get { return _tooMuchPaidRecords; }
            set { _tooMuchPaidRecords = value; }
        }

        private double _tooMuchPaidRecordsTotal = 0;
        [DataMember]
        public double TooMuchPaidRecordsTotal
        {
            get { return _tooMuchPaidRecordsTotal; }
            set { _tooMuchPaidRecordsTotal = value; }
        }

        private double _unKnownReocrds = 0;
        [DataMember]
        public double UnKnownReocrds
        {
            get { return _unKnownReocrds; }
            set { _unKnownReocrds = value; }
        }

        private double _unKnownRecordsTotal = 0;
        [DataMember]
        public double UnKnownRecordsTotal
        {
            get { return _unKnownRecordsTotal; }
            set { _unKnownRecordsTotal = value; }
        }

        private double _mappedErrorPaymentRecords = 0;
        [DataMember]
        public double MappedErrorPaymentRecords
        {
            get { return _mappedErrorPaymentRecords; }
            set { _mappedErrorPaymentRecords = value; }
        }

        private double _mappedErrorPaymentTotal = 0;
        [DataMember]
        public double MappedErrorPaymentTotal
        {
            get { return _mappedErrorPaymentTotal; }
            set { _mappedErrorPaymentTotal = value; }
        }

        private double _ignoredErrorPaymentRecords = 0;
        [DataMember]
        public double IgnoredErrorPaymentRecords
        {
            get { return _ignoredErrorPaymentRecords; }
            set { _ignoredErrorPaymentRecords = value; }
        }
        private double _ignoredErrorPaymentTotal = 0;
        [DataMember]
        public double IgnoredErrorPaymentTotal
        {
            get { return _ignoredErrorPaymentTotal; }
            set { _ignoredErrorPaymentTotal = value; }
        }

        private int _exceedPaymentAritemNo = -1;
        [DataMember]
        public int ExceedPaymentAritemNo
        {
            get { return _exceedPaymentAritemNo; }
            set { _exceedPaymentAritemNo = value; }
        }

         private int _exceedPaymentApportionId = -1;
         [DataMember]
         public int ExceedPaymentApportionId
         {
             get { return _exceedPaymentApportionId; }
             set { _exceedPaymentApportionId = value; }
         }

         private string _status = string.Empty;
         [DataMember]
         public string Status
         {
             get { return _status; }
             set { _status = value; }
         }

         private bool _enableButtons = true;
         [DataMember]
         public bool EnableButtons
         {
             get { return _enableButtons; }
             set { _enableButtons = value; }
         }

         private string _recordType = string.Empty;
         [DataMember]
         public string RecordType
         {
             get { return _recordType; }
             set { _recordType = value; }
         }

    }
}
