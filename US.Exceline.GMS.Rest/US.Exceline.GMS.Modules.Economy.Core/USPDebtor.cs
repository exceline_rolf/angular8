﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Core
{
    public class USPDebtor : IUSPDebtor
    {
        #region IUSPDebtor Members


        private string _FirstName = string.Empty;
        public string DebtorFirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }


        private string _SecondName = string.Empty;
        public string DebtorSecondName
        {
            get
            {
                return _SecondName;
            }
            set
            {
                _SecondName = value;
            }
        }


        private string _custumerID = string.Empty;
        public string DebtorcustumerID
        {
            get
            {
                return _custumerID;
            }
            set
            {
                _custumerID = value;
            }
        }


        private string _BirthDay = string.Empty;
        public string DebtorBirthDay
        {
            get
            {
                return _BirthDay;
            }
            set
            {
                _BirthDay = value;
            }
        }


        private string _PersonNo = string.Empty;
        public string DebtorPersonNo
        {
            get
            {
                return _PersonNo;
            }
            set
            {
                _PersonNo = value;
            }
        }


        private List<IUSPAddress> _AddressList = new List<IUSPAddress>();
        public List<IUSPAddress> DebtorAddressList
        {
            get
            {
                return _AddressList;
            }
            set
            {
                _AddressList = value;
            }
        }


        private int _EntityID = -1;
        public int DebtorEntityID
        {
            get
            {
                return _EntityID;
            }
            set
            {
                _EntityID = value;
            }
        }


        private int _EntityRoleID = -1;
        public int DebtorEntityRoleID
        {
            get
            {
                return _EntityRoleID;
            }
            set
            {
                _EntityRoleID = value;
            }
        }

        #endregion

        private string _debtorTitle = string.Empty;
        public string DebtorTitle
        {
            get
            {
                return _debtorTitle;
            }
            set
            {
                _debtorTitle = value;
            }
        }
    }
}
