﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    [DataContract]
    public class DataForLedger
    {
        private string _signature;
        [DataMember]
        public string signature
        {
            get { return _signature; }
            set { _signature = value; }
        }

        private string _transDate;
        [DataMember]
        public string transDate
        {
            get { return _transDate; }
            set { _transDate = value; }
        }

        private string _transTime;
        [DataMember]
        public string transTime
        {
            get { return _transTime; }
            set { _transTime = value; }
        }

        private int _nr;
        [DataMember]
        public int nr
        {
            get { return _nr; }
            set { _nr = value; }
        }

        private double _transAmntIn;
        [DataMember]
        public double transAmntIn
        {
            get { return _transAmntIn; }
            set { _transAmntIn = value; }
        }

        private double _transAmntEx;
        [DataMember]
        public double transAmntEx
        {
            get { return _transAmntEx; }
            set { _transAmntEx = value; }
        }

        private int _branchId;
        [DataMember]
        public int branchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private int _SalePointID;
        [DataMember]
        public int SalePointID
        {
            get { return _SalePointID; }
            set { _SalePointID = value; }
        }
    }
}