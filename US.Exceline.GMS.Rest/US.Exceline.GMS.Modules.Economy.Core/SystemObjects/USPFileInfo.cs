﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class USPFileInfo : IUSPDataFileInfo
    {
        #region IUSPDataFileInfo Members

        public string FilePath
        {
            get;
            set;
        }

        public string PluginID
        {
            get;
            set;
        }

        #endregion
    }
}
