﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class PaymentProcessStepResult
    {
        private bool _resultStatus = false;

        public bool ResultStatus
        {
            get { return _resultStatus; }
            set { _resultStatus = value; }
        }
        private string _stepName = string.Empty;

        public string StepName
        {
            get { return _stepName; }
            set { _stepName = value; }
        }
        private IUSPTransaction _transaction;

        public IUSPTransaction Transaction
        {
            get { return _transaction; }
            set { _transaction = value; }
        }
        private List<string> _messageList = new List<string>();

        public List<string> MessageList
        {
            get { return _messageList; }
            set { _messageList = value; }
        }

        private decimal _balanceAmount = 0;
        public decimal BalanceAmount
        {
            get { return _balanceAmount; }
            set { _balanceAmount = value; }
        }
    }
}
