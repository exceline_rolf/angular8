﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class CreditorOrderLine
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private int _orderNo;
        public int OrderNo
        {
            get { return _orderNo; }
            set { _orderNo = value; }
        }

        private string _orderType;
        public string OrderType
        {
            get { return _orderType; }
            set { _orderType = value; }
        }

        private string _creditorName;
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }
        private string _creditorNameView;
        public string CreditorNameView
        {
            get { return _creditorNameView; }
            set { _creditorNameView = value; }
        }
        private int _remitJournalDetailId;
        public int RemitJournalDetailId
        {
            get { return _remitJournalDetailId; }
            set { _remitJournalDetailId = value; }
        }

        private int _creditorNo;
        public int CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }

        private int _subCaseNo;
        public int SubCaseNo
        {
            get { return _subCaseNo; }
            set { _subCaseNo = value; }

        }

        private string _creditorGroupName = string.Empty;
        public string CreditorGroupName
        {
            get { return _creditorGroupName; }
            set { _creditorGroupName = value; }
        }

        private int _customerNo = -1;
        public int CustomerNo
        {
            get { return _customerNo; }
            set { _customerNo = value; }
        }

        private decimal _unitPrice;
        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set { _unitPrice = value; }
        }

        private decimal _noOfItems;
        public decimal NoOfItems
        {
            get { return _noOfItems; }
            set { _noOfItems = value; }
        }

        private decimal _amount;
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private decimal _vatDeductedAmount;
        public decimal VATDeductedAmount
        {
            get { return _vatDeductedAmount; }
            set { _vatDeductedAmount = value; }
        }

        private decimal _vatInvoiceAmount;
        public decimal VATInvoiceAmount
        {
            get { return _vatInvoiceAmount; }
            set { _vatInvoiceAmount = value; }
        }

        private decimal _discount;
        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        private string _articleNo;
        public string ArticleNo
        {
            get { return _articleNo; }
            set { _articleNo = value; }
        }

        private string _articleText;
        public string ArticleText
        {
            get { return _articleText; }
            set { _articleText = value; }
        }

        private DateTime _regDate;
        public DateTime RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }

        private string _createdUser;
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _modifiedUser;
        public string ModifiedUser
        {
            get { return _modifiedUser; }
            set { _modifiedUser = value; }
        }

        private bool _isChecked = false;
        public bool IsChecked
        {
            get { return _isChecked; }
            set { _isChecked = value; }
        }

        private bool _isAdded = false;
        public bool IsAdded
        {
            get { return _isAdded; }
            set { _isAdded = value; }
        }

        private string _invoiceNo = string.Empty;
        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        private DateTime _dueDate;
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private CreditorInvoiceType _creditorInvoiceType = CreditorInvoiceType.NONE;
        public CreditorInvoiceType CreditorInvoiceType
        {
            get { return _creditorInvoiceType; }
            set { _creditorInvoiceType = value; }
        }

        private int _apportionId = -1;

        public int ApportionId
        {
            get { return _apportionId; }
            set { _apportionId = value; }
        }

        private int _caseNo = 0;
        public int CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }

        private string _sponsorReference = string.Empty;
        public string SponsorReference
        {
            get { return _sponsorReference; }
            set { _sponsorReference = value; }
        }

        private int _noOfVisits = 0;
        public int NoOfVisits
        {
            get { return _noOfVisits; }
            set { _noOfVisits = value; }
        }

        private DateTime _lastVisitDate;
        public DateTime LastVisitDate
        {
            get { return _lastVisitDate; }
            set { _lastVisitDate = value; }
        }

        private decimal _balance = 0;
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        private int _branchNo = -1;
        public int BranchNo
        {
            get { return _branchNo; }
            set { _branchNo = value; }
        }

        private string _coRelationId = string.Empty;
        public string CoRelationId
        {
            get { return _coRelationId; }
            set { _coRelationId = value; }
        }
        private string _employNo = string.Empty;
        public string EmployNo
        {
            get { return _employNo; }
            set { _employNo = value; }
        }
        private int _debtorEntNo = -1;
        public int DebtorEntNo
        {
            get { return _debtorEntNo; }
            set { _debtorEntNo = value; }
        }

        private string _processBatchNo = string.Empty;
        public string ProcessBatchNo
        {
            get { return _processBatchNo; }
            set { _processBatchNo = value; }
        }

        private int _sourceID = 0;
        public int SourceID
        {
            get { return _sourceID; }
            set { _sourceID = value; }
        }

        private string _creationSource = string.Empty;
        public string CreationSource
        {
            get { return _creationSource; }
            set { _creationSource = value; }
        }

        private bool _isInvocieFee = false;
        public bool IsInvocieFee
        {
            get { return _isInvocieFee; }
            set { _isInvocieFee = value; }
        }

        private string _voucherNo = string.Empty;
        public string VoucherNo
        {
            get { return _voucherNo; }
            set { _voucherNo = value; }
        }

        private DateTime? _expiryDate;
        public DateTime? ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }

        private string _description = string.Empty;
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }
}
