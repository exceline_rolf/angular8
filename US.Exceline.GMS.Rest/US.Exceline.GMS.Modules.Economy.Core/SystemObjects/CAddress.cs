﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class CAddress
    {
        private int _addressNo = 0;

        public int AddressNo
        {
            get { return _addressNo; }
            set { _addressNo = value; }
        }

        private string _address1 = string.Empty;

        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        private string _address2 = string.Empty;

        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        private string _address3 = string.Empty;

        public string Address3
        {
            get { return _address3; }
            set { _address3 = value; }
        }


        private string _zip = string.Empty;

        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }

        private string _country = string.Empty;

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }


        private string _zipName = string.Empty;

        public string ZipName
        {
            get { return _zipName; }
            set { _zipName = value; }
        }

        private string _countryCode = string.Empty;

        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }


        private int _addrNo = -1;

        public int AddrNo
        {
            get { return _addrNo; }
            set { _addrNo = value; }
        }


        private string _addSource = string.Empty;

        public string AddSource
        {
            get { return _addSource; }
            set { _addSource = value; }
        }

        private string _telMobile;

        public string TelMobile
        {
            get { return _telMobile; }
            set { _telMobile = value; }
        }
        private string _telWork;

        public string TelWork
        {
            get { return _telWork; }
            set { _telWork = value; }
        }
        private string _telHome;

        public string TelHome
        {
            get { return _telHome; }
            set { _telHome = value; }
        }
        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private string _fax;

        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }
        private string _msn;

        public string MSN
        {
            get { return _msn; }
            set { _msn = value; }
        }
        private string _skype;

        public string Skype
        {
            get { return _skype; }
            set { _skype = value; }
        }
        private string _sms;

        public string SMS
        {
            get { return _sms; }
            set { _sms = value; }
        }
        private bool _isDefault;

        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }

        private string _postCode;

        public string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }

        private string _city;

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        private string _municipality;

        public string Municipality
        {
            get { return _municipality; }
            set { _municipality = value; }
        }

        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
    }
}
