﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class CancellationStatus
    {
        private int _id = -1;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private DateTime? _regDate;
        public DateTime? RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }

        private string _recordType = string.Empty;
        public string RecordType
        {
            get { return _recordType; }
            set { _recordType = value; }
        }

        private string _pid = string.Empty;
        public string PID
        {
            get { return _pid; }
            set { _pid = value; }
        }

        private string _bNumber = string.Empty;
        public string BNumber
        {
            get { return _bNumber; }
            set { _bNumber = value; }
        }

        private string _cid = string.Empty;
        public string CID
        {
            get { return _cid; }
            set { _cid = value; }
        }

        private string _kid = string.Empty;
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private int _status = -1;
        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private string statusDate = string.Empty;
        public string StatusDate
        {
            get { return statusDate; }
            set { statusDate = value; }
        }

        private string _referenceid = string.Empty;
        public string ReferenceId
        {
            get { return _referenceid; }
            set { _referenceid = value; }
        }

        private string _batchID = string.Empty;
        public string BatchID
        {
            get { return _batchID; }
            set { _batchID = value; }
        }

        private string _dataSource = string.Empty;
        public string DataSource
        {
            get { return _dataSource; }
            set { _dataSource = value; }
        }

        private int _fileID = -1;
        public int FileID
        {
            get { return _fileID; }
            set { _fileID = value; }
        }

        private string _accountNo = string.Empty;
        public string AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; }
        }
    }
}
