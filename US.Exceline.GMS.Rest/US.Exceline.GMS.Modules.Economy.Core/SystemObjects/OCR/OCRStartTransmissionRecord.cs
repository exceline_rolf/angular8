﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCRStartTransmissionRecord : IUSPOCRStartTransmissionRecord
    {
       public string FormatCode
        {
            get;
            set;
        }

        public string ServiceCode
        {
            get;
            set;
        }

        public string SendingType
        {
            get;
            set;
        }

        public OCRRecordTypes RecordType
        {
            get;
            set;
        }

        public string DataSender
        {
            get;
            set;
        }

        public string SendingNumber
        {
            get;
            set;
        }

        public string DataRecipient
        {
            get;
            set;
        }
    }
}
