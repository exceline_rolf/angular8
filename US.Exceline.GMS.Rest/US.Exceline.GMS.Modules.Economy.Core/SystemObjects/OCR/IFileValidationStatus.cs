﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public interface IFileValidationStatus
    {
        bool Status { get; set; }
        List<IMessage> Mesages { get; set; }

        /// <summary>
        /// indicate weather file need to move to fail folder when validation fail
        /// </summary>
        bool MoveToFail { get; set; }

        int FileID { get; set; }

        string FileName { get; set; }

        string UpdatedFileName { get; set; }
    }
}
