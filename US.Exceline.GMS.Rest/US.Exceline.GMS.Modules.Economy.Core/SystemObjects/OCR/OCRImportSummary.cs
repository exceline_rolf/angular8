﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    [DataContract]
    public class OCRImportSummary
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _fileId = -1;
        [DataMember]
        public int FileId
        {
            get { return _fileId; }
            set { _fileId = value; }
        }

        private string _fileName = string.Empty;
        [DataMember]
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private int _paymentsCount = 0;
        [DataMember]
        public int PaymentsCount
        {
            get { return _paymentsCount; }
            set { _paymentsCount = value; }
        }

        private int _statusCount = 0;
        [DataMember]
        public int StatusCount
        {
            get { return _statusCount; }
            set { _statusCount = value; }
        }

        private decimal _paymentsSum = 0;
        [DataMember]
        public decimal PaymentsSum
        {
            get { return _paymentsSum; }
            set { _paymentsSum = value; }
        }

        private int _errorAccPayments = 0;
        [DataMember]
        public int ErrorAccPayments
        {
            get { return _errorAccPayments; }
            set { _errorAccPayments = value; }
        }

        private int _errorAccStatus = 0;
        [DataMember]
        public int ErrorAccStatus
        {
            get { return _errorAccStatus; }
            set { _errorAccStatus = value; }
        }

        private decimal _errorAccountPaymentSum = 0;
        [DataMember]
        public decimal ErrorAccountPaymentSum
        {
            get { return _errorAccountPaymentSum; }
            set { _errorAccountPaymentSum = value; }
        }

        private string _importedUser = string.Empty;
        [DataMember]
        public string ImportedUser
        {
            get { return _importedUser; }
            set { _importedUser = value; }
        }

        private DateTime? _importedDate;
         [DataMember]
        public DateTime? ImportedDate
        {
            get { return _importedDate; }
            set { _importedDate = value; }
        }

         private decimal _totalPayments = 0;
         [DataMember]
         public decimal TotalPayments
         {
             get { return _totalPayments; }
             set { _totalPayments = value; }
         }

    }
}
