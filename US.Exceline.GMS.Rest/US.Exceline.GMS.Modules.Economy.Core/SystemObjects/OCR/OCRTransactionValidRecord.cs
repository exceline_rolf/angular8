﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCRTransactionValidRecord : OCRTransactionRecord
    {
        private OCRTransactionAmountItem1 _oCRTransactionPost1;

        public OCRTransactionAmountItem1 OCRTransactionPost1
        {
            get { return _oCRTransactionPost1; }
            set { _oCRTransactionPost1 = value; }
        }

        private OCRTransactionAmountItem2 oCRTransactionPost2;

        public OCRTransactionAmountItem2 OCRTransactionPost2
        {
            get { return oCRTransactionPost2; }
            set { oCRTransactionPost2 = value; }
        }
    }
}

