﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCRTransactionMainPart
    {
        private string _formatCode;
        /// <summary>
        /// Format Code. Always NY.
        /// Index of OCR file : 1-2
        /// </summary>
        public string FormatCode
        {
            get { return _formatCode; }
            set { _formatCode = value; }
        }

        private string _serviceCode;
        /// <summary>
        /// Service Code. 
        /// Index of OCR file : 3-4
        /// </summary>
        public string ServiceCode
        {
            get { return _serviceCode; }
            set { _serviceCode = value; }
        }
        private string _transType;
        /// <summary>
        /// Sending Type.
        /// Index of OCR file : 5-6
        /// </summary>
        public string TransType
        {
            get { return _transType; }
            set { _transType = value; }
        }

        private OCRRecordTypes _recordType;
        /// <summary>
        /// Record type.
        /// Index of OCR file : 7-8
        /// </summary>
        public OCRRecordTypes RecordType
        {
            get { return _recordType; }
            set { _recordType = value; }
        }

        private string _transNumber;
        /// <summary>
        /// Agreement ID
        /// Index of OCR file : 9-15
        /// </summary>
        public string TransNumber
        {
            get { return _transNumber; }
            set { _transNumber = value; }
        }
    }
}
