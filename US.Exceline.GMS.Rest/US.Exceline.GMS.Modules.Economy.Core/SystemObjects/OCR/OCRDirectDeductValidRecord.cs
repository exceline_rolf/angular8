﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCRDirectDeductValidRecord : OCRTransactionRecord, IUSPOCRDirectDeductValidRecord
    {
        private string _formatCode;
        /// <summary>
        /// Format Code. Always NY.
        /// Index of OCR file : 1-2
        /// </summary>
        public string FormatCode
        {
            get { return _formatCode; }
            set { _formatCode = value; }
        }

        private string _serviceCode;
        /// <summary>
        /// Service Code. 
        /// Index of OCR file : 3-4
        /// </summary>
        public string ServiceCode
        {
            get { return _serviceCode; }
            set { _serviceCode = value; }
        }
        private string _taskType;
        /// <summary>
        /// Task Type.
        /// Index of OCR file : 5-6
        /// </summary>
        public string TaskType
        {
            get { return _taskType; }
            set { _taskType = value; }
        }

        private OCRRecordTypes _recordType;
        /// <summary>
        /// Record type.
        /// Index of OCR file : 7-8
        /// </summary>
        public OCRRecordTypes RecordType
        {
            get { return _recordType; }
            set { _recordType = value; }
        }

        private string _fBOCounter;
        /// <summary>
        /// FBOCounter ID
        /// Index of OCR file : 9-15
        /// </summary>
        public string FBOCounter
        {
            get { return _fBOCounter; }
            set { _fBOCounter = value; }
        }

        private string _registerType;
        /// <summary>
        /// Register Type
        /// Index of OCR file : 16
        /// </summary>
        public string RegisterType
        {
            get { return _registerType; }
            set { _registerType = value; }
        }

        private string _kid;
        /// <summary>
        /// KID
        /// Index of OCR file : 17-41
        /// </summary>
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private string _warning;
        /// <summary>
        /// Warning
        /// Index of OCR file : 42.
        /// Possible Values (J/N).
        /// </summary>
        public string Warning
        {
            get { return _warning; }
            set { _warning = value; }
        }
    }
}
