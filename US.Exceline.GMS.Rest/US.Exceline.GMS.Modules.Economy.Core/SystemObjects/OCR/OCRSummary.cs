﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCRSummary
    {
        private int _newCancellationCount = 0;
        public int NewCancellationCount
        {
            get { return _newCancellationCount; }
            set { _newCancellationCount = value; }
        }

        private int _paymentsCount = 0;
        public int PaymentsCount
        {
            get { return _paymentsCount; }
            set { _paymentsCount = value; }
        }

        private decimal _paymentAmount = 0;
        public decimal PaymentAmount
        {
            get { return _paymentAmount; }
            set { _paymentAmount = value; }
        }

        private List<string> _messages = new List<string>();
        public List<string> Messages
        {
            get { return _messages; }
            set { _messages = value; }
        }
    }
}
