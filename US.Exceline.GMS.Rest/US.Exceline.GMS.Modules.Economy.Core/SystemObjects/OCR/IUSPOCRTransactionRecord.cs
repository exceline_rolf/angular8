﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public interface IUSPOCRTransactionRecord
    {

        int LineIndexOfFirstPart
        {
            get;
            set;
        }

        int LineIndexOfSecondPart
        {
            get;
            set;
        }
    }
}
