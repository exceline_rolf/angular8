﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public interface IUSPOCRStartAssignmentRecord
    {
        string FormatCode
        {
            get;
            set;
        }

        string ServiceCode
        {
            get;
            set;
        }
        string TaskType
        {
            get;
            set;
        }

        OCRRecordTypes RecordType
        {
            get;
            set;
        }

        string AgreementID
        {
            get;
            set;
        }

        string TaskNumber
        {
            get;
            set;
        }

        string RecipientAccountNumber
        {
            get;
            set;
        }
    }
}
