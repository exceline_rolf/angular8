﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public interface IUSPOCRStartTransmissionRecord
    {
        string FormatCode
        {
            get;
            set;
        }

        string ServiceCode
        {
            get;
            set;
        }

        string SendingType
        {
            get;
            set;
        }

        OCRRecordTypes RecordType
        {
            get;
            set;
        }

        string DataSender
        {
            get;
            set;
        }

        string SendingNumber
        {
            get;
            set;
        }

        string DataRecipient
        {
            get;
            set;
        }
    }
}
