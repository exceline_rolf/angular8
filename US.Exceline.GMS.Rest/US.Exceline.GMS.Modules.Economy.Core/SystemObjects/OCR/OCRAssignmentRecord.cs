﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCRAssignmentRecord : IUSPOCRAssignmentRecord
    {
        public OCRRecordTypes RecordTypeOfFirstRecord
        {
            get;
            set;
        }

        public IUSPOCRStartAssignmentRecord StartAssignmentRecord
        {
            get;
            set;
        }

        public List<IUSPOCRTransactionRecord> TransactionRecords
        {
            get;
            set;
        }

        public IUSPOCREndAssignmentRecord EndAssignmentRecord
        {
            get;
            set;
        }
    }
}
