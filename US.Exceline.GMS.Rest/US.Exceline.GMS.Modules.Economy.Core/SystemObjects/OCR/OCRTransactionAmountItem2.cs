﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCRTransactionAmountItem2 : OCRTransactionMainPart
    {
        private string _fromNo;
        /// <summary>
        /// From No. 
        /// Index of OCR file : 16-25
        /// </summary>
        public string FromNo
        {
            get { return _fromNo; }
            set { _fromNo = value; }
        }

        private string _agreementID;
        /// <summary>
        /// Agreement ID. 
        /// Index of OCR file : 26-34
        /// </summary>
        public string AgreementID
        {
            get { return _agreementID; }
            set { _agreementID = value; }
        }

        private string _filler;
        /// <summary>
        /// Index of OCR file : 35-41
        /// </summary>
        public string Filler
        {
            get { return _filler; }
            set { _filler = value; }
        }

        private string _paymentDate;
        /// <summary>
        /// Payment Date. 
        /// Index of OCR file : 42-47
        /// </summary>
        public string PaymentDate
        {
            get { return _paymentDate; }
            set { _paymentDate = value; }
        }

        private string _debetAccount;
        /// <summary>
        /// Debet Account.
        /// Index of OCR file : 48-58
        /// </summary>
        public string DebetAccount
        {
            get { return _debetAccount; }
            set { _debetAccount = value; }
        }
    }
}
