﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class USPVATClaim
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _invoiceNo;

        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }
        private DateTime _regdate;

        public DateTime Regdate
        {
            get { return _regdate; }
            set { _regdate = value; }
        }
      
        private string _creditorName;

        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }

        private string  _debtorName;
        public string DebtorName
        {
            get { return _debtorName; }
            set { _debtorName = value; }
        }

        private string _debtorFirstName = string.Empty;
        public string DebtorFirstName
        {
            get { return _debtorFirstName; }
            set { _debtorFirstName = value; }
        }

        private string _debtorLastName = string.Empty;
        public string DebtorLastName
        {
            get { return _debtorLastName; }
            set { _debtorLastName = value; }
        }


        private string _creditorNo;

        public string CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }

        private DateTime _debtorBorn;

        public DateTime DebtorBorn
        {
            get { return _debtorBorn; }
            set { _debtorBorn = value; }
        }

        private string _debtorNo;

        public string DebtorNo
        {
            get { return _debtorNo; }
            set { _debtorNo = value; }
        }

        private int _debtorEntNo = -1;
        public int DebtorEntNo
        {
            get { return _debtorEntNo; }
            set { _debtorEntNo = value; }
        }
        
        private string _creditorInkassoID;

        public string CreditorInkassoID
        {
            get { return _creditorInkassoID; }
            set { _creditorInkassoID = value; }
        }
        private string  _addr1;

        public string Addr1
        {
            get { return _addr1; }
            set { _addr1 = value; }
        }
        private string _addr2;

        public string Addr2
        {
            get { return _addr2; }
            set { _addr2 = value; }
        }

        private string _addr3;

        public string Addr3
        {
            get { return _addr3; }
            set { _addr3 = value; }
        }
       
        private string _zipCode;

        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        private string _zipName;

        public string ZipName
        {
            get { return _zipName; }
            set { _zipName = value; }
        }
        private string _telMobile;

        public string TelMobile
        {
            get { return _telMobile; }
            set { _telMobile = value; }
        }
        private string _telWork;

        public string TelWork
        {
            get { return _telWork; }
            set { _telWork = value; }
        }
        private string _telHome;

        public string TelHome
        {
            get { return _telHome; }
            set { _telHome = value; }
        }
        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private string _fax;

        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }
        private string _city;

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
         private string _mSN;

        public string MSN
        {
            get { return _mSN; }
            set { _mSN = value; }
        }

        
        private string _countryId;

        public string CountryId
        {
            get { return _countryId; }
            set { _countryId = value; }
        }
        private DateTime _invoicedDate;

        public DateTime InvoicedDate
        {
            get { return _invoicedDate; }
            set { _invoicedDate = value; }
        }
        private DateTime _dueDate;

        public DateTime DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }
        private DateTime _paidDate;

        public DateTime PaidDate
        {
            get { return _paidDate; }
            set { _paidDate = value; }
        }
        private decimal _amount;

        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

  
        private DateTime _regDate;

        public DateTime RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }
        private DateTime _debtorBirthDay;

        public DateTime DebtorBirthDay
        {
            get { return _debtorBirthDay; }
            set { _debtorBirthDay = value; }
        }
        private string _invoiceType ="VAT";

        public string InvoiceType
        {
            get { return _invoiceType; }
            set { _invoiceType = value; }
        }

        private string _NameInContract = string.Empty;
        public string NameInContract
        {
            get
            {
                return _NameInContract;
            }
            set
            {
                _NameInContract = value;
            }
        }


        private string _AccountNumber = string.Empty;
        public string AccountNumber
        {
            get
            {
                return _AccountNumber;
            }
            set
            {
                _AccountNumber = value;
            }
        }


        private string _ContractNumber = string.Empty;
        public string ContractNumber
        {
            get
            {
                return _ContractNumber;
            }
            set
            {
                _ContractNumber = value;
            }
        }


        private string _ContractExpire = string.Empty;
        public string ContractExpire
        {
            get
            {
                return _ContractExpire;
            }
            set
            {
                _ContractExpire = value;
            }
        }


        private string _LastVisit = string.Empty;
        public string LastVisit
        {
            get
            {
                return _LastVisit;
            }
            set
            {
                _LastVisit = value;
            }
        }


        private string _LastContract = string.Empty;
        public string LastContract
        {
            get
            {
                return _LastContract;
            }
            set
            {
                _LastContract = value;
            }
        }


        private string _ContractKID = string.Empty;
        public string ContractKID
        {
            get
            {
                return _ContractKID;
            }
            set
            {
                _ContractKID = value;
            }
        }

        private string _InstallmentNumber = string.Empty;
        public string InstallmentNumber
        {
            get
            {
                return _InstallmentNumber;
            }
            set
            {
                _InstallmentNumber = value;
            }
        }


        private string _InvoiceNumber = string.Empty;
        public string InvoiceNumber
        {
            get
            {
                return _InvoiceNumber;
            }
            set
            {
                _InvoiceNumber = value;
            }
        }


        private string _OriginalDueDate = string.Empty;
        public string OriginalDueDate
        {
            get
            {
                return _OriginalDueDate;
            }
            set
            {
                _OriginalDueDate = value;
            }
        }
        private decimal _InvoiceCharge = 0;
        public decimal InvoiceCharge
        {
            get
            {
                return _InvoiceCharge;
            }
            set
            {
                _InvoiceCharge = value;
            }
        }


        private decimal _ReminderFee = 0;
        public decimal ReminderFee
        {
            get
            {
                return _ReminderFee;
            }
            set
            {
                _ReminderFee = value;
            }
        }
        private string _Balance = string.Empty;
        public string Balance
        {
            get
            {
                return _Balance;
            }
            set
            {
                _Balance = value;
            }
        }


        private string _DueBalance = string.Empty;
        public string DueBalance
        {
            get
            {
                return _DueBalance;
            }
            set
            {
                _DueBalance = value;
            }
        }


        private string _LastReminder = string.Empty;
        public string LastReminder
        {
            get
            {
                return _LastReminder;
            }
            set
            {
                _LastReminder = value;
            }
        }
        private string _TransmissionNumberBBS = string.Empty;
        public string TransmissionNumberBBS
        {
            get
            {
                return _TransmissionNumberBBS;
            }
            set
            {
                _TransmissionNumberBBS = value;
            }
        }
        private string _CollectingStatus = string.Empty;
        public string CollectingStatus
        {
            get
            {
                return _CollectingStatus;
            }
            set
            {
                _CollectingStatus = value;
            }

        }
        private int _InvoiceTypeId = -1;
        public int InvoiceTypeId
        {
            get
            {
                return _InvoiceTypeId;
            }
            set
            {
                _InvoiceTypeId = value;
            }
        }



        private string _OrganizationNo = string.Empty;
        public string OrganizationNo
        {
            get
            {
                return _OrganizationNo;
            }
            set
            {
                _OrganizationNo = value;
            }
        }

        private string _InstallmentKID = string.Empty;
        public string InstallmentKID
        {
            get
            {
                return _InstallmentKID;
            }
            set
            {
                _InstallmentKID = value;
            }
        }
        private string _group = string.Empty;

        public string Group
        {
            get { return _group; }
            set { _group = value; }
        }

        private DateTime _transferDate;
        public DateTime TransferDate
        {
            get { return _transferDate; }
            set { _transferDate = value; }
        }




        public string StatusMandatory { get; set; }

        public string CollText { get; set; }

        public string BranchNo { get; set; }

        public string MessageText { get; set; }

        public string IsNeededWritenDoc { get; set; }
        public string Description { get; set; }

        private string _invoiceText = string.Empty;

        public string InvoiceText
        {
            get { return _invoiceText; }
            set { _invoiceText = value; }
        }
    }
}
