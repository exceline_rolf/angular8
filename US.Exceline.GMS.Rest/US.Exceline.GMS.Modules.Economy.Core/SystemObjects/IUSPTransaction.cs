﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public interface IUSPTransaction
    {
        DateTime TransactionDate { set; get; }
        int PaymentId { set; get; }
        int CaseNo { set; get; }
        int SubCaseNo { set; get; }
        int ARNo { set; get; }
        int ArState { set; get; }
        int ARItemNo { set; get; }
        string TransType { set; get; }
        string TransText { set; get; }
        string CID { set; get; }
        string PID { set; get; }
        int Delayed { set; get; }
        DateTime VoucherDate { set; get; }
        DateTime DueDate { set; get; }
        DateTime ReceivedDate { set; get; }
        DateTime RegDate { set; get; }
        decimal Amount { set; get; }
        string Source { set; get; }
        string FileName { set; get; }
        string DebtorAccountNo { set; get; }
        string KID { set; get; }
        string InvoiceNo { set; get; }
        string Reference1 { set; get; }
        int ApportionStatus { set; get; }
        int VoucherDetailId { set; get; }
        int VoucherNo { set; get; }
        string CreditorAccountNo { set; get; }
        DateTime NotifiedDate { set; get; }
        string Reference2 { set; get; }
        string ContractKid { set; get; }
        DateTime CancelDate { set; get; }
        string ExternalTransactionNo { set; get; }
        int VATLiability { set; get; }
        int IsPrinted { set; get; }
        string PrintFilePath { set; get; }
        string User { set; get; }
        bool GetBalance { set; get; }
        TransactionProfile TransactionProfile { set; get; }
    }
}
