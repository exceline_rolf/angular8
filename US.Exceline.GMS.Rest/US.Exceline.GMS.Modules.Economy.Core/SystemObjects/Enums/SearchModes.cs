﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public enum SearchModes
    {
        NONE,
        AR,
        CREDITORANDAR,
        CREDITOR,
        CREDITORGROUP
    }
}
