﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums
{
    public static class ExtentionMethods
    {
        public static decimal ToValidDecimalForCulture(this string value)
        {
            string tempval = string.Empty;
            string currentCul = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            if (currentCul.Equals("nb-NO"))
            {
                tempval = value.Replace(".", ",");
            }
            else
            {
                tempval = value;
            }
            try
            {
                decimal decVal = decimal.Parse(tempval);
                return decVal;
            }
            catch (Exception)
            {
                throw new Exception("Decimal conversion error. String is not a valid decimal with current culture");
            }
        }
        public static string ToShortString(this ValidationRecordTypes recordType)
        {
            switch (recordType)
            {
                case ValidationRecordTypes.EXLINE_INVOICE:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_EXLINE_INVOICE;
                case ValidationRecordTypes.EXLINE_DIRECT_DEDUCT:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_EXLINE_DIRECT_DEDUCT;
                case ValidationRecordTypes.EXLINE_DEBT_WARNING:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_EXLINE_DEBT_WARNING;
                case ValidationRecordTypes.EXLINE_INVOICE_FOR_PRINT:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_EXLINE_INVOICE_FOR_PRINT;
                case ValidationRecordTypes.EXLINE_SPONCER:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_EXLINE_SPONSER;
                case ValidationRecordTypes.EXLINE_PAYMENT_FOR_PRINT:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_EXLINE_PAYMENT_PRINT;
                default:
                    // Record types that are not in our handling scope
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_EXLINE_UNRECOGNIZED;
            }
        }

        public static string ToShortString(this OCRValidationRecordTypes recordType)
        {
            switch (recordType)
            {
                case OCRValidationRecordTypes.StartTransactionAmountItem1:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_OCR_TRANS_1;
                case OCRValidationRecordTypes.StartTransactionAmountItem2:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_OCR_TRANS_2;
                case OCRValidationRecordTypes.RecordDirectDeduct:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_OCR_NEW_CANCELL;
                default:
                    // Record types that are not in our handling scope
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_OCR_UNRECOGNIZED;
            }
        }

        public static string ToShortString(this DPValidationRecordTypes recordType)
        {
            switch (recordType)
            {
                case DPValidationRecordTypes.SM:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_EXLINE_DIRECT_PAYMENT_SM;
                case DPValidationRecordTypes.DB:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_EXLINE_DIRECT_PAYMENT_DIRECT_PAYMENT;
                default:
                    // Record types that are not in our handling scope
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_EXLINE_DIRECT_PAYMENT_UNRECOGNIZED;
            }
        }

        public static string ToShortString(this SORValidationRecordTypes recordType)
        {
            switch (recordType)
            {
                case SORValidationRecordTypes.TransactionRecord:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_SOR_TRANS;
                case SORValidationRecordTypes.UNRECOGNIZED:
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_SOR_UNRECOGNIZED;
                default:
                    // Record types that are not in our handling scope
                    return ValidationConstents.SHORT_TEXT_RECORD_TYPE_SOR_UNRECOGNIZED;
            }
        }

        public static string GetVeryFirstNonCriticalErrorMsg(this IGrouping<ValidationRecordTypes, ExlineClaimValidationRecord> list)
        {
            try
            {
                return list.First(rec => (!rec.IsValidRecord)).ErrorMessage;
            }
            catch
            {
                // no record with error messages
                return string.Empty;
            }
        }
        public static string GetVeryFirstNonCriticalErrorMsg(this IGrouping<SORValidationRecordTypes, SORValidationBasicRecord> list)
        {
            try
            {
                return list.First(rec => (!rec.IsValidRecord)).ErrorMessage;
            }
            catch
            {
                // no record with error messages
                return string.Empty;
            }
        }
        public static string GetVeryFirstNonCriticalErrorMsg(this IGrouping<OCRValidationRecordTypes, OCRValidationRecord> list)
        {
            try
            {
                return list.First(rec => (!rec.IsValidRecord)).ErrorMessage;
            }
            catch
            {
                // no record with error messages
                return string.Empty;
            }
        }
        public static string GetVeryFirstCriticalErrorMsg(this IGrouping<ValidationRecordTypes, ExlineClaimValidationRecord> list)
        {
            try
            {
                return list.First(rec => rec.HasCriticalError).CriticalErrorMessage;
            }
            catch
            {
                return string.Empty;
            }
        }
        public static string GetVeryFirstNonCriticalErrorMsg(this IGrouping<DPValidationRecordTypes, ExlineDPValidationRecord> list)
        {
            try
            {
                return list.First(rec => (!rec.IsValidRecord)).ErrorMessage;
            }
            catch
            {
                // no record with error messages
                return string.Empty;
            }
        }

        public static OCRRecordTypes ToOCRRecordTypes(this string str, int startingIndex)
        {
            try
            {
                return (OCRRecordTypes)int.Parse(str);
            }
            catch
            {
                throw new Exception("Invalid Record Type. Starting Index : " + startingIndex + ".");
            }
        }

        public static OCRValidationRecordTypes ToOCRValidationRecordTypes(this OCRRecordTypes recordType)
        {
            switch (recordType)
            {
                //case OCRRecordTypes.StartSending:
                //    break;
                //case OCRRecordTypes.StartRecordAssingment:
                //    break;
                case OCRRecordTypes.StartTransactionAmountItem1:
                    return OCRValidationRecordTypes.StartTransactionAmountItem1;

                case OCRRecordTypes.StartTransactionAmountItem2:
                    return OCRValidationRecordTypes.StartTransactionAmountItem2;

                //case OCRRecordTypes.EndRecordAssignment:
                //    break;
                //case OCRRecordTypes.EndSending:
                //    break;
                case OCRRecordTypes.RecordDirectDeduct:
                    return OCRValidationRecordTypes.RecordDirectDeduct;

                default:
                    return OCRValidationRecordTypes.UNRECOGNIZED;
                //break;
            }
        }

        public static string ToSQLDateFormat(this string str, int fieldIndex, string fieldName, bool essential)
        {
            if (str != "")
            {
                string[] parts = str.Split(new char[] { Constents.DATE_SEPERATOR_DOT });
                try
                {
                    int year = int.Parse(parts[2]);
                    int month = int.Parse(parts[1]);
                    int day = int.Parse(parts[0]);
                    return year.ToString() + "-" + month.ToString() + "-" + day.ToString();
                }
                catch
                {
                    parts = str.Split(new char[] { Constents.DATE_SEPERATOR_BLACK_SLASH });
                    try
                    {
                        int year = int.Parse(parts[2]);
                        int month = int.Parse(parts[1]);
                        int day = int.Parse(parts[0]);
                        return year.ToString() + "-" + month.ToString() + "-" + day.ToString();
                    }
                    catch
                    {
                        throw new Exception("Date is not in correct format." + " Field index : " + fieldIndex + ". Field Name : " + fieldName);
                    }
                }
            }
            else
            {
                if (essential == true)
                {
                    throw new Exception("Date is not in correct format. Field index : " + fieldIndex + ". Field Name : " + fieldName);
                }
                return "";

            }

        }

    }
}
