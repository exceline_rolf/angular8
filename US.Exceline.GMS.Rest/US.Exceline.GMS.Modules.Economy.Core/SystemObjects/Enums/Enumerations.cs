﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums
{
    public enum DirectDeductStatus
    {
        New = 1,
        Approved = 6,
        Deleted = 7
    }

    public enum CollectingStatus
    {
        NoStatus = 0,
        NewDirectDeduct = 1,
        MandatoryNotApprovedWhenFileSent = 2,
        SentButNotPaid = 3,
        MandatoryDeleted = 4,
        UnpaidInvoice = 8,
        UnpaidReminder = 9,
    }

    public enum FreezingType
    {
        FreezeDebtor,
        FreezeInvoice
    }

    public enum FreezingStatesResult
    {
        ARFreezed,
        ARUnfreezed,
        CaseFreezed,
        CaseUnfreezed
    }
    /// <summary>
    /// Type of invoices that are handled by the USP. Assigned integer value is the database
    /// Item Type equivalent
    /// </summary>
    public enum InvoiceTypes : int
    {
        DirectDeduct = 1,
        InvoiceForPrint = 2,
        DebtWarning = 4,
        PP = 14,
        Sponser = 7,
        Invoice = 3,
        DB = 11,
        SM = 12,
        OP = 5,
        OL = 8,
        CL = 13,
        PaymentDocumentForPrint = 14,
        PaymentMemo = 10,
        Default = -1,
        ClassInvoice = 100,
        ShopInvoice = 101,
        DRPInvoice = 102
    }

    public enum ErrorLevels
    {
        Error,
        Warning,
        Info
    }
    public enum ATGRecordType
    {
        NotifyByBank = 1,
        NotifyByCrediCare = 0,
        Cancellation = 2
    }

    
    public enum FreezingTypeDB
    {
        FreezeDebtor,
        FreezeInvoice
    }

    public enum TransactionTypes
    {
        TransactionGiroDebetAccount = 10,
        StandingOrder = 11,
        DirectRemit = 12,
        CompanyTerminalGiro = 13,
        PaydInBank = 14,
        DirectDeductATG = 15,
        PaydOverPhone = 16,
        PaidCashInBank = 17,
        RevercingWithKID = 18,
        PurchaseWithKID = 19,
        RevercingWithText = 20,
        PurchaseWithText = 21
    }
    public enum OCRRecordTypes
    {
        StartSending = 10,
        StartRecordAssingment = 20,
        StartTransactionAmountItem1 = 30,
        StartTransactionAmountItem2 = 31,
        EndRecordAssignment = 88,
        EndSending = 89,
        RecordDirectDeduct = 70
    }

    public enum OCRValidationRecordTypes
    {
        StartTransactionAmountItem1 = 30,
        StartTransactionAmountItem2 = 31,
        RecordDirectDeduct = 70,
        UNRECOGNIZED
    }

    public enum ValidationRecordTypes
    {
        EXLINE_INVOICE,
        EXLINE_DIRECT_DEDUCT,
        EXLINE_DEBT_WARNING,
        EXLINE_INVOICE_FOR_PRINT,
        EXLINE_SPONCER,
        EXLINE_ORDER_LINE,
        EXLINE_PAYMENT_FOR_PRINT
    }

    public enum DPValidationRecordTypes
    {
        SM,
        DB,
        UNKNOWN
    }

    public enum SORValidationRecordTypes
    {
        TransactionRecord,
        UNRECOGNIZED
    }


}
