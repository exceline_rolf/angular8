﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class USPClaim : IUSPClaim
    {
        #region IUSPClaim Members


        private string _NameInContract = string.Empty;
        public string NameInContract
        {
            get
            {
                return _NameInContract;
            }
            set
            {
                _NameInContract = value;
            }
        }


        private string _AccountNumber = string.Empty;
        public string AccountNumber
        {
            get
            {
                return _AccountNumber;
            }
            set
            {
                _AccountNumber = value;
            }
        }


        private string _ContractNumber = string.Empty;
        public string ContractNumber
        {
            get
            {
                return _ContractNumber;
            }
            set
            {
                _ContractNumber = value;
            }
        }


        private string _ContractExpire = string.Empty;
        public string ContractExpire
        {
            get
            {
                return _ContractExpire;
            }
            set
            {
                _ContractExpire = value;
            }
        }


        private string _LastVisit = string.Empty;
        public string LastVisit
        {
            get
            {
                return _LastVisit;
            }
            set
            {
                _LastVisit = value;
            }
        }


        private string _LastContract = string.Empty;
        public string LastContract
        {
            get
            {
                return _LastContract;
            }
            set
            {
                _LastContract = value;
            }
        }


        private string _ContractKID = string.Empty;
        public string ContractKID
        {
            get
            {
                return _ContractKID;
            }
            set
            {
                _ContractKID = value;
            }
        }


        private DirectDeductStatus _Status = new DirectDeductStatus();
        public DirectDeductStatus Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }


        private string _InstallmentNumber = string.Empty;
        public string InstallmentNumber
        {
            get
            {
                return _InstallmentNumber;
            }
            set
            {
                _InstallmentNumber = value;
            }
        }


        private string _InvoiceNumber = string.Empty;
        public string InvoiceNumber
        {
            get
            {
                return _InvoiceNumber;
            }
            set
            {
                _InvoiceNumber = value;
            }
        }


        private string _Text = string.Empty;
        public string Text
        {
            get
            {
                return _Text;
            }
            set
            {
                _Text = value;
            }
        }


        private string _InvoicedDate = string.Empty;
        public string InvoicedDate
        {
            get
            {
                return _InvoicedDate;
            }
            set
            {
                _InvoicedDate = value;
            }
        }


        private string _DueDate = string.Empty;
        public string DueDate
        {
            get
            {
                return _DueDate;
            }
            set
            {
                _DueDate = value;
            }
        }


        private string _PaidDate = string.Empty;
        public string PaidDate
        {
            get
            {
                return _PaidDate;
            }
            set
            {
                _PaidDate = value;
            }
        }


        private string _PrintDate = string.Empty;
        public string PrintDate
        {
            get
            {
                return _PrintDate;
            }
            set
            {
                _PrintDate = value;
            }
        }


        private string _OriginalDueDate = string.Empty;
        public string OriginalDueDate
        {
            get
            {
                return _OriginalDueDate;
            }
            set
            {
                _OriginalDueDate = value;
            }
        }


        private decimal _Amount = 0;
        public decimal Amount
        {
            get
            {
                return _Amount;
            }
            set
            {
                _Amount = value;
            }
        }


        private decimal _InvoiceCharge = 0;
        public decimal InvoiceCharge
        {
            get
            {
                return _InvoiceCharge;
            }
            set
            {
                _InvoiceCharge = value;
            }
        }


        private decimal _ReminderFee = 0;
        public decimal ReminderFee
        {
            get
            {
                return _ReminderFee;
            }
            set
            {
                _ReminderFee = value;
            }
        }


        private string _KID = string.Empty;
        public string KID
        {
            get
            {
                return _KID;
            }
            set
            {
                _KID = value;
            }
        }


        private string _TransmissionNumberBBS = string.Empty;
        public string TransmissionNumberBBS
        {
            get
            {
                return _TransmissionNumberBBS;
            }
            set
            {
                _TransmissionNumberBBS = value;
            }
        }


        private string _Balance = string.Empty;
        public string Balance
        {
            get
            {
                return _Balance;
            }
            set
            {
                _Balance = value;
            }
        }


        private string _DueBalance = string.Empty;
        public string DueBalance
        {
            get
            {
                return _DueBalance;
            }
            set
            {
                _DueBalance = value;
            }
        }


        private string _LastReminder = string.Empty;
        public string LastReminder
        {
            get
            {
                return _LastReminder;
            }
            set
            {
                _LastReminder = value;
            }
        }


        private CollectingStatus _CollectingStatus = new CollectingStatus();
        public CollectingStatus CollectingStatus
        {
            get
            {
                return _CollectingStatus;
            }
            set
            {
                _CollectingStatus = value;
            }
        }

        private string _collectingText = string.Empty;

        public string CollectingText
        {
            get { return _collectingText; }
            set { _collectingText = value; }
        }

        private string _InkassoTest = string.Empty;
        public string InkassoTest
        {
            get
            {
                return _InkassoTest;
            }
            set
            {
                _InkassoTest = value;
            }
        }


        private string _BranchNumber = string.Empty;
        public string BranchNumber
        {
            get
            {
                return _BranchNumber;
            }
            set
            {
                _BranchNumber = value;
            }
        }


        private IUSPDebtor _Debtor = new USPDebtor();
        public IUSPDebtor Debtor
        {
            get
            {
                return _Debtor;
            }
            set
            {
                _Debtor = value;
            }
        }


        private IUSPCreditor _Creditor = new USPCreditor();
        public IUSPCreditor Creditor
        {
            get
            {
                return _Creditor;
            }
            set
            {
                _Creditor = value;
            }
        }


        private List<IUSPOrderLine> _OrderLines = new List<IUSPOrderLine>();
        public List<IUSPOrderLine> OrderLines
        {
            get
            {
                return _OrderLines;
            }
            set
            {
                _OrderLines = value;
            }
        }


        private InvoiceTypes _InvoiceType = new InvoiceTypes();
        public InvoiceTypes InvoiceType
        {
            get
            {
                return _InvoiceType;
            }
            set
            {
                _InvoiceType = value;
            }
        }

        private int _InvoiceTypeId = -1;
        public int InvoiceTypeId
        {
            get
            {
                return _InvoiceTypeId;
            }
            set
            {
                _InvoiceTypeId = value;
            }
        }



        private string _OrganizationNo = string.Empty;
        public string OrganizationNo
        {
            get
            {
                return _OrganizationNo;
            }
            set
            {
                _OrganizationNo = value;
            }
        }

        private string _InstallmentKID = string.Empty;
        public string InstallmentKID
        {
            get
            {
                return _InstallmentKID;
            }
            set
            {
                _InstallmentKID = value;
            }
        }
        private string _group = string.Empty;

        public string Group
        {
            get { return _group; }
            set { _group = value; }
        }

        #endregion

        #region ITagEnabled Members


        private string _Tag1 = string.Empty;
        public string Tag1
        {
            get
            {
                return _Tag1;
            }
            set
            {
                _Tag1 = value;
            }
        }


        private string _Tag2 = string.Empty;
        public string Tag2
        {
            get
            {
                return _Tag2;
            }
            set
            {
                _Tag2 = value;
            }
        }


        private string _Tag3 = string.Empty;
        public string Tag3
        {
            get
            {
                return _Tag3;
            }
            set
            {
                _Tag3 = value;
            }
        }

        #endregion

        #region IUSPClaim Members



        private string _InvoicePath = string.Empty;
        public string InvoicePath
        {
            get
            {
                return _InvoicePath;
            }
            set
            {
                _InvoicePath = value;
            }
        }

        #endregion

        #region IUSPClaim Members



        private int _IsPrinted = 0;
        public int IsPrinted
        {
            get
            {
                return _IsPrinted;
            }
            set
            {
                _IsPrinted = value;
            }
        }

        private int _ARItemNo = -1;
        public int ARItemNo
        {
            get
            {
                return _ARItemNo;
            }
            set
            {
                _ARItemNo = value;
            }
        }

        #endregion

        #region IUSPClaim Members



        private string _InvoicePrintDate = string.Empty;
        public string InvoicePrintDate
        {
            get
            {
                return _InvoicePrintDate;
            }
            set
            {
                _InvoicePrintDate = value;
            }
        }

        #endregion



        #region IUSPClaim Members



        private string invoiceRef = string.Empty;
        public string InvoiceRef
        {
            get
            {
                return invoiceRef;
            }
            set
            {
                invoiceRef = value;
            }
        }



        #endregion

        #region IUSPClaim Members

        private List<IUSPCustomSetting> _custommSettings = new List<IUSPCustomSetting>();
        public List<IUSPCustomSetting> CustomSettings
        {
            get
            {
                return _custommSettings;
            }
            set
            {
                _custommSettings = value;
            }
        }

        public DateTime ManulaMapDueDate { get; set; }

        #endregion

        private DateTime _transferDate;
        public DateTime TransferDate
        {
            get
            {
                return _transferDate;
            }
            set
            {
                _transferDate = value;
            }
        }
    }
}
