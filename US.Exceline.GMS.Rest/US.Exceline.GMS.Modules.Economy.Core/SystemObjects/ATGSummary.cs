﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class ATGSummary
    {
        private int _count = 0;
        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }

        private decimal _sumAmount = 0;
        public decimal SumAmount
        {
            get { return _sumAmount; }
            set { _sumAmount = value; }
        }

        private List<string> logRecords = new List<string>();
        public List<string> LogRecords
        {
            get { return logRecords; }
            set { logRecords = value; }
        }
    }
}
