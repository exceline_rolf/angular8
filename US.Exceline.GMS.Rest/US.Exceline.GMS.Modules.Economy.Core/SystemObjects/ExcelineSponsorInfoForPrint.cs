﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    [DataContract]
    public class ExcelineSponsorInfoForPrint
    {
        private String _invoiceNo = String.Empty;
        public String InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        private String _viewVisit = String.Empty;
        public String ViewVisit
        {
            get { return _viewVisit; }
            set { _viewVisit = value; }
        }

        private String _gymName = String.Empty;
        public String GymName
        {
            get { return _gymName; }
            set { _gymName = value; }
        }

        private String _gymAddress1 = String.Empty;
        public String GymAddress1
        {
            get { return _gymAddress1; }
            set { _gymAddress1 = value; }
        }

        private String _gymAddress2 = String.Empty;
        public String GymAddress2
        {
            get { return _gymAddress2; }
            set { _gymAddress2 = value; }
        }

        private String _gymPostalAddress = String.Empty;
        public String GymPostalAddress
        {
            get { return _gymPostalAddress; }
            set { _gymPostalAddress = value; }
        }

        private String _gymOrganizationNo = String.Empty;
        public String GymOrganizationNo
        {
            get { return _gymOrganizationNo; }
            set { _gymOrganizationNo = value; }
        }
        private String _gymAccountNo = String.Empty;
        public String GymAccountNo
        {
            get { return _gymAccountNo; }
            set { _gymAccountNo = value; }
        }

        private String _firstName = String.Empty;
        public String FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private String _lastName = String.Empty;
        public String LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private String _address = String.Empty;
        public String Address
        {
            get { return _address; }
            set { _address = value; }
        }

        private String _sponsorAddress2 = String.Empty;
        public String SponsorAddress2
        {
            get { return _sponsorAddress2; }
            set { _sponsorAddress2 = value; }
        }

        private String _postalAddress = String.Empty;
        public String PostalAddress
        {
            get { return _postalAddress; }
            set { _postalAddress = value; }
        }

        private String _sponsorCountry = String.Empty;
        public String SponsorCountry
        {
            get { return _sponsorCountry; }
            set { _sponsorCountry = value; }
        }

        private int _arItemNo = -1;
        public int ArItemNo
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }

        private int _custID = -1;
        public int CustID
        {
            get { return _custID; }
            set { _custID = value; }
        }

        private String _invoiceGeneratedDate = String.Empty;
        public String InvoiceGeneratedDate
        {
            get { return _invoiceGeneratedDate; }
            set { _invoiceGeneratedDate = value; }
        }

        private String _dueDate = String.Empty;
        public String DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private String _reference = String.Empty;
        public String Reference
        {
            get { return _reference; }
            set { _reference = value; }
        }

        private Double _invoiceAmount = -1.00;
        public Double InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        private Double _discountTotal = -1.00;
        public Double DiscountTotal
        {
            get { return _discountTotal; }
            set { _discountTotal = value; }
        }

        private String _contactPerson = String.Empty;
        public String ContactPerson
        {
            get { return _contactPerson; }
            set { _contactPerson = value; }
        }

        private int _dicountCount = -1;
        public int DicountCount
        {
            get { return _dicountCount; }
            set { _dicountCount = value; }
        }

        private int _sponsorCount = -1;
        public int SponsorCount
        {
            get { return _sponsorCount; }
            set { _sponsorCount = value; }
        }
        private String _originalCustomer = String.Empty;
        public String OriginalCustomer
        {
            get { return _originalCustomer; }
            set { _originalCustomer = value; }
        }
        private String _orderText = String.Empty;
        public String OrderText
        {
            get { return _orderText; }
            set { _orderText = value; }
        }

        private int _isDisplayVisitNo = -1;
        public int IsDisplayVisitNo
        {
            get { return _isDisplayVisitNo; }
            set { _isDisplayVisitNo = value; }
        }

        private int _isDisplayRemain = -1;
        public int IsDisplayRemain
        {
            get { return _isDisplayRemain; }
            set { _isDisplayRemain = value; }
        }

        private String _iD = String.Empty;
        public String ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        private String _gymCode = String.Empty;
        public String GymCode
        {
            get { return _gymCode; }
            set { _gymCode = value; }
        }

        private int _gymCompanyId = -1;
        public int GymCompanyId
        {
            get { return _gymCompanyId; }
            set { _gymCompanyId = value; }
        }


        private String _branchID = String.Empty;
        public String BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }


        private String _docType = String.Empty;
        public String DocType
        {
            get { return _docType; }
            set { _docType = value; }
        }

        private int _invoiceID = -1;
        public int InvoiceID
        {
            get { return _invoiceID; }
            set { _invoiceID = value; }
        }

    }
}


    

