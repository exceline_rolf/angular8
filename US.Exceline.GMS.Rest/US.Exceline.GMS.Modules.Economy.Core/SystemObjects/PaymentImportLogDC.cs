﻿using System;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    [DataContract]
    public class PaymentImportLogDC
    {
        private int _id = -1;
        private DateTime _createdDate;
        private string _createdUser = string.Empty;
        private string _description = string.Empty;
        private string _kID = string.Empty;

        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember]
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [DataMember]
        public string KID
        {
            get { return _kID; }
            set { _kID = value; }
        }
    }
}
