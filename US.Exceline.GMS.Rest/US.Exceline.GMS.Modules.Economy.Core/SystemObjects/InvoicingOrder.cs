﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class InvoicingOrder
    {
        private int _id = -1;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _creditorNo = string.Empty;
        public string CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }
    }
}
