﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class FileDetail
    {
        private int _fileId = -1;
        public int FileId
        {
            get { return _fileId; }
            set { _fileId = value; }
        }

        private string _fileName = string.Empty;
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private int _printingStatus;
        public int PrintingStatus
        {
            get { return _printingStatus; }
            set { _printingStatus = value; }
        }

        private int _isSelectedToPrint;
        public int IsSelectedToPrint
        {
            get { return _isSelectedToPrint; }
            set { _isSelectedToPrint = value; }
        }

        private int _isPrinted;
        public int IsPrinted
        {
            get { return _isPrinted; }
            set { _isPrinted = value; }
        }
    }
}
