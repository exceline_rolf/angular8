﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    [DataContract]
    public class InvoiceInfo
    {

        private String _payerName;
        public String PayerName
        {
            get { return _payerName; }
            set { _payerName = value; }
        }

        private String _payerAdress;
        public String PayerAdress
        {
            get { return _payerAdress; }
            set { _payerAdress = value; }
        }

        private String _payerPostNumber;
        public String PayerPostNumber
        {
            get { return _payerPostNumber; }
            set { _payerPostNumber = value; }
        }

        private String _payerPostPlace = String.Empty;
        public String PayerPostPlace
        {
            get { return _payerPostPlace; }
            set { _payerPostPlace = value; }
        }

        private String _customerRefrence = String.Empty;
        public String CustomerRefrence
        {
            get { return _customerRefrence; }
            set { _customerRefrence = value; }
        }

        private String _employeeNo = String.Empty;
        public String EmployeeNo
        {
            get { return _employeeNo; }
            set { _employeeNo = value; }
        }

        private String _invoiceNumber;
        public String InvoiceNumber
        {
            get { return _invoiceNumber; }
            set { _invoiceNumber = value; }
        }
        private String _paidByDate;
        public String PaidByDate
        {
            get { return _paidByDate; }
            set { _paidByDate = value; }
        }

        private String _invoiceDate;
        public String InvoiceDate
        {
            get { return _invoiceDate; }
            set { _invoiceDate = value; }
        }

        private String _customerId;
        public String CustomerId
        {
            get { return _customerId; }
            set { _customerId = value; }
        }

        private String _receiverAccountNumber;
        public String ReceiverAccountNumber
        {
            get { return _receiverAccountNumber; }
            set { _receiverAccountNumber = value; }
        }

        private String _kID;
        public String KID
        {
            get { return _kID; }
            set { _kID = value; }
        }

        private List<InvoiceOrderLine> _orderLines = new List<InvoiceOrderLine>();
        public List<InvoiceOrderLine> OrderLines
        {
            get { return _orderLines; }
            set { _orderLines = value; }
        }

        private String _totalInvoiceAmount = String.Empty;
        public String TotalInvoiceAmount
        {
            get { return String.Format("{0:0.00}",_totalInvoiceAmount); }
            set { _totalInvoiceAmount = value; }
        }

        private SenderInformation _senderInfo;
        public SenderInformation SenderInfo
        {
            get { return _senderInfo; }
            set { _senderInfo = value; }
        }

        private bool _isATG = false;
        public bool IsATG
        {
            get { return _isATG; }
            set { _isATG = value; }
        }

        private String _paidOnBehalfOfCustId = String.Empty;
        public String PaidOnBehalfOfCustId
        {
            get { return _paidOnBehalfOfCustId; }
            set { _paidOnBehalfOfCustId = value; }
        }

        private String _paidOnBehalfOfName = String.Empty;
        public String PaidOnBehalfOfName
        {
            get { return _paidOnBehalfOfName; }
            set { _paidOnBehalfOfName = value; }
        }
    }
}
