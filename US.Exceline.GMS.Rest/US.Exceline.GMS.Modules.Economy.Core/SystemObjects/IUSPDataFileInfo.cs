﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public interface IUSPDataFileInfo
    {
        string FilePath { get; set; }
        string PluginID { get; set; }
    }
}
