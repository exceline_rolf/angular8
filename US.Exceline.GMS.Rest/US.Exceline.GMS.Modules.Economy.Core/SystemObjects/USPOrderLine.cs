﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class USPOrderLine : IUSPOrderLine, ITagEnabled
    {
        #region IUSPOrderLine Members


        private string _DiscountAmount = string.Empty;
        public string DiscountAmount
        {
            get
            {
                return _DiscountAmount;
            }
            set
            {
                _DiscountAmount = value;
            }
        }


        private string _EmployeeNo = string.Empty;
        public string EmployeeNo
        {
            get
            {
                return _EmployeeNo;
            }
            set
            {
                _EmployeeNo = value;
            }
        }


        private string _SponsorRef = string.Empty;
        public string SponsorRef
        {
            get
            {
                return _SponsorRef;
            }
            set
            {
                _SponsorRef = value;
            }
        }


        private int _Visit = -1;
        public int Visit
        {
            get
            {
                return _Visit;
            }
            set
            {
                _Visit = value;
            }
        }


        private string _LastVisit = string.Empty;
        public string LastVisit
        {
            get
            {
                return _LastVisit;
            }
            set
            {
                _LastVisit = value;
            }
        }


        private string _Amount = string.Empty;
        public string Amount
        {
            get
            {
                return _Amount;
            }
            set
            {
                _Amount = value;
            }
        }


        private string _AmountThisOrderLines = string.Empty;
        public string AmountThisOrderLines
        {
            get
            {
                return _AmountThisOrderLines;
            }
            set
            {
                _AmountThisOrderLines = value;
            }
        }
        private int _OrderLineId;
        public int OrderLineId
        {
            get
            {
                return _OrderLineId;
            }
            set
            {
                _OrderLineId = value;
            }
        }
        private int _ARItemNo;
        public int ARItemNo
        {
            get
            {
                return _ARItemNo;
            }
            set
            {
                _ARItemNo = value;
            }
        }




        private string _InkassoId = string.Empty;
        public string InkassoId
        {
            get
            {
                return _InkassoId;
            }
            set
            {
                _InkassoId = value;
            }
        }

        private string _CustId = string.Empty;
        public string CustId
        {
            get
            {
                return _CustId;
            }
            set
            {
                _CustId = value;
            }
        }

        private string _LastName = string.Empty;
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }

        private string _FirstName = string.Empty;
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        private string _NameOncontract = string.Empty;
        public string NameOncontract
        {
            get
            {
                return _NameOncontract;
            }
            set
            {
                _NameOncontract = value;
            }
        }

        private string _Address = string.Empty;
        public string Address
        {
            get
            {
                return _Address;
            }
            set
            {
                _Address = value;
            }
        }
        private string _ZipCode = string.Empty;
        public string ZipCode
        {
            get
            {
                return _ZipCode;
            }
            set
            {
                _ZipCode = value;
            }
        }

        private string _PostPlace = string.Empty;
        public string PostPlace
        {
            get
            {
                return _PostPlace;
            }
            set
            {
                _PostPlace = value;
            }
        }





        #endregion

        #region ITagEnabled Members


        private string _Tag1 = string.Empty;
        public string Tag1
        {
            get
            {
                return _Tag1;
            }
            set
            {
                _Tag1 = value;
            }
        }


        private string _Tag2 = string.Empty;
        public string Tag2
        {
            get
            {
                return _Tag2;
            }
            set
            {
                _Tag2 = value;
            }
        }


        private string _Tag3 = string.Empty;
        public string Tag3
        {
            get
            {
                return _Tag3;
            }
            set
            {
                _Tag3 = value;
            }
        }

        #endregion







        private int _sourceInstallmentID = -1;
        public int SourceInstallmentID
        {
            get
            {
                return _sourceInstallmentID;
            }
            set
            {
                _sourceInstallmentID = value;
            }
        }
    }
}
