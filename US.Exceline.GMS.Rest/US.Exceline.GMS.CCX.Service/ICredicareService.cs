﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.CCX.Service
{
    [ServiceContract]
    interface ICredicareService
    {   
        [OperationContract]
        string GetAllInvoices(int creditorNo, int chunkSize, int batchSequence);
        [OperationContract]
        string GetDebtWarningInvoices(int creditorNo, int chunkSize, int batchSequence);
        [OperationContract]
        string GetInvoices(DateTime startDate, DateTime endDate, int chunkSize);
        [OperationContract]
        string GetDirectPayments(int creditorNo, int chunkSize,int batchSequenceNo);
        [OperationContract]
        int AddPaymentStatus(string paymentStatusData, int creditorNo);
        [OperationContract]
        int AddNewCancelledStatus(string newCancelledStatus, int creditorNo);
        [OperationContract]
        string ServiceTestMethod(string testString);
        [OperationContract]
        int GetDataImportReceipt(string dataImportReceipt, int creditorNo);
        [OperationContract]
        void GetRemitFile(byte[] byteStream, int creditorNo);
        [OperationContract]
        int UpdateAddress(string addressData, int creditorNo);
       
    }
}
