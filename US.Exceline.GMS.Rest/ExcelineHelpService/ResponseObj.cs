﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ExcelineHelpService
{
    [DataContract]
    public class ResponseObj
    {
        [DataMember]
        public string Url { get; set; }
    }
}