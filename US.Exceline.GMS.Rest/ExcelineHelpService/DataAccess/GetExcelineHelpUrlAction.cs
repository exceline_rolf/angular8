﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using US_DataAccess;

namespace ExcelineHelpService.DataAccess
{
    public class GetExcelineHelpUrlAction : USDBActionBase<string>
    {
        private readonly string _namespace;
        private readonly string _version;

        public GetExcelineHelpUrlAction(string namespaceStr, string version)
        {
            _namespace = namespaceStr;
            _version = version;
        }

        protected override string Body(System.Data.Common.DbConnection dbConnection)
        {
            const string storedProcedureName = "ExceGMSGetExcelineHelpUrl";
            string url;

            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Namespace", DbType.String, _namespace));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Version", DbType.String, _version));
                var obj = command.ExecuteScalar();
                url = obj.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return url;
        }
    }
}