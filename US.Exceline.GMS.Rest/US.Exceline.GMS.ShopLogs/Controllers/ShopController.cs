﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Exceline.GMS.ShopLogs.Models;

namespace US.Exceline.GMS.ShopLogs.Controllers
{
    [RoutePrefix("api/Sales")]
    public class ShopController : ApiController
    {
        [HttpPost]
        [Route("AddShopLog")]
        public void AddShopLog(ShopLogDetails logDetails)
        {
            try
            {
                string message = logDetails.Time + ": " + logDetails.BranchId + ": " + logDetails.Reference + ": " + logDetails.Message;
                USLogError.WriteToFile(message, new Exception(), "shoplogger");
            }
            catch (Exception) { }
        }
    }
}
