﻿using bfDemoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using US.Common.Logging.API;
using US.Common.RestAPI.JsonObjects;
using US.Common.USSAdmin.API;
using US.Common.Web.UI.Core.SystemObjects;
using US.Exceline.GMS.Modules.Login.API;
using US.GMS.API;

using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.SystemObjects.UserOperations;
using US.GMS.Core.SystemObjects.USerOperations;
using US.GMS.Core.Utils;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;

namespace US.Common.RestAPI.Controllers
{
    [RoutePrefix("api/User")]
    public class USPUserController : ApiController
    {
        [HttpGet]
        [Route("GetUserInfo")]
        [Authorize]
        public HttpResponseMessage GetUserInfo(string userName)
        {
            try
            {
                USPUserDC uspUserDC = new USPUserDC();
                USImportResult<USPUserProfileInfo> uspUserProfileInfo = AdminUserSettings.GetUserProfileInfo(userName);
                if (uspUserProfileInfo.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in uspUserProfileInfo.NotificationMessages)
                    {
                        if (message.ErrorLevel == ErrorLevels.Error)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), userName);
                            errorMsg.Add(message.Message);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new USPUserDC(), GMS.Core.SystemObjects.ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    uspUserDC.UserID = uspUserProfileInfo.MethodReturnValue.UserId;
                    uspUserDC.UserName = uspUserProfileInfo.MethodReturnValue.UserName;
                    uspUserDC.ExternalUserName = uspUserProfileInfo.MethodReturnValue.ExternalUserName;
                    uspUserDC.HomePage = uspUserProfileInfo.MethodReturnValue.UserHome;
                    uspUserDC.UserID = uspUserProfileInfo.MethodReturnValue.UserId;
                    uspUserDC.Branches = uspUserProfileInfo.MethodReturnValue.Branches;
                    uspUserDC.Email = uspUserProfileInfo.MethodReturnValue.Email;
                    uspUserDC.DisplayName = uspUserProfileInfo.MethodReturnValue.DisplayName;
                    uspUserDC.IsLoginFirstTime = uspUserProfileInfo.MethodReturnValue.IsLoginFirstTime;
                    uspUserDC.IsActive = uspUserProfileInfo.MethodReturnValue.IsActive;
                    uspUserDC.Culture = uspUserProfileInfo.MethodReturnValue.Culture;
                    uspUserDC.CulturalDisplayName = uspUserProfileInfo.MethodReturnValue.CulturalDisplayName;

                    foreach (var item in uspUserProfileInfo.MethodReturnValue.ModuleList)
                    {
                        USPModuleDC mod = new USPModuleDC();
                        mod.DeafaultFeature = GetServiceFeature(item.DeafaultFeature);
                        mod.DisplayName = item.DisplayName;
                        mod.ModuleId = item.ID;
                        mod.Features = GetServiceFeatures(item.Features);
                        mod.ID = item.Name;
                        mod.ModuleHome = item.ModuleHome;
                        mod.Operations = GetServiceOparations(item.Operations);
                        mod.ThumbnailImage = item.ThumbnailImage;
                        mod.ModuleColor = item.ModuleColor;
                        mod.ModulePackage = item.ModulePackage;
                        mod.CulturalDisplayName = item.CulturalDisplayName;
                        mod.ModuleHomeURL = item.ModuleHomeURL;
                        uspUserDC.ModuleList.Add(mod);
                    }
                    uspUserDC.SubOperationList = uspUserProfileInfo.MethodReturnValue.UserSubOperationList;
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(uspUserDC, GMS.Core.SystemObjects.ApiResponseStatus.OK));

                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new USPUserDC(), GMS.Core.SystemObjects.ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("SaveUserCulture")]
        [Authorize]
        public HttpResponseMessage SaveUserCulture(UserCultureInfo cultureInfo)
        {
            try
            {
                OperationResultValue<bool> result = AdminUserSettings.SaveUserCulture(cultureInfo.culture, cultureInfo.userName);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), cultureInfo.userName);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, GMS.Core.SystemObjects.ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, GMS.Core.SystemObjects.ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, GMS.Core.SystemObjects.ApiResponseStatus.ERROR));
            }
        }

        [HttpPost]
        [Route("EditUserInfo")]
        [Authorize]
        public HttpResponseMessage EditUserBasicInfo(USPUserDC uspUser)
        {
            string decryptedNewPassword = string.Empty;
            if (uspUser.Password != null)
            {
                decryptedNewPassword = EncryptDecrypt.DecryptStringAES(uspUser.Password);
            }
            if (uspUser.OldPassword != null)
            {
                string decryptedOldPassword = EncryptDecrypt.DecryptStringAES(uspUser.OldPassword);
            }

            USPUser user = new USPUser();
            user.Id = uspUser.UserID;
            user.DisplayName = uspUser.DisplayName;
            user.email = uspUser.Email;
            user.passowrd = decryptedNewPassword;
            user.IsLoginFirstTime = uspUser.IsLoginFirstTime;
            try
            {
                OperationResultValue<bool> result = AdminUserSettings.EditUSPUserBasicInfo(uspUser.isPasswdChange, user, uspUser.loggedUser);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), uspUser.loggedUser);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, GMS.Core.SystemObjects.ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, GMS.Core.SystemObjects.ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, GMS.Core.SystemObjects.ApiResponseStatus.ERROR));
            }
        }

        
        [HttpGet]
        [Route("GetUserSelectedBranches")]
        [Authorize]
        public HttpResponseMessage GetUserSelectedBranches()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = ManageExcelineLogin.GetUserSelectedBranches(user, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetVersionNoList")]
        [Authorize]
        public HttpResponseMessage GetVersionNoList()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.GetVersionNoList(ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }



        [HttpGet]
        [Route("GetLoginGymByUser")]
        [Authorize]
        public HttpResponseMessage GetLoginGymByUser()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.GetLoginGymByUser(user, ExceConnectionManager.GetGymCode(user));
                if (result <= 0 )
                {
                    List<string> errorMsg = new List<string>();
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("UpdateSettingForUserRoutine")]
        [Authorize]
        public HttpResponseMessage UpdateSettingForUserRoutine(string key, string value)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSUser.UpdateSettingForUserRoutine(key, value, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }



        private USPFeatureDC GetServiceFeature(USPFeature uSPFeature)
        {
            if (uSPFeature == null)
            {
                return null;
            }
            USPFeatureDC feature = new USPFeatureDC();
            if (uSPFeature != null)
            {
                feature.DisplayName = uSPFeature.DisplayName;
                feature.HomeUIControl = uSPFeature.HomeUIControl;
                feature.ID = uSPFeature.Name;
                feature.FeatureId = uSPFeature.ID;
                feature.Operations = GetServiceOparations(uSPFeature.Operations);
                feature.Priority = uSPFeature.Priority;
                feature.ThumbnailImage = uSPFeature.ThumbnailImage;
                feature.FeatureColor = uSPFeature.FeaturColor;
                feature.CulturalDisplayName = uSPFeature.CulturalDisplayName;
                feature.FeatureHomeURL = uSPFeature.FeatureHomeURL;
            }
            return feature;
        }

        private List<USPFeatureDC> GetServiceFeatures(List<USPFeature> list)
        {
            if (list == null)
            {
                return null;
            }

            List<USPFeatureDC> features = new List<USPFeatureDC>();
            foreach (var item in list)
            {
                features.Add(GetServiceFeature(item));
            }
            return features;
        }

        private List<OperationDC> GetServiceOparations(List<USPOperation> list)
        {
            if (list == null)
            {
                return null;
            }
            List<OperationDC> oparations = new List<OperationDC>();
            foreach (var item in list)
            {
                oparations.Add(GetServiceOparation(item));
            }

            return oparations;
        }

        private OperationDC GetServiceOparation(USPOperation item)
        {
            if (item == null)
            {
                return null;
            }
            OperationDC operation = new OperationDC();
            operation.DisplayName = item.DisplayName;
            operation.ID = item.Name;
            operation.OperationId = item.ID;
            operation.OperationHome = item.OperationHome;
            operation.OperationNameSpace = item.OperationNameSpace;
            operation.ThumbnailImage = item.ThumbnailImage;
            operation.OperationColor = item.OperationColor;
            operation.Mode = item.Mode;
            operation.RoleId = item.RoleId;
            operation.IsAddedToWorkStation = item.IsAddedToWorkStation;
            operation.ExtendedInfo = item.OperationExInfo;
            operation.SubOperationList = GetSubOperationList(item.SubOperationList);
            operation.OperationPackage = item.OperationPackage;
            operation.Group = item.Group;
            operation.CulturalDisplayName = item.CulturalDisplayName;
            operation.OperationHomeURL = item.OperationHomeURL;
            return operation;
        }

        public List<SubOperationDC> GetSubOperationList(List<USPSubOperation> subOpList)
        {
            List<SubOperationDC> subOperationList = new List<SubOperationDC>();
            foreach (USPSubOperation uspso in subOpList)
            {
                SubOperationDC sodc = new SubOperationDC();
                sodc.Name = uspso.Name;
                sodc.DisplayName = uspso.DisplayName;
                sodc.ExtendedInfo = uspso.ExtendedInfo;
                sodc.ID = uspso.Id.ToString();
                sodc.OperationColor = uspso.OperationColor;
                sodc.OperationId = uspso.OperationID;
                sodc.ThumbnailImage = uspso.ThumbnailImage;
                sodc.SubOperationHome = uspso.SubOperationHome;
                sodc.SubOperationPackage = uspso.SubOperationPackage;
                subOperationList.Add(sodc);
            }
            return subOperationList;
        }

    }
}
