﻿using System;

namespace US.Common.Notification.Core.DomainObjects
{
    public class USNotificationSummaryInfo
    {
        public int Id { get; set; }
        public int SeverityId { get; set; }
        public string Severity { get; set; }
        public int TypeId { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string AssignTo { get; set; }
        public DateTime? LastAttendDate { get; set; }
        public DateTime? ActionDate { get; set; }
        public string MobileNo { get; set; }
        public string GymName { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }

        private bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isNew = false;
        public bool IsNew
        {
            get { return _isNew; }
            set { _isNew = value; }
        }

        private string _category = string.Empty;
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private string _createdUser = string.Empty;
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private bool _isEnabled = true;
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; }
        }

        private string _cricitalIconVisibility = "Collapsed";
        public string CriticalIconVisibility
        {
            get { return _cricitalIconVisibility; }
            set { _cricitalIconVisibility = value; }
        }

        private string _moderateIconVisibility = "Collapsed";
        public string ModerateIconVisibility
        {
            get { return _moderateIconVisibility; }
            set { _moderateIconVisibility = value; }
        }

        private string _minorIconVisibility = "Collapsed";
        public string MinorIconVisibility
        {
            get { return _minorIconVisibility; }
            set { _minorIconVisibility = value; }
        }

        private string _errorsIconVisibility = "Collapsed";
        public string ErrorsIconVisibility
        {
            get { return _errorsIconVisibility; }
            set { _errorsIconVisibility = value; }
        }

        private string _warningIconVisibility = "Collapsed";
        public string WarningIconVisibility
        {
            get { return _warningIconVisibility; }
            set { _warningIconVisibility = value; }
        }

        private string _todoIconVisibility = "Collapsed";
        public string TodoIconVisibility
        {
            get { return _todoIconVisibility; }
            set { _todoIconVisibility = value; }
        }

        private string _messagesIconVisibility = "Collapsed";
        public string MessagesIconVisibility
        {
            get { return _messagesIconVisibility; }
            set { _messagesIconVisibility = value; }
        }

    }
}
