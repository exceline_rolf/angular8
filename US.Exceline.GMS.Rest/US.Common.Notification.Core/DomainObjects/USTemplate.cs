﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core.DomainObjects
{
    public class USTemplate
    {
        public int TemplateId { get; set; }
        public string Application { get; set; }
        public string TemplateCategory { get; set; }
        public string TemplateName { get; set; }
    }
}
