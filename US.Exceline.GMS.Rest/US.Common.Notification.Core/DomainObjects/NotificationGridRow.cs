﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core.DomainObjects
{
    public class NotificationGridRow
    {
        public int NotificationId { get; set; }
        public string ModuleName{ get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TimeSpan TimeStamp { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public string Role{ get; set; }
        public DateTime LastAttendDate { get; set; }
        public string Severity { get; set; }
        public bool Status { get; set; }
        public int ChannelId { get; set; }
        public DateTime NotificationDueDate { get; set; }
        public string Occurance { get; set; }
        public string NotificationMessage { get; set; }
        public DateTime NotificationNextDueDate { get; set; }
        public string AssignChannel { get; set; }
        public bool ActiveStatus { get; set; }
        public string USCTemplateCategory { get; set; }
        public string USCTemplateName { get; set; }
        public string NotificationType { get; set; }
        public int ActionId { get; set; }
        public string Comment { get; set; }
        public string AssgnAction { get; set; }
        public string ReAssignAction { get; set; }
        public float TimeSpent { get; set; }
    }
}
