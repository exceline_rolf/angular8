﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Common.Notification.Core.DomainObjects
{
    [DataContract]
    public class ExceNotificationTepmlateDC
    {
        private int _iD = -1;
        [DataMember]
        public int ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _typeID = -1;
        [DataMember]
        public int TypeID
        {
            get { return _typeID; }
            set { _typeID = value; }
        }

        private string _text = string.Empty;
        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private int _methodID = -1;
        [DataMember]
        public int MethodID
        {
            get { return _methodID; }
            set { _methodID = value; }
        }
    }
}
