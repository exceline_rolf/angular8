﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core
{
    public class NotificationRunTimeSettings
    {
        private static Dictionary<string, string> _loadedSettings = new Dictionary<string, string>();
        public static string GetNotificationReceiver(string module)
        {
            if (_loadedSettings.ContainsKey(module.Trim()))
            {
                return _loadedSettings[module.Trim()];
            }
            else
            {
                //Get BMD by Call Actiuon Class
                return "DefaultUser";
            }
        }

        private static Dictionary<string, string> _loadTemplateCategories = new Dictionary<string, string>();
        public static string GetNotificationTemplateCategory(string module)
        {
            if (_loadTemplateCategories.ContainsKey(module.Trim()))
            {
                return _loadTemplateCategories[module.Trim()];
            }
            else
            {
                //Get BMD by Call Actiuon Class
                return "DefaultTemplateCategory";
            }
        }

        private static Dictionary<string, string> _loadTemplateNames = new Dictionary<string, string>();
        public static string GetNotificationTemplateName(string module)
        {
            if (_loadTemplateNames.ContainsKey(module.Trim()))
            {
                return _loadTemplateNames[module.Trim()];
            }
            else
            {
                //Get BMD by Call Actiuon Class
                return "DefaultTemplateName";
            }
        }
        
    }
}
