﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core.Enums
{
    public enum NotificationSeverityEnum
    {
        Critical=1,
        Moderate=2,
        Minor=3
    }
}
