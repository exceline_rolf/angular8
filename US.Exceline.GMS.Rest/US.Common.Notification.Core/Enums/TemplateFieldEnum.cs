﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core.Enums
{
    public enum TemplateFieldEnum
    {
        ACCESSTIME,
        AMOUNT,
        ARTICLENAMES,
        ARTICLENAME,
        ARTICLENUMBERS,
        BOOKINGDATE,
        BOOKINGTIME,
        COMMENT,
        CONTRACTNO,
        CONTRACTPRICE,
        CONTRACTENDDATE,
        CONTRACTTEMPLATE,
        COORDERNO,
        COST,
        CREATEDUSER,
        CREATEDDATE,
        CREDITNOTENO,
        DATE,
        DAY,
        DISCOUNT,
        DUEDATE,
        ENDDATE,
        FROMDATE,
        GYMNAME,
        EXISTMEMBERNO,
        FOLLOWUPNAME,
        GYMACCOUNT,
        INFO,
        INSTALLMENTID,
        INTRODUCEDNAME,
        INTRODUCEDNO,
        INVOICENO,
        KID,
        LASTACCESSTIME,
        LASTUPDATEDDATEAFTER,
        LASTUPDATEDDATEBEFORE,
        MEMBERNAME,
        MEMBERNO,
        MONTH,
        NEWAMOUNT,
        NEWCATEGORT,
        NEWDISCOUNTSPONSOR,
        NEWEMPNO,
        NEWGYM,
        NEWNO,
        NEWPERIODE,
        NEWREF,
        NEWSPONSOR,
        NEXTDUEDATE,
        NONE,
        NOOFMONTHS,
        NOOFORDERS,
        NOOFPENDINGORDERS,
        NTRACTNO,
        OLDAMOUNT,
        OLDCATEGORY,
        OLDDISCOUNTCATEGORY,
        OLDEMPNO,
        OLDREF,
        OLDSPONSOR,
        ONOFODRDER,
        ORDERAMOUNT,
        ORDERNO,
        ORDERPRICE,
        CURRENTPRICE,
        NEWPRICE,
        OVERDUEAMOUNT,
        PERIODE,
        PREVIOUSEENDDATE,
        PREVIOUSENO,
        PREVIOUSESTARTDATE,
        PREVIOUSGYM,
        PREVIOUSPERIODE,
        REASON,
        RENEWEFFECTIVEDATE,
        RESIGNEDDATE,
        RESOURCEID,
        STARTDATE,
        STATUSAFTER,
        STATUSBEFORE,
        STOCKCHANGE,
        TEMPLATENO,
        TIME,
        TYPE,
        FROMTIME,
        TOTIME,
        RESOURCENAME,
        UNTILDATE,
        USER,
        CATEGORY,
        FROM,
        TO,
        WHO,
        WHEN,
        PASSWORD,
        LASTVISITDATE,
        BODY,
        FIRSTNAME,
        LASTNAME,
        ORIGINALPRICE,
        CHANGEDPRICE,
        PAYMENTMETHOD,
        ATGLIMIT,
        TEMPCODE,
        ORDERNOLIST,
        NOOFDAYS,
        PUNCHES,
        OLDPUNCHES,
        PAYMENTTYPE,
        TOTALORDERAMOUNT,
        DUEDATEFORLASTORDER,
        BALANCE,
        FREEZEENDDATE,
        FREEZESTARTDATE,
        FREEZEREASON,
        CONTRACTNUMBER
    }
}
