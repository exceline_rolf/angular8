﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Shop.Data.SystemObjects;
using US.GMS.Core.DomainObjects.Admin;
using US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Data.DataAdapters.SQLServer.Commands;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageMemberships;
using SaveWithdrawalAction = US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands.SaveWithdrawalAction;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer
{
    public class SQLServerShopDataAdapter : IShopDataAdapter
    {

        #region Manage Shop Sales
        public SaleResultDC AddShopSales(String cardType, InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberBranchID, int loggedBranchID, string user, string gymCode, bool isBookingPayment)
        {
            AddShopSalesAction action = new AddShopSalesAction(cardType, installment, shopSaleDetails, paymentDetails, memberBranchID, loggedBranchID, user, gymCode, isBookingPayment);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public SaleResultDC AddSunBedShopSales(SunBedHelperObject sunbedHelperObject, SunBedBusinessHelperObject businessHelperObj)
        {
            AddSunbedShopSalesAction action = new AddSunbedShopSalesAction(sunbedHelperObject, businessHelperObj);
            return action.Execute(EnumDatabase.Exceline,sunbedHelperObject.GymCode);
        }

        public int ValidateArticleStockLevel(InstallmentDC installment, string gymCode, int branchID)
        {
            ValidateArticleStockLevelAction action = new ValidateArticleStockLevelAction(installment, branchID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ShopSalesItemDC> GetDailySales(DateTime salesDate, int branchId, string gymCode)
        {
            GetDailySalesAction action = new GetDailySalesAction(salesDate, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ShopSalesPaymentDC> GetShopBillSummary(DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            GetShopBillSummaryByDateRangeAction action = new GetShopBillSummaryByDateRangeAction(fromDate, toDate, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ShopBillDetailDC GetShopSaleBillDetails(int saleId, int branchId, string gymCode)
        {
            GetShopSaleBillDetailsBySaleAction action = new GetShopSaleBillDetailsBySaleAction(saleId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<WithdrawalDC> GetWithdrawalByMemberId(DateTime startDate, DateTime endDate, WithdrawalTypes type, int memberId, string gymCode, string user)
        {
            GetWithdrawalAction action = new GetWithdrawalAction(startDate, endDate, type, memberId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        public DailySettlementDC GetDailySettlementsBySalePointId(int salePointId, string gymCode)
        {
            GetDailySettlementAction action = new GetDailySettlementAction(salePointId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddDailySettlement(DailySettlementDC dailysettlement, string gymCode)
        {
            AddDailySettlementAction action = new AddDailySettlementAction(dailysettlement);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveWithdrawal(int branchId, WithdrawalDC withDrawal, string gymCode, string user)
        {
            SaveWithdrawalAction action = new SaveWithdrawalAction(branchId, withDrawal, user, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveOpenCashRegister(int branchId, CashRegisterDC cashRegister, string gymCode, string user)
        {
            SaveOpenCashRegisterAction action = new SaveOpenCashRegisterAction(branchId, cashRegister, user, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public Dictionary<string, decimal> GetMemberEconomyBalances(string gymCode, int memberId)
        {
            GetMemberEconomyBalancesAction action = new GetMemberEconomyBalancesAction(gymCode, memberId);
            return action.Execute(EnumDatabase.Exceline, gymCode);            
        }

        public int SaveShopInstallment(InstallmentDC installment, int branchId, string gymCode, String sessionKey)
        {
            SaveShopInstallmentAction action = new SaveShopInstallmentAction(installment, branchId, 0, sessionKey);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int GetMemberByEmployeeID(int memberId, int branchId, string gymCode,string user)
        {
            GetMemberByEmployeeIDAction action = new GetMemberByEmployeeIDAction(memberId, branchId,user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Manage Vouchers
        public List<VoucherDC> GetVouchersList(int branchId, string gymCode)
        {
            GetVouchersListAction action = new GetVouchersListAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddVoucher(int branchId, VoucherDC voucher, string gymCode)
        {
            AddVoucherAction action = new AddVoucherAction(branchId, voucher);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int EditVoucher(int branchId, VoucherDC voucher, string gymCode)
        {
            EditVoucherAction action = new EditVoucherAction(branchId, voucher);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Manage Returned
        public List<ReturnItemDC> GetReturnList(int branchId, string gymCode)
        {
            GetReturnItemsAction action = new GetReturnItemsAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddReturnitem(int branchId, ReturnItemDC returnedItem, string gymCode)
        {
            AddReturnItemAction action = new AddReturnItemAction(returnedItem, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int EditReturnitem(int branchId, ReturnItemDC editReturnedItem, string gymCode)
        {
            EditReturnedItemAction action = new EditReturnedItemAction(branchId, editReturnedItem);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        public SalePointDC GetSalesPointByMachineName(int branchID, string machineName, string gymCode)
        {
            GetSalesPointByMachineNameAction action = new GetSalesPointByMachineNameAction(branchID, machineName);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<SalePointDC> GetSalesPointList(int branchID, string gymCode)
        {
            GetSalesPointListAction action = new GetSalesPointListAction(branchID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SavePointOfSale(int branchId, SalePointDC pointOfSale, string user, string gymCode)
        {
            SavePointOfSaleAction action = new SavePointOfSaleAction(branchId, pointOfSale, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddEditUserHardwareProfile(int branchId, int hardwareProfileId, string user, string gymCode)
        {
            try
            {
                SaveUserShopHardwareProfileAction action = new SaveUserShopHardwareProfileAction(branchId, hardwareProfileId, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetSelectedHardwareProfile(int branchId, string user, string gymCode)
        {
            try
            {
                GetSelectedHardwareProfileAction action = new GetSelectedHardwareProfileAction(branchId, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int GetNextGiftVoucherNumberAction(string seqId, string subSeqId, string gymCode)
        {
            var action = new GetNextGiftVoucherNumberAction(seqId, subSeqId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ShopSalesPaymentDC> GetGiftVoucherSaleSummary(DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            var action = new GetGiftVoucherSaleSummaryAction(fromDate, toDate, branchId);
            return action.Execute(EnumDatabase.Exceline,gymCode);
        }


        public int ValidateGiftVoucherNumber(string voucherNumber,decimal payment, string gymCode, int branchID)
        {
            var action = new ValidateGiftVoucherNumberAction(voucherNumber, payment, branchID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveGiftVoucherArticle(US.GMS.Core.DomainObjects.ManageContracts.ArticleDC article, string gymCode)
        {
            var action = new SaveGiftVoucherArticleAction(article);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        public Dictionary<string, string> GetGiftVoucherDetail(string gymCode)
        {
            var action = new GetGiftVoucherDetailAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool IsCashdrawerOpen(string machinename, string gymCode, string user)
        {   
            try
            {
                var action = new IsCashdrawerOpenAction(machinename);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InvoiceForReturnDC> GetInvoicesForArticleReturn(int memberID, int branchId, string articleNo, int shopOnly, string gymCode, string user)
        {
            try
            {
                var action = new GetInvoicesForArticleReturnAction(memberID, branchId, articleNo, shopOnly);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DailySettlementPrintData ExcelineGetDailySettlementDataForPrint(String dailySettlementId, String reconiliationId, String salePointId, String branchId, String mode, String startDate, String endDate, String gymCode, String countedCashDraft)
        {
            try
            {
                var action = new ExcelineGetDailySettlementDataForPrint(dailySettlementId, reconiliationId, salePointId, branchId, mode, startDate, endDate, gymCode, countedCashDraft);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HelpDataForDailySettlmentPrintData ExcelineGetHelpDataForDailySettlmentPrintData(String branchId, String FromDateTime, String ToDateTime, String SalePointId, String gymCode)
        {
            try
            {
                var action = new  ExcelineGetHelpDataForDailySettlmentPrintDataAction( branchId, FromDateTime, ToDateTime , SalePointId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CheckIfReceiptIsValidForPrint(String invoiceNo, int branchId, String gymCode)
        {
            try
            {
                var action = new CheckIfReceiptIsValidForPrintAction(invoiceNo, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public String ExcelineGetReconcilliationTextForDailySettlementPrint(int dailSettlementId, String gymCode)
        {
            try
            {
                var action = new ExcelineGetReconcilliationTextForDailySettlementPrintAction(dailSettlementId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



    }
}
