﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateGiftVoucherNumberAction : USDBActionBase<int>
    {
        private readonly string _voucherNumber;
        private readonly decimal _payment;
        private readonly int _branchId;

        public ValidateGiftVoucherNumberAction(string voucherNumber, decimal payment, int branchID)
        {
            _voucherNumber = voucherNumber;
            _payment = payment;
            _branchId = branchID;
        }
        protected override int Body(System.Data.Common.DbConnection dBconnection)
        {
            var status = 0;
            const string spName = "USExceGMSShopValidateGiftVoucher";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GiftVoucherNumber", DbType.String, _voucherNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Payment", DbType.Decimal, _payment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                
                var obj = cmd.ExecuteScalar();
                status = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }
    }
}
