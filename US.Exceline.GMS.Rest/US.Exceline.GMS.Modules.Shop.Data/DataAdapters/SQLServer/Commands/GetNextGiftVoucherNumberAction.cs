﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetNextGiftVoucherNumberAction : USDBActionBase<int>
    {
        private readonly string _seqId = string.Empty;
        private readonly string _subSeqId = string.Empty;

        public GetNextGiftVoucherNumberAction(string seqId, string subSeqId)
        {
            _seqId = seqId;
            _subSeqId = subSeqId;
        }
        protected override int Body(System.Data.Common.DbConnection dbConnection)
        {
            var nextNo = -1;
            const string storedProcedureName = "USExceGMSGetNextVoucherNo";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SeqId", DbType.String, _seqId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SeqSubId", DbType.String, _subSeqId));

                var obj = cmd.ExecuteScalar();
                nextNo = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return nextNo;
        }
    }
}
