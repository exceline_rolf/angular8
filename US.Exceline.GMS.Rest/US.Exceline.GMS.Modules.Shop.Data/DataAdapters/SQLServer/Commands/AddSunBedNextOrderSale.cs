﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer.Commands;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class AddSunBedNextOrderSale : USDBActionBase<SaleResultDC>
    {
        private GatpurchaseShopItem _gatPuchaseShopItem;
        private ShopSalesDC _saleDetails;
        private decimal _totalAmount = 0;
        private string _user = string.Empty;
        private int _memberBemberId = -1;
        private int _loggingBranchId = -1;
        private int _memberId = -1;

        public AddSunBedNextOrderSale(GatpurchaseShopItem gatPurchaseShopItem,int memberId, decimal payModeAmout, ShopSalesDC shopSaleDetails, int memberBranchID, int loggingBranchId, string user)
        {
            _saleDetails = shopSaleDetails;
            _totalAmount = payModeAmout;
            _user = user;
            _memberBemberId = memberBranchID;
            _loggingBranchId = loggingBranchId;
            _memberId = memberId;
            _gatPuchaseShopItem = gatPurchaseShopItem;
            _gatPuchaseShopItem.IsNextOrder = true;
        }

        protected override SaleResultDC Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSShopAddNextOrderSale";
            DbTransaction transaction = null;
            SaleResultDC saleResult = new SaleResultDC();
            try
            {
                transaction = connection.BeginTransaction();
                DataTable salesItemsDT = new DataTable();
                DataColumn col = null;
                col = new DataColumn("ArticleId", typeof(Int32));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Amount", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Discount", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Quantity", typeof(Int32));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("TotalAmount", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Category", typeof(String));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Comment", typeof(String));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("IsCompaign", typeof(bool));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("DefaultPrice", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("SalePrice", typeof(Decimal));
                salesItemsDT.Columns.Add(col);

                foreach (ShopSalesItemDC item in _saleDetails.ShopSalesItemList)
                {
                    salesItemsDT.Rows.Add(item.ArticleId, item.TotalAmount, item.Discount, item.Quantity, item.TotalAmount, item.ItemCategory, item.ItemName, item.IsCompaign,item.DefaultAmount,item.SaleAmount);

                }
                //Comment is used as the item  name

                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Transaction = transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@salesItems", SqlDbType.Structured, salesItemsDT));
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberBranchID", DbType.Int32, _memberBemberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@loggingBranchId", DbType.Int32, _loggingBranchId));

                DbParameter outOrderID = new SqlParameter();
                outOrderID.ParameterName = "@NextOrderID";
                outOrderID.DbType = DbType.Int32;
                outOrderID.Value = -1;
                outOrderID.Direction = ParameterDirection.Output;
                command.Parameters.Add(outOrderID);

                DbParameter outDueDate = new SqlParameter();
                outDueDate.ParameterName = "@NextDueDate";
                outDueDate.DbType = DbType.DateTime;
                outDueDate.Direction = ParameterDirection.Output;
                command.Parameters.Add(outDueDate);
                
                DbParameter custId = new SqlParameter();
                custId.ParameterName = "@custId";
                custId.DbType = DbType.String;
                custId.Size = 20;
                custId.Direction = ParameterDirection.Output;
                command.Parameters.Add(custId);
                
                command.ExecuteNonQuery();
                int result = Convert.ToInt32(outOrderID.Value);

                if (result > 0)
                {
                    saleResult.DueDate = Convert.ToDateTime(outDueDate.Value);
                    saleResult.CustId = Convert.ToString(custId.Value);

                    AddSalesDetailsAction salesDetailAction = new AddSalesDetailsAction(_saleDetails, _loggingBranchId, _user, -1, result,string.Empty);
                    salesDetailAction.RunOnTransaction(transaction);
                    //transaction.Commit();

                    ShopTransactionDC _shopTrans = new ShopTransactionDC();
                    _shopTrans.BranchId = _loggingBranchId;
                    _shopTrans.CreatedUser = _user;
                    _shopTrans.CreatedDate = DateTime.Now;
                    _shopTrans.Mode = TransactionTypes.NEXTORDER;
                    _shopTrans.Amount = _totalAmount;
                    _shopTrans.SalePointId = _saleDetails.SalesPointId;
                    _shopTrans.EntityId = _saleDetails.EntitiyId;
                    _shopTrans.EntityRoleType = _saleDetails.EntityRoleType;
                    SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                    shopTransactionAction.RunOnTransaction(transaction);

                    //save the Gatpurchase update
                    UpdateShopAccountItemAndGatPurchaseAction updateGatPurchaseAction = new UpdateShopAccountItemAndGatPurchaseAction(_gatPuchaseShopItem);
                    bool isUpdated = updateGatPurchaseAction.RunOnTransaction(transaction);
                    if (!isUpdated)
                    {
                        transaction.Rollback();
                        saleResult.SaleStatus = SaleErrors.ERROR;
                        return saleResult;
                    }

                    transaction.Commit();
                    saleResult.SaleStatus = SaleErrors.SUCCESS;
                    saleResult.MemberId = _memberId;
                    return saleResult;
                }
                else
                {
                    transaction.Rollback();
                    saleResult.SaleStatus = SaleErrors.NOORDERFOUND;
                    return saleResult;
                }
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public SaleResultDC RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSShopAddNextOrderSale";           
            SaleResultDC saleResult = new SaleResultDC();
            try
            {                
                DataTable salesItemsDT = new DataTable();
                DataColumn col = null;
                col = new DataColumn("ArticleId", typeof(Int32));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Amount", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Discount", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Quantity", typeof(Int32));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("TotalAmount", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Category", typeof(String));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("Comment", typeof(String));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("IsCompaign", typeof(bool));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("DefaultPrice", typeof(Decimal));
                salesItemsDT.Columns.Add(col);
                col = new DataColumn("SalePrice", typeof(Decimal));
                salesItemsDT.Columns.Add(col);

                foreach (ShopSalesItemDC item in _saleDetails.ShopSalesItemList)
                {
                    salesItemsDT.Rows.Add(item.ArticleId, item.TotalAmount, item.Discount, item.Quantity, item.TotalAmount, item.ItemCategory, item.ItemName, item.IsCompaign, item.DefaultAmount, item.SaleAmount);

                }
                //Comment is used as the item  name

                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@salesItems", SqlDbType.Structured, salesItemsDT));
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberBranchID", DbType.Int32, _memberBemberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@loggingBranchId", DbType.Int32, _loggingBranchId));

                DbParameter outOrderID = new SqlParameter();
                outOrderID.ParameterName = "@NextOrderID";
                outOrderID.DbType = DbType.Int32;
                outOrderID.Value = -1;
                outOrderID.Direction = ParameterDirection.Output;
                command.Parameters.Add(outOrderID);

                DbParameter outDueDate = new SqlParameter();
                outDueDate.ParameterName = "@NextDueDate";
                outDueDate.DbType = DbType.DateTime;
                outDueDate.Direction = ParameterDirection.Output;
                command.Parameters.Add(outDueDate);

                DbParameter custId = new SqlParameter();
                custId.ParameterName = "@custId";
                custId.DbType = DbType.String;
                custId.Size = 20;
                custId.Direction = ParameterDirection.Output;
                command.Parameters.Add(custId);

                command.ExecuteNonQuery();
                int result = Convert.ToInt32(outOrderID.Value);
                saleResult.NextOrderId = result;

                if (result > 0)
                {
                    saleResult.DueDate = Convert.ToDateTime(outDueDate.Value);
                    saleResult.CustId = Convert.ToString(custId.Value);

                    AddSalesDetailsAction salesDetailAction = new AddSalesDetailsAction(_saleDetails, _loggingBranchId, _user, -1, result, string.Empty);
                    salesDetailAction.RunOnTransaction(transaction);
                    //transaction.Commit();

                    ShopTransactionDC _shopTrans = new ShopTransactionDC();
                    _shopTrans.BranchId = _loggingBranchId;
                    _shopTrans.CreatedUser = _user;
                    _shopTrans.CreatedDate = DateTime.Now;
                    _shopTrans.Mode = TransactionTypes.NEXTORDER;
                    _shopTrans.Amount = _totalAmount;
                    _shopTrans.SalePointId = _saleDetails.SalesPointId;
                    _shopTrans.EntityId = _saleDetails.EntitiyId;
                    _shopTrans.EntityRoleType = _saleDetails.EntityRoleType;
                    SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                    shopTransactionAction.RunOnTransaction(transaction);

                    //save the Gatpurchase update
                    UpdateShopAccountItemAndGatPurchaseAction updateGatPurchaseAction = new UpdateShopAccountItemAndGatPurchaseAction(_gatPuchaseShopItem);
                    bool isUpdated = updateGatPurchaseAction.RunOnTransaction(transaction);
                    if (!isUpdated)
                    {
                        transaction.Rollback();
                        saleResult.SaleStatus = SaleErrors.ERROR;
                        return saleResult;
                    }

                   // transaction.Commit();
                    saleResult.SaleStatus = SaleErrors.SUCCESS;
                    saleResult.MemberId = _memberId;
                    return saleResult;
                }
                else
                {
                   // transaction.Rollback();
                    saleResult.SaleStatus = SaleErrors.NOORDERFOUND;
                    return saleResult;
                }
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
