﻿
// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetReturnItemsAction : USDBActionBase<List<ReturnItemDC>>
    {
        private int _branchId;

        public GetReturnItemsAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ReturnItemDC> Body(DbConnection connection)
        {
            List<ReturnItemDC> ReturnItemslist = new List<ReturnItemDC>();
            string storedProcedureName = "USExceGMSShopGetReturnItems";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ReturnItemDC returnItem = new ReturnItemDC();

                    returnItem.Id = Convert.ToInt32(reader["ID"].ToString());
                    returnItem.ArticleId = Convert.ToInt32(reader["ArticleId"].ToString());
                    returnItem.Description = reader["Description"].ToString();
                    returnItem.Quantity = Convert.ToInt32(reader["Quantity"].ToString());
                    returnItem.ReturnDate = Convert.ToDateTime(reader["Date"].ToString());
                    returnItem.Comment = reader["Comment"].ToString();
                    returnItem.CategoryId = Convert.ToInt32(reader["CategoryId"].ToString());

                    ReturnItemslist.Add(returnItem);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ReturnItemslist;
        }
    }
}
