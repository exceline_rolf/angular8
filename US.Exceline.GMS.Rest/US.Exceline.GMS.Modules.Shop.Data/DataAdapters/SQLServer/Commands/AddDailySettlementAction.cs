﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 2013 -10 -17
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class AddDailySettlementAction : USDBActionBase<int>
    {
        private DailySettlementDC _dailySettlement;

        public AddDailySettlementAction(DailySettlementDC dailySettlement)
        {
            _dailySettlement = dailySettlement;
        }

        protected override int Body(DbConnection connection)
        {
            int outPutID = -1;
            string storedProcedureName = "USExceGMSShopAddDailySettlement";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Exchange", DbType.Decimal, _dailySettlement.Exchange));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Cash", DbType.Decimal, _dailySettlement.Cash));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BankTerminal", DbType.Decimal, _dailySettlement.BankTerminal));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GiftCard", DbType.Decimal, _dailySettlement.GiftCard));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OnAccount", DbType.Decimal, _dailySettlement.OnAccount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ShopReturn", DbType.Decimal, _dailySettlement.ShopReturn));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CashWithdrawal", DbType.Decimal, _dailySettlement.CashWithdrawl));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PettyCash", DbType.Decimal, _dailySettlement.PettyCash));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CashReturn", DbType.Decimal, _dailySettlement.CashReturn));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _dailySettlement.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", DbType.DateTime, _dailySettlement.ToDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TotalReceived", DbType.Decimal, _dailySettlement.TotalReceived));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TotalPayments", DbType.Decimal, _dailySettlement.TotalPayments));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountedCash", DbType.Decimal, _dailySettlement.CountedCash));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Deviation", DbType.Decimal, _dailySettlement.Deviation));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SalePointId", DbType.Int32, _dailySettlement.SalePointId));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NextOrder", DbType.Decimal, _dailySettlement.NextOrder));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _dailySettlement.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Invoice", DbType.Decimal, _dailySettlement.Invoice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SMSEmailInvoice", DbType.Decimal, _dailySettlement.SMSEmailInvoice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", DbType.DateTime, _dailySettlement.FromDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsForAllSalePoint", DbType.Boolean, _dailySettlement.IsForAllSalePoint));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TotalInvoice", DbType.Decimal, _dailySettlement.TotalInvoice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PrepaidPayment", DbType.Decimal, _dailySettlement.PrePaidPayment));

                DbParameter para = new SqlParameter();
                para.DbType = DbType.Int32;
                para.ParameterName = "@OutputID";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);
                cmd.ExecuteNonQuery();
                outPutID = Convert.ToInt32(para.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return outPutID;
        }
    }
}
