﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class SaveGiftVoucherArticleAction : USDBActionBase<int>
    {
        private readonly ArticleDC _article;

        public SaveGiftVoucherArticleAction(ArticleDC article)
        {
            _article = article;
        }

        protected override int Body(System.Data.Common.DbConnection dbConnection)
        {
            var voucherId = -1;
            const string storedProcedureName = "USExceGMSSaveGiftVoucherArticle";

            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNumber", DbType.String, _article.VoucherNumber));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherPrice", DbType.Decimal, _article.DefaultPrice));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DiscountPrice", DbType.Decimal, _article.Discount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ExpiryDate", DbType.DateTime, _article.ExpiryDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _article.MemberId));

                var obj = command.ExecuteScalar();
                voucherId = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return voucherId;
        }
    }
}
