﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 2013 -10 -17
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetDailySettlementAction : USDBActionBase<DailySettlementDC>
    {
        private int _salePointId;


        public GetDailySettlementAction(int salePointId)
        {
            _salePointId = salePointId;
        }

        protected override DailySettlementDC Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSShopGetDailySettlement";
            DailySettlementDC dailySettlement = new DailySettlementDC();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@salePointId", DbType.Int32, _salePointId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    dailySettlement.Exchange = Convert.ToDecimal(reader["Exchange"]);
                    dailySettlement.Cash = Convert.ToDecimal(reader["Cash"]);
                    dailySettlement.BankTerminal = Convert.ToDecimal(reader["BankTerminal"]);
                    dailySettlement.GiftCard = Convert.ToDecimal(reader["GiftCard"]);
                    dailySettlement.OnAccount = Convert.ToDecimal(reader["OnAccount"]);
                    dailySettlement.ShopReturn = Convert.ToDecimal(reader["ShopReturn"]);
                    dailySettlement.CashWithdrawl = Convert.ToDecimal(reader["CashWithdrawal"]);
                    dailySettlement.PettyCash = Convert.ToDecimal(reader["PettyCash"]);
                    dailySettlement.CashReturn = Convert.ToDecimal(reader["CashReturn"]);
                    dailySettlement.NextOrder = Convert.ToDecimal(reader["NextOrder"]);
                    dailySettlement.FromDateTime = Convert.ToDateTime(reader["FromDateTime"]);
                    dailySettlement.TotalReceived = Convert.ToDecimal(reader["TotalReceived"]);
                    dailySettlement.TotalPayments = Convert.ToDecimal(reader["TotalPayments"]);
                    dailySettlement.ToDateTime = Convert.ToDateTime(reader["ToDateTime"]);
                    dailySettlement.CountedCash = Convert.ToDecimal(reader["CountedCash"]);
                    dailySettlement.Deviation = Convert.ToDecimal(reader["Deviation"]);
                    dailySettlement.Invoice = Convert.ToDecimal(reader["Invoice"]);
                    dailySettlement.SMSEmailInvoice = Convert.ToDecimal(reader["SMSEmailInvoice"]);
                    dailySettlement.TotalInvoice = Convert.ToDecimal(reader["TotalInvoice"]);
                    dailySettlement.CashIn = Convert.ToDecimal(reader["CashIn"]);
                    dailySettlement.CashOut = Convert.ToDecimal(reader["CashOut"]);
                    dailySettlement.PrePaidPayment = Convert.ToDecimal(reader["PrePaidPayment"]);
                    dailySettlement.SettlmentCode = Convert.ToInt32(reader["SettlmentCode"]);                 

                    dailySettlement.CreatedDate = DateTime.Now;
                  
                    
                    
                    
                  
                   
                   
                    
                    

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dailySettlement;
        }


    }
}
