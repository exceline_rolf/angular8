﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 14/08/2014
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetShopSaleBillDetailsBySaleAction : USDBActionBase<ShopBillDetailDC>
    {

        private int _saleId = -1;
        private int _branchId = -1;

        public GetShopSaleBillDetailsBySaleAction(int saleId, int branchId)
        {
            _saleId = saleId;
            _branchId = branchId;
        }

        protected override ShopBillDetailDC Body(System.Data.Common.DbConnection connection)
        {
            var shopBillDetail = new ShopBillDetailDC();
            var saleList = new List<ShopSalesItemDC>();
            var paymentList = new List<ShopSalesPaymentDC>();
            var invoiceNo = string.Empty;
            var memberId = -1;
            var custId = string.Empty;

            // -----------------------------
            // Get Sales Item List
            // -----------------------------
            DbDataReader reader = null;
            try
            {
                var spName = "USExceGMSShopGetShopSaleBillDetailsBySale";
                var command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchID", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@saleId", System.Data.DbType.Int32, _saleId));
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ShopSalesItemDC sale = new ShopSalesItemDC();
                    if (reader["id"] != DBNull.Value)
                        sale.ShopSalesId = Convert.ToInt32(reader["id"]);
                    sale.ItemCode = Convert.ToString(reader["ArticleNo"]);
                    sale.ItemName = Convert.ToString(reader["Description"]);

                    if (reader["Quantity"] != DBNull.Value)
                        sale.Quantity = Convert.ToInt32(reader["Quantity"]);
                    if (reader["Amount"] != DBNull.Value)
                        sale.DefaultAmount = Convert.ToDecimal(reader["Amount"]);
                    if (reader["Amount"] != DBNull.Value)
                        sale.Amount = Convert.ToDecimal(reader["Amount"]);
                    if (reader["VatRate"] != DBNull.Value)
                        sale.VatRate = Convert.ToDecimal(reader["VatRate"]);
                    if (reader["Discount"] != DBNull.Value)
                        sale.Discount = Convert.ToDecimal(reader["Discount"]);
                    if (reader["TotalAmount"] != DBNull.Value)
                        sale.TotalAmount = Convert.ToDecimal(reader["TotalAmount"]);
                    if (reader["saleDate"] != DBNull.Value)
                        sale.PurchaseDate = Convert.ToDateTime(reader["saleDate"]);
                    saleList.Add(sale);
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            
            // -----------------------------
            // Get Payment Item List
            // -----------------------------
            DbDataReader reader1 = null;
            try
            {
                var spName1 = "USExceGMSShopGetShopSalePaymentDetailBySale";
                var command1 = CreateCommand(System.Data.CommandType.StoredProcedure, spName1);
                command1.Parameters.Add(DataAcessUtils.CreateParam("@branchID", System.Data.DbType.Int32, _branchId));
                command1.Parameters.Add(DataAcessUtils.CreateParam("@saleId", System.Data.DbType.Int32, _saleId));
                reader1 = command1.ExecuteReader();
                
                while (reader1.Read())
                {
                    var payment = new ShopSalesPaymentDC();

                    if (reader1["saleId"] != DBNull.Value)
                        payment.ShopSalesId = Convert.ToInt32(reader1["saleId"]);
                    if (reader1["TransType"] != DBNull.Value)
                        payment.PaymentCode = GetpaymentType(Convert.ToString(reader1["TransType"]));
                    if (reader1["DueDate"] != DBNull.Value)
                        payment.SaleDate = Convert.ToDateTime(reader1["DueDate"]);
                    if (reader1["Amount"] != DBNull.Value)
                        payment.Amount = Convert.ToDecimal(reader1["Amount"]);
                    if (reader1["invoiceNo"] != DBNull.Value && string.IsNullOrEmpty(invoiceNo))
                        invoiceNo = Convert.ToString(reader1["invoiceNo"]);
                    if (reader1["memberID"] != DBNull.Value && memberId < 0)
                        memberId = Convert.ToInt32(reader1["memberID"]);
                    if (reader1["custId"] != DBNull.Value && string.IsNullOrEmpty(custId))
                        custId = Convert.ToString(reader1["custId"]);

                    paymentList.Add(payment);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader1 != null)
                    reader1.Close();
            }

            shopBillDetail.SalesItemList = saleList;
            shopBillDetail.SalesPaymentList = paymentList;
            shopBillDetail.InvoiceNo = invoiceNo;
            shopBillDetail.MemberId = memberId;
            shopBillDetail.CustId = custId;
            shopBillDetail.SalseId = _saleId;
            shopBillDetail.Branch = _branchId;
            return shopBillDetail;
        }

        private string GetpaymentType(string transType)
        {
            switch (transType)
            {
                case "CSH":
                    return "CASH";

                case "TMN":
                    return "BANKTERMINAL";

                case "LOP":
                    return "OCR";

                case "OP":
                    return "OCR";

                case "BS":
                    return "BANKSTATEMENT";

                case "DC":
                    return "DEBTCOLLECTION";

                case "OA":
                    return "ONACCOUNT";

                case "GC":
                    return "GIFTCARD";

                case "BNK":
                    return "BANK";

                case "PB":
                    return "PREPAIDBALANCE";

                case "EML":
                    return "EMAILSMSINVOICE";

                case "NO":
                    return "NEXTORDER";

                case "SHOPINVOICE":
                    return "SHOPINVOICE";
            }
            return string.Empty;
        }

    }
}
