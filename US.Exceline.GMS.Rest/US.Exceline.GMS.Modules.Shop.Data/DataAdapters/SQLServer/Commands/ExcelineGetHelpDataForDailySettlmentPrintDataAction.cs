﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class ExcelineGetHelpDataForDailySettlmentPrintDataAction : USDBActionBase<HelpDataForDailySettlmentPrintData>
    {

        int _branchId = -1;
        int _salePointId = -1;
        DateTime _startDate = DateTime.Now;
        DateTime _endDate = DateTime.Now;


        public ExcelineGetHelpDataForDailySettlmentPrintDataAction(String branchId, String FromDateTime, String ToDateTime, String SalePointId)
        {
            /* Inputs */
            _branchId = Convert.ToInt32(branchId);
            _salePointId = Convert.ToInt32(SalePointId);
            _startDate = DateTime.ParseExact(DateTime.ParseExact(FromDateTime, "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            _endDate = DateTime.ParseExact(DateTime.ParseExact(ToDateTime, "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
        }
        protected override HelpDataForDailySettlmentPrintData Body(System.Data.Common.DbConnection dBconnection)
        {
            var helpData = new HelpDataForDailySettlmentPrintData();
            var mVADataList = new List<SalesPerMVACodeData>();
            var accountDataList = new List<SalesPerAccountData>();
            var paymentTypeDataList = new List<SalesPerPaymentTypeData>();
            var receiptData = new ReceiptsPerPeriodeForDailySettlement();

            const string storedProcedureName1 = "GetSalesPerMVASats";
            const string storedProcedureName2 = "GetSalesPerAccount";
            const string storedProcedureName3 = "GetSalesPerPaymentType";
            const string storedProcedureName4 = "GetReceiptForPeriode";
            const string storedProcedureName5 = "GetNettoSalesForPeriode";
            const string storedProcedureName6 = "GetDiscountSaleSumForPeriode";
            const string storedProcedureName7 = "GetNumberOfOpenCashDrawer";

            try
            {
                DbCommand command1 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName1);
                command1.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.String, _branchId));
                command1.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", System.Data.DbType.DateTime, _startDate));
                command1.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", System.Data.DbType.DateTime, _endDate));
                command1.Parameters.Add(DataAcessUtils.CreateParam("@SalePointId", System.Data.DbType.String, _salePointId));

                var reader1 = command1.ExecuteReader();

                while (reader1.Read())
                {
                    var mVAData = new SalesPerMVACodeData();
                    mVAData.MVASats = Convert.ToString(reader1["MVASats"]);
                    mVAData.ItemsSold = Convert.ToString(reader1["ItemsSold"]);
                    mVAData.TotalAmount = Convert.ToString(reader1["TotalAmount"]);

                    mVADataList.Add(mVAData);
                }
                reader1.Close();
                DbCommand command2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName2);
                command2.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.String, _branchId));
                command2.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", System.Data.DbType.DateTime, _startDate));
                command2.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", System.Data.DbType.DateTime, _endDate));
                command2.Parameters.Add(DataAcessUtils.CreateParam("@SalePointId", System.Data.DbType.String, _salePointId));

                var reader2 = command2.ExecuteReader();

                while (reader2.Read())
                {
                    var accountData = new SalesPerAccountData();
                    
                    accountData.AccountNo = Convert.ToString(reader2["AccountNo"]);
                    accountData.AccountName = Convert.ToString(reader2["AccountName"]);
                    accountData.ItemsSold = Convert.ToString(reader2["ItemsSold"]);
                    accountData.TotalAmount = Convert.ToString(reader2["TotalAmount"]);
                    accountData.DebitOrKredit = Convert.ToChar(reader2["DebitOrKredit"]);
                    accountData.Transaction = Convert.ToString(reader2["Transactions"]);

                    accountDataList.Add(accountData);
                }
                reader2.Close();
                DbCommand command3 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName3);
                command3.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.String, _branchId));
                command3.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", System.Data.DbType.DateTime, _startDate));
                command3.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", System.Data.DbType.DateTime, _endDate));
                command3.Parameters.Add(DataAcessUtils.CreateParam("@SalePointId", System.Data.DbType.String, _salePointId));

                var reader3 = command3.ExecuteReader();

                while (reader3.Read())
                {
                    var paymentTypeData = new SalesPerPaymentTypeData();

                    paymentTypeData.PaymentType = Convert.ToString(reader3["PaymentType"]);
                    paymentTypeData.Transactions = Convert.ToString(reader3["Transactions"]);
                    paymentTypeData.TotalAmount = Convert.ToString(reader3["TotalAmount"]);
                    paymentTypeData.DebitOrKredit = Convert.ToChar(reader3["DebitOrKredit"]);

                    paymentTypeDataList.Add(paymentTypeData);
                }
                reader3.Close();

       
                DbCommand command4 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName4);
                command4.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.String, _branchId));
                command4.Parameters.Add(DataAcessUtils.CreateParam("@SalePointId", System.Data.DbType.String, _salePointId));
                command4.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", System.Data.DbType.DateTime, _startDate));
                command4.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", System.Data.DbType.DateTime, _endDate));

                var reader4 = command4.ExecuteReader();

                while (reader4.Read())
                {
                   

                    receiptData.NumberOfReceipts = Convert.ToInt32(reader4["AntallKvitteringer"]);
                    receiptData.ReceiptAmount = Convert.ToDouble(reader4["KvitteringsBeløp"]);
                    receiptData.NumberOfCopys = Convert.ToInt32(reader4["AntallKopier"]);
                    receiptData.CopyAmount = Convert.ToDouble(reader4["KopiBeløp"]);
                    receiptData.NumberOfReturns = Convert.ToInt32(reader4["AntallRetur"]);
                    receiptData.ReturnAmount = Convert.ToDouble(reader4["ReturBeløp"]);



                }
                reader4.Close();

                DbCommand command5 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName5);
                command5.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.String, _branchId));
                command5.Parameters.Add(DataAcessUtils.CreateParam("@SalePointId", System.Data.DbType.String, _salePointId));
                command5.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", System.Data.DbType.DateTime, _startDate));
                command5.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", System.Data.DbType.DateTime, _endDate));

                var reader5 = command5.ExecuteReader();

                while (reader5.Read())
                {


                    helpData.NettoAmount = Convert.ToDouble(reader5["NettoAmount"]);




                }
                reader5.Close();

                DbCommand command6 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName6);
                command6.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.String, _branchId));
                command6.Parameters.Add(DataAcessUtils.CreateParam("@SalePointId", System.Data.DbType.String, _salePointId));
                command6.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", System.Data.DbType.DateTime, _startDate));
                command6.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", System.Data.DbType.DateTime, _endDate));

                var reader6 = command6.ExecuteReader();

                while (reader6.Read())
                {


                    helpData.DiscountedAmount = Convert.ToDouble(reader6["DiscountedAmount"]);
                    helpData.NumberOfDiscounts = Convert.ToInt32(reader6["NumberOfDiscounts"]);




                }
                reader6.Close();

                DbCommand command7 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName7);
                command7.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.String, _branchId));
                command7.Parameters.Add(DataAcessUtils.CreateParam("@SalePointId", System.Data.DbType.String, _salePointId));
                command7.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", System.Data.DbType.DateTime, _startDate));
                command7.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", System.Data.DbType.DateTime, _endDate));

                var reader7 = command7.ExecuteReader();

                while (reader7.Read())
                {


                    helpData.NumberOfOpenCashDrawer = Convert.ToInt32(reader7["NumberOfOpenCashDrawer"]);




                }
                reader7.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            helpData.PerAccount = accountDataList;
            helpData.PerMVACode = mVADataList;
            helpData.PerPaymentType = paymentTypeDataList;
            helpData.ReceiptsForPeriode = receiptData;
        
            return helpData;
        }
    }
}



