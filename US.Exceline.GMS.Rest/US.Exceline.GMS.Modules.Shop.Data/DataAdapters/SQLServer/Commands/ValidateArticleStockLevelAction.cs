﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateArticleStockLevelAction : USDBActionBase<int>
    {
        private InstallmentDC _installment;
        private int _branchID = -1;
        public ValidateArticleStockLevelAction(InstallmentDC installment, int branchID)
        {
            _installment = installment;
            _branchID = branchID;
        }
        protected override int Body(DbConnection connection)
        {
            int result = 0;
            if (_installment.AddOnList != null)
                foreach (ContractItemDC contractItem in _installment.AddOnList)
                {
                    if (contractItem.StockStatus && contractItem.Quantity > 0)
                    {
                        const string storedProcedure = "USExceGMSShopValidateArticleStockLevel";
                        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, contractItem.ArticleId));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Quantity", DbType.Int32, contractItem.Quantity));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _branchID));

                        DbParameter output = new SqlParameter();
                        output.DbType = DbType.Int32;
                        output.ParameterName = "@outId";
                        output.Size = 50;
                        output.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(output);
                        cmd.ExecuteNonQuery();
                        result = Convert.ToInt32(output.Value);
                        if (result < 1)
                        {
                            return result;
                        }
                    }
                }
            return 1;
        }
    }
}
