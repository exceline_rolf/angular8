﻿using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.IBooking;
using US.Exceline.GMS.IBooking.Data.DataAdapters;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Linq;
using System.Collections.ObjectModel;
using System.IO;
using US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.ManageMembership;
using US.GMS.Core.SystemObjects;
using System.Globalization;
using US.Exceline.GMS.IBooking.Core;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using System.Xml.Linq;
using System.Net;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using US.GMS.Core.DomainObjects.API;

namespace US.Exceline.GMS.IBooking.BusinessLogic
{
    public class ExcelineIBookingManager
    {

        private static DateTime? ContractDueDate;

        #region GetMembers
        public static OperationResult<List<ExceIBookingMember>> GetMembers(string gymCode, int branchId, int changedSinceDays, int systemId)
        {
            OperationResult<List<ExceIBookingMember>> result = new OperationResult<List<ExceIBookingMember>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingMembers(gymCode, branchId, changedSinceDays, systemId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting members - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExceIBookingMember>> GetMembers(string gymCode, int branchId, int systemId)
        {
            OperationResult<List<ExceIBookingMember>> result = new OperationResult<List<ExceIBookingMember>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingMembers(gymCode, branchId, systemId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting members - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExceIBookingMember>> GetMemberById(string gymCode, int branchId, string customerNo, int systemId)
        {
            OperationResult<List<ExceIBookingMember>> result = new OperationResult<List<ExceIBookingMember>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingMemberById(gymCode, branchId, customerNo, systemId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetCompanyDetails
        public static OperationResult<ExceIBookingSystem> GetSystemDetails(string gymCode, int systemId)
        {
            OperationResult<ExceIBookingSystem> result = new OperationResult<ExceIBookingSystem>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingSystemDetails(gymCode, systemId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting company details - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetBranches
        public static OperationResult<List<ExceIBookingGym>> GetGyms(string gymCode)
        {
            OperationResult<List<ExceIBookingGym>> result = new OperationResult<List<ExceIBookingGym>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingGyms(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting branches - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetWebOfferings
        public static OperationResult<List<ExceIBookingWebOffering>> GetWebOfferings(string gymCode)
        {
            OperationResult<List<ExceIBookingWebOffering>> result = new OperationResult<List<ExceIBookingWebOffering>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingWebOfferings(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting WebOfferings - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetInstructors
        public static OperationResult<List<ExceIBookingInstructor>> GetInstructors(string gymCode)
        {
            OperationResult<List<ExceIBookingInstructor>> result = new OperationResult<List<ExceIBookingInstructor>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingInstructors(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting instructors - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetIBookingClassCategories
        public static OperationResult<List<ExceIBookingClassCategory>> GetIBookingClassCategories(string gymCode)
        {
            OperationResult<List<ExceIBookingClassCategory>> result = new OperationResult<List<ExceIBookingClassCategory>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingClassCategories(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting class categories - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetIBookingClassKeyword
        public static OperationResult<List<ExceIBookingClassKeyword>> GetIBookingClassKeyword(string gymCode)
        {
            OperationResult<List<ExceIBookingClassKeyword>> result = new OperationResult<List<ExceIBookingClassKeyword>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingClassKeyword(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting class keywords - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetIBookingCategories
        public static OperationResult<List<CategoryDC>> GetCategoriesByType(string type, string gymCode)
        {
            OperationResult<List<CategoryDC>> result = new OperationResult<List<CategoryDC>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetCategoriesByType(type, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Categories" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetIBookingStartReasons
        public static OperationResult<List<ExceIBookingStartReason>> GetIBookingStartReasons(string gymCode)
        {
            OperationResult<List<ExceIBookingStartReason>> result = new OperationResult<List<ExceIBookingStartReason>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingStartReasons(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Start Reasons - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetClassTypes
        public static OperationResult<List<ExceIBookingClassType>> GetClassTypes(string gymCode)
        {
            OperationResult<List<ExceIBookingClassType>> result = new OperationResult<List<ExceIBookingClassType>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingClassTypes(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting class types - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetClassSchedules
        public static OperationResult<List<ExceIBookingClassCalendar>> GetClassSchedules(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            OperationResult<List<ExceIBookingClassCalendar>> result = new OperationResult<List<ExceIBookingClassCalendar>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingClassSchedules(gymCode, branchId, fromDate, toDate);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting ClassSchedules - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetExcelineShowUps
        public static OperationResult<List<ExceIBookingShowUp>> GetExcelineShowUps(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            OperationResult<List<ExceIBookingShowUp>> result = new OperationResult<List<ExceIBookingShowUp>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingExcelineShowUps(gymCode, branchId, fromDate, toDate);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Exceline Showups - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetPayments
        public static OperationResult<List<ExceIBookingPayment>> GetPayments(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            OperationResult<List<ExceIBookingPayment>> result = new OperationResult<List<ExceIBookingPayment>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingPayments(gymCode, branchId, fromDate, toDate);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Payments - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region RegisterNewIBookingMember
        public static OperationResult<string> RegisterNewIBookingMember(string gymCode, ExceIBookingNewMember member, string user)
        {
            OperationResult<string> result = new OperationResult<string>();
            result.OperationReturnValue = string.Empty;
            try
            {
                string outPut = ExcelineIBookingFacade.RegisterNewIBookingMember(gymCode, member);
                if (!string.IsNullOrEmpty(outPut))
                {
                    string[] sTextArray = outPut.Split(new char[] { '/' });
                    if (sTextArray.Length > 1)
                    {
                        try
                        {
                            int savedMemberId = Convert.ToInt32(sTextArray[0]);
                            if (savedMemberId > 0)
                            {
                                if (member.OfferingId != "-1")
                                {
                                    PackageDC template = ExcelineIBookingFacade.GetContractTemplate(member.OfferingId, Convert.ToInt32(member.GymId), gymCode, savedMemberId);
                                    if (template != null)
                                    {
                                        int guardianID = -1;
                                        if (sTextArray.Length > 3)
                                        {
                                            int.TryParse(sTextArray[3], out guardianID);
                                        }

                                        MemberContractDC contract = GenerateContract(member, template, savedMemberId, (member.FirstName + member.LastName), Convert.ToInt32(member.StartreasonId), Convert.ToInt32(member.GymId), guardianID, gymCode);
                                        if (contract != null)
                                        {
                                            int memberContractId = ManageMembershipFacade.SaveMemberContract(contract, gymCode, user).MemberContractId;
                                            if (memberContractId > 0)
                                                result.OperationReturnValue = "SUCCESS/" + outPut + "/" + memberContractId.ToString();
                                            else if (memberContractId == -2)
                                                result.OperationReturnValue = "DUPLICATECONTRACTS/" + outPut; // another basic activity found
                                            else if (memberContractId == -3)
                                                result.OperationReturnValue = "RETURNSHOP/" + outPut; // return the shop account
                                            else if (memberContractId == -4)
                                                result.OperationReturnValue = "RETURNPREPAID/" + outPut; // rerun the prepaid
                                            else if (memberContractId == -5)
                                                result.OperationReturnValue = "PAYTHESHOP/" + outPut; // pay the minus shopaccount amount
                                            else
                                                result.OperationReturnValue = "CONTRACTSAVEERROR/" + outPut;
                                        }
                                        else
                                        {
                                            result.OperationReturnValue = "CONTRACTNOTCREATED/" + outPut;
                                        }
                                    }
                                    else
                                    {
                                        result.OperationReturnValue = "NOOFFER/" + outPut;
                                    }
                                }
                                else
                                {
                                    result.OperationReturnValue = "NOOFFER/" + outPut;
                                }
                            }
                            else
                            {
                                result.OperationReturnValue = "MEMBERNOTSAVED";
                            }
                        }
                        catch (Exception ex)
                        {
                            result.OperationReturnValue = "CONTRACTGENERATIONERROR";
                            AddStatus(member, 0, "Error in contract generation process, " + ex.Message, gymCode);
                        }
                    }
                    else
                    {
                        result.OperationReturnValue = "MEMBERSAVEERROR1";
                    }
                }
                else
                {
                    result.OperationReturnValue = "MEMBERSAVEERROR2";
                }
            }
            catch (Exception ex)
            {
                AddStatus(member, 0, "Error in member registration" + ex.Message, gymCode);
            }
            return result;
        }
        #endregion

        #region UpdateIBookingClass
        public static OperationResult<int> UpdateIBookingClass(string gymCode, ExceIBookingUpdateClass exceClass)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.UpdateIBookingClass(gymCode, exceClass);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating class - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region UpdateIBookingMemberClassVisit
        public static OperationResult<int> UpdateIBookingMemberClassVisit(string gymCode, ExceIBookingMemberClassVisit visit)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.UpdateIBookingMemberClassVisit(gymCode, visit);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating member class visit - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region UpdateIBookingMemberVisit
        public static OperationResult<int> UpdateIBookingMemberVisit(string gymCode, ExceIBookingMemberVisit visit)
        {
            ContractSummaryDC selectedContract = null;
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                List<ContractSummaryDC> contractSummaries = new List<ContractSummaryDC>();
                DateTime visitDate = Convert.ToDateTime(visit.VisitDateTime);// DateTime.ParseExact(visit.VisitDateTime, "yyyy-MM-dd hh:mm:ss,fff", CultureInfo.InvariantCulture);

                contractSummaries = ExcelineIBookingFacade.GetContractSummaries(visit.CustomerNo, gymCode);
                foreach (ContractSummaryDC contract in contractSummaries)
                {
                    if (contract.AccessProfileId > 0)
                    {
                        if (!contract.IsFreezed && contract.StartDate <= visitDate && contract.EndDate >= visitDate)
                        {
                            ContractAccessTimeDC accessTime = GetAccessTime(contract, visitDate);
                            if (accessTime != null)
                            {
                                if ((visitDate.TimeOfDay >= accessTime.InTime.Value.TimeOfDay) && (visitDate.TimeOfDay < accessTime.OutTime.Value.TimeOfDay))
                                {
                                    selectedContract = contract;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (selectedContract == null)
                {
                    foreach (ContractSummaryDC contract in contractSummaries)
                    {
                        if (contract.AccessProfileId == 0 || contract.AccessProfileId < 0)
                        {
                            if (!contract.IsFreezed && contract.StartDate <= visitDate && contract.EndDate >= visitDate)
                            {
                                ContractAccessTimeDC accessTime = GetAccessTime(contract, visitDate);
                                if (accessTime != null)
                                {
                                    if ((visitDate.TimeOfDay >= accessTime.InTime.Value.TimeOfDay) && (visitDate.TimeOfDay < accessTime.OutTime.Value.TimeOfDay))
                                    {
                                        selectedContract = contract;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                EntityVisitDC excelinevisit = null;
                if (selectedContract != null)
                {
                    if (selectedContract.ContractTypeCode == "PUNCHCARD")
                    {
                        if (selectedContract.RemainingVisits > 0)
                        {
                            excelinevisit = new EntityVisitDC();
                            excelinevisit.CutomerNo = visit.CustomerNo;
                            excelinevisit.VisitDate = visitDate;
                            excelinevisit.InTime = visitDate;
                            excelinevisit.BranchId = Convert.ToInt32(visit.GymId);
                            excelinevisit.MemberContractId = selectedContract.Id;
                            excelinevisit.ItemName = selectedContract.TemplateName;
                            excelinevisit.ActivityId = selectedContract.ActivityId;
                            excelinevisit.VisitType = "CON";
                            excelinevisit.CountVist = true;
                        }
                    }
                    else
                    {
                        excelinevisit = new EntityVisitDC();
                        excelinevisit.CutomerNo = visit.CustomerNo;
                        excelinevisit.VisitDate = visitDate;
                        excelinevisit.InTime = visitDate;
                        excelinevisit.BranchId = Convert.ToInt32(visit.GymId);
                        excelinevisit.MemberContractId = selectedContract.Id;
                        excelinevisit.ItemName = selectedContract.TemplateName;
                        excelinevisit.ActivityId = selectedContract.ActivityId;
                        excelinevisit.VisitType = "CON";
                        excelinevisit.CountVist = true;
                    }
                }
                else
                {
                    excelinevisit = new EntityVisitDC();
                    excelinevisit.CutomerNo = visit.CustomerNo;
                    excelinevisit.BranchId = Convert.ToInt32(visit.GymId);
                    excelinevisit.VisitDate = visitDate;
                    excelinevisit.InTime = visitDate;
                    excelinevisit.VisitType = "IB";
                    excelinevisit.CountVist = true;
                }
                if (excelinevisit != null)
                {
                    result.OperationReturnValue = ExcelineIBookingFacade.UpdateIBookingMemberVisit(gymCode, excelinevisit);
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating member visit - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        private static ContractAccessTimeDC GetAccessTime(ContractSummaryDC contract, DateTime intime)
        {
            switch (intime.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Monday");

                case DayOfWeek.Tuesday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Tuesday");

                case DayOfWeek.Wednesday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Wednesday");

                case DayOfWeek.Thursday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Thursday");

                case DayOfWeek.Friday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Friday");

                case DayOfWeek.Saturday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Saturday");

                case DayOfWeek.Sunday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Sunday");
            }
            return null;
        }
        #endregion

        #region UpdateIBookingMember
        public static OperationResult<int> UpdateIBookingMember(string gymCode, ExceIBookingUpdateMember member)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.UpdateIBookingMember(gymCode, member);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating member - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetGymCodeByCompanyId
        public static OperationResult<string> GetGymCodeByCompanyId(string systemId)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetGymCodeByCompanyId(systemId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Exceline GymCode - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetEntityValid
        public static OperationResult<bool> GetEntityValid(int entityId, string entityType, int branchId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingEntityValid(entityId, entityType, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in checking entity exist - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region DeleteClassBooking
        public static OperationResult<int> DeleteIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.DeleteIBookingClassBooking( systemId,  gymId,  classId,  customerId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in cancelling contract - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region CancelContract
        public static OperationResult<ContractResignDetails> CancelIBookingContract(int systemId, int gymId, int customerId, string gymCode)
        {
            OperationResult<ContractResignDetails> result = new OperationResult<ContractResignDetails>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.CancelIBookingContract(systemId, gymId, customerId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in cancelling contract - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region AddClassBooking
        public static OperationResult<int> AddIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.AddIBookingClassBooking( systemId,  gymId,  classId,  customerId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in cancelling contract - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion


        private static MemberContractDC GenerateContract(ExceIBookingNewMember member, PackageDC template, int memberId, string memberName, int signUpCategoryId, int branchId, int guardianID, string gymCode)
        {
            try
            {
                if (template != null)
                {
                    if (template.EveryMonthItemList != null)
                    {
                        (template.EveryMonthItemList.FirstOrDefault(x => x.ArticleId == template.ArticleNo)).IsActivityArticle = true;
                    }

                    MemberContractDC MemberContract = new MemberContractDC();
                    MemberContract.StartUpItemList = new List<ContractItemDC>();
                    MemberContract.EveryMonthItemList = new List<ContractItemDC>();

                    MemberContract.IsATG = template.IsATG;
                    MemberContract.MemberId = memberId;
                    MemberContract.BranchName = template.BranchName;
                    MemberContract.ContractId = template.PackageId;
                    MemberContract.NumberOfVisits = template.NumOfVisits;
                    MemberContract.ContractName = template.PackageName;
                    MemberContract.BranchId = branchId;
                    MemberContract.EnrollmentFee = template.EnrollmentFee;
                    MemberContract.NoOfInstallments = template.NumOfInstallments;
                    MemberContract.NoofDays = template.NoOfDays;
                    MemberContract.ContractPrize = template.PackagePrice;
                    MemberContract.CreatedDate = DateTime.Today;

                    if (template.FixStartDateOfContract.HasValue)
                        MemberContract.ContractStartDate = template.FixStartDateOfContract.Value;
                    else
                        MemberContract.ContractStartDate = DateTime.Today;

                    if (template.FixDateOfContract.HasValue)
                        MemberContract.ContractEndDate = template.FixDateOfContract.Value;
                    else if (template.NoOfMonths != 0)
                        MemberContract.ContractEndDate = MemberContract.ContractStartDate.AddMonths(template.NoOfMonths);
                    else if (template.NoOfDays != 0)
                        MemberContract.ContractEndDate = MemberContract.ContractStartDate.AddDays(template.NoOfDays);

                    MemberContract.ActivityId = template.ActivityId;
                    MemberContract.ActivityName = template.ActivityName;
                    MemberContract.ContractTypeId = template.ContractTypeValue.Id;
                    MemberContract.RestPlusMonth = template.RestPlusMonth;
                    MemberContract.LockInPeriod = template.LockInPeriod;
                    MemberContract.GuarantyPeriod = template.PriceGuaranty;

                    MemberContract.AutoRenew = template.AutoRenew;
                    MemberContract.IsInvoiceDetails = template.IsInvoiceDetail;
                    MemberContract.ArticleId = template.ArticleNo;
                    MemberContract.ArticleName = template.ArticleText;
                    MemberContract.AccessProfileId = template.AccessProfileId;
                    MemberContract.NoOfMonths = template.NoOfMonths;
                    MemberContract.FixDateOfContract = template.FixDateOfContract;
                    MemberContract.FirstDueDate = template.FirstDueDate;
                    MemberContract.SecondDueDate = template.SecondDueDate;
                    MemberContract.CreditoDueDays = template.CreditDueDays;
                    MemberContract.DueDateSetting = template.CreditDueDateSetting;

                    MemberContract.StartUpItemPrice = template.StartUpItemPrice;
                    MemberContract.EveryMonthItemPrice = template.EveryMonthItemsPrice;
                    MemberContract.LockInPeriodUntilDate = MemberContract.ContractStartDate.AddMonths(template.LockInPeriod);
                    MemberContract.PriceGuarantyUntillDate = MemberContract.ContractStartDate.AddMonths(template.PriceGuaranty);

                    MemberContract.StartUpItemList = template.StartUpItemList;
                    MemberContract.StartUpItemPrice = template.StartUpItemPrice;
                    MemberContract.EveryMonthItemList = template.EveryMonthItemList;
                    MemberContract.EveryMonthItemPrice = template.EveryMonthItemsPrice;
                    MemberContract.ContractTypeName = template.ContractTypeValue.Code.ToString();
                    MemberContract.AmountPerInstallment = template.RatePerInstallment;
                    MemberContract.OrderPrice = template.OrderPrice;
                    MemberContract.ContractPrize = template.PackagePrice;
                    MemberContract.ContractDescription = template.ContractDescritpion;
                    MemberContract.TemplateNo = template.TemplateNumber;
                    MemberContract.CategoryId = template.PackageCategory.Id;
                    MemberContract.ContractCategoryName = template.PackageCategory.Name;
                    MemberContract.ContractTypeCode = template.PackageCategory.Code;
                    MemberContract.SignUpCategoryId = signUpCategoryId;
                    MemberContract.CreditPeriod = template.CreditPeriod;
                    MemberContract.MemberCreditPeriod = template.MemberCreditPeriod;
                    MemberContract.NextTemplateId = template.NextTemplateId;
                    MemberContract.CreatedUser = "ibooking";
                    MemberContract.UseTodayAsDueDate = template.UseTodayAsDueDate;
                    MemberContract.PostPay = template.PostPay;
                    SetDueDate(MemberContract);

                    if (MemberContract.OrderPrice == 0)
                        MemberContract.OrderPrice = template.PackagePrice;

                    if (!MemberContract.FixDateOfContract.HasValue)
                    {
                        SetContractEndDate(MemberContract.NoOfMonths, MemberContract);
                    }
                    else if(MemberContract.FixDateOfContract <= DateTime.Today)
                    {
                        SetContractEndDate(MemberContract.NoOfMonths, MemberContract);
                    }
                    GenerateOrders(MemberContract, memberId, memberName, guardianID);
                    SetTrainingDates(MemberContract.InstalllmentList, MemberContract);
                    return MemberContract;
                }
                else
                {
                    AddStatus(member, 0, "Error in Contract generation : Template not found", gymCode);
                    return null;
                }
            }
            catch (Exception ex)
            {
                AddStatus(member, 0, "Error in Contract generation : " + ex.Message, gymCode);
                return null;
            }
        }


        private static void SetTrainingDates(List<InstallmentDC> installments, MemberContractDC memberContract)
        {
            if (installments != null && memberContract != null)
            {
                int generatedOrderscount = installments.Count;
                DateTime traininfStartDate = memberContract.ContractStartDate;
                DateTime trainingEndDate = memberContract.ContractEndDate;

                if (generatedOrderscount < memberContract.NoOfMonths && generatedOrderscount > 0)
                {
                    int noofSections = memberContract.NoOfMonths / generatedOrderscount;
                    InstallmentDC lastInstallment = installments[installments.Count - 1];
                    foreach (InstallmentDC installment in installments)
                    {
                        installment.TrainingPeriodStart = traininfStartDate;

                        if (lastInstallment == installment)
                        {
                            trainingEndDate = memberContract.ContractEndDate;
                            installment.TrainingPeriodEnd = trainingEndDate;
                            if (installment.TrainingPeriodEnd < installment.TrainingPeriodStart)
                                installment.TrainingPeriodEnd = installment.TrainingPeriodStart;
                            installment.Text = installment.TrainingPeriodStart.Value.ToShortDateString() + " - " + installment.TrainingPeriodEnd.Value.ToShortDateString();
                        }
                        else
                        {
                            trainingEndDate = (traininfStartDate.AddMonths(noofSections)).AddDays(-1);
                            installment.TrainingPeriodEnd = trainingEndDate;
                            if (installment.TrainingPeriodEnd < installment.TrainingPeriodStart)
                                installment.TrainingPeriodEnd = installment.TrainingPeriodStart;
                            installment.Text = installment.TrainingPeriodStart.Value.ToShortDateString() + " - " + installment.TrainingPeriodEnd.Value.ToShortDateString();
                            traininfStartDate = trainingEndDate.AddDays(1);
                        }
                    }
                }
                else if (generatedOrderscount > 0)
                {
                    InstallmentDC lastInstallment = installments[installments.Count - 1];
                    foreach (InstallmentDC installment in installments)
                    {
                        if (lastInstallment == installment)
                        {
                            trainingEndDate = memberContract.ContractEndDate;
                            installment.TrainingPeriodEnd = trainingEndDate;
                            if (installment.TrainingPeriodEnd < installment.TrainingPeriodStart)
                                installment.TrainingPeriodEnd = installment.TrainingPeriodStart;
                            installment.Text = installment.TrainingPeriodStart.Value.ToShortDateString() + " - " + installment.TrainingPeriodEnd.Value.ToShortDateString();
                        }
                    }
                }
            }
        }


        private static void SetContractEndDate(int noofMonths, MemberContractDC MemberContract)
        {
            if (MemberContract.RestPlusMonth && noofMonths > 0)
            {
                int noofDaysForMonth = DateTime.DaysInMonth(MemberContract.ContractStartDate.Year, MemberContract.ContractStartDate.Month);
                int restDays = (noofDaysForMonth - MemberContract.ContractStartDate.Day);

                MemberContract.ContractEndDate = MemberContract.ContractStartDate.AddDays(restDays).AddMonths(noofMonths);

                int noofDaysforEndDateMonth = DateTime.DaysInMonth(MemberContract.ContractEndDate.Year, MemberContract.ContractEndDate.Month);
                if (noofDaysforEndDateMonth != MemberContract.ContractEndDate.Day)
                    MemberContract.ContractEndDate = MemberContract.ContractEndDate.AddDays(noofDaysforEndDateMonth - MemberContract.ContractEndDate.Day);
            }
            else if (noofMonths > 0)
            {
                MemberContract.ContractEndDate = MemberContract.ContractStartDate.AddMonths(noofMonths);
            }
            else if (MemberContract.NoofDays > 0)
            {
                MemberContract.ContractEndDate = MemberContract.ContractStartDate.AddDays(MemberContract.NoofDays);
            }
        }


        private static void SetDueDate(MemberContractDC MemberContract)
        {
            if (MemberContract.FirstDueDate.HasValue)// use the templates's first due date
            {
                ContractDueDate = MemberContract.FirstDueDate;
            }
            else if (MemberContract.CreditoDueDays > 0)// get the setting from the template
            {
                ContractDueDate = MemberContract.ContractStartDate.AddDays(MemberContract.CreditoDueDays);
            }
            else if (MemberContract.UseTodayAsDueDate)
            {
                ContractDueDate = DateTime.Today;
            }
            else if (MemberContract.DueDateSetting > 0)
            {
                ContractDueDate = new DateTime(MemberContract.ContractStartDate.Year, (MemberContract.ContractStartDate.Month), MemberContract.DueDateSetting);
                if (ContractDueDate < DateTime.Today)
                    ContractDueDate = ContractDueDate.Value.AddMonths(1);
            }
            else
            {
                ContractDueDate = MemberContract.ContractStartDate;
            }
        }

        private static void GenerateOrders(MemberContractDC MemberContract, int memberId, string memberName, int guardianID)
        {
            decimal ContractServicePrice = 0;
            decimal ContractItemPrice = 0;
            List<InstallmentDC> _installmentList;
            DateTime trainingStartDate = MemberContract.ContractStartDate;
            decimal restplusAmount = 0;
            DateTime trainingEndDate = MemberContract.ContractStartDate.AddMonths(1).AddDays(-1);
            if (MemberContract != null)
            {
                _installmentList = new List<InstallmentDC>();
                try
                {
                    if (MemberContract.ContractTypeCode != "PUNCHCARD")
                    {
                        decimal installmentAmount = MemberContract.OrderPrice / MemberContract.NoOfInstallments;

                        InstallmentDC installment1 = new InstallmentDC();
                        installment1.IsATG = false;
                        installment1.OrderOperationsEnabled = false;
                        installment1.InstallmentNo = 1;
                        installment1.InstallmentId = 1;
                        installment1.OriginalCustomer = memberName;
                        installment1.MemberId = memberId;
                        installment1.PayerId = (guardianID > 0) ? guardianID : memberId;
                        installment1.MemberContractId = MemberContract.Id;
                        installment1.MemberContractNo = MemberContract.MemberContractNo;
                        installment1.InstallmentDate = MemberContract.ContractStartDate;
                        installment1.TemplateId = MemberContract.ContractId;
                        //Setting the Due Date
                        if (ContractDueDate.HasValue)
                        {
                            installment1.OriginalDueDate = ContractDueDate.Value;
                            installment1.DueDate = ContractDueDate.Value;
                        }
                        else
                        {
                            installment1.OriginalDueDate = MemberContract.ContractStartDate;
                            installment1.DueDate = MemberContract.ContractStartDate;
                        }

                        installment1.BillingDate = DateTime.Today;
                        installment1.TrainingPeriodStart = trainingStartDate;
                        installment1.TrainingPeriodEnd = trainingEndDate;
                        installment1.AddOnList = new List<ContractItemDC>();
                        installment1.Amount = 0;

                        if (MemberContract.RestPlusMonth)
                        {
                            int noofDaysForMonth = DateTime.DaysInMonth(MemberContract.ContractStartDate.Year, MemberContract.ContractStartDate.Month);
                            int restDays = (noofDaysForMonth - MemberContract.ContractStartDate.Day) + 1;

                            ContractItemDC activityArticles = MemberContract.EveryMonthItemList.FirstOrDefault(X => X.ActivityID == MemberContract.ActivityId);
                            if (activityArticles != null)
                            {
                                if (MemberContract.ContractStartDate.Day == 1)
                                {
                                    restplusAmount = activityArticles.Price;
                                }
                                else
                                {
                                    decimal amountPerDay = decimal.Round((activityArticles.Price / 30), 2);
                                    restplusAmount = restDays * amountPerDay;
                                }
                            }
                            else
                            {
                                restplusAmount = 0;
                            }

                            MemberContract.ContractPrize = restplusAmount;
                            MemberContract.EveryMonthItemPrice = restplusAmount;
                            MemberContract.RestPlusMonthAmount = restplusAmount;
                            MemberContract.RestPlusMonthAmount = restplusAmount;
                            trainingEndDate = (MemberContract.ContractStartDate.AddDays(restDays - 1));
                            DateTime nextMonth = trainingEndDate.AddDays(1);
                            int noofDaysForNextMonth = DateTime.DaysInMonth(nextMonth.Year, nextMonth.Month);
                            trainingEndDate = trainingEndDate.AddDays(noofDaysForNextMonth);
                            installment1.TrainingPeriodEnd = trainingEndDate;

                            if (MemberContract.NoOfMonths > 0)
                                MemberContract.ContractEndDate = trainingEndDate.AddDays(1).AddMonths(MemberContract.NoOfMonths - 1).AddDays(-1);

                        }

                        installment1.Text = installment1.TrainingPeriodStart.Value.ToShortDateString() + " - " + installment1.TrainingPeriodEnd.Value.ToShortDateString();

                        int Priority = 1;
                        if (MemberContract.StartUpItemList != null)
                        {
                            foreach (ContractItemDC item in MemberContract.StartUpItemList)
                            {
                                ContractItemDC startUpItem = DeepCopy(item);
                                startUpItem.Priority = Priority;
                                installment1.AddOnList.Add(startUpItem);
                                installment1.AdonPrice += startUpItem.Price;
                                installment1.Amount += startUpItem.Price;
                                installment1.ItemAmount += startUpItem.Price;
                                ContractItemPrice += startUpItem.Price;
                                Priority++;
                            }
                        }

                        if (MemberContract.EveryMonthItemList != null)
                        {
                            foreach (ContractItemDC monthItem in MemberContract.EveryMonthItemList)
                            {
                                ContractItemDC item = DeepCopy(monthItem);
                                item.Priority = Priority;
                                if (item.ActivityID == MemberContract.ActivityId)
                                {
                                    item.IsActivityArticle = true;
                                    item.Price += restplusAmount;
                                }

                                if (item.EndOrder > 0)
                                {
                                    if (installment1.InstallmentNo >= item.StartOrder && installment1.InstallmentNo <= item.EndOrder)
                                    {
                                        installment1.Amount += item.Price;
                                        if (!item.IsActivityArticle)
                                        {
                                            installment1.AdonPrice += item.Price;
                                        }
                                        else
                                        {
                                            installment1.ActivityPrice += item.Price;
                                        }

                                        installment1.ServiceAmount += item.Price;
                                        ContractServicePrice += item.Price;
                                        installment1.AddOnList.Add(item);
                                        Priority++;
                                    }
                                }
                                else if (installment1.InstallmentNo >= item.StartOrder) // no end order found
                                {
                                    installment1.Amount += item.Price;
                                    if (!item.IsActivityArticle)
                                        installment1.AdonPrice += item.Price;
                                    else
                                        installment1.ActivityPrice += item.Price;

                                    installment1.ServiceAmount += item.Price;
                                    ContractServicePrice += item.Price;
                                    installment1.AddOnList.Add(item);
                                    Priority++;
                                }
                            }
                        }

                        installment1.Balance = installment1.Amount;
                        installment1.EstimatedOrderAmount = installment1.Amount;
                        installment1.PriceListItem = MemberContract.ContractName;


                        if (installment1.DueDate.Date == DateTime.Today.Date)
                        {
                            installment1.EstimatedInvoiceDate = DateTime.Today;
                        }
                        else
                        {
                            if (MemberContract.MemberCreditPeriod > 0)
                                installment1.EstimatedInvoiceDate = installment1.DueDate.AddDays(MemberContract.MemberCreditPeriod * -1);
                            else if (MemberContract.CreditPeriod > 0)
                                installment1.EstimatedInvoiceDate = installment1.DueDate.AddDays(MemberContract.CreditPeriod * -1);
                        }

                        //this is to pick for autoamted invoice. it runs early in morning
                        if (installment1.EstimatedInvoiceDate.Value.Date <= DateTime.Today.Date && installment1.DueDate > DateTime.Today)
                            installment1.EstimatedInvoiceDate = installment1.EstimatedInvoiceDate.Value.AddDays(1);


                        _installmentList.Add(installment1);

                        //if Rest + month is tru -- following 
                        if (MemberContract.RestPlusMonth)
                        {
                            DateTime _dueDate = installment1.DueDate.AddMonths(1);
                            DateTime _installmentRegisterDate = MemberContract.ContractStartDate;
                            int monofDaysForMonth = DateTime.DaysInMonth(MemberContract.ContractStartDate.Year, MemberContract.ContractStartDate.Month);
                            int restDays = monofDaysForMonth - MemberContract.ContractStartDate.Day;

                            //Prepare the installment date
                            if (DateTime.Now.Day > 1)
                            {
                                _installmentRegisterDate = MemberContract.ContractStartDate.AddDays(restDays);
                                DateTime nextmonth = _installmentRegisterDate.AddDays(1);
                                int noofDaysForNextMonth = DateTime.DaysInMonth(nextmonth.Year, nextmonth.Month);
                                _installmentRegisterDate = _installmentRegisterDate.AddDays(noofDaysForNextMonth + 1);
                            }
                            else
                            {
                                _installmentRegisterDate = MemberContract.ContractStartDate.AddMonths(1);
                            }
                            for (int i = 2; i < MemberContract.NoOfInstallments + 1; i++)
                            {
                                if (MemberContract.SecondDueDate.HasValue && (i == 2))
                                {
                                    if (MemberContract.SecondDueDate > DateTime.Today && installment1.DueDate < MemberContract.SecondDueDate)
                                    {
                                        _dueDate = MemberContract.SecondDueDate.Value;
                                    }
                                    else if (MemberContract.PostPay)
                                    {
                                        _dueDate = new DateTime(installment1.DueDate.AddMonths(2).Year, installment1.DueDate.AddMonths(2).Month, MemberContract.DueDateSetting);
                                    }
                                    else
                                    {
                                        _dueDate = new DateTime(installment1.DueDate.AddMonths(1).Year, installment1.DueDate.AddMonths(1).Month, MemberContract.DueDateSetting);
                                    }
                                }
                                else if (i == 2)
                                {
                                    if (MemberContract.PostPay)
                                    {
                                        _dueDate = new DateTime(installment1.DueDate.AddMonths(2).Year, installment1.DueDate.AddMonths(2).Month, MemberContract.DueDateSetting);
                                    }
                                    else
                                    {
                                        _dueDate = new DateTime(installment1.DueDate.AddMonths(1).Year, installment1.DueDate.AddMonths(1).Month, MemberContract.DueDateSetting);
                                    }
                                }

                                trainingStartDate = trainingEndDate.AddDays(1);
                                DateTime tempNDate = trainingEndDate.AddDays(1);
                                int noofDaysInNextMonth = DateTime.DaysInMonth(tempNDate.Year, tempNDate.Month);
                                trainingEndDate = trainingEndDate.AddDays(noofDaysInNextMonth);

                                InstallmentDC installment = new InstallmentDC();
                                installment.OrderOperationsEnabled = false;
                                installment.AddOnList = new List<ContractItemDC>();
                                installment.IsATG = MemberContract.IsATG;
                                installment.DueDate = _dueDate;
                                installment.OriginalDueDate = _dueDate;
                                installment.BillingDate = _installmentRegisterDate;
                                installment.InstallmentDate = _installmentRegisterDate;
                                installment.OriginalCustomer = memberName;
                                installment.TransferDate = _dueDate.AddDays(-6);
                                installment.MemberId = memberId;
                                installment.PayerId = (guardianID > 0) ? guardianID : memberId;
                                installment.MemberContractId = MemberContract.Id;
                                installment.MemberContractNo = MemberContract.MemberContractNo;
                                installment.InstallmentId = i;
                                installment.InstallmentNo = i;
                                installment.TrainingPeriodStart = trainingStartDate;
                                installment.TrainingPeriodEnd = trainingEndDate;
                                installment.TemplateId = MemberContract.ContractId;
                                installment.Text = installment.TrainingPeriodStart.Value.ToShortDateString() + " - " + installment.TrainingPeriodEnd.Value.ToShortDateString();
                                installment.PriceListItem = MemberContract.ContractName;

                                if (MemberContract.EveryMonthItemList != null)
                                {
                                    int priotityIndex = 1;
                                    foreach (ContractItemDC monthItem in MemberContract.EveryMonthItemList)
                                    {
                                        ContractItemDC item = DeepCopy(monthItem);
                                        item.Priority = priotityIndex;
                                        if (item.ActivityID == MemberContract.ActivityId)
                                            item.IsActivityArticle = true;


                                        if (item.EndOrder > 0)
                                        {
                                            if (installment.InstallmentNo >= item.StartOrder && installment.InstallmentNo <= item.EndOrder)
                                            {
                                                installment.Amount += item.Price;
                                                if (!item.IsActivityArticle)
                                                {
                                                    installment.AdonPrice += item.Price;
                                                }
                                                else
                                                {
                                                    installment.ActivityPrice += item.Price;
                                                }

                                                installment.ServiceAmount += item.Price;
                                                ContractServicePrice += item.Price;
                                                installment.AddOnList.Add(item);
                                                Priority++;
                                            }
                                        }
                                        else if (installment.InstallmentNo >= item.StartOrder) // no end order found
                                        {
                                            installment.Amount += item.Price;
                                            if (!item.IsActivityArticle)
                                                installment.AdonPrice += item.Price;
                                            else
                                                installment.ActivityPrice += item.Price;

                                            installment.ServiceAmount += item.Price;
                                            ContractServicePrice += item.Price;
                                            installment.AddOnList.Add(item);
                                            Priority++;
                                        }
                                    }
                                }

                                if (MemberContract.EveryMonthItemList.Count == 0)
                                    installment.Amount = installmentAmount;
                                installment.Balance = installment.Amount;
                                installment.EstimatedOrderAmount = installment.Amount;

                                if (MemberContract.MemberCreditPeriod > 0)
                                    installment.EstimatedInvoiceDate = installment.DueDate.AddDays(MemberContract.MemberCreditPeriod * -1);
                                else if (MemberContract.CreditPeriod > 0)
                                    installment.EstimatedInvoiceDate = installment.DueDate.AddDays(MemberContract.CreditPeriod * -1);

                                if (MemberContract.DueDateSetting > 0)
                                {
                                    _dueDate = _dueDate.AddMonths(1);
                                    _dueDate = new DateTime(_dueDate.Year, _dueDate.Month, MemberContract.DueDateSetting);
                                }
                                else
                                {
                                    _dueDate = _dueDate.AddMonths(1);
                                }

                                DateTime tempDate = _installmentRegisterDate;
                                int noOfDays = DateTime.DaysInMonth(_installmentRegisterDate.Year, _installmentRegisterDate.Month);
                                _installmentRegisterDate = tempDate.AddDays(noOfDays);
                                _installmentList.Add(installment);
                            }
                        }
                        else
                        {
                            DateTime _dueDate = installment1.DueDate.AddMonths(1);
                            if (MemberContract.EveryMonthItemList.Count == 0)
                                installmentAmount = MemberContract.OrderPrice / MemberContract.NoOfInstallments;

                            DateTime _installmentRegisterDate = MemberContract.ContractStartDate.AddMonths(1);
                            for (int i = 2; i < MemberContract.NoOfInstallments + 1; i++)
                            {
                                if (MemberContract.SecondDueDate.HasValue && (i == 2))
                                {
                                    _dueDate = MemberContract.SecondDueDate.Value;
                                }
                                else if (i == 2)
                                {
                                    _dueDate = new DateTime(installment1.DueDate.AddMonths(1).Year, installment1.DueDate.AddMonths(1).Month, MemberContract.DueDateSetting);
                                }

                                trainingStartDate = trainingEndDate.AddDays(1);
                                trainingEndDate = trainingStartDate.AddMonths(1).AddDays(-1);

                                InstallmentDC installment = new InstallmentDC();
                                installment.OrderOperationsEnabled = false;
                                installment.IsATG = MemberContract.IsATG;
                                installment.AddOnList = new List<ContractItemDC>();
                                installment.DueDate = _dueDate;
                                installment.OriginalDueDate = _dueDate;
                                installment.BillingDate = _installmentRegisterDate;
                                installment.InstallmentDate = _installmentRegisterDate;
                                installment.OriginalCustomer = memberName;
                                installment.TransferDate = _dueDate.AddDays(-6);
                                installment.MemberId = memberId;
                                installment.PayerId = (guardianID > 0) ? guardianID : memberId;
                                installment.MemberContractId = MemberContract.Id;
                                installment.MemberContractNo = MemberContract.MemberContractNo;
                                installment.InstallmentId = i;
                                installment.InstallmentNo = i;
                                installment.TrainingPeriodStart = trainingStartDate;
                                installment.TrainingPeriodEnd = trainingEndDate;
                                installment.TemplateId = MemberContract.ContractId;
                                installment.Text = installment.TrainingPeriodStart.Value.ToShortDateString() + " - " + installment.TrainingPeriodEnd.Value.ToShortDateString();
                                installment.PriceListItem = MemberContract.ContractName;

                                if (MemberContract.EveryMonthItemList != null)
                                {
                                    int priotityIndex = 1;
                                    foreach (ContractItemDC monthItem in MemberContract.EveryMonthItemList)
                                    {

                                        ContractItemDC item = DeepCopy(monthItem);
                                        item.Priority = priotityIndex;
                                        if (item.ActivityID == MemberContract.ActivityId)
                                            item.IsActivityArticle = true;

                                        if (item.EndOrder > 0)
                                        {
                                            if (installment.InstallmentNo >= item.StartOrder && installment.InstallmentNo <= item.EndOrder)
                                            {
                                                installment.Amount += item.Price;
                                                if (!item.IsActivityArticle)
                                                {
                                                    installment.AdonPrice += item.Price;
                                                }
                                                else
                                                {
                                                    installment.ActivityPrice += item.Price;
                                                }

                                                installment.ServiceAmount += item.Price;
                                                ContractServicePrice += item.Price;
                                                installment.AddOnList.Add(item);
                                                Priority++;
                                            }
                                        }
                                        else if (installment.InstallmentNo >= item.StartOrder) // no end order found
                                        {
                                            installment.Amount += item.Price;
                                            if (!item.IsActivityArticle)
                                                installment.AdonPrice += item.Price;
                                            else
                                                installment.ActivityPrice += item.Price;

                                            installment.ServiceAmount += item.Price;
                                            ContractServicePrice += item.Price;
                                            installment.AddOnList.Add(item);
                                            Priority++;
                                        }
                                    }
                                }

                                if (MemberContract.EveryMonthItemList.Count == 0)
                                    installment.Amount = installmentAmount;
                                installment.Balance = installment.Amount;
                                installment.EstimatedOrderAmount = installment.Amount;

                                if (MemberContract.MemberCreditPeriod > 0)
                                    installment.EstimatedInvoiceDate = installment.DueDate.AddDays(MemberContract.MemberCreditPeriod * -1);
                                else if (MemberContract.CreditPeriod > 0)
                                    installment.EstimatedInvoiceDate = installment.DueDate.AddDays(MemberContract.CreditPeriod * -1);

                                if (MemberContract.DueDateSetting > 0)
                                {
                                    _dueDate = _dueDate.AddMonths(1);
                                    _dueDate = new DateTime(_dueDate.Year, _dueDate.Month, MemberContract.DueDateSetting);
                                }
                                else
                                {
                                    _dueDate = _dueDate.AddMonths(1);
                                }
                                _installmentRegisterDate = _installmentRegisterDate.AddMonths(1);
                                _installmentList.Add(installment);
                            }
                        }
                    }
                    else
                    {
                        DateTime dueDate = MemberContract.ContractStartDate;
                        DateTime tSdate = MemberContract.ContractStartDate;
                        DateTime tEDate = MemberContract.ContractStartDate.AddMonths(1).AddDays(-1);

                        for (int i = 1; i < MemberContract.NoOfInstallments + 1; i++)
                        {
                            InstallmentDC installment = new InstallmentDC();
                            installment.OrderOperationsEnabled = false;
                            installment.AddOnList = new List<ContractItemDC>();
                            installment.InstallmentNo = i;
                            installment.InstallmentId = i;
                            installment.IsATG = MemberContract.IsATG;
                            installment.MemberId = memberId;
                            installment.PayerId = (guardianID > 0) ? guardianID : memberId;
                            installment.TemplateId = MemberContract.ContractId;
                            installment.OriginalCustomer = memberName;
                            installment.MemberContractId = MemberContract.Id;
                            installment.MemberContractNo = MemberContract.MemberContractNo;

                            installment.InstallmentDate = MemberContract.ContractStartDate;
                            //setting the training period
                            if (i == MemberContract.NoOfInstallments)
                            {
                                installment.TrainingPeriodStart = tSdate;
                                installment.TrainingPeriodEnd = MemberContract.ContractEndDate;
                            }
                            else
                            {
                                installment.TrainingPeriodStart = tSdate;
                                installment.TrainingPeriodEnd = tEDate;
                            }

                            int Priority = 1;
                            if (i == 1)//adding start up items
                            {
                                //Setting the Due Date
                                if (ContractDueDate.HasValue)
                                {
                                    dueDate = ContractDueDate.Value;
                                }
                                else
                                {
                                    dueDate = MemberContract.ContractStartDate;
                                }

                                if (MemberContract.StartUpItemList != null)
                                {
                                    foreach (ContractItemDC item in MemberContract.StartUpItemList)
                                    {
                                        ContractItemDC startUpItem = DeepCopy(item);
                                        startUpItem.Priority = Priority;
                                        installment.AddOnList.Add(startUpItem);
                                        installment.AdonPrice += startUpItem.Price;
                                        installment.Amount += startUpItem.Price;
                                        installment.ItemAmount += startUpItem.Price;
                                        Priority++;
                                    }
                                }
                            }

                            if (MemberContract.EveryMonthItemList != null)//adding monthly items
                            {
                                foreach (ContractItemDC monthItem in MemberContract.EveryMonthItemList)
                                {
                                    ContractItemDC item = DeepCopy(monthItem);
                                    item.Priority = Priority;
                                    if (item.ActivityID == MemberContract.ActivityId)
                                        item.IsActivityArticle = true;

                                    if (item.EndOrder > 0)
                                    {
                                        if (installment.InstallmentNo >= item.StartOrder && installment.InstallmentNo <= item.EndOrder)
                                        {
                                            installment.Amount += item.Price;
                                            if (!item.IsActivityArticle)
                                            {
                                                installment.AdonPrice += item.Price;
                                            }
                                            else
                                            {
                                                installment.ActivityPrice += item.Price;
                                            }

                                            installment.ServiceAmount += item.Price;
                                            ContractServicePrice += item.Price;
                                            installment.AddOnList.Add(item);
                                            Priority++;
                                        }
                                    }
                                    else if (installment.InstallmentNo >= item.StartOrder) // no end order found
                                    {
                                        installment.Amount += item.Price;
                                        if (!item.IsActivityArticle)
                                            installment.AdonPrice += item.Price;
                                        else
                                            installment.ActivityPrice += item.Price;

                                        installment.ServiceAmount += item.Price;
                                        ContractServicePrice += item.Price;
                                        installment.AddOnList.Add(item);
                                        Priority++;
                                    }
                                }
                            }
                            if (MemberContract.SecondDueDate.HasValue && (i == 2))
                            {
                                dueDate = MemberContract.SecondDueDate.Value;
                            }
                            else if (i == 2)
                            {
                                dueDate = new DateTime(dueDate.Year, (dueDate.Month), MemberContract.DueDateSetting);
                            }

                            installment.DueDate = dueDate;
                            installment.OriginalDueDate = dueDate;

                            installment.BillingDate = DateTime.Today;
                            installment.PriceListItem = MemberContract.ContractName;
                            installment.Balance = installment.Amount;
                            installment.EstimatedOrderAmount = installment.Amount;

                            if (MemberContract.MemberCreditPeriod > 0)
                                installment.EstimatedInvoiceDate = installment.DueDate.AddDays(MemberContract.MemberCreditPeriod * -1);
                            else if (MemberContract.CreditPeriod > 0)
                                installment.EstimatedInvoiceDate = installment.DueDate.AddDays(MemberContract.CreditPeriod * -1);

                            _installmentList.Add(installment);
                            if (MemberContract.DueDateSetting > 0)
                            {
                                dueDate = dueDate.AddMonths(1);
                                dueDate = new DateTime(dueDate.Year, dueDate.Month, MemberContract.DueDateSetting);
                            }
                            else
                            {
                                dueDate = dueDate.AddMonths(1);
                            }

                            installment.Text = installment.TrainingPeriodStart.Value.ToShortDateString() + " - " + installment.TrainingPeriodEnd.Value.ToShortDateString();

                            tSdate = tEDate.AddDays(1);
                            tEDate = tSdate.AddMonths(1).AddDays(-1);
                        }
                    }
                    MemberContract.EveryMonthItemPrice = ContractServicePrice;
                    MemberContract.StartUpItemPrice = ContractItemPrice;
                    MemberContract.ContractPrize = ContractServicePrice + ContractItemPrice;
                    MemberContract.InstalllmentList = _installmentList;
                }
                catch
                {

                }
            }
        }

        public static ContractItemDC DeepCopy(ContractItemDC contractItem)
        {
            return contractItem.ShallowCopy();
        }

        public static ExceIBookingNotificationDetails GetTemplateDetails(string gymCode, string templateNo, int branchID, string type)
        {
            return ExcelineIBookingFacade.GetTemplateDetails(gymCode, templateNo, branchID, type);
        }

        public static List<InstallmentDC> GetTodayOrder(string gymCode, int savedMemberID, int memberContractID)
        {
            return ExcelineIBookingFacade.GetTodayOrder(gymCode, savedMemberID, memberContractID);
        }

        public static OperationResult<List<ExceIBookingResource>> GetResources(string gymCode, int branchId)
        {
            var result = new OperationResult<List<ExceIBookingResource>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetResources(gymCode, branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Resource - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<List<ExceIBookingResourceBooking>> GetResourceBooking(string gymCode, int branchId, DateTime fromDate, DateTime toDate)
        {
            var result = new OperationResult<List<ExceIBookingResourceBooking>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetResourceBooking(gymCode, branchId, fromDate, toDate);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Resource booking- IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<int> AddResourceBooking(string gymCode, ExceIBookingResourceBooking resourceBooking)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.AddResourceBooking(gymCode, resourceBooking);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Adding resource booking - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> DeleteResourceBooking(string gymCode, int bookingId, int branchId)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.DeleteResourceBooking(gymCode, bookingId, branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in deleting resource booking - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<Dictionary<int, int>> ValidateAvailableForBooking(string gymCode, List<int> resourceIdList, DateTime startDateTime, DateTime endDateTime)
        {
            var result = new OperationResult<Dictionary<int, int>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.ValidateAvailableForBooking(gymCode, resourceIdList, startDateTime, endDateTime);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in validation resource booking - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        public static OperationResult<List<ExceIBookingResourceSchedule>> GetResourceSchedules(string gymCode, int branchId, int resourceId, DateTime fromDate, DateTime toDate)
        {
            var result = new OperationResult<List<ExceIBookingResourceSchedule>>();
            try
            {
                var itemList = ExcelineIBookingFacade.GetResourceSchedules(gymCode, branchId, resourceId, fromDate, toDate);
                result.OperationReturnValue = FormatGetSchedule(itemList);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting resource  schedules - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExceIBookingResourceAvailableTime>> GetResourceAvailableTimes(string gymCode, int branchId, int resourceId)
        {
            var result = new OperationResult<List<ExceIBookingResourceAvailableTime>>();
            try
            {
                var itemList = ExcelineIBookingFacade.GetResourceAvailableTimes(gymCode, branchId, resourceId);
                result.OperationReturnValue = FormatGetAvailableTime(itemList);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting resource  Availabletime - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        private static List<ExceIBookingResourceSchedule> FormatGetSchedule(IEnumerable<ExceIBookingResourceSchedule> resourceSchedules)
        {
            var resourceScheduleList = new List<ExceIBookingResourceSchedule>();
            //var resunavailableTime = new List<ExceIBookingResourceSchedule>();

            foreach (var exceIBookingResourceSchedule in resourceSchedules)
            {
                if (exceIBookingResourceSchedule.RoleType == "RESSCHEDULE")
                {
                    if (resourceScheduleList.Any(x => x.Day == exceIBookingResourceSchedule.Day))
                    {
                        var item = resourceScheduleList.FirstOrDefault(x => x.Day == exceIBookingResourceSchedule.Day);
                        if (item != null)
                        {
                            var resourceScheduleTime = new ExceIBookingResourceScheduleTime
                                {
                                    Id = exceIBookingResourceSchedule.Id,
                                    FromTime = exceIBookingResourceSchedule.FromTime,
                                    ToTime = exceIBookingResourceSchedule.ToTime
                                };
                            item.ScheduleTime.Add(resourceScheduleTime);
                            if (exceIBookingResourceSchedule.UnavailableTime != null)
                            {
                                foreach (var itemUnavailable in exceIBookingResourceSchedule.UnavailableTime)
                                {
                                    var resourceUnavailable = new ExceIBookingResourceScheduleTime
                                    {
                                        Id = itemUnavailable.Id,
                                        FromTime = itemUnavailable.FromTime,
                                        ToTime = itemUnavailable.ToTime
                                    };
                                    if (item.UnavailableTime == null)
                                        item.UnavailableTime = new List<ExceIBookingResourceScheduleTime>();

                                    item.UnavailableTime.Add(resourceUnavailable);
                                }
                            }
                        }
                    }
                    else
                    {
                        var resourceScheduleTime = new ExceIBookingResourceScheduleTime
                            {
                                Id = exceIBookingResourceSchedule.Id,
                                FromTime = exceIBookingResourceSchedule.FromTime,
                                ToTime = exceIBookingResourceSchedule.ToTime
                            };
                        if (exceIBookingResourceSchedule.ScheduleTime == null)
                            exceIBookingResourceSchedule.ScheduleTime = new List<ExceIBookingResourceScheduleTime>();

                        exceIBookingResourceSchedule.ScheduleTime.Add(resourceScheduleTime);
                        exceIBookingResourceSchedule.Id = -1;
                        exceIBookingResourceSchedule.FromTime = string.Empty;
                        exceIBookingResourceSchedule.ToTime = string.Empty;
                        resourceScheduleList.Add(exceIBookingResourceSchedule);
                    }
                }
                else
                {
                    if (resourceScheduleList.Any(x => x.Day == exceIBookingResourceSchedule.Day))
                    {
                        var item = resourceScheduleList.FirstOrDefault(x => x.Day == exceIBookingResourceSchedule.Day);
                        if (item != null)
                        {
                            var resourceScheduleTime = new ExceIBookingResourceScheduleTime
                            {
                                Id = exceIBookingResourceSchedule.Id,
                                FromTime = exceIBookingResourceSchedule.FromTime,
                                ToTime = exceIBookingResourceSchedule.ToTime
                            };

                            if (item.UnavailableTime == null)
                                item.UnavailableTime = new List<ExceIBookingResourceScheduleTime>();
                            item.UnavailableTime.Add(resourceScheduleTime);
                        }
                    }
                    else
                    {
                        var resourceScheduleTime = new ExceIBookingResourceScheduleTime
                        {
                            Id = exceIBookingResourceSchedule.Id,
                            FromTime = exceIBookingResourceSchedule.FromTime,
                            ToTime = exceIBookingResourceSchedule.ToTime
                        };
                        if (exceIBookingResourceSchedule.UnavailableTime == null)
                            exceIBookingResourceSchedule.UnavailableTime = new List<ExceIBookingResourceScheduleTime>();

                        exceIBookingResourceSchedule.UnavailableTime.Add(resourceScheduleTime);
                        exceIBookingResourceSchedule.Id = -1;
                        exceIBookingResourceSchedule.FromTime = string.Empty;
                        exceIBookingResourceSchedule.ToTime = string.Empty;
                        resourceScheduleList.Add(exceIBookingResourceSchedule);
                    }
                }

            }
            return resourceScheduleList;

        }

        private static List<ExceIBookingResourceAvailableTime> FormatGetAvailableTime(IEnumerable<ExceIBookingResourceAvailableTime> resourceAvailableTimes)
        {
            var resourceAvailableTimeList = new List<ExceIBookingResourceAvailableTime>();
            foreach (var resourceAvailableTime in resourceAvailableTimes)
            {
                if (resourceAvailableTimeList.Any(x => x.Day == resourceAvailableTime.Day))
                {
                    var item = resourceAvailableTimeList.FirstOrDefault(x => x.Day == resourceAvailableTime.Day);
                    if (item != null)
                    {
                        var resourceAvailable = new ExceIBookingResourceScheduleTime
                        {
                            FromTime = resourceAvailableTime.FromTime,
                            ToTime = resourceAvailableTime.ToTime
                        };
                        item.AvailableTimes.Add(resourceAvailable);
                    }
                }
                else
                {
                    var resourceAvailabel = new ExceIBookingResourceScheduleTime
                        {
                            FromTime = resourceAvailableTime.FromTime,
                            ToTime = resourceAvailableTime.ToTime
                        };
                    if (resourceAvailableTime.AvailableTimes == null)
                        resourceAvailableTime.AvailableTimes = new List<ExceIBookingResourceScheduleTime>();

                    resourceAvailableTime.AvailableTimes.Add(resourceAvailabel);
                    resourceAvailableTimeList.Add(resourceAvailableTime);
                }
            }
            return resourceAvailableTimeList;
        }

        public static OperationResult<List<ExceIBookingInvoice>> GetInvoices(int branchID, string gymCode, DateTime startDate, DateTime endDate, string documentService, string folderPath)
        {
            OperationResult<List<ExceIBookingInvoice>> result = new OperationResult<List<ExceIBookingInvoice>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetInvoices(branchID, gymCode, startDate, endDate, documentService, folderPath);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting INvoices - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static void AddStatus(ExceIBookingNewMember NewMember, int status, string description, string gymCode)
        {
            try
            {
                ExcelineIBookingFacade.AddStatus(NewMember, status, description, gymCode);
            }
            catch (Exception)
            {
            }

        }

        public static OperationResult<List<ExceIBookingInterest>> GetInterests(string gymCode)
        {
            OperationResult<List<ExceIBookingInterest>> result = new OperationResult<List<ExceIBookingInterest>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingInterests(gymCode);
            }
            catch (Exception)
            {
            }
            return result;
        }

        public static OperationResult<ARXSetting> GetARXSettings(int systemId)
        {
            OperationResult<ARXSetting> result = new OperationResult<ARXSetting>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetARXSettings(systemId);
            }
            catch (Exception)
            {
            }
            return result;
        }

        public static void UploadARXData(int savedMemberID, int memberContractID, string gymCode, int systemID, string fileLocation, string CustId, ARXSetting arxSetting)
        {
            try
            {
                person arxPerson = ExcelineIBookingFacade.GetARXData(savedMemberID, memberContractID,  gymCode);
                if (arxPerson != null)
                {
                    XDocument doc = new XDocument();
                    doc.Declaration = new XDeclaration("1.0", "ISO-8859-1", "yes");

                    XElement arxDataRoot = new XElement("arxdata");
                    doc.Add(arxDataRoot);

                    XElement persons = new XElement("persons");
                    XElement person = new XElement("person");
                    XElement id = new XElement("id", systemID.ToString() + "_" + arxPerson.BranchID.ToString() + "_" + arxPerson.id);
                    XElement fName = new XElement("first_name", arxPerson.first_name);
                    XElement lName = new XElement("last_name", arxPerson.last_name);
                    person.Add(id, fName, lName);
                    XElement accessCategories = new XElement("access_categories");
                    XElement accessCategory = new XElement("access_category");

                    if (string.IsNullOrEmpty(arxPerson.AccessProfileName))
                    {
                        XElement accessName = new XElement("name", arxPerson.BranchName.Trim());
                        accessCategory.Add(accessName);
                    }
                    else
                    {
                        XElement accessName = new XElement("name", arxPerson.AccessProfileName);
                        accessCategory.Add(accessName);
                    }

                    XElement endadate = new XElement("end_date", arxPerson.EndDate.Trim() + " 23:59:00");
                    accessCategory.Add(endadate);

                    accessCategories.Add(accessCategory);
                    person.Add(accessCategories);
                    persons.Add(person);
                    arxDataRoot.Add(persons);

                    XElement cards = new XElement("cards");
                    XElement card = new XElement("card");
                    XElement number = new XElement("number", arxPerson.CardNo);
                    XElement personid = new XElement("person_id", systemID.ToString() + "_" + arxPerson.BranchID.ToString() + "_" + arxPerson.id);

                    if (arxPerson.FormatName == "deleted")
                    {
                        XElement formatname = new XElement("format_name", "Tastatur");
                        XElement deleted = new XElement("DELETED");
                        card.Add(number, personid, formatname, deleted);
                    }
                    else
                    {
                        XElement formatname = new XElement("format_name", arxPerson.FormatName);
                        card.Add(number, personid, formatname);
                    }

                    cards.Add(card);
                    arxDataRoot.Add(cards);

                    string xmlString = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" + doc.ToString();
                    string sourceFilePath = fileLocation + "\\" + gymCode;
                    string backupPath = Path.Combine(fileLocation + "\\Backup" + "\\" + gymCode, CustId + ".xml");
                    string sourcePath = Path.Combine(fileLocation + "\\" + gymCode, CustId + ".xml");

                    if (!Directory.Exists(sourceFilePath))
                        Directory.CreateDirectory(sourceFilePath);

                    if (!Directory.Exists(fileLocation + "\\Backup" + "\\" + gymCode))
                        Directory.CreateDirectory(fileLocation + "\\Backup" + "\\" + gymCode);

                    File.WriteAllText(sourcePath, xmlString);

                    UploadFile(sourcePath, arxSetting.UserName, arxSetting.Password, arxSetting.ServiceURL, gymCode);

                    if (File.Exists(backupPath))
                        File.Delete(backupPath);

                    File.Move(sourcePath, backupPath);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string UploadFile(string filename, string userName, string password, string url, string gymCode)
        {
            try
            {
                WebResponse response;

                ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateValidationCallbackCb;
                UriBuilder wfUri = new UriBuilder(url);
                CredentialCache myCredentialCache = new CredentialCache();
                myCredentialCache.Add(wfUri.Uri, "Basic", new NetworkCredential(userName, password));


                HttpWebRequest webrequest;

                string sBoundary = "----------" + DateTime.Now.Ticks.ToString("x");

                // Preauthentification is needed when AllowStreamBuffering is set to false for some reason..
                webrequest = (HttpWebRequest)WebRequest.Create(wfUri.Uri);
                webrequest.Method = "HEAD";
                webrequest.Credentials = myCredentialCache;
                webrequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)";
                webrequest.PreAuthenticate = true;
                response = (HttpWebResponse)webrequest.GetResponse();
                response.Close();
                //---------------------------------------------------------------

                webrequest = (HttpWebRequest)WebRequest.Create(wfUri.Uri);
                webrequest.Timeout = 900000;
                webrequest.ContentType = "multipart/form-data; boundary=" + sBoundary;
                webrequest.Method = "POST";
                webrequest.Credentials = myCredentialCache;
                webrequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)";
                webrequest.Accept = "application/x-ms-application, image/jpeg, application/xaml+xml, image/gif, image/pjpeg, application/x-ms-xbap, application/x-shockwave-flash, application/x-silverlight, */*";

                webrequest.PreAuthenticate = true;
                webrequest.AllowWriteStreamBuffering = false;
                webrequest.MaximumResponseHeadersLength = 4;


                // Create header
                StringBuilder sbHeader = new StringBuilder();
                sbHeader.Append("--");
                sbHeader.Append(sBoundary);
                sbHeader.Append("\r\n");
                sbHeader.Append("Content-Disposition: form-data; name=\"upfile\"; filename=\"");
                sbHeader.Append(Path.GetFileName(filename));
                sbHeader.Append("\"");
                sbHeader.Append("\r\n");
                sbHeader.Append("Content-Type: application/octet-stream");
                sbHeader.Append("\r\n");
                sbHeader.Append("\r\n");

                string postHeader = sbHeader.ToString();
                byte[] postHeaderBytes = Encoding.UTF8.GetBytes(postHeader);

                // Build the trailing boundary string as a byte array
                // ensuring the boundary appears on a line by itself
                byte[] btBoundary = Encoding.ASCII.GetBytes("\r\n--" + sBoundary + "--\r\n");

                FileStream fileStream = new FileStream(filename,
                                            FileMode.Open, FileAccess.Read);
                long length = postHeaderBytes.Length + fileStream.Length + btBoundary.Length;
                webrequest.ContentLength = length;

                Stream requestStream = webrequest.GetRequestStream();

                // Write out our post header
                requestStream.Write(postHeaderBytes, 0, postHeaderBytes.Length);

                // Write out the file contents
                byte[] buffer = new Byte[checked((uint)Math.Min(4096, (int)fileStream.Length))];
                int bytesRead;
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                    requestStream.Write(buffer, 0, bytesRead);

                // Write out the trailing boundary
                requestStream.Write(btBoundary, 0, btBoundary.Length);

                requestStream.Flush();

                fileStream.Close();
                requestStream.Close();


                response = webrequest.GetResponse();
                Stream s = response.GetResponseStream();
                var sr = new StreamReader(s);
                return sr.ReadToEnd();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool RemoteCertificateValidationCallbackCb(Object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static int AddClassVisit(string gymID, string classID, List<MemberVisitStatus> visits, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.AddClassVisit(gymID, classID, visits, gymCode);
            }
            catch (Exception)
            {
                throw;
            }
            return result.OperationReturnValue;
        }

        public static void SetPDFFileParth(int id, string fileParth, string flag, int branchId, string gymCode, string user)
        {
            try
            {
                ExcelineIBookingFacade.SetPDFFileParth(id, fileParth, flag, branchId, gymCode, user);
            }
            catch (Exception) {           
            }
        }

        public static OperationResult<int> SetResourceBookingAsPaid(ExceIBookingResourceBookingPaid resourceBookingPaid, string gymCode)
        {
            OperationResult<int> operationResult = new OperationResult<int>();
            try
            {
                //Get Booking details 
                BookingDetails bookingDetails = ExcelineIBookingFacade.GetBookingDetails(resourceBookingPaid.BookingId, gymCode);
                if (bookingDetails != null)
                {
                    //Create the Installment 
                    InstallmentDC installment = new InstallmentDC()
                    {
                        Amount = Convert.ToDecimal(resourceBookingPaid.InvoiceAmount),
                        InvoiceGeneratedDate = DateTime.Today,
                        InstallmentDate = DateTime.Today,
                        InstallmentType = "S",
                        IsATG = false,
                        DueDate = DateTime.Today,
                        MemberId = bookingDetails.BookingMemberID
                    };

                    //Add the orderline 
                    ContractItemDC orderline = new ContractItemDC();
                    orderline.EmployeePrice = Convert.ToDecimal(resourceBookingPaid.InvoiceAmount);
                    orderline.CategoryId = bookingDetails.ArticleCategoryID;
                    orderline.ArticleId = bookingDetails.ArticleID;
                    orderline.ItemName = bookingDetails.ArticleName;
                    orderline.UnitPrice = Convert.ToDecimal(resourceBookingPaid.InvoiceAmount);
                    orderline.Price = Convert.ToDecimal(resourceBookingPaid.InvoiceAmount);
                    orderline.ReturnComment = string.Empty;
                    orderline.Quantity = 1;
                    orderline.Discount = 0;
                    orderline.IsActivityArticle = false;
                    orderline.StockStatus = false;
                    installment.AddOnList.Add(orderline);

                    PayModeDC paymentMode = new PayModeDC();
                    paymentMode.Amount = Convert.ToDecimal(resourceBookingPaid.PaymentAmount);
                    paymentMode.PaidDate = DateTime.Now;
                    paymentMode.PaymentType = "BANKSTATEMENT";
                    paymentMode.PaymentTypeCode = "BANKSTATEMENT";
                    paymentMode.PaymentTypeId = -1;

                    List<PayModeDC> payModes = new List<PayModeDC>();
                    payModes.Add(paymentMode);

                    PaymentDetailDC paymentDetails = new PaymentDetailDC();
                    paymentDetails.PaymentSource = PaymentTypes.INSTALLMENTS;
                    paymentDetails.PayModes = payModes;
                    paymentDetails.PaidAmount = Convert.ToDecimal(resourceBookingPaid.PaymentAmount);;
                    paymentDetails.InvoiceAmount = Convert.ToDecimal(resourceBookingPaid.InvoiceAmount);;

                    int orderID = ExcelineIBookingFacade.AddShopOrder(installment, resourceBookingPaid.GymID, resourceBookingPaid.GymID, gymCode);
                    if (orderID > 0)
                    {
                        //add the payment 
                        installment.Id = orderID;
                        OperationResult<SaleResultDC> result = ExcelineIBookingFacade.PayIBookingInvoice(resourceBookingPaid.GymID, "IBooking", installment, paymentDetails, gymCode);
                        if (result.OperationReturnValue.SaleStatus != SaleErrors.SUCCESS)
                        {
                            //remove the booking 
                            operationResult.OperationReturnValue = 2; // error in adding payments and boking is deleted
                            ExcelineIBookingFacade.CancelPaymentOrder(resourceBookingPaid.BookingId, orderID, result.Notifications[0].Message, gymCode);
                        }
                        else
                        {
                            //Update Booking Related Info 
                            ExcelineIBookingFacade.UpdateBookingPaymentDetails(resourceBookingPaid.BookingId, bookingDetails.BookingMemberID, result.OperationReturnValue.AritemNo, Convert.ToDecimal(resourceBookingPaid.PaymentAmount),resourceBookingPaid.RefId, gymCode);
                            operationResult.OperationReturnValue = 1; //Success
                        }
                    }
                    else
                    {
                        operationResult.OperationReturnValue = 3; //order adding error
                        ExcelineIBookingFacade.CancelPaymentOrder(resourceBookingPaid.BookingId, -1, "order adding error", gymCode);
                    }
                }
                else
                {
                    operationResult.OperationReturnValue = 4; //booking not found
                    ExcelineIBookingFacade.CancelPaymentOrder(resourceBookingPaid.BookingId, -1, "booking not found", gymCode);
                }
            }
            catch (Exception ex)
            {
                operationResult.CreateMessage("Error in Adding Payment", MessageTypes.ERROR);
                operationResult.OperationReturnValue = 5; //Error
                ExcelineIBookingFacade.CancelPaymentOrder(resourceBookingPaid.BookingId, -1, ex.Message, gymCode);
            }
            return operationResult;
        }

        public static OperationResult<bool> UpdateBRISData(OrdinaryMemberDC member, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.UpdateBRISData(member, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updateing BRIS Data " + ex.Message , MessageTypes.ERROR);
                result.OperationReturnValue = false; //Error
                result.ErrorOccured = true;
            }
            return result;
        }


        #region GetMemberGyms
        public static OperationResult<List<ExceIBookingMemberGymDetail>> GetMemberGyms(string gymCode )
        {
            OperationResult<List<ExceIBookingMemberGymDetail>> result = new OperationResult<List<ExceIBookingMemberGymDetail>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetIBookingMemberGyms(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting members - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        public static OperationResult<List<ExceIBookingModifiedMember>> GetMembersUpdatedByDate(string _date, string _systemId, string _branchId, string gymCode)
        {
            DateTime date;
            int systemId;
            int branchId;
            OperationResult<List<ExceIBookingModifiedMember>> result = new OperationResult<List<ExceIBookingModifiedMember>>();
            try
            {
                date = Convert.ToDateTime(_date);
                systemId = Int32.Parse(_systemId);
                branchId = Int32.Parse(_branchId);
                result.OperationReturnValue = ExcelineIBookingFacade.GetMembersUpdated(date, systemId, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting the members with updated details during " + _date + ". " + ex.Message, MessageTypes.ERROR);
                result.OperationReturnValue = new List<ExceIBookingModifiedMember>();
                result.ErrorOccured = true;
                throw;
            }
            return result;
        }


        public static OperationResult<List<ExceIBookingAllContracts>> GetAllContracts(string _systemId, string gymCode)
        {
            DateTime date;
            int systemId;
            int branchId;
            OperationResult<List<ExceIBookingAllContracts>> result = new OperationResult<List<ExceIBookingAllContracts>>();
            try
            {                
                systemId = Int32.Parse(_systemId);             
                result.OperationReturnValue = ExcelineIBookingFacade.GetAllContracts(systemId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting the members with updated details during " + ex.Message, MessageTypes.ERROR);
                result.OperationReturnValue = new List<ExceIBookingAllContracts>();
                result.ErrorOccured = true;
                throw;
            }
            return result;
        }


        #region RegisterNewFreeze
        public static OperationResult<int> RegisterNewFreeze(string gymCode, ExceIBookingNewFreeze freeze)
        {            
            OperationResult<int> result = new OperationResult<int>();
            
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.RegisterNewFreeze(gymCode, freeze);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in adding freeze" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region RegisterResign
        public static OperationResult<int> RegisterResign(string gymCode, ExceIBookingNewResign resign)
        {
            OperationResult<int> result = new OperationResult<int>();

            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.RegisterResign(gymCode, resign);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in adding resign" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetContracts

        public static OperationResult<List<ExceIBookingContractFreeze>> GetContractByMemberId(string gymCode, string customerNo, int systemId)
        {
            OperationResult<List<ExceIBookingContractFreeze>> result = new OperationResult<List<ExceIBookingContractFreeze>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetContractByMemberId(gymCode, customerNo, systemId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Contracts - API, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion


        #region GetFreeze

        public static OperationResult<List<ExceIBookingListFreeze>> GetFreezeByCustId(string gymCode, string custId, int systemId)
        {
            OperationResult<List<ExceIBookingListFreeze>> result = new OperationResult<List<ExceIBookingListFreeze>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetFreezeByCustId(gymCode, custId, systemId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Freeze history - API, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion


        #region GetAPIMembers
        public static OperationResult<List<ExceAPIMember>> GetAPIMembers(string gymCode, int branchId, int changedSinceDays, int systemId)
        {
            OperationResult<List<ExceAPIMember>> result = new OperationResult<List<ExceAPIMember>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetAPIMembers(gymCode, branchId, changedSinceDays, systemId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting members - API, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        #endregion

        #region GetFreezeResignCategories
        public static OperationResult<List<ExceIBookingFreezeResignCategories>> GetFreezeResignCategories(string gymCode)
        {
            OperationResult<List<ExceIBookingFreezeResignCategories>> result = new OperationResult<List<ExceIBookingFreezeResignCategories>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetFreezeResignCategories(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting freeze/resign categories - Exceline, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region GetBrisIdActiveFreezed
        public static OperationResult<List<int?>> GetBrisIdActiveFreezed(string gymCode)
        {
            OperationResult<List<int?>> result = new OperationResult<List<int?>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetBrisIdActiveFreezed(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting BrisIds Active/freezed - Exceline, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region NTNUIGetMembers

        public static OperationResult<List<ExceIBookingMember>> NTNUIGetMembers(string gymCode, int changedSinceDays, int systemId)
        {
            OperationResult<List<ExceIBookingMember>> result = new OperationResult<List<ExceIBookingMember>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.NTNUIGetIBookingMembers(gymCode, changedSinceDays, systemId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting members - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        #endregion


        #region GetVisitCount

        public static OperationResult<List<ExceIBookingVisitCount>> GetVisitCount(string gymCode, int branchId, int year, int systemId)
        {
            OperationResult<List<ExceIBookingVisitCount>> result = new OperationResult<List<ExceIBookingVisitCount>>();
            try
            {
                result.OperationReturnValue = ExcelineIBookingFacade.GetVisitCount(gymCode, branchId, year, systemId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Visit count history - API, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion


    }
}