﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Model
{
    public class SaveInventory
    {
        public int InventoryId { get; set; }
        public List<ArticleDC> ArticleList { get; set; }
    }
}