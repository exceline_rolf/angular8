﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Model
{
    public class DeleteShopSponsorDiscount
    {
        public int discountId { get; set; }
        public string type { get; set; }
    }
}