﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Model
{
    public class EntityActiveTime
    {
        public int Branchid { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<int> EntityList { get; set; }
        public string EntityRoleType { get; set; }

        public int ActiveTimeId { get; set; }
        public string ArticleName { get; set; }

        public string RoleType { get; set; }
        public bool IsArrived { get; set; }
        public bool IsPaid { get; set; }
    }
}