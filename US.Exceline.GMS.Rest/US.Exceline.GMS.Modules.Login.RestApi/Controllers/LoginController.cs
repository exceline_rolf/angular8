﻿using bfDemoService; // Used to encrypt/decrypt from front-end.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Common.USSAdmin.API;
using US.Common.Web.UI.Core.SystemObjects;
using US.Exceline.GMS.Modules.Login.API;
using US.Exceline.GMS.Modules.Login.BusinessLogic; //email client
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Login;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Payment.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Login.RestApi.Controllers
{
    [RoutePrefix("api/Login")]
    public class LoginController : ApiController
    {
        [HttpGet]
        [Route("getdata")]
        [Authorize]
        public IHttpActionResult GetString()
        {
            return Ok("string");
        }

        [HttpGet]
        [Route("AnonymousUserPasswordReset")]
        [AllowAnonymous]
        public HttpResponseMessage AnonymousUserPasswordReset(string gymCode, string potentialUserName)
        {
            try
            {
                var result = ManageExcelineLogin.AnonymousUserPasswordReset(gymCode, potentialUserName);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), potentialUserName);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                // If result is ReturnCode 0, send email to user
                if (Convert.ToInt32(result.OperationReturnValue.ReturnCode) == 0) {
                    try
                    {
                        var returnCode = result.OperationReturnValue.ReturnCode;
                        var userEmail = result.OperationReturnValue.UserEmail;
                        var hashBase = Uri.EscapeDataString(result.OperationReturnValue.HashBase);

                        EmailSMTPClient client = new EmailSMTPClient();
                        client.SendPasswordResetMail(userEmail, potentialUserName, hashBase);
                    }
                    catch (Exception Ex)
                    {
                        throw Ex;
                    }
                    
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(0, ApiResponseStatus.OK));
            }
            catch (Exception Ex)
            {
                List<string> exceptionMsg = new List<string>
                {
                    Ex.Message
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }       

        [HttpPost]
        [Route("SetNewUserPassword")]
        [AllowAnonymous]
        public HttpResponseMessage SetNewUserPassword(ExcePWSetRecDC userProfile)
        {
            try
            {
                ExcePWSetRecDC userData = new ExcePWSetRecDC();
                userData.GymCode = userProfile.GymCode;
                userData.UserName = userProfile.UserName;
                userData.Base = userProfile.Base;
                userData.Password = EncryptDecrypt.DecryptStringAES(userProfile.Password);

                var result = ManageExcelineLogin.SetNewUserPassword(userData);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), userData.UserName);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(0, ApiResponseStatus.OK));
            }
            catch (Exception Ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(Ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetUserBranches")]
        [Authorize]
        public HttpResponseMessage GetUserSelectedBranches()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = ManageExcelineLogin.GetUserSelectedBranches(user, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExceUserBranchDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
               
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceUserBranchDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }



        [HttpGet]
        [Route("GetLoggedUserDetails")]
        [Authorize]
        public HttpResponseMessage GetLoggedUserDetails()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = ManageExcelineLogin.GetLoggedUserDetails(user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExceUserBranchDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceUserBranchDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("EditBasicInfo")]
        [Authorize]
        public HttpResponseMessage EditUSPUserBasicInfo(bool isPasswdChange, ExecUserDC uspUser)
        {
            try
            {
                string loggeduser = Request.Headers.GetValues("UserName").First();
                USPUser user = new USPUser();
                user.Id = uspUser.UserID;
                user.DisplayName = uspUser.DisplayName;
                user.email = uspUser.Email;
                user.passowrd = uspUser.Password;
                user.IsLoginFirstTime = uspUser.IsLoginFirstTime;

                OperationResultValue<bool> result = AdminUserSettings.EditUSPUserBasicInfo(isPasswdChange, user, loggeduser);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), loggeduser);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch(Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
            
        }
    }
}
