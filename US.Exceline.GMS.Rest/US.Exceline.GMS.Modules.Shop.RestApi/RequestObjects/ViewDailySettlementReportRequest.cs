﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Shop.RestApi.RequestObjects
{
    public class ViewDailySettlementReportRequest
    {

        private int _reconciliationId;

        public int ReconciliationId
        {
            get { return _reconciliationId; }
            set { _reconciliationId = value; }
        }

        private decimal _countedCashDraft;

        public decimal CountedCashDraft
        {
            get { return _countedCashDraft; }
            set { _countedCashDraft = value; }
        }

        private int _dailySettlementId;

        public int DailySettlementId
        {
            get { return _dailySettlementId; }
            set { _dailySettlementId = value; }
        }

        private int _salePointId;

        public int SalePointId
        {
            get { return _salePointId; }
            set { _salePointId = value; }
        }

        private int _branchId;

        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private DailySettlementPrintType _mode;

        public DailySettlementPrintType Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }

        private DateTime _startDate;

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _endDate;

        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

    }
}