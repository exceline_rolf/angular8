﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Shop.RestApi.RequestObjects
{
    public class ShopSaleRequest
    {
        private string _CardType;

        public string CardType
        {
            get { return _CardType; }
            set { _CardType = value; }
        }

        private InstallmentDC _installment;

        public InstallmentDC Installment
        {
            get { return _installment; }
            set { _installment = value; }
        }

        private ShopSalesDC _shopSaleDetails;

        public ShopSalesDC ShopSaleDetails
        {
            get { return _shopSaleDetails; }
            set { _shopSaleDetails = value; }
        }

        private PaymentDetailDC _paymentDetails;

        public PaymentDetailDC PaymentDetails
        {
            get { return _paymentDetails; }
            set { _paymentDetails = value; }
        }

        private int _memberBranchID;

        public int MemberBranchID
        {
            get { return _memberBranchID; }
            set { _memberBranchID = value; }
        }

        private int _loggedBranchID;

        public int LoggedBranchID
        {
            get { return _loggedBranchID; }
            set { _loggedBranchID = value; }
        }

        private bool _printInvoice;

        public bool PrintInvoice
        {
            get { return _printInvoice; }
            set { _printInvoice = value; }
        }

        private string _custRoleType;

        public string CustRoleType
        {
            get { return _custRoleType; }
            set { _custRoleType = value; }
        }

        public String _sessionKey = String.Empty;
        public String SessionKey
        {
            get { return _sessionKey; }
            set { _sessionKey = value; }
        }

    }
}