﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Communication.API;
using US.Communication.Core.SystemCore.DomainObjects;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.Exceline.GMS.Modules.Shop.API;
using US.Exceline.GMS.Modules.Shop.API.InventoryManagement;
using US.Exceline.GMS.Modules.Shop.API.ManageShop;
using US.Exceline.GMS.Modules.Shop.RestApi.Models;
using US.Exceline.GMS.Modules.Shop.RestApi.RequestObjects;
using US.GMS.API;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;

namespace US.Exceline.GMS.Modules.Shop.RestApi.Controllers
{
    [RoutePrefix("api/ShopAccount")]
    public class ShopAccountController : ApiController
    {
        #region Shop Account
        [HttpPost]
        [Route("ManageMemberShopAccount")]
        [Authorize]
        public HttpResponseMessage ManageMemberShopAccount(MemberShopAccountDC memberShopAccount)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSMember.ManageMemberShopAccount(memberShopAccount, memberShopAccount.Mode, user, memberShopAccount.SalePointId, memberShopAccount.BranchID, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("PayCreditNote")]
        [Authorize]
        public HttpResponseMessage PayCreditNote(CreditNote creditNote)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSInvoice.PayCreditNote(creditNote.AritemNo, creditNote.PaymentModes, creditNote.MemberId, creditNote.ShopAccountId, ExceConnectionManager.GetGymCode(user), user, creditNote.SalePointId, creditNote.LoggedBranchId);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        public int PayCreditNote(int aritemNo, List<PayModeDC> paymentModes, int memberId, int shopAccountid, string user, int salePointId, int loggedBranchID)
        {
            OperationResult<int> result = GMSInvoice.PayCreditNote(aritemNo, paymentModes, memberId, shopAccountid, ExceConnectionManager.GetGymCode(user), user, salePointId, loggedBranchID);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        [HttpPost]
        [Route("AddShopAccount")]
        [Authorize]
        public HttpResponseMessage AddShopAccount(MemberShopAccountDC memberShopAccount)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSMember.ManageMemberShopAccount(memberShopAccount, memberShopAccount.Mode, user, memberShopAccount.BranchID, memberShopAccount.SalePointId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetMemberShopAccount")]
        [Authorize]
        public HttpResponseMessage GetMemberShopAccounts(int memberId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSMember.GetMemberShopAccounts(memberId, 1, true, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new MemberShopAccountDC(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new MemberShopAccountDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetMemberShopAccountsForEntityType")]
        [Authorize]
        public HttpResponseMessage GetMemberShopAccountsForEntityType(int memberId, string entityType)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSMember.GetMemberShopAccounts(memberId, entityType, 1, true, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new MemberShopAccountDC(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new MemberShopAccountDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        #endregion

        #region Daily Settlement 
        [HttpGet]
        [Route("GetDailySettlements")]
        [Authorize]
        public HttpResponseMessage GetDailySettlements(int branchId, int salePointId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShop.GetDailySettlements(salePointId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new DailySettlementDC(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new DailySettlementDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("AddDailySettlements")]
        [Authorize]
        public HttpResponseMessage AddDailySettlements(DailySettlementDC dailySettlement)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShop.AddDailySettlements(dailySettlement, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new DailySettlementDC(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new DailySettlementDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("ViewDailySettlementReport")]
        [Authorize]
        public HttpResponseMessage ViewDailySettlementReport(ViewDailySettlementReportRequest request)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.GetDailySettlementPrint(request.DailySettlementId.ToString(), request.ReconciliationId.ToString(), request.SalePointId.ToString(), request.BranchId.ToString(), request.Mode.ToString(), request.StartDate.ToString(), request.EndDate.ToString(), ExceConnectionManager.GetGymCode(user),request.CountedCashDraft.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new PDFPrintResultDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [NonAction]
        private List<IProcessingStatus> ExcecuteWithUSC(string application, string OutputPlugging, Dictionary<string, string> dataDictionary, string gymCode, string user)
        {
            List<IProcessingStatus> lists = null;
            try
            {
                lists = TemplateManager.ExecuteTemplate(dataDictionary, application, OutputPlugging, gymCode);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                throw ex;
            }
            return lists;
        }



        #endregion

        #region Withdrawal
        [HttpPost]
        [Route("SaveWithdrawal")]
        [Authorize]
        public HttpResponseMessage SaveWithdrawal( WithdrawalDC withDrawal)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.SaveWithdrawal(withDrawal.BranchId, withDrawal, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetWithdrawalByMemberId")]
        [Authorize]
        public HttpResponseMessage GetWithdrawalByMemberId(DateTime startDate, DateTime endDate, WithdrawalTypes type, int memberId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.GetWithdrawalByMemberId(startDate, endDate, type, memberId, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<WithdrawalDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<WithdrawalDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }
        #endregion

        #region Payments
        [HttpPost]
        [Route("AddShopSales")]
        [Authorize]
        public HttpResponseMessage AddShopSales(ShopSaleRequest request)
        {
            try
            {

                string user = Request.Headers.GetValues("UserName").First();
               
                PDFPrintResultDC pdfResult = new PDFPrintResultDC();
                OperationResult<SaleResultDC> result = GMSShop.AddShopSales(request.CardType, request.Installment, request.ShopSaleDetails, request.PaymentDetails, request.MemberBranchID, request.LoggedBranchID, user, ExceConnectionManager.GetGymCode(user), false, request.CustRoleType, request.SessionKey);
                //_______________________ SIGN THE SALE ________________

                if (request.Installment.AddOnList.Where(x => x.Quantity < 0).ToList().Count > 0 || (request.PaymentDetails.PayModes[0].PaymentTypeCode == "NEXTORDER" 
                    || request.PaymentDetails.PayModes[0].PaymentTypeCode == "INVOICE"))
                {
                    // dont logg transaction
                } else
                {
                    TransactionRegistrationForLedger newTran = new TransactionRegistrationForLedger();
                    double VATRate = newTran.getVatRateForArticle(Convert.ToInt32(result.OperationReturnValue.InvoiceNo), ExceConnectionManager.GetGymCode(user));
                    decimal amountVat;
                    if (VATRate == 0)
                    {
                        amountVat = 0;
                    }
                    else
                    {
                        amountVat = request.Installment.Amount / (decimal)VATRate;
                    }
                    newTran.initiateRegisterOfSale(request.ShopSaleDetails.SalesPointId, request.LoggedBranchID, DateTime.Now, DateTime.Now, Convert.ToInt32(result.OperationReturnValue.InvoiceNo), request.Installment.Amount, amountVat, ExceConnectionManager.GetGymCode(user));

                }

                //______________________________________________________
                if (result.ErrorOccured)
                {
                
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new SaleResultDC(), ApiResponseStatus.ERROR, errorMsg));
                }

                try
                {
                    if (request.PaymentDetails != null && request.PaymentDetails.PayModes != null && result.OperationReturnValue.SaleStatus == SaleErrors.SUCCESS)
                    {
                        if (request.PaymentDetails.PayModes.FirstOrDefault(x => x.PaymentTypeCode == "EMAILSMSINVOICE") != null)
                        {
                            try
                            {
                                var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"].ToString();
                                var dataDictionary = new Dictionary<string, string>() { { "itemID", result.OperationReturnValue.AritemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                                pdfResult = GetFilePathForPdf(appUrl, "PDF", user, request.MemberBranchID, dataDictionary);
                                SetPDFFileParth(result.OperationReturnValue.AritemNo, pdfResult.FileParth, "INV", request.MemberBranchID, user);

                                var InvoiceDetails = GMSInvoice.GetInvoiceDetails(result.OperationReturnValue.AritemNo, request.MemberBranchID, ExceConnectionManager.GetGymCode(user));

                               
                                if (InvoiceDetails.ErrorOccured)
                                {
                                    List<string> errorMsg = new List<string>();
                                    foreach (NotificationMessage message in InvoiceDetails.Notifications)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                        errorMsg.Add(message.Message);
                                    }

                                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<SalePointDC>(), ApiResponseStatus.ERROR, errorMsg));
                                }

                                ExcelineInvoiceDetailDC invoice = InvoiceDetails.OperationReturnValue;

                                // create parameter string for notification
                                Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                                paraList.Add(TemplateFieldEnum.INVOICENO, invoice.InvoiceNo.ToString());
                                paraList.Add(TemplateFieldEnum.DUEDATE, invoice.InvoiceDueDate.ToString());
                                paraList.Add(TemplateFieldEnum.AMOUNT, invoice.InvoiceAmount.ToString());
                                paraList.Add(TemplateFieldEnum.KID, invoice.BasicDetails.KID.ToString());
                                paraList.Add(TemplateFieldEnum.GYMACCOUNT, invoice.OtherDetails.BranchAccountNo);
                                paraList.Add(TemplateFieldEnum.GYMNAME, invoice.OtherDetails.BranchName.ToString());

                                // create common notification
                                USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                                commonNotification.IsTemplateNotification = true;
                                commonNotification.ParameterString = paraList;
                                commonNotification.BranchID = request.MemberBranchID;
                                commonNotification.CreatedUser = user;
                                commonNotification.Role = "MEM";
                                commonNotification.NotificationType = NotificationTypeEnum.Messages;
                                commonNotification.Severity = NotificationSeverityEnum.Minor;
                                commonNotification.MemberID = result.OperationReturnValue.MemberId;
                                commonNotification.Status = NotificationStatusEnum.New;

                                // FOR SMS **************************************************************
                                commonNotification.Type = TextTemplateEnum.SMSINVOICE;
                                commonNotification.Method = NotificationMethodType.SMS;
                                commonNotification.Title = "INVOICE SMS";

                                //add SMS notification into notification tables
                                USImportResult<int> SMSresult = NotificationAPI.AddNotification(commonNotification);

                                if (SMSresult.ErrorOccured)
                                {
                                    List<string> errorMsg = new List<string>();
                                    foreach (NotificationMessage message in InvoiceDetails.Notifications)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                        errorMsg.Add(message.Message);
                                    }

                                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                                }
                                else
                                {
                                    string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                                    string outputFormat = "SMS";

                                    try
                                    {
                                        // send SMS through the USC API
                                        IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), SMSresult.MethodReturnValue.ToString());

                                        NotificationStatusEnum status = NotificationStatusEnum.Attended;
                                        if (smsStatus != null)
                                            status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                        var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                                        // update status in notification table
                                        USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(SMSresult.MethodReturnValue, user, status, statusMessage);

                                        if (updateResult.ErrorOccured)
                                        {
                                            foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                            {
                                                if (message.ErrorLevel == ErrorLevels.Error)
                                                {
                                                    USLogError.WriteToFile(message.Message, new Exception(), user);
                                                }
                                            }
                                            pdfResult.ErrorStateId = -4;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        USLogError.WriteToFile(e.Message, new Exception(), user);
                                        pdfResult.ErrorStateId = -4;
                                    }
                                }

                                // FOR EMAIL **************************************************************
                                commonNotification.Type = TextTemplateEnum.EMAILINVOICE;
                                commonNotification.Method = NotificationMethodType.EMAIL;
                                commonNotification.AttachmentFileParth = pdfResult.FileParth;
                                commonNotification.Title = "INVOICE EMAIL";

                                //add EMAIL notification into notification tables
                                USImportResult<int> EMAILresult = NotificationAPI.AddNotification(commonNotification);

                                if (result.ErrorOccured)
                                {
                                    foreach (USNotificationMessage message in EMAILresult.NotificationMessages)
                                    {
                                        if (message.ErrorLevel == ErrorLevels.Error)
                                        {
                                            USLogError.WriteToFile(message.Message, new Exception(), user);
                                        }
                                    }
                                    pdfResult.ErrorStateId = -4;
                                }
                                else
                                {
                                    string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
                                    string outputFormat = "EMAIL";

                                    try
                                    {
                                        // send EMAIL through the USC API
                                        IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), EMAILresult.MethodReturnValue.ToString());

                                        NotificationStatusEnum status = NotificationStatusEnum.Attended;

                                        if (emailStatus != null)
                                            status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                        var statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                                        // update status in notification table
                                        USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(EMAILresult.MethodReturnValue, user, status, statusMessage);

                                        if (updateResult.ErrorOccured)
                                        {
                                            foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                            {
                                                if (message.ErrorLevel == ErrorLevels.Error)
                                                {
                                                    USLogError.WriteToFile(message.Message, new Exception(), user);
                                                }
                                            }
                                            pdfResult.ErrorStateId = -4;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        USLogError.WriteToFile(e.Message, new Exception(), user);
                                        pdfResult.ErrorStateId = -4;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                USLogError.WriteToFile(e.Message, new Exception(), user);
                                pdfResult.ErrorStateId = -4;
                            }
                        }

                        if (request.PaymentDetails.PayModes.Where(x => x.PaymentTypeCode == "INVOICE").FirstOrDefault() != null && request.PrintInvoice)
                        {
                            var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"].ToString();
                            var dataDictionary = new Dictionary<string, string>() { { "itemID", result.OperationReturnValue.AritemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                            pdfResult = GetFilePathForPdf(appUrl, "PDF", user, request.MemberBranchID, dataDictionary);
                            SetPDFFileParth(result.OperationReturnValue.AritemNo, pdfResult.FileParth, "INV", request.MemberBranchID, user);
                            
                        }
                    }
                }
                catch (Exception e)
                {
                    USLogError.WriteToFile(e.Message, new Exception(), user);
                    pdfResult.ErrorStateId = -4;
                }

               

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new SaleResultDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [NonAction]
        private PDFPrintResultDC GetFilePathForPdf(string application, string OutputPlugging, string user, int branchId, Dictionary<string, string> dataDictionary)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var list = ExcecuteWithUSC(application, OutputPlugging, dataDictionary, ExceConnectionManager.GetGymCode(user), user);
                foreach (var item in list)
                {
                    var Message = item.Messages.Where(mssge => mssge.Header.ToLower().Equals("filepath")).First();
                    if (Message != null)
                    {
                        result.FileParth = Message.Message;
                        break;
                    }
                }

                // for get visibility on browser.
                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return result;
        }

        [NonAction]
        private void SetPDFFileParth(int id, string fileParth, string flag, int branchId, string user)
        {
            GMSManageMembership.SetPDFFileParth(id, fileParth, flag, branchId, ExceConnectionManager.GetGymCode(user), user);
        }


        #endregion

        #region Discount
        [HttpGet]
        [Route("GetDiscountList")]
        [Authorize]
        public HttpResponseMessage GetDiscountList(int branchId, DiscountType discountType)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetDiscountList(branchId, discountType, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new DiscountDC(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new DiscountDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        #endregion

        #region OpenCashDrawer
        [HttpPost]
        [Route("SaveOpenCashRegister")]
        [Authorize]
        public HttpResponseMessage SaveOpenCashRegister(CashRegisterDC cashRegister)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.SaveOpenCashRegister(cashRegister.BranchId, cashRegister, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        #endregion

        [HttpGet]
        [Route("IsCashdrawerOpen")]
        [Authorize]
        public HttpResponseMessage IsCashdrawerOpen(string machinename)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.IsCashdrawerOpen(machinename, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("GetInvoicesForArticleReturn")]
        [Authorize]
        public HttpResponseMessage GetInvoicesForArticleReturn(int memberID, int branchId, string articleNo, int shopOnly)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.GetInvoicesForArticleReturn(memberID, branchId, articleNo, shopOnly, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


    }
}
