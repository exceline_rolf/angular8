﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Common.Notification.Data.DBCommands;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.Utils;
using US_DataAccess;
using System.Linq;

namespace US.Common.Notification.Data
{
    public class NotificationFacade
    {
        public static int AddNotification(USNotificationTree notification)
        {
            return new AddNotificationAction(notification).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static int AddNotification(USCommonNotificationDC commonNotification)
        {
            if (commonNotification.MemberIdList != null && commonNotification.MemberIdList.Keys.Count > 0)
            {
                foreach (var memberId in commonNotification.MemberIdList)
                {
                    commonNotification.MemberID = memberId.Key;
                    if (commonNotification.ParameterString != null &&  commonNotification.ParameterString.Keys.Count > 0)
                    {
                        if(commonNotification.ParameterString.ContainsKey(TemplateFieldEnum.MEMBERNAME))
                            commonNotification.ParameterString[TemplateFieldEnum.MEMBERNAME] = memberId.Value;

                    }
                    var actionSms = new AddCommonNotificationAction(commonNotification);
                    actionSms.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(commonNotification.CreatedUser));
                }
                return 1;
            }
            var action = new AddCommonNotificationAction(commonNotification);
            return action.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(commonNotification.CreatedUser));
        }

        public static bool UpdateNotification(USNotification notification)
        {
            return new UpdateNotificationAction(notification).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static bool UpdateNotificationStatus(List<int> selectedIds, int status, string user)
        {
            return new UpdateNotificationStatusAction(selectedIds, status, user).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static bool UpdateNotificationStatus(int notificationID, string user, NotificationStatusEnum status, string statusMessage)
        {
            var action = new UpdateCommonNotificationStatusAction(notificationID, user, status, statusMessage);
            return action.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(user));
        }

        public static bool UpdateNotificationStatusWithDeliveryReport(int notificationId, string mobileNo, int statusId, string statusMessage, string user)
        {
            var action = new UpdateNotificationStatusWithDeliveryReportAction(notificationId, mobileNo, statusId, statusMessage, user);
            return action.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(user));
        }

        public static List<USNotificationSummaryInfo> GetNotificationSummaryList(int branchId, NotificationStatusEnum status, string user)
        {
            return new GetNotificationSummaryListAction( branchId,status, user).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static List<USNotificationSummaryInfo> GetMemberNotificationSummaryList(int memberId, NotificationStatusEnum status, string user)
        {
            return new GetMemberNotificationListAction(memberId, status, user).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static USNotificationSummaryInfo GetNotificationByIdAction(int notificationId, string user)
        {
            return new GetNotificationByIdAction(user, notificationId).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static List<USNotificationSummaryInfo> GetNotificationsByMemberId(int memeberId, string user)
        {
            return new GetNotificationsByMemberIdAction(user, memeberId).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static int SaveNotificationTextTemplate(int entityId, NotificationTemplateText templatetText, int branchId, string loggedUser)
        {
            return new SaveNotificationTextTemplateAction(entityId, templatetText, branchId, loggedUser).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static List<USNotificationSummaryInfo> GetNotificationSearchResult(int branchId, string user, string title, string assignTo, DateTime? receiveDateFrom, DateTime? receiveDateTo, DateTime? dueDateFrom, DateTime? dueDateTo, List<int> severityIdList, List<int> typeIdList, int statusId)
        {
            return new GetNotificationSearchResultAction( branchId,user, title, assignTo, receiveDateFrom, receiveDateTo, dueDateFrom, dueDateTo, severityIdList, typeIdList, statusId).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static List<USNotificationSummaryInfo> GetMemberNotificationSearchResult(int membrID, string user, string title, string assignTo, DateTime? receiveDateFrom, DateTime? receiveDateTo, DateTime? dueDateFrom, DateTime? dueDateTo, List<int> severityIdList, List<int> typeIdList, int statusId)
        {
            return new GetMemberNotificationSearchResultAction(membrID, user, title, assignTo, receiveDateFrom, receiveDateTo, dueDateFrom, dueDateTo, severityIdList, typeIdList, statusId).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static List<NotificationTemplateText> GetNotificationTemplateText(string methodCode, string typeCode, string gymCode)
        {
            var action = new GetNotificationTemplateTextAction(methodCode, typeCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public static NotificationTemplateText GetNotificationTextByType(NotifyMethodType method, TextTemplateEnum type, string user, int branchId)
        {
            return new GetNotificationTextByTypeAction(method, type, user, branchId).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static List<USNotificationSummaryInfo> GetNotificationByNotifiyMethod(int memberId, string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            return new GetMemberNotificationByMethodAction(memberId, user, branchId, fromDate, toDate, notifyMethod).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static List<USNotificationSummaryInfo> GetChangeLogNotificationList(string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            return new GetChangeLogNotificationListAction(user, branchId, fromDate, toDate, notifyMethod).Execute(US_DataAccess.EnumDatabase.USP);
        }

        public static List<ExceNotificationTepmlateDC> GetNotificationTemplateListByMethod(NotificationMethodType method, TextTemplateEnum temp, string user, int branchId)
        {
            GetNotificationTemplateListByMethodAction action = new GetNotificationTemplateListByMethodAction(method, temp, user, branchId);
            return action.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(user));
        }

        public static string AddAutomatedSMSNotification(string gymCode, int branchID)
        {
            AddAutomatedSMSNotificationAction action = new AddAutomatedSMSNotificationAction(branchID);
            return action.Execute(EnumDatabase.Exceline, gymCode); 
        }

        public static List<NotificationSMS> GetNotificationSMSToSend(int notificationID, string gymCode, string user)
        {
            var action = new GetNotificationSMSToSendAction(notificationID, gymCode);
            var smsList = action.Execute(EnumDatabase.Exceline, gymCode);
            var updateAction = UpdateSMSNotificationList(smsList, gymCode, user);
            return !updateAction ? new List<NotificationSMS>() : smsList;
        }
        public static bool UpdateSMSNotificationList(List<NotificationSMS> smsList, string gymCode, string user)
        {
            var action = new UpdateSMSNotificationListAction(smsList, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public static List<NotificationEmail> GetNotificationEmailToSend(int notificationID, string gymCode, string user)
        {
            var action = new GetNotificationEmailToSendAction(notificationID);
            var emailList = action.Execute(EnumDatabase.Exceline, gymCode);
            var updateAction = UpdateEmailNotificationList(emailList, gymCode, user);
            return !updateAction ? new List<NotificationEmail>() : emailList;
        }
        public static bool UpdateEmailNotificationList(List<NotificationEmail> emailList, string gymCode, string user)
        {
            var action = new UpdateEmailNotificationListAction(emailList, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public static bool AddNotificationWithReportScheduler(string gymCode, string user)
        {
            var action = new AddNotificationWithReportSchedulerAction(user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public static bool UpdateNotificationWithReceiveSMS(string gymCompanyRef, string notificationCode, string message, string sender, string user)
        {
            try
            {
                var gymCode = string.Empty;
                var tempAction = new GetGymCodeByCompanyRefAction(gymCompanyRef);
                gymCode = tempAction.Execute(EnumDatabase.WorkStation);

                if (string.IsNullOrEmpty(gymCode))
                    new Exception("Gym Code Not Provided.");

                var action = new UpdateNotificationWithReceiveSMSAction(notificationCode, message, sender, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ExceNotificationScheduleItem> AutomatedPrintPDFGetItemList(string gymCode, string user)
        {
            try
            {
                var action = new AutomatedPrintPDFGetItemListAction(user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool AutomatedPrintPDFUpdateItemList(List<ExceNotificationScheduleItem> itemList, string gymCode, string user)
        {
            try
            {
                var action = new AutomatedPrintPDFUpdateItemListAction(itemList, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<EntityDC> GetEntityList(string gymCode)
        {
            try
            {
                var action = new GetEntityListAction();
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<CategoryDC> GetTemplateTextKeyByEntity(string type, int entity, string gymCode)
        {
            try
            {
                var action = new GetTemplateTextKeyByEntityAction(type, entity);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ExceNotificationTepmlateDC> GetSMSNotificationTemplateByEntity(string entity, string gymCode)
        {
            try
            {
                var action = new GetSMSNotificationTemplateByEntityAction(entity);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int AddSMSNotification(string subject, string body, string mobileNo, string entity, int entityId, int memberId, int branchId, string user, string gymCode)
        {
            try
            {
                var action = new AddSMSNotificationAction(subject, body, mobileNo, entity, entityId, memberId, branchId, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<AccessDeniedLog> GetAccessDeniedList(DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            try
            {
                var action = new GetAccessDeniedListAction(fromDate, toDate, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
