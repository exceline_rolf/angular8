﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    class UpdateSMSNotificationListAction : USDBActionBase<bool>
    {
        private readonly List<NotificationSMS> _smsList = new List<NotificationSMS>();
        private readonly string _user = string.Empty;

        public UpdateSMSNotificationListAction(List<NotificationSMS> smsList, string user)
        {
            _smsList = smsList;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            try
            {
                const string storedProcedure = "US_UpdateNotificationStatusList";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notificationStatusList", SqlDbType.Structured, GetNotificationList()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                DbParameter para = new SqlParameter();
                para.DbType = DbType.Boolean;
                para.ParameterName = "@outPut";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);
                cmd.ExecuteNonQuery();

                return Convert.ToBoolean(para.Value);
            }
            catch (Exception)
            {
                throw;
            }
        }


        private DataTable GetNotificationList()
        {
            try
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add(new DataColumn("NotificationID", typeof(int)));
                dataTable.Columns.Add(new DataColumn("StatusCode", typeof(string)));
                dataTable.Columns.Add(new DataColumn("StatusMessage", typeof(string)));

                foreach (var sms in _smsList)
                {
                    var dataTableRow = dataTable.NewRow();
                    dataTableRow["NotificationID"] = sms.NotificationID;
                    dataTableRow["StatusCode"] = sms.StatusCode;
                    dataTableRow["StatusMessage"] = sms.StatusMessage;
                    dataTable.Rows.Add(dataTableRow);
                }

                return dataTable;
            }
            catch
            {
                throw;
            }
        }
    }
}
