﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class GetTemplateTextKeyByEntityAction : USDBActionBase<List<CategoryDC>>
    {
        private readonly string _type;
        private readonly int _entity;

        public GetTemplateTextKeyByEntityAction(string type, int entity)
        {
            _type = type;
            _entity = entity;
        }

        protected override List<CategoryDC> Body(DbConnection connection)
        {
            var categoryList = new List<CategoryDC>();
            const string storedProcedureName = "USExceGMSGetTemplateTextKeyByEntity";
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _type));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entity", DbType.Int32, _entity));
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var category = new CategoryDC();
                    category.Id = Convert.ToInt32(reader["ID"]);
                    category.TypeId = Convert.ToInt32(reader["CategoryTypeId"]);
                    if (!string.IsNullOrEmpty(_type))
                    {
                        if (reader["CategoryTypeCode"] != DBNull.Value)
                        {
                            category.CategoryTypeCode = Convert.ToString(reader["CategoryTypeCode"]);
                        }
                    }
                    category.Code = reader["Code"].ToString();
                    category.Name = reader["Description"].ToString();
                    category.Description = reader["Details"].ToString();
                    categoryList.Add(category);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return categoryList;
        }
    }
}
