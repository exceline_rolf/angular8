﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class GetNotificationTemplateTextAction : USDBActionBase<List<NotificationTemplateText>>
    {
        private readonly string _methodCode = string.Empty;
        private readonly string _typeCode = string.Empty;

        public GetNotificationTemplateTextAction(string methodCode, string typeCode)
        {
            _methodCode = methodCode;
            _typeCode = typeCode;
        }

        protected override List<NotificationTemplateText> Body(DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "dbo.US_GetNotificationTemplateTextAction";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(new SqlParameter("@methodCode", _methodCode));
                cmd.Parameters.Add(new SqlParameter("@typeCode", _typeCode));

                var reader = cmd.ExecuteReader();

                var notificationTextList = new List<NotificationTemplateText>();
                while (reader.Read())
                {
                    var notificationText = new NotificationTemplateText();
                    notificationText.Id = Convert.ToInt32(reader["Id"]);
                    notificationText.Text = Convert.ToString(reader["Text"]);
                    notificationText.MethodID = Convert.ToInt32(reader["MethodId"]);
                    notificationText.TypeID = Convert.ToInt32(reader["TypeId"]);
                    notificationTextList.Add(notificationText);
                }

                return notificationTextList;
            }
            catch
            {
                throw;
            }
        }
    }
}
