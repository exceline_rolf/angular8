﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    class UpdateNotificationStatusWithDeliveryReportAction : USDBActionBase<bool>
    {
        private int _notificationId = -1;
        private string _mobileNo = string.Empty;
        private int _statusId = -1;
        private string _statusMessage = string.Empty;
        private string _user = string.Empty;

        public UpdateNotificationStatusWithDeliveryReportAction(int notificationId, string mobileNo, int statusId, string statusMessage, string user)
        {
            _notificationId = notificationId;
            _mobileNo = mobileNo;
            _statusId = statusId;
            _statusMessage = statusMessage;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedure = "US_NotificationUpdateSMSDeliveryReport";

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notificationId", DbType.Int32, _notificationId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobileNo", DbType.String, _mobileNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@statusId", DbType.Int32, _statusId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@statusMessage", DbType.String, _statusMessage));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                DbParameter para = new SqlParameter();
                para.DbType = DbType.Boolean;
                para.ParameterName = "@outPut";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);
                cmd.ExecuteNonQuery();

                return Convert.ToBoolean(para.Value);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
