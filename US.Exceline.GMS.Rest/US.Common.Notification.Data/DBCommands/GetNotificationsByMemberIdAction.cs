﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class GetNotificationsByMemberIdAction : USDBActionBase<List<USNotificationSummaryInfo>>
    {
        private int _memberID;
        private string _loggedUser = string.Empty;

        public GetNotificationsByMemberIdAction(string user, int memberId)
        {
            _memberID = memberId;
            _loggedUser = user;
            OverwriteUser(_loggedUser);
        }


        protected override List<USNotificationSummaryInfo> Body(DbConnection connection)
        {
            try
            {
                string storedProcedureName = "dbo.US_GetNotificationsByMemberId";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(new SqlParameter("@memberId", _memberID));

                DbDataReader reader = cmd.ExecuteReader();

                List<USNotificationSummaryInfo> notificationSummaryList = new List<USNotificationSummaryInfo>();

                while (reader.Read())
                {


                    USNotificationSummaryInfo notificationSummary = new USNotificationSummaryInfo();

                    notificationSummary.Id = Convert.ToInt32(reader["NotificationId"]);
                    notificationSummary.SeverityId = Convert.ToInt32(reader["SeverityId"]);
                    notificationSummary.Severity = reader["Severity"].ToString();
                    notificationSummary.TypeId = Convert.ToInt32(reader["TypeId"]);
                    notificationSummary.Type = reader["Type"].ToString();
                    notificationSummary.Title = reader["Title"].ToString();
                    notificationSummary.Description = reader["Description"].ToString();
                    notificationSummary.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    notificationSummary.DueDate = (reader["DueDate"] != DBNull.Value) ? Convert.ToDateTime(reader["DueDate"]) : (DateTime?)null;
                    notificationSummary.AssignTo = reader["AssignTo"].ToString();
                    notificationSummary.LastAttendDate = (reader["LastAttendDate"] != DBNull.Value) ? Convert.ToDateTime(reader["LastAttendDate"]) : (DateTime?)null;
                    notificationSummary.StatusId = Convert.ToInt32(reader["StatusId"]);
                    if (notificationSummary.StatusId == 1)
                    {
                        notificationSummary.IsNew = true;
                    }
                    notificationSummary.Status = reader["Status"].ToString();

                    switch (notificationSummary.Type)
                    {
                        case "Errors":
                            notificationSummary.ErrorsIconVisibility = "Visible";
                            break;
                        case "Warning":
                            notificationSummary.WarningIconVisibility = "Visible";
                            break;
                        case "TODO":
                            notificationSummary.TodoIconVisibility = "Visible";
                            break;
                        case "Messages":
                            notificationSummary.MessagesIconVisibility = "Visible";
                            break;
                    }

                    switch (notificationSummary.Severity)
                    {
                        case "Critical":
                            notificationSummary.CriticalIconVisibility = "Visible";
                            break;
                        case "Moderate":
                            notificationSummary.ModerateIconVisibility = "Visible";
                            break;
                        case "Minor":
                            notificationSummary.MinorIconVisibility = "Visible";
                            break;
                    }
                    notificationSummaryList.Add(notificationSummary);
                }

                return notificationSummaryList;
            }
            catch
            {
                throw;
            }

        }
    }
}
