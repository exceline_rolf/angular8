﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.GMS.Core.DomainObjects.Notification;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class GetNotificationTextByTypeAction : USDBActionBase<NotificationTemplateText>
    {
        private NotifyMethodType _method = NotifyMethodType.NONE;
        private string _user = string.Empty;
        private TextTemplateEnum _type = TextTemplateEnum.NONE;
        private int _branchId;

        public GetNotificationTextByTypeAction(NotifyMethodType method, TextTemplateEnum type, string user, int branchId)
        {
            _method = method;
            _branchId = branchId;
            _type = type;
            _user = user;
            OverwriteUser(_user);
        }


        protected override NotificationTemplateText Body(DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "dbo.US_GetNotificationTemplateTextByType";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _type));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@method", DbType.String, _method.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();
                var notificationText = new NotificationTemplateText();

                while (reader.Read())
                {
                    notificationText.Id = Convert.ToInt32(reader["Id"]);
                    notificationText.TypeID = Convert.ToInt32(reader["TypeId"]);
                    notificationText.Text = Convert.ToString(reader["Text"]);
                    notificationText.MethodID = Convert.ToInt32(reader["MethodId"]);
                }
                return notificationText;
            }
            catch
            {
                throw;
            }

        }
    }
}
