﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/28/2012 3:38:49 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class SaveNotificationTextTemplateAction : USDBActionBase<int>
    {
        private int _entityId = -1;
        private string _loggedUser = string.Empty;
        private int _branchId = -1;
        private NotificationTemplateText _templatetText = new NotificationTemplateText();

        public SaveNotificationTextTemplateAction(int entityId, NotificationTemplateText templatetText, int branchId, string loggedUser)
        {
            _entityId = entityId;
            _loggedUser = loggedUser;
            _templatetText = templatetText;
            _branchId = branchId;

            OverwriteUser(_loggedUser);
        }

        protected override int Body(DbConnection connection)
        {
            int _result = -1;
            try
            {
                string storedProcedure = "dbo.US_SaveNotificationTemplateText";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@templateText", DbType.String, _templatetText.Text));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, _templatetText.Id));
                if (_templatetText.TypeID > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@typeId", DbType.Int32, _templatetText.TypeID));
                }
                if (_templatetText.MethodID > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@methodId", DbType.Int32, _templatetText.MethodID));
                }
                if (_branchId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                }
                DbParameter outPut = new SqlParameter();
                outPut.DbType = DbType.Int32;
                outPut.ParameterName = "@textTemplateId";
                outPut.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outPut);
                cmd.ExecuteNonQuery();
                _result = Convert.ToInt32(outPut.Value);
            }
            catch
            {
                throw;
            }
            return _result;
        }
    }
}
