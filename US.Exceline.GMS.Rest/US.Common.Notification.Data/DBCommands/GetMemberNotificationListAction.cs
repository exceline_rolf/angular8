﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Xml.Linq;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US_DataAccess;


namespace US.Common.Notification.Data.DBCommands
{
    public class GetMemberNotificationListAction : USDBActionBase<List<USNotificationSummaryInfo>>
    {

        private NotificationStatusEnum _status;
        private string _user;
        private int _memberId = -1;

        public GetMemberNotificationListAction(int memberId, NotificationStatusEnum status, string user)
        {
            this._status = status;
            this._user = user;
            _memberId = memberId;
            OverwriteUser(_user);
        }

        protected override List<USNotificationSummaryInfo> Body(System.Data.Common.DbConnection connection)
        {
            List<USNotificationSummaryInfo> notificationSummaryList = new List<USNotificationSummaryInfo>();

            try
            {
                string storedProcedureName = "dbo.US_GetMemberNotificationSummaryList";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(new SqlParameter("@status", (int)_status));
                cmd.Parameters.Add(new SqlParameter("@user", _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var notificationSummary = new USNotificationSummaryInfo();

                    notificationSummary.Id = Convert.ToInt32(reader["NotificationId"]);
                    notificationSummary.SeverityId = Convert.ToInt32(reader["SeverityId"]);
                    notificationSummary.Severity = reader["Severity"].ToString();
                    notificationSummary.TypeId = Convert.ToInt32(reader["TypeId"]);
                    notificationSummary.Type = reader["Type"].ToString();
                    notificationSummary.Title = GetTitleWithoutXML(reader["Title"].ToString());
                    notificationSummary.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    notificationSummary.DueDate = (reader["DueDate"] != DBNull.Value) ? Convert.ToDateTime(reader["DueDate"]) : (DateTime?)null;
                    notificationSummary.AssignTo = reader["AssignTo"].ToString();
                    notificationSummary.LastAttendDate = (reader["LastAttendDate"] != DBNull.Value) ? Convert.ToDateTime(reader["LastAttendDate"]) : (DateTime?)null;
                    notificationSummary.StatusId = Convert.ToInt32(reader["StatusId"]);
                    notificationSummary.Status = reader["Status"].ToString();

                    switch (notificationSummary.Type)
                    {
                        case "Errors":
                            notificationSummary.ErrorsIconVisibility = "Visible";
                            break;
                        case "Warning":
                            notificationSummary.WarningIconVisibility = "Visible";
                            break;
                        case "TODO":
                            notificationSummary.TodoIconVisibility = "Visible";
                            break;
                        case "Messages":
                            notificationSummary.MessagesIconVisibility = "Visible";
                            break;
                    }

                    switch (notificationSummary.Severity)
                    {
                        case "Critical":
                            notificationSummary.CriticalIconVisibility = "Visible";
                            break;
                        case "Moderate":
                            notificationSummary.ModerateIconVisibility = "Visible";
                            break;
                        case "Minor":
                            notificationSummary.MinorIconVisibility = "Visible";
                            break;
                    }

                    notificationSummaryList.Add(notificationSummary);
                }
            }
            catch
            {
                throw;
            }
            return notificationSummaryList;
        }

        private string GetTitleWithoutXML(string title)
        {
            try
            {
                string newtitle = string.Empty;
                bool hasLinks = false;

                if (title.Contains("NotificationLink"))
                {
                    hasLinks = true;
                    var checkString = title;
                    while (hasLinks)
                    {
                        var firstIndex = checkString.IndexOf("<NotificationLink>");
                        var lastIndex = checkString.IndexOf("</NotificationLink>");

                        if (firstIndex != 0)
                        {
                            var normalText = checkString.Substring(0, firstIndex);

                            newtitle += normalText;
                        }

                        var linkData = checkString.Substring(firstIndex, (lastIndex - firstIndex) + 19);
                        newtitle += GetLinkText(linkData);

                        checkString = checkString.Substring(lastIndex + 19);

                        if (!checkString.Contains("NotificationLink"))
                        {
                            hasLinks = false;

                            newtitle += checkString;
                        }
                    }

                    return newtitle;
                }
                else
                {
                    return title;
                }
            }
            catch
            {
                throw;
            }
        }

        private string GetLinkText(string xmlContent)
        {

            var doc = XDocument.Parse(xmlContent);
            var result = doc.Descendants("NotificationLink").FirstOrDefault();
            var linkText = result.Element("LinkText").Value;
            return linkText;
        }
    }
}
