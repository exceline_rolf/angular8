﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Notification;

using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Notifications.API.Notifications;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.API;
using US.GMS.Core.Utils;


namespace US.Exceline.GMS.Modules.Notifications.Service
{
    public class NotificationsService : INotificationsService
    {
        public string Test()
        {
            return string.Empty;
        }

        public int SaveNotification(NotificationDC notification, string createdUser, int branchId)
        {
            OperationResult<int> result = new OperationResult<int>();
            result = GMSNotification.SaveNotification(notification, createdUser, branchId, ExceConnectionManager.GetGymCode(createdUser));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), createdUser);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<NotificationMethodDC> GetNotifications(int branchId, string user )
        {
            OperationResult<List<NotificationMethodDC>> result = new OperationResult<List<NotificationMethodDC>>();
            result = GMSNotification.GetNotifications(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<NotificationMethodDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryDC> GetCategories(string type, string user, int branchId)
        {
            OperationResult<List<CategoryDC>> result = GMSCategory.GetCategoriesByType(type, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>(); ;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int UpdateIsNotified(int notificationMethodId, string user, int branchId)
        {
            OperationResult<int> result = GMSNotification.UpdateIsNotified(notificationMethodId, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteNotificationMethod(int notificationMethodId, string user, int branchId)
        {
            OperationResult<bool> result = GMSNotification.DeleteNotificationMethod(notificationMethodId, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
    }
}
