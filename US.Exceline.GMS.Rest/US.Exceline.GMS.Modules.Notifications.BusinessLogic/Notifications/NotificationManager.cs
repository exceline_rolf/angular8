﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.Exceline.GMS.Modules.Notifications.BusinessLogic
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 19/6/2012 4:27:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Notification;
using US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.ManageNotifications;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Notifications.BusinessLogic.Notifications
{
    public class NotificationManager
    {
        public static OperationResult<int> SaveNotification(NotificationDC notification, string createdUser, int branchId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                int notificationId = -1;
                result.OperationReturnValue = ManageNotificationsFacade.SaveNotification(notification, createdUser, branchId, gymCode);
                if (notification != null)
                {
                    notificationId = result.OperationReturnValue;
                    foreach (NotificationMethodDC method in notification.MethodList)
                    {
                        method.NotificationID = notificationId;
                        ManageNotificationsFacade.SaveNotificationMethod(method, createdUser, branchId, gymCode);
                    }
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Adding Notifications " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveNotificationTemplate(NotificationTemplateDC notificationTemplate, string createdUser, int branchId,string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageNotificationsFacade.SaveNotificationTemplate(notificationTemplate, createdUser, branchId, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Adding Notification templates " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<NotificationMethodDC>> GetNotifications(int branchId,string gymCode)
        {
            OperationResult<List<NotificationMethodDC>> result = new OperationResult<List<NotificationMethodDC>>();
            try
            {
                result.OperationReturnValue = ManageNotificationsFacade.GetNotifications(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retrieving Notifications " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<List<NotificationTemplateDC>> GetNotificationTemplates(int branchId, string gymCode)
        {
            OperationResult<List<NotificationTemplateDC>> result = new OperationResult<List<NotificationTemplateDC>>();
            try
            {
                result.OperationReturnValue = ManageNotificationsFacade.GetNotificationTemplates(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retrieving Notification Templates " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<List<NotificationMethodDC>> GetNotificationsByMember(int branchId, int memeberId,string gymCode)
        {
            OperationResult<List<NotificationMethodDC>> result = new OperationResult<List<NotificationMethodDC>>();
            try
            {
                result.OperationReturnValue = ManageNotificationsFacade.GetNotificationsByMember(branchId, memeberId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retrieving Notifications " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<int> UpdateIsNotified(int notificationMethodId, string createdUser, int branchId,string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageNotificationsFacade.UpdateIsNotified(notificationMethodId, createdUser, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updatig isNotified field  " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteNotificationMethod(int notificationMethodId, string createdUser, int branchId,string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageNotificationsFacade.DeleteNotificationMethod(notificationMethodId, createdUser, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in deleting notification method" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
