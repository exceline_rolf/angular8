﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.IBooking.Core
{
    public class ARXSetting
    {
        private int _id = -1;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _gymCode = string.Empty;
        public string GymCode
        {
            get { return _gymCode; }
            set { _gymCode = value; }
        }

        private string _serviceURL = string.Empty;
        public string ServiceURL
        {
            get { return _serviceURL; }
            set { _serviceURL = value; }
        }

        private string _userName = string.Empty;
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private string _password = string.Empty;
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
    }
}
