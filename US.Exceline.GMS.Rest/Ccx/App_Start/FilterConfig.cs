﻿using System.Web;
using System.Web.Mvc;

namespace US.Exceline.GMS.CCX.DueDateUpdateService.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}