﻿using US.Common.Logging.DB.DBCommands;
using US.DBAccess.Core.DomainObjects;

namespace US.Common.Logging.DB
{
    public class LogDBManager
    {
        
        DBSettings _dbSettings = new DBSettings();     
        public LogDBManager()        
        {

            _dbSettings.ConnectionString = USLogConfigurationReader.GetUSPCongfiuration().UsLogDBConnection;
        }

        
        public  void WrightEventToSQLDB(string message, string User, string applicationName, string category = "", string eventDescription1 = "", string eventDescription2 = "", string eventDescription3 = "")
        {

            AddEventLogAction dbAction = new AddEventLogAction(_dbSettings, message, User,applicationName, category, eventDescription1, eventDescription2, eventDescription3);
            dbAction.Execute();

        }
        public void WrightErrorToSQLDB( string message, string user, string code, string applicationName, string category, string eventDescription1 = "", string eventDescription2 = "", string eventDescription3 = "")
        {

            AddErrorLogAction dbAction = new AddErrorLogAction(_dbSettings, message, user,code, applicationName, category, eventDescription1, eventDescription2, eventDescription3);
            dbAction.Execute();

        }
    }
}
