﻿using System;
using UD.Framework.Core.Data;
using US.DBAccess.Core.DomainObjects;
using System.Data.Common;
using System.Data;

namespace US.Common.Logging.DB.DBCommands
{
    public class AddErrorLogAction : UDAbstractDataAction<int>
    {
        private string _eventName = string.Empty;
        private string _category = string.Empty;
        private string _eventDescription1 = string.Empty;
        private string _eventDescription2 = string.Empty;
        private string _eventDescription3 = string.Empty;
        private string _eventUser = string.Empty;
        protected string _applicationName = string.Empty;
        private string _code = string.Empty;
        public AddErrorLogAction(DBSettings dbSettings, string message, string user, string code, string applicationName, string category, string eventDescription1 = "", string eventDescription2 = "", string eventDescription3 = "")
        {
            _eventName = message;
            _eventUser = user;
            _eventDescription1 = eventDescription1;
            _eventDescription1 = eventDescription2;
            _eventDescription1 = eventDescription3;
            _applicationName = applicationName;
            _code = code;
            OverrideConnectionString(dbSettings.ConnectionString);
        }



        protected override int Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                DBManager db = new DBManager();
                string storedProcedureName = "US.AddEventLog";
                DbCommand cmd = db.getCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = connection;
                cmd.Parameters.Add(DBManager.CreateParam("@Name", DbType.String, _eventName.Trim()));
                cmd.Parameters.Add(DBManager.CreateParam("@ApplicationName", DbType.String, _eventName.Trim()));
                if (!string.IsNullOrEmpty(_category))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@Category", DbType.String, _category.Trim()));
                }
                if (!string.IsNullOrEmpty(_eventDescription1))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@Description1", DbType.String, _eventDescription1.Trim()));
                }
                if (!string.IsNullOrEmpty(_eventDescription2))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@Description2", DbType.String, _eventDescription2.Trim()));
                }
                if (!string.IsNullOrEmpty(_eventDescription3))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@Description3", DbType.String, _eventDescription3.Trim()));
                }
                if (!string.IsNullOrEmpty(_eventUser))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@UserName", DbType.String, _eventUser.Trim()));
                }
                if (!string.IsNullOrEmpty(_eventUser))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@UserName", DbType.String, _eventUser.Trim()));
                }
                if (!string.IsNullOrEmpty(_code))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@Code", DbType.String, _eventUser.Trim()));
                }
                cmd.ExecuteNonQuery();
                return 1;
            }
            catch 
            {
                return 0;
               // throw ex;
            }
        }
    }
}
