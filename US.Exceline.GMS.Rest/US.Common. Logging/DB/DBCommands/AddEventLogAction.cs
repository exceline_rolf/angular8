﻿using System;
using System.Data;
//using System.Data.SQLite;
using System.Data.Common;
using UD.Framework.Core.Data;
using US.DBAccess.Core.DomainObjects;

namespace US.Common.Logging.DB.DBCommands
{
    public class AddEventLogAction : UDAbstractDataAction<int>
    {

        private string _eventName = string.Empty;
        private string _category = string.Empty;
        private string _eventDescription1 = string.Empty;
        private string _eventDescription2 = string.Empty;
        private string _eventDescription3 = string.Empty;
        private string _eventUser = string.Empty;
        protected string _applicationName = string.Empty;
        public AddEventLogAction(DBSettings dbSettings, string message, string user, string applicationName,string category , string eventDescription1, string eventDescription2 , string eventDescription3)
        {
            _eventName = message;
            _eventUser = user;
            _eventDescription1 = eventDescription1;
            _eventDescription1 = eventDescription2;
            _eventDescription1 = eventDescription3;
            _applicationName   = applicationName;
            OverrideConnectionString(dbSettings.ConnectionString); 
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                DBManager db = new DBManager();
                string storedProcedureName = "US.AddEventLog";
                DbCommand cmd = db.getCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = connection;
                cmd.Parameters.Add(DBManager.CreateParam("@Name", DbType.String, _eventName.Trim()));
                cmd.Parameters.Add(DBManager.CreateParam("@ApplicationName", DbType.String, _eventName.Trim()));
                if (!string.IsNullOrEmpty(_category))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@Category", DbType.String, _category.Trim()));
                }
                if (!string.IsNullOrEmpty(_eventDescription1))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@Description1", DbType.String, _eventDescription1.Trim()));
                }
                if (!string.IsNullOrEmpty(_eventDescription2))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@Description2", DbType.String, _eventDescription2.Trim()));
                }
                if (!string.IsNullOrEmpty(_eventDescription3))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@Description3", DbType.String, _eventDescription3.Trim()));
                }
                if (!string.IsNullOrEmpty(_eventUser))
                {
                    cmd.Parameters.Add(DBManager.CreateParam("@UserName", DbType.String, _eventUser.Trim()));
                }
                cmd.ExecuteNonQuery();
                return 1;
            }
            catch 
            {
                 return 0;
            }
        }
        
        public  void WriteToSQLiteDB(string messages, string user)
        {
            try
            {
                //string eventName = messages;
                //string category = string.Empty;
                //string eventDescription1 = string.Empty;
                //string eventDescription2 = string.Empty;
                //string eventDescription3 = string.Empty;
                //string eventUser = user;
                //string db_file_location = @"D:\USLOG_test\USLOG.US.sqlite";


                //string connectionString = string.Format("Data Source={0}", db_file_location);
                //SQLiteConnection sqliteConnection = new SQLiteConnection(connectionString);
                //sqliteConnection.Open();
                //string insertString = string.Format
                //    ("INSERT INTO \"main\".\"USEventLog\" (\"Name\",\"Category\",\"RegDate\",\"Description1\",\"Description2\",\"Description3\",\"USer\") VALUES (\"{0}\",\"{1}\",datetime('now','localtime'),\"{2}\",\"{3}\",\"{4}\",\"{5}\")", eventName, category, eventDescription1, eventDescription2, eventDescription3, eventUser);
                ////SQLiteTransaction transaction = sqliteConnection.BeginTransaction();              
               
                //SQLiteCommand cmd = new SQLiteCommand(insertString, sqliteConnection);
                //int i = cmd.ExecuteNonQuery();
                ////transaction.Commit();
                ////transaction.Dispose();
                //sqliteConnection.Close();
            }
            catch (Exception)
            {

                throw;
            }

        }


    }
}
