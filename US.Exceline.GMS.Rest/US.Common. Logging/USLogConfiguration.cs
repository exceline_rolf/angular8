﻿// --------------------------------------------------------------------------
// Copyright(c) 2012 UnicornSolutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Common.Logging
// Coding Standard   : US Coding Standards
// Author            : AAB
// Created Timestamp : 05/01/2012 
// --------------------------------------------------------------------------
using System.Collections.Generic;

namespace US.Common.Logging
{
    public class USLogConfiguration
    {
        private string _usLogRootFolder = string.Empty;
        public string UsLogRootFolder
        {
            get { return _usLogRootFolder; }
            set { _usLogRootFolder = value; }
        }
        private string _usLogDBConnection = string.Empty;
        public string UsLogDBConnection
        {
            get { return _usLogDBConnection; }
            set { _usLogDBConnection = value; }
        }   
        private int _enableEventLog = 0;
        public int EnableEventLog
        {
            get { return _enableEventLog; }
            set { _enableEventLog = value; }
        }

        private int _enableErrorLog = 1;
        public int EnableErrorLog
        {
            get { return _enableErrorLog; }
            set { _enableErrorLog = value; }
        }

        private int _enableBusinessScenario = 0;
        public int EnableBusinessScenario
        {
            get { return _enableBusinessScenario; }
            set { _enableBusinessScenario = value; }
        }

        private List<USLogConfigurationItem> _cofigurationItems = new List<USLogConfigurationItem>();
        public List<USLogConfigurationItem> CofigurationItems
        {
            get { return _cofigurationItems; }
            set { _cofigurationItems = value; }
        }
    }
}
