﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Logging
{
    public enum SeverityFilter
    {
        Critical,
        Error,
        Warning,
        Information,
        Verbose
    }
}
