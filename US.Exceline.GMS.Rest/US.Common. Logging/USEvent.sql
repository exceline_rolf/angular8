
/* --------------------------------------------------------------------------
-- Copyright(c) <2012> Unicorn Solutions.
-- Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
-- All rights reserved.
-- Solution Name     : USPayment
-- Project Name      : US.Common.Logging
-- Coding Standard   : US Coding Standards
-- Author            : AAB
-- Created Timestamp : 21/07/2012 HH:MM  PM
-- --------------------------------------------------------------------------*/

-- Create event log table
-- Create sp to add eventlog data
-- Create sp to delet old data



GO

/****** Object:  Schema [USRPT]    Script Date: 10/22/2012 10:46:28 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'US')
DROP SCHEMA [US]
GO


GO

/****** Object:  Schema [USRPT]    Script Date: 10/22/2012 10:46:28 ******/
CREATE SCHEMA [US] AUTHORIZATION [db_accessadmin]
GO




CREATE TABLE [US].USEventLog 
(
  Id INT PRIMARY KEY  identity(1,1)
, Name					VARCHAR(1000) 
, ApplicationName		VARCHAR(100) 
, Category				VARCHAR(100)  NULL
, RegDate				DATETIME      NULL
, Description1			VARCHAR(2000) NULL
, Description2			VARCHAR(MAX) NULL
, Description3			VARCHAR(MAX) NULL
, UserName				VARCHAR(50)   NULL
)


GO




GO

/****** Object:  StoredProcedure [US].[AddErrorLog]    Script Date: 10/22/2012 10:52:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[US].[AddErrorLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [US].[AddErrorLog]
GO


GO

/****** Object:  StoredProcedure [US].[AddErrorLog]    Script Date: 10/22/2012 10:52:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE proc [US].[AddErrorLog]
 @Name				VARCHAR(1000)
,@ApplicationName	VARCHAR(100)
,@Category			VARCHAR(100)  =NULL
,@Code		        VARCHAR(100)  =NULL
,@Description1		VARCHAR(2000) =NULL
,@Description2		VARCHAR(MAX) =NULL
,@Description3		VARCHAR(MAX) =NULL
,@UserName			VARCHAR(50)   =NULL
AS
BEGIN

INSERT INTO [US].[USErrorLog]
           ([Name]
           ,[ApplicationName]
           ,[Category]
           ,[Code]
           ,[RegDate]
           ,[Description1]
           ,[Description2]
           ,[Description3]
           ,[UserName])
     VALUES
           (@Name
           ,@ApplicationName
           ,@Category
           ,@Code	
           ,GETDATE()
           ,@Description1
           ,@Description1
           ,@Description1
           ,@UserName)

END


GO



GO

/****** Object:  StoredProcedure [US].[AddEventLog]    Script Date: 10/22/2012 10:52:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[US].[AddEventLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [US].[AddEventLog]
GO


GO

/****** Object:  StoredProcedure [US].[AddEventLog]    Script Date: 10/22/2012 10:52:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE proc [US].[AddEventLog]
 @Name				VARCHAR(1000)
,@ApplicationName	VARCHAR(100)
,@Category			VARCHAR(100)  =NULL
,@Description1		VARCHAR(2000) =NULL
,@Description2		VARCHAR(2000) =NULL
,@Description3		VARCHAR(2000) =NULL
,@UserName			VARCHAR(50)   =NULL
AS
BEGIN

INSERT INTO [US].[USEventLog]
           ([Name]
           ,[ApplicationName]
           ,[Category]
           ,[RegDate]
           ,[Description1]
           ,[Description2]
           ,[Description3]
           ,[UserName])
     VALUES
           (@Name
           ,@ApplicationName
           ,@Category
           ,GETDATE()
           ,@Description1
           ,@Description1
           ,@Description1
           ,@UserName)

END

GO


GO

/****** Object:  StoredProcedure [US].[DeleteOldErrorData]    Script Date: 10/22/2012 10:52:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[US].[DeleteOldErrorData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [US].[DeleteOldErrorData]
GO


GO

/****** Object:  StoredProcedure [US].[DeleteOldErrorData]    Script Date: 10/22/2012 10:52:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [US].[DeleteOldErrorData]
AS
BEGIN


-- Old recods will be deleted
-- Valid no of days can be set in here it set to as 30. 
-- That means only last 30 days data will remain in table
-- This SP will executed by using sqlServerAgent job
-- Recomended to execute this SP in everyday at midnight. Then any given day it has only one month data
DECLARE @DeleteFromDate datetime

SET @DeleteFromDate = GETDATE()-30

DELETE FROM [US].USErrorLog WHERE RegDate < @DeleteFromDate



END


GO


GO

/****** Object:  StoredProcedure [US].[DeleteOldEventData]    Script Date: 10/22/2012 10:52:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[US].[DeleteOldEventData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [US].[DeleteOldEventData]
GO


GO

/****** Object:  StoredProcedure [US].[DeleteOldEventData]    Script Date: 10/22/2012 10:52:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create proc [US].[DeleteOldEventData]
AS
BEGIN


-- Old recods will be deleted
-- Valid no of days can be set in here it set to as 30. 
-- That means only last 30 days data will remain in table
-- This SP will executed by using sqlServerAgent job
-- Recomended to execute this SP in everyday at midnight. Then any given day it has only one month data
DECLARE @DeleteFromDate datetime

SET @DeleteFromDate = GETDATE()-30

DELETE FROM [US].USEventLog WHERE RegDate < @DeleteFromDate



END

GO



