﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateEntityScheduleItemsAction : USDBActionBase<int>
    {
        private int _scheduleItemId=-1;
        private ObservableCollection<ScheduleItemDC> _sheduleItemList;
        private bool _isFixed = false;       
        private ScheduleItemDC _ScheduleItem;
        private int _entityScheduleId;

        public UpdateEntityScheduleItemsAction(ObservableCollection<ScheduleItemDC> scheduleItemList, int entityId, string type, int entityScheduleId)
        {
            _sheduleItemList = scheduleItemList;      
            _entityScheduleId = entityScheduleId;
        }



        protected override int Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageClassesUpdateEntityScheduleItem";
            DbTransaction transaction = null;
            try
            {

                foreach (ScheduleItemDC scheduleitem in _sheduleItemList)
                {
                    _ScheduleItem = scheduleitem;
                   // foreach (ScheduleDC sdc in _scheduleList)
                   // {
                        //if (sdc.EntityId == _entityId && sdc.EntityRoleType == _type)
                        //{
                            _ScheduleItem.ScheduleId = _entityScheduleId;
                        //}
                        if (_ScheduleItem.Occurrence.ToString() == "FIX")
                            _isFixed = true;

                        transaction = connection.BeginTransaction();
                        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                        cmd.Transaction = transaction;
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, _ScheduleItem.ScheduleId));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Occurrence", DbType.String, _ScheduleItem.Occurrence.ToString()));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Day", DbType.String, _ScheduleItem.Day));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Week", DbType.Int32, _ScheduleItem.Week));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Month", DbType.String, _ScheduleItem.Month));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Year", DbType.Int32, _ScheduleItem.Year));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.DateTime, _ScheduleItem.StartTime));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.DateTime, _ScheduleItem.EndTime));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedDateTime", DbType.DateTime, DateTime.Now));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _ScheduleItem.CreatedUser));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser", DbType.String, _ScheduleItem.LastModifiedUser));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, _ScheduleItem.ActiveStatus));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsFixed", DbType.Boolean, _isFixed));
                        object obj = cmd.ExecuteScalar();
                        _scheduleItemId = Convert.ToInt32(obj);
                    //}
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _scheduleItemId;
        }

        public int RunOnTransaction(DbTransaction Transaction)
        {
            string StoredProcedureName = "USExceGMSManageClassesUpdateEntityScheduleItem";         
            try
            {

                foreach (ScheduleItemDC scheduleitem in _sheduleItemList)
                {
                    _ScheduleItem = scheduleitem;                    
                    _ScheduleItem.ScheduleId = _entityScheduleId;
                     
                        if (_ScheduleItem.Occurrence.ToString() == "FIX")
                            _isFixed = true;
                      
                        DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                        cmd.Connection = Transaction.Connection;
                        cmd.Transaction = Transaction;
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, _ScheduleItem.ScheduleId));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Occurrence", DbType.String, _ScheduleItem.Occurrence.ToString()));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Day", DbType.String, _ScheduleItem.Day));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Week", DbType.Int32, _ScheduleItem.Week));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Month", DbType.String, _ScheduleItem.Month));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Year", DbType.Int32, _ScheduleItem.Year));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.DateTime, _ScheduleItem.StartTime));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.DateTime, _ScheduleItem.EndTime));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _ScheduleItem.StartDate));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _ScheduleItem.EndDate));                        
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedDateTime", DbType.DateTime, DateTime.Now));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _ScheduleItem.CreatedUser));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser", DbType.String, _ScheduleItem.LastModifiedUser));                       
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsFixed", DbType.Boolean, _isFixed));
                        object obj = cmd.ExecuteScalar();
                        _scheduleItemId = Convert.ToInt32(obj);
                        if (_scheduleItemId == 0)
                            _scheduleItemId = -1;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _scheduleItemId;
        }
    }
}
