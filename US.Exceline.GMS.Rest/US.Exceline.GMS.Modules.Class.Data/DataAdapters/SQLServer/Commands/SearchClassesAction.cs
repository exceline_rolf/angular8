﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class SearchClassesAction : USDBActionBase<List<ExcelineClassDC>>
    {
        private string _className = string.Empty;

        public SearchClassesAction(string className)
        {
            _className = className;
        }

        protected override List<ExcelineClassDC> Body(DbConnection connection)
        {
            List<ExcelineClassDC> excelineClassLst = new List<ExcelineClassDC>();
            string StoredProcedureName = "USExceGMSManageClassesGetClasses";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
               // cmd.Parameters.Add(DataAcessUtils.CreateParam(" "));
            }

            catch (Exception ex)
            {
                throw new NotImplementedException();
            }

            return excelineClassLst;
        }
    }
}
