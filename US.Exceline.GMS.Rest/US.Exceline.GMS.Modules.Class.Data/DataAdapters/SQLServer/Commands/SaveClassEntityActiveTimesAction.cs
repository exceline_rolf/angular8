﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class SaveClassEntityActiveTimesAction : USDBActionBase<int>
    {
        private DataTable _dtActiveTimes;
        private ScheduleItemDC _scheduleItem;
        public SaveClassEntityActiveTimesAction(ScheduleItemDC item)
        {
            _scheduleItem = item;
            _dtActiveTimes = GetActiveTimesDataTable(item);
        }

        private DataTable GetActiveTimesDataTable(ScheduleItemDC item)
        {
            _dtActiveTimes = new DataTable();
            _dtActiveTimes.Columns.Add(new DataColumn("ScheduleItemId", typeof(int)));
            _dtActiveTimes.Columns.Add(new DataColumn("StartDateTime", typeof(DateTime)));
            _dtActiveTimes.Columns.Add(new DataColumn("EndDateTime", typeof(DateTime)));

            if (item.ActiveTimes != null && item.ActiveTimes.Any())
            {
                foreach (EntityActiveTimeDC activeTime in item.ActiveTimes)
                {
                    DataRow dr = _dtActiveTimes.NewRow();
                    dr["ScheduleItemId"] = item.Id;
                    dr["StartDateTime"] = activeTime.StartDateTime;
                    dr["EndDateTime"] = activeTime.EndDateTime;
                    _dtActiveTimes.Rows.Add(dr);
                }
            }
            return _dtActiveTimes;
        }

        protected override int Body(DbConnection connection)
        {
            int scheduleItemId;
            const string storedProcedureName = "USExceGMSClassesGenerateEntityActiveTimes";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItem.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _scheduleItem.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _scheduleItem.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimesList", SqlDbType.Structured, _dtActiveTimes));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OutPutID", DbType.Int32, _scheduleItem.Id));
                object obj = cmd.ExecuteScalar();
                scheduleItemId = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return scheduleItemId;
        }
    }
}
