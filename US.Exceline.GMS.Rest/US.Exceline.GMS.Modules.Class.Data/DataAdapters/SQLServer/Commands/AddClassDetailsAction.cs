﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class AddClassDetailsAction : USDBActionBase<string>
    {
        ExcelineClassDC _exceineClass = new ExcelineClassDC();
        int _classScheduleId  = -1;
        private string _output = string.Empty;
        private string _gymCode = string.Empty;
        private int _branchId;
        private string _user = string.Empty;

        public AddClassDetailsAction(ExcelineClassDC excelineClass, int branchId, string gymCode,string user)
        {
            _exceineClass = excelineClass;
            _gymCode = gymCode;
            _branchId = branchId;
            _user = user;
        }

        protected override string Body(DbConnection connection)
        {
            try
            {
                DbTransaction dbtran = connection.BeginTransaction();
                SaveClassAction saveAction = new SaveClassAction(_exceineClass);
                _output = saveAction.RunOnTransation(dbtran);
                 string[] keywordparts = _output.Trim().Split(' ');
                 if (keywordparts.Length > 1)
                 {
                     _classScheduleId =Convert.ToInt32(keywordparts[1].Trim()) ;
                 }

                 if (_classScheduleId == -1)
                 {
                     dbtran.Rollback();
                     return "-1";
                 }
                 else
                 {
                     if (_exceineClass.Schedule.SheduleItemList != null && (_exceineClass.Schedule != null && _exceineClass.Schedule.SheduleItemList.Count > 0))
                     {
                         int schItemId = -1;
                         _exceineClass.Schedule.SheduleItemList.ToList().ForEach(x => x.ScheduleId = _classScheduleId);
                         foreach (ScheduleItemDC item in _exceineClass.Schedule.SheduleItemList)
                         {  
                            if (_exceineClass.IsCopyClass)
                            {
                                item.Id = -1;
                                item.StartDate = _exceineClass.Schedule.StartDate;
                                if (_exceineClass.Schedule.EndDate.HasValue)
                                {
                                    DateTime tempDate = _exceineClass.Schedule.EndDate ?? _exceineClass.Schedule.StartDate;
                                    item.EndDate = tempDate;
                                }
                            }
                            item.Id = -1;
                             SaveScheduleItemAction saveScheduleItemAction = new SaveScheduleItemAction(item, _branchId, _user, _exceineClass.IsCopyClass);
                             schItemId = saveScheduleItemAction.Execute(US_DataAccess.EnumDatabase.Exceline, _gymCode);
                             if (schItemId == -1)
                             {
                                 dbtran.Rollback();
                                 return "-1";
                             }
                         }
                     }
                 }
                dbtran.Commit();
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _output;
        }
    }
}
