﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateClassActiveTimeWithEntityActiveTimeAction : USDBActionBase<bool>
    {
        private readonly int _activeTimeId = -1;
        private readonly DateTime _startDateTime;
        private readonly DateTime _endDateTime;
        private readonly int _maxNoOfBookings;
        private readonly int _noOfParticipants;
        private DataTable _dataTableEntity;

        public UpdateClassActiveTimeWithEntityActiveTimeAction(int activeTimeId, DateTime startDateTime, DateTime endDateTime, int maxNumberOfBookings, int numberOfParticipants, List<int> addedInsList, List<int> addedResList, List<int> deletedInsList, List<int> deletedResList)
        {
            _activeTimeId = activeTimeId;
            _startDateTime = startDateTime;
            _endDateTime = endDateTime;
            _maxNoOfBookings = maxNumberOfBookings;
            _noOfParticipants = numberOfParticipants;
            _dataTableEntity = GetEntityList(addedInsList, addedResList, deletedInsList, deletedResList);

        }

        private DataTable GetEntityList(IEnumerable<int> addInsList, IEnumerable<int> addResList, IEnumerable<int> deletedInsList, IEnumerable<int> deletedResList)
        {
            _dataTableEntity = new DataTable();
            _dataTableEntity.Columns.Add(new DataColumn("EntityId", typeof(int)));
            _dataTableEntity.Columns.Add(new DataColumn("EntityRoleType", typeof(string)));
            _dataTableEntity.Columns.Add(new DataColumn("Status", typeof(string)));

            if (addInsList != null)
            {
                var enumerable = addInsList as int[] ?? addInsList.ToArray();
                if (enumerable.Any())
                {
                    foreach (int id in enumerable)
                    {
                        DataRow dataTableRow = _dataTableEntity.NewRow();
                        dataTableRow["EntityId"] = id;
                        dataTableRow["EntityRoleType"] = "INS";
                        dataTableRow["Status"] = "ADD";
                        _dataTableEntity.Rows.Add(dataTableRow);
                    }
                }
            }

            if (addResList != null)
            {
                var enumerable = addResList as int[] ?? addResList.ToArray();
                if (enumerable.Any())
                {
                    foreach (int id in enumerable)
                    {
                        DataRow dataTableRow = _dataTableEntity.NewRow();
                        dataTableRow["EntityId"] = id;
                        dataTableRow["EntityRoleType"] = "RES";
                        dataTableRow["Status"] = "ADD";
                        _dataTableEntity.Rows.Add(dataTableRow);
                    }
                }
            }

            if (deletedInsList != null)
            {
                var enumerable = deletedInsList as int[] ?? deletedInsList.ToArray();
                if (enumerable.Any())
                {
                    foreach (int id in enumerable)
                    {
                        DataRow dataTableRow = _dataTableEntity.NewRow();
                        dataTableRow["EntityId"] = id;
                        dataTableRow["EntityRoleType"] = "INS";
                        dataTableRow["Status"] = "REMOVE";
                        _dataTableEntity.Rows.Add(dataTableRow);
                    }
                }
            }

            if (deletedResList != null)
            {
                var enumerable = deletedResList as int[] ?? deletedResList.ToArray();
                if (enumerable.Any())
                {
                    foreach (int id in enumerable)
                    {
                        DataRow dataTableRow = _dataTableEntity.NewRow();
                        dataTableRow["EntityId"] = id;
                        dataTableRow["EntityRoleType"] = "RES";
                        dataTableRow["Status"] = "REMOVE";
                        _dataTableEntity.Rows.Add(dataTableRow);
                    }
                }
            }
            return _dataTableEntity;
        }

        protected override bool Body(DbConnection connection)
        {
            return false;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            var isUpdated = false;
            const string storedProcedureName = "USEXCEGMSUpdateClassActiveTimeWithEntityActiveTimes";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure,storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassActiveTimeId",DbType.Int32, _activeTimeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.DateTime, _startDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.DateTime, _endDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MaxNoOfBookings", DbType.Int32, _maxNoOfBookings));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NoOfParticipants", DbType.Int32, _noOfParticipants));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExceClassEntityList", SqlDbType.Structured, _dataTableEntity));

                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;

                cmd.ExecuteNonQuery();
                isUpdated = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return isUpdated;
        }
    }
}
