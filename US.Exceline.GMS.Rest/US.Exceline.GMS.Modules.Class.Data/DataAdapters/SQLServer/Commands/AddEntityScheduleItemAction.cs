﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class AddEntityScheduleItemAction : USDBActionBase<int>
    {
        private int _scheduleId;
        private ObservableCollection<ScheduleItemDC> _sheduleItemList;
        private DataTable _dataTable;

        public AddEntityScheduleItemAction(int scheduleId, ObservableCollection<ScheduleItemDC> scheduleItemList)
        {
            _scheduleId = scheduleId;
            _sheduleItemList = scheduleItemList;
            _dataTable = GetScheduleItemList(_sheduleItemList);
        }

        private DataTable GetScheduleItemList(ObservableCollection<ScheduleItemDC> scheduleItemList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ScheduleId", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("Occurrence", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("Day", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("Week", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("Month", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("Year", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("StartTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("EndTime", typeof(DateTime)));            
            _dataTable.Columns.Add(new DataColumn("StartDate", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("EndDate", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("CreatedDateTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("LastModifiedDateTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("CreatedUser", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("LastModifiedUser", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("ActiveStatus", typeof(bool)));
            _dataTable.Columns.Add(new DataColumn("IsFixed", typeof(bool)));

            foreach (ScheduleItemDC scheduleitem in scheduleItemList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["ScheduleId"] = _scheduleId;
                _dataTableRow["Occurrence"] = scheduleitem.Occurrence;
                _dataTableRow["Day"] = scheduleitem.Day;
                _dataTableRow["Week"] = scheduleitem.Week;
                _dataTableRow["Month"] = scheduleitem.Month;
                _dataTableRow["Year"] = scheduleitem.Year;
                _dataTableRow["StartTime"] = scheduleitem.StartTime;
                _dataTableRow["EndTime"] = scheduleitem.EndTime;
                _dataTableRow["StartDate"] = scheduleitem.StartDate;
                _dataTableRow["EndDate"] = scheduleitem.EndDate;
                _dataTableRow["CreatedDateTime"] = DateTime.Now;//scheduleitem.CreatedDate;
                _dataTableRow["LastModifiedDateTime"] = DateTime.Now;//scheduleitem.LastModifiedDate;
                _dataTableRow["CreatedUser"] = scheduleitem.CreatedUser;
                _dataTableRow["LastModifiedUser"] = scheduleitem.LastModifiedUser;
                _dataTableRow["ActiveStatus"] = true;
                _dataTableRow["IsFixed"] = false; // should be changed.
                _dataTable.Rows.Add(_dataTableRow);
            }

            return _dataTable;
        }

        protected override int Body(DbConnection connection)
        {
            int _scheduleItemId = 0;
            string StoredProcedureName = "USExceGMSManageClassesAddEntityScheduleItem";
            DbTransaction transaction = null;
            try
            {
                transaction = connection.BeginTransaction();
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleItemList", SqlDbType.Structured, _dataTable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OutPutID", DbType.Int32, 0));
                object obj = cmd.ExecuteScalar();
                _scheduleItemId = Convert.ToInt32(obj);
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _scheduleItemId;
        }

        public int RunOnTransaction(DbTransaction Transaction)
        {
            int _scheduleItemId = 0;
            string StoredProcedureName = "USExceGMSManageClassesAddEntityScheduleItem";           
            try
            {
               
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = Transaction.Connection;
                cmd.Transaction = Transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleItemList", SqlDbType.Structured, _dataTable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OutPutID", DbType.Int32, 0));
                object obj = cmd.ExecuteScalar();
                _scheduleItemId = Convert.ToInt32(obj);
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _scheduleItemId;
        }
    }
}
