﻿using System;
using System.Collections.Generic;

namespace US.Exceline.GMS.Modules.Class.Data.SystemObjects
{
    public class ScheduleData
    {
        private int _branchId = -1;
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private int _id = -1;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private String _name = String.Empty;
        public String Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private DateTime? _startDate = null;
        public DateTime? StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime? _endDate = null;
        public DateTime? EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private List<ClassData> _classList = new List<ClassData>();
        public List<ClassData> ClassList
        {
            get { return _classList; }
            set { _classList = value; }
        }

    }
}
