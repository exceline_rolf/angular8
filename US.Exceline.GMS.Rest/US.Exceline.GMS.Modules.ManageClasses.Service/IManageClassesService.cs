﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageClasses.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IManageClassesService
    {
        #region Class
      
       
        [OperationContract]
        int SaveClass(ExcelineClassDC excelineClass, string user);
        [OperationContract]
        bool UpdateClass(ExcelineClassDC excelineClass, string user);
        [OperationContract]
        bool DeleteClass(int classId, string user);       
        [OperationContract]
        List<ExcelineClassDC> GetClasses(string className, int branchId, string user);
        [OperationContract]
        List<TrainerDC> GetTrainersForClass(int branchId, string searchText, string user);
        [OperationContract]
        List<InstructorDC> GetInstructorsForClass(int branchId, string searchText, string user);
        [OperationContract]
        string GetMembersForClass(int branchId, string searchText, string user,int hit);
        [OperationContract]
        List<ResourceDC> GetResourcesForClass(int branchId, string searchText, string user);
        [OperationContract]
        int GetNextClassId(string user);
        [OperationContract]
        List<ExcelineMemberDC> GetMembersForClassUpdate(int classId, string user);
        [OperationContract]
        List<TrainerDC> GetTrainersForClassUpdate(int classId, string user);
        [OperationContract]
        List<InstructorDC> GetInstructorsForClassUpdate(int classId, string user);
        [OperationContract]
        List<ResourceDC> GetResourcesForClassUpdate(int classId, string user);
        [OperationContract]
        List<OrdinaryMemberDC> GetMembersForClassBooking(int classId, int branchId, string searchText, string user);
        [OperationContract]
        List<ClassBookingActiveTimeDC> GetClassActiveTimesForBooking(int branchId, DateTime startTime, DateTime endTime, int entNO, string user);
        [OperationContract]
        List<MemberBookingDetailsDC> GetMemberBookingDetails(int classId, int branchId, string user);
        [OperationContract]
        int SaveMemberBookingDetails(int classId, OrdinaryMemberDC ordinaryMemberDC, int memberId, List<ClassBookingDC> bookingList, decimal totalBookingAmount, decimal totalAvailableAmount, string user, int branchId, string scheduleCategoryType);
        [OperationContract]
        bool ApplyClassScheduleForNextYear(int classId, DateTime startdate, DateTime enddate, string name, int branchId, string createdUser, List<ScheduleItemDC> scheduleItemList);
        [OperationContract]
        bool GetVerificationOfAppliedClassScheduleAction(int branchId, string user, int classScheduleId);
        [OperationContract]
        List<ArticleDC> GetArticlesForClass(int branchId, string user);
        [OperationContract]
        int GetExcelineClassIdByName(int branchId, string user, string className);
        #endregion

        #region Member
        [OperationContract]
        List<MemberContractDC> GetMemberContracts(int memberID, string user, int branchId);
        #endregion

        #region Category
        [OperationContract]
        List<CategoryDC> GetCategories(string type, string user, int branchId);
        [OperationContract]
        ScheduleDC GetClassSchedule(int classId, int branchId, string user);
        [OperationContract]
        List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId);
        [OperationContract]
        int SaveCategory(CategoryDC category, string user, int branchId);
        [OperationContract]
        List<ExcelineClassActiveTimeDC> GetClassHistory(int classId, List<ScheduleItemDC> scheduleItemList, string user);
        #endregion

        #region Schedule
        [OperationContract]
        bool UpdateActiveTime(EntityActiveTimeDC activetime, string user, bool isDeleted);
        [OperationContract]
        List<EntityActiveTimeDC> GetEntityActiveTimes(int branchid, DateTime startDate, DateTime endDate, List<int> entityList, string entityRoleType, string user);
        #endregion

        #region Article
        [OperationContract]
        ArticleDC GetArticle(int articleId, int branchId, string user);
        #endregion

        #region Booking
        [OperationContract]
        int SaveClassBookingPayment(int memberId, int classId, BookingArticleDC bookingArticle, List<ClassBookingDC> PaidBookings, List<BookingPaymentDC> bookingPayments, decimal totalAmount, decimal paidAmount, decimal defaultPrice, string user, int branchId);
        #endregion

        #region Utility
        [OperationContract]
        List<CalendarHoliday> GetHolidays(DateTime calendarDate, string user, int branchId);
        #endregion
    }
}
