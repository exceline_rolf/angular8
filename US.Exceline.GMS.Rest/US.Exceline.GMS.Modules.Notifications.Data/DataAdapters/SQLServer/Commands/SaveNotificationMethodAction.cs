﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.Exceline.GMS.Modules.Notifications.Data
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 19/6/2012 4:27:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Notification;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.SQLServer.Commands
{
    public class SaveNotificationMethodAction : USDBActionBase<int>
    {
        private NotificationMethodDC _notificationMethod = new NotificationMethodDC();
         
         

        public SaveNotificationMethodAction(NotificationMethodDC notificationMethod, string createdUser, int branchId)
        {
            _notificationMethod = notificationMethod;            

        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            string StoredProcedureName = "USExceGMSNotificationsSaveNotificationMethod";

            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _notificationMethod.Id));
                if (_notificationMethod.DueDate != DateTime.MinValue)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@dueDate", DbType.DateTime, _notificationMethod.DueDate));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@occurance", DbType.String, _notificationMethod.Occurance));
                //Method ----------->Method, Email, PopUp, SMS
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@method", DbType.String, _notificationMethod.Method.ToString().ToUpper()));
                //Method Type----------->Example: If Method is "SMS" , there are SMS types as Member SMS, Service SMS & Reminder SMS
                if (_notificationMethod.Method == NotifyMethodType.SMS)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@methodType", DbType.String, _notificationMethod.SMSType.ToString().ToUpper()));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notification", DbType.String, _notificationMethod.Notification));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notificationID", DbType.String, _notificationMethod.NotificationID));
                if (_notificationMethod.DueDate != DateTime.MinValue)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@nextDueDate", DbType.DateTime, _notificationMethod.NextDueDate));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contactEntity", DbType.String, _notificationMethod.ContactEntity));
                if (_notificationMethod.USCTemplateType != USCTemplateType.NONE)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@USCTemplate", DbType.String, _notificationMethod.USCTemplateType));
                }
                //Category Type = "NOTIFYMETHOD"
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryType", DbType.String, CategoryTypes.NOTIFYMETHOD.ToString().ToUpper()));

                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@OutID";
                outputPara.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara);
                cmd.ExecuteNonQuery();

                result = Convert.ToInt32(outputPara.Value);



            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }
    }
}
