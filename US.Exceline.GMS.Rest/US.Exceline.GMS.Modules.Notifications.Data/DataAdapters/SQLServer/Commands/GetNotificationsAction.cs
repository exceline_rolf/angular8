﻿
// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Notifications.Data
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 28/6/2012 10:53:39
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Notification;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.SQLServer.Commands
{
    public class GetNotificationsAction : USDBActionBase<List<NotificationMethodDC>>
    {
        private int _branchId = -1;

        public GetNotificationsAction(int branchId)
        {

            _branchId = branchId;

        }

        protected override List<NotificationMethodDC> Body(DbConnection connection)
        {
            List<NotificationMethodDC> _notificationList = new List<NotificationMethodDC>();
            string StoredProcedureName = "USExceGMSNotificationsGetNotifications";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromCreatedDate", DbType.DateTime, _branchId));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@toCreatedDate", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    NotificationMethodDC _notificationMethod = new NotificationMethodDC();

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["ID"]).Trim()))
                    {
                        _notificationMethod.Id = Convert.ToInt32(reader["ID"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["MethodDueDate"]).Trim()))
                    {
                        _notificationMethod.DueDate = Convert.ToDateTime(reader["MethodDueDate"]);

                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Occurance"]).Trim()))
                    {
                        try
                        {
                            string occuranceType = reader["Occurance"].ToString();
                            _notificationMethod.Occurance = (OccuranceType)Enum.Parse(typeof(OccuranceType), occuranceType);
                        }
                        catch
                        {
                            _notificationMethod.Occurance = OccuranceType.NONE;
                        }
                    }
                    else
                    {
                        _notificationMethod.Occurance = OccuranceType.NONE;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Method"]).Trim()))
                    {
                        try
                        {
                            string methodType = reader["Method"].ToString();
                            _notificationMethod.Method = (NotifyMethodType)Enum.Parse(typeof(NotifyMethodType), methodType);
                        }
                        catch
                        {
                            _notificationMethod.Method = NotifyMethodType.NONE;
                        }
                    }
                    else
                    {
                        _notificationMethod.Method = NotifyMethodType.NONE;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Notification"]).Trim()))
                    {
                        _notificationMethod.Notification = Convert.ToString(reader["Notification"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["NotificationID"]).Trim()))
                    {
                        _notificationMethod.NotificationID = Convert.ToInt32(reader["NotificationID"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["NextDueDate"]).Trim()))
                    {
                        _notificationMethod.NextDueDate = Convert.ToDateTime(reader["NextDueDate"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["SourceID"]).Trim()))
                    {
                        _notificationMethod.SourceID = Convert.ToInt32(reader["SourceID"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Source"]).Trim()))
                    {
                        _notificationMethod.Source = Convert.ToString(reader["Source"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["ReceiverEntityID"]).Trim()))
                    {
                        _notificationMethod.ReceiverEntityID = Convert.ToInt32(reader["ReceiverEntityID"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["CreatedDate"]).Trim()))
                    {
                        _notificationMethod.RegisterdDate = Convert.ToDateTime(reader["CreatedDate"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["IsNotified"]).Trim()))
                    {
                        try
                        {
                            _notificationMethod.IsNew = !(Convert.ToBoolean(reader["IsNotified"]));
                        }
                        catch
                        {
                        }
                    }
                    //if (!string.IsNullOrEmpty(Convert.ToString(reader["CreatedUser"]).Trim()))
                    //{
                    //    _notificationMethod.ActivityId = Convert.ToInt32(reader["CreatedUser"]);
                    //}
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Role"]).Trim()))
                    {
                        _notificationMethod.Role = Convert.ToString(reader["Role"]);
                    }

                    _notificationList.Add(_notificationMethod);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _notificationList;
        }
    }
}
