﻿
// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Notifications.Data
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 13/8/2012 10:53:39
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Notification;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.SQLServer.Commands
{
    public class SaveNotificationTemplateAction : USDBActionBase<int>
    {
        private NotificationTemplateDC _notificationTemplate = new NotificationTemplateDC();
        private string _createdUser = string.Empty;
        private int _branchId = -1;

        public SaveNotificationTemplateAction(NotificationTemplateDC notificationTemplate, string createdUser, int branchId)
        {
            _notificationTemplate = notificationTemplate;
            _createdUser = createdUser;
            _branchId = branchId;
        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            string StoredProcedureName = "USExceGMSNotificationsSaveNotificationTemplate";

            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _notificationTemplate.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notifyMethod", DbType.String, _notificationTemplate.NotifyMethod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryID", DbType.String, _notificationTemplate.TypeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Text", DbType.String, _notificationTemplate.Text));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedDate", DbType.String, _notificationTemplate.CreatedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _createdUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@OutID";
                outputPara.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara);
                cmd.ExecuteNonQuery();

                result = Convert.ToInt32(outputPara.Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
