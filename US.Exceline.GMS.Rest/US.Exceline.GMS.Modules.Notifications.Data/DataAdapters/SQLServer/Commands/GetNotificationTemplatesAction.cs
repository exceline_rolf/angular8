﻿
// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Notifications.Data
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 14/8/2012 10:53:39
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Notification;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.SQLServer.Commands
{
    public class GetNotificationTemplatesAction : USDBActionBase<List<NotificationTemplateDC>>
    {
        private int _branchId = -1;

        public GetNotificationTemplatesAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<NotificationTemplateDC> Body(DbConnection connection)
        {
            List<NotificationTemplateDC> _notificationTemplateList = new List<NotificationTemplateDC>();
            string StoredProcedureName = "USExceGMSNotificationsGetNotificationTemplates";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    NotificationTemplateDC _notificationTemplate = new NotificationTemplateDC();

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["ID"]).Trim()))
                    {
                        _notificationTemplate.Id = Convert.ToInt32(reader["ID"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["TmpltText"]).Trim()))
                    {
                        _notificationTemplate.Text = Convert.ToString(reader["TmpltText"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["CategoryID"]).Trim()))
                    {
                        _notificationTemplate.TypeId = Convert.ToInt32(reader["CategoryID"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["CategoryName"]).Trim()))
                    {
                        _notificationTemplate.CategoryName = Convert.ToString(reader["CategoryName"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["NotifyMethod"]).Trim()))
                    {
                        try
                        {
                            string methodType = reader["NotifyMethod"].ToString();
                            _notificationTemplate.NotifyMethod = (NotifyMethodType)Enum.Parse(typeof(NotifyMethodType), methodType);
                        }
                        catch
                        {
                            _notificationTemplate.NotifyMethod = NotifyMethodType.NONE;
                        }
                    }
                    _notificationTemplateList.Add(_notificationTemplate);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _notificationTemplateList;
        }
    }
}
