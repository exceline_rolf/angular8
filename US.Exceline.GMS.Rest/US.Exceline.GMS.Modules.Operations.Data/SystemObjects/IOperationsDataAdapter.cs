﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.Common.Web.UI.Core.SystemObjects;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Modules.Operations.Data.SystemObjects
{
    interface IOperationsDataAdapter
    {
        List<ExcACCCommand> GetCommands(int branchId, string gymCode);
        ExceACCSettings GetSettings(int branchId, string gymCode);
        List<ExceACCAccessProfileTime> GetAccessTimes(int branchId, string gymCode, string dateFromat);
        List<GymACCOpenTime> GetOpenTimes(int branchId, string gymCode);
        List<ExceACCGymClosedTime> GetClosedTimes(int branchId, string gymCode);
        List<ExceACCMember> GetMembers(int branchId, string gymCode);
        string GetImage(int branchId, string gymCode, string customerNo);
        List<person> GetARXMembers(int branchId, string gymCode, bool isAllMember);
        int GetSystemId(string gymCode);
        int AddVisitList(string gymCode, List<EntityVisitDC> entityVisitLst);
        int RegisterControl(ExcACCAccessControl accessControl, string gymCode);
        int PingServer(int terminalId, string gymCode);
        int AddEventLogs(ExcAccessEvent accessEvent, string gymCode);
        ExceACCTerminal GetTerminalDetails(int terminalID, string gymCode);
        int GetAvailableVisitByContractId(int contractId, string gymCode);

    }
}
