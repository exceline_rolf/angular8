﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class AddAccessEventAction : USDBActionBase<int>
    {
        private ExcAccessEvent _accessEvent;
        public AddAccessEventAction(ExcAccessEvent accessEvent)
        {
            _accessEvent = accessEvent;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSACCRegisterAccessEvent";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", System.Data.DbType.Int32, _accessEvent.MemberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _accessEvent.BranchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@terminalId", System.Data.DbType.Int32, _accessEvent.TerminalId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@message", System.Data.DbType.String, _accessEvent.Messages));
                command.Parameters.Add(DataAcessUtils.CreateParam("@cardNo", System.Data.DbType.String, _accessEvent.CardNo));
                command.ExecuteNonQuery();
                return 1;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
