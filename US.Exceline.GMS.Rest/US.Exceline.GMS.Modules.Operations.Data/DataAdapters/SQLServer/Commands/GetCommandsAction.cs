﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class GetCommandsAction : USDBActionBase<List<ExcACCCommand>>
    {
        int _branchId = -1;
        public GetCommandsAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ExcACCCommand> Body(System.Data.Common.DbConnection connection)
        {
            List<ExcACCCommand> commands = new List<ExcACCCommand>();
            string spName = "ExceGMSACCGetCommands";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExcACCCommand acccommand = new ExcACCCommand();
                    acccommand.Cmd = Convert.ToString(reader["Command"]);
                    acccommand.EntityName = Convert.ToString(reader["EntityName"]);
                    if(reader["LastUpdatedTime"] != DBNull.Value)
                        acccommand.LastUpdateTime = Convert.ToDateTime(reader["LastUpdatedTime"]).ToString("yyyy-MM-dd HH:mm:ss"); ;
                    commands.Add(acccommand);
                }
                return commands;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
