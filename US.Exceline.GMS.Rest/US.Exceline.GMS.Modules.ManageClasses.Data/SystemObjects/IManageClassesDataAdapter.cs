﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;


namespace US.Exceline.GMS.Modules.ManageClasses.Data.SystemObjects
{
    public interface IManageClassesDataAdapter
    {
        List<ExcelineClassDC> SearchClass(string category, string searchText, string gymCode);
        List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, string gymCode);
        List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, DateTime endDate, string gymCode);
        int SaveClass(ExcelineClassDC excelineClass, string gymCode);
        bool UpdateClass(ExcelineClassDC excelineClass, string gymCode);
        bool DeleteClass(int classId, string gymCode);
        int GetNextClassId(string gymCode);
        List<ExcelineClassDC> GetClasses(string className, int branchId, string gymCode);
        List<ExcelineClassActiveTimeDC> GetClassActiveTimes(int branchId, DateTime startDate, DateTime endDate, int entNo, string gymCode);
        List<ExcelineMemberDC> GetMembersForClass(int classId, string gymCode);
        List<TrainerDC> GetTrainersForClass(int classId, string gymCode);
        List<InstructorDC> GetInstructorsForClass(int classId, string gymCode);
        List<ResourceDC> GetResourcesForClass(int classId, string gymCode);
        ScheduleDC GetClassSchedule(int classId, string gymCode);
        List<ExcelineClassActiveTimeDC> GetClassHistory(int classId, List<ScheduleItemDC> scheduleItemList, string gymCode);
        List<OrdinaryMemberDC> GetMembersForClassBooking(int classId, int branchId, string searchText, string gymCode);
        List<ClassBookingActiveTimeDC> GetClassActiveTimesForBooking(int branchId, DateTime startDate, DateTime endDate, int entNo, string gymCode);
        List<MemberBookingDetailsDC> GetMemberBookingDetails(int classId, int branchId, string gymCode);
        int SaveMemberBookingDetails(int classId, OrdinaryMemberDC ordinaryMemberDC, int memberId, List<ClassBookingDC> bookingList, decimal totalBookingAmount, decimal totalAvailableAmount, string scheduleCategoryType, string user, int branchId, string gymCode);
        bool ApplyClassScheduleForNextYear(int classId, DateTime startdate, DateTime enddate, string name, int branchId, string createdUser, List<ScheduleItemDC> scheduleItemList, string gymCode);
        bool GetVerificationOfAppliedClassScheduleAction(int classScheduleId, string gymCode);
        List<ArticleDC> GetArticlesForClass(string gymCode);
        int SaveClassBookingPayment(int memberId, int classId, BookingArticleDC bookingArticle, List<ClassBookingDC> PaidBookings, List<BookingPaymentDC> bookingPayments, decimal totalAmount, decimal paidAmount, decimal defaultPrice, string user, int branchId, string gymCode);
        int GetExcelineClassIdByName(string className, string gymCode);
    }
}
