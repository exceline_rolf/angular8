﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class GetTrainersForClassAction : USDBActionBase<List<TrainerDC>>
    {
        private int _classId = -1;

        public GetTrainersForClassAction(int classId)
        {
            _classId = classId;
        }

        protected override List<TrainerDC> Body(DbConnection connection)
        {
            List<TrainerDC> _trainerLst = new List<TrainerDC>();
            string StoredProcedureName = "USExceGMSManageClassesGetTrainersForClass";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                DbDataReader reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    TrainerDC classTrainer = new TrainerDC();
                    classTrainer.Id = Convert.ToInt32(reader["TrainerId"]);
                    classTrainer.FirstName = reader["FirstName"].ToString();
                    classTrainer.LastName = reader["LastName"].ToString();
                    _trainerLst.Add(classTrainer);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _trainerLst;
        }
    }
}
