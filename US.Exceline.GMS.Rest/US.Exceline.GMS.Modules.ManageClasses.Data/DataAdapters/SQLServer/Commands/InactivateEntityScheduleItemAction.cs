﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class InactivateEntityScheduleItemAction : US_DataAccess.USDBActionBase<bool>
    {
        private int _entityScheduleId;
        private bool _isActivated = false;

        public InactivateEntityScheduleItemAction(int entityScheduleId)
        {
            this._entityScheduleId = entityScheduleId;
        }

        protected override bool Body(DbConnection connection)
        {
            string _spName = "USExceGMSManageClassesInactivateEntityScheduleItem";   

            try
            {

                DbCommand command = CreateCommand(CommandType.StoredProcedure, _spName);
                // command.Connection = Transaction.Connection;
                // command.Transaction = Transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@EntityScheduleId", DbType.Int32, _entityScheduleId));
                command.ExecuteNonQuery();
                _isActivated = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isActivated;
        }

        public bool RunOnTransaction(DbTransaction Transaction)
        {
             string _spName = "USExceGMSManageClassesInactivateEntityScheduleItem";   

            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, _spName);
                command.Connection = Transaction.Connection;
                command.Transaction = Transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@EntityScheduleId", DbType.Int32, _entityScheduleId));
                command.ExecuteNonQuery();
                _isActivated = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isActivated;
        }
    }
}
