﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class GetExcelineClassIdAction : USDBActionBase<int>
    {
        private string _className = string.Empty;
        private int classId = -1;

        public GetExcelineClassIdAction(string classname)
        {
            this._className = classname;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string storedprocedure = "USExceGMSManageClassesGetClassIdByName";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedprocedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@className", System.Data.DbType.String, _className));
                object obj = cmd.ExecuteScalar();
                classId = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return classId;
        }
    }
}
