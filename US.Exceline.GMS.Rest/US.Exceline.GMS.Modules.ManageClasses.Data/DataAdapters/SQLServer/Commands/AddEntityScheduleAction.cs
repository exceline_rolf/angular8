﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class AddEntityScheduleAction : USDBActionBase<int>
    {
        private int _parentId;
        private int _entityId;
        private string _entityType;
        private string _className;
        private int _categoryId;
        private int _branchId;
   

        public AddEntityScheduleAction(int parentId, int entityId, string entityType, string className, int categoryId, int branchId)
        {
            _parentId = parentId;
            _entityId = entityId;
            _entityType = entityType;
            _className = className;
            _categoryId = categoryId;
            _branchId = branchId;
        
        }

        protected override int Body(DbConnection connection)
        {
            int _scheduleId = 0;
            string StoredProcedureName = "USExceGMSManageClassesAddEntitySchedule";
            DbTransaction transaction = null;
            try
            {
                transaction = connection.BeginTransaction();
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@parentId", DbType.Int32, _parentId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityType", DbType.String, _entityType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _className));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.String, _branchId));
                
                object obj = cmd.ExecuteScalar();
                _scheduleId = Convert.ToInt32(obj);

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _scheduleId;
        }

        public int RunOnTransaction(DbTransaction Transaction)
        {
            int _scheduleId = 0;
            string StoredProcedureName = "USExceGMSManageClassesAddEntitySchedule";
          
            try
            {
               
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = Transaction.Connection;
                cmd.Transaction = Transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@parentId", DbType.Int32, _parentId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityType", DbType.String, _entityType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _className));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.String, _branchId));

                object obj = cmd.ExecuteScalar();
                _scheduleId = Convert.ToInt32(obj);

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _scheduleId;
        }
    }
}
