﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class GetClassHistoryAction : USDBActionBase<List<ExcelineClassActiveTimeDC>>
    {
        private int _classId;
        private List<ScheduleItemDC> _classScheduleList;
        private int _scheduleId;

        public GetClassHistoryAction(int classId, List<ScheduleItemDC> classScheduleList)
        {
            _classId = classId;
            _classScheduleList = classScheduleList;
            foreach (ScheduleItemDC sdc in _classScheduleList)
            {
                _scheduleId = sdc.ScheduleId;
            }
        }

        protected override List<ExcelineClassActiveTimeDC> Body(DbConnection connection)
        {
            List<ExcelineClassActiveTimeDC> activeTimeList = new List<ExcelineClassActiveTimeDC>();
            string StoredProcedureName = "USExceGMSManageClassesGetClassHistory";

            try
            {

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classScheduleId", DbType.Int32, _scheduleId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExcelineClassActiveTimeDC activeTime = new ExcelineClassActiveTimeDC();
                    activeTime.Id = Convert.ToInt32(reader["ActiveTimeId"]);
                    activeTime.Name = reader["ClassName"].ToString();
                    activeTime.StartDateTime = Convert.ToDateTime(reader["StartDate"]);
                    activeTime.EndDateTime = Convert.ToDateTime(reader["EndDate"]);
                    activeTime.StartDate = activeTime.StartDateTime.ToShortDateString();
                    activeTime.NumberOfMembers = Convert.ToInt32(reader["MemberCount"]);
                    activeTime.InstructorName = reader["InstructorName"].ToString();
                    if (activeTime.EndDateTime < DateTime.Now)
                        activeTimeList.Add(activeTime);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return activeTimeList;
        }
    }
}
