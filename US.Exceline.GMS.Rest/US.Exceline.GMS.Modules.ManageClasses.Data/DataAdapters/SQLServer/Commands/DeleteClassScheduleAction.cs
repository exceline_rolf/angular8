﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteClassScheduleAction:USDBActionBase<bool>
    {
        private int _scheduleItemId = -1;

        public DeleteClassScheduleAction(int scheduleItemId)
        {
            _scheduleItemId = scheduleItemId;
        }

        protected override bool Body(DbConnection connection)
        {
            bool _isDeleted = false;
            string StoredProcedureName = "USExceGMSManageClassesDeleteSchedule";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityScheduleId", DbType.Int32, _scheduleItemId));
                cmd.ExecuteNonQuery();
                _isDeleted = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isDeleted;
        }
    }
}
