﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.ManageClasses.Data.SystemObjects;
using US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer
{
    class SQLServerManageClassDataAdapter : IManageClassesDataAdapter
    {
        public List<ExcelineClassDC> SearchClass(string category, string searchText, string gymCode)
        {
            return new List<ExcelineClassDC>();
        }

        public List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, string gymCode)
        {
            return new List<ExcelineClassDC>();
        }

        public List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, DateTime endDate, string gymCode)
        {
            return new List<ExcelineClassDC>();
        }

        public int SaveClass(ExcelineClassDC excelineClass, string gymCode)
        {
            int classscheduleId = 0;
            AddClassDetailsAction addclassdetailsAction = new AddClassDetailsAction(excelineClass);
            classscheduleId = addclassdetailsAction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            return classscheduleId;
        }

        public List<ExcelineClassDC> GetClasses(string className, int branchId, string gymCode)
        {
            List<ExcelineClassDC> classes = new List<ExcelineClassDC>();
            GetClassDetailsAction getAction = new GetClassDetailsAction(className, branchId);
            classes = getAction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            foreach (ExcelineClassDC eclass in classes)
            {
                GetClassScheduleAction saction = new GetClassScheduleAction(eclass.Id);
                eclass.Schedule = saction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
                GetInstructorsForClassAction Iaction = new GetInstructorsForClassAction(eclass.Id);
                eclass.InstructorList = Iaction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
                GetTrainersForClassAction Taction = new GetTrainersForClassAction(eclass.Id);
                eclass.TrainersList = Taction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
                GetMembersForClassAction Maction = new GetMembersForClassAction(eclass.Id);
                eclass.MemberList = Maction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
                GetResourcesForClassAction Raction = new GetResourcesForClassAction(eclass.Id);
                eclass.ResourceList = Raction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            }
            return classes;
        }

        public int GetNextClassId(string gymCode)
        {
            int _nextClassId;
            GetNextClassIdAction action = new GetNextClassIdAction();
            _nextClassId = action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            return _nextClassId;
        }

        public bool UpdateClass(ExcelineClassDC excelineClass, string gymCode)
        {
            bool _isUpdated = false;
            int output = 0;
            UpdateClassDetailsAction updateAction = new UpdateClassDetailsAction(excelineClass);
            output = updateAction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            if (output > 0)
                _isUpdated = true;
            return _isUpdated;
        }

        public bool DeleteClass(int classId, string gymCode)
        {
            DeleteClassAction deleteAction = new DeleteClassAction(classId);
            return deleteAction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineClassActiveTimeDC> GetClassActiveTimes(int branchId, DateTime startDate, DateTime endDate, int entNo, string gymCode)
        {
            GetClassActiveTimesAction action = new GetClassActiveTimesAction(branchId, startDate, endDate, entNo);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineMemberDC> GetMembersForClass(int classId, string gymCode)
        {
            GetMembersForClassAction action = new GetMembersForClassAction(classId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<TrainerDC> GetTrainersForClass(int classId, string gymCode)
        {
            GetTrainersForClassAction action = new GetTrainersForClassAction(classId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<InstructorDC> GetInstructorsForClass(int classId, string gymCode)
        {
            GetInstructorsForClassAction action = new GetInstructorsForClassAction(classId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<ResourceDC> GetResourcesForClass(int classId, string gymCode)
        {
            GetResourcesForClassAction action = new GetResourcesForClassAction(classId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public ScheduleDC GetClassSchedule(int classId, string gymCode)
        {
            GetClassScheduleAction action = new GetClassScheduleAction(classId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineClassActiveTimeDC> GetClassHistory(int classId, List<ScheduleItemDC> scheduleItemList, string gymCode)
        {
            GetClassHistoryAction getActiveTimeHistory = new GetClassHistoryAction(classId, scheduleItemList);
            return getActiveTimeHistory.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }
        public List<OrdinaryMemberDC> GetMembersForClassBooking(int classId, int branchId, string searchText, string gymCode)
        {
            GetMembersForClassBookingAction action = new GetMembersForClassBookingAction(classId, branchId, searchText);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<ClassBookingActiveTimeDC> GetClassActiveTimesForBooking(int branchId, DateTime startDate, DateTime endDate, int entNo, string gymCode)
        {
            GetClassActiveTimesForBookingAction action = new GetClassActiveTimesForBookingAction(branchId, startDate, endDate, entNo);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public List<MemberBookingDetailsDC> GetMemberBookingDetails(int classId, int branchId, string gymCode)
        {
            GetMemberBookingDetailsForClassAction action = new GetMemberBookingDetailsForClassAction(classId, branchId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public int SaveMemberBookingDetails(int classId, OrdinaryMemberDC ordinaryMemberDC, int memberId, List<ClassBookingDC> bookingList, decimal totalBookingAmount, decimal totalAvailableAmount, string scheduleCategoryType, string user, int branchId, string gymCode)
        {
            SaveMemberBookingsAction action = new SaveMemberBookingsAction(classId, ordinaryMemberDC, memberId, bookingList, totalBookingAmount, totalAvailableAmount, scheduleCategoryType, user, branchId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline,gymCode);
        }

        public bool ApplyClassScheduleForNextYear(int classId, DateTime startdate, DateTime enddate, string name, int branchId, string createdUser, List<ScheduleItemDC> scheduleItemList, string gymCode)
        {
            ApplyClassScheduleForNextYearAction action = new ApplyClassScheduleForNextYearAction(classId, startdate, enddate, name, branchId, createdUser, scheduleItemList);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline,gymCode);
        }

        public bool GetVerificationOfAppliedClassScheduleAction(int classScheduleId, string gymCode)
        {
            GetVerificationOfAppliedClassScheduleAction action = new GetVerificationOfAppliedClassScheduleAction(classScheduleId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline,gymCode);
        }

        public List<ArticleDC> GetArticlesForClass(string gymCode)
        {
            GetArticlesForClassAction action = new GetArticlesForClassAction();
            return action.Execute(US_DataAccess.EnumDatabase.Exceline,gymCode);
        }

        public int SaveClassBookingPayment(int memberId, int classId, BookingArticleDC bookingArticle, List<ClassBookingDC> PaidBookings, List<BookingPaymentDC> bookingPayments, decimal totalAmount, decimal paidAmount, decimal defaultPrice, string user, int branchId, string gymCode)
        {
            SaveClassBookingPaymentAction action = new SaveClassBookingPaymentAction(memberId, classId, bookingArticle, PaidBookings, bookingPayments, totalAmount, paidAmount,defaultPrice,  user,branchId);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline,gymCode);
        }

        public int GetExcelineClassIdByName(string className, string gymCode)
        {
            GetExcelineClassIdAction action = new GetExcelineClassIdAction(className);
            return action.Execute(US_DataAccess.EnumDatabase.Exceline,gymCode);
        }
    }
}
