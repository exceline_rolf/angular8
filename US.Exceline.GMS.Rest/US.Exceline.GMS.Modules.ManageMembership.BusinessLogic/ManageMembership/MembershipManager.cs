﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Technogym.MWApps.Client.Common;
using Technogym.MWApps.Client.Core.Client;
using Technogym.MWApps.Client.Core.Entity;
using Technogym.MWApps.Client.Core.Request;
using Technogym.MWApps.Client.Core.Result;
using Technogym.MWApps.Client.Training.Entity;
using Technogym.MWApps.Client.Training.Request;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageSystemSettings;
using US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.ManageMembership;
using US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations;
using US.Exceline.GMS.Modules.Shop.BusinessLogic.InventoryManagement;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.GMS.Data.DataAdapters;
using US.GMS.Data.DataAdapters.ManageSystemSettings;
using System.Web;
using System.Configuration;
using US.Common.Notification.Core.DomainObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.BusinessLogic.ManageMembership
{
    public class MembershipManager
    {
        #region Manage Member
        public static OperationResult<string> SaveMember(OrdinaryMemberDC member, string imagefolderPath, string gymCode, string user)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveMember(member, gymCode, user);

                // save the member profile picture as a string
                if (!string.IsNullOrEmpty(member.Image))
                {
                    OperationResult<int> gymId = GetSystemId(gymCode);
                    string[] sTextArray = result.OperationReturnValue.Split(new char[] { ':' });
                    if(sTextArray.Length > 0)
                    {
                        try
                        {
                            if(sTextArray.Length > 2)
                            {
                                int memberId;
                                if (int.TryParse(sTextArray[0], out memberId))
                                {
                                    string custId = sTextArray[2];
                                    member.ImagePath = StringToImage(member.Image, custId, imagefolderPath, gymId.OperationReturnValue);
                                    UpdateMemberImagePath(memberId, member.ImagePath, gymCode);
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                            result.CreateMessage("Error in member image save" + ex.Message, MessageTypes.ERROR);
                        }
                    }
                }

                //if (member.ProfilePicture != null)
                //{
                //    OperationResult<int> gymId = GetSystemId(gymCode);
                //    string[] sTextArray = result.OperationReturnValue.Split(new char[] { ':' });
                //    if (sTextArray.Length > 0)
                //    {
                //        try
                //        {
                //            if (sTextArray.Length > 2)
                //            {
                //                int memberId;
                //                if (int.TryParse(sTextArray[0], out memberId))
                //                {
                //                    string custId = sTextArray[2];
                //                    member.ImagePath = byteArrayToImage(member.ProfilePicture, custId, imagefolderPath, gymCode, gymId.OperationReturnValue);
                //                    UpdateMemberImagePath(memberId, member.ImagePath, gymCode);
                //                }
                //            }
                //        }
                //        catch (Exception ex)
                //        {
                //            result.CreateMessage("Error in member image save" + ex.Message, MessageTypes.ERROR);
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving the member" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ValidateMemberBrisId(int brisId, string gymCode)
        {
            var result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.ValidateMemberBrisId(brisId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in validating bris id" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateMemberImagePath(int memberId, string imagePath, string gymCode)
        {
            var result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdateMemberImagePath(memberId, imagePath, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving member image path" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> GetSystemId(string gymCode)
        {
            return OperationsManager.GetSystemId(gymCode);
        }

        public static OperationResult<List<OrdinaryMemberDC>> GetGroupMemebersByGroup(int branchId, string searchText, bool IsActive, int groupId, string gymCode)
        {
            OperationResult<List<OrdinaryMemberDC>> result = new OperationResult<List<OrdinaryMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetGroupMemebersByGroup(branchId, searchText, IsActive, groupId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retrieving group memebers" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static string StringToImage(string imgString, string memberCustId, string imageFolderPath, int gymId)
        {
            String path = String.Empty;

            if (!string.IsNullOrEmpty(imgString))
            {
                try
                {
                    if (!Directory.Exists(imageFolderPath + "/" + gymId))
                    {
                        Directory.CreateDirectory(imageFolderPath + "/" + gymId);
                    }
                    if(File.Exists(imageFolderPath + "/" + gymId + "/" + memberCustId + ".jpg"))
                    {
                        File.Delete(imageFolderPath + "/" + gymId + "/" + memberCustId + ".jpg");
                    }
                    path = imageFolderPath + '/' + gymId;
                    string imageName = memberCustId + ".jpg";
                    path = Path.Combine(path, imageName);
                    string[] imageStringInfo = imgString.Split(',');
                    byte[] imageBytes = Convert.FromBase64String(imageStringInfo[1]);
                    //File.WriteAllBytes(imagePath, imageBytes);
                    Image image;
                    using (MemoryStream ms = new MemoryStream(imageBytes))
                    {
                        image = Image.FromStream(ms);
                        image.Save(path);
                    }
                }
                catch (Exception e)
                {

                    throw e;
                }
            }
            return path;
        }

        public static string byteArrayToImage(byte[] byteArrayIn, string memberCustId, string imageFolderPath, string gymCode, int gymId)
        {
            string imagePath = string.Empty;
            if (byteArrayIn != null)
            {
                try
                {
                    if (!Directory.Exists(imageFolderPath + "/" + gymCode))
                    {

                        Directory.CreateDirectory(imageFolderPath + "/" + gymCode);

                    }
                    if (File.Exists(imageFolderPath + "/" + gymCode + "/" + memberCustId + ".file"))
                    {
                        File.Delete(imageFolderPath + "/" + gymCode + "/" + memberCustId + ".file");
                    }
                    ByteArrayToFile(imageFolderPath + "/" + gymCode + "/" + memberCustId + ".file", byteArrayIn);
                    imagePath = imageFolderPath + "/" + gymCode + "/" + memberCustId + ".file";


                    if (gymId > 0)
                    {
                        if (!Directory.Exists(imageFolderPath + "/" + gymId.ToString()))
                        {
                            Directory.CreateDirectory(imageFolderPath + "/" + gymId.ToString());
                        }
                        if (File.Exists(imageFolderPath + "/" + gymId.ToString() + "/" + memberCustId + ".jpg"))
                        {
                            File.Delete(imageFolderPath + "/" + gymId.ToString() + "/" + memberCustId + ".jpg");
                        }
                        ByteArrayToJpgImage(imageFolderPath + "/" + gymId.ToString() + "/" + memberCustId + ".jpg", byteArrayIn);
                    }

                }
                catch
                {

                }
            }
            return imagePath;
        }


        public static void ByteArrayToJpgImage(string path, Byte[] bArray)
        {
            var width = 92; // for example
            var height = 92; // for example
            var dpiX = 96d;
            var dpiY = 96d;
            var pixelFormat = PixelFormats.Pbgra32; // for example
            var bytesPerPixel = (pixelFormat.BitsPerPixel + 7) / 8;
            var stride = bytesPerPixel * width;
            var bitmapSource = BitmapSource.Create(width, height, dpiX, dpiY,
                                             pixelFormat, null, bArray, stride);

            Bitmap bitmap;
            using (var outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapSource));
                enc.Save(outStream);
                bitmap = new Bitmap(outStream);
            }

            var newBitmap = new Bitmap(bitmap);
            bitmap.Dispose();
            bitmap = null;
            newBitmap.Save(path);
        }

        public static bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {
            FileStream _FileStream = null;
            try
            {
                _FileStream = new FileStream(_FileName, FileMode.Create, FileAccess.Write);
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);
                _FileStream.Close();
                _FileStream.Dispose();
                return true;
            }
            catch
            {

            }
            finally
            {
                if (_FileStream != null)
                {
                    _FileStream.Close();
                    _FileStream.Dispose();
                }
            }
            return false;
        }

        public static OperationResult<string> UpdateMember(OrdinaryMemberDC member, string imagefolderPath, string gymCode, string user)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                if (member.Image != null)
                {
                    OperationResult<int> gymId = GetSystemId(gymCode);
                    member.ImagePath = StringToImage(member.Image, member.CustId, imagefolderPath, gymId.OperationReturnValue);
                }
                result.OperationReturnValue = ManageMembershipFacade.UpdateMember(member, gymCode, user);


            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Follow Up Updating" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DisableMember(int memberId, bool activeState, string comment, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.DisableMember(memberId, activeState, comment, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Follow Up Updating" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteCreditNote(int creditNoteId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.DeleteCreditNote(creditNoteId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in deleteing creditNote" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> MembercardVisit(int memberId, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.MembercardVisit(memberId, user, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in logging member visit from memberlist(memberclick to membercard) " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<MemberForMemberlist>> GetMemberList(FilterMemberList parameters, string user, string gymCode)
        {
            OperationResult<List<MemberForMemberlist>> result = new OperationResult<List<MemberForMemberlist>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberList(parameters, user, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting members to memberlist" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<int>> GetMemberCount(string gymCode, int branchId)
        {
            OperationResult<List<int>> result = new OperationResult<List<int>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberCount(gymCode, branchId);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting member count" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<MemberForMemberlist>> GetVisitedMembers(string user, string gymCode)
        {
            OperationResult<List<MemberForMemberlist>> result = new OperationResult<List<MemberForMemberlist>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetVisitedMembers(user, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting visited members for user" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineMemberDC>> GetMembers(int branchId, string searchText, int statuse, MemberSearchType searchType, MemberRole memberRole, string user, string gymCode, int hit, bool isHeaderClick, bool isAscending, string sortName)
        {
            OperationResult<List<ExcelineMemberDC>> result = new OperationResult<List<ExcelineMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMembers(branchId, searchText, statuse, searchType, memberRole, user, gymCode, hit, isHeaderClick, isAscending, sortName);
                
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting members" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<OrdinaryMemberDC>> GetMembersByStatus(int statusId, string systemName, string user, string gymCode)
        {
            OperationResult<List<OrdinaryMemberDC>> result = new OperationResult<List<OrdinaryMemberDC>>();
            try
            {
                List<OrdinaryMemberDC> members = ManageMembershipFacade.GetMembersByStatus(statusId, systemName, user, gymCode);
                result.OperationReturnValue = members;

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting members by Status  for  integration" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<PackageDC>> GetContracts(int branchId, int contractType, string gymCode)
        {
            OperationResult<List<PackageDC>> result = new OperationResult<List<PackageDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetContracts(branchId, contractType, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }

        public static OperationResult<bool> SetContractSequenceID(List<PackageDC> contractList, int branchID, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SetContractSequenceID(contractList, branchID, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<PackageDC> GetContractTemplateDetails(string type, int templateID, string gymCode)
        {
            OperationResult<PackageDC> result = new OperationResult<PackageDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetContractTemplateDetails(type, templateID, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting contract template details " + ex.Message, MessageTypes.ERROR);
            }
            return result;

        }


        public static OperationResult<ContractSaveResultDC> SaveMemberContract(MemberContractDC memberContract, string gymCode, string user)
        {
            OperationResult<ContractSaveResultDC> result = new OperationResult<ContractSaveResultDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveMemberContract(memberContract, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Saving " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<MemberContractDC> GetMemberContractDetails(int contractId, int branchId, string gymCode)
        {
            OperationResult<MemberContractDC> result = new OperationResult<MemberContractDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberContractDetails(contractId, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Contract details " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ContractItemDC>> GetContractTemplateItems(int contractId, int branchId, string gymCode)
        {
            OperationResult<List<ContractItemDC>> result = new OperationResult<List<ContractItemDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetContractTemplateItems(contractId, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Contracts " + ex.Message, MessageTypes.ERROR);
            }

            return result;
        }

        public static OperationResult<List<InstallmentDC>> GetInstallments(int memberContractId, string gymCode)
        {
            OperationResult<List<InstallmentDC>> result = new OperationResult<List<InstallmentDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetInstallments(memberContractId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Installments " + ex.Message, MessageTypes.ERROR);
            }

            return result;
        }

        public static OperationResult<bool> UpdateMemberInstallment(InstallmentDC installment, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdateMemberInstallment(installment, gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Updating Installment " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> RenewMemberContract(MemberContractDC renewedContract, List<InstallmentDC> installmentList, string gymCode, int branchId, bool isAutoRenew, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.RenewMemberContract(renewedContract, installmentList, gymCode, branchId, isAutoRenew, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Renewing Member Contract " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateMemberAddonInstallments(List<InstallmentDC> installments, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdateMemberAddonInstallments(installments, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Updating Order " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SwitchPayer(int memberId, int payerId, DateTime activatedDate, bool isSave, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SwitchPayer(memberId, payerId, activatedDate, isSave, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving switchpayer " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<EntityVisitDetailsDC> GetMemberVisits(int memberId, DateTime selectedDate, int branchId, string gymCode, string user)
        {
            OperationResult<EntityVisitDetailsDC> result = new OperationResult<EntityVisitDetailsDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberVisits(memberId, selectedDate, branchId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Member Visits " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveMemberVisit(EntityVisitDC visit, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveMemberVisit(visit, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erron in Saving Member Visit " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteMemberVisit(int memberVisitId, int memberContractId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.DeleteMemberVisit(memberVisitId, memberContractId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erron in Deleting Member Visit " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> CancelMemberContract(int memberContractId, string canceledBy, string comment, List<int> installmentIds, int minNumber, int tergetInstallmentId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.CancelMemberContract(memberContractId, canceledBy, comment, installmentIds, minNumber, tergetInstallmentId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static OperationResult<bool> IntroducePayer(int memberId, int introduceId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.IntroducePayer(memberId, introduceId, gymCode);
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static OperationResult<List<MemberContractDC>> GetMemberContractsForActivity(int memberID, int activityId, int branchId, string user, string gymCode)
        {
            OperationResult<List<MemberContractDC>> result = new OperationResult<List<MemberContractDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberContractsForActivity(memberID, activityId, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Member Contracts " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ClassDetailDC>> GetClassByMemberId(int branchId, int memberId, DateTime? calssDate, string user, string gymCode)
        {
            OperationResult<List<ClassDetailDC>> result = new OperationResult<List<ClassDetailDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetClassByMemberId(branchId, memberId, calssDate, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Class for member" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<bool> AddFamilyMember(int memberId, int familyMemberId, int memberContractId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.AddFamilyMember(memberId, familyMemberId, memberContractId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erron in Adding FamilyMember " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<InstallmentDC>> UpdateInstallmentWithMemberContract(decimal installmentAmount, int updatingInstallmentNo, MemberContractDC memberContract, string gymCode)
        {
            OperationResult<List<InstallmentDC>> result = new OperationResult<List<InstallmentDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.AddFamilyMember(installmentAmount, updatingInstallmentNo, memberContract, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erron in Updating the installments " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SaveSponsor(int memberId, int sponsorId, int branchId, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveSponsor(memberId, sponsorId, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erron in Adding FamilyMember " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> SaveMemberParent(OrdinaryMemberDC memberParent, int branchId, string user, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveMemberParent(memberParent, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erron in Savimg MemberParent " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<OrdinaryMemberDC> GetMemberDetailsByMemberId(int branchId, string user, int memberId, string memberRole, string gymCode)
        {
            OperationResult<OrdinaryMemberDC> result = new OperationResult<OrdinaryMemberDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberDetailsByMemberId(branchId, user, memberId, memberRole, gymCode);
                // adding for the image path url
                if (!string.IsNullOrEmpty(result.OperationReturnValue.ImagePath))
                {
                    int gymId = GetSystemId(gymCode).OperationReturnValue;
                    result.OperationReturnValue.ImageURLDomain = ConfigurationManager.AppSettings["ImageUrl"] + '/' +
                        gymId.ToString() + '/' + result.OperationReturnValue.CustId + ".jpg";
                }else
                {
                    result.OperationReturnValue.ImageURLDomain = string.Empty;
                }
                
                /////////////////////////////////////////////

                
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erron in getting MemberDetailfor member id " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<CategoryDC>> GetInterestCategoryByMember(int memberId, int branchId, string gymCode)
        {
            OperationResult<List<CategoryDC>> result = new OperationResult<List<CategoryDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetInterestCategoryByMember(memberId, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Interest Category " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SaveInterestCategoryByMember(int memberId, int branchId, List<CategoryDC> interestCategoryList, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveInterestCategoryByMember(memberId, branchId, interestCategoryList, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in saving Interest Category " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<OrdinaryMemberDC>> GetSwitchPayers(int branchId, int memberId, string gymCode)
        {
            OperationResult<List<OrdinaryMemberDC>> result = new OperationResult<List<OrdinaryMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetSwitchPayers(branchId, memberId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in geting switch payer " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineMemberDC>> GetIntroducedMembers(int memberId, int brachId, string gymCode)
        {
            OperationResult<List<ExcelineMemberDC>> result = new OperationResult<List<ExcelineMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetIntroducedMembers(memberId, brachId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in geting Introduced Members " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateIntroducedMembers(int memberId, DateTime? creditedDate, string creditedText, bool isDelete, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdateIntroducedMembers(memberId, creditedDate, creditedText, isDelete, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating introduced member" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<Dictionary<int, string>> GetMemberStatus(string gymCode, bool getAllStatuses)
        {
            OperationResult<Dictionary<int, string>> result = new OperationResult<Dictionary<int, string>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberStatus(gymCode, getAllStatuses);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in geting member status " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region Sponser Contarct
        public static OperationResult<SponsorSettingDC> GetSponsorSetting(int branchId, string user, int sponsorId, string gymCode)
        {
            OperationResult<SponsorSettingDC> result = new OperationResult<SponsorSettingDC>();
            try
            {
                int discountTypeId = -1;
                SponsorSettingDC sponsorSetting = new SponsorSettingDC();
                sponsorSetting = ManageMembershipFacade.GetSponsorSetting(branchId, user, sponsorId, gymCode);
                discountTypeId = sponsorSetting.DiscountTypeId;
                sponsorSetting.DiscountCategoryList = ManageMembershipFacade.GetGroupDiscountByType(branchId, user, discountTypeId, gymCode, sponsorId);
                sponsorSetting.EmployeeCategoryList = ManageMembershipFacade.GetSponsorEmployeeCategoryList(branchId, gymCode, sponsorId);
                sponsorSetting.SponsoredMembers = ManageMembershipFacade.GetSponsoredMemberList(branchId, sponsorId, gymCode);
                result.OperationReturnValue = sponsorSetting;
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retrieving contract setting " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<int>> GetSponsoredMemberListByTimePeriod(int sponsorId, Dictionary<int, List<DateTime>> sponsorMembers, string gymcode)
        {
            OperationResult<List<int>> result = new OperationResult<List<int>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetSponsoredMemberListByTimePeriod(sponsorId, sponsorMembers, gymcode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retrieving Sponsor Members " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<List<DiscountDC>> GetGroupDiscountByType(int branchId, string user, int discountTypeId, string gymCode, int sponsorId)
        {
            OperationResult<List<DiscountDC>> result = new OperationResult<List<DiscountDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetGroupDiscountByType(branchId, user, discountTypeId, gymCode, sponsorId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retrieving discounts by type " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<bool> RemoveSponsorshipOfMember(int sponsoredRecordId, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.RemoveSponsorshipOfMember(sponsoredRecordId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in removing sponsoship" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SaveSponsorSetting(SponsorSettingDC sponsorSetting, int branchId, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveSponsorSetting(sponsorSetting, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retrieving contract setting " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteEmployeeCategory(int categoryID, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.DeleteEmployeeCategory(categoryID, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in deleting employee category" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> AddSponserShipForMember(int memContractId, int sponserContractId, int employeeCategotyID, int actionType, string employeeRef, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.AddSponserShipForMember(memContractId, sponserContractId, employeeCategotyID, actionType, employeeRef, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in adding sponsership" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveSponserContract(SponsorSettingDC sponserContract, int branchId, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveSponserContract(sponserContract, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in saving sponser contract" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<OrdinaryMemberDC>> GetMembersByRoleType(int branchId, string user, int status, MemberRole roleType, string searchText, string gymCode)
        {
            OperationResult<List<OrdinaryMemberDC>> result = new OperationResult<List<OrdinaryMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMembersByRoleType(branchId, user, status, roleType, searchText, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retreving members" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<EmployeeCategoryDC>> GetSponsorEmployeeCategoryList(int branchId, string gymcode, int sponsorId)
        {
            OperationResult<List<EmployeeCategoryDC>> result = new OperationResult<List<EmployeeCategoryDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetSponsorEmployeeCategoryList(branchId, gymcode, sponsorId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retreving employee categories" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<OrdinaryMemberDC> GetEmployeeCategoryBySponsorId(int branchId, string gymcode, int sponsorId)
        {
            OperationResult<OrdinaryMemberDC> result = new OperationResult<OrdinaryMemberDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetEmployeeCategoryBySponsorId(branchId, gymcode, sponsorId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retreving employee categories by SponsorId" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<List<ExcelineMemberDC>> GetMembersBySponsorId(int sponsorId, int branchId, string gymcode)
        {
            OperationResult<List<ExcelineMemberDC>> result = new OperationResult<List<ExcelineMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMembersBySponsorId(sponsorId, branchId, gymcode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retreving sponsored members by sponnsorId " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }





        public static OperationResult<List<MemberContractDC>> GetSponserContractsByActivity(int sponserId, int activityId, int branchId, string gymCode)
        {
            OperationResult<List<MemberContractDC>> result = new OperationResult<List<MemberContractDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetSponserContractsByActivity(sponserId, activityId, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retreving employee categories" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<bool> CancelSponsorContract(int contractId, string user, string comment, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.CancelSponsorContract(contractId, user, comment, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in cancelling sponsor contract" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        #endregion

        #region Manage Discounts

        public static OperationResult<bool> AddMemberToGroup(List<int> memberIdList, int groupId, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                if (memberIdList != null)
                {
                    foreach (int memeber in memberIdList)
                    {
                        result.OperationReturnValue = ManageMembershipFacade.AddMemberToGroup(memeber, groupId, user, gymCode);
                    }
                    result.OperationReturnValue = true;
                }
                else
                {
                    result.OperationReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in adding member to group" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> RemoveMemberFromGroup(List<int> memberList, int groupId, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                if (memberList != null)
                {
                    foreach (int memeberId in memberList)
                    {
                        result.OperationReturnValue = ManageMembershipFacade.RemoveMemberFromGroup(memeberId, groupId, user, gymCode);
                    }
                    result.OperationReturnValue = true;
                }
                else
                {
                    result.OperationReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in removing members from group" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteDiscount(List<int> discountIDList, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                foreach (int discountID in discountIDList)
                {
                    result.OperationReturnValue = ManageMembershipFacade.DeleteDiscount(discountID, gymCode);
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in deleting employee category" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<DiscountDC>> GetDiscountList(int branchId, DiscountType discountType, string gymCode)
        {
            OperationResult<List<DiscountDC>> result = new OperationResult<List<DiscountDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetDiscountList(branchId, discountType, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retrieveing error list" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveDiscount(List<DiscountDC> discountList, string user, int branchId, int contractSettingID, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                if (discountList != null)
                {
                    //foreach (DiscountDC discount in discountList)
                    //{
                    // discount.BranchId = branchId;
                    result.OperationReturnValue = ManageMembershipFacade.SaveDiscount(discountList, user, branchId, contractSettingID, gymCode);
                    //}
                }
                else
                {
                    result.OperationReturnValue = 0;
                }
            }
            catch (Exception ex)
            {
                result.OperationReturnValue = 0;
                result.CreateMessage("Error in inserting discounts" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<DiscountDC>> GetDiscountsForActivity(int branchId, int activityId, string gymCode)
        {
            OperationResult<List<DiscountDC>> result = new OperationResult<List<DiscountDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetDiscountsForActivity(branchId, activityId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in retrieveing discount list" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<ShopSalesDC> GetMemberPerchaseHistory(int memberId, DateTime fromDate, DateTime toDate, string gymCode, int branchId, string user)
        {
            OperationResult<ShopSalesDC> result = new OperationResult<ShopSalesDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberPerchaseHistory(memberId, fromDate, toDate, gymCode, branchId, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #endregion

        #region Freeze Contract

        public static OperationResult<int> SaveContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveContractFreezeItem(freezeItem, freezeInstallments, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Freezing Contract " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ContractFreezeItemDC>> GetContractFreezeItems(int memberContractId, string user, string gymCode)
        {
            OperationResult<List<ContractFreezeItemDC>> result = new OperationResult<List<ContractFreezeItemDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetContractFreezeItems(memberContractId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting freeze items " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ContractFreezeInstallmentDC>> GetContractFreezeInstallments(int freezeItemId, string user, string gymCode)
        {
            OperationResult<List<ContractFreezeInstallmentDC>> result = new OperationResult<List<ContractFreezeInstallmentDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetContractFreezeInstallments(freezeItemId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting freeze installments " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<InstallmentDC>> GetInstallmentsToFreeze(int memberContractId, string user, string gymCode)
        {
            OperationResult<List<InstallmentDC>> result = new OperationResult<List<InstallmentDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetInstallmentsToFreeze(memberContractId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting installments to freeze " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> ExtendContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.ExtendContractFreezeItem(freezeItem, freezeInstallments, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Extending Freez Contract " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> UnfreezeContract(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> unfreezeInstallments, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UnfreezeContract(freezeItem, unfreezeInstallments, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Unfreezing Contract " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> IsFreezeAllowed(int memberContractId, int memberId, string gymCode, int branchId)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.IsFreezeAllowed(memberContractId, memberId, gymCode, branchId);
            }
            catch (Exception)
            {
                result.CreateMessage("Error on checking allow to freeze", MessageTypes.ERROR);
            }
            return result;
        }

        #endregion

        #region Payments History
        public static OperationResult<List<ExcePaymentInfoDC>> GetMemberPaymentsHistory(int memberId, string paymentType, string gymCode, int hit, string user)
        {
            OperationResult<List<ExcePaymentInfoDC>> result = new OperationResult<List<ExcePaymentInfoDC>>();
            try
            {
                //result.OperationReturnValue = ManageMembershipFacade.GetMemberPaymentsHistory(memberId, paymentType, gymCode);
                List<ExcePaymentInfoDC> paymentInfo = ManageMembershipFacade.GetMemberPaymentsHistory(memberId, paymentType, gymCode, hit, user);
                result.OperationReturnValue = paymentInfo;
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving the member" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        #endregion
        
        public static OperationResult<int> SaveNote(int branchId, string use, int memberId, string note, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveNote(branchId, use, memberId, note, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving note" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        
        public static OperationResult<bool> SaveRenewContractDetails(RenewSummaryDC renewSummary, int branchId, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveRenewContractDetails(renewSummary, branchId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating renew contrcat details" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        
        public static OperationResult<List<RenewContractDC>> GetContractsForAutoRenew(int branchId, string gymCode)
        {
            OperationResult<List<RenewContractDC>> result = new OperationResult<List<RenewContractDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetContractsForAutoRenew(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving note" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> RenewContract(int memberContractId, int branchId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.RenewContract(memberContractId, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Renewing member contract" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<InstallmentDC> GetMemberContractLastInstallment(int memberContractId, string gymCode)
        {
            OperationResult<InstallmentDC> result = new OperationResult<InstallmentDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberContractLastInstallment(memberContractId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting contract last installment" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<OrdinaryMemberDC> GetNote(int branchId, string user, int memberId, string gymCode)
        {
            OperationResult<OrdinaryMemberDC> result = new OperationResult<OrdinaryMemberDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetNote(branchId, user, memberId, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting note" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineMemberDC>> GetGroupMembers(int groupId, string gymCode)
        {
            OperationResult<List<ExcelineMemberDC>> result = new OperationResult<List<ExcelineMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetGroupMembers(groupId, gymCode);
              //  result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(resultList);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Group Members" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        
        public static OperationResult<List<ExcelineMemberDC>> GetFamilyMembers(int memberId, string gymCode)
        {
            OperationResult<List<ExcelineMemberDC>> result = new OperationResult<List<ExcelineMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetFamilyMembers(memberId, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Family Members" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SaveFollowUpPopup(int followUpId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveFollowUpPopup(followUpId, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Follow-Ups" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }



      
        public static OperationResult<List<FollowUpDC>> GetFollowUps(int memberId,int followUpId, string gymCode, string user)
        {
            OperationResult<List<FollowUpDC>> result = new OperationResult<List<FollowUpDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetFollowUps(memberId, followUpId, gymCode, user);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Follow-Ups" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        public static OperationResult<List<FollowUpTemplateTaskDC>> GetFollowUpTask(string gymCode)
        {
            OperationResult<List<FollowUpTemplateTaskDC>> result = new OperationResult<List<FollowUpTemplateTaskDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetFollowUpTask(gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Follow-Up Task" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveFollowUp(List<FollowUpDC> followUpList, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveFollowUp(followUpList, gymCode, user);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Save follow-up" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineMemberDC>> GetListOfGuardian(int guardianId, string gymCode)
        {
            OperationResult<List<ExcelineMemberDC>> result = new OperationResult<List<ExcelineMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetListOfGuardian(guardianId, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Guardian" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<OrdinaryMemberDC> GetEconomyDetails(int memberId, string gymCode)
        {
            OperationResult<OrdinaryMemberDC> result = new OperationResult<OrdinaryMemberDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetEconomyDetails(memberId, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Economy Details" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        
        public static OperationResult<bool> SaveRenewMemberContractItem(List<ContractItemDC> itemList, int memberContractId, int branchId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveRenewMemberContractItem(itemList, memberContractId, branchId, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Save renewing contract items" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        
        public static OperationResult<OrdinaryMemberDC> GetMemberInfoWithNotes(int branchId, int memberId, string gymCode)
        {
            OperationResult<OrdinaryMemberDC> result = new OperationResult<OrdinaryMemberDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberInfoWithNotes(branchId, memberId, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting InfoWithNotes Details" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        
        public static OperationResult<double> ChangePrePaidAccount(string increase, float amount, int memberId, int branchId, string gymCode, string user)
        {
            OperationResult<double> result = new OperationResult<double>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.ChangePrePaidAccount(increase, amount, memberId, branchId, gymCode, user);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in changing Pre Paid Account balance" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SaveEconomyDetails(OrdinaryMemberDC member, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveEconomyDetails(member, gymCode, user);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Save Economy Details " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> SaveMemberInfoWithNotes(OrdinaryMemberDC member, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveMemberInfoWithNotes(member, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Member Info With Notes Details " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ContractBookingDC>> GetContractBookings(int membercontractId, string gymCode)
        {
            OperationResult<List<ContractBookingDC>> result = new OperationResult<List<ContractBookingDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetContractBookings(membercontractId, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Bookings" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> UpdateMemberContract(MemberContractDC memberContract, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdateMemberContract(memberContract, gymCode, user);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating contract " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteInstallment(List<int> installmentIdList, int memberContractId, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.DeleteInstallment(installmentIdList, memberContractId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Deleting installment " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ResignContract(ContractResignDetailsDC resignDetails, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.ResignContract(resignDetails, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Deleting installment " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<InstallmentDC>> GetMemberOrders(int memberId, string type, string gymCode, string user)
        {
            OperationResult<List<InstallmentDC>> result = new OperationResult<List<InstallmentDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberOrders(memberId, type, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting orders " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<CreditNoteDC> GetCreditNoteDetails(int arItemNo, string gymCode)
        {
            OperationResult<CreditNoteDC> result = new OperationResult<CreditNoteDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetCreditNoteDetails(arItemNo, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error retrieving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> GetInvoicePathByARitemNo(int arItemNo, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetInvoicePathByARitemNo(arItemNo, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error retrieving invoice file  path" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SaveMemberInstallments(List<InstallmentDC> installmentList, bool initialGenInstmnts, DateTime startDate, DateTime endDate, int memberContractId, int membercontractNo, int branchId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveMemberInstallments(installmentList, initialGenInstmnts, startDate, endDate, memberContractId, membercontractNo, branchId, gymCode, -1);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in saving Installments" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> GetContractMembers(int memberContractId, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                List<ExcelineMemberDC> contractMembers = ManageMembershipFacade.GetContractMembers(memberContractId, gymCode);
                result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(contractMembers);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Contract Members" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateContractOrder(List<InstallmentDC> installmentList, string gymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdateContractOrder(installmentList, gymCode, user);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Order " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        
        public static OperationResult<int> SaveContractGroupMembers(int groupId, List<ExcelineMemberDC> groupMemberList, int contractId, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveContractGroupMembers(groupId, groupMemberList, contractId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Group Member saving " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> CheckBookingAvailability(int resourceID, string day, DateTime startTime, DateTime endTime, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.CheckBookingAvailability(resourceID, day, startTime, endTime, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Checking booking availability " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> AddInstallment(InstallmentDC installment, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.AddInstallment(installment, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Group Member saving " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> AddBlancInstallment(int MemberId, int BranchId, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.AddBlancInstallment(MemberId, BranchId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Group Member saving " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> GetMemberSearchCategory(int branchId, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberSearchCategory(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error geting Member search category " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SaveDocumentData(DocumentDC document, string username, int branchId)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SaveDocumentData(document, username, branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error saving document upload data " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<DocumentDC>> GetDocumentData(bool isActive, string custId, string searchText, int documentType, string gymCode, int branchId)
        {
            OperationResult<List<DocumentDC>> result = new OperationResult<List<DocumentDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetDocumentData(isActive, custId, searchText, documentType, gymCode, branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error getting document upload data " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteDocumentData(int docId, string username, int branchId)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.DeleteDocumentData(docId, username, branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error deleating document upload data " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static object SetPDFFileParth(int id, string fileParth, string flag, int branchId, string GymCode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.SetPDFFileParth(id, fileParth, flag, branchId, GymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error Set PDF Pile Parth " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<CustomerBasicInfoDC> GetMemberBasicInfo(int memberId, string gymcode)
        {
            OperationResult<CustomerBasicInfoDC> result = new OperationResult<CustomerBasicInfoDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberBasicInfo(memberId, gymcode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member basic info " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> GetVisitCount(int memberId, DateTime startDate, DateTime endDate, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetVisitCount(memberId, startDate, endDate, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member visit count " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<InstallmentDC>> MergeOrders(List<InstallmentDC> updatedOrders, List<int> removedOrder, int memberContractId, string gymCode, string user)
        {
            OperationResult<List<InstallmentDC>> result = new OperationResult<List<InstallmentDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.MergeOrders(updatedOrders, removedOrder, memberContractId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in merge orders " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<FollowUpDetailDC>> GetFollowUpDetailByEmpId(int empId, string gymCode)
        {
            OperationResult<List<FollowUpDetailDC>> result = new OperationResult<List<FollowUpDetailDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetFollowUpDetailByEmpId(empId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Get FollowUp Detail By EmpId " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<MemberBookingDC>> GetMemberBookings(int empId, string gymCode, string user)
        {
            OperationResult<List<MemberBookingDC>> result = new OperationResult<List<MemberBookingDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberBookings(empId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Member Bookings " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineMemberDC>> GetOtherMembersForBookging(int scheduleId, string gymCode)
        {
            OperationResult<List<ExcelineMemberDC>> result = new OperationResult<List<ExcelineMemberDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetOtherMembersBooking(scheduleId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Other Members For Bookings " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ValidateMemberWithCard(string cardNo, string accessType, string gymCode, int branchId, string user)
        {
            bool memberFound = false;
            OperationResult<bool> result = new OperationResult<bool>();
            ExceACCMember selectedMember = null;
            try
            {
                List<ExceACCMember> members = new List<ExceACCMember>();
                members = ManageMembershipFacade.GetMemberDetailsForCard(cardNo, accessType, gymCode);

                ExceACCSettings setting = new ExceACCSettings();
                Dictionary<string, object> gymSettingDictionary = new Dictionary<string, object>();
                List<string> settings = new List<string>();
                settings.Add("AccBlockAccessNoOfUnpaidInvoices");
                settings.Add("AccBlockAccessDueBalance");
                settings.Add("AccBlockAccessOnAccountBalance");
                settings.Add("AccMinutesBetweenAccess");

                gymSettingDictionary = GymSettingFacade.GetSelectedGymSettings(settings, true, branchId, gymCode);
                foreach (KeyValuePair<string, object> pair in gymSettingDictionary)
                {
                    switch (pair.Key)
                    {
                        case "AccBlockAccessNoOfUnpaidInvoices":
                            setting.MinimumUnpaidInvoices = Convert.ToInt32(pair.Value);
                            break;

                        case "AccBlockAccessDueBalance":
                            setting.UnPaidDueBalance = Convert.ToDecimal(pair.Value);
                            break;

                        case "AccBlockAccessOnAccountBalance":
                            setting.MinimumOnAccountBalance = Convert.ToDecimal(pair.Value);
                            break;

                        case "AccMinutesBetweenAccess":
                            setting.MinutesBetweenSwipes = Convert.ToInt32(pair.Value);
                            break;
                    }
                }

                List<ExceAccessProfileDC> AccessProfiles = new List<ExceAccessProfileDC>();
                AccessProfiles = SystemFacade.GetAccessProfiles(gymCode, Gender.ALL);

                List<GymOpenTimeDC> openTimes = new List<GymOpenTimeDC>();
                openTimes = GymSettingFacade.GetGymOpenTimes(branchId, gymCode, gymCode);

                List<ActivityTimeDC> ClosedTimes = new List<ActivityTimeDC>();
                ClosedTimes = SystemSettingsFacade.GetActivityUnavailableTimes(branchId, gymCode);

                ExceACCMember contractMemberWithAcess = null;
                ExceACCMember contractMemberWithOutAcess = null;
                ExceACCMember punchCardMember = null;
                ExceACCMember punchCardMemberWithNoAcces = null;
                if (members.Count > 0)
                {
                    contractMemberWithAcess = members.FirstOrDefault(X => X.MemberCardNo == cardNo && X.ContractType == "CONTRACT" && X.AccessProfileId != -1);
                    contractMemberWithOutAcess = members.FirstOrDefault(X => X.MemberCardNo == cardNo && X.ContractType == "CONTRACT" && X.AccessProfileId == -1);
                    punchCardMember = members.FirstOrDefault(X => X.MemberCardNo == cardNo && X.ContractType == "PUNCHCARD" && X.AccessProfileId != -1);
                    punchCardMemberWithNoAcces = members.FirstOrDefault(X => X.MemberCardNo == cardNo && X.ContractType == "PUNCHCARD" && X.AccessProfileId == -1);

                    if (contractMemberWithAcess != null)
                    {
                        if (ValidateWithHolidays(contractMemberWithAcess.ActivityId, contractMemberWithAcess.ActivityName, ClosedTimes))//Validate with holidays 
                        {
                            if (CheckForAccessTimes(contractMemberWithAcess.AccessProfileId, AccessProfiles))
                            {
                                if (ValidateMember(contractMemberWithAcess, setting, result))
                                {
                                    result.OperationReturnValue = true;
                                    memberFound = true;
                                    selectedMember = contractMemberWithAcess;
                                }
                                else
                                {
                                    result.OperationReturnValue = false;
                                }
                            }
                        }
                        else
                        {
                            result.OperationReturnValue = false;
                            NotificationMessage message = new NotificationMessage("Senteret er stengt for din aktivitet", MessageTypes.INFO);
                            if (!result.Notifications.Contains(message))
                            {
                                result.CreateMessage("Senteret er stengt for din aktivitet", MessageTypes.INFO);
                            }
                        }
                    }

                    if (memberFound == false && punchCardMember != null)
                    {
                        if (ValidateWithHolidays(punchCardMember.ActivityId, punchCardMember.ActivityName, ClosedTimes))//Validate with holidays 
                        {
                            if (CheckForAccessTimes(punchCardMember.AccessProfileId, AccessProfiles))
                            {
                                if (ValidateMember(punchCardMember, setting, result))
                                {
                                    memberFound = true;
                                    result.OperationReturnValue = true;
                                    selectedMember = punchCardMember;
                                }
                                else
                                {
                                    result.OperationReturnValue = false;
                                }
                            }
                        }
                        else
                        {
                            result.OperationReturnValue = false;
                            NotificationMessage message = new NotificationMessage("Senteret er stengt for din aktivitet", MessageTypes.INFO);
                            if (!result.Notifications.Contains(message))
                            {
                                result.CreateMessage("Senteret er stengt for din aktivitet", MessageTypes.INFO);
                            }
                        }
                    }

                    if (memberFound == false && contractMemberWithOutAcess != null)
                    {
                        if (ValidateWithHolidays(contractMemberWithOutAcess.ActivityId, contractMemberWithOutAcess.ActivityName, ClosedTimes))//Validate with holidays 
                        {
                            if (CkeckForGymOpenTimes(openTimes))
                            {
                                if (ValidateMember(contractMemberWithOutAcess, setting, result))
                                {
                                    memberFound = true;
                                    result.OperationReturnValue = true;
                                    selectedMember = contractMemberWithOutAcess;
                                }
                                else
                                {
                                    result.OperationReturnValue = false;
                                }
                            }
                            else
                            {
                                result.OperationReturnValue = false;
                            }
                        }
                        else
                        {
                            result.OperationReturnValue = false;
                            NotificationMessage message = new NotificationMessage("Senteret er stengt for din aktivitet", MessageTypes.INFO);
                            if (!result.Notifications.Contains(message))
                            {
                                result.CreateMessage("Senteret er stengt for din aktivitet", MessageTypes.INFO);
                            }
                        }
                    }

                    if (memberFound == false && punchCardMemberWithNoAcces != null)
                    {
                        if (ValidateWithHolidays(punchCardMemberWithNoAcces.ActivityId, punchCardMemberWithNoAcces.ActivityName, ClosedTimes))//Validate with holidays 
                        {
                            if (CkeckForGymOpenTimes(openTimes))
                            {
                                if (ValidateMember(punchCardMemberWithNoAcces, setting, result))
                                {
                                    memberFound = true;
                                    result.OperationReturnValue = true;
                                    selectedMember = punchCardMemberWithNoAcces;
                                }
                                else
                                {
                                    result.OperationReturnValue = false;
                                }
                            }
                        }
                        else
                        {
                            result.OperationReturnValue = false;
                            NotificationMessage message = new NotificationMessage("Senteret er stengt for din aktivitet", MessageTypes.INFO);
                            if (!result.Notifications.Contains(message))
                            {
                                result.CreateMessage("Senteret er stengt for din aktivitet", MessageTypes.INFO);
                            }
                        }
                    }

                    if (contractMemberWithAcess == null && contractMemberWithOutAcess == null && punchCardMember == null && punchCardMemberWithNoAcces == null)
                    {
                        result.OperationReturnValue = false;
                        NotificationMessage message = new NotificationMessage("Ditt kortnummer kan ikke identifiseres", MessageTypes.INFO);
                        if (!result.Notifications.Contains(message))
                        {
                            result.CreateMessage("Ditt kortnummer kan ikke identifiseres", MessageTypes.INFO);
                        }
                    }
                }
                else
                {
                    result.OperationReturnValue = false;
                    NotificationMessage message = new NotificationMessage("Ditt kortnummer kan ikke identifiseres", MessageTypes.INFO);
                    if (!result.Notifications.Contains(message))
                    {
                        result.CreateMessage("Ditt kortnummer kan ikke identifiseres", MessageTypes.INFO);
                    }
                }

                //Add the visit
                if (result.OperationReturnValue)
                {
                    if (selectedMember != null)
                    {
                        EntityVisitDC entityVist = new EntityVisitDC();
                        entityVist.ActivityId = selectedMember.ActivityId;
                        entityVist.ArticleID = -1;
                        entityVist.BranchId = branchId;
                        entityVist.ContractNo = selectedMember.MemberContractNo;
                        entityVist.CountVist = true;
                        entityVist.CutomerNo = selectedMember.CustId;
                        entityVist.EntityId = selectedMember.Id;
                        entityVist.EntityName = selectedMember.Name;
                        entityVist.GymName = "";
                        entityVist.Id = -1;
                        entityVist.InTime = DateTime.Now;
                        entityVist.InvoiceRerefence = -1;
                        entityVist.ItemName = selectedMember.TemplateName;
                        entityVist.MemberContractId = selectedMember.MemberContractId;
                        entityVist.VisitDate = DateTime.Today;
                        entityVist.VisitType = "CON";
                        ManageMembershipFacade.SaveMemberVisit(entityVist, gymCode, user);
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static Dictionary<string, string> _smsMessage;
        public static OperationResult<ExceACCMember> ValidateMemberWithCardForExcAccessControl(string cardNo, string accessType, int terminalId, string gymCode, int branchId, string user)
        {
            _smsMessage = new Dictionary<string, string>();
            var result = new OperationResult<ExceACCMember>();
            bool isFisrtItemCheck = false;
            try
            {
                var members = ManageMembershipFacade.GetMemberDetailsForExcAccessController(cardNo, branchId, gymCode);
                var filterMemList = new List<ExceACCMember>();
                var terminal = OperationsManager.GetTerminalDetails(terminalId, gymCode);

                if (members != null && members.Count > 1 && terminal != null)
                {
                    if (terminal.OperationReturnValue.ActivityId > 0)
                    {
                        filterMemList = members.Where(x => x.ActivityId == terminal.OperationReturnValue.ActivityId).ToList();
                        if (!filterMemList.Any())
                            isFisrtItemCheck = true;
                    }

                    if (terminal.OperationReturnValue.AccessProfileIDList.Any())
                    {
                        if (filterMemList.Any())
                            filterMemList = filterMemList.Where(x => terminal.OperationReturnValue.AccessProfileIDList.Contains(x.AccessProfileId)).ToList();
                        else if (terminal.OperationReturnValue.ActivityId <= 0) filterMemList = members.Where(x => terminal.OperationReturnValue.AccessProfileIDList.Contains(x.AccessProfileId)).ToList();


                        if (!filterMemList.Any())
                            isFisrtItemCheck = true;
                    }
                }

                if (filterMemList.Any())
                    members = filterMemList;


                if (members != null && members.Count > 0)
                {
                    var setting = new ExceACCSettings();
                    var gender = Gender.ALL;
                    var firstItem = members.FirstOrDefault();
                    if (firstItem != null)
                        gender = (Gender)Enum.Parse(typeof(Gender), firstItem.Gender, true);

                    List<ActivityTimeDC> closedTimes = SystemSettingsFacade.GetActivityUnavailableTimes(branchId, gymCode);
                    List<ExceAccessProfileDC> accessProfiles = null; //SystemFacade.GetAccessProfiles(gymCode, gender);
                    List<GymOpenTimeDC> openTimes = null;// GymSettingFacade.GetGymOpenTimes(branchId, gymCode, gymCode);


                    foreach (var member in members)
                    {
                        setting.MinimumUnpaidInvoices = member.MinimumUnpaidInvoices;
                        setting.UnPaidDueBalance = member.UnPaidDueBalance;
                        setting.MinimumOnAccountBalance = member.MinimumOnAccountBalance;
                        setting.MinutesBetweenSwipes = member.MinutesBetweenSwipes;
                        setting.IsSendSms = member.IsSendSms;
                        setting.MinimumPunches = member.MinimumPunches;


                        _smsMessage = new Dictionary<string, string>();
                        if (member.ContractType == "CONTRACT" && member.AccessProfileId != -1)
                        {
                            result.OperationReturnValue = member;
                            if (ValidateWithHolidays(member.ActivityId, member.ActivityName, closedTimes))
                            {
                                accessProfiles = SystemFacade.GetAccessProfiles(gymCode, gender);
                                if (CheckForAccessTimesExcAc(branchId, member.Gender, member.AccessProfileId, accessProfiles))
                                {
                                    if (ValidateMemberForExcAccessController(member, setting, result))
                                    {
                                        if (terminal == null || terminal.OperationReturnValue.ActivityId <= 0 || (terminal.OperationReturnValue.ActivityId > 0 &&
                                             terminal.OperationReturnValue.ActivityId == member.ActivityId))
                                        {
                                            if (terminal == null || !terminal.OperationReturnValue.AccessProfileIDList.Any() || terminal.OperationReturnValue.AccessProfileIDList.Contains(member.AccessProfileId))
                                            {
                                                result.OperationReturnValue = member;
                                                result.OperationReturnValue.IsError = false;
                                                result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                                return result;
                                            }
                                            else
                                            {
                                                result.OperationReturnValue = member;
                                                result.OperationReturnValue.IsError = true;
                                                
                                                result.OperationReturnValue.MemberMessages = USGMSResources.AccessProfileValid;
                                                if (isFisrtItemCheck)
                                                {
                                                    result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                                    return result;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            result.OperationReturnValue = member;
                                            result.OperationReturnValue.IsError = true;
                                            // result.OperationReturnValue.Messages.Add(USGMSResources.ActivityValidate);
                                            result.OperationReturnValue.MemberMessages = USGMSResources.ActivityValidate;
                                            if (isFisrtItemCheck)
                                            {
                                                result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                                return result;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result.OperationReturnValue = member;
                                        result.OperationReturnValue.IsError = true;
                                        if (isFisrtItemCheck)
                                        {
                                            result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                            return result;
                                        }
                                    }
                                }
                                else
                                {
                                    result.OperationReturnValue.IsError = true;
                                    // result.OperationReturnValue.Messages.Add(USGMSResources.InvalidAccessTime);
                                    result.OperationReturnValue.MemberMessages = USGMSResources.InvalidAccessTime;
                                    if (isFisrtItemCheck)
                                    {
                                        result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                        return result;
                                    }
                                }
                            }
                            else
                            {
                                result.OperationReturnValue.IsError = true;
                                //  result.OperationReturnValue.Messages.Add(USGMSResources.ClosedfortheActivity);
                                result.OperationReturnValue.MemberMessages = USGMSResources.ClosedfortheActivity;
                                if (isFisrtItemCheck)
                                {
                                    result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                    return result;
                                }
                            }
                        }
                        else if (member.ContractType == "CONTRACT" && member.AccessProfileId == -1)
                        {
                            result.OperationReturnValue = member;
                            if (ValidateWithHolidays(member.ActivityId, member.ActivityName, closedTimes))//Validate with holidays 
                            {
                                openTimes = GymSettingFacade.GetGymOpenTimes(branchId, gymCode, gymCode);
                                if (CkeckForGymOpenTimes(openTimes))
                                {
                                    if (ValidateMemberForExcAccessController(member, setting, result))
                                    {
                                        if (terminal == null || terminal.OperationReturnValue.ActivityId <= 0 || (terminal.OperationReturnValue.ActivityId > 0 &&
                                             terminal.OperationReturnValue.ActivityId == member.ActivityId))
                                        {
                                            result.OperationReturnValue = member;
                                            result.OperationReturnValue.IsError = false;
                                            result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                            return result;
                                        }
                                        else
                                        {
                                            result.OperationReturnValue = member;
                                            result.OperationReturnValue.IsError = true;
                                            // result.OperationReturnValue.Messages.Add(USGMSResources.ActivityValidate);
                                            result.OperationReturnValue.MemberMessages = USGMSResources.ActivityValidate;
                                            if (isFisrtItemCheck)
                                            {
                                                result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                                return result;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result.OperationReturnValue = member;
                                        result.OperationReturnValue.IsError = true;
                                        // result.OperationReturnValue.Messages.Add(USGMSResources.ContractEndDateSmsMessage + " " + member.ContractEndDate);
                                        if (isFisrtItemCheck)
                                        {
                                            result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                            return result;
                                        }
                                    }
                                }
                                else
                                {
                                    result.OperationReturnValue.IsError = true;
                                    //result.OperationReturnValue.Messages.Add(USGMSResources.GymNotOpen);
                                    result.OperationReturnValue.MemberMessages = USGMSResources.GymNotOpen;
                                    if (isFisrtItemCheck)
                                    {
                                        result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                        return result;
                                    }
                                }
                            }
                            else
                            {
                                result.OperationReturnValue.IsError = true;
                                //result.OperationReturnValue.Messages.Add(USGMSResources.ClosedfortheActivity);
                                result.OperationReturnValue.MemberMessages = USGMSResources.ClosedfortheActivity;
                                if (isFisrtItemCheck)
                                {
                                    result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                    return result;
                                }
                            }
                        }
                        else if (member.ContractType == "PUNCHCARD" && member.AccessProfileId != -1)
                        {
                            // _smsMessage.Add("CONTRACTENDDATE", USGMSResources.ContractEndDateSmsMessage + " " + member.ContractEndDate);
                            result.OperationReturnValue = member;
                            if (ValidateWithHolidays(member.ActivityId, member.ActivityName, closedTimes))//Validate with holidays 
                            {
                                accessProfiles = SystemFacade.GetAccessProfiles(gymCode, gender);
                                if (CheckForAccessTimesExcAc(branchId, member.Gender, member.AccessProfileId, accessProfiles))
                                {
                                    if (ValidateMemberForExcAccessController(member, setting, result))
                                    {
                                        if (terminal == null || terminal.OperationReturnValue.ActivityId <= 0 || (terminal.OperationReturnValue.ActivityId > 0 &&
                                             terminal.OperationReturnValue.ActivityId == member.ActivityId))
                                        {
                                            if (terminal == null || !terminal.OperationReturnValue.AccessProfileIDList.Any() || terminal.OperationReturnValue.AccessProfileIDList.Contains(member.AccessProfileId))
                                            {
                                                result.OperationReturnValue = member;
                                                result.OperationReturnValue.IsError = false;
                                                result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                                return result;
                                            }
                                            else
                                            {
                                                result.OperationReturnValue = member;
                                                result.OperationReturnValue.IsError = true;
                                                //  result.OperationReturnValue.Messages.Add(USGMSResources.AccessProfileValid);
                                                result.OperationReturnValue.MemberMessages = USGMSResources.AccessProfileValid;
                                                if (isFisrtItemCheck)
                                                {
                                                    result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                                    return result;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            result.OperationReturnValue = member;
                                            result.OperationReturnValue.IsError = true;
                                            //result.OperationReturnValue.Messages.Add(USGMSResources.ActivityValidate);
                                            result.OperationReturnValue.MemberMessages = USGMSResources.ActivityValidate;
                                            if (isFisrtItemCheck)
                                            {
                                                result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                                return result;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result.OperationReturnValue = member;
                                        result.OperationReturnValue.IsError = true;
                                        if (isFisrtItemCheck)
                                        {
                                            result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                            return result;
                                        }
                                    }
                                }
                                else
                                {
                                    result.OperationReturnValue.IsError = true;
                                    //result.OperationReturnValue.Messages.Add(USGMSResources.InvalidAccessTime);
                                    result.OperationReturnValue.MemberMessages = USGMSResources.InvalidAccessTime;
                                    if (isFisrtItemCheck)
                                    {
                                        result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                        return result;
                                    }
                                }
                            }
                            else
                            {
                                result.OperationReturnValue.IsError = true;
                                //result.OperationReturnValue.Messages.Add(USGMSResources.ClosedfortheActivity);
                                result.OperationReturnValue.MemberMessages = USGMSResources.ClosedfortheActivity;
                                if (isFisrtItemCheck)
                                {
                                    result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                    return result;
                                }
                            }
                        }
                        else if (member.ContractType == "PUNCHCARD" && member.AccessProfileId == -1)
                        {
                            // _smsMessage.Add("CONTRACTENDDATE",USGMSResources.ContractEndDateSmsMessage + " " + member.ContractEndDate);
                            result.OperationReturnValue = member;
                            if (ValidateWithHolidays(member.ActivityId, member.ActivityName, closedTimes))
                            //Validate with holidays 
                            {
                                openTimes = GymSettingFacade.GetGymOpenTimes(branchId, gymCode, gymCode);
                                if (CkeckForGymOpenTimes(openTimes))
                                {
                                    if (ValidateMemberForExcAccessController(member, setting, result))
                                    {
                                        if (terminal == null || terminal.OperationReturnValue.ActivityId <= 0 ||
                                            (terminal.OperationReturnValue.ActivityId > 0 &&
                                             terminal.OperationReturnValue.ActivityId == member.ActivityId))
                                        {
                                            result.OperationReturnValue = member;
                                            result.OperationReturnValue.IsError = false;
                                            result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                            return result;
                                        }
                                        else
                                        {
                                            result.OperationReturnValue = member;
                                            result.OperationReturnValue.IsError = true;
                                            // result.OperationReturnValue.Messages.Add(USGMSResources.ActivityValidate);
                                            result.OperationReturnValue.MemberMessages = USGMSResources.ActivityValidate;
                                            if (isFisrtItemCheck)
                                            {
                                                result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                                return result;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result.OperationReturnValue = member;
                                        result.OperationReturnValue.IsError = true;
                                        if (isFisrtItemCheck)
                                        {
                                            result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                            return result;
                                        }
                                    }
                                }
                                else
                                {
                                    result.OperationReturnValue.IsError = true;
                                    // result.OperationReturnValue.Messages.Add(USGMSResources.GymNotOpen);
                                    result.OperationReturnValue.MemberMessages = USGMSResources.GymNotOpen;
                                    if (isFisrtItemCheck)
                                    {
                                        result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                        return result;
                                    }
                                }
                            }
                            else
                            {
                                result.OperationReturnValue.IsError = true;
                                //result.OperationReturnValue.Messages.Add(USGMSResources.ClosedfortheActivity);
                                result.OperationReturnValue.MemberMessages = USGMSResources.ClosedfortheActivity;
                                if (isFisrtItemCheck)
                                {
                                    result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                    return result;
                                }
                            }
                        }
                        else
                        {

                            var unknownMember = new ExceACCMember { Id = -1 };
                            // unknownMember.Messages.Add(USGMSResources.CardNotIdentified);
                            unknownMember.MemberMessages = USGMSResources.CardNotIdentified;
                            result.OperationReturnValue = unknownMember;
                            result.OperationReturnValue.IsError = true;
                            if (isFisrtItemCheck)
                            {
                                result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                                return result;
                            }
                        }
                    }
                    //result.OperationReturnValue.SmsMessages = setting.IsSendSms ? _smsMessage : null;
                }
                else
                {
                    var unknownMember = new ExceACCMember { Id = -1 };
                    //  unknownMember.Messages.Add(USGMSResources.CardNotIdentified);
                    unknownMember.MemberMessages = USGMSResources.CardNotIdentified;
                    result.OperationReturnValue = unknownMember;
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static bool ValidateMemberForExcAccessController(ExceACCMember contractMemberWithAcess, ExceACCSettings settings, OperationResult<ExceACCMember> result)
        {
            if (contractMemberWithAcess.IsFreezed == "True")
            {
                _smsMessage.Add("FREEZE", USGMSResources.FreezeSmsMessage + " " + contractMemberWithAcess.FreezeToDate);
                // contractMemberWithAcess.Messages.Add(USGMSResources.ContractFreezed);
                contractMemberWithAcess.MemberMessages = USGMSResources.ContractFreezed;
                return false;
            }
            else
            {
                if (contractMemberWithAcess.IsContractATG == "True" && contractMemberWithAcess.MemberATGStatus != "6")
                {
                    // _smsMessage.Add("ATGNOTAPPROVED", USGMSResources.AtgSmsMessage);
                    contractMemberWithAcess.Messages.Add(USGMSResources.GetATGApproved);
                }

                if (string.IsNullOrEmpty(contractMemberWithAcess.AntiDopingDate))
                {
                    //  _smsMessage.Add("ANTIDOPING", USGMSResources.AntiDopingSmsMessage);
                    contractMemberWithAcess.Messages.Add(USGMSResources.AntidopeSign);
                }

                if (settings != null)
                {
                    if (contractMemberWithAcess.AvailableVisits < settings.MinimumPunches && contractMemberWithAcess.ContractType == "PUNCHCARD" && settings.MinimumPunches != 0)
                    {
                        //_smsMessage.Add("PUNCHESLOWESTNO", USGMSResources.PuncheslowestSmsMessage + " " + contractMemberWithAcess.AvailableVisits + " " + USGMSResources.punchesleft);
                        contractMemberWithAcess.Messages.Add(USGMSResources.YouHave + " " + contractMemberWithAcess.AvailableVisits + " " + USGMSResources.VisitsRemain);
                    }

                    if (contractMemberWithAcess.CreditBalance > settings.UnPaidDueBalance && settings.UnPaidDueBalance > 0 && contractMemberWithAcess.CreditBalance != 0)
                    {
                        _smsMessage.Add("DUEBALANCE", USGMSResources.DueBalanceSmsMessage + " " + contractMemberWithAcess.CreditBalance);
                        // contractMemberWithAcess.Messages.Add(USGMSResources.PayLastInvoice);
                        contractMemberWithAcess.MemberMessages = USGMSResources.PayLastInvoice;
                        return false;
                    }

                    if (contractMemberWithAcess.ShopAccBalance < settings.MinimumOnAccountBalance && settings.MinimumOnAccountBalance > 0 && contractMemberWithAcess.ShopAccBalance != 0)
                    {
                        _smsMessage.Add("DUEONACCOUNT", USGMSResources.DueOnAccountSmsMessage);
                        //contractMemberWithAcess.Messages.Add(USGMSResources.ShopBalanceAxceeded);
                        contractMemberWithAcess.MemberMessages = USGMSResources.ShopBalanceAxceeded;
                        return false;
                    }

                    if (contractMemberWithAcess.UnPaidInvoiceCount >= settings.MinimumUnpaidInvoices && settings.MinimumUnpaidInvoices > 0)
                    {
                        _smsMessage.Add("UPPAIDINVOICE", USGMSResources.Unpaidinvoice);
                        // contractMemberWithAcess.Messages.Add(USGMSResources.PayLastInvoice);
                        contractMemberWithAcess.MemberMessages = USGMSResources.Unpaidinvoice;
                        return false;
                    }
                }

                if (!(contractMemberWithAcess.AvailableVisits > 0) && contractMemberWithAcess.ContractType == "PUNCHCARD")
                {
                    // contractMemberWithAcess.Messages.Add(USGMSResources.NoVisitsAvailable);
                    contractMemberWithAcess.MemberMessages = USGMSResources.NoVisitsAvailable;
                    return false;
                }
            }
            contractMemberWithAcess.MemberMessages = USGMSResources.AccessGranted;
            return true;
        }

        private static bool ValidateMember(ExceACCMember contractMemberWithAcess, ExceACCSettings settings, OperationResult<bool> result)
        {
            if (contractMemberWithAcess.IsFreezed == "True")
            {
                NotificationMessage message = new NotificationMessage("Kontrakten er i fryse", MessageTypes.INFO);
                if (!result.Notifications.Contains(message))
                {
                    result.CreateMessage("Kontrakten er i fryse", MessageTypes.INFO);
                }
                result.OperationReturnValue = false;
            }
            else
            {
                if (contractMemberWithAcess.CreditBalance > settings.UnPaidDueBalance && settings.UnPaidDueBalance != 0 && contractMemberWithAcess.CreditBalance != 0)
                {
                    NotificationMessage message = new NotificationMessage("Vennligst betal for eldre fakturaer", MessageTypes.INFO);
                    if (!result.Notifications.Contains(message))
                    {
                        result.CreateMessage("Vennligst betal for eldre fakturaer", MessageTypes.INFO);
                    }
                    result.OperationReturnValue = false;
                }

                if (contractMemberWithAcess.ShopAccBalance < settings.MinimumOnAccountBalance && settings.UnPaidDueBalance != 0 && contractMemberWithAcess.CreditBalance != 0)
                {
                    NotificationMessage message = new NotificationMessage("Din kredittgrense i shop er overskredet", MessageTypes.INFO);
                    if (!result.Notifications.Contains(message))
                    {
                        result.CreateMessage("Din kredittgrense i shop er overskredet", MessageTypes.INFO);
                    }
                    result.OperationReturnValue = false;
                }
                if (!(contractMemberWithAcess.AvailableVisits > 0) && contractMemberWithAcess.ContractType == "PUNCHCARD")
                {
                    NotificationMessage message = new NotificationMessage("ingen besøk i butikken", MessageTypes.INFO);
                    if (!result.Notifications.Contains(message))
                    {
                        result.CreateMessage("ingen besøk i butikken", MessageTypes.INFO);
                    }
                    result.OperationReturnValue = false;
                }
            }
            return true;
        }
        
        private bool ValidateWithAccessProfile(ExceACCTerminal exceAccTerminal, int accessProfileId)
        {
            return exceAccTerminal.AccessProfileIDList != null && exceAccTerminal.AccessProfileIDList.Contains(accessProfileId);
        }
        
        private static bool ValidateWithHolidays(int activityId, string activityName, List<ActivityTimeDC> unavailableTimes)
        {
            foreach (ActivityTimeDC closedTime in unavailableTimes)
            {
                if (DateTime.Now.Date >= closedTime.Startdate && DateTime.Now.Date <= closedTime.EndDate && DateTime.Now.TimeOfDay >= closedTime.StartTime.Value.TimeOfDay && DateTime.Now.TimeOfDay <= closedTime.EndTime.Value.TimeOfDay && closedTime.ActivityId == activityId)
                {
                    _smsMessage.Add("CLOSEDPERIOD", USGMSResources.ClosedPeriodSmsMesssage + " " + activityName + " " + USGMSResources.until + " " + closedTime.EndDate.Value.Date.ToString("dd/MM/yyyy"));
                    return false;
                }
            }
            return true;
        }

        private static bool CheckForAccessTimesExcAc(int branchId, string gender, int accessProfileId, List<ExceAccessProfileDC> accessProfiles)
        {
            ExceAccessProfileDC selecteddAccessTime = accessProfiles.FirstOrDefault(X => X.Id == accessProfileId);
            if (selecteddAccessTime != null)
            {
                List<ExceAccessProfileTimeDC> validTimes = new List<ExceAccessProfileTimeDC>();
                if (!selecteddAccessTime.BranchIdList.Any(x => x.Equals(branchId)))
                {
                    _smsMessage.Add("OUTSIDEPROFILE", USGMSResources.OutsideProfileSmsMessage);
                    return false;
                }
                if (selecteddAccessTime.Gender.ToString().ToLower() != gender.ToLower() && gender.ToLower() != "all" && gender.ToLower() != "none" && selecteddAccessTime.Gender.ToString().ToLower() != "all")
                {
                    _smsMessage.Add("OUTSIDEPROFILE", USGMSResources.OutsideProfileSmsMessage);
                    return false;
                }

                switch (DateTime.Today.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Sunday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Monday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Monday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Tuesday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Tuesday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Wednesday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Wednesday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Thursday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Thursday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Friday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Friday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Saturday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Saturday).ToList<ExceAccessProfileTimeDC>();
                        break;
                }

                if (validTimes.Count > 0)
                {
                    foreach (ExceAccessProfileTimeDC validTime in validTimes)
                    {
                        bool validateResult = HandleDayAccessExcAc(validTime.FromTime.Value, validTime.ToTime.Value);
                        if (validateResult)
                        {
                            return true;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    _smsMessage.Add("OUTSIDEPROFILE", USGMSResources.OutsideProfileSmsMessage);
                    return false;
                }
                else
                {
                    _smsMessage.Add("OUTSIDEPROFILE", USGMSResources.OutsideProfileSmsMessage);
                    return false;
                }
            }
            else
            {
                _smsMessage.Add("OUTSIDEPROFILE", USGMSResources.OutsideProfileSmsMessage);
                return false;
            }

        }

        private static bool HandleDayAccessExcAc(DateTime StartTime, DateTime EndTime)
        {
            int minitesToHave = (StartTime.TimeOfDay - DateTime.Now.TimeOfDay).Minutes;
            if ((StartTime.TimeOfDay <= DateTime.Now.TimeOfDay && EndTime.TimeOfDay > DateTime.Now.TimeOfDay) || (minitesToHave < 10 && minitesToHave > 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool CheckForAccessTimes(int accessProfileId, List<ExceAccessProfileDC> accessProfiles)
        {
            ExceAccessProfileDC selecteddAccessTime = accessProfiles.FirstOrDefault(X => X.Id == accessProfileId);
            if (selecteddAccessTime != null)
            {
                List<ExceAccessProfileTimeDC> validTimes = new List<ExceAccessProfileTimeDC>();
                switch (DateTime.Today.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Sunday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Monday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Monday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Tuesday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Tuesday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Wednesday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Wednesday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Thursday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Thursday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Friday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Friday).ToList<ExceAccessProfileTimeDC>();
                        break;

                    case DayOfWeek.Saturday:
                        validTimes = selecteddAccessTime.AccessTimeList.Where(X => X.Saturday).ToList<ExceAccessProfileTimeDC>();
                        break;
                }

                if (validTimes.Count > 0)
                {
                    foreach (ExceAccessProfileTimeDC validTime in validTimes)
                    {
                        return HandleDayAccess(validTime.FromTime.Value, validTime.ToTime.Value);
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        private static bool HandleDayAccess(DateTime StartTime, DateTime EndTime)
        {
            int minitesToHave = (StartTime.TimeOfDay - DateTime.Now.TimeOfDay).Minutes;
            if ((StartTime.TimeOfDay <= DateTime.Now.TimeOfDay && EndTime.TimeOfDay > DateTime.Now.TimeOfDay) || (minitesToHave < 10 && minitesToHave > 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool CkeckForGymOpenTimes(List<GymOpenTimeDC> openTimes)
        {
            GymOpenTimeDC todayOpenDetails = null;
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    todayOpenDetails = openTimes.FirstOrDefault(X => X.IsSunday);
                    break;

                case DayOfWeek.Monday:
                    todayOpenDetails = openTimes.FirstOrDefault(X => X.IsMonday);
                    break;

                case DayOfWeek.Tuesday:
                    todayOpenDetails = openTimes.FirstOrDefault(X => X.IsTuesday);
                    break;

                case DayOfWeek.Wednesday:
                    todayOpenDetails = openTimes.FirstOrDefault(X => X.IsWednesday);
                    break;

                case DayOfWeek.Thursday:
                    todayOpenDetails = openTimes.FirstOrDefault(X => X.IsThursday);
                    break;

                case DayOfWeek.Friday:
                    todayOpenDetails = openTimes.FirstOrDefault(X => X.IsFriday);
                    break;

                case DayOfWeek.Saturday:
                    todayOpenDetails = openTimes.FirstOrDefault(X => X.IsSaturday);
                    break;
            }

            if (todayOpenDetails != null)
            {
                if (todayOpenDetails.StartTime.Value.TimeOfDay <= DateTime.Now.TimeOfDay && todayOpenDetails.EndTime.Value.TimeOfDay > DateTime.Now.TimeOfDay)
                {
                    return true;
                }
                else
                {
                    _smsMessage.Add("CLOSEDOUTSIDEOPENHOURS", USGMSResources.GymOpenTimeSmsMessage + " " + todayOpenDetails.StartTime.Value.TimeOfDay.ToString() + " " + USGMSResources.till + " " + todayOpenDetails.EndTime.Value.TimeOfDay.ToString());
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static OperationResult<List<InstallmentDC>> GetSponsorOrders(List<int> sponsors, string gymCode)
        {
            OperationResult<List<InstallmentDC>> result = new OperationResult<List<InstallmentDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetSponsorOrders(sponsors, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Orders for sponsors  " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<SaleResultDC> AddSunBedShopSale(SunBedBusinessHelperObject businessHelperObj)
        {
            OperationResult<SaleResultDC> result = new OperationResult<SaleResultDC>();
            SunBedHelperObject sunBedHelper = new SunBedHelperObject();
            sunBedHelper.GatPuchaseShopItem = businessHelperObj.GatPurchaseShopItem;
            sunBedHelper.GymCode = businessHelperObj.GymCode;
            sunBedHelper.MemberBranchID = businessHelperObj.MemberBranchId;
            sunBedHelper.LoggedbranchID = businessHelperObj.BranchId;
            sunBedHelper.IsBookingPayment = false;
            sunBedHelper.User = businessHelperObj.User;

            switch (businessHelperObj.PaidAccessMode)
            {
                case 1:
                    //direct on account sale
                    InstallmentDC installment = new InstallmentDC();
                    installment.MemberId = businessHelperObj.MemberId;
                    installment.InvoiceGeneratedDate = DateTime.Now;
                    installment.InstallmentDate = DateTime.Now;
                    installment.DueDate = DateTime.Now;
                    installment.Amount = businessHelperObj.Amount;
                    installment.AddOnList = new List<ContractItemDC>();

                    ContractItemDC article = new ContractItemDC();
                    article.ArticleId = businessHelperObj.ArticleNo;
                    article.CategoryId = businessHelperObj.ArticleCategoryId;
                    article.ItemName = businessHelperObj.ArticleName;
                    article.Price = businessHelperObj.Amount;
                    article.IsActivityArticle = false;
                    installment.AddOnList.Add(article);

                    PaymentDetailDC paymentDetails = new PaymentDetailDC();
                    paymentDetails.PayModes = new List<PayModeDC>();
                    paymentDetails.PaidAmount = businessHelperObj.Amount;
                    PayModeDC payMode = new PayModeDC();
                    payMode.Amount = businessHelperObj.Amount;
                    payMode.PaidDate = DateTime.Now;
                    payMode.PaymentType = "ONACCOUNT";
                    payMode.PaymentTypeCode = "ONACCOUNT";
                    paymentDetails.PayModes.Add(payMode);

                    ShopSalesDC shopSalesDC = new ShopSalesDC();
                    shopSalesDC.EntitiyId = businessHelperObj.MemberId;
                    shopSalesDC.SalesPointId = businessHelperObj.SalepointId;
                    shopSalesDC.EntityRoleType = "MEM";

                    shopSalesDC.ShopSalesItemList = new List<ShopSalesItemDC>();
                    ShopSalesItemDC saleItem = new ShopSalesItemDC();
                    saleItem.Amount = businessHelperObj.Amount;
                    saleItem.ArticleId = businessHelperObj.ArticleNo;
                    saleItem.Discount = 0;
                    saleItem.ItemName = businessHelperObj.ArticleName;
                    saleItem.Quantity = 1;
                    saleItem.ItemCategory = "ITEM";
                    saleItem.TotalAmount = businessHelperObj.Amount;
                    shopSalesDC.ShopSalesItemList.Add(saleItem);

                    sunBedHelper.Installments.Add(installment);
                    sunBedHelper.PaymentDetails.Add(paymentDetails);
                    sunBedHelper.ShopSaleDetails.Add(shopSalesDC);

                    result.OperationReturnValue = ShopManager.AddSunBedShopSales(sunBedHelper, businessHelperObj).OperationReturnValue;

                    break;
                case 2:
                    if (businessHelperObj.PaymentOrderStatus == GatPurchasePaymentStatus.ONACCOUNTANDNEXTORDER)
                    {
                        InstallmentDC OnAccountInstallment = new InstallmentDC();
                        OnAccountInstallment.MemberId = businessHelperObj.MemberId;
                        OnAccountInstallment.InvoiceGeneratedDate = DateTime.Now;
                        OnAccountInstallment.InstallmentDate = DateTime.Now;
                        OnAccountInstallment.DueDate = DateTime.Now;
                        OnAccountInstallment.Amount = businessHelperObj.OnAccountPaymentAmount;
                        OnAccountInstallment.AddOnList = new List<ContractItemDC>();

                        ContractItemDC OnAccountArticle = new ContractItemDC();
                        OnAccountArticle.ArticleId = businessHelperObj.ArticleNo;
                        OnAccountArticle.CategoryId = businessHelperObj.ArticleCategoryId;
                        OnAccountArticle.ItemName = businessHelperObj.ArticleName;
                        OnAccountArticle.Price = businessHelperObj.OnAccountPaymentAmount;
                        OnAccountArticle.IsActivityArticle = false;
                        OnAccountInstallment.AddOnList.Add(OnAccountArticle);

                        PaymentDetailDC onAccountPaymentDetails = new PaymentDetailDC();
                        onAccountPaymentDetails.PayModes = new List<PayModeDC>();
                        onAccountPaymentDetails.PaidAmount = businessHelperObj.OnAccountPaymentAmount;
                        PayModeDC onAccountPayMode = new PayModeDC();
                        onAccountPayMode.Amount = businessHelperObj.OnAccountPaymentAmount;
                        onAccountPayMode.PaidDate = DateTime.Now;
                        onAccountPayMode.PaymentType = "ONACCOUNT";
                        onAccountPayMode.PaymentTypeCode = "ONACCOUNT";
                        onAccountPaymentDetails.PayModes.Add(onAccountPayMode);

                        ShopSalesDC onAccountShopSalesDC = new ShopSalesDC();
                        onAccountShopSalesDC.EntitiyId = businessHelperObj.MemberId;
                        onAccountShopSalesDC.SalesPointId = businessHelperObj.SalepointId;
                        onAccountShopSalesDC.EntityRoleType = "MEM";

                        onAccountShopSalesDC.ShopSalesItemList = new List<ShopSalesItemDC>();
                        ShopSalesItemDC onAccountSaleItem = new ShopSalesItemDC();
                        onAccountSaleItem.Amount = businessHelperObj.OnAccountPaymentAmount;
                        onAccountSaleItem.ArticleId = businessHelperObj.ArticleNo;
                        onAccountSaleItem.Discount = 0;
                        onAccountSaleItem.ItemName = businessHelperObj.ArticleName;
                        onAccountSaleItem.Quantity = 1;
                        onAccountSaleItem.ItemCategory = "ITEM";
                        onAccountSaleItem.TotalAmount = businessHelperObj.OnAccountPaymentAmount;
                        onAccountShopSalesDC.ShopSalesItemList.Add(onAccountSaleItem);

                        sunBedHelper.OnlyShopOrder = businessHelperObj.OnlyShopOrder;
                        sunBedHelper.Installments.Add(OnAccountInstallment);
                        sunBedHelper.PaymentDetails.Add(onAccountPaymentDetails);
                        sunBedHelper.ShopSaleDetails.Add(onAccountShopSalesDC);

                        // add next order installment
                        InstallmentDC nosInstallment = new InstallmentDC();
                        nosInstallment.MemberId = businessHelperObj.MemberId;

                        ContractItemDC nosArticle = new ContractItemDC();
                        nosArticle.ArticleId = businessHelperObj.ArticleNo;
                        nosArticle.CategoryId = businessHelperObj.ArticleCategoryId;
                        nosArticle.ItemName = businessHelperObj.ArticleName;
                        nosArticle.Price = businessHelperObj.NextOrderPaymentAmount;
                        nosArticle.IsActivityArticle = false;
                        nosInstallment.AddOnList.Add(nosArticle);

                        PaymentDetailDC nosPaymentDetails = new PaymentDetailDC();
                        nosPaymentDetails.PayModes = new List<PayModeDC>();
                        PayModeDC nosPayMode = new PayModeDC();
                        nosPayMode.Amount = businessHelperObj.NextOrderPaymentAmount;
                        nosPayMode.PaidDate = DateTime.Now;
                        nosPayMode.PaymentType = "NEXTORDER";
                        nosPayMode.PaymentTypeCode = "NEXTORDER";
                        nosPaymentDetails.PayModes.Add(nosPayMode);

                        ShopSalesDC nosShopSalesDC = new ShopSalesDC();
                        nosShopSalesDC.EntitiyId = businessHelperObj.MemberId;
                        nosShopSalesDC.SalesPointId = businessHelperObj.SalepointId;

                        nosShopSalesDC.EntityRoleType = "MEM";

                        nosShopSalesDC.ShopSalesItemList = new List<ShopSalesItemDC>();
                        ShopSalesItemDC nosSaleItem = new ShopSalesItemDC();
                        nosSaleItem.Amount = businessHelperObj.NextOrderPaymentAmount;
                        nosSaleItem.ArticleId = businessHelperObj.ArticleNo;
                        nosSaleItem.Discount = 0;
                        nosSaleItem.ItemName = businessHelperObj.ArticleName;
                        nosSaleItem.Quantity = 1;
                        nosSaleItem.ItemCategory = "ITEM";
                        nosSaleItem.TotalAmount = businessHelperObj.NextOrderPaymentAmount;
                        nosShopSalesDC.ShopSalesItemList.Add(nosSaleItem);

                        sunBedHelper.OnlyShopOrder = businessHelperObj.OnlyShopOrder;
                        sunBedHelper.Installments.Add(nosInstallment);
                        sunBedHelper.PaymentDetails.Add(nosPaymentDetails);
                        sunBedHelper.ShopSaleDetails.Add(nosShopSalesDC);

                        result.OperationReturnValue = ShopManager.AddSunBedShopSales(sunBedHelper, businessHelperObj).OperationReturnValue;

                    }
                    else if (businessHelperObj.PaymentOrderStatus == GatPurchasePaymentStatus.ONACCOUNTONLY)
                    {
                        InstallmentDC OOInstallment = new InstallmentDC();
                        OOInstallment.MemberId = businessHelperObj.MemberId;
                        OOInstallment.InvoiceGeneratedDate = DateTime.Now;
                        OOInstallment.InstallmentDate = DateTime.Now;
                        OOInstallment.DueDate = DateTime.Now;
                        OOInstallment.Amount = businessHelperObj.Amount;
                        OOInstallment.AddOnList = new List<ContractItemDC>();

                        ContractItemDC OOArticle = new ContractItemDC();
                        OOArticle.ArticleId = businessHelperObj.ArticleNo;
                        OOArticle.CategoryId = businessHelperObj.ArticleCategoryId;
                        OOArticle.ItemName = businessHelperObj.ArticleName;
                        OOArticle.Price = businessHelperObj.Amount;
                        OOArticle.IsActivityArticle = false;
                        OOInstallment.AddOnList.Add(OOArticle);

                        PaymentDetailDC OOPaymentDetails = new PaymentDetailDC();
                        OOPaymentDetails.PayModes = new List<PayModeDC>();
                        OOPaymentDetails.PaidAmount = businessHelperObj.Amount;
                        PayModeDC OOPayMode = new PayModeDC();
                        OOPayMode.Amount = businessHelperObj.Amount;
                        OOPayMode.PaidDate = DateTime.Now;
                        OOPayMode.PaymentType = "ONACCOUNT";
                        OOPayMode.PaymentTypeCode = "ONACCOUNT";
                        OOPaymentDetails.PayModes.Add(OOPayMode);

                        ShopSalesDC OOShopSalesDC = new ShopSalesDC();
                        OOShopSalesDC.EntitiyId = businessHelperObj.MemberId;
                        OOShopSalesDC.SalesPointId = businessHelperObj.SalepointId;
                        OOShopSalesDC.EntityRoleType = "MEM";

                        OOShopSalesDC.ShopSalesItemList = new List<ShopSalesItemDC>();
                        ShopSalesItemDC OOSaleItem = new ShopSalesItemDC();
                        OOSaleItem.Amount = businessHelperObj.Amount;
                        OOSaleItem.ArticleId = businessHelperObj.ArticleNo;
                        OOSaleItem.Discount = 0;
                        OOSaleItem.ItemName = businessHelperObj.ArticleName;
                        OOSaleItem.Quantity = 1;
                        OOSaleItem.ItemCategory = "ITEM";
                        OOSaleItem.TotalAmount = businessHelperObj.Amount;
                        OOShopSalesDC.ShopSalesItemList.Add(OOSaleItem);

                        sunBedHelper.Installments.Add(OOInstallment);
                        sunBedHelper.PaymentDetails.Add(OOPaymentDetails);
                        sunBedHelper.ShopSaleDetails.Add(OOShopSalesDC);

                        result.OperationReturnValue = ShopManager.AddSunBedShopSales(sunBedHelper, businessHelperObj).OperationReturnValue;
                    }
                    else if (businessHelperObj.PaymentOrderStatus == GatPurchasePaymentStatus.NEXTORDERONLY)
                    {
                        InstallmentDC nextOrderinstallment = new InstallmentDC();
                        nextOrderinstallment.MemberId = businessHelperObj.MemberId;

                        ContractItemDC nextArticle = new ContractItemDC();
                        nextArticle.ArticleId = businessHelperObj.ArticleNo;
                        nextArticle.CategoryId = businessHelperObj.ArticleCategoryId;
                        nextArticle.ItemName = businessHelperObj.ArticleName;
                        nextArticle.Price = businessHelperObj.Amount;
                        nextArticle.IsActivityArticle = false;
                        nextOrderinstallment.AddOnList.Add(nextArticle);

                        PaymentDetailDC nextOrderPaymentDetails = new PaymentDetailDC();
                        nextOrderPaymentDetails.PayModes = new List<PayModeDC>();
                        PayModeDC nextOrderPayMode = new PayModeDC();
                        nextOrderPayMode.Amount = businessHelperObj.Amount;
                        nextOrderPayMode.PaidDate = DateTime.Now;
                        nextOrderPayMode.PaymentType = "NEXTORDER";
                        nextOrderPayMode.PaymentTypeCode = "NEXTORDER";
                        nextOrderPaymentDetails.PayModes.Add(nextOrderPayMode);

                        ShopSalesDC nextOrderShopSalesDC = new ShopSalesDC();
                        nextOrderShopSalesDC.EntitiyId = businessHelperObj.MemberId;
                        nextOrderShopSalesDC.SalesPointId = businessHelperObj.SalepointId;
                        nextOrderShopSalesDC.EntityRoleType = "MEM";

                        nextOrderShopSalesDC.ShopSalesItemList = new List<ShopSalesItemDC>();
                        ShopSalesItemDC nextOrderSaleItem = new ShopSalesItemDC();
                        nextOrderSaleItem.Amount = businessHelperObj.Amount;
                        nextOrderSaleItem.ArticleId = businessHelperObj.ArticleNo;
                        nextOrderSaleItem.Discount = 0;
                        nextOrderSaleItem.ItemName = businessHelperObj.ArticleName;
                        nextOrderSaleItem.Quantity = 1;
                        nextOrderSaleItem.ItemCategory = "ITEM";
                        nextOrderSaleItem.TotalAmount = businessHelperObj.Amount;
                        nextOrderShopSalesDC.ShopSalesItemList.Add(nextOrderSaleItem);

                        sunBedHelper.OnlyShopOrder = businessHelperObj.OnlyShopOrder;
                        sunBedHelper.Installments.Add(nextOrderinstallment);
                        sunBedHelper.PaymentDetails.Add(nextOrderPaymentDetails);
                        sunBedHelper.ShopSaleDetails.Add(nextOrderShopSalesDC);

                        result.OperationReturnValue = ShopManager.AddSunBedShopSales(sunBedHelper, businessHelperObj).OperationReturnValue;
                    }
                    break;
                case 3:
                    //direct Next Order sale
                    InstallmentDC nextInstallment = new InstallmentDC();
                    nextInstallment.MemberId = businessHelperObj.MemberId;

                    ContractItemDC nextOrderArticle = new ContractItemDC();
                    nextOrderArticle.ArticleId = businessHelperObj.ArticleNo;
                    nextOrderArticle.CategoryId = businessHelperObj.ArticleCategoryId;
                    nextOrderArticle.ItemName = businessHelperObj.ArticleName;
                    nextOrderArticle.Price = businessHelperObj.Amount;
                    nextOrderArticle.IsActivityArticle = false;
                    nextInstallment.AddOnList.Add(nextOrderArticle);

                    PaymentDetailDC nextPaymentDetails = new PaymentDetailDC();
                    nextPaymentDetails.PayModes = new List<PayModeDC>();
                    PayModeDC nextPayMode = new PayModeDC();
                    nextPayMode.Amount = businessHelperObj.Amount;
                    nextPayMode.PaidDate = DateTime.Now;
                    nextPayMode.PaymentType = "NEXTORDER";
                    nextPayMode.PaymentTypeCode = "NEXTORDER";
                    nextPaymentDetails.PayModes.Add(nextPayMode);

                    ShopSalesDC nextShopSalesDC = new ShopSalesDC();
                    nextShopSalesDC.EntitiyId = businessHelperObj.MemberId;
                    nextShopSalesDC.SalesPointId = businessHelperObj.SalepointId;
                    nextShopSalesDC.EntityRoleType = "MEM";

                    nextShopSalesDC.ShopSalesItemList = new List<ShopSalesItemDC>();
                    ShopSalesItemDC nextSaleItem = new ShopSalesItemDC();
                    nextSaleItem.Amount = businessHelperObj.Amount;
                    nextSaleItem.ArticleId = businessHelperObj.ArticleNo;
                    nextSaleItem.Discount = 0;
                    nextSaleItem.ItemName = businessHelperObj.ArticleName;
                    nextSaleItem.Quantity = 1;
                    nextSaleItem.ItemCategory = "ITEM";
                    nextSaleItem.TotalAmount = businessHelperObj.Amount;
                    nextShopSalesDC.ShopSalesItemList.Add(nextSaleItem);

                    sunBedHelper.Installments.Add(nextInstallment);
                    sunBedHelper.PaymentDetails.Add(nextPaymentDetails);
                    sunBedHelper.ShopSaleDetails.Add(nextShopSalesDC);

                    result.OperationReturnValue = ShopManager.AddSunBedShopSales(sunBedHelper, businessHelperObj).OperationReturnValue;

                    break;
            }
            return result;
        }

        public static OperationResult<int> AddMachineSale(int articleNo, int memberId, string articleName, decimal amount, string user, string gymCode, int brachId, int salepointId)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                InstallmentDC installment = new InstallmentDC();
                installment.MemberId = memberId;

                PaymentDetailDC paymentDetails = new PaymentDetailDC();
                paymentDetails.PayModes = new List<PayModeDC>();
                PayModeDC payMode = new PayModeDC();
                payMode.Amount = amount;
                payMode.PaidDate = DateTime.Now;
                payMode.PaymentType = "NEXTORDER";
                payMode.PaymentTypeCode = "NEXTORDER";
                paymentDetails.PayModes.Add(payMode);

                ShopSalesDC shopSalesDC = new ShopSalesDC();
                shopSalesDC.EntitiyId = memberId;
                shopSalesDC.SalesPointId = salepointId;
                shopSalesDC.EntityRoleType = "MEM";

                shopSalesDC.ShopSalesItemList = new List<ShopSalesItemDC>();
                ShopSalesItemDC saleItem = new ShopSalesItemDC();
                saleItem.Amount = amount;
                saleItem.ArticleId = articleNo;
                saleItem.Discount = 0;
                saleItem.ItemName = articleName;
                saleItem.Quantity = 1;
                saleItem.ItemCategory = "ITEM";
                saleItem.TotalAmount = amount;
                shopSalesDC.ShopSalesItemList.Add(saleItem);
                SaleResultDC saleResult = ShopManager.AddShopSales("", installment, shopSalesDC, paymentDetails, brachId, brachId, user, gymCode, false, "").OperationReturnValue;
                result.OperationReturnValue = saleResult.AritemNo;
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Add machine Sale  " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<MemberIntegrationSettingDC>> GetMemberIntegrationSettings(int branchId, int memberId, string gymCode)
        {
            OperationResult<List<MemberIntegrationSettingDC>> result = new OperationResult<List<MemberIntegrationSettingDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetMemberIntegrationSettings(branchId, memberId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Member Integration Settings " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> WellnessIntegrationOperation(OrdinaryMemberDC member, MemberIntegrationSettingDC wellnessIntegrationSetting, WellnessOperationType wellnessOperationType, string gymCode, int branchId, string user, EntityVisitDC memberVisit, TrainingProgramClassVisitDetailDC classVisitDetail)
        {
            OperationResult<bool> successResult = new OperationResult<bool>();
            try
            {
            OtherIntegrationSettingsDC otherIntegrationSettings = GymSettingFacade.GetOtherIntegrationSettings(branchId, gymCode, "Wellness");
            Guid professionalAppId = new Guid(otherIntegrationSettings.Guid);
            ClientContext.ServicesUrl = otherIntegrationSettings.ServiceBaseUrl;
            ClientContext.ApplicationId = professionalAppId;
            ClientContext.FacilityUrl = otherIntegrationSettings.FacilityUrl;
            ClientContext.ClientApplication = "thirdParties";

           
                ApplicationClient applicationClient = new ApplicationClient(professionalAppId);

                AccessIntegrationRequest request = new AccessIntegrationRequest();
                request.ApiKey = new Guid(otherIntegrationSettings.ApiKey);
                request.Username = otherIntegrationSettings.UserName;
                request.Password = otherIntegrationSettings.Password;

                ServiceResponse<LoginResult> result = applicationClient.AccessIntegration(request);

                if (!result.HasErrors)
                {
                    if (result.Data.Result == LoginResultTypes.Success)
                    {
                        switch (wellnessOperationType)
                        {
                            case WellnessOperationType.REGISTER:
                                if (CreateFacilityUserFromThirdParties(result.Token, result.Data.Facilities[0].Id, member, branchId, gymCode, user) != null)
                                {
                                    successResult.OperationReturnValue = true;
                                }
                                break;
                            case WellnessOperationType.UPDATE:
                                if ((FacilityUserUpdate(result.Token, member, wellnessIntegrationSetting, user, gymCode)) != null)
                                {
                                    successResult.OperationReturnValue = true;
                                }
                                break;
                            case WellnessOperationType.VISIT:
                                if (Visit(result.Token, result.Data.Facilities[0].Id, member, new Guid(wellnessIntegrationSetting.UserId), memberVisit))
                                {
                                    successResult.OperationReturnValue = true;
                                }
                                break;
                            case WellnessOperationType.CLASS:
                                if (ClassVisitTest(result.Token, result.Data.Facilities[0].Id, member, new Guid(wellnessIntegrationSetting.UserId), classVisitDetail))
                                {
                                    successResult.OperationReturnValue = true;
                                    // SearchTrackingActivities(result.Token, result.Data.Facilities[0].Id, new Guid(wellnessIntegrationSetting.UserId));
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        successResult.OperationReturnValue = false;
                    }
                }
                else
                {
                    successResult.OperationReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                successResult.CreateMessage("Error in Wellness Integration Operation" + ex.Message, MessageTypes.ERROR);
            }
            return successResult;
        }

        public static CreateFacilityUserFromThirdPartyResult CreateFacilityUserFromThirdParties(string token, Guid facilityId, OrdinaryMemberDC member, int branchId, string gymCode, string user)
        {
            try
            {
                FacilityClient facilityClient = new FacilityClient(facilityId);
                CreateFacilityUserFromThirdPartyRequest request = new CreateFacilityUserFromThirdPartyRequest();
                request.ExternalId = member.CustId;
                request.FirstName = member.FirstName;
                request.LastName = member.LastName;

                if (member.Role == MemberRole.COM)
                {
                    try
                    {
                        string s = member.LastName;
                        string[] words = s.Split(' ');

                        string first = string.Empty;
                        string second = string.Empty;

                        if (words.Length > 1)
                        {
                            for (int i = 0; i < words.Length; i++)
                            {
                                if (i == 0)
                                {
                                    first = words[0];
                                }
                                else
                                {
                                    second = second + " " + words[i];
                                }
                            }

                            request.FirstName = first;
                            request.LastName = second;
                        }
                    }
                    catch
                    {
                    }
                }

                switch (member.Gender)
                {
                    case Gender.MALE:
                        request.Gender = GenderTypes.M;
                        break;
                    case Gender.FEMALE:
                        request.Gender = GenderTypes.F;
                        break;
                    case Gender.NONE:
                        request.Gender = GenderTypes.M;
                        break;
                }

                if (member.BirthDate > new DateTime(1900, 1, 1))
                {
                    request.DateOfBirth = member.BirthDate;
                }
                else
                {
                    request.DateOfBirth = null;
                }
                request.Email = member.Email;
                request.Token = token;

                ServiceResponse<CreateFacilityUserFromThirdPartyResult> result = facilityClient.CreateFacilityUserFromThirdParty(request);

                if (!result.HasErrors)
                {
                    if (result.Data.Result == CreateFacilityUserFromThirdPartyResultTypes.Created)
                    {
                        MemberIntegrationSettingDC memberIntegrationSettingsDetail = new MemberIntegrationSettingDC();
                        memberIntegrationSettingsDetail.BranchId = branchId;
                        memberIntegrationSettingsDetail.MemberId = member.Id;
                        memberIntegrationSettingsDetail.Token = result.Data.PermanentToken;
                        memberIntegrationSettingsDetail.UserId = result.Data.UserId.ToString();
                        memberIntegrationSettingsDetail.FacilityUserId = result.Data.FacilityUserId.ToString();
                        memberIntegrationSettingsDetail.SystemName = "Wellness";
                        memberIntegrationSettingsDetail.User = user;
                        if (ManageMembershipFacade.AddUpdateMemberIntegrationSettingsAction(memberIntegrationSettingsDetail, gymCode))
                        {
                            return result.Data;
                        }
                    }
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static FacilityUser FacilityUserUpdate(string token, OrdinaryMemberDC member, MemberIntegrationSettingDC wellnessIntegrationSetting, string user, string gymCode)
        {
            FacilityUserClient facilityUserClient = new FacilityUserClient(new Guid(wellnessIntegrationSetting.FacilityUserId));
            FacilityUserUpdateRequest request = new FacilityUserUpdateRequest();

            request.Address1 = member.Address1;
            request.Address2 = member.Address2;
            if (member.BirthDate > new DateTime(1900, 1, 1))
            {
                request.BirthDate = member.BirthDate;
            }
            else
            {
                request.BirthDate = null;
            }

            request.City = member.PostPlace;
            request.Email = member.Email;
            request.FirstName = member.FirstName;
            switch (member.Gender)
            {
                case Gender.MALE:
                    request.Gender = GenderTypes.M;
                    break;
                case Gender.FEMALE:
                    request.Gender = GenderTypes.F;
                    break;
            }

            request.LastName = member.LastName;
            request.MobilePhoneNumber = member.Mobile;
            request.Notes = member.Note;
            request.PhoneNumber = member.PrivateTeleNo;
            request.Token = token;
            request.ZipCode = member.PostCode;
            if (member.Role == MemberRole.COM)
            {
                try
                {
                    string s = member.LastName;
                    string[] words = s.Split(' ');

                    string first = string.Empty;
                    string second = string.Empty;

                    if (words.Length > 1)
                    {
                        for (int i = 0; i < words.Length; i++)
                        {
                            if (i == 0)
                            {
                                first = words[0];
                            }
                            else
                            {
                                second = second + " " + words[i];
                            }
                        }

                        request.FirstName = first;
                        request.LastName = second;
                    }
                }
                catch
                {
                }
            }
            //request.AssignedStaffs  = member.
            //request.CoachId  = member.
            //request.CountryId  = member.
            //request.DeleteAllAssignedGroups  = member.
            // request.DeleteAllAssignedStaffs  = member.
            // request.DeleteCoach  = member.
            // request.DeletePicture  = member.
            //request.ExpenditureUnitOfMeasure  = member.  
            // request.Groups  = member.
            //request.LanguageId  = member. 
            //request.MeasurementSystem  = member.  
            //request.MovergyUserToken  = member.     
            //request.NickName  = member.
            //request.PictureContent  = member.     
            // request.StateProvince  = member.
            //request.TimeZoneId  = member.
            //request.OccupationId  = member. 

            ServiceResponse<FacilityUser> result = facilityUserClient.Update(request);

            if (!result.HasErrors)
            {
                MemberIntegrationSettingDC memberIntegrationSettingsDetail = new MemberIntegrationSettingDC();
                memberIntegrationSettingsDetail.Id = wellnessIntegrationSetting.Id;
                memberIntegrationSettingsDetail.BranchId = wellnessIntegrationSetting.BranchId;
                memberIntegrationSettingsDetail.MemberId = member.Id;
                memberIntegrationSettingsDetail.Token = wellnessIntegrationSetting.Token;
                memberIntegrationSettingsDetail.UserId = wellnessIntegrationSetting.UserId;
                memberIntegrationSettingsDetail.FacilityUserId = wellnessIntegrationSetting.FacilityUserId;
                memberIntegrationSettingsDetail.SystemName = "Wellness";
                memberIntegrationSettingsDetail.User = user;
                //if (ManageMembershipFacade.AddUpdateMemberIntegrationSettingsAction(memberIntegrationSettingsDetail, gymCode))
                //{
                //    return result.Data;
                //}
                return result.Data;
            }
            else
            {
                //update failed
            }
            return null;
        }

        public static bool ClassVisitTest(string token, Guid facilityId, OrdinaryMemberDC member, Guid userId, TrainingProgramClassVisitDetailDC classVisit)
        {
            UserClient userClassClient = new UserClient(userId);

            UserVisitRequest request = new UserVisitRequest();
            request.FacilityId = facilityId;
            request.VisitDate = classVisit.StartDateTime;
            request.Token = token;

            ServiceResponse<UserCheckInResult> result = userClassClient.Visit(request);

            if (!result.HasErrors)
            {
                return result.Data.Done;
            }
            return false;
        }
        
        public static bool Visit(string token, Guid facilityId, OrdinaryMemberDC member, Guid userId, EntityVisitDC memberVisit)
        {

            UserClient userClient = new UserClient(userId);
            UserVisitRequest request = new UserVisitRequest();
            request.FacilityId = facilityId;
            request.VisitDate = memberVisit.InTime;
            request.Token = token;
            ServiceResponse<UserCheckInResult> result = userClient.Visit(request);
            if (!result.HasErrors)
            {
                return result.Data.Done;
            }

            return false;
        }

        public static bool ClassVisit(string token, Guid facilityId, Guid userId, TrainingProgramClassVisitDetailDC classVisitDetail, TrackingActivityItem trackingActivityItem)
        {
            Technogym.MWApps.Client.Training.Client.UserClient trainingUserClient = new Technogym.MWApps.Client.Training.Client.UserClient(userId);

            TrackClassRequest request = new TrackClassRequest();
            request.FacilityId = facilityId;
            request.PerformedDate = classVisitDetail.StartDateTime;
            request.Duration = new GenericPhysicalProperty { Name = PhysicalPropertyTypes.Duration, Value = classVisitDetail.Duration, Um = MeasurementUnitTypes.Minute, AdditionalWeight = false };
            if (trackingActivityItem != null)
            {
                request.PhysicalActivityId = trackingActivityItem.PhysicalActivityId;
            }
            request.Token = token;

            ServiceResponse<Technogym.MWApps.Client.Training.Result.SaveTrackingActivityResult> result = trainingUserClient.TrackClass(request);

            if (!result.HasErrors)
            {
                return result.Data.SaveResult;
            }
            return false;
        }

        public static bool SearchTrackingActivities(string token, Guid facilityId, Guid userId)
        {
            Technogym.MWApps.Client.Training.Client.UserClient trainingUserClient = new Technogym.MWApps.Client.Training.Client.UserClient(userId);

            SearchTrackingActivitiesRequest request = new SearchTrackingActivitiesRequest();
            request.NewTrackingActivityTypeFilter = new[] { NewTrackingActivityTypes.Class };
            request.OnlyThisFacility = new Guid("E5CECE35-6933-44E2-817F-FD03EE463867");
            request.FilterText = string.Empty;
            request.From = 1;
            request.To = 100;
            request.Token = token;

            ServiceResponse<Technogym.MWApps.Client.Training.Result.SearchTrackingActivitiesResult> result = trainingUserClient.SearchTrackingActivities(request);

            if (!result.HasErrors)
            {
                List<TrackingActivityItem> trackingActivityItemList = (List<TrackingActivityItem>)result.Data.Items;

                foreach (var trackingActivityItem in trackingActivityItemList)
                {
                    TrainingProgramClassVisitDetailDC classVisitDetail = new TrainingProgramClassVisitDetailDC();

                    if (ClassVisit(token, facilityId, userId, classVisitDetail, trackingActivityItem))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static OperationResult<int> ValidateChangeGymInMember(int currentBranchID, int newBranchID, int branchID, int memberID, string user, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.ValidateChangeGymInMember(currentBranchID, newBranchID, branchID, memberID, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage(ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        public static OperationResult<int> CheckContractGymChange(int memberID, int memberContractID, int oldBranchID, int newBranchID, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.CheckContractGymChange(memberID, memberContractID, oldBranchID, newBranchID, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in checking contract gym change " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<ArticleDC> GetTimeMachineArticle(string gymCode)
        {
            OperationResult<ArticleDC> result = new OperationResult<ArticleDC>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetTimeMachineArticle(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error Getting Time Machine Article" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateShopItemAndGatPurchase(GatpurchaseShopItem gatPurchaseShopItem)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdateShopItemAndGatPurchase(gatPurchaseShopItem);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error Updating Gatpurchase" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> ValidateMemberWithStatus(int memberId, int statusId, string user, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.ValidateMemberWithStatus(memberId, statusId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage(ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> GetPriceChangeContractCount(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetPriceChangeContractCount(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, gymCode, changeToNextTemplate);
            }
            catch (Exception ex)
            {
                result.CreateMessage(ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdatePrice(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdatePrice(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, gymCode, changeToNextTemplate, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage(ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineContractDC>> GetPriceUpdateContracts(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate,  int hit)
        {
            OperationResult<List<ExcelineContractDC>> result = new OperationResult<List<ExcelineContractDC>>();
            try
            {
                List<ExcelineContractDC> excelineContractDcs = ManageMembershipFacade.GetPriceUpdateContracts(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, gymCode,changeToNextTemplate, hit);
                result.OperationReturnValue = excelineContractDcs;
            }
            catch (Exception ex)
            {
                result.CreateMessage(ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<PackageDC>> GetTemplatesForPriceUpdate(string gymCode)
        {
            OperationResult<List<PackageDC>> result = new OperationResult<List<PackageDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetTemplatesForPriceUpdate(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage(ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        #region InfoGym
        public static string InfoGymGetAccessToken(string clientId, string clientSecret, string host, string data)
        {
            try
            {
                var client = new WebClient();
                var credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(clientId + ":" + clientSecret));
                client.Headers[HttpRequestHeader.Host] = host;
                client.Headers[HttpRequestHeader.KeepAlive] = "true";
                client.Headers[HttpRequestHeader.Accept] = "application/json";
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded;charset=UTF-8";

                client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                ServicePointManager.Expect100Continue = false;
                client.Headers[HttpRequestHeader.AcceptEncoding] = "gzip, deflate";
                client.Headers[HttpRequestHeader.AcceptLanguage] = "en-US,en;q=0.8";

                var url = @"https://" + host + @"/token";
                var result = client.UploadString(url, data);
                return GetValueFromJson(result, "access_token");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string InfoGymRegisterUpdateMember(InfoGymMember member, string url, string host, string accessToken, HttpMethod method)
        {
            try
            {
                var json = MakeJson(member);
                if (!string.IsNullOrEmpty(accessToken.Trim()))
                {
                    try
                    {
                        var client = new WebClient();

                        client.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36";

                        client.Headers[HttpRequestHeader.Host] = host;
                        client.Headers[HttpRequestHeader.KeepAlive] = "true";
                        client.Headers[HttpRequestHeader.ContentType] = "application/json";
                        client.Headers[HttpRequestHeader.Accept] = "application/json";

                        client.Headers[HttpRequestHeader.Authorization] = "Bearer " + accessToken;
                        ServicePointManager.Expect100Continue = false;
                        client.Headers[HttpRequestHeader.AcceptEncoding] = "gzip, deflate";
                        client.Headers[HttpRequestHeader.AcceptLanguage] = "en-US,en;q=0.8";

                        var result = client.UploadString(url, method.ToString(), json);
                        return GetValueFromJson(result, "id");
                    }
                    catch (WebException ex)
                    {
                        throw ex;
                    }
                }
                throw new Exception("Access Token could not be retrieved");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InfoGymUpdateExceMember(string infoGymId, int memberId, string user)
        {
            try
            {
                return ManageMembershipFacade.InfoGymUpdateExceMember(infoGymId, memberId, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static readonly JsonSerializerSettings _jsonSettings = new JsonSerializerSettings
        {
            Formatting = Formatting.Indented,
            ContractResolver = new DefaultContractResolver(),
            DateTimeZoneHandling = DateTimeZoneHandling.Utc
        };
        private static string MakeJson(object value)
        {
            return JsonConvert.SerializeObject(value, _jsonSettings);
        }
        private static string GetValueFromJson(string json, string key)
        {
            var json_serializer = new JavaScriptSerializer();
            var results_list = (IDictionary<string, object>)json_serializer.DeserializeObject(json);
            var accessToken = results_list[key].ToString();
            return accessToken;
        }

        #endregion

        public static OperationResult<List<ContractSummaryDC>> GetContractSummaries(int memberId, int branchId, string gymCode, bool isFreezDetailNeeded, string user, bool isFreezdView)
        {
            OperationResult<List<ContractSummaryDC>> result = new OperationResult<List<ContractSummaryDC>>();
            //  OperationResult<List<ContractSummaryDC>> familyresult = new OperationResult<List<ContractSummaryDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetContractSummaries(memberId, branchId, gymCode, isFreezDetailNeeded, user, isFreezdView);
                //familyresult.OperationReturnValue = ManageMembershipFacade.GetFamilyMemberContracts(memberId, branchId, gymCode,isFreezDetailNeeded);
                //foreach (var item in familyresult.OperationReturnValue)
                //{
                //    result.OperationReturnValue.Add(item);
                //}
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member contract summaries " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ContractSummaryDC>> GetContractSummariesByEmployee(int employeeID, int branchId, DateTime createdDate, string gymCode)
        {
            var result = new OperationResult<List<ContractSummaryDC>>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetContractSummariesByEmployee(employeeID, branchId, createdDate, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting member contract summaries " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<DateTime?> ValidateSponsoringGeneration(string gymCode)
        {
            var result = new OperationResult<DateTime?>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.ValidateSponsoringGeneration( gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Validating Sponsoring genaration" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateBRISMemberData(OrdinaryMemberDC member, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdateBRISMemberData(member, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Validating Sponsoring genaration" + ex.Message, MessageTypes.ERROR);
                result.ErrorOccured = true;
            }
            return result;
        }

        public static OperationResult<string> GetCountryCode(string landCode, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.GetCountryCode(landCode, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting country Code" + ex.Message, MessageTypes.ERROR);
                result.ErrorOccured = true;
            }
            return result;
        }


        public static OperationResult<bool> UpdateAgressoId(int memberID, int aggressoID, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdateAgressoId(memberID, aggressoID, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Updating agresso ID " + ex.Message, MessageTypes.ERROR);
                result.ErrorOccured = true;
            }

            return result;
        }

        public static OperationResult<bool> UpdateBRISStatus(int brisID, string message, string response, string status, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdateBRISStatus(brisID, message, response, status, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Updating aBRIS Status " + ex.Message, MessageTypes.ERROR);
                result.ErrorOccured = true;
            }
            
            return result;
        }

        public static OperationResult<bool> RemoveContractResign(int memberContractId, string user, string gymCode)
        {
            var result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.RemoveContractResign(memberContractId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Validating Sponsoring genaration" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<bool> UpdateInvoicePdfPath(int arItemNo, string invoiceFilePath, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.UpdateInvoicePdfPath(arItemNo, invoiceFilePath, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error updating invoice file  path" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ImageChangedAddEvent(USNotificationTree notification, string gymcode, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.ImageChangedAddEvent(notification, gymcode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error updating invoice file  path" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> RegisterChangeOfInvoiceDueDate(AlterationInDueDateData changeData, String user, String gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ManageMembershipFacade.RegisterChangeOfInvoiceDueDate( changeData,  user,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error updating invoice file  path" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}


