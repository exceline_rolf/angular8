﻿using System;
using System.Collections.Generic;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Common.Notification.Data;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.ResultNotifications;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;

namespace US.Common.Notification.BusinessLogic
{
    public class NotificationManager
    {
        public static USImportResult<int> AddNotification(USNotificationTree notification)
        {
            USImportResult<int> result = new USImportResult<int>();
            try
            {
                result.TagSavedRecordID = -1;
                result.TagSavedRecordID = NotificationFacade.AddNotification(notification);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in Adding USNotification . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }

        public static USImportResult<int> AddNotification(USCommonNotificationDC commonNotification)
        {
            USImportResult<int> result = new USImportResult<int>();
            try
            {
                result.MethodReturnValue = -1;
                result.MethodReturnValue = NotificationFacade.AddNotification(commonNotification);
            }
            catch (Exception ex)
            {
                result.MethodReturnValue = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in Adding USNotification . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }

        public static USImportResult<bool> UpdateNotification(USNotification notification)
        {
            USImportResult<bool> result = new USImportResult<bool>();
            try
            {
                result.TagSavedRecordID = -1;
                result.TagSavedRecordID = NotificationFacade.UpdateNotification(notification);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in Adding USNotification . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }

        public static USImportResult<bool> UpdateNotificationStatus(int notificationID, string user, NotificationStatusEnum status, string statusMessage)
        {
            USImportResult<bool> result = new USImportResult<bool>();
            try
            {
                result.MethodReturnValue = NotificationFacade.UpdateNotificationStatus(notificationID, user, status,statusMessage);
                result.ErrorOccured = false;
            }
            catch (Exception ex)
            {
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in updating notification status . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);

            }
            return result;
        }

        public static bool UpdateNotificationStatusWithDeliveryReport(int notificationId, string mobileNo, int statusId, string statusMessage, string user)
        {
            try
            {
                return NotificationFacade.UpdateNotificationStatusWithDeliveryReport(notificationId, mobileNo, statusId, statusMessage, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static USImportResult<bool> UpdateNotificationStatus(List<int> selectedIds, int status, string user)
        {
            USImportResult<bool> result = new USImportResult<bool>();
            try
            {
                result.MethodReturnValue = NotificationFacade.UpdateNotificationStatus(selectedIds, status, user);
                result.ErrorOccured = false;
            }
            catch (Exception ex)
            {
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in updating notification status . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);

            }
            return result;
        }

        public static USImportResult<List<NotificationTemplateText>> GetNotificationTemplateText(string methodCode, string typeCode, string gymCode)
        {
            USImportResult<List<NotificationTemplateText>> result = new USImportResult<List<NotificationTemplateText>>();
            try
            {
                result.MethodReturnValue = NotificationFacade.GetNotificationTemplateText(methodCode, typeCode, gymCode);
                result.ErrorOccured = false;
            }
            catch (Exception ex)
            {
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in getting notifiction template text . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);

            }
            return result;
        }

        public static USImportResult<NotificationTemplateText> GetNotificationTextByType(NotifyMethodType method, TextTemplateEnum type, string user, int branchId)
        {
            USImportResult<NotificationTemplateText> result = new USImportResult<NotificationTemplateText>();
            try
            {
                result.TagSavedRecordID = -1;
                result.MethodReturnValue = NotificationFacade.GetNotificationTextByType(method, type, user, branchId);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in Getting Template Text By Type . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }

        public static USImportResult<List<USNotificationSummaryInfo>> GetNotificationSummaryList(int branchId, NotificationStatusEnum status, string user)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = new USImportResult<List<USNotificationSummaryInfo>>();
            try
            {
                result.TagSavedRecordID = -1;
                result.MethodReturnValue = NotificationFacade.GetNotificationSummaryList( branchId,status, user);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in getting notification summary list . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }

        public static USImportResult<List<USNotificationSummaryInfo>> GetMemberNotificationSummaryList(int memberId, NotificationStatusEnum status, string user)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = new USImportResult<List<USNotificationSummaryInfo>>();
            try
            {
                result.TagSavedRecordID = -1;
                result.MethodReturnValue = NotificationFacade.GetMemberNotificationSummaryList(memberId, status, user);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in getting member notification summary list . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }

        public static USImportResult<USNotificationSummaryInfo> GetNotificationByIdAction(string user, int notificationId)
        {
            USImportResult<USNotificationSummaryInfo> result = new USImportResult<USNotificationSummaryInfo>();
            try
            {
                result.TagSavedRecordID = -1;
                result.MethodReturnValue = NotificationFacade.GetNotificationByIdAction(notificationId, user);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in getting notification . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }


        public static USImportResult<List<USNotificationSummaryInfo>> GetNotificationsByMemberId(string user, int memeberId)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = new USImportResult<List<USNotificationSummaryInfo>>();
            try
            {
                result.TagSavedRecordID = -1;
                result.MethodReturnValue = NotificationFacade.GetNotificationsByMemberId(memeberId, user);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in getting memeber notifications . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }


        public static USImportResult<int> SaveNotificationTextTemplate(int entityId, NotificationTemplateText templatetText, int branchId, string loggedUser)
        {
            var result = new USImportResult<int>();
            try
            {
                result.TagSavedRecordID = -1;
                result.MethodReturnValue = NotificationFacade.SaveNotificationTextTemplate(entityId, templatetText, branchId, loggedUser);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in getting memeber notifications . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }


        public static USImportResult<List<USNotificationSummaryInfo>> GetNotificationByNotifiyMethod(int memberId, string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = new USImportResult<List<USNotificationSummaryInfo>>();
            try
            {
                result.TagSavedRecordID = -1;
                result.MethodReturnValue = NotificationFacade.GetNotificationByNotifiyMethod(memberId, user, branchId, fromDate, toDate, notifyMethod);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in getting memeber notifications by method . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }


        public static USImportResult<List<USNotificationSummaryInfo>> GetNotificationSearchResult(int branchId, string user, string title, string assignTo, DateTime? receiveDateFrom, DateTime? receiveDateTo, DateTime? dueDateFrom, DateTime? dueDateTo, List<int> severityIdList, List<int> typeIdList, int statusId)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = new USImportResult<List<USNotificationSummaryInfo>>();
            try
            {
                result.TagSavedRecordID = -1;
                result.MethodReturnValue = NotificationFacade.GetNotificationSearchResult( branchId, user, title, assignTo, receiveDateFrom, receiveDateTo, dueDateFrom, dueDateTo, severityIdList, typeIdList, statusId);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in getting notification search result . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }

        public static USImportResult<List<USNotificationSummaryInfo>> GetMemberNotificationSearchResult(int memberID, string user, string title, string assignTo, DateTime? receiveDateFrom, DateTime? receiveDateTo, DateTime? dueDateFrom, DateTime? dueDateTo, List<int> severityIdList, List<int> typeIdList, int statusId)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = new USImportResult<List<USNotificationSummaryInfo>>();
            try
            {
                result.TagSavedRecordID = -1;
                result.MethodReturnValue = NotificationFacade.GetMemberNotificationSearchResult(memberID, user, title, assignTo, receiveDateFrom, receiveDateTo, dueDateFrom, dueDateTo, severityIdList, typeIdList, statusId);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in getting member notification search result . " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }


        public static USImportResult<List<USNotificationSummaryInfo>> GetChangeLogNotificationList(string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = new USImportResult<List<USNotificationSummaryInfo>>();
            try
            {
                result.TagSavedRecordID = -1;
                result.MethodReturnValue = NotificationFacade.GetChangeLogNotificationList(user, branchId, fromDate, toDate, notifyMethod);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in getting Change Log Notifications. " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }


        public static USImportResult<List<ExceNotificationTepmlateDC>> GetNotificationTemplateListByMethod(NotificationMethodType method,TextTemplateEnum temp, string user, int branchId)
        {
            USImportResult<List<ExceNotificationTepmlateDC>> result = new USImportResult<List<ExceNotificationTepmlateDC>>();
            try
            {
                result.TagSavedRecordID = -1;
                result.MethodReturnValue = NotificationFacade.GetNotificationTemplateListByMethod(method, temp, user, branchId);
                if (result.TagSavedRecordID == null)
                {
                    result.TagSavedRecordID = -1;
                }
            }
            catch (Exception ex)
            {
                result.TagSavedRecordID = -1;
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in getting Notification Template." + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }

        public static OperationResult<string> AddAutomatedSMSNotification(string gymCode, int branchID)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = NotificationFacade.AddAutomatedSMSNotification(gymCode, branchID);
            }
            catch(Exception ex)
            {
                result.OperationReturnValue = ex.Message;
                NotificationMessage message = new NotificationMessage("Error in Adding Automated SMS Notification" + ex.Message, MessageTypes.ERROR);
                result.ErrorOccured = true;
            }
            return result;
        }

        public static OperationResult<List<NotificationSMS>> GetNotificationSMSToSend(int notificationID, string gymCode, string user)
        {
            var result = new OperationResult<List<NotificationSMS>>();
            try
            {
                result.OperationReturnValue = NotificationFacade.GetNotificationSMSToSend(notificationID, gymCode, user);
                result.Tag1 = "SMS Details Getting success.";
                result.ErrorOccured = false;
            }
            catch (Exception ex)
            {
                result.Tag1 = ex.Message;
                result.ErrorOccured = true;
            }
            return result;
        }

        public static OperationResult<List<NotificationEmail>> GetNotificationEmailToSend(int notificationID, string gymCode, string user)
        {
            var result = new OperationResult<List<NotificationEmail>>();
            try
            {
                result.OperationReturnValue = NotificationFacade.GetNotificationEmailToSend(notificationID, gymCode, user);
                result.Tag1 = "Email Details Getting success.";
                result.ErrorOccured = false;
            }
            catch (Exception ex)
            {
                result.Tag1 = ex.Message;
                result.ErrorOccured = true;
            }
            return result;
        }

        public static OperationResult<bool> UpdateSMSNotificationList(List<NotificationSMS> smsList, string gymCode, string user)
        {
            var result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = NotificationFacade.UpdateSMSNotificationList(smsList, gymCode, user);
                result.Tag1 = "SMS Details updatting success.";
                result.ErrorOccured = false;
            }
            catch (Exception ex)
            {
                result.Tag1 = ex.Message;
                result.ErrorOccured = true;
            }
            return result;
        }

        public static OperationResult<bool> UpdateEmailNotificationList(List<NotificationEmail> emailList, string gymCode, string user)
        {
            var result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = NotificationFacade.UpdateEmailNotificationList(emailList, gymCode, user);
                result.Tag1 = "Email Details updatting success.";
                result.ErrorOccured = false;
            }
            catch (Exception ex)
            {
                result.Tag1 = ex.Message;
                result.ErrorOccured = true;
            }
            return result;
        }

        public static OperationResult<bool> AddNotificationWithReportScheduler(string gymCode, string user)
        {
            var result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = NotificationFacade.AddNotificationWithReportScheduler(gymCode, user);
                result.Tag1 = "AddNotificationWithReportScheduler success.";
                result.ErrorOccured = false;
            }
            catch (Exception ex)
            {
                result.Tag1 = ex.Message;
                result.ErrorOccured = true;
            }
            return result;
        }

        public static bool UpdateNotificationWithReceiveSMS(string gymCompanyRef, string notificationCode, string message, string sender, string user)
        {
            try
            {
                return NotificationFacade.UpdateNotificationWithReceiveSMS(gymCompanyRef, notificationCode, message, sender, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ExceNotificationScheduleItem> AutomatedPrintPDFGetItemList(string gymCode, string user)
        {
            try
            {
                return NotificationFacade.AutomatedPrintPDFGetItemList(gymCode, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool AutomatedPrintPDFUpdateItemList(List<ExceNotificationScheduleItem> itemList, string gymCode, string user)
        {
            try
            {
                return NotificationFacade.AutomatedPrintPDFUpdateItemList(itemList, gymCode, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OperationResult<List<EntityDC>> GetEntityList(string gymCode)
        {
            var result = new OperationResult<List<EntityDC>>();
            try
            {
                result.OperationReturnValue = NotificationFacade.GetEntityList(gymCode);
                result.Tag1 = "";
                result.ErrorOccured = false;
            }
            catch (Exception ex)
            {
                result.Tag1 = ex.Message;
                result.ErrorOccured = true;
            }
            return result;
        }

        public static OperationResult<List<CategoryDC>> GetTemplateTextKeyByEntity(string type, int entity, string gymCode)
        {
            var result = new OperationResult<List<CategoryDC>>();
            try
            {
                result.OperationReturnValue = NotificationFacade.GetTemplateTextKeyByEntity(type, entity, gymCode);
                result.Tag1 = "";
                result.ErrorOccured = false;
            }
            catch (Exception ex)
            {
                result.Tag1 = ex.Message;
                result.ErrorOccured = true;
            }
            return result;
        }

        public static USImportResult<List<ExceNotificationTepmlateDC>> GetSMSNotificationTemplateByEntity(string entity, string gymCode)
        {
            var result = new USImportResult<List<ExceNotificationTepmlateDC>>();
            try
            {
                result.MethodReturnValue = NotificationFacade.GetSMSNotificationTemplateByEntity(entity, gymCode);
            }
            catch (Exception ex)
            {
                var message = new USNotificationMessage(ErrorLevels.Error, "Error in getting Notification template. " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }

        public static USImportResult<int> AddSMSNotification(string subject, string body, string mobileNo, string entity, int entityId, int memberId, int branchId, string user, string gymCode)
        {
            var result = new USImportResult<int>();
            result.MethodReturnValue = -1;
            try
            {
                result.MethodReturnValue = NotificationFacade.AddSMSNotification(subject, body, mobileNo, entity, entityId, memberId, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                var message = new USNotificationMessage(ErrorLevels.Error, "Error in Adding SMSNotification. " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }

        public static USImportResult<List<AccessDeniedLog>> GetAccessDeniedList(DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            var result = new USImportResult<List<AccessDeniedLog>>();
            try
            {
                result.MethodReturnValue = NotificationFacade.GetAccessDeniedList(fromDate, toDate, branchId, gymCode);
            }
            catch (Exception ex)
            {
                var message = new USNotificationMessage(ErrorLevels.Error, "Error in getting Access Denied Log detail. " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }
    }
}
