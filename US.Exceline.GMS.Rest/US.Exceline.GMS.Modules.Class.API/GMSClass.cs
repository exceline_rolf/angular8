﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Class.BusinessLogic;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;


namespace US.Exceline.GMS.Modules.Class.API
{
    public class GMSClass
    {
        public static OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, string gymCode)
        {
            return ClassManager.SearchClass(category, searchText, gymCode);
        }

        public static OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, DateTime startDate, string gymCode)
        {
            return ClassManager.SearchClass(category, searchText, startDate, gymCode);
        }

        public static OperationResult<List<ExcelineClassDC>> SearchClass(string category, string searchText, DateTime startDate, DateTime endDate, string gymCode)
        {
            return ClassManager.SearchClass(category, searchText, startDate, endDate, gymCode);
        }

        public static OperationResult<int> GetNextClassId(string gymCode)
        {
            return ClassManager.GetNextClassId(gymCode);
        }

        public static OperationResult<string> SaveClass(ExcelineClassDC excelineClass, int branchId, string gymCode, string user, string culture)
        {
            return ClassManager.SaveClass(excelineClass, branchId, gymCode, user, culture);
        }

        public static OperationResult<bool> UpdateClass(ExcelineClassDC excelineClass, string gymCode)
        {
            return ClassManager.UpdateClass(excelineClass, gymCode);
        }

        public static OperationResult<bool> DeleteClass(int classId, string gymCode)
        {
            return ClassManager.DeleteClass(classId, gymCode);
        }


        public static OperationResult<List<ExcelineClassDC>> GetClasses(string className, int branchId, string gymCode)
        {
            return ClassManager.GetClasses(className, branchId, gymCode);
        }

        public static OperationResult<List<ExcelineClassMemberDC>> GetMembersByActiveTimeId(int branchId, int activeTimeId, string gymCode)
        {
            return ClassManager.GetMembersByActiveTimeId(branchId, activeTimeId, gymCode);
        }

        public static OperationResult<List<ScheduleItemDC>> GetScheduleItemsByClass(int classId, int branchId, string gymCode)
        {
            return ClassManager.GetScheduleItemsByClass(classId, branchId, gymCode);
        }

        public static OperationResult<List<ScheduleItemDC>> GetScheduleItemsByClassIds(List<int> classIds, int branchId, string gymCode)
        {
            return ClassManager.GetScheduleItemsByClassIds(classIds, branchId, gymCode);
        }

        public static OperationResult<List<ExcelineClassActiveTimeDC>> GetExcelineClassActiveTimes(int branchId, DateTime startTime, DateTime endTime, int entNO, string gymCode)
        {
            return ClassManager.GetExcelineClassActiveTimes(branchId, startTime, endTime, entNO, gymCode);
        }
     
        public static OperationResult<List<InstructorDC>> GetInstructorsForClass(int classId, string gymCode)
        {
            return ClassManager.GetInstructorsForClass(classId, gymCode);
        }

        public static OperationResult<List<ResourceDC>> GetResourceForClass(int classId, string gymCode)
        {
            return ClassManager.GetResourcesForClass(classId, gymCode);
        }

        public static OperationResult<ScheduleDC> GetClassSchedule(int classId, string gymCode)
        {
            return ClassManager.GetClassSchedule(classId, gymCode);
        }

        public static OperationResult<int> GetExcelineClassIdByName(string className, string gymCode)
        {
            return ClassManager.GetExcelineClassIdByName(className, gymCode);
        }

        public static OperationResult<int> UpdateTimeTableScheduleItems(List<ScheduleItemDC> scheduleItemList, int branchId, string gymCode, string user, string culture)
        {
            return ClassManager.UpdateTimeTableScheduleItems(scheduleItemList, branchId, gymCode, user, culture);
        }

        public static OperationResult<string> CheckActiveTimeOverlapWithClass(ScheduleItemDC scheduleItem,string culture,  string gymCode)
        {
            return ClassManager.CheckActiveTimeOverlapWithClass(scheduleItem, culture,gymCode);
        }

        public static OperationResult<int> SaveScheduleItem(ScheduleItemDC scheduleItem, int branchId, string gymCode, string culture, string user)
        {
            return ClassManager.SaveScheduleItem(scheduleItem, branchId, gymCode,culture, user);
        }

        public static OperationResult<int> DeleteScheduleItem(List<int> scheduleItemIdList, string gymCode,string user)
        {
            return ClassManager.DeleteScheduleItem(scheduleItemIdList, gymCode, user);
        }

        public static OperationResult<bool> DeleteSchedule(ExcelineClassDC exceClass, string gymCode, string user)
        {
            return ClassManager.DeleteSchedule(exceClass, gymCode, user);
        }

        public static OperationResult<int> SaveActiveTimes(List<ScheduleItemDC> scheduleItemList, DateTime startDate, DateTime endDate, string user, string culture)
        {
            return ClassManager.SaveActiveTimes(scheduleItemList, startDate, endDate, user, culture);
        }

        public static OperationResult<int> UpdateClassCalendarActiveTime(EntityActiveTimeDC activeTime, string user, string gymCode)
        {
            return ClassManager.UpdateClassCalendarActiveTime(activeTime, user, gymCode);
        }

        public static OperationResult<int> UpdateSeasonEndDateWithClassActiveTimes(int branchId, int seasonId, DateTime endDate, string gymCode)
        {
            return ClassManager.UpdateSeasonEndDateWithClassActiveTimes(branchId,seasonId,endDate,gymCode);
        }

        public static OperationResult<bool> DeleteClassActiveTime(int actTimeId, string gymCode, string user)
        {
            return ClassManager.DeleteClassActiveTime(actTimeId, gymCode, user);
        }

        public static OperationResult<int> UpdateClassActiveTime(UpdateClassActiveTimeHelperDC helperObj, string gymCode, string user)
        {
            return ClassManager.UpdateClassActiveTime(helperObj, gymCode, user);
        }

        public static OperationResult<List<EntityActiveTimeDC>> GetActiveTimesForClassScheduleItem(int scheduleItemId, string gymCode)
        {
            return ClassManager.GetActiveTimesForClassScheduleItem(scheduleItemId, gymCode);
        }


        public static OperationResult<Data.SystemObjects.ScheduleData> GetSeasonAndClassInfo(int branchId, int scheduleId, String gymCode)
        {
            return ClassManager.GetSeasonAndClassInfo( branchId,  scheduleId,  gymCode);
        }
       

        public static string GetListOrCalendarInitialViewByUser(string user, string gymCode)
        {
            try
            {
                return ClassManager.GetListOrCalendarInitialViewByUser(user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
