﻿// --------------------------------------------------------------------------
// Copyright(c) 2008 UnicornSolutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.Exceline.GMS.Modules.Admin.API
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : 22/03/2012 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageEmployees;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.Exceline.GMS.Modules.Admin.API.ManageEmployees
{
    public class GMSGymEmployee
    {
        public static OperationResult<string> SaveGymEmployee(GymEmployeeDC employee, string imageFolderPath, string gymCode)
        {
            return GymEmployeeManager.SaveGymEmployee(employee, imageFolderPath, gymCode);
        }

        public static OperationResult<int> UpdateGymEmployee(GymEmployeeDC employee, int branchId, string user, string gymCode)
        {
            return GymEmployeeManager.UpdateGymEmployee(employee, branchId, user, gymCode);
        }

        //public static OperationResult<bool> DeleteGymEmployee(int gymEmployeeId, int assignEmpId, int branchId, string user, string gymCode)
        //{
        //    return GymEmployeeManager.DeleteGymEmployee(gymEmployeeId,assignEmpId, branchId, user, gymCode);
        //}

        public static OperationResult<bool> DeActivateGymEmployee(int gymEmployeeId, int branchId, string user, string gymCode)
        {
            return GymEmployeeManager.DeActivateGymEmployee(gymEmployeeId, branchId, user, gymCode);
        }

        public static OperationResult<List<GymEmployeeDC>> GetGymEmployees(int branchId, string searchText, bool isActive, int roleId, string gymCode)
        {
            return GymEmployeeManager.GetGymEmployees(branchId, searchText, isActive,roleId, gymCode);
        }

        public static OperationResult<GymEmployeeDC> GetGymEmployeeById(int branchId, int employeeId, string gymCode)
        {
            return GymEmployeeManager.GetGymEmployeeById(branchId, employeeId, gymCode);
        }

        public static OperationResult<GymEmployeeDC> GetGymEmployeeByEmployeeId(int branchId, string user, int employeeId, string gymCode)
        {
            return GymEmployeeManager.GetGymEmployeeByEmployeeId(branchId, user, employeeId, gymCode);
        }

        public static OperationResult<bool> ValidateEmployeeFollowUp(int employeeId, DateTime endDate, string gymCode)
        {
            return GymEmployeeManager.ValidateEmployeeFollowUp(employeeId, endDate, gymCode);
        }

        public static OperationResult<List<GymEmployeeRoleDurationDC>> GetGymEmployeeRoleDuration(int employeeId, string gymCode)
        {
            return GymEmployeeManager.GetGymEmployeeRoleDuration(employeeId, gymCode);
        }

        public static OperationResult<List<GymEmployeeWorkPlanDC>> GetGymEmployeeWorkPlan(int employeeId, string gymCode)
        {
            return GymEmployeeManager.GetGymEmployeeWorkPlan( employeeId,  gymCode);
        }

        public static OperationResult<List<GymEmployeeApprovalDC>> GetGymEmployeeApprovals(int employeeId, string approvalType, string gymCode)
        {
            return GymEmployeeManager.GetGymEmployeeApprovals(employeeId,approvalType, gymCode);
        }

        public static OperationResult<bool> AddEmployeeTimeEntry(EmployeeTimeEntryDC timeEntry,string gymCode)
        {
            return GymEmployeeManager.AddEmployeeTimeEntry(timeEntry, gymCode);
        }

        public static OperationResult<List<EmployeeTimeEntryDC>> GetEmployeeTimeEntries(int employeeId, string gymCode)
        {
            return GymEmployeeManager.GetEmployeeTimeEntries(employeeId, gymCode);
        }

        public static OperationResult<bool> ApproveAllEmployeeTimes(int employeeId, bool isAllApproved, string timeIdString, string gymCode)
        {
            return GymEmployeeManager.ApproveAllEmployeeTimes(employeeId, isAllApproved, timeIdString, gymCode);
        }

        public static OperationResult<int> AdminApproveEmployeeWork(GymEmployeeWorkDC work, bool isApproved, string user, string gymCode)
        {
            return GymEmployeeManager.AdminApproveEmployeeWork(work, isApproved, user, gymCode);
        }
        public static OperationResult<int> SaveGymEmployeeWorkScheduleItem(ScheduleItemDC scheduleItem,int resourceId, int branchId, string gymCode)
        {
            return GymEmployeeManager.SaveGymEmployeeWorkScheduleItem( scheduleItem,resourceId,  branchId,  gymCode);
        }

        public static OperationResult<int> SaveWorkActiveTime(GymEmployeeWorkPlanDC work, int branchId, string gymCode)
        {
            return GymEmployeeManager.SaveWorkActiveTime(work, branchId, gymCode);
        }

        public static OperationResult<int> SaveWorkItem(GymEmployeeWorkDC work, int branchId, string gymCode)
        {
            return GymEmployeeManager.SaveWorkItem(work, branchId, gymCode);
        }

        public static OperationResult<bool> DeleteGymEmployeeActiveTime(int activeTimeId, string gymCode)
        {
            return GymEmployeeManager.DeleteGymEmployeeActiveTime(activeTimeId, gymCode);
        }

        public static OperationResult<List<EmployeeEventDC>> GetEmployeeEvents(int employeeId, string gymCode)
        {
            return GymEmployeeManager.GetEmployeeEvents(employeeId, gymCode);
        }

        public static OperationResult<List<EmployeeJobDC>> GetEmployeeJobs(int employeeId, string gymCode)
        {
            return GymEmployeeManager.GetEmployeeJobs(employeeId, gymCode);
        }

        public static OperationResult<List<EmployeeBookingDC>> GetEmployeeBookings(int employeeId, string gymCode)
        {
            return GymEmployeeManager.GetEmployeeBookings(employeeId, gymCode);
        }

    }
}
