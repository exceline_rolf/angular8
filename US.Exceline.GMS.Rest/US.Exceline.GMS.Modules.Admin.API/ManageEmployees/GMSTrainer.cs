﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/29/2012 8:24:25 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Admin.API.ManageEmployees
{
    public class GMSTrainer
    {

        public static OperationResult<List<TrainerDC>> GetTrainers(int branchId, string searchText, bool isActive, int trainerId, string gymCode)
        {
            OperationResult<List<TrainerDC>> result = TrainerManager.GetTrainers(branchId, searchText, isActive, trainerId, gymCode);//new OperationResult<List<TrainerDC>>();
            try
            {
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
