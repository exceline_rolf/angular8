﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageShop;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageInventoryDiscounts;
using US.GMS.Core.DomainObjects.Admin;

namespace US.Exceline.GMS.Modules.Admin.API.ManageInventoryDiscounts
{
    public class GMSInventoryDiscounts
    {
        public static OperationResult<bool> AddDiscount(ShopDiscountDC newDiscount,string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = DiscountManager.AddDiscount(newDiscount, gymCode);
            return result;
        }

        public static OperationResult<List<ShopDiscountDC>> GetDiscountsList(int branchId, bool isActive,string gymCode)
        {
            OperationResult<List<ShopDiscountDC>> result = new OperationResult<List<ShopDiscountDC>>();
            result = DiscountManager.GetDiscountsList(branchId, isActive,gymCode);
            return result;
        }

        public static OperationResult<bool> UpdateDiscount(ShopDiscountDC updateDiscount,string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = DiscountManager.UpdateDiscount(updateDiscount, gymCode);
            return result;
        }

        public static OperationResult<List<ShopSalesItemDC>> GetDailySales(DateTime salesDate, int branchId, string gymCode)
        {
            return DiscountManager.GetDailySales(salesDate, branchId,  gymCode);
        }

        public static OperationResult<List<InventoryItemDC>> GetinventoryList(int branchId)
        {
            OperationResult<List<InventoryItemDC>> result = new OperationResult<List<InventoryItemDC>>();
            result = DiscountManager.GetInventoryList(branchId);
            return result;
        }

        public static OperationResult<bool> DeleteDiscount(int discountId, string type, string gymCode)
        {
             OperationResult<bool> result = new OperationResult<bool>();
             result = DiscountManager.DeleteDiscount(discountId, type, gymCode);
             return result;
        }
    }
}
