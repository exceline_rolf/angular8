﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/29/2012 16:15:15
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.SystemObjects;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageAnonymizing;
using US.GMS.Core.DomainObjects.Admin.ManageAnonymizing;

namespace US.Exceline.GMS.Modules.Admin.API.ManageAnonymizing
{
    public class GMSAnonymizing
    {
        public static OperationResult<int> DeleteAnonymizingData(ExceAnonymizingDC anonymizing,string user, int branchId, string gymCode)
        {
            return AnonymizingManager.DeleteAnonymizingData(anonymizing,user, branchId, gymCode);
        }

        public static OperationResult<List<ExceAnonymizingDC>> GetAnonymizingData(string gymCode)
        {
            return AnonymizingManager.GetAnonymizingData( gymCode);
        }
    
    }
}
