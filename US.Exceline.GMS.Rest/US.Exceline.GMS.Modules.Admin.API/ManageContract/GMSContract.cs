﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/31/2012 6:28:50 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageContract;

namespace US.Exceline.GMS.Modules.Admin.API.ManageContract
{
    public class GMSContract
    {
        public static OperationResult<int> SaveContract(PackageDC resource, string user, string gymCode)
        {
            return ContractManager.SaveContract(resource, user,  gymCode);
        }

        public static OperationResult<int> UpdateContractPriority(Dictionary<int,int> packages, string user, string gymCode)
        {
            return ContractManager.UpdateContractPriority(packages, user, gymCode);
        }
        public static OperationResult<bool> UpdateContract(PackageDC resource, string user)
        {
            return ContractManager.UpdateContract(resource, user);
        }
       
        public static OperationResult<bool> DeActivateContract(int resourceId, int branchId, string user)
        {
            return ContractManager.DeActivateContract(resourceId, branchId, user);
        }
        public static OperationResult<List<PackageDC>> GetContracts(int branchId, string searchText, int searchType, string searchCriteria, string gymCode)
        {
            return ContractManager.GetContracts( branchId,  searchText,  searchType, searchCriteria,  gymCode);
        }
        public static OperationResult<string> GetNextContractId(int barnchId, string gymCode)
        {
            return ContractManager.GetNextContractId(barnchId,  gymCode);
        }

        public static OperationResult<int> DeleteContract(int contractId, string gymCode)
        {
            return ContractManager.DeleteContract(contractId,  gymCode);
        }

        public static OperationResult<List<PriceItemTypeDC>> GetPriceItemTypes(int branchId, string gymCode)
        {
            return ContractManager.GetPriceItemTypes(branchId,  gymCode);
        }
        
        #region Contract items
        public static OperationResult<List<ContractItemDC>> GetContractItems(int branchId, string gymCode)
        {
            return ContractManager.GetContractItems(branchId,  gymCode);
        }

        public static OperationResult<bool> AddContractItem(ContractItemDC contractItem, string user, int branchId, string gymCode)
        {
            return ContractManager.AddContractItem(contractItem, user, branchId,  gymCode);
        }

        public static OperationResult<bool> RemoveContractItem(int contractItemID, string user, int branchId, string gymCode)
        {
            return ContractManager.RemoveContractItem(contractItemID, user, branchId,  gymCode);
        } 
        #endregion
    }
}
