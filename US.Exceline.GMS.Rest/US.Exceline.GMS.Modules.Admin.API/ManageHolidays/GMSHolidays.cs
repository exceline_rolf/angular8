﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin.HolydayManagement;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageHolidays;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Admin.API.ManageHolidays
{
    public class GMSHolidays
    {
        public static OperationResult<List<HolidayScheduleDC>> GetHolidays(int branchId, string holidayType, DateTime startDate, DateTime endDate, string gymCode)
        {
            return HolidaysManager.GetHolidays(branchId, holidayType, startDate, endDate,  gymCode);
        }

        public static OperationResult<bool> AddNewHoliday(HolidayScheduleDC newHoliday, string gymCode)
        {
            return HolidaysManager.AddNewHoliday(newHoliday,  gymCode);
        }

        public static OperationResult<int> GetHolidayCategoryId(string categoryName, string gymCode)
        {
            return HolidaysManager.GetHolidayCategoryId(categoryName,  gymCode);
        }

        public static OperationResult<bool> UpdateHolidayStatus(HolidayScheduleDC updateHoliday, string gymCode)
        {
            return HolidaysManager.UpdateHolidayStatus(updateHoliday,  gymCode);
        }
    }
}
