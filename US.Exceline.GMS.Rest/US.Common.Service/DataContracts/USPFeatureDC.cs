﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Common.Service.DataContracts
{
    [DataContract]
    public class USPFeatureDC
    {
        [DataMember]
        public string ID { get; set; }

        [DataMember]
        public int FeatureId { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string HomeUIControl { get; set; }

        [DataMember]
        public string ThumbnailImage { get; set; }

        [DataMember]
        public string FeatureColor { get; set; }

        [DataMember]
        public int Priority { get; set; }

        private List<OperationDC> _operations = new List<OperationDC>();

        [DataMember]
        public List<OperationDC> Operations
        {
            get { return _operations; }
            set { _operations = value; }
        }

        [DataMember]
        public string FeaturePath { get; set; }

        [DataMember]
        public string CulturalDisplayName { get; set; }
    }
}
