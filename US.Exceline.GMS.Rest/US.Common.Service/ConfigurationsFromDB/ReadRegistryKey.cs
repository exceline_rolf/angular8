﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace US.Common.Service.ConfigurationsFromDB
{
    public class ReadRegistryKey
    {
        public static List<string> valueList = null;

        public static List<string> Values()
        {
            try
            {
                if (valueList != null)
                    return valueList;

                var unicorn = Registry.LocalMachine.OpenSubKey("SOFTWARE\\UnicornSolutions");

                if (unicorn == null) return null;

                valueList = new List<string>
                {
                    unicorn.GetValue(null).ToString(),
                    unicorn.GetValue("DBConnection").ToString()
                };

                return valueList;

            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}
