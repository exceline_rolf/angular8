﻿
namespace US.GMS.Core.SystemObjects
{
    public enum WellnessOperationType
    {
        REGISTER,
        UPDATE,
        VISIT,
        CLASS
    }
}
