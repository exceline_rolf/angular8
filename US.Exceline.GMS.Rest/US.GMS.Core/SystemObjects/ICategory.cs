﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{ 
    public interface ICategory
    {        
        int Id { get; set; }
        string Code { get; set; }       
        int TypeId { get; set; }        
        string Name { get; set; }        
        byte[] CategoryImage { get; set; }        
        //CategoryTypes CategoryType { get; set; }
    }
}
