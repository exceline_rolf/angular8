﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class MemberATGStatusInfo
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _memberID = -1;
        [DataMember]
        public int MemberID
        {
            get { return _memberID; }
            set { _memberID = value; }
        }

        private string _accountNumber = string.Empty;
        [DataMember]
        public string AccountNumber
        {
            get { return _accountNumber; }
            set { _accountNumber = value; }
        }

        private int _atgStatus = 1;
        [DataMember]
        public int AtgStatus
        {
            get { return _atgStatus; }
            set { _atgStatus = value; }
        }

        private DateTime? _lastUpdatedDateTime;
        [DataMember]
        public DateTime? LastUpdatedDateTime
        {
            get { return _lastUpdatedDateTime; }
            set { _lastUpdatedDateTime = value; }
        }

        private int _gymCount = 0;
         [DataMember]
        public int GymCount
        {
            get { return _gymCount; }
            set { _gymCount = value; }
        }

         private int _selectedStatus = -1;
         [DataMember]
         public int SelectedStatus
         {
             get { return _selectedStatus; }
             set { _selectedStatus = value; }
         }

        private bool _isVisibleAtgStatus;
        [DataMember]
        public bool IsVisibleAtgStatus
        {
            get { return _isVisibleAtgStatus; }
            set { _isVisibleAtgStatus = value; }
        }

        private bool _isDisableAtgStatus;
        [DataMember]
        public bool IsDisableAtgStatus
        {
            get { return _isDisableAtgStatus; }
            set { _isDisableAtgStatus = value; }
        }
    }
}
