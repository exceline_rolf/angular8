﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class ExcelineShopLoggData
    {
        private SalePointInfo _salePointInfo = new SalePointInfo();
        [DataMember]
        public SalePointInfo salePointInfo
        {
            get { return _salePointInfo; }
            set { _salePointInfo = value; }
        }

        private CustomerInfo _customerInfo = new CustomerInfo();
        [DataMember]
        public CustomerInfo customerInfo
        {
            get { return _customerInfo; }
            set { _customerInfo = value; }
        }

        private PaymentTypeContent _paymentTypeContent = new PaymentTypeContent();
        [DataMember]
        public PaymentTypeContent paymentTypeContent
        {
            get { return _paymentTypeContent; }
            set { _paymentTypeContent = value; }
        }

        private ShopCartContent _shopCartContent = new ShopCartContent();
        [DataMember]
        public ShopCartContent shopCartContent
        {
            get { return _shopCartContent; }
            set { _shopCartContent = value; }
        }

        private PostSaleInfo _postSaleInfo = new PostSaleInfo();
        [DataMember]
        public PostSaleInfo postSaleInfo
        {
            get { return _postSaleInfo; }
            set { _postSaleInfo = value; }
        }
    }


    [DataContract]
    public class CartItem
    {
        private int _cartItemNo = -1;
        [DataMember]
        public int CartitemNo
        {
            get { return _cartItemNo; }
            set { _cartItemNo = value; }
        }

        private String _articleId = String.Empty;
        [DataMember]
        public String ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }

        private String _articleName = String.Empty;
        [DataMember]
        public String ArticleName
        {
            get { return _articleName; }
            set { _articleName = value; }
        }

        private String _articlePrice = String.Empty;
        [DataMember]
        public String ArticlePrice
        {
            get { return _articlePrice; }
            set { _articlePrice = value; }
        }

        private String _currentArticleStock = String.Empty;
        [DataMember]
        public String CurrentArticleStock
        {
            get { return _currentArticleStock; }
            set { _currentArticleStock = value; }
        }

        private String _discountPercentage = String.Empty;
        [DataMember]
        public String DiscountPercentage
        {
            get { return _discountPercentage; }
            set { _discountPercentage = value; }
        }

        private String _discountAmount = String.Empty;
        [DataMember]
        public String DiscountAmount
        {
            get { return _discountAmount; }
            set { _discountAmount = value; }
        }

        private String _numberOfItems = String.Empty;
        [DataMember]
        public String NumberOfItems
        {
            get { return _numberOfItems; }
            set { _numberOfItems = value; }
        }

        private String _mVA = String.Empty;
        [DataMember]
        public String MVA
        {
            get { return _mVA; }
            set { _mVA = value; }
        }

        private String _unitPriceExcludingMVA = String.Empty;
        [DataMember]
        public String UnitPriceExcludingMVA
        {
            get { return _unitPriceExcludingMVA; }
            set { _unitPriceExcludingMVA = value; }
        }

        private String _totalPriceExcludingMVA = String.Empty;
        [DataMember]
        public String TotalPriceExcludingMVA
        {
            get { return _totalPriceExcludingMVA; }
            set { _totalPriceExcludingMVA = value; }
        }

        private String _totalPriceIncludingMVA = String.Empty;
        [DataMember]
        public String TotalPriceIncludingMVA
        {
            get { return _totalPriceIncludingMVA; }
            set { _totalPriceIncludingMVA = value; }
        }

        private String _discountComment = String.Empty;
        [DataMember]
        public String DiscountComment
        {
            get { return _discountComment; }
            set { _discountComment = value; }
        }

        private String _installmentShareId = String.Empty;
        [DataMember]
        public String InstallmentShareId
        {
            get { return _installmentShareId; }
            set { _installmentShareId = value; }
        }

        private String _shopSaleItemId = String.Empty;
        [DataMember]
        public String ShopSaleItemId
        {
            get { return _shopSaleItemId; }
            set { _shopSaleItemId = value; }
        }

        private String _aRItemNo = String.Empty;
        [DataMember]
        public String ARItemNo
        {
            get { return _aRItemNo; }
            set { _aRItemNo = value; }
        }

        private String _orderLineId = String.Empty;
        [DataMember]
        public String OrderLineId
        {
            get { return _orderLineId; }
            set { _orderLineId = value; }
        }

        private String _returnComment = String.Empty;
        [DataMember]
        public String ReturnComment
        {
            get { return _returnComment; }
            set { _returnComment = value; }
        }

    }

    [DataContract]
    public class PaymentType
    {
        private String _type = String.Empty;
        [DataMember]
        public String Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private String _typeCode = String.Empty;
        [DataMember]
        public String TypeCode
        {
            get { return _typeCode; }
            set { _typeCode = value; }
        }

        private String _typeId = String.Empty;
        [DataMember]
        public String TypeId
        {
            get { return _typeId; }
            set { _typeId = value; }
        }

        private String _amount = String.Empty;
        [DataMember]
        public String Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
    }

    [DataContract]
    public class SalePointInfo
    {
        private String _branchId = String.Empty;
        [DataMember]
        public String BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private String _machineName = String.Empty;
        [DataMember]
        public String MachineName
        {
            get { return _machineName; }
            set { _machineName = value; }
        }

        private String _salePointId = String.Empty;
        [DataMember]
        public String SalePointId
        {
            get { return _salePointId; }
            set { _salePointId = value; }
        }

        private String _userName = String.Empty;
        [DataMember]
        public String UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private String _sessionsKey = String.Empty;
        [DataMember]
        public String SessionsKey
        {
            get { return _sessionsKey; }
            set { _sessionsKey = value; }
        }

        private String _terminalActive = String.Empty;
        [DataMember]
        public String TerminalActive
        {
            get { return _terminalActive; }
            set { _terminalActive = value; }
        }

        private String _date = String.Empty;
        [DataMember]
        public String Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private String _time = String.Empty;
        [DataMember]
        public String Time
        {
            get { return _time; }
            set { _time = value; }
        }

        private String _creditorNo = String.Empty;
        [DataMember]
        public String CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }
    }

    [DataContract]
    public class CustomerInfo
    {
        private String _isDefaultCustomer = String.Empty;
        [DataMember]
        public String IsDefaultCustomer
        {
            get { return _isDefaultCustomer; }
            set { _isDefaultCustomer = value; }
        }

        private String _customerId = String.Empty;
        [DataMember]
        public String CustomerId
        {
            get { return _customerId; }
            set { _customerId = value; }
        }

        private String _memberId = String.Empty;
        [DataMember]
        public String MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private String _memberBranchId = String.Empty;
        [DataMember]
        public String MemberBranchId
        {
            get { return _memberBranchId; }
            set { _memberBranchId = value; }
        }

        private String _roleType = String.Empty;
        [DataMember]
        public String RoleType
        {
            get { return _roleType; }
            set { _roleType = value; }
        }

    }

    [DataContract]
    public class PostSaleInfo
    {
        private String _installmentId = String.Empty;
        [DataMember]
        public String InstallmentId
        {
            get { return _installmentId; }
            set { _installmentId = value; }
        }

        private String _aRNo = String.Empty;
        [DataMember]
        public String ARNo
        {
            get { return _aRNo; }
            set { _aRNo = value; }
        }

        private String _shopSaleId = String.Empty;
        [DataMember]
        public String ShopSaleId
        {
            get { return _shopSaleId; }
            set { _shopSaleId = value; }
        }

        private String _paymentId = String.Empty;
        [DataMember]
        public String PaymentId
        {
            get { return _paymentId; }
            set { _paymentId = value; }
        }

    }

    [DataContract]
    public class ShopCartContent
    {
        private List<CartItem> _cartItems = new List<CartItem>();
        [DataMember]
        public List<CartItem> CartItems
        {
            get { return _cartItems; }
            set { _cartItems = value; }
        }
    }

    [DataContract]
    public class PaymentTypeContent
    {
        private List<PaymentType> _paymentTypes = new List<PaymentType>();
        [DataMember]
        public List<PaymentType> PaymentTypes
        {
            get { return _paymentTypes; }
            set { _paymentTypes = value; }
        }
    }
}

    
