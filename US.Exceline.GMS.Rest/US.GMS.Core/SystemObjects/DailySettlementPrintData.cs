﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class DailySettlementPrintData
    {
        private String _gymName = String.Empty;
        [DataMember]
        public String GymName
        {
            get { return _gymName; }
            set { _gymName = value; }
        }

        private String _createdUser = String.Empty;
        [DataMember]
        public String CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private String _dailySettlementID = String.Empty;
        [DataMember]
        public String DailySettlementID
        {
            get { return _dailySettlementID; }
            set { _dailySettlementID = value; }
        }

        private String _fromDateTime = String.Empty;
        [DataMember]
        public String FromDateTime
        {
            get { return _fromDateTime; }
            set { _fromDateTime = value; }
        }

        private String _toDateTime = String.Empty;
        [DataMember]
        public String ToDateTime
        {
            get { return _toDateTime; }
            set { _toDateTime = value; }
        }

        private String _activityTurnOver = String.Empty;
        [DataMember]
        public String ActivityTurnOver
        {
            get { return _activityTurnOver; }
            set { _activityTurnOver = value; }
        }
        private String _shopTurnOver = String.Empty;
        [DataMember]
        public String ShopTurnOver
        {
            get { return _shopTurnOver; }
            set { _shopTurnOver = value; }
        }

        private String _giftCardTurnOver = String.Empty;
        [DataMember]
        public String GiftCardTurnOver
        {
            get { return _giftCardTurnOver; }
            set { _giftCardTurnOver = value; }
        }

        private String _onAccountTurnOver = String.Empty;
        [DataMember]
        public String OnAccountTurnOver
        {
            get { return _onAccountTurnOver; }
            set { _onAccountTurnOver = value; }
        }

        private String _totalTurnOver = String.Empty;
        [DataMember]
        public String TotalTurnOver
        {
            get { return _totalTurnOver; }
            set { _totalTurnOver = value; }
        }

        private String _cash = String.Empty;
        [DataMember]
        public String Cash
        {
            get { return _cash; }
            set { _cash = value; }
        }

        private String _bankTerminal = String.Empty;
        [DataMember]
        public String BankTerminal
        {
            get { return _bankTerminal; }
            set { _bankTerminal = value; }
        }

        private String _giftCard = String.Empty;
        [DataMember]
        public String GiftCard
        {
            get { return _giftCard; }
            set { _giftCard = value; }
        }
        private String _onAccount = String.Empty;
        [DataMember]
        public String OnAccount
        {
            get { return _onAccount; }
            set { _onAccount = value; }
        }
        private String _nextOrder = String.Empty;
        [DataMember]
        public String NextOrder
        {
            get { return _nextOrder; }
            set { _nextOrder = value; }
        }
        private String _invoice = String.Empty;
        [DataMember]
        public String Invoice
        {
            get { return _invoice; }
            set { _invoice = value; }
        }
        private String _smsEmailInvoice = String.Empty;
        [DataMember]
        public String SMSEmailInvoice
        {
            get { return _smsEmailInvoice; }
            set { _smsEmailInvoice = value; }
        }
        private String _prePaid = String.Empty;
        [DataMember]
        public String PrePaid
        {
            get { return _prePaid; }
            set { _prePaid = value; }
        }

        private String _totalReceived = String.Empty;
        [DataMember]
        public String TotalReceived
        {
            get { return _totalReceived; }
            set { _totalReceived = value; }
        }

        private String _countedCash = String.Empty;
        [DataMember]
        public String CountedCash
        {
            get { return _countedCash; }
            set { _countedCash = value; }
        }

        private String _exchange = String.Empty;
        [DataMember]
        public String Exchange
        {
            get { return _exchange; }
            set { _exchange = value; }
        }

        private String _cashInOut = String.Empty;
        [DataMember]
        public String CashInOut
        {
            get { return _cashInOut; }
            set { _cashInOut = value; }
        }

        private String _paidOut = String.Empty;
        [DataMember]
        public String PaidOut
        {
            get { return _paidOut; }
            set { _paidOut = value; }
        }

        private String _deviation = String.Empty;
        [DataMember]
        public String Deviation
        {
            get { return _deviation; }
            set { _deviation = value; }
        }

        private String _toBank = String.Empty;
        [DataMember]
        public String ToBank
        {
            get { return _toBank; }
            set { _toBank = value; }
        }

        private String _shopReturn = String.Empty;
        [DataMember]
        public String ShopReturn
        {
            get { return _shopReturn; }
            set { _shopReturn = value; }
        }

        private String _cashWithdrawal = String.Empty;
        [DataMember]
        public String CashWithdrawal
        {
            get { return _cashWithdrawal; }
            set { _cashWithdrawal = value; }
        }

        private String _pettyCash = String.Empty;
        [DataMember]
        public String PettyCash
        {
            get { return _pettyCash; }
            set { _pettyCash = value; }
        }

        private String _totalPayments = String.Empty;
        [DataMember]
        public String TotalPayments
        {
            get { return _totalPayments; }
            set { _totalPayments = value; }
        }

        private String _salePointId = String.Empty;
        [DataMember]
        public String SalePointId
        {
            get { return _salePointId; }
            set { _salePointId = value; }
        }

        private String _cashIn = String.Empty;
        [DataMember]
        public String CashIn
        {
            get { return _cashIn; }
            set { _cashIn = value; }
        }

        private String _cashOut = String.Empty;
        [DataMember]
        public String CashOut
        {
            get { return _cashOut; }
            set { _cashOut = value; }
        }

        private String _totalInvoice = String.Empty;
        [DataMember]
        public String TotalInvoice
        {
            get { return _totalInvoice; }
            set { _totalInvoice = value; }
        }

        private String _cashReturn = String.Empty;
        [DataMember]
        public String CashReturn
        {
            get { return _cashReturn; }
            set { _cashReturn = value; }
        }

        private String _reconciliationText = String.Empty;
        [DataMember]
        public String ReconciliationText
        {
            get { return _reconciliationText; }
            set { _reconciliationText = value; }
        }

        private String _isReconciled = String.Empty;
        [DataMember]
        public String IsReconciled
        {
            get { return _isReconciled; }
            set { _isReconciled = value; }
        }

        private String _iD = String.Empty;
        [DataMember]
        public String ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        private String _docType = String.Empty;
        [DataMember]
        public String DocType
        {
            get { return _docType; }
            set { _docType = value; }
        }

        private String _gymCode = String.Empty;
        [DataMember]
        public String GymCode
        {
            get { return _gymCode; }
            set { _gymCode = value; }
        }

        private String _branchId = String.Empty;
        [DataMember]
        public String BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private String _orgNo = String.Empty;
        [DataMember]
        public String OrgNo
        {
            get { return _orgNo; }
            set { _orgNo = value; }
        }

    }
    
}
