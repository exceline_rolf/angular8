﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public enum PaymentTypes
    {
       SHOP,
       CLASS,
       DROPINVISIT,
       INSTALLMENTS,
       INVOICE,
       OTHER
    }
}
