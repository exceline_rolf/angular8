﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class MemberInvoiceDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _membeInvoiceId = -1;
        [DataMember]
        public int MembeInvoiceId
        {
            get { return _membeInvoiceId; }
            set { _membeInvoiceId = value; }
        }

        private string _invoiceNo = string.Empty;
         [DataMember]
        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        private string _contractId = string.Empty;
        [DataMember]
        public string ContractId
        {
            get { return _contractId; }
            set { _contractId = value; }
        }

        private DateTime? _invoiceDate;
        [DataMember]
        public DateTime? InvoiceDate
        {
            get { return _invoiceDate; }
            set { _invoiceDate = value; }
        }

        private DateTime? _invoiceDueDate;
        [DataMember]
        public DateTime? InvoiceDueDate
        {
            get { return _invoiceDueDate; }
            set { _invoiceDueDate = value; }
        }

        private decimal _invoiceAmount = 0;
        [DataMember]
        public decimal InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        private decimal _invoiceBalance = 0;
        [DataMember]
        public decimal InvoiceBalance
        {
            get { return _invoiceBalance; }
            set { _invoiceBalance = value; }
        }

        private string _invoiceStatus = string.Empty;
        [DataMember]
        public string InvoiceStatus
        {
            get { return _invoiceStatus; }
            set { _invoiceStatus = value; }
        }

        private string _debtCollectionStatus = string.Empty;
        [DataMember]
        public string DebtCollectionStatus
        {
            get { return _debtCollectionStatus; }
            set { _debtCollectionStatus = value; }
        }

        private int _payDirection = 0;
        [DataMember]
        public int PayDirection
        {
            get { return _payDirection; }
            set { _payDirection = value; }
        }

        private string _payeePayerName = string.Empty;
        [DataMember]
        public string PayeePayerName
        {
            get { return _payeePayerName; }
            set { _payeePayerName = value; }
        }

        private bool _isFullyPaid = false;
        [DataMember]
        public bool IsFullyPaid
        {
            get { return _isFullyPaid; }
            set { _isFullyPaid = value; }
        }

        private bool _isPartiallyPaid = false;
        [DataMember]
        public bool IsPartiallyPaid
        {
            get { return _isPartiallyPaid; }
            set { _isPartiallyPaid = value; }
        }

        private bool _isNotPaid = false;
        [DataMember]
        public bool IsNotPaid
        {
            get { return _isNotPaid; }
            set { _isNotPaid = value; }
        }

        private bool _isSponsorInvoice = false;
        [DataMember]
        public bool IsSponsorInvoice
        {
            get { return _isSponsorInvoice; }
            set { _isSponsorInvoice = value; }
        }

        private string _pDFFileParth = string.Empty;
        [DataMember]
        public string PDFFileParth
        {
            get { return _pDFFileParth; }
            set { _pDFFileParth = value; }
        }

        private bool _canCancel = true;
        [DataMember]
        public bool CanCancel
        {
            get { return _canCancel; }
            set { _canCancel = value; }
        }

        private string _invoiceType = string.Empty;
         [DataMember]
        public string InvoiceType
        {
            get { return _invoiceType; }
            set { _invoiceType = value; }
        }

         private string _paymentTypes = string.Empty;
         [DataMember]
         public string PaymentTypes
         {
             get { return _paymentTypes; }
             set { _paymentTypes = value; }
         }

         private string _referencedInvoiceNo = string.Empty;
         [DataMember]
         public string ReferencedInvoiceNo
         {
             get { return _referencedInvoiceNo; }
             set { _referencedInvoiceNo = value; }
         }

         private bool _isShopInvoice = false;
         [DataMember]
         public bool IsShopInvoice
         {
             get { return _isShopInvoice; }
             set { _isShopInvoice = value; }
         }

         private int _gymID = -1;
         [DataMember]
         public int GymID
         {
             get { return _gymID; }
             set { _gymID = value; }
         }

         private string _remindCollStatus = string.Empty;
         [DataMember]
         public string RemindCollStatus
         {
             get { return _remindCollStatus; }
             set { _remindCollStatus = value; }
         }
    }
}
