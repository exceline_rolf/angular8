﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    public class ClassDetailDC
    {
        private DateTime _startDate;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _endDate;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private string _classType = string.Empty;
        [DataMember]
        public string ClassType
        {
            get { return _classType; }
            set { _classType = value; }
        }

        private string _classGroup = string.Empty;
        [DataMember]
        public string ClassGroup
        {
            get { return _classGroup; }
            set { _classGroup = value; }
        }

        private int _activeTimeId;
        [DataMember]
        public int ActiveTimeId
        {
            get { return _activeTimeId; }
            set { _activeTimeId = value; }
        }

        private Dictionary<int, string> _instructorList = new Dictionary<int, string>();
        [DataMember]
        public Dictionary<int, string> InstructorList
        {
            get { return _instructorList; }
            set { _instructorList = value; }
        }

        private int _instructorCount;
        [DataMember]
        public int InstructorCount
        {
            get { return _instructorCount; }
            set { _instructorCount = value; }
        }

        private bool _isHasInstructor;
        [DataMember]
        public bool IsHasInstructor
        {
            get { return _isHasInstructor; }
            set { _isHasInstructor = value; }
        }

        private DateTime _visitTime;
        [DataMember]
        public DateTime VisitTime
        {
            get { return _visitTime; }
            set { _visitTime = value; }
        }

        private string _gymName = string.Empty;
        [DataMember]
        public string GymName
        {
            get { return _gymName; }
            set { _gymName = value; }
        }

        private int _classLevelId;
        [DataMember]
        public int ClassLevelId
        {
            get { return _classLevelId; }
            set { _classLevelId = value; }
        }

        private List<string> _classKeywordList = new List<string>();
        [DataMember]
        public List<string> ClassKeywordList
        {
            get { return _classKeywordList; }
            set { _classKeywordList = value; }
        }

        private string _day = string.Empty;
        [DataMember]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private string _location = string.Empty;
        [DataMember]
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        private string _instructorStrn = string.Empty;
        [DataMember]
        public string InstructorStrn
        {
            get { return _instructorStrn; }
            set { _instructorStrn = value; }
        }

    }
}
