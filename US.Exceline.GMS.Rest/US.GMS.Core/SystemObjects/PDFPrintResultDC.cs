﻿using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class PDFPrintResultDC
    {
        private string _fileParth = string.Empty;
        private bool _isPriview = false;
        private int _errorStateId;

        [DataMember]
        public string FileParth
        {
            get { return _fileParth; }
            set { _fileParth = value; }
        }

        [DataMember]
        public bool IsPriview
        {
            get { return _isPriview; }
            set { _isPriview = value; }
        }

        [DataMember]
        public int ErrorStateId
        {
            get { return _errorStateId; }
            set { _errorStateId = value; }
        }

       
    }
}
