﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    public class PaymentDetailDC
    {
        private decimal _paidAmount = 0;
        [DataMember]
        public decimal PaidAmount
        {
            get { return _paidAmount; }
            set { _paidAmount = value; }
        }

        private decimal _invoiceAmount = 0;
        [DataMember]
        public decimal InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        private PaymentTypes _paymentSource = PaymentTypes.SHOP;
        [DataMember]
        public PaymentTypes PaymentSource
        {
            get { return _paymentSource; }
            set { _paymentSource = value; }
        }

        private string _paymentSourceId = string.Empty;
        [DataMember]
        public string PaymentSourceId
        {
            get { return _paymentSourceId; }
            set { _paymentSourceId = value; }
        }

        private List<PayModeDC> _payModes = new List<PayModeDC>();
        [DataMember]
        public List<PayModeDC> PayModes
        {
            get { return _payModes; }
            set { _payModes = value; }
        }

        private int _custId = -1;
        [DataMember]
        public int CustId
        {
            get { return _custId; }
            set { _custId = value; }
        }

        private int _creditorno = -1;
        [DataMember]
        public int Creditorno
        {
            get { return _creditorno; }
            set { _creditorno = value; }
        }

        private string _kid = string.Empty;
         [DataMember]
        public string Kid
        {
            get { return _kid; }
            set { _kid = value; }
        }

         private string _ref = string.Empty;
         [DataMember]
         public string Ref
         {
             get { return _ref; }
             set { _ref = value; }
         }

         private int _salepointId = -1;
         [DataMember]
         public int SalepointId
         {
             get { return _salepointId; }
             set { _salepointId = value; }
         }

         private string _paidUser = string.Empty;
         [DataMember]
         public string PaidUser
         {
             get { return _paidUser; }
             set { _paidUser = value; }
         }

         private int _paidMemberId = -1;
         [DataMember]
         public int PaidMemberId
         {
             get { return _paidMemberId; }
             set { _paidMemberId = value; }
         }

        
    }
}
