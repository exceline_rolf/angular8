﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class InvoicePaymentSummaryDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private DateTime? _paymentDate;
        [DataMember]
        public DateTime? PaymentDate
        {
            get { return _paymentDate; }
            set { _paymentDate = value; }
        }

        private DateTime? _paymentRegDate;
        [DataMember]
        public DateTime? PaymentRegDate
        {
            get { return _paymentRegDate; }
            set { _paymentRegDate = value; }
        }


        private string _type = string.Empty;
        [DataMember]
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private decimal _paymentAmount = 0;
        [DataMember]
        public decimal PaymentAmount
        {
            get { return _paymentAmount; }
            set { _paymentAmount = value; }
        }

        private decimal _returnAmount = 0;
        [DataMember]
        public decimal ReturnAmount
        {
            get { return _returnAmount; }
            set { _returnAmount = value; }
        }

        private string _employee;
        [DataMember]
        public string Employee
        {
            get { return _employee; }
            set { _employee = value; }
        }
    }
}
