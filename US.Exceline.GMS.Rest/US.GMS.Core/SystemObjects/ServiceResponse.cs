﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public class ServiceResponse
    {
        public string Status { get; set; }
        public List<string> Messages { get; set; }
        public object Data { get; set; }
    }
}
