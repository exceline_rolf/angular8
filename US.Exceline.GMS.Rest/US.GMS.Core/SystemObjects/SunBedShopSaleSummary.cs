﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public class SunBedShopSaleSummary
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private int _memberBranchId;

        public int MemberBranchId
        {
            get { return _memberBranchId; }
            set { _memberBranchId = value; }
        }
        private int _terminalBranchId;

        public int TerminalBranchId
        {
            get { return _terminalBranchId; }
            set { _terminalBranchId = value; }
        }
        private decimal _amount;

        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        private decimal _invoceAmount;

        public decimal InvoiceAmount
        {
            get { return _invoceAmount; }
            set { _invoceAmount = value; }
        }
        private decimal _nextOrderAmount;

        public decimal NextOrderAmount
        {
            get { return _nextOrderAmount; }
            set { _nextOrderAmount = value; }
        }
        private decimal _shopOrderAmount;

        public decimal ShopOrderAmount
        {
            get { return _shopOrderAmount; }
            set { _shopOrderAmount = value; }
        }
        private string _invoiceNumber;

        public string InvoiceNumber
        {
            get { return _invoiceNumber; }
            set { _invoiceNumber = value; }
        }
        private int _nextOrderId;

        public int NextOrderId
        {
            get { return _nextOrderId; }
            set { _nextOrderId = value; }
        }
        private int _shopOrderId;

        public int ShopOrderId
        {
            get { return _shopOrderId; }
            set { _shopOrderId = value; }
        }
        private int _terminalId;

        public int TerminalId
        {
            get { return _terminalId; }
            set { _terminalId = value; }
        }
        private int _memberId;

        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }
        private int _paidAccessMode;

        public int PaidAccessMode
        {
            get { return _paidAccessMode; }
            set { _paidAccessMode = value; }
        }
    }
}
