﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class EntityVisitDetailsDC
    {
        private DateTime? trailDate;
        [DataMember]
        public DateTime? TrailDate
        {
          get { return trailDate; }
          set { trailDate = value; }
        }

        private List<EntityVisitDC> _visitList = new List<EntityVisitDC>();
        [DataMember]
        public List<EntityVisitDC> VisitList
        {
            get { return _visitList; }
            set { _visitList = value; }
        }

        private List<ExcelineBranchDC> _gymList = new List<ExcelineBranchDC>();
        [DataMember]
        public List<ExcelineBranchDC> GymList
        {
            get { return _gymList; }
            set { _gymList = value; }
        }
    }
}
