﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class CustomerBasicInfoDC
    {
        private string _firstName = string.Empty;
        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName = string.Empty;
        [DataMember]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private DateTime? _contractStartDate;
        [DataMember]
        public DateTime? ContractStartDate
        {
            get { return _contractStartDate; }
            set { _contractStartDate = value; }
        }

        private DateTime? _contractEndDate;
        [DataMember]
        public DateTime? ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }

        private DateTime? _lastVisit;
        [DataMember]
        public DateTime? LastVisit
        {
            get { return _lastVisit; }
            set { _lastVisit = value; }
        }

        private decimal _creditBalance = 0;
        [DataMember]
        public decimal CreditBalance
        {
            get { return _creditBalance; }
            set { _creditBalance = value; }
        }

         private decimal _dueBalance = 0;
         [DataMember]
         public decimal DueBalance
         {
             get { return _dueBalance; }
             set { _dueBalance = value; }
         }

         private string _address = string.Empty;
         [DataMember]
         public string Address
         {
             get { return _address; }
             set { _address = value; }
         }

         private string _postalCode = string.Empty;
         [DataMember]
         public string PostalCode
         {
             get { return _postalCode; }
             set { _postalCode = value; }
         }

         private string _zipName = string.Empty;
         [DataMember]
         public string ZipName
         {
             get { return _zipName; }
             set { _zipName = value; }
         }

         private string _accountNo = string.Empty;
         [DataMember]
         public string AccountNo
         {
             get { return _accountNo; }
             set { _accountNo = value; }
         }

         private byte[] _image;
         [DataMember]
         public byte[] Image
         {
             get { return _image; }
             set { _image = value; }
         }

         private decimal _onAccountBalance = 0;
         [DataMember]
         public decimal OnAccountBalance
         {
             get { return _onAccountBalance; }
             set { _onAccountBalance = value; }
         }

         private string _memberContractNo = string.Empty;
         [DataMember]
         public string MemberContractNo
         {
             get { return _memberContractNo; }
             set { _memberContractNo = value; }
         }

         private string _templateNo = string.Empty;
         [DataMember]
         public string TemplateNo
         {
             get { return _templateNo; }
             set { _templateNo = value; }
         }

         private string _roleId = string.Empty;
          [DataMember]
        public string RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

       


    }
}
