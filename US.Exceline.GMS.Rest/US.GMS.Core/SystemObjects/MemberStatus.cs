﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public class MemberStatus
    {
        public int StatusID { get; set; }
        public string StatusName { get; set; }
    }
}
