﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/22/2012 17:23:02
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public class PayModeDC
    {
        public int Id { get; set; }
        public int PaymentTypeId { get; set; }        
        public string PaymentType { get; set; }
        public string PaymentTypeCode { get; set; }
        public decimal Amount { get; set; }
        public DateTime PaidDate { get; set; }
        public string VoucherNo { get; set; }
    }
}
