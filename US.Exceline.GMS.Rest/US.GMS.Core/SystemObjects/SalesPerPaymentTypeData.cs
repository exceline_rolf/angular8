﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class SalesPerPaymentTypeData
    {
        private String _paymentType = String.Empty;
        [DataMember]
        public String PaymentType
        {
            get { return _paymentType; }
            set { _paymentType = value; }
        }

        private String _transactions = String.Empty;
        [DataMember]
        public String Transactions
        {
            get { return _transactions; }
            set { _transactions = value; }
        }

        private String _totalAmount = String.Empty;
        [DataMember]
        public String TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }

        private Char _debitOrKredit = 'X';
        [DataMember]
        public Char DebitOrKredit
        {
            get { return _debitOrKredit; }
            set { _debitOrKredit = value; }
        }
    }
}



