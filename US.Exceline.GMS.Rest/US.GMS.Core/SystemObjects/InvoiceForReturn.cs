﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class InvoiceForReturnDC
    {

        private int _arItemNo = -1;
        [DataMember]
        public int ArItemNO
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }

        private int _creditorInvoiceId = -1;
        [DataMember]
        public int CreditorInvoiceId
        {
            get { return _creditorInvoiceId; }
            set { _creditorInvoiceId = value; }
        }

        private decimal _creditorInvoiceAmount = 0;
        [DataMember]
        public decimal CreditorInvoiceAmount
        {
            get { return _creditorInvoiceAmount; }
            set { _creditorInvoiceAmount = value; }
        }

        private decimal _creditOrderLineAmount = 0;
        [DataMember]
        public decimal CreditOrderLineAmount
        {
            get { return _creditOrderLineAmount; }
            set { _creditOrderLineAmount = value; }
        }

        private int _NoOfItems = -1;
        [DataMember]
        public int NoOfItems
        {
            get { return _NoOfItems; }
            set { _NoOfItems = value; }
        }

        private decimal _UnitPrice = 0;
        [DataMember]
        public decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        private int _ArticleNo = -1;
        [DataMember]
        public int ArticleNo
        {
            get { return _ArticleNo; }
            set { _ArticleNo = value; }
        }

        private string _ArticleText = string.Empty;
        [DataMember]
        public string ArticleText
        {
            get { return _ArticleText; }
            set { _ArticleText = value; }
        }

        private decimal _Balance = 0;
        [DataMember]
        public decimal Balance
        {
            get { return _Balance; }
            set { _Balance = value; }
        }

        private DateTime? _Regdate;
        [DataMember]
        public DateTime? Regdate
        {
            get { return _Regdate; }
            set { _Regdate = value; }
        }

        private string _InvoiceNo = string.Empty;
        [DataMember]
        public string InvoiceNo
        {
            get { return _InvoiceNo; }
            set { _InvoiceNo = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private bool _IsCredited = false;
        [DataMember]
        public bool IsCredited
        {
            get { return _IsCredited; }
            set { _IsCredited = value; }
        }

        private int _CreditedQuantity = -1;
        [DataMember]
        public int CreditedQuantity
        {
            get { return _CreditedQuantity; }
            set { _CreditedQuantity = value; }
        }

        private decimal _CreditedAmount = 0;
        [DataMember]
        public decimal CreditorNotesCreditedAmount
        {
            get { return _CreditedAmount; }
            set { _CreditedAmount = value; }
        }
    }
}
