﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Economy
{
    [DataContract]
    public class CreditNoteDetailsDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _creditNoteId = -1;
        [DataMember]
        public int CreditNoteId
        {
            get { return _creditNoteId; }
            set { _creditNoteId = value; }
        }

        private int _articleId = -1;
        [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }

        private string _articleText;
        [DataMember]
        public string ArticleText
        {
            get { return _articleText; }
            set { _articleText = value; }
        }

        private decimal _articlePrice = 0;
        [DataMember]
        public decimal ArticlePrice
        {
            get { return _articlePrice; }
            set { _articlePrice = value; }
        }

        private decimal _amount;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private int _creditedCount = 0;
        [DataMember]
        public int CreditedCount
        {
            get { return _creditedCount; }
            set { _creditedCount = value; }
        }

        private bool _isEnabled = false;
        [DataMember]
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private string _user;
        [DataMember]
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        private int _orderlineId = -1;
        [DataMember]
        public int OrderlineId
        {
            get { return _orderlineId; }
            set { _orderlineId = value; }
        }

        private string _note = string.Empty;
        [DataMember]
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }

    }
}
