﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageClasses
{
    [DataContract]
    public class ClassBookingActiveTimeDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
            }
        }

        private int _sheduleItemId = -1;
        [DataMember]
        public int SheduleItemId
        {
            get
            {
                return _sheduleItemId;
            }
            set
            {
                _sheduleItemId = value;
            }
        }

        private DateTime _startDateTime;
        [DataMember]
        public DateTime StartDateTime
        {
            get { return _startDateTime; }
            set
            {
                _startDateTime = value;
            }
        }

        private DateTime _endDateTime;
        [DataMember]
        public DateTime EndDateTime
        {
            get { return _endDateTime; }
            set
            {
                _endDateTime = value;
            }
        }

        private int _entityId = -1;
        [DataMember]
        public int EntityId
        {
            get { return _entityId; }
            set
            {
                _entityId = value;
            }
        }

        private string _entityRoleType = string.Empty;
        [DataMember]
        public string EntityRoleType
        {
            get { return _entityRoleType; }
            set
            {
                _entityRoleType = value;
            }
        }

        private int _entNo = -1;
        [DataMember]
        public int EntNo
        {
            get { return _entNo; }
            set
            {
                _entNo = value;
            }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }
        }

        private int _activeTimeId = -1;
        [DataMember]
        public int ActiveTimeId
        {
            get { return _activeTimeId; }
            set { _activeTimeId = value; }
        }

        private string _startDate;
        [DataMember]
        public string StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }
    }
}
