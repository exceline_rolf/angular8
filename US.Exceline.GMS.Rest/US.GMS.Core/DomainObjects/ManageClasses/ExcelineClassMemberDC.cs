﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageClasses
{
    [DataContract]
    public class ExcelineClassMemberDC
    {
        private int _memberNumber = -1;
        [DataMember]
        public int MemberNumber
        {
            get { return _memberNumber; }
            set { _memberNumber = value; }
        }

        private string _memberName;
        [DataMember]
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        private Nullable<DateTime> _visitTime;
        [DataMember]
        public Nullable<DateTime> VisitTime
        {
            get { return _visitTime; }
            set { _visitTime = value; }
        }

    }
}
