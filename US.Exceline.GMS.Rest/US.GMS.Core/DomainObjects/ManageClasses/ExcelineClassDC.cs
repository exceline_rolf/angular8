﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.ManageClasses
{
    [DataContract]
    public class ExcelineClassDC : IExcelineClass
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private CategoryDC _classCategory = new CategoryDC();
        [DataMember]
        public CategoryDC ClassCategory
        {
            get { return _classCategory; }
            set { _classCategory = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _mode = string.Empty;
        [DataMember]
        public string Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }

        private int _maxNumberOfMembers = 0;
        [DataMember]
        public int MaxNumberOfMembers
        {
            get { return _maxNumberOfMembers; }
            set { _maxNumberOfMembers = value; }
        }

        private ScheduleDC _schedule = new ScheduleDC();
        [DataMember]
        public ScheduleDC Schedule
        {
            get { return _schedule; }
            set { _schedule = value; }
        }

        private List<TrainerDC> _trainersList = new List<TrainerDC>();
        [DataMember]
        public List<TrainerDC> TrainersList
        {
            get { return _trainersList; }
            set { _trainersList = value; }
        }


        private List<InstructorDC> _instructorList = new List<InstructorDC>();
        [DataMember]
        public List<InstructorDC> InstructorList
        {
            get { return _instructorList; }
            set { _instructorList = value; }
        }

        private List<ResourceDC> _resourceList = new List<ResourceDC>();
        [DataMember]
        public List<ResourceDC> ResourceList
        {
            get { return _resourceList; }
            set { _resourceList = value; }
        }

        private List<ExcelineMemberDC> _memberList = new List<ExcelineMemberDC>();
        [DataMember]
        public List<ExcelineMemberDC> MemberList
        {
            get { return _memberList; }
            set { _memberList = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _modifiedUser;
        [DataMember]
        public string ModifiedUser
        {
            get { return _modifiedUser; }
            set { _modifiedUser = value; }
        }

     
        private decimal totalClassFee;
        [DataMember]
        public decimal TotalClassFee
        {
            get { return totalClassFee; }
            set { totalClassFee = value; }
        }

        private decimal activeTimeFee;
        [DataMember]
        public decimal ActiveTimeFee
        {
            get { return activeTimeFee; }
            set { activeTimeFee = value; }
        }

        private int _availableNumberOfMembers;
        [DataMember]
        public int AvailableNumberOfMembers
        {
            get { return _availableNumberOfMembers; }
            set { _availableNumberOfMembers = value; }
        }

        private int _articleId = 0;
        [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }

        private bool _isPayable = false;
        [DataMember]
        public bool IsPayable
        {
            get { return _isPayable; }
            set { _isPayable = value; }
        }

        
        public bool _isSelected = false;
        [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        public bool _isCalendarPriority = false;
        [DataMember]
        public bool IsCalendarPriority
        {
            get { return _isCalendarPriority; }
            set { _isCalendarPriority = value; }
        }       

        private string _classType = string.Empty;
        [DataMember]
        public string ClassType
        {
            get { return _classType; }
            set { _classType = value; }
        }

        private bool _isCopyClass;
         [DataMember]
        public bool IsCopyClass
        {
            get { return _isCopyClass; }
            set { _isCopyClass = value; }
        }

        private bool _isClassDeleted = true;
         [DataMember]
        public bool IsClassDeleted
        {
            get { return _isClassDeleted; }
            set { _isClassDeleted = value; }
        }


        #region IExcelineClass Members

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private DateTime _ModifiedDate;
        [DataMember]
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }

        private bool _activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        #endregion

        #region IExcelineClass Members

        private List<int> _trainerIdList = new List<int>();
        [DataMember]
        public List<int> TrainerIdList
        {
            get { return _trainerIdList; }
            set { _trainerIdList = value; }
        }

        private List<int> _memberIdList = new List<int>();
        [DataMember]
        public List<int> MemberIdList
        {
            get { return _memberIdList; }
            set { _memberIdList = value; }
        }

        private List<int> _instructorIdList = new List<int>();
        [DataMember]
        public List<int> InstructorIdList
        {
            get { return _instructorIdList; }
            set { _instructorIdList = value; }
        }
        private List<int> _employeeIdList = new List<int>();
        [DataMember]
        public List<int> EmployeeIdList
        {
            get { return _employeeIdList; }
            set { _employeeIdList = value; }
        }

        #endregion

        #region IExcelineClass Members

        private List<int> _resourceIdList = new List<int>();
        [DataMember]
        public List<int> ResourceIdList
        {
            get { return _resourceIdList; }
            set { _resourceIdList = value; }
        }

       
        #endregion
    }
}
