﻿
// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.Exceline.GMS.Core
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 26/6/2012 11:27:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Notification
{
    [DataContract]
    public class NotificationMethodDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _sourceID = -1;
        [DataMember]
        public int SourceID
        {
            get { return _sourceID; }
            set { _sourceID = value; }
        }

        private string _source = string.Empty;
        [DataMember]
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }
        private int _receiverEntityID = -1;
        [DataMember]
        public int ReceiverEntityID
        {
            get { return _receiverEntityID; }
            set { _receiverEntityID = value; }
        }

        private DateTime _dueDate = DateTime.Now;
        [DataMember]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private string _notification = string.Empty;
        [DataMember]
        public string Notification
        {
            get { return _notification; }
            set { _notification = value; }
        }

        private OccuranceType _occurance = OccuranceType.NONE;
        [DataMember]
        public OccuranceType Occurance
        {
            get { return _occurance; }
            set { _occurance = value; }
        }

        private NotifyMethodType _method = NotifyMethodType.NONE;
        [DataMember]
        public NotifyMethodType Method
        {
            get { return _method; }
            set { _method = value; }
        }

        private DateTime _nextDueDate = DateTime.Now;
        [DataMember]
        public DateTime NextDueDate
        {
            get { return _nextDueDate; }
            set { _nextDueDate = value; }
        }

        private int _notificationID = -1;
        [DataMember]
        public int NotificationID
        {
            get { return _notificationID; }
            set { _notificationID = value; }
        }

        private string _role = string.Empty;
        [DataMember]
        public string Role
        {
            get { return _role; }
            set { _role = value; }
        }

        private string _contactEntity = string.Empty;
        [DataMember]
        public string ContactEntity
        {
            get { return _contactEntity; }
            set { _contactEntity = value; }
        }

        private DateTime _registerdDate = DateTime.Now;
        [DataMember]
        public DateTime RegisterdDate
        {
            get { return _registerdDate; }
            set { _registerdDate = value; }
        }

        private bool _isNew = true;
        [DataMember]
        public bool IsNew
        {
            get { return _isNew; }
            set { _isNew = value; }
        }

        private NotificationState _state = NotificationState.NONE;
        [DataMember]
        public NotificationState State
        {
            get { return _state; }
            set { _state = value; }
        }


        private SMSType _smsType = SMSType.NONE;
        [DataMember]
        public SMSType SMSType
        {
            get { return _smsType; }
            set { _smsType = value; }
        }
        //Accoring to the USCTemplateType, The relevant template is selected by USC rules.
        private USCTemplateType _uscTemplateType = USCTemplateType.NONE;
        [DataMember]
        public USCTemplateType USCTemplateType
        {
            get { return _uscTemplateType; }
            set { _uscTemplateType = value; }
        }
    }
}
