﻿
// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.Exceline.GMS.Core
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 26/6/2012 11:27:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.GMS.Core.DomainObjects.Notification
{
    [DataContract]
    public class NotificationDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _sourceID = -1;
        [DataMember]
        public int SourceID
        {
            get { return _sourceID; }
            set { _sourceID = value; }
        }

        private string _source = string.Empty;
        [DataMember]
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }
        private int _receiverEntityID = -1;
        [DataMember]
        public int ReceiverEntityID
        {
            get { return _receiverEntityID; }
            set { _receiverEntityID = value; }
        }

        private List<NotificationMethodDC> _methodList = new List<NotificationMethodDC>();
        [DataMember]
        public List<NotificationMethodDC> MethodList
        {
            get { return _methodList; }
            set { _methodList = value; }
        }
        private MemberRole _role = MemberRole.NONE;
        [DataMember]
        public MemberRole Role
        {
            get { return _role; }
            set { _role = value; }
        }
    }
}
