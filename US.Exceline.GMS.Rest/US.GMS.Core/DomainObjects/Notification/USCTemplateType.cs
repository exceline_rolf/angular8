﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.Notification
{
    public enum USCTemplateType
    {
        PENDING_PAYMENT,
        CONTRACT_EXPIRE,
        MEMBER_ARRIVAL,
        BOOKING,
        TERMINATION_FREEZ,
        SHEDULE_CHANGES,
        BIRTHDAY,
        TEXT,
        BASIC,
        NONE
    }
}
