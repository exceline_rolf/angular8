﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    [DataContract]
    public class ExceAccessControlTerminalDetailDC
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public string Name { get; set; }
        [DataMember(Order = 3)]
        public int Port { get; set; }
        [DataMember(Order = 4)]
        public bool IsOnline { get; set; }
        [DataMember(Order = 5)]
        public string Location { get; set; }
        [DataMember(Order = 6)]
        public string TypeId { get; set; }
        [DataMember(Order = 7)]
        public bool IsSecondIdentification { get; set; }
        //Shouldn't want to pass those property to windows service 
        //[DataMember(Order = 8)]
        public int SalePointId { get; set; }
        //[DataMember(Order = 9)]
        public bool IsPaid { get; set; }
        //[DataMember(Order = 10)]
        public string ArticleNo { get; set; }
        //[DataMember(Order = 11)]
        public string ArticleName { get; set; }
        //[DataMember(Order = 12)]
        public decimal ArticlePrice { get; set; }
        public int ArticleCategoryId { get; set; }
    }
}
