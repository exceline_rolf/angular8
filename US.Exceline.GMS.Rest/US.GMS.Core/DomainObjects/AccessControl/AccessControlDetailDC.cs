﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    [DataContract]
    public class AccessControlDetailDC
    {
        private int _minimumPunches = -1;
        [DataMember]
        public int MinimumPunches
        {
            get { return _minimumPunches; }
            set { _minimumPunches = value; }
        }

        private int _minimumUnpaidInvoices = 0;
        [DataMember]
        public int MinimumUnpaidInvoices
        {
            get { return _minimumUnpaidInvoices; }
            set { _minimumUnpaidInvoices = value; }
        }

        private decimal _unPaidDueBalance = 0;
        [DataMember]
        public decimal UnPaidDueBalance
        {
            get { return _unPaidDueBalance; }
            set { _unPaidDueBalance = value; }
        }

        private decimal _minimumOnAccountBalance = 0;
        [DataMember]
        public decimal MinimumOnAccountBalance
        {
            get { return _minimumOnAccountBalance; }
            set { _minimumOnAccountBalance = value; }
        }

        private int _minutesBetweenSwipes = 0;
        [DataMember]
        public int MinutesBetweenSwipes
        {
            get { return _minutesBetweenSwipes; }
            set { _minutesBetweenSwipes = value; }
        }
    }
}
