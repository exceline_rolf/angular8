﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    public class ExcAccessEvent
    {
        public int id {get;set;}
        public int MemberId { get; set; }
        public int BranchId { get; set; }
        public int TerminalId { get; set; }
        public string CardNo { get; set; }
        public string Messages { get; set; }
    }
}
