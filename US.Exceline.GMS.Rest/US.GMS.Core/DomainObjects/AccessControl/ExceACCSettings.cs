﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    [DataContract]
    public class ExceACCSettings
    {
        private int _minimumPunches = 0;
        [DataMember]
        public int MinimumPunches
        {
            get { return _minimumPunches; }
            set { _minimumPunches = value; }
        }

        private int _minimumUnpaidInvoices = 0;
        [DataMember]
        public int MinimumUnpaidInvoices
        {
            get { return _minimumUnpaidInvoices; }
            set { _minimumUnpaidInvoices = value; }
        }

        private decimal _unPaidDueBalance = 0;
        [DataMember]
        public decimal UnPaidDueBalance
        {
            get { return _unPaidDueBalance; }
            set { _unPaidDueBalance = value; }
        }

        private decimal _minimumOnAccountBalance = 0;
        [DataMember]
        public decimal MinimumOnAccountBalance
        {
            get { return _minimumOnAccountBalance; }
            set { _minimumOnAccountBalance = value; }
        }

        private int _minutesBetweenSwipes = 0;
        [DataMember]
        public int MinutesBetweenSwipes
        {
            get { return _minutesBetweenSwipes; }
            set { _minutesBetweenSwipes = value; }
        }

        private bool _isSendSms;
        [DataMember]
        public bool IsSendSms
        {
            get { return _isSendSms; }
            set { _isSendSms = value; }
        }

        private bool _isDisplayName;
        [DataMember]
        public bool IsDisplayName
        {
            get { return _isDisplayName; }
            set { _isDisplayName = value; }
        }

        private bool _isDisplayCustomerNumber;
        [DataMember]
        public bool IsDisplayCustomerNumber
        {
            get { return _isDisplayCustomerNumber; }
            set { _isDisplayCustomerNumber = value; }
        }

        private bool _isDisplayContractTemplate;
        [DataMember]
        public bool IsDisplayContractTemplate
        {
            get { return _isDisplayContractTemplate; }
            set { _isDisplayContractTemplate = value; }
        }

        private bool _isDisplayHomeGym;
        [DataMember]
        public bool IsDisplayHomeGym
        {
            get { return _isDisplayHomeGym; }
            set { _isDisplayHomeGym = value; }
        }

        private bool _isDisplayContractEndDate;
        [DataMember]
        public bool IsDisplayContractEndDate
        {
            get { return _isDisplayContractEndDate; }
            set { _isDisplayContractEndDate = value; }
        }

        private bool _isDisplayBirthDate;
        [DataMember]
        public bool IsDisplayBirthDate
        {
            get { return _isDisplayBirthDate; }
            set { _isDisplayBirthDate = value; }
        }

        private bool _isDisplayAge;
        [DataMember]
        public bool IsDisplayAge
        {
            get { return _isDisplayAge; }
            set { _isDisplayAge = value; }
        }

        private bool _isDisplayLastVisit;
        [DataMember]
        public bool IsDisplayLastVisit
        {
            get { return _isDisplayLastVisit; }
            set { _isDisplayLastVisit = value; }
        }

        private bool _isDisplayCreditBalance;
        [DataMember]
        public bool IsDisplayCreditBalance
        {
            get { return _isDisplayCreditBalance; }
            set { _isDisplayCreditBalance = value; }
        }

        private bool _isDisplayPicture;
        [DataMember]
        public bool IsDisplayPicture
        {
            get { return _isDisplayPicture; }
            set { _isDisplayPicture = value; }
        }

        private bool _isDisplayLastAccess;
        [DataMember]
        public bool IsDisplayLastAccess
        {
            get { return _isDisplayLastAccess; }
            set { _isDisplayLastAccess = value; }
        }

        private bool _isDisplayNotification;
        [DataMember]
        public bool IsDisplayNotification
        {
            get { return _isDisplayNotification; }
            set { _isDisplayNotification = value; }
        }

        
    }
}
