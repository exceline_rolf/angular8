﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    [DataContract]
    public class EntityVisitDetails
    {
        [DataMember (Order=1)]
        public string ActivityId { get; set; }
        [DataMember(Order = 2)]
        public string ArticleID { get; set; }
         [DataMember(Order = 3)]
        public string BranchId { get; set; }
         [DataMember(Order = 4)]
        public string ContractNo { get; set; }
         [DataMember(Order = 5)]
        public string CountVist { get; set; }
         [DataMember(Order = 6)]
        public string CutomerNo { get; set; }
        [DataMember(Order = 7)]
        public string EntityId { get; set; }
         [DataMember(Order = 8)]
        public string EntityName { get; set; }
         [DataMember(Order = 9)]
        public string GymName { get; set; }
         [DataMember(Order = 10)]
        public string Id { get; set; }
         [DataMember(Order = 11)]
        public string InTime { get; set; }
         [DataMember(Order = 12)]
        public string InvoiceRerefence { get; set; }
         [DataMember(Order = 13)]
        public string ItemName { get; set; }
         [DataMember(Order = 14)]
        public string MemberContractId { get; set; }
         [DataMember(Order = 15)]
        public string VisitDate { get; set; }
         [DataMember(Order = 16)]
        public string VisitType { get; set; }
         [DataMember(Order = 17)]
         public List<string> Messages { get; set; }
         [DataMember(Order = 18)]
        public bool IsVisit { get; set; }
         [DataMember(Order = 19)]
         public string  CardNo { get; set; }
         [DataMember(Order = 20)]
         public string Mobile { get; set; }
         [DataMember(Order = 21)]
         public bool IsSendSms { get; set; }
         [DataMember(Order = 22)]
         public int MinimumPunches { get; set; }

    }
}
