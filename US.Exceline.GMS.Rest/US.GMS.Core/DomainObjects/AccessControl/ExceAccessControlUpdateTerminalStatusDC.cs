﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    [DataContract]
    public class ExceAccessControlUpdateTerminalStatusDC
    {
        private bool _isOnlined;
        private int _terminalId;

        [DataMember(Order = 1)]
        public bool IsOnlined
        {
            get { return _isOnlined; }
            set { _isOnlined = value; }
        }

        [DataMember(Order = 2)]
        public int TerminalId
        {
            get { return _terminalId; }
            set { _terminalId = value; }
        }
    }
}
