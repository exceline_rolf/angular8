﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    [DataContract]
    public class GymACCOpenTime
    {
        private string _branchId;
        [DataMember]
        public string BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private string _region = string.Empty;
        [DataMember]
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        private string _id = string.Empty;
        [DataMember]
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _isExpress = string.Empty;
        [DataMember]
        public string IsExpress
        {
            get { return _isExpress; }
            set { _isExpress = value; }
        }

        private string _startTime = string.Empty;
        [DataMember]
        public string StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private string _endTime = string.Empty;
        [DataMember]
        public string EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private string _isMonday = string.Empty;
        [DataMember]
        public string IsMonday
        {
            get { return _isMonday; }
            set { _isMonday = value; }
        }

        private string _isTuesday = string.Empty;
        [DataMember]
        public string IsTuesday
        {
            get { return _isTuesday; }
            set { _isTuesday = value; }
        }

        private string _isWednesday = string.Empty;
        [DataMember]
        public string IsWednesday
        {
            get { return _isWednesday; }
            set { _isWednesday = value; }
        }

        private string _isThursday = string.Empty;
        [DataMember]
        public string IsThursday
        {
            get { return _isThursday; }
            set { _isThursday = value; }
        }

        private string _isFriday = string.Empty;
        [DataMember]
        public string IsFriday
        {
            get { return _isFriday; }
            set { _isFriday = value; }
        }

        private string _isSaturday = string.Empty;
        [DataMember]
        public string IsSaturday
        {
            get { return _isSaturday; }
            set { _isSaturday = value; }
        }

        private string _isSunday = string.Empty;
        [DataMember]
        public string IsSunday
        {
            get { return _isSunday; }
            set { _isSunday = value; }
        }

        private string _days = string.Empty;
        [DataMember]
        public string Days
        {
            get { return _days; }
            set { _days = value; }
        }
    }
}
