﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class ArticleDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _articleNo = string.Empty;
        [DataMember]
        public string ArticleNo
        {
            get { return _articleNo; }
            set { _articleNo = value; }
        }

        private string _color = "#0000FF";
        [DataMember]
        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }

        private string _code = string.Empty;
        [DataMember]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _description;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private decimal _defaultPrice;
        [DataMember]
        public decimal DefaultPrice
        {
            get { return _defaultPrice; }
            set { _defaultPrice = value; }
        }

        private decimal _employeePrice;
        [DataMember]
        public decimal EmployeePrice
        {
            get { return _employeePrice; }
            set { _employeePrice = value; }
        }

        private decimal _purchasedPrice;
        [DataMember]
        public decimal PurchasedPrice
        {
            get { return _purchasedPrice; }
            set { _purchasedPrice = value; }
        }

        private decimal _purchasePriceWithoutVat;
        [DataMember]
        public decimal PurchasePriceWithoutVat
        {
            get { return _purchasePriceWithoutVat; }
            set { _purchasePriceWithoutVat = value; }
        }


        private decimal _vatAmount = 0.00M;
        [DataMember]
        public decimal VatAmount
        {
            get { return _vatAmount; }
            set { _vatAmount = value; }
        }

        private int _categoryId;
        [DataMember]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }


        private string _category;
        [DataMember]
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private string _categoryCode = string.Empty;
        [DataMember]
        public string CategoryCode
        {
            get { return _categoryCode; }
            set { _categoryCode = value; }
        }

        private decimal _unitPeriod;
        [DataMember]
        public decimal UnitPeriod
        {
            get { return _unitPeriod; }
            set { _unitPeriod = value; }
        }

        private string _periodType;
        [DataMember]
        public string PeriodType
        {
            get { return _periodType; }
            set { _periodType = value; }
        }

        private bool _isPeak;
        [DataMember]
        public bool IsPeak
        {
            get { return _isPeak; }
            set { _isPeak = value; }
        }


        private int _activityId;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private bool activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get { return activeStatus; }
            set { activeStatus = value; }
        }


        private bool _isArticleChecked = false;
        [DataMember]
        public bool IsArticleChecked
        {
            get { return _isArticleChecked; }
            set { _isArticleChecked = value; }
        }


        private int _sortCutKey = -1;
        [DataMember]
        public int SortCutKey
        {
            get { return _sortCutKey; }
            set { _sortCutKey = value; }
        }


        private int? _venderId = -1;
        [DataMember]
        public int? VenderId
        {
            get { return _venderId; }
            set { _venderId = value; }
        }

        private string _venderName = string.Empty;
         [DataMember]
        public string VenderName
        {
            get { return _venderName; }
            set { _venderName = value; }
        }


        private string _barCode = string.Empty;
        [DataMember]
        public string BarCode
        {
            get { return _barCode; }
            set { _barCode = value; }
        }

        private string _ledgerAccount = string.Empty;
        [DataMember]
        public string LedgerAccount
        {
            get { return _ledgerAccount; }
            set { _ledgerAccount = value; }
        }
        private int _stockLevel = 0;
        [DataMember]
        public int StockLevel
        {
            get { return _stockLevel; }
            set { _stockLevel = value; }
        }
        private int _reOrderLevel = 0;
        [DataMember]
        public int ReOrderLevel
        {
            get { return _reOrderLevel; }
            set { _reOrderLevel = value; }
        }
        private bool _stockStatus = false;
        [DataMember]
        public bool StockStatus
        {
            get { return _stockStatus; }
            set { _stockStatus = value; }
        }

        private bool _isNtstockStatus = false;
        [DataMember]
        public bool IsNtstockStatus
        {
            get { return _isNtstockStatus; }
            set { _isNtstockStatus = value; }
        }

        private bool _obsoleteStatus = false;
        [DataMember]
        public bool ObsoleteStatus
        {
            get { return _obsoleteStatus; }
            set { _obsoleteStatus = value; }
        }

        private bool _isSelected = false;
        [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private int? _vatCodeID = -1;
        [DataMember]
        public int? VatCodeID
        {
            get { return _vatCodeID; }
            set { _vatCodeID = value; }
        }

        private int? _revenueAccountId = -1;
        [DataMember]
        public int? RevenueAccountId
        {
            get { return _revenueAccountId; }
            set { _revenueAccountId = value; }
        }
        private string _revenueAccountNo = string.Empty;
        [DataMember]
        public string RevenueAccountNo
        {
            get { return _revenueAccountNo; }
            set { _revenueAccountNo = value; }
        }
        private string _revenueAccountName = string.Empty;
        [DataMember]
        public string RevenueAccountName
        {
            get { return _revenueAccountName; }
            set { _revenueAccountName = value; }
        }
        private int _nonVATAccountId = -1;
        [DataMember]
        public int NonVATAccountId
        {
            get { return _nonVATAccountId; }
            set { _nonVATAccountId = value; }
        }

        private string _articleType = string.Empty;
        [DataMember]
        public string ArticleType
        {
            get { return _articleType; }
            set { _articleType = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _vendorName = string.Empty;
        [DataMember]
        public string VendorName
        {
            get { return _vendorName; }
            set { _vendorName = value; }
        }

        private string _vendorNumber = string.Empty;
         [DataMember]
        public string VendorNumber
        {
            get { return _vendorNumber; }
            set { _vendorNumber = value; }
        }

        private bool _notifyAlert = false;
        [DataMember]
        public bool NotifyAlert
        {
            get { return _notifyAlert; }
            set { _notifyAlert = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }


        private string _currency = string.Empty;
        [DataMember]
        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }

        private string _vatCodeName = string.Empty;
        [DataMember]
        public string VatCodeName
        {
            get { return _vatCodeName; }
            set { _vatCodeName = value; }
        }

        private int vatCode = -1;
        [DataMember]
        public int VatCode
        {
            get { return vatCode; }
            set { vatCode = value; }
        }

        private decimal _vatRate = 0.00M;
        [DataMember]
        public decimal VatRate
        {
            get { return _vatRate; }
            set { _vatRate = value; }
        }

        private int _noOfMinutes = -1;
        [DataMember]
        public int NoOfMinutes
        {
            get { return _noOfMinutes; }
            set { _noOfMinutes = value; }
        }

        private decimal _vatStatus;
        [DataMember]
        public decimal VatStatus
        {
            get { return _vatStatus; }
            set { _vatStatus = value; }
        }

        private bool _isVatAdded;
        [DataMember]
        public bool IsVatAdded
        {
            get { return _isVatAdded; }
            set { _isVatAdded = value; }
        }

        #region Shop Sales
        #region Order Quantity
        private int _orderQuantity = -1;
        [DataMember]
        public int OrderQuantity
        {
            get { return _orderQuantity; }
            set { _orderQuantity = value; }
        }
        #endregion

        #region Discount
        private decimal _discount;
        [DataMember]
        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        #endregion

        #region Price
        private decimal _price;
        [DataMember]
        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }
        #endregion

        #region Selected Price
        private decimal _selectedPrice;
        [DataMember]
        public decimal SelectedPrice
        {
            get { return _selectedPrice; }
            set { _selectedPrice = value; }
        }
        #endregion

        #region Quantity
        private int _quantity = -1;
        [DataMember]
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        #endregion


        private int _stockCategoryId = -1;
        [DataMember]
        public int StockCategoryId
        {
            get { return _stockCategoryId; }
            set { _stockCategoryId = value; }
        }



        #endregion

        private bool _isVoucher;
        [DataMember]
        public bool IsVoucher
        {
            get { return _isVoucher; }
            set { _isVoucher = value; }
        }



        private int _unitId = -1;
        [DataMember]
        public int UnitId
        {
            get { return _unitId; }
            set { _unitId = value; }
        }


        private bool _isPunchCard;
        [DataMember]
        public bool IsPunchCard
        {
            get { return _isPunchCard; }
            set { _isPunchCard = value; }
        }



        private bool _isContractBooking;
        [DataMember]
        public bool IsContractBooking
        {
            get { return _isContractBooking; }
            set { _isContractBooking = value; }
        }

        private int _counted = -1;
        [DataMember]
        public int Counted
        {
            get { return _counted; }
            set { _counted = value; }
        }

        private int _deviation;
        [DataMember]
        public int Deviation
        {
            get { return _deviation; }
            set { _deviation = value; }
        }


        private decimal _netValue;
        [DataMember]
        public decimal NetValue
        {
            get { return _netValue; }
            set { _netValue = value; }
        }

        private int _inventoryDetailId;
        [DataMember]
        public int InventoryDetailId
        {
            get { return _inventoryDetailId; }
            set { _inventoryDetailId = value; }
        }

        private string _returnComment = string.Empty;
        [DataMember]
        public string ReturnComment
        {
            get { return _returnComment; }
            set { _returnComment = value; }
        }

        private bool _isStockAdd;
        [DataMember]
        public bool IsStockAdd
        {
            get { return _isStockAdd; }
            set { _isStockAdd = value; }
        }

        private int _articleSettingId;
         [DataMember]
        public int ArticleSettingId
        {
            get { return _articleSettingId; }
            set { _articleSettingId = value; }
        }

        private bool _isBelongToBranch;
        [DataMember]
        public bool IsBelongToBranch
        {
            get { return _isBelongToBranch; }
            set { _isBelongToBranch = value; }
        }

        private decimal _countedValue;
         [DataMember]
        public decimal CountedValue
        {
            get { return _countedValue; }
            set { _countedValue = value; }
        }

        private List<int> _branchIdList;
        [DataMember]
        public List<int> BranchIdList
        {
            get { return _branchIdList; }
            set { _branchIdList = value; }
        }

        private bool _isAdminUser;
        [DataMember]
        public bool IsAdminUser
        {
            get { return _isAdminUser; }
            set { _isAdminUser = value; }
        }

        private bool _isUpdate;
        [DataMember]
        public bool IsUpdate
        {
            get { return _isUpdate; }
            set { _isUpdate = value; }
        }

        private bool _isSave;
        [DataMember]
        public bool IsSave
        {
            get { return _isSave; }
            set { _isSave = value; }
        }

        private string _unitName = string.Empty;
          [DataMember]
        public string UnitName
        {
            get { return _unitName; }
            set { _unitName = value; }
        }

         private bool _isError;
         [DataMember]
        public bool IsError
        {
            get { return _isError; }
            set { _isError = value; }
        }

        private string _voucherNumber = string.Empty;
        [DataMember]
        public string VoucherNumber
        {
            get { return _voucherNumber; }
            set { _voucherNumber = value; }
        }
        private DateTime? _expiryDate;
        [DataMember]
        public DateTime? ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }

        private int _memberId;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }
    }
}
