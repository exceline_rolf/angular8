﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class RenewSummaryDC
    {
        private int _id=-1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _noOfMonths=0;
        [DataMember]
        public int NoOfMonths
        {
            get { return _noOfMonths; }
            set { _noOfMonths = value; }
        }


        private decimal _renewContractPrice = 0;
        [DataMember]
        public decimal  RenewContractPrice
        {
            get { return _renewContractPrice; }
            set { _renewContractPrice = value; }
        }



        private int _templateId=-1;
        [DataMember]
        public int TemplateId
        {
            get { return _templateId; }
            set { _templateId = value; }
        }


        private int _memeberId = -1;
        [DataMember]
        public int MemeberId
        {
            get { return _memeberId; }
            set { _memeberId = value; }
        }


        private int _noOfOrders=0;
        [DataMember]
        public int NoOfOrders
        {
            get { return _noOfOrders; }
            set { _noOfOrders = value; }
        }


        private DateTime ? _lockInUntilDate;
        [DataMember]
        public DateTime? LockInUntilDate
        {
            get { return _lockInUntilDate; }
            set { _lockInUntilDate = value; }
        }

        private DateTime?_renewEffectiveDate;
        [DataMember]
        public DateTime? RenewEffectiveDate
        {
            get { return _renewEffectiveDate; }
            set { _renewEffectiveDate = value; }
        }

        private DateTime? _priceGuarantyUntilDate;
        [DataMember]
        public DateTime? PriceGuarantyUntilDate
        {
            get { return _priceGuarantyUntilDate; }
            set { _priceGuarantyUntilDate = value; }
        }

        private int _lockinPeriod=0;
        [DataMember]
        public int LockinPeriod
        {
            get { return _lockinPeriod; }
            set { _lockinPeriod = value; }
        }

        private int _priceGuarantyPeriod=0;
        [DataMember]
        public int PriceGuarantyPeriod
        {
            get { return _priceGuarantyPeriod; }
            set { _priceGuarantyPeriod = value; }
        }

        private int _memberContractId=-1;
        [DataMember]
        public int MemberContractId
        {
            get { return _memberContractId; }
            set { _memberContractId = value; }
        }

        private List<ContractItemDC> _itemList = new List<ContractItemDC>();
         [DataMember]
        public List<ContractItemDC> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }
    }
}
