﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 4/30/2012 8:42:09 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin
{
    [DataContract]
    public class InventoryItemDC
    {
        #region ID
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        #endregion

        #region Name
        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        #endregion

        #region item Code
        private string _itemCode = string.Empty;
        [DataMember]
        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }
        #endregion

        #region Is Active
        private bool _isActive = false;
        [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        #endregion

        #region Notify Alert
        private bool _notifyAlert = false;
        [DataMember]
        public bool NotifyAlert
        {
            get { return _notifyAlert; }
            set { _notifyAlert = value; }
        }
        #endregion

        #region PurchasedPrice
        private decimal _purchasedPrice;
        [DataMember]
        public decimal PurchasedPrice
        {
            get { return _purchasedPrice; }
            set { _purchasedPrice = value; }
        }
        #endregion

        #region Unit Price For Employee
        private decimal _unitPriceForEmployee = 0.00M;
        [DataMember]
        public decimal UnitPriceForEmployee
        {
            get { return _unitPriceForEmployee; }
            set { _unitPriceForEmployee = value; }
        }
        #endregion

        #region Unit Price For Customers
        private decimal _unitPriceForCustomers = 0.00M;
        [DataMember]
        public decimal UnitPriceForCustomers
        {
            get { return _unitPriceForCustomers; }
            set { _unitPriceForCustomers = value; }
        }

        private decimal _unitPrice;
        [DataMember]
        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set { _unitPrice = value; }
        }
        #endregion

        #region Quantity
        private int _quantity = -1;
        [DataMember]
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
        #endregion

        #region Category ID
        private int _categoryId = -1;
        [DataMember]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }
        #endregion

        #region Category Name
        private string _categoryName = string.Empty;
        [DataMember]
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }
        #endregion

        #region Brach ID
        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }
        #endregion

        #region Re Oreder Level
        private int _reOrderlevel = -1;
        [DataMember]
        public int ReOrderLevel
        {
            get { return _reOrderlevel; }
            set { _reOrderlevel = value; }
        }
        #endregion

        #region Received Date
        private DateTime _receivedDate;
        [DataMember]
        public DateTime ReceivedDate
        {
            get { return _receivedDate; }
            set { _receivedDate = value; }
        }
        #endregion

        #region Expire Date
        private DateTime _expireDate;
        [DataMember]
        public DateTime ExpireDate
        {
            get { return _expireDate; }
            set { _expireDate = value; }
        }
        #endregion

        #region Discount
        private double _discount;
        [DataMember]
        public double Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        #endregion

        #region Price
        private decimal _price;
        [DataMember]
        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }
        #endregion

        #region Order Quantity
        private int _orderQuantity;
        [DataMember]
        public int OrderQuantity
        {
            get { return _orderQuantity; }
            set { _orderQuantity = value; }
        }
        #endregion

        #region Selected Price
        private decimal _selectedPrice;
        [DataMember]
        public decimal SelectedPrice
        {
            get { return _selectedPrice; }
            set { _selectedPrice = value; }
        }
        #endregion

        #region Is Item Checked
        private bool _isItemChecked;
        [DataMember]
        public bool IsItemChecked
        {
            get { return _isItemChecked; }
            set { _isItemChecked = value; }
        }

        #endregion

        #region Discount Start Date
        private DateTime _discountStartDate;
        [DataMember]
        public DateTime DiscountStartDate
        {
            get { return _discountStartDate; }
            set { _discountStartDate = value; }
        }
        #endregion

        #region Discount End Date
        private DateTime _discountEndDate;
        [DataMember]
        public DateTime DiscountEndDate
        {
            get { return _discountEndDate; }
            set { _discountEndDate = value; }
        }

       

        #endregion

        private DateTime? _startDate;
        [DataMember]
        public DateTime? StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime? _approvedDate;
        [DataMember]
        public DateTime? ApprovedDate
        {
            get { return _approvedDate; }
            set { _approvedDate = value; }
        }

        private decimal _netValue;
        [DataMember]
        public decimal NetValue
        {
            get { return _netValue; }
            set { _netValue = value; }
        }
    }
}
