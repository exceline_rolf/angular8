﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Admin.ManageEmployees
{
    [DataContract]
    public class GymEmployeeApprovalDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
          get { return _id; }
          set { _id = value; }
        }

        private int _employeeId = -1;
        [DataMember]
        public int EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }

        private int _scheduleItemId = -1;
        [DataMember]
        public int ScheduleItemId
        {
            get { return _scheduleItemId; }
            set { _scheduleItemId = value; }
        }

        private string _day = string.Empty;
        [DataMember]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private DateTime _startTime = DateTime.MinValue;
        [DataMember]
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime _endTime = DateTime.MinValue;
        [DataMember]
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private DateTime _date = DateTime.MinValue;
        [DataMember]
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private string _approvalType = string.Empty;
        [DataMember]
        public string ApprovalType
        {
            get { return _approvalType; }
            set { _approvalType = value; }
        }

        private bool _isEnabledApprove ;
          [DataMember]
        public bool IsEnabledApprove
        {
            get { return _isEnabledApprove; }
            set { _isEnabledApprove = value; }
        }

        private int _branchId;
         [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

         private string _branchName = string.Empty;
         [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        



    }
}
