﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Admin.ManageEmployees
{
    [DataContract]
    public  class GymEmployeeWorkPlanDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
          get { return _id; }
          set { _id = value; }
        }
        private string _day = string.Empty;
        [DataMember]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private DateTime _startDate = DateTime.MinValue;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime _endDate = DateTime.MinValue;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private DateTime _startTime = DateTime.MinValue;
        [DataMember]
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime _endTime = DateTime.MinValue;
        [DataMember]
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private DateTime _date = DateTime.MinValue;
        [DataMember]
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private bool isApproved = false;
        [DataMember]
        public bool IsApproved
        {
            get { return isApproved; }
            set { isApproved = value; }
        }

        private string _resource = string.Empty;
        [DataMember]
        public string Resource
        {
            get { return _resource; }
            set { _resource = value; }
        }

        private bool _isSalaryPerBooking = false;
        [DataMember]
        public bool IsSalaryPerBooking
        {
            get { return _isSalaryPerBooking; }
            set { _isSalaryPerBooking = value; }
        }

        private string _approveButtonContent = string.Empty;
        [DataMember]
        public string ApproveButtonContent
        {
            get { return _approveButtonContent; }
            set { _approveButtonContent = value; }
        }

        private int _weekType = -1;
        [DataMember]
        public int WeekType
        {
            get { return _weekType; }
            set { _weekType = value; }
        }

        private bool _isDefault = false;
        [DataMember]
        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }

        private List<EmployeeBookingDC> _bookingList = new List<EmployeeBookingDC>();
        [DataMember]
        public List<EmployeeBookingDC> BookingList
        {
            get { return _bookingList; }
            set { _bookingList = value; }
        }

        private int _branchId;
         [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private bool _isEnabledApprove;
          [DataMember]
        public bool IsEnabledApprove
        {
            get { return _isEnabledApprove; }
            set { _isEnabledApprove = value; }
        }

       
    }
}
