﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.GMS.Core.DomainObjects.Admin.ManageEmployees
{
    [DataContract]
    public class InstructorDC : EmployeeDC
    {

        private int _id = -1;
        [DataMember]
        public override int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _entNo = -1;
        [DataMember]
        public override int EntNo
        {
            get { return _entNo; }
            set { _entNo = value; }
        }

        private string _entityRoleType;
        [DataMember]
        public override string EntityRoleType
        {
            get { return _entityRoleType; }
            set { _entityRoleType = value; }
        }

        private byte[] _profilePicture = null;
        [DataMember]
        public override byte[] ProfilePicture
        {
            get { return _profilePicture; }
            set { _profilePicture = value; }
        }

        private CategoryDC _inscategory = new CategoryDC();
        [DataMember]
        public override CategoryDC Category
        {
            get { return _inscategory; }
            set { _inscategory = value; }
        }

        private string _firstName = string.Empty;
        [DataMember]
        public override string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }

        private string _lastName = string.Empty;
        [DataMember]
        public override string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }

        private string _email = string.Empty;
        [DataMember]
        public override string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }

        private string _mobile = string.Empty;
        [DataMember]
        public override string Mobile
        {
            get
            {
                return _mobile;
            }
            set
            {
                _mobile = value;
            }
        }

        private string _work = string.Empty;
        [DataMember]
        public override string Work
        {
            get
            {
                return _work;
            }
            set
            {
                _work = value;
            }
        }

        private string _privatePhone = string.Empty;
        [DataMember]
        public override string Private
        {
            get
            {
                return _privatePhone;
            }
            set
            {
                _privatePhone = value;
            }
        }

        private string _description = string.Empty;
        [DataMember]
        public override string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        private string _gender = string.Empty;
        [DataMember]
        public override string Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value;
            }
        }

        DateTime _birthDay = new DateTime();
        [DataMember]
        public override DateTime BirthDay
        {
            get
            {
                return _birthDay;
            }
            set
            {
                _birthDay = value;
            }
        }

        private int _age =-1;
        [DataMember]
        public override int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }


        private string _postCode = string.Empty;
        [DataMember]
        public override string PostCode
        {
            get
            {
                return _postCode;
            }
            set
            {
                _postCode = value;
            }
        }

        private string _city = string.Empty;
        [DataMember]
        public override string City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value;
            }
        }

        private int _personNo =-1;
        [DataMember]
        public override int PersonNo
        {
            get
            {
                return _personNo;
            }
            set
            {
                _personNo = value;
            }
        }

        private string _roleId = string.Empty;
        [DataMember]
        public override string RoleId
        {
            get
            {
                return _roleId;
            }
            set
            {
                _roleId = value;
            }
        }

        private string _postPlace = string.Empty;
        [DataMember]
        public override string PostPlace
        {
            get
            {

                return _postPlace;
            }
            set
            {
                _postPlace = value;
            }
        }

        private int _branchId = -1;
        [DataMember]
        public override int BranchId
        {
            get
            {
                return _branchId;
            }
            set
            {
                _branchId = value;

            }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get
            {
                return _branchName;
            }
            set
            {
                _branchName = value;

            }
        }


        private DateTime _createdDate;
        [DataMember]
        public override DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        private DateTime _lastModifiedDate;
        [DataMember]
        public override DateTime LastModifiedDate
        {
            get
            {
                return _lastModifiedDate;
            }
            set
            {
                _lastModifiedDate = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public override string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _lastModifiedUser = string.Empty;
        [DataMember]
        public override string LastModifiedUser
        {
            get
            {
                return _lastModifiedUser;
            }
            set
            {
                _lastModifiedUser = value;
            }
        }

        private bool _activeStatus=true;
        [DataMember]
        public override bool ActiveStatus
        {
            get
            {
                return _activeStatus;
            }
            set
            {
                _activeStatus = value;
            }
        }

        private string _name = string.Empty;
        [DataMember]
        public override string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        private string _address1 = string.Empty;
        [DataMember]
        public override string Address1
        {
            get
            {
                return _address1;
            }
            set
            {
                _address1 = value;
            }
        }
        private string _address2 = string.Empty;
        [DataMember]
        public override string Address2
        {
            get
            {
                return _address2;
            }
            set
            {
                _address2 = value;
            }
        }
        private string _address3 = string.Empty;
        [DataMember]
        public override string Address3
        {
            get
            {
                return _address3;
            }
            set
            {
                _address3 = value;
            }
        }

        private ScheduleDC _schedule = new ScheduleDC();
        [DataMember]
        public override ScheduleDC Schedule
        {
            get
            {
                return _schedule;
            }
            set
            {
                _schedule = value;
            }
        }

        private bool _isSelected = false;
        [DataMember]
        public override bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private string _imagePath = string.Empty;
        [DataMember]
        public override string ImagePath
        {
            get { return _imagePath; }
            set { _imagePath = value; }
        }

        private List<ActivityDC> _activityList = new List<ActivityDC>();
        [DataMember]
        public override List<ActivityDC> ActivityList
        {
            get
            {
                return _activityList;
            }
            set
            {
                _activityList = value;
            }
        }

        private bool _isAssign;
        [DataMember]
        public override bool IsAssign
        {
            get
            {
                return _isAssign;
            }
            set
            {
                _isAssign = value;
            }
        }


        private List<string> _roleItemList;
        [DataMember]
        public override List<string> RoleItemList
        {
            get
            {
                return _roleItemList;
            }
            set
            {
                _roleItemList = value;
            }
        }

        private List<RoleActivityDC> _roleActivityList;
        [DataMember]
        public override List<RoleActivityDC> RoleActivityList
        {
            get
            {
                return _roleActivityList;
            }
            set
            {
                _roleActivityList = value;
            }
        }

        private int _assignedEntityId;
        [DataMember]
        public override int AssignedEntityId
        {
            get
            {
                return _assignedEntityId;
            }
            set
            {
                _assignedEntityId = value;
            }
        }

        private string _commentForInactive = string.Empty;
        [DataMember]
        public override string CommentForInactive
        {
            get
            {
                return _commentForInactive;
            }
            set
            {
                _commentForInactive = value;
            }
        }

        //public DateTime _inactivatedStartDate;
        //[DataMember]
        //public override DateTime InactivatedStartDate
        //{
        //    get
        //    {
        //        return _inactivatedStartDate;
        //    }
        //    set
        //    {
        //        _inactivatedStartDate = value;
        //    }
        //}

        //public DateTime _inactivatedEndDate;
        //[DataMember]
        //public override DateTime InactivatedEndDate
        //{
        //    get
        //    {
        //        return _inactivatedEndDate;
        //    }
        //    set
        //    {
        //        _inactivatedEndDate = value;
        //    }
        //}

        public bool _isHasTask;
        [DataMember]
        public override bool IsHasTask
        {
            get
            {
                return _isHasTask;
            }
            set
            {
                _isHasTask = value;
            }
        }

        public int _sourceEntity;
        [DataMember]
        public override int SourceEntity
        {
            get
            {
                return _sourceEntity;
            }
            set
            {
                _sourceEntity = value;
            }
        }

        public string _custId = string.Empty;
        [DataMember]
        public override string CustId
        {
            get
            {
                return _custId;
            }
            set
            {
                _custId = value;
            }
        }

        private bool _isNotValid;
        [DataMember]
        public bool IsNotValid
        {
            get { return _isNotValid; }
            set { _isNotValid = value; }
        }


        private bool _isReadOnly;
         [DataMember]
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set { _isReadOnly = value; }
        }
    }
}
