﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/29/2012 15:35:43
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin
{
    [DataContract]
    public class ExcelineBranchDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _entNo = -1;
        [DataMember]
        public int EntNo
        {
            get { return _entNo; }
            set { _entNo = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private int _groupId = -1;
        [DataMember]
        public int GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        private string _groupName = string.Empty;
        [DataMember]
        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }

        private DateTime _registeredDate = new DateTime();
        [DataMember]
        public DateTime RegisteredDate
        {
            get { return _registeredDate; }
            set { _registeredDate = value; }
        }

        private string _addr1 = string.Empty;
        [DataMember]
        public string Addr1
        {
            get { return _addr1; }
            set { _addr1 = value; }
        }

        private string _addr2 = string.Empty;
        [DataMember]
        public string Addr2
        {
            get { return _addr2; }
            set { _addr2 = value; }
        }

        private string _addr3 = string.Empty;
        [DataMember]
        public string Addr3
        {
            get { return _addr3; }
            set { _addr3 = value; }
        }

        private string _zipCode = string.Empty;
        [DataMember]
        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        private string _zipName = string.Empty ;
        [DataMember]
        public string ZipName
        {
            get { return _zipName; }
            set { _zipName = value; }
        }

        private int _creditorCollectionId = -1;
        [DataMember]
        public int CreditorCollectionId
        {
            get { return _creditorCollectionId; }
            set { _creditorCollectionId = value; }
        }

        private string _countryId = string.Empty;
        [DataMember]
        public string CountryId
        {
            get { return _countryId; }
            set { _countryId = value; }
        }

        private string _region = String.Empty;
        [DataMember]
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        private string _telWork = string.Empty;
        [DataMember]
        public string TelWork
        {
            get { return _telWork; }
            set { _telWork = value; }
        }

        private string _email = string.Empty;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _fax = string.Empty;
        [DataMember]
        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        private string _bankAccountNo = string.Empty;
        [DataMember]
        public string BankAccountNo
        {
            get { return _bankAccountNo; }
            set { _bankAccountNo = value; }
        }

        private string _kidSwapAccountNo = string.Empty;
        [DataMember]
        public string KidSwapAccountNo
        {
            get { return _kidSwapAccountNo; }
            set { _kidSwapAccountNo = value; }
        }

        private string _collectionAccountNo = string.Empty;
        [DataMember]
        public string CollectionAccountNo
        {
            get { return _collectionAccountNo; }
            set { _collectionAccountNo = value; }
        }

        private string _registeredNo = string.Empty;
        [DataMember]
        public string RegisteredNo
        {
            get { return _registeredNo; }
            set { _registeredNo = value; }
        }

        private bool _isParentGroup = false;
        [DataMember]
        public bool IsParentGroup
        {
            get { return _isParentGroup; }
            set { _isParentGroup = value; }
        }

        private bool _isCheck = false;
        [DataMember]
        public bool IsCheck
        {
            get { return _isCheck; }
            set { _isCheck = value; }
        }

        private bool _isEnabled = true;
        [DataMember]
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; }
        }

        private bool _isExpressGym = false;
        [DataMember]
        public bool IsExpressGym
        {
            get { return _isExpressGym; }
            set { _isExpressGym = value; }
        }

        private int _regionId = 0;
        [DataMember]
        public int RegionId
        {
            get { return _regionId; }
            set { _regionId = value; }
        }

        private string _officialName;
        [DataMember]
        public string OfficialName
        {
            get { return _officialName; }
            set { _officialName = value; }
        }

        private string _web;
        [DataMember]
        public string Web
        {
          get { return _web; }
          set { _web = value; }
        }

        private string _countryMobilePrefix;
        [DataMember]
        public string CountryMobilePrefix
        {
            get { return _countryMobilePrefix; }
            set { _countryMobilePrefix = value; }
        }

        private string _rowColour = "#ff994848";
         [DataMember]
        public string RowColour
        {
            get { return _rowColour; }
            set { _rowColour = value; }
        }

        private int _area = 0;
        [DataMember]
        public int Area
        {
            get { return _area; }
            set { _area = value; }
        }

        private string _otherAdminMobile = string.Empty;
         [DataMember]
        public string OtherAdminMobile
        {
            get { return _otherAdminMobile; }
            set { _otherAdminMobile = value; }
        }


        private string _otherAdminEmail = string.Empty;
          [DataMember]
        public string OtherAdminEmail
        {
            get { return _otherAdminEmail; }
            set { _otherAdminEmail = value; }
        }

          private string _bureauAccountNO = string.Empty;
          [DataMember]
          public string BureauAccountNO
          {
              get { return _bureauAccountNO; }
              set { _bureauAccountNO = value; }
          }

          private string _reorderSMSReceiver = string.Empty;
          [DataMember]
          public string ReorderSmsReceiver
          {
              get { return _reorderSMSReceiver; }
              set { _reorderSMSReceiver = value; }
          }
          private int _companyIdSalary;
          [DataMember]
          public int CompanyIdSalary
          {
              get { return _companyIdSalary; }
              set { _companyIdSalary = value; }
          }


    }
}
