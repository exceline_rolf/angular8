﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.Admin.ManageAnonymizing
{
    [DataContract]
    public class ExceAnonymizingDC
    {
      

        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _code = string.Empty;
        [DataMember]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

    }
}
