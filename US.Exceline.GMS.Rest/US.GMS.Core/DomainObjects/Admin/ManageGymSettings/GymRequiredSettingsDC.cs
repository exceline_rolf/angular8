﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class GymRequiredSettingsDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private bool _isExpressGym;
        [DataMember]
        public bool IsExpressGym
        {
            get { return _isExpressGym; }
            set { _isExpressGym = value; }
        }



        private bool _memberRequired = false;
        [DataMember]
        public bool MemberRequired
        {
            get { return _memberRequired; }
            set { _memberRequired = value; }
        }

        private bool _mobileRequired = false;
        [DataMember]
        public bool MobileRequired
        {
            get { return _mobileRequired; }
            set { _mobileRequired = value; }
        }

        private bool _addressRequired = false;
        [DataMember]
        public bool AddressRequired
        {
            get { return _addressRequired; }
            set { _addressRequired = value; }
        }

        private bool _zipCodeRequired = false;
        [DataMember]
        public bool ZipCodeRequired
        {
            get { return _zipCodeRequired; }
            set { _zipCodeRequired = value; }
        }

        private string _region = string.Empty;
        [DataMember]
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        
    }
}
