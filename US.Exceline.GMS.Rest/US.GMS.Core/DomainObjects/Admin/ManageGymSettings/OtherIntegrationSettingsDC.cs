﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    public class OtherIntegrationSettingsDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _serviceBaseUrl = string.Empty;
        [DataMember]
        public string ServiceBaseUrl
        {
            get { return _serviceBaseUrl; }
            set { _serviceBaseUrl = value; }
        }

        private string _userName = string.Empty;
        [DataMember]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private string _password = string.Empty;
        [DataMember]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private string _apiKey = string.Empty;
        [DataMember]
        public string ApiKey
        {
            get { return _apiKey; }
            set { _apiKey = value; }
        }

        private string _facilityUrl = string.Empty;
        [DataMember]
        public string FacilityUrl
        {
            get { return _facilityUrl; }
            set { _facilityUrl = value; }
        }

        private string _systemName = string.Empty;
        [DataMember]
        public string SystemName
        {
            get { return _systemName; }
            set { _systemName = value; }
        }

        private string _user = string.Empty;
        [DataMember]
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        private string _guid;
         [DataMember]
        public string Guid
        {
            get { return _guid; }
            set { _guid = value; }
        }

      
    }
}
