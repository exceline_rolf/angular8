﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class ExceVatDC
    {
        private int _vatCodeCategoryId = -1;
        [DataMember]
        public int VatCodeCategoryId
        {
            get { return _vatCodeCategoryId; }
            set { _vatCodeCategoryId = value; }
        }

        private string _vatCode = String.Empty;
        [DataMember]
        public string VatCode
        {
            get { return _vatCode; }
            set { _vatCode = value; }
        }

        private decimal _vatAmount = 0;
        [DataMember]
        public decimal VatAmount
        {
            get { return _vatAmount; }
            set { _vatAmount = value; }
        }
    }
}
