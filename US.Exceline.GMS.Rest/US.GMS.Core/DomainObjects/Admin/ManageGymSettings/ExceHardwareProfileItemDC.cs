﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class ExceHardwareProfileItemDC
    {
        

        private int _id;
        [DataMember ]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _hardwareProfileId;
        [DataMember]
        public int HardwareProfileId
        {
            get { return _hardwareProfileId; }
            set { _hardwareProfileId = value; }
        }

        private int _categoryId;
        [DataMember]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }

        private string _categoryName;
        [DataMember]
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

        private string _code;
        [DataMember]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
         
       
    }
}
