﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class ExceHardwareProfileDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private decimal _changeMoney;
        [DataMember]
        public decimal ChangeMoney
        {
            get { return _changeMoney; }
            set { _changeMoney = value; }
        }


        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private bool _isExpress = false;
        [DataMember]
        public bool IsExpress
        {
            get { return _isExpress; }
            set { _isExpress = value; }
        }


        private bool _isDelete;
        [DataMember]
        public bool IsDelete
        {
            get { return _isDelete; }
            set { _isDelete = value; }
        }

        private List<ExceHardwareProfileItemDC> _hardwareProfileItemList = new List<ExceHardwareProfileItemDC>();
        [DataMember]
        public List<ExceHardwareProfileItemDC> HardwareProfileItemList
        {
            get { return _hardwareProfileItemList; }
            set { _hardwareProfileItemList = value; }
        }

        private string _categoryListString = string.Empty;
        [DataMember]
        public string CategoryListString
        {
            get { return _categoryListString; }
            set { _categoryListString = value; }
        }

        private string _region = string.Empty;
         [DataMember]
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

         private bool _canDelete = true;
         [DataMember]
         public bool CanDelete
         {
             get { return _canDelete; }
             set { _canDelete = value; }
         }
    }
}
