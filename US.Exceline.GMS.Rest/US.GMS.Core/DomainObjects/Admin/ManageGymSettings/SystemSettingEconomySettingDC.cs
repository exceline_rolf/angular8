﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class SystemSettingEconomySettingDC
    {
        private int _iD = -1;
        [DataMember]
        public int ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        private string _creditPeriod = string.Empty;
        [DataMember]
        public string CreditPeriod
        {
            get { return _creditPeriod; }
            set { _creditPeriod = value; }
        }

        private bool _oneDueDateAtDate = false;
        [DataMember]
        public bool OneDueDateAtDate
        {
            get { return _oneDueDateAtDate; }
            set { _oneDueDateAtDate = value; }
        }

        private string _fixedDueDay = string.Empty;
        [DataMember]
        public string FixedDueDay
        {
            get { return _fixedDueDay; }
            set { _fixedDueDay = value; }
        }

        private string _sponsoringDay = string.Empty;
        [DataMember]
        public string SponsoringDay
        {
            get { return _sponsoringDay; }
            set { _sponsoringDay = value; }
        }

        private string _sponsoredDueDate = string.Empty;
        [DataMember]
        public string SponsoredDueDate
        {
            get { return _sponsoredDueDate; }
            set { _sponsoredDueDate = value; }
        }

        private int _sponsorDueDay = 0;
        [DataMember]
        public int SponsorDueDay
        {
            get { return _sponsorDueDay; }
            set { _sponsorDueDay = value; }
        }

        private string _sponsorInvoiceText = string.Empty;
        [DataMember]
        public string SponsorInvoiceText
        {
            get { return _sponsorInvoiceText; }
            set { _sponsorInvoiceText = value; }
        }

        private string _contractAutoRenewalDays = string.Empty;
        [DataMember]
        public string ContractAutoRenewalDays
        {
            get { return _contractAutoRenewalDays; }
            set { _contractAutoRenewalDays = value; }
        }

        private bool _isSMSInvoice;
        [DataMember]
        public bool IsSMSInvoice
        {
            get { return _isSMSInvoice; }
            set { _isSMSInvoice = value; }
        }

        private bool _deviationFollowup = false;
        [DataMember]
        public bool DeviationFollowup
        {
            get { return _deviationFollowup; }
            set { _deviationFollowup = value; }
        }

        private bool _deviationFollowupCompleted = false;
        [DataMember]
        public bool DeviationFollowupCompleted
        {
            get { return _deviationFollowupCompleted; }
            set { _deviationFollowupCompleted = value; }
        }

        private int _reminderDueDays = 0;
        [DataMember]
        public int ReminderDueDays
        {
            get { return _reminderDueDays; }
            set { _reminderDueDays = value; }
        }
    }
}
