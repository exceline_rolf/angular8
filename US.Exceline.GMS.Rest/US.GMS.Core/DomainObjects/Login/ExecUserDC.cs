﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Login
{

    [DataContract]
    public class ExecUserDC
    {
        private int _userID = -1;

        [DataMember]
        public int UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _userName = string.Empty;

        [DataMember]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private string _displayName = string.Empty;
        [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
        private List<int> _branches;
        [DataMember]
        public List<int> Branches
        {
            get { return _branches; }
            set { _branches = value; }
        }

        private string _email = string.Empty;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _password = string.Empty;
        [DataMember]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private bool _isLoginFirstTime = false;
        [DataMember]
        public bool IsLoginFirstTime
        {
            get { return _isLoginFirstTime; }
            set { _isLoginFirstTime = value; }
        }

        private string _cardNumber = string.Empty;
        [DataMember]
        public string CardNumber
        {
            get { return _cardNumber; }
            set { _cardNumber = value; }
        }

        private int _hardwareProfileId = -1;
        [DataMember]
        public int HardwareProfileId
        {
            get { return _hardwareProfileId; }
            set { _hardwareProfileId = value; }
        }

        private string _machineName = string.Empty;
        [DataMember]
        public string MachineName
        {
            get { return _machineName; }
            set { _machineName = value; }
        }

        private int _employeeId = -1;
        [DataMember]
        public int EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }

        private int _roleId;
         [DataMember]
        public int RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

        private bool _myWelliness;
        [DataMember]
        public bool MyWelliness
        {
            get { return _myWelliness; }
            set { _myWelliness = value; }
        }

        private bool _exor;
        [DataMember]
        public bool Exor
        {
            get { return _exor; }
            set { _exor = value; }
        }

        private bool _myWorkout;
        [DataMember]
        public bool MyWorkout
        {
            get { return _myWorkout; }
            set { _myWorkout = value; }
        }

        private bool _ifogym;
        [DataMember]
        public bool Inforgym
        {
            get { return _ifogym; }
            set { _ifogym = value; }
        }

        private bool _boostCommunication;
        [DataMember]
        public bool BoostCommunication
        {
            get { return _boostCommunication; }
            set { _boostCommunication = value; }
        }

        private bool _tripletex;
        [DataMember]
        public bool Tripletex
        {
            get { return _tripletex; }
            set { _tripletex = value; }
        }

        private bool _visima;
        [DataMember]
        public bool Visma
        {
            get { return _visima; }
            set { _visima = value; }
        }

        private bool _uniMicro;
        [DataMember]
        public bool UniMicro
        {
            get { return _uniMicro; }
            set { _uniMicro = value; }
        }

        private bool _documentScanner;
        [DataMember]
        public bool DocumentScanner
        {
            get { return _documentScanner; }
            set { _documentScanner = value; }
        }

        private bool _bankTerminalIntegrated;
        [DataMember]
        public bool BankTerminalIntegrated
        {
            get { return _bankTerminalIntegrated; }
            set { _bankTerminalIntegrated = value; }
        }

        private bool _receiptPrinter;
        [DataMember]
        public bool ReceiptPrinter
        {
            get { return _receiptPrinter; }
            set { _receiptPrinter = value; }
        }

        private bool _followUp;
        [DataMember]
        public bool FollowUp
        {
            get { return _followUp; }
            set { _followUp = value; }
        }

        private int _numberOfActivities;
        [DataMember]
        public int NumberOfActivites
        {
            get { return _numberOfActivities; }
            set { _numberOfActivities = value; }
        }
        
        private bool _recruitment;
        [DataMember]
            public bool Recruitment
        {
            get { return _recruitment; }
            set { _recruitment = value; }
        }

        private bool _excelineAccess;
        [DataMember]
        public bool ExcelineAccess
        {
            get { return _excelineAccess; }
            set { _excelineAccess = value; }
        }

        private bool _gantnerAccess;
        [DataMember]
        public bool GantnerAccess
        {
            get { return _gantnerAccess; }
            set { _gantnerAccess = value; }
        }

        private bool _gantnerVending;
        [DataMember]
        public bool GantnerVending
        {
            get { return _gantnerVending; }
            set { _gantnerVending = value; }
        }

        private bool _gantnerTime;
        [DataMember]
        public bool GantnerTime
        {
            get { return _gantnerTime; }
            set { _gantnerTime = value; }
        }

        private string _culture = string.Empty;
        [DataMember]
        public string Culture
        {
            get { return _culture; }
            set { _culture = value; }
        }
    }
}
