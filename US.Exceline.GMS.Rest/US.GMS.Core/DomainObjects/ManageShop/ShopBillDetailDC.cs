﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageShop
{
    [DataContract]
    public class ShopBillDetailDC
    {
        private int _salseId = -1;
        private int _branch = -1;
        private int _aritemNo = -1;
        private string _invoiceNo = string.Empty;
        private int _memberId = -1;
        private string _custId = string.Empty;
        private List<ShopSalesItemDC> _salesItemList = new List<ShopSalesItemDC>();
        private List<ShopSalesPaymentDC> _salesPaymentList = new List<ShopSalesPaymentDC>();

        [DataMember]
        public int SalseId
        {
            get { return _salseId; }
            set { _salseId = value; }
        }

        [DataMember]
        public int Branch
        {
            get { return _branch; }
            set { _branch = value; }
        }

        [DataMember]
        public int AritemNo
        {
            get { return _aritemNo; }
            set { _aritemNo = value; }
        }

        [DataMember]
        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        [DataMember]
        public string CustId
        {
            get { return _custId; }
            set { _custId = value; }
        }

        [DataMember]
        public List<ShopSalesItemDC> SalesItemList
        {
            get { return _salesItemList; }
            set { _salesItemList = value; }
        }

        [DataMember]
        public List<ShopSalesPaymentDC> SalesPaymentList
        {
            get { return _salesPaymentList; }
            set { _salesPaymentList = value; }
        }
    }
}
