﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageShop
{
    [DataContract]
    public  class ReturnItemDC
    {
        #region Id
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        #endregion

        #region Article Id
        private int _articleId = -1;
        [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }
        #endregion

        #region Description
        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        #endregion

        #region Quantity
        private int _quantity = -1;
        [DataMember]
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
        #endregion

        #region Return Date
        private DateTime _returnDate;
        [DataMember]
        public DateTime ReturnDate
        {
            get { return _returnDate; }
            set { _returnDate = value; }
        }
        #endregion

        #region Comment
        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }
        #endregion

        #region Category Id
        private int _categoryId = -1;
        [DataMember]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }
        #endregion

        #region Short Cut Key
        private int _shortCutKey = -1;
        [DataMember]
        public int ShortCutKey
        {
            get { return _shortCutKey; }
            set { _shortCutKey = value; }
        }
        #endregion

        private int _branchId;
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }
    }
}
