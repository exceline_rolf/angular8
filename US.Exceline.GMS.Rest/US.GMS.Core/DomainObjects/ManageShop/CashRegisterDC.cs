﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.ManageShop
{
    [DataContract]
    public class CashRegisterDC
    {
        private int _shopSalesId = -1;
        [DataMember]
        public int ShopSalesId
        {
            get { return _shopSalesId; }
            set { _shopSalesId = value; }
        }

        private int _entitiyId = -1;
        [DataMember]
        public int EntitiyId
        {
            get { return _entitiyId; }
            set { _entitiyId = value; }
        }

        private string _entityRoleType = string.Empty;
        [DataMember]
        public string EntityRoleType
        {
            get { return _entityRoleType; }
            set { _entityRoleType = value; }
        }

        private int _salesPointId = -1;
        [DataMember]
        public int SalesPointId
        {
            get { return _salesPointId; }
            set { _salesPointId = value; }
        }

        private string _userName = string.Empty;
        [DataMember]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }
        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }
        private string _machinename = string.Empty;
        [DataMember]
        public string MachineName
        {
            get { return _machinename; } 
            set { _machinename = value; }
        }
    }
}
