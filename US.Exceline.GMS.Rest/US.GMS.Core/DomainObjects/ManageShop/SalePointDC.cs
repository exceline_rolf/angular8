﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageShop
{
    [DataContract]
    public class SalePointDC
    {
        private int _iD = 0;
        [DataMember]
        public int ID
        {
            get { return _iD; }
            set { _iD = value; }
        }

        private string _machineName = string.Empty;
        [DataMember]
        public string MachineName
        {
            get { return _machineName; }
            set { _machineName = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private int _hardwareProfileId = 0;
        [DataMember]
        public int HardwareProfileId
        {
            get { return _hardwareProfileId; }
            set { _hardwareProfileId = value; }
        }

        private int _branchID = 0;
        [DataMember]
        public int BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

    }
}
