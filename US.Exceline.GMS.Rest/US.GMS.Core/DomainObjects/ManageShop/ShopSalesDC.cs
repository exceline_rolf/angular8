﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageShop
{
    [DataContract]
    public class ShopSalesDC
    {
        private int _shopSalesId = -1;
        [DataMember]
        public int ShopSalesId
        {
            get { return _shopSalesId; }
            set { _shopSalesId = value; }
        }

        private int _entitiyId = -1;
        [DataMember]
        public int EntitiyId
        {
            get { return _entitiyId; }
            set { _entitiyId = value; }
        }

        private string _entityRoleType = string.Empty;
        [DataMember]
        public string EntityRoleType
        {
            get { return _entityRoleType; }
            set { _entityRoleType = value; }
        }

        private int _salesPointId = -1;
        [DataMember]
        public int SalesPointId
        {
            get { return _salesPointId; }
            set { _salesPointId = value; }
        }

        private DateTime _purchaseDate;
        [DataMember]
        public DateTime PurchaseDate
        {
            get { return _purchaseDate; }
            set { _purchaseDate = value; }
        }

        private List<ShopSalesItemDC> _shopSalesItemList = new List<ShopSalesItemDC>();
        [DataMember]
        public List<ShopSalesItemDC> ShopSalesItemList
        {
            get { return _shopSalesItemList; }
            set { _shopSalesItemList = value; }
        }

      
        private string _userName = string.Empty;
          [DataMember]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private int _tryNumber = 0;
        [DataMember]
        public int TryNumber
        {
            get { return _tryNumber; }
            set { _tryNumber = value; }
        }

    }
}
