﻿using System;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class TrainingProgramVisitDetailDC
    {
        private MemberIntegrationSettingDC _memberIntegrationSetting;
        [DataMember]
        public MemberIntegrationSettingDC MemberIntegrationSetting
        {
            get { return _memberIntegrationSetting; }
            set { _memberIntegrationSetting = value; }
        }

        private EntityVisitDC _memberVisit;
        [DataMember]
        public EntityVisitDC MemberVisit
        {
            get { return _memberVisit; }
            set { _memberVisit = value; }
        }

        private DateTime _currentUpdatedDateTime;
        [DataMember]
        public DateTime CurrentUpdatedDateTime
        {
            get { return _currentUpdatedDateTime; }
            set { _currentUpdatedDateTime = value; }
        }

        private int _visitId;
         [DataMember]
        public int VisitId
        {
            get { return _visitId; }
            set { _visitId = value; }
        }

    }
}

