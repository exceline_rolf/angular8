﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
      [DataContract]
    public class RegionDC
    {
        private int _Id = 0;
        [DataMember]
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _code = string.Empty;
        [DataMember]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _countryId = string.Empty;
        [DataMember]
        public string CountryId
        {
           get { return _countryId; }
           set { _countryId = value; }
        }

        private bool _isCheck = false;
        [DataMember]
        public bool IsCheck
        {
            get { return _isCheck; }
            set { _isCheck = value; }
        }
    }
}
