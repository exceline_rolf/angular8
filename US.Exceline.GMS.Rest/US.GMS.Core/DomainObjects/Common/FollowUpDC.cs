﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class FollowUpDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private int _followUpTemplateId = -1;
        [DataMember]
        public int FollowUpTemplateId
        {
            get { return _followUpTemplateId; }
            set { _followUpTemplateId = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _smsPhoneNo;
        [DataMember]
        public string SmsPhoneNo
        {
            get { return _smsPhoneNo; }
            set { _smsPhoneNo = value; }
        }

        private string _smsPrefix;
        [DataMember]
        public string SmsPrefix
        {
            get { return _smsPrefix; }
            set { _smsPrefix = value; }
        }

        private string _emailAddress;
        [DataMember]
        public string EmailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; }
        }

        private List<FollowUpDetailDC> _followUpDetailList = new List<FollowUpDetailDC>();
        [DataMember]
        public List<FollowUpDetailDC> FollowUpDetailList
        {
            get { return _followUpDetailList; }
            set { _followUpDetailList = value; }
        }

        private bool _isInsert;
        [DataMember]
        public bool IsInsert
        {
            get { return _isInsert; }
            set { _isInsert = value; }
        }
    }
}
