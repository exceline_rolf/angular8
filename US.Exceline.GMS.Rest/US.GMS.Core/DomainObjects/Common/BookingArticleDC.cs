﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class BookingArticleDC
    {
        private int _articleId;
        [DataMember]
        public int ArticleId
        {
            get
            {
                return _articleId;
            }
            set
            {
                _articleId = value;
            }
        }

        private string _articleNo;
        [DataMember]
        public string ArticleNo
        {
            get
            {
                return _articleNo;
            }
            set
            {
                _articleNo = value;
            }
        }

        private string _description;
        [DataMember]
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }
        
        private decimal _defaultPrice;
        [DataMember]
        public decimal DefaultPrice
        {
            get
            {
                return _defaultPrice;
            }
            set
            {
                _defaultPrice = value;
            }
        }

        private string _category;
        [DataMember]
        public string Category
        {
            get
            {
                return _category;
            }
            set
            {
                _category = value;
            }
        }

        private int _activityId;
        [DataMember]
        public int ActivityId
        {
            get
            {
                return _activityId;
            }
            set
            {
                _activityId = value;
            }
        }

        private bool _activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get
            {
                return _activeStatus;
            }
            set
            {
                _activeStatus = value;
            }
        }

        private bool _checked;
        [DataMember]
        public bool Checked
        {
            get
            {
                return _checked;
            }
            set
            {
                _checked = value;
            }
        }

        private decimal _units;
        [DataMember]
        public decimal Units
        {
            get
            {
                return _units;
            }
            set
            {
                _units = value;
            }
        }

        private decimal _total;
        [DataMember]
        public decimal Total
        {
            get
            {
                return _total;
            }
            set
            {
                _total = value;
            }
        }

        private int _bookingEntitiyId;
        [DataMember]
        public int BookingEntitiyId
        {
            get
            {
                return _bookingEntitiyId;
            }
            set
            {
                _bookingEntitiyId = value;
            }
        }

        private string _bookingEntityType;
        [DataMember]
        public string BookingEntityType
        {
            get
            {
                return _bookingEntityType;
            }
            set
            {
                _bookingEntityType = value;
            }
        }

        private decimal _discount=0;
        [DataMember]
        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        private decimal _contractDeduction = 0;
        [DataMember]
        public decimal ContractDeduction
        {
            get { return _contractDeduction; }
            set { _contractDeduction = value; }
        }

        private bool _contractDeducationEnabled;
        [DataMember]
        public bool ContractDeducationEnabled
        {
            get { return _contractDeducationEnabled; }
            set { _contractDeducationEnabled = value; }
        }

    }
}
