﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.ScheduleManagement
{
    public enum TaskTemplateType
    {
        NONE,
        FOLLOWUP,
        LOSTANDFOUND,
        TODOITEMS,
        COMMON
    }
}
