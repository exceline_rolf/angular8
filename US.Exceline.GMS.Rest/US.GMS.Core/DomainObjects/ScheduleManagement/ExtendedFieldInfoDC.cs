﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ScheduleManagement
{
    [DataContract]
    public class ExtendedFieldInfoDC
    {
        private List<TaskExtendedFieldDC> _extendedFieldsList = new List<TaskExtendedFieldDC>();
        [DataMember]
        public List<TaskExtendedFieldDC> ExtendedFieldsList
        {
            get { return _extendedFieldsList; }
            set { _extendedFieldsList = value; }
        }
    }
}
