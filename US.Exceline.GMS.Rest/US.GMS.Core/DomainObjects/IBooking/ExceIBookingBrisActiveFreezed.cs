﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingBrisActiveFreezed
    {
        private int _brisid = -1;
        //private int _activeStatus = -1;
        private string _categoryName = string.Empty;
        private string _code = string.Empty;        

        [DataMember(Order = 1)]
        public int BrisID
        {
            get { return _brisid; }
            set { _brisid = value; }
        }

        /*
        [DataMember(Order = 2)]
        public int ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }
        */
    }
}
