﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingPayment
    {
        private int _customerId = -1;
        private string _contractNumber = string.Empty;
        private int _classActivityType = -1;
        private string _dueDate;
        private string _paidDate;
        private int _offeringId = -1;
        private int _paymentNumber = -1;
        private decimal _invoiceAmount = -1;
        private string _invoiceText = string.Empty;

        [DataMember(Order = 1)]
        public int CustomerNo
        {
            get { return _customerId; }
            set { _customerId = value; }
        }

        [DataMember(Order = 2)]
        public string ContractNumber
        {
            get { return _contractNumber; }
            set { _contractNumber = value; }
        }

        [DataMember(Order = 3)]
        public int ClassActivityType
        {
            get { return _classActivityType; }
            set { _classActivityType = value; }
        }

        [DataMember(Order = 4)]
        public string DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        [DataMember(Order = 5)]
        public string PaidDate
        {
            get { return _paidDate; }
            set { _paidDate = value; }
        }

        [DataMember(Order = 6)]
        public int OfferingId
        {
            get { return _offeringId; }
            set { _offeringId = value; }
        }

        [DataMember(Order = 7)]
        public int PaymentNumber
        {
            get { return _paymentNumber; }
            set { _paymentNumber = value; }
        }

        [DataMember(Order = 8)]
        public decimal InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        [DataMember(Order = 9)]
        public string InvoiceText
        {
            get { return _invoiceText; }
            set { _invoiceText = value; }
        }
    }
}
