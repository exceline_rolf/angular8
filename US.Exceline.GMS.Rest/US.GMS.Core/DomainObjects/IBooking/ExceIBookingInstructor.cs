﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingInstructor
    {
        private int _instructorId = -1;
        private string _instructorName = string.Empty;
        private int _gymId = -1;

        [DataMember(Order = 1)]
        public int InstructorId
        {
            get { return _instructorId; }
            set { _instructorId = value; }
        }

        [DataMember(Order = 2)]
        public string InstructorName
        {
            get { return _instructorName; }
            set { _instructorName = value; }
        }

        [DataMember(Order = 3)]
        public int GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }
    }
}
