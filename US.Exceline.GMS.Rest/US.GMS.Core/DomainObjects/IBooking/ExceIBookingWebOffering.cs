﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingWebOffering
    {
        private int _offeringId = -1;
        private int _gymId = -1;
        private string _offeringName = string.Empty;
        private int _activityType = -1;
        private string _fromDate;
        private string _toDate;
        private decimal _initialPrice = -1;
        private decimal _monthlyPrice = -1;
        private bool _avtaleGiro = false;
        private bool _restPlusMonth = false;
        private string _condition = string.Empty;
        private bool _showOnNet = false;


        [DataMember(Order = 1)]
        public int OfferingId
        {
            get { return _offeringId; }
            set { _offeringId = value; }
        }

        [DataMember(Order = 2)]
        public int GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        [DataMember(Order = 3)]
        public string OfferingName
        {
            get { return _offeringName; }
            set { _offeringName = value; }
        }

        [DataMember(Order = 4)]
        public int ActivityType
        {
            get { return _activityType; }
            set { _activityType = value; }
        }

        [DataMember(Order = 5)]
        public string FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }

        [DataMember(Order = 6)]
        public string ToDate
        {
            get { return _toDate; }
            set { _toDate = value; }
        }

        [DataMember(Order = 7)]
        public decimal InitialPrice
        {
            get { return _initialPrice; }
            set { _initialPrice = value; }
        }

        [DataMember(Order = 8)]
        public decimal MonthlyPrice
        {
            get { return _monthlyPrice; }
            set { _monthlyPrice = value; }
        }

        [DataMember(Order = 9)]
        public bool AvtaleGiro
        {
            get { return _avtaleGiro; }
            set { _avtaleGiro = value; }
        }

        [DataMember(Order = 10)]
        public bool RestPlusMonth
        {
            get { return _restPlusMonth; }
            set { _restPlusMonth = value; }
        }

        [DataMember(Order = 11)]
        public string Condition
        {
            get { return _condition; }
            set { _condition = value; }
        }

        [DataMember(Order = 12)]
        public bool ShowOnNet
        {
            get { return _showOnNet; }
            set { _showOnNet = value; }
        }
    }
}
