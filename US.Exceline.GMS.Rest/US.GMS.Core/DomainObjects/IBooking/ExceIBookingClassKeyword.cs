﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingClassKeyword
    {
        private string _classKeywordName = string.Empty;
        private int _keyWordId = -1;

        [DataMember(Order = 1)]
        public int KeyWordId
        {
            get { return _keyWordId; }
            set { _keyWordId = value; }
        }

        [DataMember(Order = 2)]
        public string ClassKeywordName
        {
            get { return _classKeywordName; }
            set { _classKeywordName = value; }
        }
    }
}
