﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingContractFreeze
    {
        private int _id = -1;
        private int _memberID = -1;
        private string _contractNo = string.Empty;
        private bool _isATG = false;
        private string _templateName = string.Empty;
        private int _activityId = -1;
        private string _activityName = string.Empty;
        private string _signedDate;
        private string _startDate;
        private string _endDate;
        private string _closedate;
        private string _renewedDate;
        private string _resignedDate;
        private string _lockinperiodendDate;
        private string _originalendDate;
        private int _noOfOrders = 1;
        private decimal _startUpItemsPrice = 0;
        private decimal _monthlyitemPrice = 0;
        private decimal _totalPrice = 0;
        private string _status = string.Empty;
        private int _accessProfileId = -1;
        private int _branchId = -1;
        private int _remainingVisits = 0;
        private string _contractTypeCode = string.Empty;
        private bool _isGroupContract = false;
        private bool _isFreezed = false;
        private string _freezeFromDate;
        private string _freezeToDate;
        private int _freezeMonthsThisYear;
        private int _freezeAllowed;
        private string _invoicefreeMonths = string.Empty;
        private string _lastDueDate;
        private int _membershipType = -1;        


        private decimal _serviceAmount = 0;
        private string _gymName = string.Empty;
        private bool _isGroup;
        private int _employeeID = -1;

        [DataMember(Order = 1)]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember(Order = 2)]
        public int MemberID
        {
            get { return _memberID; }
            set { _memberID = value; }
        }


        [DataMember(Order = 3)]
        public string ContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }


        [DataMember(Order = 4)]
        public bool IsATG
        {
            get { return _isATG; }
            set { _isATG = value; }
        }


        [DataMember(Order = 5)]
        public string TemplateName
        {
            get { return _templateName; }
            set { _templateName = value; }
        }


        [DataMember(Order = 6)]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }


        [DataMember(Order = 7)]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }


        [DataMember(Order = 8)]
        public string SignedDate
        {
            get { return _signedDate; }
            set { _signedDate = value; }
        }


        [DataMember(Order = 9)]
        public string StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }


        [DataMember(Order = 10)]
        public string EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        [DataMember(Order = 11)]
        public string Closedate
        {
            get { return _closedate; }
            set { _closedate = value; }
        }


        [DataMember(Order = 12)]
        public string RenewedDate
        {
            get { return _renewedDate; }
            set { _renewedDate = value; }
        }

        [DataMember(Order = 13)]
        public string ResignedDate
        {
            get { return _resignedDate; }
            set { _resignedDate = value; }
        }

        [DataMember(Order = 14)]
        public string LockinPeriodEndDate
        {
            get { return _lockinperiodendDate; }
            set { _lockinperiodendDate = value; }
        }

        [DataMember(Order = 15)]
        public string OriginalEndDate
        {
            get { return _originalendDate; }
            set { _originalendDate = value; }
        }

        [DataMember(Order = 16)]
        public int NoOfOrders
        {
            get { return _noOfOrders; }
            set { _noOfOrders = value; }
        }


        [DataMember(Order = 17)]
        public decimal StartUpItemsPrice
        {
            get { return _startUpItemsPrice; }
            set { _startUpItemsPrice = value; }
        }


        [DataMember(Order = 18)]
        public decimal MonthlyitemPrice
        {
            get { return _monthlyitemPrice; }
            set { _monthlyitemPrice = value; }
        }


        [DataMember(Order = 19)]
        public decimal TotalPrice
        {
            get { return _totalPrice; }
            set { _totalPrice = value; }
        }


        [DataMember(Order = 20)]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }


        [DataMember(Order = 21)]
        public int AccessProfileId
        {
            get { return _accessProfileId; }
            set { _accessProfileId = value; }
        }

        [DataMember(Order = 22)]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }


        [DataMember(Order = 23)]
        public int RemainingVisits
        {
            get { return _remainingVisits; }
            set { _remainingVisits = value; }
        }


        [DataMember(Order = 24)]
        public string ContractTypeCode
        {
            get { return _contractTypeCode; }
            set { _contractTypeCode = value; }
        }


        [DataMember(Order = 25)]
        public bool IsGroupContract
        {
            get { return _isGroupContract; }
            set { _isGroupContract = value; }
        }


        [DataMember(Order = 26)]
        public bool IsFreezed
        {
            get { return _isFreezed; }
            set { _isFreezed = value; }
        }

        [DataMember(Order = 27)]
        public string FreezeFromDate
        {
            get { return _freezeFromDate; }
            set { _freezeFromDate = value; }
        }

        [DataMember(Order = 28)]
        public string FreezeToDate
        {
            get { return _freezeToDate; }
            set { _freezeToDate = value; }
        }


        [DataMember(Order = 29)]
        public int FreezeMonthsThisYear
        {
            get { return _freezeMonthsThisYear; }
            set { _freezeMonthsThisYear = value; }
        }

        [DataMember(Order = 30)]
        public int FreezeAllowed
        {
            get { return _freezeAllowed; }
            set { _freezeAllowed = value; }
        }

        [DataMember(Order = 31)]
        public string InvoiceFreeMonths
        {
            get { return _invoicefreeMonths; }
            set { _invoicefreeMonths = value; }
        }


        [DataMember(Order = 32)]
        public string LastDueDate
        {
            get { return _lastDueDate; }
            set { _lastDueDate = value; }
        }


        [DataMember(Order = 33)]
        public int MemberShipType
        {
            get { return _membershipType; }
            set { _membershipType = value; }
        }

        [DataMember(Order = 34)]
        public decimal ServiceAmount
        {
            get { return _serviceAmount; }
            set { _serviceAmount = value; }
        }

        [DataMember(Order = 35)]
        public string GymName
        {
            get { return _gymName; }
            set { _gymName = value; }
        }

        [DataMember(Order = 36)]
        public bool IsGroup
        {
            get { return _isGroup; }
            set { _isGroup = value; }
        }
        
        [DataMember(Order = 37)]
        public int EmployeeId
        {
            get { return _employeeID; }
            set { _employeeID = value; }
        }


    }
}
