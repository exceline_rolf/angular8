﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
   public class ExceIBookingResourceScheduleTime
   {
       private int _id = -1;
       [DataMember(Order = 1, Name = "Id")] 
       public int Id
       {
           get { return _id; }
           set { _id = value; }
       }

       private string _fromTime = string.Empty;
       [DataMember(Order = 2, Name = "from")] 
       public string FromTime
       {
           get { return _fromTime; }
           set { _fromTime = value; }
       }

       private string _toTime = string.Empty;
       [DataMember(Order = 3, Name = "to")]
       public string ToTime
       {
           get { return _toTime; }
           set { _toTime = value; }
       }

     
   }
}
