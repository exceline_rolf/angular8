﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingResourceCalendar
    {
        private int _resourceId = -1;

        [DataMember(Order = 1)]
        public int ResourceId
        {
            get { return _resourceId; }
            set { _resourceId = value; }
        }
    }
}
