﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Script.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    public class ExceIBookingResourceSchedule
    {
        private int _id;
        [IgnoreDataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _day = string.Empty;
        [DataMember(Order = 1, Name = "Day")]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }


        private List<ExceIBookingResourceScheduleTime> _scheduleTime;
         [DataMember(Order = 2, Name = "ScheduleTimes")]
        public List<ExceIBookingResourceScheduleTime> ScheduleTime
        {
            get { return _scheduleTime; }
            set { _scheduleTime = value; }
        }

        private List<ExceIBookingResourceScheduleTime> _unavailableTime;
         [DataMember(Order = 3, Name = "UnavailableTimes")]
        public List<ExceIBookingResourceScheduleTime> UnavailableTime
        {
            get { return _unavailableTime; }
            set { _unavailableTime = value; }
        }

        private string _roleType = string.Empty;
        [IgnoreDataMember]
        public string RoleType
        {
            get { return _roleType; }
            set { _roleType = value; }
        }

        private string _fromTime = string.Empty;
        [IgnoreDataMember]
        public string FromTime
        {
            get { return _fromTime; }
            set { _fromTime = value; }
        }

        private string _toTime = string.Empty;
        [IgnoreDataMember]
        public string ToTime
        {
            get { return _toTime; }
            set { _toTime = value; }
        }

    }
}
