﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.Notification;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingVisitCount
    {
        private int _year = -1;
        private int _month = -1;
        private int _count = -1;


        [DataMember(Order = 1)]
        public int Year
        {
            get { return _year; }
            set { _year = value; }
        }

        [DataMember(Order = 2)]
        public int Month
        {
            get { return _month; }
            set { _month = value; }
        }

        [DataMember(Order = 3)]
        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }
       
    }
}