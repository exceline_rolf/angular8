﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingUpdateMemberProfilePicture
    {
        private int _systemId = -1;
        private int _gymId = -1;
        private string _profilePictureURL = string.Empty;
        private int _customerId = -1;
        
        [DataMember(Order = 1)]
        public int SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        [DataMember(Order = 2)]
        public int GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        [DataMember(Order = 3)]
        public string ProfilePictureURL
        {
            get { return _profilePictureURL; }
            set { _profilePictureURL = value; }
        }

        [DataMember(Order = 4)]
        public int CustomerId
        {
            get { return _customerId; }
            set { _customerId = value; }
        }
    }
}
