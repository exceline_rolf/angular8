﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingResourceBookingType
    {
        private int _resourceBookingId = -1;

        [DataMember(Order = 1)]
        public int ResourceBookingId
        {
            get { return _resourceBookingId; }
            set { _resourceBookingId = value; }
        }
    }
}
