﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingClassMemberBooking
    {
        [DataMember(Order = 1)]
        public string CustomerId { get; set; }
        [DataMember(Order = 2)]
        public int ClassId { get; set; }
        [DataMember(Order = 3)]
        public int TimeId { get; set; }
        [DataMember(Order = 4)]
        public Nullable<DateTime> ClassStartTime { get; set; }
        [DataMember(Order = 5)]
        public Nullable<DateTime> ClassEndTime { get; set; }
        [DataMember(Order = 6)]
        public int Participants { get; set; }
        [DataMember(Order = 7)]
        public bool Paid { get; set; }
    }
}
