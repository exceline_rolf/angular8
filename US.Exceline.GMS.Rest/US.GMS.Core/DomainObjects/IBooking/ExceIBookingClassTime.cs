﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingClassTime
    {
        [DataMember(Order = 1)]
        public int ClassId { get; set; }
        [DataMember(Order = 2)]
        public int TimeId { get; set; }
        [DataMember(Order = 3)]
        public DateTime ClassDate { get; set; }
        [DataMember(Order = 4)]
        public DateTime ClassStartTime { get; set; }
        [DataMember(Order = 5)]
        public DateTime ClassEndTime { get; set; }
        [DataMember(Order = 6)]
        public int TotalAvailableSlots { get; set; }
    }
}
