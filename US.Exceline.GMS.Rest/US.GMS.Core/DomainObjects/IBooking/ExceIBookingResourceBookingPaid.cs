﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingResourceBookingPaid
    {
        private int _systemId = -1;
        [DataMember(Order = 1, Name = "SystemId")]
        public int SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        private int _gymID = -1;
        [DataMember(Order = 2, Name = "GymID")]
        public int GymID
        {
            get { return _gymID; }
            set { _gymID = value; }
        }

        private int _bookingId = -1;
        [DataMember(Order = 3, Name = "BookingId")]

        public int BookingId
        {
            get { return _bookingId; }
            set { _bookingId = value; }
        }

        private string _refId = string.Empty;
        [DataMember(Order = 4, Name = "RefId")]
        public string RefId
        {
            get { return _refId; }
            set { _refId = value; }
        }

        private string _invoiceAmount;
        [DataMember(Order = 5, Name = "InvoiceAmount")]
        public string InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        private string _paymentAmount;
         [DataMember(Order = 6, Name = "PaymentAmount")]
        public string PaymentAmount
        {
            get { return _paymentAmount; }
            set { _paymentAmount = value; }
        }

         private string _paymentDate;
         [DataMember(Order = 7, Name = "PaymentDate")]
         public string PaymentDate
        {
            get { return _paymentDate; }
            set { _paymentDate = value; }
        }

        


    }
}
