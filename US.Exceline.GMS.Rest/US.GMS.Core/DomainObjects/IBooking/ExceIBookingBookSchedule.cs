﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingBookSchedule
    {
        [DataMember]
        public string TimeId { get; set; }
        [DataMember]
        public int Participants { get; set; }
    }
}
