﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
    public class ExceIBookingAllContracts
    {
        private int _contractId = -1;
        private int _customerID = -1;
        private int _templateNo = -1;
        private string _templateName = string.Empty;
        private string _contractStartDate = string.Empty;
        private string _contractEndDate = string.Empty;
        private string _activity = string.Empty;
        private int _branchid = -1;
        private int _availableVisits = -1;
        private int _recordedVisits = -1;
        private int _contractType = -1;

        [DataMember(Order = 1)]
        public int ContractID
        {
            get { return _contractId; }
            set { _contractId = value; }
        }

        [DataMember(Order = 2)]
        public int CustomerID
        {
            get { return _customerID; }
            set { _customerID = value; }
        }

        [DataMember(Order = 3)]
        public int TemplateNo
        {
            get { return _templateNo; }
            set { _templateNo = value; }
        }


        [DataMember(Order = 6)]
        public string ContractTempName
        {
            get { return _templateName; }
            set { _templateName = value; }
        }

        [DataMember(Order = 7)]
        public string ContractStartDate
        {
            get { return _contractStartDate; }
            set { _contractStartDate = value; }
        }

        [DataMember(Order = 8)]
        public string ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }


        [DataMember(Order = 9)]
        public string Activity
        {
            get { return _activity; }
            set { _activity = value; }
        }

        [DataMember(Order = 10)]
        public int BranchId
        {
            get { return _branchid; }
            set { _branchid = value; }
        }

        [DataMember(Order = 11)]
        public int AvailableVisits
        {
            get { return _availableVisits; }
            set { _availableVisits = value; }
        }

        [DataMember(Order = 12)]
        public int RecordedVisits
        {
            get { return _recordedVisits; }
            set { _recordedVisits = value; }
        }

        [DataMember(Order = 13)]
        public int ContractType
        {
            get { return _contractType; }
            set { _contractType = value; }
        }
    }
}
