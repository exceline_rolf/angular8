﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.API
{
    [DataContract]
    public class ExceAPIMember
    {
        private string _gymId = string.Empty;
        private string _firstName = string.Empty;
        private string _lastName = string.Empty;
        private string _birthDate;
        private string _address1 = string.Empty;
        private string _address2 = string.Empty;
        private string _address3 = string.Empty;
        private string _postCode = string.Empty;
        private string _postPlace = string.Empty;
        private string _mobile = string.Empty;
        private string _workTeleNo = string.Empty;
        private string _privateTeleNo = string.Empty;
        private string _email = string.Empty;
        private string _gender = string.Empty;
        private string _role = string.Empty;
        private string _active = string.Empty;
        private string _expieryDate;
        private string _cardNumber = string.Empty;
        private string _noCommercial = string.Empty;
        private string _registerdDate;
        private string _lastVisitDate;
        private string _customerNumber = string.Empty;
        private string _departmentNumber = string.Empty;
        private string _firmNumber = string.Empty;
        private string _currentOffering = string.Empty;
        private string _updatedDateTime;
        private string _autoGiro = string.Empty;
        private string _contractNumber = string.Empty;
        private string _contractStartdate;
        private decimal _contractSaldo = -1;
        private decimal _contractMaxrate = -1;
        private string _contractAvtGiroStatus = string.Empty;
        private string _countryCode = string.Empty;
        private string _gatCarNumber = string.Empty;
        private int _accessProfileID = -1;
        private string _freezeStartDate = string.Empty;
        private string _freezeEndDate = string.Empty;
        private int _instructorID = -1;
        private string _pinCode = string.Empty;
        private string _guestCardNo = string.Empty;
        private bool _sendEmail = false;
        private bool _sendSMS = false;
        private bool _customerClub = false;

        [DataMember(Order = 1)]
        public string CustomerNo
        {
            get { return _customerNumber; }
            set { _customerNumber = value; }
        }

        [DataMember(Order = 2)]
        public string GymID
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        [DataMember(Order = 3)]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        [DataMember(Order = 4)]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        [DataMember(Order = 5)]
        public string BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        [DataMember(Order = 6)]
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        [DataMember(Order = 7)]
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        [DataMember(Order = 8)]
        public string Address3
        {
            get { return _address3; }
            set { _address3 = value; }
        }

        [DataMember(Order = 9)]
        public string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }

        [DataMember(Order = 10)]
        public string PostPlace
        {
            get { return _postPlace; }
            set { _postPlace = value; }
        }

        [DataMember(Order = 11)]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        [DataMember(Order = 12)]
        public string WorkTeleNo
        {
            get { return _workTeleNo; }
            set { _workTeleNo = value; }
        }

        [DataMember(Order = 13)]
        public string PrivateTeleNo
        {
            get { return _privateTeleNo; }
            set { _privateTeleNo = value; }
        }

        [DataMember(Order = 14)]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        [DataMember(Order = 15)]
        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        [DataMember(Order = 16)]
        public string Role
        {
            get { return _role; }
            set { _role = value; }
        }

        [DataMember(Order = 17)]
        public string Active
        {
            get { return _active; }
            set { _active = value; }
        }

        [DataMember(Order = 18)]
        public string ExpieryDate
        {
            get { return _expieryDate; }
            set { _expieryDate = value; }
        }

        [DataMember(Order = 19)]
        public string CardNumber
        {
            get { return _cardNumber; }
            set { _cardNumber = value; }
        }

        [DataMember(Order = 20)]
        public string NoCommercial
        {
            get { return _noCommercial; }
            set { _noCommercial = value; }
        }

        [DataMember(Order = 21)]
        public string RegisterdDate
        {
            get { return _registerdDate; }
            set { _registerdDate = value; }
        }

        [DataMember(Order = 22)]
        public string LastVisitDate
        {
            get { return _lastVisitDate; }
            set { _lastVisitDate = value; }
        }


        [DataMember(Order = 23)]
        public string DepartmentNumber
        {
            get { return _departmentNumber; }
            set { _departmentNumber = value; }
        }

        [DataMember(Order = 24)]
        public string FirmNumber
        {
            get { return _firmNumber; }
            set { _firmNumber = value; }
        }

        [DataMember(Order = 25)]
        public string CurrentOffering
        {
            get { return _currentOffering; }
            set { _currentOffering = value; }
        }

        [DataMember(Order = 26)]
        public string UpdatedDateTime
        {
            get { return _updatedDateTime; }
            set { _updatedDateTime = value; }
        }

        [DataMember(Order = 27)]
        public string AutoGiro
        {
            get { return _autoGiro; }
            set { _autoGiro = value; }
        }

        [DataMember(Order = 28)]
        public string ContractNumber
        {
            get { return _contractNumber; }
            set { _contractNumber = value; }
        }

        [DataMember(Order = 29)]
        public string ContractStartdate
        {
            get { return _contractStartdate; }
            set { _contractStartdate = value; }
        }

        [DataMember(Order = 30)]
        public decimal ContractSaldo
        {
            get { return _contractSaldo; }
            set { _contractSaldo = value; }
        }

        [DataMember(Order = 31)]
        public decimal ContractMaxrate
        {
            get { return _contractMaxrate; }
            set { _contractMaxrate = value; }
        }

        [DataMember(Order = 32)]
        public string ContractAvtGiroStatus
        {
            get { return _contractAvtGiroStatus; }
            set { _contractAvtGiroStatus = value; }
        }

        [DataMember(Order = 33)]
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }

        [DataMember(Order = 34)]
        public string GatCarNumber
        {
            get { return _gatCarNumber; }
            set { _gatCarNumber = value; }
        }

        [DataMember(Order = 35)]
        public int AccessProfileID
        {
            get { return _accessProfileID; }
            set { _accessProfileID = value; }
        }

        [DataMember(Order = 36)]
        public string FreezeStartDate
        {
            get { return _freezeStartDate; }
            set { _freezeStartDate = value; }
        }

        [DataMember(Order = 37)]
        public string FreezeEndDate
        {
            get { return _freezeEndDate; }
            set { _freezeEndDate = value; }
        }

        [DataMember(Order = 38)]
        public int InstructorID
        {
            get { return _instructorID; }
            set { _instructorID = value; }
        }

        [DataMember(Order = 39)]
        public string PinCode
        {
            get { return _pinCode; }
            set { _pinCode = value; }
        }

        [DataMember(Order = 40)]
        public string GuestCardNo
        {
            get { return _guestCardNo; }
            set { _guestCardNo = value; }
        }

        [DataMember(Order = 41)]
        public bool SendSMS
        {
            get { return _sendSMS; }
            set { _sendSMS = value; }
        }

        [DataMember(Order = 42)]
        public bool SendEmail
        {
            get { return _sendEmail; }
            set { _sendEmail = value; }
        }

        [DataMember(Order = 43)]
        public bool CustomerClub
        {
            get { return _customerClub; }
            set { _customerClub = value; }
        }
    }
}
