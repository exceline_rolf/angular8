﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class EntityVisitDC
    {
        private int _activityId = -1;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private string _role = string.Empty;
        [DataMember]
        public string Role
        {
            get { return _role;  }
            set { _role = value; }
        }


        private int _articleID = -1;
        [DataMember]
        public int ArticleID
        {
            get { return _articleID; }
            set { _articleID = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _contractNo = string.Empty;
        [DataMember]
        public string ContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }

        private bool _countVist = true;
        [DataMember]
        public bool CountVist
        {
            get { return _countVist; }
            set { _countVist = value; }
        }

        private string _cutomerNo = string.Empty;
        [DataMember]
        public string CutomerNo
        {
            get { return _cutomerNo; }
            set { _cutomerNo = value; }
        }

        private int _entityId = -1;
        [DataMember]
        public int EntityId
        {
            get { return _entityId; }
            set { _entityId = value; }
        }

        private string _entityName = string.Empty;
        [DataMember]
        public string EntityName
        {
            get { return _entityName; }
            set { _entityName = value; }
        }

        private string _gymName = string.Empty;
        [DataMember]
        public string GymName
        {
            get { return _gymName; }
            set { _gymName = value; }
        }

        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private DateTime _inTime;
        [DataMember]
        public DateTime InTime
        {
            get { return _inTime; }
            set { _inTime = value; }
        }

        private int _invoiceRerefence = -1;
        [DataMember]
        public int InvoiceRerefence
        {
            get { return _invoiceRerefence; }
            set { _invoiceRerefence = value; }
        }

        private string _itemName = string.Empty;
        [DataMember]
        public string ItemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }

        private int _memberContractId;
        [DataMember]
        public int MemberContractId
        {
            get { return _memberContractId; }
            set { _memberContractId = value; }
        }

        private DateTime _visitDate;
        [DataMember]
        public DateTime VisitDate
        {
            get { return _visitDate; }
            set { _visitDate = value; }
        }

        private string visitType = string.Empty;
        [DataMember]
        public string VisitType
        {
            get { return visitType; }
            set { visitType = value; }
        }

        private List<string> _messages;
        [DataMember]
        public List<string> Messages
        {
            get { return _messages; }
            set { _messages = value; }
        }

        private bool _isVisit;
         [DataMember]
        public bool IsVisit
        {
            get { return _isVisit; }
            set { _isVisit = value; }
        }

         private bool _isExcAccess;
        [DataMember]
         public bool IsExcAccess
         {
             get { return _isExcAccess; }
             set { _isExcAccess = value; }
         }

        private int _cardNo;
         [DataMember]
        public int CardNo
        {
            get { return _cardNo; }
            set { _cardNo = value; }
        }

         private int _terminalId;
          [DataMember]
         public int TerminalId
         {
             get { return _terminalId; }
             set { _terminalId = value; }
         }

        private int _controllerId;
          [DataMember]
        public int ControllerId
        {
            get { return _controllerId; }
            set { _controllerId = value; }
        }

          private string accessControlType = string.Empty;
          [DataMember]
          public string AccessControlType
          {
              get { return accessControlType; }
              set { accessControlType = value; }
          }

          private string _integrationUserId  = string.Empty;
          [DataMember]
          public string IntegrationUserId
          {
              get { return _integrationUserId; }
              set { _integrationUserId = value; }
          }

         private string _mobile = string.Empty;
         [DataMember]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }


        private bool _isBtnDeleteVisible = true;
        [DataMember]
        public bool IsBtnDeleteVisible
        {
            get { return _isBtnDeleteVisible; }
            set { _isBtnDeleteVisible = value; }
        }

        private bool _isBtnDeleteEnable = true;
        [DataMember]
        public bool IsBtnDeleteEnable
        {
            get { return _isBtnDeleteEnable; }
            set { _isBtnDeleteEnable = value; }
        }

    }
}
