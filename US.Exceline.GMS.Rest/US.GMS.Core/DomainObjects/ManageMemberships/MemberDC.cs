﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public abstract class MemberDC
    {
        [DataMember]
        public abstract int Id { get; set; }
        [DataMember]
        public abstract string FirstName { get; set; }
        [DataMember]
        public abstract string LastName { get; set; }
        [DataMember]
        public abstract Gender Gender { get; set; }
        [DataMember]
        public abstract DateTime BirthDate { get; set; }
        [DataMember]
        public abstract int Age { get; set; }
        [DataMember]
        public abstract int EntNo { get; set; }
        [DataMember]
        public abstract string Address1 { get; set; }
        [DataMember]
        public abstract string Address2 { get; set; }
        [DataMember]
        public abstract string Address3 { get; set; }
        [DataMember]
        public abstract string PostCode { get; set; }
        [DataMember]
        public abstract string PostPlace { get; set; }
        [DataMember]
        public abstract string Email { get; set; }
        [DataMember]
        public abstract string Mobile { get; set; }
        [DataMember]
        public abstract string WorkTeleNo { get; set; }
        [DataMember]
        public abstract string PrivateTeleNo { get; set; }
        [DataMember]
        public abstract string Department { get; set; }
        [DataMember]
        public abstract string Instructor { get; set; }
        [DataMember]
        public abstract string AccountNo { get; set; }
        [DataMember]
        public abstract DateTime ContractStartDate { get; set; }
        [DataMember]
        public abstract DateTime ContractEndDate { get; set; }
        [DataMember]
        public abstract string CompanyName { get; set; }
        [DataMember]
        public abstract string CreatedUser { get; set; }
        [DataMember]
        public abstract string ModifiedUser { get; set; }
        [DataMember]
        public abstract string EmployeeNo { get; set; }
        [DataMember]
        public abstract int BranchId { get; set; }
        [DataMember]
        public abstract string PersonNo { get; set; }
        [DataMember]
        public abstract bool ActiveStatus { get; set; }
        [DataMember]
        public abstract byte[] ProfilePicture { get; set; }
        [DataMember]
        public abstract string Name { get; set; }
        [DataMember]
        public abstract bool IsSelected { get; set; }
        [DataMember]
        public abstract string ImagePath { get; set; }
        [DataMember]
        public abstract string Image { get; set; }
        [DataMember]
        public abstract string Description { get; set; }


    }
}
