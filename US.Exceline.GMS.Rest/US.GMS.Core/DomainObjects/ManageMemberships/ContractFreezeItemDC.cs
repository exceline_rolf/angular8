﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/6/2012 14:55:17
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Notification;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class ContractFreezeItemDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }


        private decimal _pendingAmount = 0.00M;
        [DataMember]
        public decimal PendingAmount
        {
            get { return _pendingAmount; }
            set { _pendingAmount = value; }
        }


        private string _freezeName;
        [DataMember]
        public string FreezeName
        {
            get { return _freezeName; }
            set { _freezeName = value; }
        }


        private NotifyMethodType _notifyMethod;
        [DataMember]
        public NotifyMethodType NotifyMethod
        {
            get { return _notifyMethod; }
            set { _notifyMethod = value; }
        }

        private int _memberId;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }


        private int _memberContractid;
        [DataMember]
        public int MemberContractid
        {
            get { return _memberContractid; }
            set { _memberContractid = value; }
        }

        private int _categoryId;
        [DataMember]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }

        private string _category;
        [DataMember]
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }


        private DateTime _fromDate;
        [DataMember]
        public DateTime FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }

        private DateTime _toDate;
        [DataMember]
        public DateTime ToDate
        {
            get { return _toDate; }
            set { _toDate = value; }
        }

        private string _note;
        [DataMember]
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }

        private bool _activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private string _notification;
        [DataMember]
        public string Notification
        {
            get { return _notification; }
            set { _notification = value; }
        }

        private int _freezeMonths;
        [DataMember]
        public int FreezeMonths
        {
            get { return _freezeMonths; }
            set { _freezeMonths = value; }
        }

        private int _freezeDays;
        [DataMember]
        public int FreezeDays
        {
            get { return _freezeDays; }
            set { _freezeDays = value; }
        }

        private int _unfreezeDays;
        [DataMember]
        public int UnfreezeDays
        {
            get { return _unfreezeDays; }
            set { _unfreezeDays = value; }
        }

        private int _shiftCount;
        [DataMember]
        public int ShiftCount
        {
            get { return _shiftCount; }
            set { _shiftCount = value; }
        }

        private int _shiftStartInstallmentId;
        [DataMember]
        public int ShiftStartInstallmentId
        {
            get { return _shiftStartInstallmentId; }
            set { _shiftStartInstallmentId = value; }
        }

        private DateTime _contractEndDate;
        [DataMember]
        public DateTime ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }

        private bool _isExtended = false;
        [DataMember]
        public bool IsExtended
        {
            get { return _isExtended; }
            set { _isExtended = value; }
        }

        private string _contractNo;
        [DataMember]
        public string MemberContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }
    }
}
