﻿
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class InfoGymMember
    {
        //[DataMember]
        //public string Id { get; set; }
        [DataMember]
        public string keycard { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string phone { get; set; }
        //[DataMember]
        //public int MemberId { get; set; }
        //[DataMember]
        //public Data Data { get; set; }
    }

    //[DataContract]
    //public class Data
    //{
    //    [DataMember]
    //    public string Key { get; set; }
    //    [DataMember]
    //    public string Value { get; set; }
    //}
}
