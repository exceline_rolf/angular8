﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class DropInVisitPaymentDC
    {
        private int _bookingPaymentId;
        [DataMember]
        public int BookingPaymentId
        {
            get { return _bookingPaymentId; }
            set { _bookingPaymentId = value; }
        }

        private int _bookingScheduleId;
        [DataMember]
        public int BookingScheduleId
        {
            get { return _bookingScheduleId; }
            set { _bookingScheduleId = value; }
        }

        private decimal _paymentAmount;
        [DataMember]
        public decimal PaymentAmount
        {
            get { return _paymentAmount; }
            set { _paymentAmount = value; }
        }

        private int _payModeId;
        [DataMember]
        public int PayModeId
        {
            get { return _payModeId; }
            set { _payModeId = value; }
        }

        private string _payMode;
        [DataMember]
        public string PayMode
        {
            get { return _payMode; }
            set { _payMode = value; }
        }

        private DateTime _createdDateTime;
        [DataMember]
        public DateTime CreatedDateTime
        {
            get { return _createdDateTime; }
            set { _createdDateTime = value; }
        }

        private string _createdUser;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }
    }
}
