﻿using System;
using System.Runtime.Serialization;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class MemberForMemberlist
    {
        private int _id = 0;
        [DataMember]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _roleId = string.Empty;
        [DataMember]
        public string RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

        private string _firstName = string.Empty;
        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName = string.Empty;
        [DataMember]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _mobile = string.Empty;
        [DataMember]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        private string _custId = string.Empty;
        [DataMember]
        public string CustId
        {
            get { return _custId; }
            set { _custId = value; }
        }

        private string _mainContractNo = string.Empty;
        [DataMember]
        public string MainContractNo
        {
            get { return _mainContractNo; }
            set { _mainContractNo = value; }
        }

        private string _contractId = string.Empty;
        [DataMember]
        public string ContractId
        {
            get { return _contractId; }
            set { _contractId = value; }
        }

        private string _statusName = string.Empty;
        [DataMember]
        public string StatusName
        {
            get { return _statusName; }
            set { _statusName = value; }
        }

        private string _email = string.Empty;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _membercardNo = string.Empty;
        [DataMember]
        public string MembercardNo
        {
            get { return _membercardNo; }
            set { _membercardNo = value; }
        }

        private int _orderAmount;
        [DataMember]
        public int OrderAmount
        {
            get { return _orderAmount; }
            set { _orderAmount = value; }
        }


        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private int _branchID = -1;
        [DataMember]
        public int BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }

      

        private int _age = 0;
        [DataMember]
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }


        private int _activeStatus = 0;
        [DataMember]
        public int ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private string _invoiceNo = string.Empty;
        [DataMember]
        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }




    }
}
