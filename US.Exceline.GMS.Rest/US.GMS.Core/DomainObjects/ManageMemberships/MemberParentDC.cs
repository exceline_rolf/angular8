﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
     [DataContract]
   public class MemberParentDC
    {
        private int _memberId;
         [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

         private string _parentFirstName = string.Empty;
           [DataMember]
         public string ParentFirstName
         {
             get { return _parentFirstName; }
             set { _parentFirstName = value; }
         }

         private string _parentLastName = string.Empty;
           [DataMember]
         public string ParentLastName
         {
             get { return _parentLastName; }
             set { _parentLastName = value; }
         }

           private string _address = string.Empty;
           [DataMember]
           public string Address
           {
               get { return _address; }
               set { _address = value; }
           }

           private string _postAddress = string.Empty;
           [DataMember]
           public string PostAddress
           {
               get { return _postAddress; }
               set { _postAddress = value; }
           }

           private string _sms = string.Empty;
           [DataMember]
           public string Sms
           {
               get { return _sms; }
               set { _sms = value; }
           }

           private int _parentId;
          [DataMember]
           public int ParentId
           {
               get { return _parentId; }
               set { _parentId = value; }
           }

          private string _postPlace = string.Empty;
         [DataMember]
          public string PostPlace
          {
              get { return _postPlace; }
              set { _postPlace = value; }
          }

         private string _postCode = string.Empty;
         [DataMember]
         public  string PostCode
         {
             get { return _postCode; }
             set { _postCode = value; }
         }

         private string _custId = string.Empty;
         [DataMember]
         public string CustId
         {
             get { return _custId; }
             set { _custId = value; }
         }


         private string _address2 = string.Empty;
         [DataMember]
         public string Address2
         {
             get { return _address2; }
             set { _address2 = value; }
         }


         private string _name = string.Empty;
         [DataMember]
         public string Name
         {
             get { return _name; }
             set { _name = value; }
         }

    }
}
