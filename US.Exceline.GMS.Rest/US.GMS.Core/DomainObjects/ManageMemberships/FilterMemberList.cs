﻿// --------------------------------------------------------------------------
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Author            : Martin Tømmerås
// Created Timestamp : 02.02.2019 08:28:00
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : Filter parameters for memberlist
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class FilterMemberList
    {
        private string _lastName = string.Empty;
        [DataMember]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _firstName = string.Empty;
        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _custId = string.Empty;
        [DataMember]
        public string CustId
        {
            get { return _custId; }
            set { _custId = value; }
        }

        private string _age = string.Empty;
        [DataMember]
        public string Age
        {
            get { return _age; }
            set { _age = value; }
        }
        private string _mobile;
        [DataMember]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }
        private string _email = string.Empty;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private string _membercardNo = string.Empty;
        [DataMember]
        public string MembercardNo
        {
            get { return _membercardNo; }
            set { _membercardNo = value; }
        }
        private string _contractNo = string.Empty;
        [DataMember]
        public string ContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }
        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private string _invoiceNo = string.Empty;
        [DataMember]
        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        private Int32 _status;
        [DataMember]
        public Int32 FilterStatusId
        {
            get { return _status; }
            set { _status = value; }
        }
        private Int32 _branchId;
        [DataMember]
        public Int32 FilterBranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _roleId = string.Empty;
        [DataMember]
        public string FilterRoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

    }
}
