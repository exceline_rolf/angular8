﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/6/2012 14:56:21
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class ContractFreezeInstallmentDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _memberId;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private int _memberContractId;
        [DataMember]
        public int MemberContractId
        {
            get { return _memberContractId; }
            set { _memberContractId = value; }
        }

        private int _freezeItemid;
        [DataMember]
        public int FreezeItemid
        {
            get { return _freezeItemid; }
            set { _freezeItemid = value; }
        }

        private bool _isUnfreeze;
        [DataMember]
        public bool IsUnfreeze
        {
            get { return _isUnfreeze; }
            set { _isUnfreeze = value; }
        }

        private int _installmentId;
        [DataMember]
        public int InstallmentId
        {
            get { return _installmentId; }
            set { _installmentId = value; }
        }

        private string _orderNo;
        [DataMember]
        public string OrderNo
        {
            get { return _orderNo; }
            set { _orderNo = value; }
        }
        
        private string _installmentText;
        [DataMember]
        public string InstallmentText
        {
            get { return _installmentText; }
            set { _installmentText = value; }
        }

        private DateTime _installmentDate;
        [DataMember]
        public DateTime InstallmentDate
        {
            get { return _installmentDate; }
            set { _installmentDate = value; }
        }

        private decimal _installmentAmount;
        [DataMember]
        public decimal InstallmentAmount
        {
            get { return _installmentAmount; }
            set { _installmentAmount = value; }
        }

        private DateTime _installmentDueDate;
        [DataMember]
        public DateTime InstallmentDueDate
        {
            get { return _installmentDueDate; }
            set { _installmentDueDate = value; }
        }

        private DateTime _newInstallmentDueDate;
        [DataMember]
        public DateTime NewInstallmentDueDate
        {
            get { return _newInstallmentDueDate; }
            set { _newInstallmentDueDate = value; }
        }

        private DateTime? _trainingPeriodStart;
        [DataMember]
        public DateTime? TrainingPeriodStart
        {
            get { return _trainingPeriodStart; }
            set { _trainingPeriodStart = value; }
        }

        private DateTime? _trainingPeriodEnd;
        [DataMember]
        public DateTime? TrainingPeriodEnd
        {
            get { return _trainingPeriodEnd; }
            set { _trainingPeriodEnd = value; }
        }

        private int _extendTrainingPeriodDays;
        [DataMember]
        public int ExtendTrainingPeriodDays
        {
            get { return _extendTrainingPeriodDays; }
            set { _extendTrainingPeriodDays = value; }
        }

        private DateTime? _estimatedInvoiceDate;
        [DataMember]
        public DateTime? EstimatedInvoiceDate
        {
            get { return _estimatedInvoiceDate; }
            set { _estimatedInvoiceDate = value; }
        }


    }
}
