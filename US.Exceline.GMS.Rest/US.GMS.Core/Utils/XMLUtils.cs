﻿using System;
using System.Text;
using System.Runtime.Serialization;
using System.IO;
using System.Xml.Serialization;

namespace US.GMS.Core.Utils
{
    public class XMLUtils
    {
        public static string SerializeDataContractObjectToXML(object obj)
        {
            if (obj != null)
            {
                DataContractSerializer serializer = new DataContractSerializer(obj.GetType());
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                serializer.WriteObject(ms, obj);
                byte[] bytes = ms.ToArray();
                ms.Close();
                return System.Text.Encoding.UTF8.GetString(bytes, 0, bytes.Length);
            }
            return string.Empty;
        }
        public static object DesrializeXMLToObject(string xml, Type type)
        {
            DataContractSerializer dcs = new DataContractSerializer(type);
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(xml));
            return dcs.ReadObject(ms);
        }

        public static object DesrializeXMLToObjectUTF32(string xml, Type type)
        {
            DataContractSerializer dcs = new DataContractSerializer(type);
            MemoryStream ms = new MemoryStream(Encoding.UTF32.GetBytes(xml));
            return dcs.ReadObject(ms);
        }

        public static string SerializeToXMLString(object ObjectToSerialize)
        {
            MemoryStream mem = new MemoryStream();
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(ObjectToSerialize.GetType());
            ser.Serialize(mem, ObjectToSerialize);
            byte[] bytes = mem.ToArray();
            mem.Close();
            return System.Text.Encoding.UTF8.GetString(bytes, 0, bytes.Length);
        }

        public static T DeserializeObject<T>(string xml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(T));
            MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(xml));
            return (T)xs.Deserialize(memoryStream);
        }
        private static Byte[] StringToUTF8ByteArray(string pXmlString)
        {
            UTF32Encoding encoding = new UTF32Encoding();
            byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }
    }
}
