﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.UIPO.DomainObjects;
using US.USIE.UtilityLibray;
using US.USIE.UtilityLibray.Logging;
using US.USIE.UtilityLibray.TaskConfiguration;


namespace US.Exceline.GMS.EventLogDelete
{
    public class EventLogDeleteManager
    {
        private USTaskConfiguration _usTaskConfiguration;
        private List<INotificationMessage> _notificationRecords = new List<INotificationMessage>();
        private List<LogRecord> _taskLogs = new List<LogRecord>();

        public EventLogDeleteManager()
        {
            try
            {
                LogginManager.LogErrorToLogFile("Event Log Delete process...");

                LogginManager.RegisterLogFile("EventLogDeleteTask.txt"); //ConfigurationManager.AppSettings["LogFileName"]
                _usTaskConfiguration = (USTaskConfiguration)ConfigurationUtility.DeSerializeXMLToObject(new USTaskConfiguration(), new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName + "\\EventLogDeleteTask.xml");

                if (_usTaskConfiguration == null)
                {
                    _taskLogs.Add(UDIELogger.FillLogRecord("EventLogDelete", "ERROR - EventLog Delete Task Configuration not found"));
                    LogginManager.LogToTaskLog("EventLogDelete: ERROR - EventLog Delete Task Configuration not found", string.Empty);
                    LogginManager.LogErrorToLogFile("EventLog Delete Task Task Configuration not found");
                    _notificationRecords.Add(US.UIPO.Utils.MessageSubscribing.NotificationMessage.PopulateNotification("ERROR - EventLog Delete Task Configuration not found"));
                }
                else
                {
                    LogginManager.RegisterTaskLog(_usTaskConfiguration.DisplayName);

                    if (_usTaskConfiguration.USCustomConfigurations.Count > 0 && _usTaskConfiguration.USCustomConfigurations[0].USCustomConfigurationItems.Count > 0)
                    {
                        LogginManager.RegisterLogFile(_usTaskConfiguration.USCustomConfigurations[0].USCustomConfigurationItems[0].Value);
                    }
                    else
                    {
                        LogginManager.RegisterLogFile("EventLogDeleteTask.txt");
                    }
                }
            }
            catch (Exception ex)
            {
                if (_usTaskConfiguration != null)
                {
                    _taskLogs.Add(UDIELogger.FillLogRecord(_usTaskConfiguration.DisplayName, "ERROR " + ex.Message));
                    _notificationRecords.Add(US.UIPO.Utils.MessageSubscribing.NotificationMessage.PopulateNotification(_usTaskConfiguration.DisplayName + "ERROR - " + ex.Message));
                    LogginManager.LogToTaskLog("ERROR " + ex.Message, string.Empty);
                }
                LogginManager.LogErrorToLogFile(ex.Message);
            }
        }

        public bool Execute()
        {
            LogginManager.LogErrorToLogFile("Started the Event Log Delete process...");
           
            try
            {
               
                if (System.IO.File.Exists(@"D:\Exceline\ExceCare\WindowsService\EventLog.txt"))
                {
                    File.Delete(@"D:\Exceline\ExceCare\WindowsService\EventLog.txt");
                }
           

            }
            catch (Exception ex)
            {
                LogginManager.LogErrorToLogFile(ex.Message);
            }

            LogginManager.LogErrorToLogFile("Finished the Event Log Delete process...");
            return true;
        }
    }
}
