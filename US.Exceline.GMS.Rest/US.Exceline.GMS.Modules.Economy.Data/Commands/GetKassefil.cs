﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetKassefil : USDBActionBase<List<VariablesForKassefil>>
    {
        private DateTime _FromDate = DateTime.Now;
        private DateTime _ToDate = DateTime.Now;
        private int _Branch = -1;
        private bool _AllBranches = false;
    

        public GetKassefil(DateTime FromDate, DateTime ToDate, int Branch, bool AllBranches)
        {
            _FromDate = FromDate;
            _ToDate = ToDate;
            _Branch = Branch;
            _AllBranches = AllBranches;
        }

    
        protected override List<VariablesForKassefil> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "Kassefil";
            DbDataReader reader = null;
            List<VariablesForKassefil> economyFileLogData = new List<VariablesForKassefil>();



            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", System.Data.DbType.DateTime, _FromDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", System.Data.DbType.DateTime, _ToDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@AllBranches", System.Data.DbType.Boolean, _AllBranches));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Branch", System.Data.DbType.Int32, _Branch));
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    VariablesForKassefil fileLogData = new VariablesForKassefil
                    {

                        BranchName = Convert.ToString(reader["BranchName"]),
                        VoucherDate = Convert.ToString(reader["VoucherDate"]),
                        BranchId = Convert.ToString(reader["BranchId"]),
                        CompanyIdSalary = Convert.ToString(reader["CompanyIdSalary"]),
                        AccountNo = Convert.ToString(reader["AccountNo"]),
                        AccountName = Convert.ToString(reader["AccountName"]),
                        CategoryId = Convert.ToString(reader["CategoryId"]),
                        CategoryName = Convert.ToString(reader["CategoryName"]),
                        VATRate = Convert.ToString(reader["VATRate"]),
                        TotalSum = Convert.ToString(reader["TotalSum"]),
                        IsPayment = Convert.ToString(reader["IsPayment"])
                    };
                    economyFileLogData.Add(fileLogData);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


            finally
            {

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
        
            return economyFileLogData;
        }
    }
}
