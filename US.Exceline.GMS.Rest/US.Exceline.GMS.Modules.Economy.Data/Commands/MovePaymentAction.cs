﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class MovePaymentAction : USDBActionBase<int>
    {
        private string _user = string.Empty;
        private int _paymentID = -1;
        private int _arItemno = -1;
        private string _type = string.Empty;

        public MovePaymentAction(int paymentID, int arItemNo, string type, string user)
        {
            _user = user;
            _paymentID = paymentID;
            _arItemno = arItemNo;
            _type = type;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSMoveErrorPayment";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentID", System.Data.DbType.Int32, _paymentID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@AritemNo", System.Data.DbType.Int32, _arItemno));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Type", System.Data.DbType.String, _type));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                
                SqlParameter output = new SqlParameter();
                output.ParameterName = "@Status";
                output.Direction = System.Data.ParameterDirection.Output;
                output.SqlDbType = System.Data.SqlDbType.Int;
                command.Parameters.Add(output);

                command.ExecuteNonQuery();

                return Convert.ToInt32(output.Value);
               
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
