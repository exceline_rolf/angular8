﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class GetOCRImportHistoryAction : USDBActionBase<List<OCRImportSummary>>
    {
        private DateTime? _startDate;
        private DateTime? _endDate;

        public GetOCRImportHistoryAction(DateTime? startDate, DateTime? endDate)
        {
            _startDate = startDate;
            _endDate = endDate;
        }

        protected override List<OCRImportSummary> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSGetOCRImportProcessHitory";
            DbDataReader reader = null;
            List<OCRImportSummary> summaryList = new List<OCRImportSummary>();
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", System.Data.DbType.DateTime, _startDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", System.Data.DbType.DateTime, _endDate));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    OCRImportSummary summary = new OCRImportSummary();
                    summary.Id = Convert.ToInt32(reader["ID"]);
                    summary.FileId = Convert.ToInt32(reader["FileId"]);
                    summary.FileName = Convert.ToString(reader["FileName"]);
                    summary.PaymentsCount = Convert.ToInt32(reader["PaymentCount"]);
                    summary.StatusCount = Convert.ToInt32(reader["StatusCount"]);
                    summary.PaymentsSum = Convert.ToDecimal(reader["PaymentSum"]);
                    summary.ErrorAccPayments = Convert.ToInt32(reader["ErrorAccPaymentCount"]);
                    summary.ErrorAccStatus = Convert.ToInt32(reader["ErrorAccStatusAcount"]);
                    summary.ErrorAccountPaymentSum = Convert.ToDecimal(reader["ErrorAccPaymentSum"]);
                    summary.ImportedUser = Convert.ToString(reader["ImportedUser"]);
                    summary.ImportedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    summary.TotalPayments = summary.PaymentsSum + summary.ErrorAccountPaymentSum;

                    summaryList.Add(summary);
                }

                return summaryList;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
               
        }
    }
}
