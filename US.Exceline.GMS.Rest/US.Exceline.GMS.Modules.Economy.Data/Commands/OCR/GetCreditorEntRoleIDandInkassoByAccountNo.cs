﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class GetCreditorEntRoleIDandInkassoByAccountNo : USDBActionBase<Creditor>
    {
        private string _creditorAccountNo;
        private string _checkType = string.Empty;
        public GetCreditorEntRoleIDandInkassoByAccountNo(string creditorAccountNo, string checkType)
        {
            _creditorAccountNo = creditorAccountNo;
            _checkType = checkType;
        }
        protected override Creditor Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                Creditor creditor = new Creditor();
                string storedProcedureName = "dbo.USP_GetCrdEntRoleIDandInkassoByAccNo";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditorAccountNo", DbType.String, _creditorAccountNo.Trim()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CheckType", DbType.String, _checkType.Trim()));
                using (DbDataReader detaReader = cmd.ExecuteReader())
                {
                    if (detaReader.Read())
                    {
                        creditor.EntRoleId = detaReader["EntRoleId"] == DBNull.Value ? -1 : Convert.ToInt32(detaReader["EntRoleId"]);
                        creditor.InkassoId = detaReader["CreditorInkassoID"] == DBNull.Value ? string.Empty : Convert.ToString(detaReader["CreditorInkassoID"]);
                        creditor.AccountSource = Convert.ToString(detaReader["SourceName"]);
                        return creditor;
                    }
                    else
                    {
                        return new Creditor();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
