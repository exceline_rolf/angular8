﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class ImportOCRAction : USDBActionBase<bool>
    {
        private int _fileID = -1;
        public ImportOCRAction(int fileID)
        {
            _fileID = fileID;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSCCXImportPaymentStatus";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@OCRFileID", System.Data.DbType.Int32, _fileID));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
