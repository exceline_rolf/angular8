﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddCreditorOrderlineAction : USDBActionBase<bool>
    {
        private int _installmentID = -1;
        private string _creditorNo = string.Empty;
        private string _user = string.Empty;

        public AddCreditorOrderlineAction(int installmentID, string creditorNo, string user)
        {
            _installmentID = installmentID;
            _creditorNo = creditorNo;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            return true;
        }

        public bool RunOnTransaction(DbTransaction transation)
        {
            try
            {
                string spName = "ExceGMS_INV_USExceGMSGenerateOrderlines";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Transaction = transation;
                cmd.Connection = transation.Connection;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentID", System.Data.DbType.Int32, _installmentID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditorNo", System.Data.DbType.String, _creditorNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createUser", System.Data.DbType.String, _user));

                cmd.ExecuteNonQuery();
                return true;
                
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
