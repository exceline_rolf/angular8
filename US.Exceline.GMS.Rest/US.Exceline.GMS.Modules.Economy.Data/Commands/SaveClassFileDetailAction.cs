﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class SaveClassFileDetailAction : USDBActionBase<int>
    {
        private readonly string _fileName = string.Empty;
        private readonly string _filePath = string.Empty;
        private readonly string _user = string.Empty;
        public SaveClassFileDetailAction(string fileName, string filePath, string user)
        {
            _fileName = fileName;
            _filePath = filePath;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "USExceGMSSaveClassFileDetails";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FileName", DbType.String, _fileName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FilePath", DbType.String, _filePath));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _user));

                cmd.ExecuteNonQuery();
                return 1;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
