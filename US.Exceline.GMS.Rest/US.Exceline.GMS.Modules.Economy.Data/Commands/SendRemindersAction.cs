﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class SendRemindersAction : USDBActionBase<bool>
    {
        private int _branchId = -1;

        public SendRemindersAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spname = "USExceGMSEconomySendSMSReminder";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spname);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", System.Data.DbType.Int32, _branchId));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
