﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.ATG
{
    public class GetApplicationSetting : USDBActionBase<ApplicationSetting>
    {    
        protected override ApplicationSetting Body(System.Data.Common.DbConnection connection)
        {
            ApplicationSetting appObj = new ApplicationSetting();
            try
            {
                string storedProcedureName = "dbo.USP_GetApplicationSetting";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                DbDataReader ddReader = cmd.ExecuteReader();

                while (ddReader.Read())
                {
                    appObj.companyName = Convert.ToString(ddReader["name"]).Trim();
                    appObj.TranceMeterID = Convert.ToString(ddReader["transID"]).Trim();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return appObj;
        }
    }
}
