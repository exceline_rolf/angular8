﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddCreditorInvoicingorderLineAction : USDBActionBase<int>
    {
        private CreditorOrderLine _orderline;

        public AddCreditorInvoicingorderLineAction(CreditorOrderLine Orderline)
        {
            _orderline = Orderline;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int _result = -1;

            try
            {
                string storedProcedureName = "dbo.USP_AddCrediorOrderLine";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                if (_orderline.Id > 0)
                    cmd.Parameters.Add(new SqlParameter("@Id ", _orderline.Id));
                cmd.Parameters.Add(new SqlParameter("@Text ", _orderline.Text));
                if (_orderline.OrderNo > 0)
                    cmd.Parameters.Add(new SqlParameter("@OrderNo", _orderline.OrderNo));
                cmd.Parameters.Add(new SqlParameter("@OrderType", _orderline.OrderType));
                if (_orderline.ApportionId > 0)
                    cmd.Parameters.Add(new SqlParameter("@ApportionId", _orderline.ApportionId));
                cmd.Parameters.Add(new SqlParameter("@CreditorNo", _orderline.CreditorNo));
                cmd.Parameters.Add(new SqlParameter("@DebtorEntNo", _orderline.DebtorEntNo));
                cmd.Parameters.Add(new SqlParameter("@CustId", _orderline.CustomerNo));

                if (_orderline.SubCaseNo > 0)
                    cmd.Parameters.Add(new SqlParameter("@SubCaseNo", _orderline.SubCaseNo));
                cmd.Parameters.Add(new SqlParameter("@UnitPrice", _orderline.UnitPrice));
                cmd.Parameters.Add(new SqlParameter("@NoofItems", _orderline.NoOfItems));
                cmd.Parameters.Add(new SqlParameter("@Amount", _orderline.Amount));
                cmd.Parameters.Add(new SqlParameter("@Discount", _orderline.Discount));
                cmd.Parameters.Add(new SqlParameter("@ArticleNo", _orderline.ArticleNo));
                cmd.Parameters.Add(new SqlParameter("@ArticleText", _orderline.ArticleText));
                cmd.Parameters.Add(new SqlParameter("@SponsorReference", _orderline.SponsorReference));
                if (_orderline.NoOfVisits > 0)
                    cmd.Parameters.Add(new SqlParameter("@NoOfVisits", _orderline.NoOfVisits));
                if (_orderline.LastVisitDate != DateTime.MinValue)
                    cmd.Parameters.Add(new SqlParameter("@LastVisitDate", _orderline.LastVisitDate));
                if (_orderline.Balance > 0)
                    cmd.Parameters.Add(new SqlParameter("@Balance", _orderline.Balance));
                if (_orderline.BranchNo > 0)
                    cmd.Parameters.Add(new SqlParameter("@BranchNo", _orderline.BranchNo));
                cmd.Parameters.Add(new SqlParameter("@CoRelationID", _orderline.CoRelationId));
                cmd.Parameters.Add(new SqlParameter("@EmployeeNo", _orderline.EmployNo));
                cmd.Parameters.Add(new SqlParameter("@CreatedUser", _orderline.CreatedUser));
                cmd.Parameters.Add(new SqlParameter("@ModifiedUser", _orderline.ModifiedUser));
                cmd.Parameters.Add(new SqlParameter("@InvoiceNo", _orderline.InvoiceNo));
                cmd.Parameters.Add(new SqlParameter("@IsInvoiceFee", _orderline.IsInvocieFee));
                cmd.Parameters.Add(new SqlParameter("@VoucherExpireDate", _orderline.ExpiryDate));
                cmd.Parameters.Add(new SqlParameter("@VoucherNo", _orderline.VoucherNo));
                cmd.Parameters.Add(new SqlParameter("@Description", _orderline.Description));

                DbParameter para3 = new SqlParameter();
                para3.DbType = DbType.Int32;
                para3.ParameterName = "@OutPutID";
                para3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para3);
                cmd.ExecuteNonQuery();
                _result = Convert.ToInt32(para3.Value);
            }
            catch (Exception)
            {
                throw;
            }
            return _result;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            int _result = -1;

            try
            {
                string storedProcedureName = "dbo.USP_AddCrediorOrderLine";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;

                if (_orderline.Id > 0)
                    cmd.Parameters.Add(new SqlParameter("@Id ", _orderline.Id));
                cmd.Parameters.Add(new SqlParameter("@Text ", _orderline.Text));
                if (_orderline.OrderNo > 0)
                    cmd.Parameters.Add(new SqlParameter("@OrderNo", _orderline.OrderNo));
                cmd.Parameters.Add(new SqlParameter("@OrderType", _orderline.OrderType));
                if (_orderline.ApportionId > 0)
                    cmd.Parameters.Add(new SqlParameter("@ApportionId", _orderline.ApportionId));
                cmd.Parameters.Add(new SqlParameter("@CreditorNo", _orderline.CreditorNo));
                cmd.Parameters.Add(new SqlParameter("@DebtorEntNo", _orderline.DebtorEntNo));
                cmd.Parameters.Add(new SqlParameter("@CustId", _orderline.CustomerNo));

                if (_orderline.SubCaseNo > 0)
                    cmd.Parameters.Add(new SqlParameter("@SubCaseNo", _orderline.SubCaseNo));
                cmd.Parameters.Add(new SqlParameter("@UnitPrice", _orderline.UnitPrice));
                cmd.Parameters.Add(new SqlParameter("@NoofItems", _orderline.NoOfItems));
                cmd.Parameters.Add(new SqlParameter("@Amount", _orderline.Amount));
                cmd.Parameters.Add(new SqlParameter("@Discount", _orderline.Discount));
                cmd.Parameters.Add(new SqlParameter("@ArticleNo", _orderline.ArticleNo));
                cmd.Parameters.Add(new SqlParameter("@ArticleText", _orderline.ArticleText));
                cmd.Parameters.Add(new SqlParameter("@SponsorReference", _orderline.SponsorReference));
                if (_orderline.NoOfVisits > 0)
                    cmd.Parameters.Add(new SqlParameter("@NoOfVisits", _orderline.NoOfVisits));
                if (_orderline.LastVisitDate != DateTime.MinValue)
                    cmd.Parameters.Add(new SqlParameter("@LastVisitDate", _orderline.LastVisitDate));
                if (_orderline.Balance > 0)
                    cmd.Parameters.Add(new SqlParameter("@Balance", _orderline.Balance));
                if (_orderline.BranchNo > 0)
                    cmd.Parameters.Add(new SqlParameter("@BranchNo", _orderline.BranchNo));
                cmd.Parameters.Add(new SqlParameter("@CoRelationID", _orderline.CoRelationId));
                cmd.Parameters.Add(new SqlParameter("@EmployeeNo", _orderline.EmployNo));
                cmd.Parameters.Add(new SqlParameter("@CreatedUser", _orderline.CreatedUser));
                cmd.Parameters.Add(new SqlParameter("@ModifiedUser", _orderline.ModifiedUser));
                cmd.Parameters.Add(new SqlParameter("@InvoiceNo", _orderline.InvoiceNo));
                cmd.Parameters.Add(new SqlParameter("@IsInvoiceFee", _orderline.IsInvocieFee));
                cmd.Parameters.Add(new SqlParameter("@VoucherExpireDate", _orderline.ExpiryDate));
                cmd.Parameters.Add(new SqlParameter("@VoucherNo", _orderline.VoucherNo));
                cmd.Parameters.Add(new SqlParameter("@Description", _orderline.Description));

                DbParameter para3 = new SqlParameter();
                para3.DbType = DbType.Int32;
                para3.ParameterName = "@OutPutID";
                para3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para3);
                cmd.ExecuteNonQuery();
                _result = Convert.ToInt32(para3.Value);
            }
            catch (Exception)
            {
                throw;
            }
            return _result;
        }

    }
}
