﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.GMS.Core.ResultNotifications;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GenerateInvoiceAction : USDBActionBase<OperationResult<string>>
    {
        private string _invoiceKey = string.Empty;
        private string _orderType = string.Empty;
        private string _gymCode = string.Empty;
        private List<InvoicingOrder> _orderIds;
        private string _user = string.Empty;
        private int _branchID = -1;
        public GenerateInvoiceAction(List<InvoicingOrder> orderIds, string invoiceKey, string orderType, string user, string gymCode, int branchID)
        {
            _invoiceKey = invoiceKey;
            _orderType = orderType;
            _gymCode = gymCode;
            _orderIds = orderIds;
            _user = user;
            _branchID = branchID;
        }

        protected override OperationResult<string> Body(System.Data.Common.DbConnection connection)
        {
            AddProcessLog(_user, _orderIds.Count.ToString() + " found For Invoicing branchID : " + _branchID.ToString(), -1,_gymCode);
            try
            {
                OperationResult<string> result = new OperationResult<string>();
                foreach (InvoicingOrder orderId in _orderIds)
                {
                    DbTransaction transaction = connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
                    try
                    {
                        OperationResult<List<IUSPClaim>> claimStatus = new OperationResult<List<IUSPClaim>>();

                        //add Orderline
                        AddCreditorOrderlineAction orderlineAction = new AddCreditorOrderlineAction(orderId.Id, orderId.CreditorNo, _user);
                        if (orderlineAction.RunOnTransaction(transaction))
                        {
                            //add the order 
                            int orderNo = -1;
                            CreateOrderAction orderAction = new CreateOrderAction(Convert.ToInt32(orderId.CreditorNo), _orderType, orderId.Id.ToString());
                            orderNo = orderAction.RunOnTrnsaction(transaction);
                            if (orderNo > 0)
                            {
                                string invoiceNo = string.Empty;
                                AddCreditInvoiceAction creditInvoiceAction = new AddCreditInvoiceAction(Convert.ToInt32(orderId.CreditorNo), orderNo, orderId.Id);
                                invoiceNo = creditInvoiceAction.RunOnTransaction(transaction);
                                if (!string.IsNullOrEmpty(invoiceNo))
                                {
                                     claimStatus = ImportImvoicetoExceline(transaction, Convert.ToInt32(orderId.CreditorNo), invoiceNo, orderNo, -1, "", _user, orderId.Id);
                                }
                                else
                                {
                                    transaction.Rollback();
                                    AddProcessLog(_user, "OrderID : " + orderId.Id.ToString() + " No Invoice Number found ", orderId.Id, _gymCode);
                                    continue;
                                }
                            }
                            else
                            {
                                transaction.Rollback();
                                AddProcessLog(_user, "OrderID : " + orderId.Id.ToString() + " Order not added ", orderId.Id, _gymCode);
                                continue;
                            }
                        }
                        else
                        {
                            transaction.Rollback();
                            AddProcessLog(_user, "OrderID : " + orderId.Id.ToString() + " Orderline Did not added ", orderId.Id, _gymCode);
                            continue;
                        }
                        transaction.Commit();

                        //settle with prepaid Balance 
                        try
                        {
                            SettleInvoicesWithPrepaidBalanceAction action = new SettleInvoicesWithPrepaidBalanceAction(claimStatus.OperationReturnValue[0].ARItemNo, _user);
                            action.Execute(EnumDatabase.Exceline, _gymCode);
                        }
                        catch (Exception)
                        {
                            AddProcessLog(_user, "OrderID : " + orderId.Id.ToString() + "Error in settling invoice with Prepaid Balance ", orderId.Id, _gymCode);
                        }

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        result.ErrorOccured = true;
                        result.CreateMessage("OrderID : " + orderId.Id.ToString() +" " + ex.Message, MessageTypes.ERROR);
                        AddProcessLog(_user, "OrderID : " + orderId.Id.ToString() + " " + ex.Message, orderId.Id, _gymCode);
                        continue;
                    }
                }
                return result;
            }
            catch (Exception)
            {
               
                throw;
            }
        }

        private OperationResult<List<IUSPClaim>> ImportImvoicetoExceline(DbTransaction transaction, int creditorNo, string invoiceNo, int orderNo, int fileId, string fileName, string userName, int OrderID)
        {
            OperationResult<List<IUSPClaim>> result = new OperationResult<List<IUSPClaim>>();
            List<USPVATClaim> vatClaims = new List<USPVATClaim>();
            GetVATClaimDataAction vatAction = new GetVATClaimDataAction(creditorNo,invoiceNo);
            vatClaims = vatAction.RunOntransaction(transaction);

            if(vatClaims != null)
            {
                if (vatClaims.Count > 0)
                {
                    List<IUSPClaim> claimResultList = new List<IUSPClaim>();
                    foreach (USPVATClaim uspVATClaim in vatClaims)
                    {
                        OperationResult claimImportResult = new OperationResult();
                        IUSPClaim claim = GetClaimByVATClaim(transaction, uspVATClaim, orderNo, userName);
                        claimImportResult = AddClaim(transaction, claim, fileId, fileName, userName);
                        if (uspVATClaim.Id > 0 & claimImportResult.TagSavedID != null)
                        {
                            claim.ARItemNo = Convert.ToInt32(claimImportResult.TagSavedID);
                            claim.InvoiceNumber = Convert.ToString(claimImportResult.Tag1);
                            UpdateCreditorInvoiceImportStatus(transaction, uspVATClaim.Id, Convert.ToInt32(claimImportResult.TagSavedID));
                            claimResultList.Add(claim);
                        }
                        else
                        {
                            throw new Exception("Order ID : " + OrderID + ", Claim Not added", null);
                        }
                    }
                    result.OperationReturnValue = claimResultList;
                }
                else
                {
                    throw new Exception("Order ID : " + OrderID + ", No VAT claim found", null);
                }
                return result;

            } else
            {
                throw new Exception("Order ID : " + OrderID + ", No VAT claim found", null);
            }
        }

        public static IUSPClaim GetClaimByVATClaim(DbTransaction transaction, USPVATClaim uspVATClaim, int orderNo, string userName)
        {
            IUSPClaim claim = new USPClaim();
            try
            {
                claim.Creditor = ToCreditor(uspVATClaim);
                claim.Debtor = ToDebitor(uspVATClaim);

                switch (uspVATClaim.InvoiceTypeId)
                {
                    case 1:
                        claim.InvoiceType = Core.SystemObjects.Enums.InvoiceTypes.DirectDeduct;
                        break;
                    case 2:
                        claim.InvoiceType = Core.SystemObjects.Enums.InvoiceTypes.InvoiceForPrint;
                        break;
                    case 3:
                      claim.InvoiceType= Core.SystemObjects.Enums.InvoiceTypes.Invoice;
                        break;
                    case 4:
                        claim.InvoiceType =  Core.SystemObjects.Enums.InvoiceTypes.DebtWarning;
                        break;

                    case 7: 
                        claim.InvoiceType=  Core.SystemObjects.Enums.InvoiceTypes.Sponser;
                        break;
                    case 14:
                        claim.InvoiceType = Core.SystemObjects.Enums.InvoiceTypes.PaymentDocumentForPrint;
                        break;//ShopInvoice
                    case 101:
                        claim.InvoiceType =  Core.SystemObjects.Enums.InvoiceTypes.ShopInvoice;
                        break;

                    default:
                        claim.InvoiceType = Core.SystemObjects.Enums.InvoiceTypes.Invoice;
                        break;
                }
              
                claim.NameInContract = uspVATClaim.NameInContract;
                claim.AccountNumber = uspVATClaim.AccountNumber;
                claim.ContractNumber = uspVATClaim.ContractNumber;
                claim.ContractExpire = uspVATClaim.ContractExpire;
                claim.LastVisit =uspVATClaim.LastVisit;
                claim.LastContract =uspVATClaim.LastContract;
                claim.ContractKID = uspVATClaim.ContractKID;
                //claim.Status = (US.Payment.Core.Enums.DirectDeductStatus)lineParts[21].ToValidInteger(20, "Status", false);
                claim.InstallmentNumber = uspVATClaim.InstallmentNumber;
                claim.InvoiceNumber =uspVATClaim.InvoiceNo;
                claim.DueBalance = uspVATClaim.DueBalance;
                claim.Balance = uspVATClaim.DueBalance;
                claim.InvoiceRef =uspVATClaim.CreditorInkassoID;
                claim.Text = uspVATClaim.InvoiceText;               
                claim.InvoicedDate = uspVATClaim.InvoicedDate.Year + "-" + uspVATClaim.InvoicedDate.Month + "-" + uspVATClaim.InvoicedDate.Day;
                claim.DueDate = uspVATClaim.DueDate.Year + "-" + uspVATClaim.DueDate.Month + "-" + uspVATClaim.DueDate.Day;
                //claim.PaidDate = uspVATClaim.PaidDate.ToShortDateString();
                //claim.PrintDate = DataUtils.ObjectToString(lineParts[29]).ToSQLDateFormat(27, "printDate", false);
                claim.OriginalDueDate = claim.OriginalDueDate;
                claim.Amount = uspVATClaim.Amount;
                claim.InvoiceCharge = uspVATClaim.InvoiceCharge;
                claim.ReminderFee = uspVATClaim.ReminderFee;
                claim.KID = uspVATClaim.InstallmentKID;
                claim.TransmissionNumberBBS = uspVATClaim.TransmissionNumberBBS;
                claim.Balance = uspVATClaim.Balance;
                claim.DueBalance = uspVATClaim.DueBalance;
                claim.LastReminder = uspVATClaim.LastReminder;
                claim.BranchNumber = uspVATClaim.BranchNo;
                
               //claim.CollectingStatus = (US.Payment.Core.Enums.CollectingStatus)lineParts[39].ToValidInteger(37, "CollectingStatus", false);
               // claim.InkassoTest = lineParts[40];
                //claim.BranchNumber = lineParts[41];
                if (uspVATClaim.InvoiceTypeId == 7)
                {
                    claim.OrderLines = GetSponsorOrderLine(transaction, orderNo);
                
                }
                claim.InvoiceRef = string.Empty;

                return claim;
            }
            catch (Exception )
            {
                throw;
            }
        }

        private static IUSPCreditor ToCreditor(USPVATClaim uspVATClaim)
        {
            IUSPCreditor creditor = new USPCreditor();
            creditor.CreditorInkassoID = uspVATClaim.CreditorNo;
            return creditor;
        }

        private static IUSPDebtor ToDebitor(USPVATClaim uspVATClaim)
        {
            IUSPDebtor debitor = new USPDebtor();
            try
            {
                debitor.DebtorFirstName = uspVATClaim.DebtorFirstName;
                debitor.DebtorSecondName = uspVATClaim.DebtorLastName;
                debitor.DebtorAddressList = ToAddress(uspVATClaim);
                if (uspVATClaim.DebtorBirthDay.Year > 1900)
                {
                    debitor.DebtorBirthDay = uspVATClaim.DebtorBirthDay.Year + "-" + uspVATClaim.DebtorBirthDay.Month + "-" + uspVATClaim.DebtorBirthDay.Day;
                }
                debitor.DebtorcustumerID = uspVATClaim.DebtorNo;
                debitor.DebtorPersonNo = uspVATClaim.DebtorNo;
                debitor.DebtorEntityID = uspVATClaim.DebtorEntNo;

                debitor.DebtorPersonNo = string.Empty;
            }
            catch (Exception )
            {
                throw ;
            }
            return debitor;
        }
        private static List<IUSPAddress> ToAddress(USPVATClaim uspVATClaim)
        {
            List<IUSPAddress> addressList = new List<IUSPAddress>();
            try
            {
                IUSPAddress address = new USPAddress();
                address.ZipCode = uspVATClaim.ZipCode;
                address.CountryCode = uspVATClaim.CountryId;
                address.AddSource = string.Empty;
                address.Address1 = uspVATClaim.Addr1;
                address.Address2 = uspVATClaim.Addr2;
                address.Address3 = uspVATClaim.Addr3;
                address.TelWork = uspVATClaim.TelWork;
                address.TelMobile = uspVATClaim.TelMobile;
                address.TelHome = uspVATClaim.TelHome;
                address.Email = uspVATClaim.Email;
                address.City = uspVATClaim.City;
                address.Fax = uspVATClaim.Fax;
                address.MSN = uspVATClaim.MSN;
                address.Skype = string.Empty;
                addressList.Add(address);
            }
            catch (Exception )
            {
                throw ;
            }
            return addressList;
        }

        private static List<IUSPOrderLine> GetSponsorOrderLine(DbTransaction transaction, int orderNo)
        {
            GetSponsorOrderLinesAction action = new GetSponsorOrderLinesAction(orderNo);
            return action.RunOnTransaction(transaction);
        }

        private static int UpdateCreditorInvoiceImportStatus(DbTransaction transaction, int creditorInvoiceId, int arItemNo)
        {
           UpdateCreditorInvoiceImportStatusAction action = new UpdateCreditorInvoiceImportStatusAction(creditorInvoiceId,arItemNo);
           return action.RunOnTransaction(transaction);
        }

         public static OperationResult AddClaim(DbTransaction transaction, IUSPClaim claim,int fileId,string fileName,string userName)
         {
             // Create object that send as result of method invocation
            OperationResult notificationResult = new OperationResult();
            // set comming tags to tags of sending result
            notificationResult.Tag1 = claim.Tag1;
            notificationResult.Tag2 = claim.Tag2;
            ClaimHandler claimHandler = null;

            try
            {
                // create ClaimHandler object (class that is parent class of all type of claim handling classes)
                // each child class of ClaimHandler class has logics specific to that claim type

                OperationResult resultFromSaveClaim = null;

                // Create claim handling class according to claim type
                switch (claim.InvoiceType)
                {
                    case InvoiceTypes.DirectDeduct:
                        // Claim is a Direct Deduct. DirectDeductHandler class has logics that are specific to Direct Deduct handling
                        claimHandler = new DirectDeductHandler(userName);
                        break;
                    case InvoiceTypes.InvoiceForPrint:
                        // Claim is a Invoice for Printing. IPorSPRecordHandle class has logics that are specific to Invoice for Printing handling
                        claimHandler = new IPRecordHandler(userName);
                        break;                    
                    case InvoiceTypes.DebtWarning:
                        // Claim is a Debt Warning. DebtWarning class has logics that are specific to Debt Warning handling
                        claimHandler = new DebtWarningHandler(userName);
                        break;
                    case InvoiceTypes.Sponser:
                        // Claim is a Sponser record. Sponser class has logics that are specific to Sponser handling
                        claimHandler = new IPorSPRecordHandle(userName);
                        break;
                    case InvoiceTypes.Invoice:
                        // Claim is a Invoice. Invoice class has logics that are specific to Invoice handling
                        claimHandler = new InvoiceRecordHandler(userName);
                        break;
                    case InvoiceTypes.PaymentDocumentForPrint:
                        // Claim is a Invoice. Invoice class has logics that are specific to Invoice handling
                        claimHandler = new PaymentForPrintHandler(userName);
                        break;

                    default :
                        claimHandler = new InvoiceRecordHandler(userName);
                        break;
                }
                // Save the claim and get result of adding that claim
                resultFromSaveClaim = claimHandler.SaveClaim(transaction,claim, fileId, fileName, userName);
                // Add messages of above operation to sending result
                //notificationResult.AddUSNotificationMessage(resultFromSaveClaim.NotificationMessages);
                // If above operation sent a error update seding result to has a error
                notificationResult.ErrorOccured = resultFromSaveClaim.ErrorOccured;
                notificationResult.TagSavedID = resultFromSaveClaim.TagSavedID;
                notificationResult.Tag1 = claim.InvoiceNumber;
            }
           
            catch (Exception ex)
            {
                throw new Exception("Failed to add claim : " + ex.Message, ex);
            }
            return notificationResult;
        }

         private static void AddProcessLog(string user, string message, int orderID, string gymCode)
         {
             AddProcessLogAction action = new AddProcessLogAction(user, message, orderID);
             action.Execute(EnumDatabase.Exceline, gymCode);
         }
    }
}
