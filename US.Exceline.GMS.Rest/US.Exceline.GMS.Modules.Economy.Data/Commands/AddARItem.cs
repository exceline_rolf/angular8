﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddARItem : USDBActionBase<int>
    {
        private string invoice;
        private string date = string.Empty;
        private string dueDate = string.Empty;
        private string textDiscription;
        private decimal amout;
        //private int ArNo;
        private string itemtype;
        private string kid;
        private int SubCaseNo;

        private int ARItemNo;
        private int _paymentId;
        private string _cusId = string.Empty;
        private string _incassoId = string.Empty;
        private string _exTransNo = string.Empty;
        private string payDate = string.Empty;
        private string _invoicePath = string.Empty;
        private string _printDate = string.Empty;
        private int isPrinted = 0;
        private string _fileName = string.Empty;
        private string _transferDate = string.Empty;
        private string _contractKid = string.Empty;

        public AddARItem(ARItem ARIObj)
        {
            this.invoice = ARIObj.InvoiceNumber.Trim();
            this.date = ARIObj.InvoicedDate;
            this.dueDate = ARIObj.DueDate;
            this.textDiscription = ARIObj.Text.Trim();
            this.amout = ARIObj.Amount;
            this.itemtype = ARIObj.itemType.ToString().Trim();
            //this.ArNo = ARIObj.ARNo;
            this.kid = ARIObj.KID.Trim();
            this.SubCaseNo = ARIObj.subCaseNo;
            _paymentId = ARIObj.PaymentId;
            _cusId = ARIObj.cusID.Trim();
            _incassoId = ARIObj.incasso.Trim();
            _exTransNo = ARIObj.ExternalTarnsactionNumber.Trim();
            this.payDate = ARIObj.payDate;
            isPrinted = ARIObj.IsPrinted;
            _invoicePath = ARIObj.InvoicePath;
            _printDate = ARIObj.PrintDate;
            _fileName = ARIObj.FileName;
            _transferDate = ARIObj.TransferDate;
            _contractKid = ARIObj.ContractKID;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "dbo.USP_AddARItem";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = connection;

                // cmd.Connection = GenericDBFactory.GetConnection(EnumDatabase.USP);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ref", DbType.String, invoice));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@ARNo", DbType.Int32, (object)ArNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CaseNo", DbType.Int32, SubCaseNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@txt", DbType.String, textDiscription));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemType", DbType.String, itemtype));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, amout));

                //if (!string.IsNullOrEmpty(isPrinted))
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isPrinted", DbType.Int32, isPrinted));

                if (!string.IsNullOrEmpty(_invoicePath))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoicePath", DbType.String, _invoicePath));

                if (!string.IsNullOrEmpty(_printDate))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@printDate", DbType.String, _printDate.Trim()));

                if (!string.IsNullOrEmpty(date))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@voucherDate", DbType.String, date.Trim()));
                }
                if (!string.IsNullOrEmpty(dueDate))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@dueDate", DbType.String, dueDate.Trim()));
                }
                if (!string.IsNullOrEmpty(payDate))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@payDate", DbType.String, payDate.Trim()));
                }

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@kid", DbType.String, kid));

                if (_paymentId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@paymentId", DbType.Int32, _paymentId));
                }
                if (!string.IsNullOrEmpty(_cusId))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@cusId", DbType.String, _cusId));
                }
                if (!string.IsNullOrEmpty(_incassoId))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditorInkassoID", DbType.String, _incassoId));
                }

                if (!string.IsNullOrEmpty(_exTransNo))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExtTransNo", DbType.String, _exTransNo));
                }
                if (!string.IsNullOrEmpty(_fileName))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@fileName", DbType.String, _fileName));
                }
                if (!string.IsNullOrEmpty(_contractKid))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractKid", DbType.String, _contractKid));
                }

                if (!string.IsNullOrEmpty(_transferDate))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@TransferDate", DbType.String, _transferDate));

                ARItemNo = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                ARItemNo = -1;
                US_DataAccess.core.Exception.USPException exp = new US_DataAccess.core.Exception.USPException();
                //exp.logg(ex.Message);
                exp.ErrorMessage = ex.Message;
                throw exp;
            }
            return ARItemNo;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            try
            {
                string storedProcedureName = "dbo.USP_AddARItem";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ref", DbType.String, invoice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CaseNo", DbType.Int32, SubCaseNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@txt", DbType.String, textDiscription));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemType", DbType.String, itemtype));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, amout));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isPrinted", DbType.Int32, isPrinted));

                if (!string.IsNullOrEmpty(_invoicePath))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoicePath", DbType.String, _invoicePath));

                if (!string.IsNullOrEmpty(_printDate))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@printDate", DbType.String, _printDate.Trim()));

                if (!string.IsNullOrEmpty(date))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@voucherDate", DbType.String, date.Trim()));
                }
                if (!string.IsNullOrEmpty(dueDate))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@dueDate", DbType.String, dueDate.Trim()));
                }
                if (!string.IsNullOrEmpty(payDate))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@payDate", DbType.String, payDate.Trim()));
                }

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@kid", DbType.String, kid));

                if (_paymentId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@paymentId", DbType.Int32, _paymentId));
                }
                if (!string.IsNullOrEmpty(_cusId))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@cusId", DbType.String, _cusId));
                }
                if (!string.IsNullOrEmpty(_incassoId))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditorInkassoID", DbType.String, _incassoId));
                }

                if (!string.IsNullOrEmpty(_exTransNo))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExtTransNo", DbType.String, _exTransNo));
                }
                if (!string.IsNullOrEmpty(_fileName))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@fileName", DbType.String, _fileName));
                }
                //if (!string.IsNullOrEmpty(_contractKid))
                //{
                //    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractKid", DbType.String, _contractKid));
                //}

                if (!string.IsNullOrEmpty(_transferDate))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@TransferDate", DbType.String, _transferDate));

                ARItemNo = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                ARItemNo = -1;
                US_DataAccess.core.Exception.USPException exp = new US_DataAccess.core.Exception.USPException();
                exp.ErrorMessage = ex.Message;
                throw exp;
            }
            return ARItemNo;
        }
    }
}
