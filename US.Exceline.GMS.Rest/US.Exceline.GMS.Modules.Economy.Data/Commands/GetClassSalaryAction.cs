﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetClassSalaryAction : USDBActionBase<List<ClassDetail>>
    {
        private readonly List<int> _branchIdList;
        private readonly List<int> _employeeIdList;
        private readonly List<int> _categoryIdList;
        private readonly DateTime? _fromDate;
        private readonly DateTime? _toDate;
        private DataTable _dataTableBranch = null;
        private DataTable _dataTableEmploye = null;
        private DataTable _dataTableCategory = null;
        public GetClassSalaryAction(List<int> branchIdList, List<int> employeeIdList, List<int> categoryIdList, DateTime fromDate, DateTime toDate)
        {
            _dataTableBranch = GetIdLst(branchIdList, _dataTableBranch);
            _dataTableEmploye = GetIdLst(employeeIdList, _dataTableEmploye);
            _dataTableCategory = GetIdLst(categoryIdList, _dataTableCategory);
            _fromDate = fromDate;
            _toDate = toDate;
        }

        private DataTable GetIdLst(List<int> idLst, DataTable dataTable)
        {
            dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
            if (idLst != null && idLst.Any())
                foreach (var item in idLst)
                {
                    DataRow dataTableRow = dataTable.NewRow();
                    dataTableRow["ID"] = item;
                    dataTable.Rows.Add(dataTableRow);
                }
            return dataTable;
        }

        protected override List<ClassDetail> Body(DbConnection connection)
        {
            const string spName = "USExceGMSGetClassSalaryDetails";
            DbDataReader reader = null;
            var classDetailList = new List<ClassDetail>();
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchIdList", SqlDbType.Structured, _dataTableBranch));
                command.Parameters.Add(DataAcessUtils.CreateParam("@employeeIdList", SqlDbType.Structured, _dataTableEmploye));
                command.Parameters.Add(DataAcessUtils.CreateParam("@categoryIdList", SqlDbType.Structured, _dataTableCategory));
                command.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime, _fromDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.DateTime, _toDate));

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var classDetail = new ClassDetail();
                    classDetail.CompanyId = Convert.ToString(reader["CompanyId"]);
                    classDetail.PayrollNumber = Convert.ToString(reader["EmpSalaryNumber"]);
                    classDetail.CategoryId = Convert.ToString(reader["CategoryId"]);
                    classDetail.ArtNumber = Convert.ToString(reader["ArtNumber"]);
                    classDetail.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    classDetail.Timer = Convert.ToString(reader["Timer"]);
                    classDetailList.Add(classDetail);
                }
                return classDetailList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Dispose();
            }

        }

        //private DataTable GetActivityItemLst(List<ActivityDC> activityList)
        //{
        //    _dataTable = new DataTable();
        //    _dataTable.Columns.Add(new DataColumn("ActivityId", typeof(System.Int32)));
        //    foreach (ActivityDC activityItem in activityList)
        //    {
        //        DataRow _dataTableRow = _dataTable.NewRow();
        //        _dataTableRow["ActivityId"] = activityItem.Id;
        //        _dataTable.Rows.Add(_dataTableRow);
        //    }
        //    return _dataTable;
        //}
    }
}
