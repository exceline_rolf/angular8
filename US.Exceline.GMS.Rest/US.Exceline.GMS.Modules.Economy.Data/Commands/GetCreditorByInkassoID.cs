﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetCreditorByInkassoID : USDBActionBase<Creditor>
    {
        private int _inkassoID;
        public GetCreditorByInkassoID(int inkassoID)
        {
            _inkassoID = inkassoID;
        }

        protected override Creditor Body(System.Data.Common.DbConnection connection)
        {
            return null;
        }

        public Creditor RunOnTransaction(DbTransaction transaction)
        {
            DbDataReader reader = null;
            Creditor creditorObj = null;
            try
            {

                string storedProcedureName = "dbo.USP_GetCreditorByInkassoID";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@inkassoId", DbType.Int32, (object)_inkassoID));
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    creditorObj = new Creditor();
                    creditorObj.Name = Convert.ToString(reader["Name"]);
                    creditorObj.Born = Convert.ToString(reader["Born"]);
                    creditorObj.EntID = Convert.ToString(reader["EntNo"]);
                    creditorObj.PersonNo = Convert.ToString(reader["PersonNo"]);
                }
            }
            catch (Exception)
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return creditorObj;
        }
    }
}
