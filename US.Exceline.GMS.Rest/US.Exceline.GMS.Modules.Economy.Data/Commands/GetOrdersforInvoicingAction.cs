﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetOrdersforInvoicingAction : USDBActionBase<List<InvoicingOrder>>
    {
        private int _branchID = -1;
        private DateTime? _startDueDate;
        private DateTime? _endDueDate;
        private string _orderType = string.Empty;

        public GetOrdersforInvoicingAction(int branchID, DateTime? startDueDate, DateTime? endDueDate, string orderType)
        {
            _branchID = branchID;
            _startDueDate = startDueDate;
            _endDueDate = endDueDate;
            _orderType = orderType;
        }

        protected override List<InvoicingOrder> Body(System.Data.Common.DbConnection connection)
        {
            List<InvoicingOrder> orderIDs = new List<InvoicingOrder>();
            string spName = "ExceGMSGetOrdersforInvoice";
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", System.Data.DbType.Int32, _branchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartDueDate", System.Data.DbType.DateTime, _startDueDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@EndDueDate", System.Data.DbType.DateTime, _endDueDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@OrderType", System.Data.DbType.String, _orderType));

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    InvoicingOrder invOrder = new InvoicingOrder();
                    invOrder.Id = Convert.ToInt32(reader["ID"]);
                    invOrder.CreditorNo = Convert.ToString(reader["CreditorInkassoID"]);
                    orderIDs.Add(invOrder);
                }
               
            }
            catch (Exception)
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return orderIDs;
        }
    }
}
