﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    class SaveARComplete : USDBActionBase<int>
    {
        AR _arToBeSaved = null;
        ARItem _arItemBeSaved;
        string _inkassoID;
        int _debitorID;
        private int _addressNo;
        private int _parentARNo;
        private string _userName = string.Empty;


        public SaveARComplete(AR arToBeSaved, ARItem arItemBeSaved, string inkassoID, int debitorID, int addressNo, int parentARNo, string userName)
        {
            _arToBeSaved = arToBeSaved;
            _arItemBeSaved = arItemBeSaved;
            _inkassoID = inkassoID.Trim();
            _debitorID = debitorID;
            _addressNo = addressNo;
            _parentARNo = parentARNo;
            _userName = userName;

        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            return -1;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            int savedARItemNo = -1;
            int subCaseNo = -1;

            try
            {
                try
                {
                    SaveARandARRole saveARCmd = new SaveARandARRole(_arToBeSaved.CID, _inkassoID, _debitorID, _arToBeSaved.Name, _addressNo.ToString(), string.Empty, _parentARNo, _arToBeSaved.TransferDate);
                    subCaseNo = saveARCmd.RunOntransaction(transaction);
                }
                catch
                {
                    throw new Exception("Error in adding ar role");
                }

                if (subCaseNo == -1)
                {
                    throw new Exception("Error in adding ar role");
                }
                else
                {
                    try
                    {
                        _arItemBeSaved.subCaseNo = subCaseNo;
                        AddARItem arItemComm = new AddARItem(_arItemBeSaved);
                        savedARItemNo = arItemComm.RunOnTransaction(transaction);
                    }
                    catch
                    {
                        throw new Exception("Error in adding aritem");
                    }
                }

            }
            catch (Exception ex)
            {
                //arTrans.Rollback();
                throw new Exception("Failed to save AR " + ex.Message);
            }
            return savedARItemNo;
        }
    }
}
