﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class ReverseApportionmentPaymentAction : USDBActionBase<int>
    {
        private int _paymentId = -1;
        private int _isReversePayment = -1;
        public ReverseApportionmentPaymentAction(int paymentId, int IsReversePayment)
        {
            _paymentId = paymentId;
            _isReversePayment = IsReversePayment;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            return 0;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            int _result = -1;
            try
            {
                DbCommand cmd = GenericDBFactory.Factory.CreateCommand();
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;
                cmd.CommandText = "USP_ReverseApportionedPayment";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaymentId", System.Data.DbType.Int32, _paymentId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsReversePayment", System.Data.DbType.Int32, _isReversePayment));
                DbParameter outPara = new SqlParameter();
                outPara.ParameterName = "@ReserseResult";
                outPara.DbType = System.Data.DbType.Int32;
                outPara.Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add(outPara);

                cmd.ExecuteNonQuery();

                _result = Convert.ToInt32(outPara.Value);

            }
            catch (Exception)
            {
                throw;
            }
            return _result;
        }
    }
}
