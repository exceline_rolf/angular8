﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    class ReadARItem : USDBActionBase<int>
    {
        private string custId;
        private int InkassoId;
        private string invoice;
        public DbDataReader invoiceData;
        public ReadARItem(int inka, string cust, string invoice)
        {
            this.InkassoId = inka;
            this.custId = cust.Trim();
            this.invoice = invoice.Trim();
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int arItemNo = -2;
            try
            {

                string storedProcedureName = "dbo.USP_GetInvoiceDetails";
                DbCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedureName;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@inkassoId", DbType.Int32, InkassoId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@cusId", DbType.String, custId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ref", DbType.String, invoice));
                invoiceData = cmd.ExecuteReader();
                if (invoiceData.Read())
                {
                    arItemNo = Convert.ToInt32(invoiceData[0]); // 
                }

            }
            catch (Exception)
            {
                throw;
            }
          
            return arItemNo;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            int arItemNo = -2;
            try
            {

                string storedProcedureName = "dbo.USP_GetInvoiceDetails";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@inkassoId", DbType.Int32, InkassoId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@cusId", DbType.String, custId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ref", DbType.String, invoice));
                invoiceData = cmd.ExecuteReader();
                if (invoiceData.Read())
                {
                    arItemNo = Convert.ToInt32(invoiceData[0]); // 
                }

                if (invoiceData != null)
                {
                    invoiceData.Close();
                    invoiceData.Dispose();
                }

            }
            catch (Exception)
            {
                if (invoiceData != null)
                {
                    invoiceData.Close();
                    invoiceData.Dispose();
                }
                throw;
            }
            return arItemNo;
        }
    }
}
