﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class ReverseApportionmentByArItemNoAction : USDBActionBase<int>
    {
        private int _paymentId = -1;
        private int _arItem = -1;
        private string _user = string.Empty;

        public ReverseApportionmentByArItemNoAction(int paymentID, int arItem, string user)
        {
            _paymentId = paymentID;
            _arItem = arItem;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            return 0;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            int result = -1;
            try
            {
                DbCommand command = GenericDBFactory.Factory.CreateCommand();
                command.CommandText = "USP_ReverseApportionmentByArItemNo";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.Connection = transaction.Connection;

                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentId", System.Data.DbType.Int32, _paymentId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@arItemNo", System.Data.DbType.Int32, _arItem));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                DbParameter outPara = new SqlParameter();
                outPara.ParameterName = "@ReverseAppResult";
                outPara.Direction = System.Data.ParameterDirection.Output;
                outPara.DbType = System.Data.DbType.Int32;
                command.Parameters.Add(outPara);
                command.ExecuteNonQuery();

                result = Convert.ToInt32(outPara.Value);

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
    }
}
