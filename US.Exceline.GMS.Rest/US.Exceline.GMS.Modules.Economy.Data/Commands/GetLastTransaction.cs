﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetLastTransactionAction : USDBActionBase<String>
    {
        private int _salePointID = -1;
        private int _branchID = -1;


        public GetLastTransactionAction(int salePointID,int branchID)
        {
            _salePointID = salePointID;
            _branchID = branchID;

        }


        protected override String Body(System.Data.Common.DbConnection connection)
        {
            string spName = "GetLastTransaction";
            DbDataReader reader = null;
            String economyFileLogData = "";



            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@salePointID", System.Data.DbType.Int32, _salePointID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchID", System.Data.DbType.Int32, _branchID));
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    String signature;
                    {

                        signature = Convert.ToString(reader["signature"]);

                    };

                    economyFileLogData = signature;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


            finally
            {

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return economyFileLogData;
        }
    }
}
