﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetInformationForInvoicePrintAction : USDBActionBase<InvoiceInfo>
    {
        private String _invoiceRef = String.Empty;
        private int _branchId = 0;

        public GetInformationForInvoicePrintAction(String invoiceRef, int branchId)
        {
            _invoiceRef = invoiceRef;
            _branchId = branchId;
        }

        protected override InvoiceInfo Body(DbConnection connection)
        {
            string spName = "GetInformationForInvoicePrint";
            DbDataReader reader = null;
            InvoiceInfo AllInvoiceInfo = new InvoiceInfo();
            SenderInformation asd  = new SenderInformation();

            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Ref", System.Data.DbType.String, _invoiceRef));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                reader = command.ExecuteReader();
                
                while (reader.Read())
                {
                    InvoiceOrderLine Orderline = new InvoiceOrderLine();
                    {
                        //PayerInfo
                        AllInvoiceInfo.PayerName = Convert.ToString(reader["CustomerName"]);
                        AllInvoiceInfo.PayerAdress = Convert.ToString(reader["CustomerAdress"]);
                        AllInvoiceInfo.PayerPostNumber = Convert.ToString(reader["CustomerZipCode"]);
                        AllInvoiceInfo.PayerPostPlace = Convert.ToString(reader["CustomerZipName"]);
                        AllInvoiceInfo.InvoiceNumber = Convert.ToString(reader["InvoiceRef"]);
                        AllInvoiceInfo.PaidByDate = Convert.ToString(reader["DueDate"]);
                        AllInvoiceInfo.InvoiceDate = Convert.ToString(reader["InvoiceGeneratedDay"]);
                        AllInvoiceInfo.CustomerId = Convert.ToString(reader["CustomerId"]);
                        AllInvoiceInfo.ReceiverAccountNumber = Convert.ToString(reader["ReceiverAccountNumber"]);
                        AllInvoiceInfo.KID = Convert.ToString(reader["KID"]);
                        AllInvoiceInfo.IsATG = Convert.ToBoolean(reader["IsATG"]);
                        AllInvoiceInfo.CustomerRefrence = Convert.ToString(reader["CustomerRefrence"]);
                        AllInvoiceInfo.EmployeeNo = Convert.ToString(reader["EmployeeNo"]);
                        AllInvoiceInfo.PaidOnBehalfOfCustId = Convert.ToString(reader["PaidOnBehalfOfCustId"]);
                        AllInvoiceInfo.PaidOnBehalfOfName = Convert.ToString(reader["PaidOnBehalfOfName"]);
                        //SenderInfo
                        asd.SenderName = Convert.ToString(reader["BranchName"]);
                        asd.SenderAdress = Convert.ToString(reader["SenderAdress"]);
                        asd.SenderWebSite = Convert.ToString(reader["Web"]);
                        asd.SenderZipCode = Convert.ToString(reader["SenderZipCode"]);
                        asd.SenderZipName = Convert.ToString(reader["SenderZipName"]);
                        asd.SenderPhoneNumber = Convert.ToString(reader["SenderPhoneNumber"]);
                        asd.SenderOrganiztionNumber = Convert.ToString(reader["SenderOrganiztionNumber"]);
                        AllInvoiceInfo.SenderInfo = asd;
                        //OrderLineInfo
                        Orderline.ArticleDescription = Convert.ToString(reader["ArticleDescription"]);
                        Orderline.PeriodSpan = Convert.ToString(reader["Periode"]);
                        Orderline.NumberOfUnits = Convert.ToString(reader["NumberOfUnits"]);
                        Orderline.UnitPrice = Convert.ToString(reader["UnitPrice"]);
                        Orderline.VATRate = Convert.ToDecimal(reader["VATRate"]);
                        Orderline.Note = Convert.ToString(reader["Note"]);

                    };
                    AllInvoiceInfo.OrderLines.Add(Orderline);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


            finally
            {

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return AllInvoiceInfo;
        }
    }
}
