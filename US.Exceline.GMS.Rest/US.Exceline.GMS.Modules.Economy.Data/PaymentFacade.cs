﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;

namespace US.Exceline.GMS.Modules.Economy.Data
{
    public class PaymentFacade
    {
        private static ISQLServerEconomyDataAdapter GetDataAdapter()
        {
            return new SQLServerEconomyDataAdapter();
        }

        public static List<USPErrorPayment> GetErrorPaymentDetails(string errorPaymentType, DateTime from, DateTime to, string gymCode, int branchid, string batchId=null)
        {
            return GetDataAdapter().GetErrorPaymentDetails(errorPaymentType, from, to, gymCode, branchid, batchId);
        }

        public static bool RemovePayment(int paymentID, int arItemNo,  string user, string gymCode, string type)
        {
            return GetDataAdapter().RemovePayment(paymentID, arItemNo, user, gymCode, type);
        }

        public static int MovePayment(int paymentID, int aritemno, string type, string user, string gymCode)
        {
            return GetDataAdapter().MovePayment(paymentID, aritemno, type, user, gymCode);
        }
    }
}
