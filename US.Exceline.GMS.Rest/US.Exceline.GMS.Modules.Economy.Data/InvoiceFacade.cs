﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Ccx;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.Data
{
    public class InvoiceFacade
    {
        private static ISQLServerEconomyDataAdapter GetDataAdapter()
        {
            return new SQLServerEconomyDataAdapter();
        }

        public static List<USPARItem> AdvanceInvoiceSearch(InvoiceSearchCriteria criteria, SearchModes searchMode, object constValue, string gymCode)
        {
            return GetDataAdapter().AdvanceInvoiceSearch(criteria, searchMode, constValue, gymCode);
        }

        public static List<USPARItem> GeneralInvoiceSearch(string fieldOne, string fieldTwo, int suggestion, int invoiceType, string fieldType, SearchModes searchMode, object constValue, string gymCode, int paymentID)
        {
            return GetDataAdapter().GeneralInvoiceSearch(fieldOne, fieldTwo, suggestion, invoiceType, fieldType, searchMode, constValue, gymCode, paymentID);
        }

        public static List<InvoicingOrder> GetOrdersforInvoice(int branchID, DateTime? startDueDate, DateTime? endDueDate, string orderType, string gymCode)
        {
            return GetDataAdapter().GetOrdersforInvoice(branchID, startDueDate, endDueDate, orderType, gymCode);
        }

        public static OperationResult<string> GenerateInvoiceswithOrders(List<InvoicingOrder> orderIds, string invoiceKey, string orderType, string user, string gymCode, int branchID)
        {
            return GetDataAdapter().GenerateInvoiceswithOrders(orderIds, invoiceKey, orderType, user, gymCode, branchID);
        }

        public static EstimatedInvoiceDetails GetEstimatedInvoiceDetails(int branchID, DateTime? startDueDate, DateTime? endDueDate, string gymCode)
        {
            return GetDataAdapter().GetEstimatedInvoiceDetails(branchID, startDueDate, endDueDate, gymCode);
        }

        public static EstimatedInvoiceDetails GetInvoicedAmounts(int branchID, DateTime? startDueDate, DateTime? endDueDate, string gymCode)
        {
            return GetDataAdapter().GetInvoicedAmounts(branchID, startDueDate, endDueDate, gymCode);
        }

        public static int AddCreditorOrderLine(CreditorOrderLine OrderLine, string user, DbTransaction transaction)
        {
            return GetDataAdapter().AddCreditorOrderLine(OrderLine, user, transaction);
        }

        public static OperationResult<List<IUSPClaim>> SelectedOrderLineInvoiceGenerator(List<int> creditorOrdeLineIdList, string invoiceKey, string orderType, string user, DbTransaction transaction, int installmentID, int creditorNo, string gymCode)
        {
            return GetDataAdapter().SelectedOrderLineInvoiceGenerator(creditorOrdeLineIdList, invoiceKey, orderType, user, transaction, installmentID, creditorNo, gymCode);
        }

        public static List<InvoiceDC> GetInvoiceList(string category, DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            try
            {
                return GetDataAdapter().GetInvoiceList(category, fromDate, toDate, branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdatePDFFileParth(int id, string fileParth, int branchId, string gymCode, string user)
        {
            try
            {
                return GetDataAdapter().UpdatePDFFileParth(id, fileParth, branchId, gymCode, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateHistoryPDFPrintProcess(int id, int invoiceCount, int FailCount, int branchId, string user, string gymCode, string filePath)
        {
            try
            {
                return GetDataAdapter().UpdateHistoryPDFPrintProcess(id, invoiceCount, FailCount, branchId, user, gymCode, filePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<HistoryPDFPrint> GetHistoryPDFPrintDetail(DateTime fromDate, DateTime toDate, string gymCode)
        {
            try
            {
                return GetDataAdapter().GetHistoryPDFPrintDetail(fromDate, toDate, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateBulkPDFPrintDetails(int id, string docType, string dataIdList, string pdfFilePath, int noOfDoc, int failCount, int branchId, string user, string gymCode)
        {
            try
            {
                return GetDataAdapter().UpdateBulkPDFPrintDetails(id, docType, dataIdList, pdfFilePath, noOfDoc, failCount, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BulkPDFPrint> GetBulkPDFPrintDetailList(string type, DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            try
            {
                return GetDataAdapter().GetBulkPDFPrintDetailList(type, fromDate, toDate, branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateCcxInvoice(List<Invoice> invoices)
        {
            try
            {
                return GetDataAdapter().UpdateCcxInvoice(invoices);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static List<Invoice> GetCcxInvoiceDetail(int batchId)
        {
            try
            {
                return GetDataAdapter().GetCcxInvoiceDetail(batchId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
