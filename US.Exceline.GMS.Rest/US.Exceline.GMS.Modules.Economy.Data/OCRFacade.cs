﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;

namespace US.Exceline.GMS.Modules.Economy.Data
{
    public class OCRFacade
    {
        private static ISQLServerEconomyDataAdapter GetDataAdapter()
        {
            return new SQLServerEconomyDataAdapter();
        }

        public static int AddFileValidationSatatesRecord(string creditorNo, int fileID, int recordCounte, decimal sum, string recordTypeString, string message1, string message2, string applicaton, string gymCode)
        {
            return GetDataAdapter().AddFileValidationSatatesRecord(creditorNo, fileID, recordCounte, sum, recordTypeString, message1, message2, applicaton, gymCode);
        }

        public static int AddFileStatus(int fileId, DateTime time, string status, string msg1, string msg2, string gymCode)
        {
            return GetDataAdapter().AddFileStatus(fileId, time, status, msg1, msg2, gymCode);
        }

        public static Creditor GetCreditorInkassoIDbyAccountNumber(string accountNumber, string checkType, string gymCode)
        {
            return GetDataAdapter().GetCreditorInkassoIDbyAccountNumber(accountNumber, checkType, gymCode);
        }

        public static SortedList GetRecordTypes(string gymCode)
        {
            return GetDataAdapter().GetRecordTypes(gymCode);
        }

        public static int GetFileIDbyFileName(string fileNameWithoutPath, string gymCode)
        {
            return GetDataAdapter().GetFileIDbyFileName(fileNameWithoutPath, gymCode);
        }

        public static Creditor GetCreditorEntityRoleIDbyAccountNumber(string creditorAccountNumber, string checkType, string gymCode)
        {
            return GetDataAdapter().GetCreditorEntityRoleIDbyAccountNumber(creditorAccountNumber, checkType, gymCode);
        }

        public static void UpdateWarningType(string kid, string credAccountNo, int status, string gymCode)
        {
            GetDataAdapter().UpdateWarningType(kid,credAccountNo, status, gymCode);
        }

        public static string UpdateDirectDeductCancellationStatus(string cancellationKID, string credAccountNo, string status, string gymCode)
        {
            return GetDataAdapter().UpdateDirectDeductCancellationStatus(cancellationKID, credAccountNo, status, gymCode);
        }

        public static void UpdateOCRAllRecords(string fileName, string kid, string status, string creditorAccountNo, string gymCode)
        {
            GetDataAdapter().UpdateOCRAllRecords(fileName, kid, status, creditorAccountNo, gymCode);
        }

        public static int SaveFileLog(string fileName, string folderPath, string user,string fileType, string gymCode)
        {
            return GetDataAdapter().SaveFileLog(fileName, folderPath, user,fileType, gymCode);
        }

        public static bool AddPaymentStatus(List<ImportStatus> payments, string gymCode)
        {
            return GetDataAdapter().AddPaymentStatus(payments, gymCode);
        }

        public static bool ImportOCRPayments(int fileID, string gymCode)
        {
            return GetDataAdapter().ImportOCRPayments(fileID,gymCode);
        }

        public static bool AddCancellationStatus(List<CancellationStatus> statusList, string gymCode)
        {
            return GetDataAdapter().AddCancellationStatus(statusList, gymCode);
        }

        public static bool ImportCancellations(string gymCode)
        {
            return GetDataAdapter().ImportCancellations(gymCode);
        }

        public static void AddProcessLog(List<string> messages, string user, int fileId, string gymCode)
        {
            GetDataAdapter().AddProcessLog(messages, user, fileId, gymCode);
        }

        public static void AddImportHistory(OCRImportSummary oCRImportSummary, string user, string gymCode)
        {
            GetDataAdapter().AddImportHistory(oCRImportSummary, user, gymCode);
        }

        public static List<OCRImportSummary> GetOCRImportHistory(DateTime? startDate, DateTime? endDate, string gymCode)
        {
            return GetDataAdapter().GetOCRImportHistory(startDate, endDate, gymCode);
        }

        public static List<PaymentImportLogDC> GetPaymentImportProcessLog(DateTime fromDate, DateTime toDate, string gymCode)
        {
            return GetDataAdapter().GetPaymentImportProcessLog(fromDate, toDate, gymCode);
        }
    }
}
