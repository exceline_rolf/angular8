﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data
{
    public class TransactionFacade
    {
        private static ISQLServerEconomyDataAdapter GetDataAdapter()
        {
            return new SQLServerEconomyDataAdapter();
        }

        public static PaymentProcessResult RegisterTransaction(List<IUSPTransaction> transactionList, DbTransaction transaction)
        {
            return GetDataAdapter().RegisterTransaction(transactionList, transaction);
        }

    }
}
