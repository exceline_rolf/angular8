﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.US.Payment.Core.BusinessDomainObjects;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class ATGManager
    {
        private const int NOTIFY_BY_BANK_NOTIFY_CREDI_CARE_STATE_VALUE = 0;
        private const int CANCELLATION_STATE_VALUE = 2;

        private static  string NOTIFY_BY_BANK_NOTIFICATION_VALUE = "1";
        private static  string NOTIFY_SELF_NOTIFICATION_VALUE = "0";
        private static  string CANCELLATION_NOTIFICATION_VALUE = "2";
        private static  string NOTIFY_CREDI_CARE_NOTIFICATION_VALUE = "0";

        private static List<string> allLines = new List<string>();
        private static string filepath = string.Empty;
        private static string filename2 = string.Empty;

        private static int numberOfrec = 0;
        private static int numberOfIte = 0;
        private static double totalAm = 0;
        private static List<DateTime> dueForEnd = new List<DateTime>();
        private static List<string> logrecodes = new List<string>();
        

        public static OperationResult<List<IUSPCreditor>> GetATGCrediors(ATGRecordType ATGType, int branchId, DateTime startDate, DateTime endDate, string gymCode, string sendingNo)
        {
            OperationResult<List<IUSPCreditor>> notificationResult = new OperationResult<List<IUSPCreditor>>();
            List<Creditor> creditorsList = new List<Creditor>();
            List<IUSPCreditor> returningCreditorsList = new List<IUSPCreditor>();
            // US_ClaimImport is not refactoring with this refactor. (2010.11.25)
            // From US_ClaimImport return Notify by Creditor and Notify by Credi Care in same method call and
            // Cancellation with another method call - THIS SCHENARIO SHOULD BE REFACTORED
            try
            {
                if (ATGType == ATGRecordType.Cancellation)
                {// Get cretors that have cancellation records
                    if (!string.IsNullOrEmpty(sendingNo))
                    {
                        ATGFacade.SetCancellationStatus(sendingNo, gymCode);
                    }

                    creditorsList = ATGFacade.GetDDCreditors(CANCELLATION_STATE_VALUE, branchId, startDate, endDate, gymCode);

                    // coply only Cancellation creditors
                    foreach (Creditor crditor in creditorsList)
                    {
                        IUSPCreditor cre = new USPCreditor();
                        cre.CreditorAccountNo = crditor.AccountNo;
                        int creditorEntID;
                        if (int.TryParse(crditor.EntID, out creditorEntID))
                        {
                            cre.CreditorEntityID = creditorEntID;
                        }
                        else
                        {
                            cre.CreditorEntityID = -1;
                        }
                        cre.CreditorName = crditor.Name;
                        //cre.Notification = dd.Notify;
                        returningCreditorsList.Add(cre);
                    }
                }
                else if (ATGType == ATGRecordType.NotifyByBank)
                {// Get cretors with Notify by Bank records and cretors with Notify by CrediCare records
                    creditorsList = ATGFacade.GetDDCreditors(NOTIFY_BY_BANK_NOTIFY_CREDI_CARE_STATE_VALUE, branchId, startDate, endDate, gymCode);
                    // above "creditorsList" contains Creditors with Notify by Bank records and cretors with Notify by CrediCare records

                    // coply only Notify by Bank creditors
                    foreach (Creditor crditor in creditorsList)
                    {
                        // Filter notify by bank records
                        if (crditor.Notify == NOTIFY_BY_BANK_NOTIFICATION_VALUE)
                        {
                            IUSPCreditor cre = new USPCreditor();
                            cre.CreditorAccountNo = crditor.AccountNo;
                            int creditorEntID;
                            if (int.TryParse(crditor.EntID, out creditorEntID))
                            {
                                cre.CreditorEntityID = creditorEntID;
                            }
                            else
                            {
                                cre.CreditorEntityID = -1;
                            }
                            cre.CreditorName = crditor.Name;
                            //cre.Notification = dd.Notify;
                            returningCreditorsList.Add(cre);
                        }
                    }
                }
                else if (ATGType == ATGRecordType.NotifyByCrediCare)
                {
                    // Get cretors with Notify by Bank records and cretors with Notify by CrediCare records
                    creditorsList = ATGFacade.GetDDCreditors(NOTIFY_BY_BANK_NOTIFY_CREDI_CARE_STATE_VALUE, branchId, startDate, endDate, gymCode);
                    // above "creditorsList" contains Creditors with Notify by Bank records and cretors with Notify by CrediCare records\

                    // coply only Notify by CrediCare creditors
                    foreach (Creditor crditor in creditorsList)
                    {
                        // Filter notify by CrediCare records
                        if (crditor.Notify == NOTIFY_CREDI_CARE_NOTIFICATION_VALUE)
                        {
                            IUSPCreditor cre = new USPCreditor();
                            cre.CreditorAccountNo = crditor.AccountNo;
                            int creditorEntID;
                            if (int.TryParse(crditor.EntID, out creditorEntID))
                            {
                                cre.CreditorEntityID = creditorEntID;
                            }
                            else
                            {
                                cre.CreditorEntityID = -1;
                            }
                            cre.CreditorName = crditor.Name;
                            //cre.Notification = dd.Notify;
                            returningCreditorsList.Add(cre);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                notificationResult.ErrorOccured = true;
                notificationResult.CreateMessage("Failed to retrieve creditor list needed by ATG : " + ex.Message, MessageTypes.ERROR);
            }

            notificationResult.OperationReturnValue = returningCreditorsList;
            return notificationResult;
        }

        public static IFileLog ProcessFile(DateTime startDate, DateTime endDate, string notify, List<IUSPCreditor> creditors, int status, string location, string gymCode, string sendingNo)
        {
            IFileLog fileInfo = new USPFileLog();
            List<string> logrecodes = new List<string>();
            string fileName = string.Empty;
            allLines.Clear();
            filepath = string.Empty;
            totalAm = 0;
            numberOfIte = 0;
            numberOfrec = 0;
            dueForEnd.Clear();
            if (notify.Equals(NOTIFY_BY_BANK_NOTIFICATION_VALUE))
            {
                logrecodes.Add("start notify by bank file creation");
            }
            if (notify.Equals(NOTIFY_SELF_NOTIFICATION_VALUE))
            {
                logrecodes.Add("start notify by credicare file creation");
            }

            if (notify.Equals(CANCELLATION_NOTIFICATION_VALUE))
            {
                logrecodes.Add("start cancellation file creation");
            }

            if (creditors.Count != 0)
            {
               
                OperationResult<IUSPApplicationSetting> appSetting = GetSettingForATG(gymCode);
                if (appSetting.ErrorOccured)
                {
                    logrecodes.Add("ERROR :fail to get application setting ID from service " + appSetting.Notifications[0].Message);
                    fileInfo.NotificationMessages = logrecodes;
                    return fileInfo;
                }
                string unitID = appSetting.OperationReturnValue.TrancemeterID.PadLeft(8, '0');
                string transactionID = string.Empty;
                try
                {
                    OperationResult<string> transIDRe = GetATGTransactionID(gymCode);
                    if (transIDRe.ErrorOccured)
                    {
                        logrecodes.Add("ERROR :fail to get transaction ID from service ");
                        fileInfo.NotificationMessages = logrecodes;
                        return fileInfo;

                    }
                    transactionID = transIDRe.OperationReturnValue;
                    fileName = GetAGTFileName(transactionID);
                }
                catch (Exception)
                {
                    logrecodes.Add("ERROR :fail to get transaction ID from service ");
                    fileInfo.NotificationMessages = logrecodes;
                    return fileInfo;
                }

                filepath = location + "\\" + fileName + ".txt";
                fileInfo.FilePath = filepath;
                filename2 = fileName;

                string line1 = "NY000010" + unitID + transactionID + "00008080" + "0000000000000000000000000000000000000000000000000\n";
                allLines.Add(line1);

                foreach (IUSPCreditor creditor in creditors)
                {
                    try
                    {
                        logrecodes.Add("reading DD items for creditor " + creditor.CreditorName);

                        OperationResult<IUSPDirectDeductInfo> DDInfoResult = GetATGRecords(startDate, endDate, creditor.CreditorEntityID.ToString(), (ATGRecordType)status, gymCode, sendingNo);
                        if (DDInfoResult.ErrorOccured)
                        {
                            continue;
                        }
                        IUSPDirectDeductInfo DDInfo = DDInfoResult.OperationReturnValue;
                        List<IUSPStandingOrderItem> singlefile = DDInfo.Ddobj;
                        string total = DDInfo.TotalAmount;

                        if (total.Equals("0") || total == null || singlefile.Count == 0)
                        {
                            logrecodes.Add("no valid recode found for this creditor");
                            continue;
                        }
                        else
                        {
                            List<IUSPDDHistory> stUpdate = WriteStandingOrder(singlefile, creditor.CreditorName, DDInfo.FirstDueDate, DDInfo.LastDueDate, DDInfo.TotalAmount, DDInfo.FileName, creditor.CreditorAccountNo, notify, status);
                            OperationResult HistoryResult = UpdateATGSucceededStatus(stUpdate, gymCode);
                            if (HistoryResult.ErrorOccured)
                            {
                                logrecodes.Add("Error occured while Updating the DDHistory " + HistoryResult.Notifications[0].Message);

                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        logrecodes.Add("Standin order Request ERROR " + ex.Message);
                        continue;
                    }
                }

            }

            else
            {
                logrecodes.Add("no items found");

            }
            try
            {
                if (dueForEnd.Count > 0)
                {
                    dueForEnd.Sort();

                    string linelast2 = "NY000089" + numberOfrec.ToString().PadLeft(8, '0') + (numberOfIte + 2).ToString().PadLeft(8, '0') + totalAm.ToString().PadLeft(17, '0') + String.Format("{0:ddMMyy}", dueForEnd[0]) + "000000000000000000000000000000000\n";

                    allLines.Add(linelast2);

                    if (allLines.Count > 2)
                    {
                        System.Text.Encoding targetEncoding = System.Text.Encoding.GetEncoding(1252);
                        if (File.Exists(filepath))
                        {
                            File.Delete(filepath);
                        }
                        foreach (string line in allLines)
                        {
                            File.AppendAllText(filepath, line, targetEncoding);
                        }

                        logrecodes.Add("successfully create the file");
                        // Record file log information to log db.
                        try
                        {
                            fileInfo.FileName = fileName + ".txt";
                        }
                        catch (Exception ex)
                        {
                            logrecodes.Add("Fail to write file loging details . " + ex.Message);
                        }

                    }
                    else
                    {
                        logrecodes.Add("successfully create the file");
                    }

                }

            }
            catch (Exception ex)
            {

                logrecodes.Add("Failed to create file " + ex.Message);
            }
            fileInfo.NotificationMessages = logrecodes;
            return fileInfo;
        }

        public static OperationResult<IUSPApplicationSetting> GetSettingForATG(string gymCode)
        {
            OperationResult<IUSPApplicationSetting> notificationResult = new OperationResult<IUSPApplicationSetting>();
            try
            {
                ApplicationSetting appObj = new ApplicationSetting();
                IUSPApplicationSetting appSetting = new USPApplicationSetting();

                appObj = ATGFacade.GetApplicationSetting(gymCode);
                appSetting.Name = appObj.companyName;
                appSetting.TrancemeterID = appObj.TranceMeterID;
                notificationResult.OperationReturnValue = appSetting;
            }
            catch (Exception ex)
            {
                notificationResult.ErrorOccured = true;
                notificationResult.CreateMessage("Failed to retrieve setting needed by ATG : " + ex.Message, MessageTypes.ERROR);
            }
            return notificationResult;
        }

        public static OperationResult<string> GetATGTransactionID(string gymCode)
        {
            OperationResult<string> notificationResult = new OperationResult<string>();
            try
            {
                string transactionID = ATGFacade.GetATGTransactionID(gymCode);
                notificationResult.OperationReturnValue = transactionID;
            }
            catch (Exception ex)
            {
                notificationResult.ErrorOccured = true;
                notificationResult.CreateMessage("Failed to retrieve tranaction ID needed by ATG : " + ex.Message, MessageTypes.ERROR);
            }
            return notificationResult;
        }

        private static string GetAGTFileName(string transactionNo)
        {

            string fileSequenceNo = transactionNo.Substring(6, 1);
            string day = Convert.ToString(DateTime.Now.Day);
            if (day.Length == 1)
            {
                day = string.Concat("0", day);
            }
            string month = Convert.ToString(DateTime.Now.Month);
            if (month.Length == 1)
            {
                month = string.Concat("0", month);
            }
            string year = Convert.ToString(DateTime.Now.Year).Substring(2, 2);

            string fileName = day + month + year + fileSequenceNo;
            return fileName;


        }

        public static OperationResult<IUSPDirectDeductInfo> GetATGRecords(DateTime startDate, DateTime endDate, string creditorEntityID, ATGRecordType ATGType, string gymCode, string sendingNo)
        {
            OperationResult<IUSPDirectDeductInfo> notificationResult = new OperationResult<IUSPDirectDeductInfo>();
            try
            {
                IUSPDirectDeductInfo ddinfo = new USPDirectDeductInfo();
                List<IUSPStandingOrderItem> ddConList = new List<IUSPStandingOrderItem>();
                List<DirectDeduct> ddList = new List<DirectDeduct>();
                DirectDeductInfo ddInfoEnt = new DirectDeductInfo();
                ddInfoEnt = ATGFacade.GetDirectDeduct(startDate, endDate, creditorEntityID, (int)ATGType, gymCode, sendingNo);
                ddList = ddInfoEnt.ddObj;

                if (ddList != null)
                {
                    foreach (DirectDeduct dd in ddList)
                    {
                        IUSPStandingOrderItem ddCon = new USPStandingOrderItem();


                        ddCon.KID = dd.InstallmentKID;

                        ddCon.Amount = dd.amount;
                        ddCon.DueDate = dd.dueDate;
                        ddCon.CreName = dd.debtorName;
                        ddCon.aRitem = dd.AritemNo;
                        ddCon.ContractNo = dd.ContractNo;
                        ddCon.Message = dd.message;
                        ddConList.Add(ddCon);
                    }
                }

                ddinfo.Ddobj = ddConList;
                ddinfo.FirstDueDate = ddInfoEnt.firstDue;
                ddinfo.LastDueDate = ddInfoEnt.LastDue;
                ddinfo.TotalAmount = ddInfoEnt.totalAmount;
                ddinfo.FileName = ddInfoEnt.seq;

                notificationResult.OperationReturnValue = ddinfo;
            }
            catch (Exception ex)
            {
                notificationResult.ErrorOccured = true;
                notificationResult.CreateMessage("Failed to retrieve ATG record list : " + ex.Message, MessageTypes.ERROR);                
            }
            return notificationResult;
        }

        public static List<IUSPDDHistory> WriteStandingOrder(List<IUSPStandingOrderItem> DDInfo, string creditorName, string firstDueDate, string lastDuedate, string totalAmount, string filename, string account, string notify, int st)
        {
            try
            {
                string notiText;
                string Otype;

                if (st == NOTIFY_BY_BANK_NOTIFY_CREDI_CARE_STATE_VALUE)
                {
                    if (notify.Equals(NOTIFY_SELF_NOTIFICATION_VALUE))
                    {
                        notiText = "02";
                    }
                    else
                    {
                        notiText = "21";
                    }
                    Otype = "00";
                }

                else
                {
                    notiText = "93";
                    Otype = "36";
                }


                DateTime dt = DateTime.Now;
                string ss = String.Format("{0:ddMM}", dt);
                //string unitID = "001".PadLeft(8, '0');
                string filen = ss + filename.PadLeft(3, '0');


                string line2 = "NY21" + Otype + "20000000000" + filen + account.PadLeft(11, '0') + "000000000000000000000000000000000000000000000\n";

                allLines.Add(line2);

                totalAmount = "0";
                double total = 0;

                int order = 1;
                int numOfspec = 0;
                List<DateTime> datelist = new List<DateTime>();
                List<IUSPDDHistory> status = new List<IUSPDDHistory>();
                foreach (IUSPStandingOrderItem item in DDInfo)
                {
                    try
                    {
                        DateTime dueDate = DateTime.ParseExact(item.DueDate, "ddMMyy", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
                        //int amount = Convert.ToInt32(item.Amount);

                        if (!item.KID.Equals(""))
                        {
                            total = total + Convert.ToDouble(item.Amount);

                            string message = item.Message;
                            string orderStr = order.ToString().PadLeft(7, '0');
                            string amountStr = item.Amount.PadLeft(17, '0');
                            //string date1 = String.Format("{0:ddMMyy}", dt);
                            string KIDStr = item.KID.PadLeft(25, ' ');
                            datelist.Add(dueDate);
                            string debtorName = Left(item.CreName.PadRight(10, ' '), 10);
                            string ext = "Treningsavtale" + item.ContractNo;
                            string externalref = ext.PadLeft(25, ' ');
                            string lineA = "NY21" + notiText + "30" + orderStr + item.DueDate + "           " + amountStr + KIDStr + "000000\n";
                            allLines.Add(lineA);
                            string lineB = "NY21" + notiText + "31" + orderStr + debtorName + "                         " + externalref + "00000\n";
                            allLines.Add(lineB);


                            if (!(message == null || message == ""))
                            {
                                List<string> spec = ProcessSpecification(message, notiText, orderStr);
                                numOfspec = numOfspec + spec.Count;
                                foreach (string s in spec)
                                {
                                    allLines.Add(s);
                                }
                            }


                            IUSPDDHistory ddhosto = new USPDDHistory();
                            ddhosto.ArNo = Int32.Parse(item.aRitem);
                            ddhosto.Cancellatrion = st.ToString();
                            ddhosto.FileName = filename2;
                            ddhosto.OrderId = filen;
                            ddhosto.TransactionID = orderStr;
                            status.Add(ddhosto);

                            order = order + 1;
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                if (order > 1)
                {
                    string numberofRecodes = (order - 1).ToString().PadLeft(8, '0');
                    string numberOfItems = ((order - 1) * 2 + numOfspec + 2).ToString().PadLeft(8, '0');
                    //string numberOfItems2 = ((order - 1) * 2 + numOfspec + 4).ToString().PadLeft(8, '0');
                    numberOfrec = numberOfrec + (order - 1);
                    numberOfIte = numberOfIte + ((order - 1) * 2 + numOfspec + 2);
                    if (totalAmount == null) totalAmount = "0";
                    if (firstDueDate == null) firstDueDate = string.Empty;
                    if (lastDuedate == null) lastDuedate = string.Empty;
                    string totalAmount1 = total.ToString().PadLeft(17, '0');
                    //string firstDueDate1 = firstDueDate.PadLeft(6, '0');
                    //string lastDueDate1 = lastDuedate.PadLeft(6, '0');
                    totalAm = totalAm + total;

                    datelist.Sort();



                    string linelast1 = "NY21" + Otype + "88" + numberofRecodes + numberOfItems + totalAmount1 + String.Format("{0:ddMMyy}", datelist[0]) + String.Format("{0:ddMMyy}", datelist[datelist.Count - 1]) + "000000000000000000000000000\n";
                    allLines.Add(linelast1);


                    //firtDue = firstDueDate1;
                    dueForEnd.Add(datelist[0]);
                }
                if (order == 1)
                {
                    allLines.RemoveAt(allLines.Count - 1);
                    logrecodes.Add("no valid items found for this creditor");
                }

                return status;

            }
            catch
            {
                throw;
            }
        }

        public static OperationResult UpdateATGSucceededStatus(List<IUSPDDHistory> directDeductHistories, string gymCode)
        {
            OperationResult notificationResult = new OperationResult();
            try
            {
                List<DDHistory> historyEnt = new List<DDHistory>();
                foreach (IUSPDDHistory ddh in directDeductHistories)
                {
                    DDHistory history = new DDHistory();
                    history.ArNo = ddh.ArNo;
                    history.FileName = ddh.FileName;
                    history.Cancellatrion = ddh.Cancellatrion;
                    history.OrderId = ddh.OrderId;
                    history.TransactionID = ddh.TransactionID;
                    historyEnt.Add(history);
                }

                bool result = ATGFacade.UpdateStatus(historyEnt, gymCode);
                if (!result)
                {
                    notificationResult.ErrorOccured = true;
                    notificationResult.CreateMessage("Failed to update ATG writen succed status.", MessageTypes.ERROR);
                }
            }
            catch (Exception ex)
            {
                notificationResult.ErrorOccured = true;
                notificationResult.CreateMessage("Failed to update ATG writen succed status : " + ex.Message, MessageTypes.ERROR);         
            }
            return notificationResult;
        }

        public static String Right(String strParam, int iLen)
        {
            if (iLen > 0)
                return strParam.Substring(strParam.Length - iLen, iLen);
            else
                return strParam;
        }

        public static String Left(String strParam, int iLen)
        {
            if (iLen > 0)
                return strParam.Substring(0, iLen);
            else
                return strParam;
        }


        private static List<string> ProcessSpecification(string message, string notitxt, string orderStr)
        {
            List<string> specfic = new List<string>();

            //int length = message.Length;
            List<string> result = new List<string>(Regex.Split(message, @"(?<=\G.{40})"));

            int line = 1;
            int col = 1;
            foreach (string txt in result)
            {

                string line1 = "NY21" + notitxt + "49" + orderStr + "4" + line.ToString().PadLeft(3, '0') + col.ToString() + txt.PadRight(40, ' ') + "00000000000000000000\n";
                if (col == 1)
                {
                    col = 2;
                }
                else
                {
                    col = 1;
                    line = line + 1;
                }
                specfic.Add(line1);
            }


            return specfic;

        }


        public static ATGSummary GetATGSummary(DateTime startDate, DateTime endDate, string notify, List<IUSPCreditor> creditors, int status, string folderPath, string gymCode, string sendingNo)
        {
            ATGSummary atgSummary = new ATGSummary();
            atgSummary.Count = 0;
            atgSummary.SumAmount = 0;
            List<string> logrecodes = new List<string>();
            string fileName = string.Empty;
            allLines.Clear();
            filepath = string.Empty;
            totalAm = 0;
            numberOfIte = 0;
            numberOfrec = 0;
            dueForEnd.Clear();
           
            if (creditors.Count != 0)
            {
                foreach (IUSPCreditor creditor in creditors)
                {
                    try
                    {
                        logrecodes.Add("reading DD items for creditor " + creditor.CreditorName);
                        OperationResult<IUSPDirectDeductInfo> DDInfoResult = GetATGRecords(startDate, endDate, creditor.CreditorEntityID.ToString(), (ATGRecordType)status, gymCode, sendingNo);
                        if (DDInfoResult.ErrorOccured)
                        {
                            continue;
                        }
                        IUSPDirectDeductInfo DDInfo = DDInfoResult.OperationReturnValue;
                        List<IUSPStandingOrderItem> singlefile = DDInfo.Ddobj;
                        string total = DDInfo.TotalAmount;

                        if (total.Equals("0") || total == null || singlefile.Count == 0)
                        {
                            logrecodes.Add("no valid recode found for this creditor");
                            continue;
                        }
                        else
                        {
                            atgSummary.SumAmount += Convert.ToDecimal(total)/100;
                            atgSummary.Count += singlefile.Count;
                        }
                    }
                    catch (Exception ex)
                    {
                        logrecodes.Add("Standin order Request ERROR " + ex.Message);
                        continue;
                    }
                }
            }
            else
            {
                logrecodes.Add("no Creditors items found");
            }
            return atgSummary;
        }
    }
}
