﻿using System;
using System.Collections.Generic;
using System.IO;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Data;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using US.GMS.Core.SystemObjects.Enums;


namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class ExcelineSponsorInvoiceGenerator
    {
        static public String XMLData = String.Empty;
        static private byte[] xmlHash = null;
        static public String output;
        static public String documentType;
        static public String VoucherId;
        static public String docId;
        static public Stream XMLStream = new MemoryStream();
        static public Byte[] result;

        static public String Getoutput()
        {
            return output;
        }

        static public void Setoutput(String input)
        {
            output = input;
        }

        public Byte[] GetResult()
        {
            return result;
        }

        static public void SetResult(Byte[] input)
        {
            result = input;
        }

        public ExcelineSponsorInvoiceGenerator(int sponsorARItemNo, int branchid, String gymcode, Boolean anonymizeNames = false)
        {

            //Retrieve the Data:

            ExcelineSponsorInfoForPrint sponsorInfo = EconomyFacade.ExcelineSponsorInfoForPrint(sponsorARItemNo.ToString(), gymcode);
            List<ExcelineSponsorMembersForPrint> sponsoredMembers = EconomyFacade.ExcelineGetSponsoredMembersInvoicePrint(sponsorARItemNo.ToString(), gymcode);
            var ReplacementDict = new Dictionary<String, String> { };
            //Begin populating the dictionary

            ReplacementDict.Add("{InvoiceNo}", sponsorInfo.InvoiceNo);
            ReplacementDict.Add("{ViewVisit}", sponsorInfo.ViewVisit);
            ReplacementDict.Add("{GymName}", sponsorInfo.GymName);
            ReplacementDict.Add("{GymAddress1}", sponsorInfo.GymAddress1);
            ReplacementDict.Add("{GymAddress2}", sponsorInfo.GymAddress2);
            ReplacementDict.Add("{GymPostalAddress}", sponsorInfo.GymPostalAddress);
            ReplacementDict.Add("{GymOrganizationNo}", sponsorInfo.GymOrganizationNo);
            ReplacementDict.Add("{GymAccountNo}", sponsorInfo.GymAccountNo);
            ReplacementDict.Add("{FirstName}", sponsorInfo.FirstName);
            ReplacementDict.Add("{LastName}", sponsorInfo.LastName);
            ReplacementDict.Add("{Address}", sponsorInfo.Address);
            ReplacementDict.Add("{SponsorAddress2}", sponsorInfo.SponsorAddress2);
            ReplacementDict.Add("{PostalAddress}", sponsorInfo.PostalAddress);
            ReplacementDict.Add("{SponsorCountry}", sponsorInfo.SponsorCountry);
            ReplacementDict.Add("{DueDate}", sponsorInfo.DueDate);
            ReplacementDict.Add("{InvoiceGeneratedDate}", sponsorInfo.InvoiceGeneratedDate);
            ReplacementDict.Add("{Reference}", sponsorInfo.Reference);
            ReplacementDict.Add("{InvoiceAmount}", sponsorInfo.InvoiceAmount.ToString());
            ReplacementDict.Add("{DiscountTotal}", sponsorInfo.DiscountTotal.ToString());
            ReplacementDict.Add("{ContactPerson}", sponsorInfo.ContactPerson);
            ReplacementDict.Add("{DicountCount}", sponsorInfo.DicountCount.ToString());
            ReplacementDict.Add("{SponsorCount}", sponsorInfo.SponsorCount.ToString());
            ReplacementDict.Add("{OriginalCustomer}", sponsorInfo.OriginalCustomer.Substring(0, sponsorInfo.OriginalCustomer.IndexOf(' ')));
            ReplacementDict.Add("{OrderText}", sponsorInfo.OrderText);
            ReplacementDict.Add("{IsDisplayVisitNo}", sponsorInfo.IsDisplayVisitNo.ToString());
            ReplacementDict.Add("{IsDisplayRemain}", sponsorInfo.IsDisplayRemain.ToString());
            ReplacementDict.Add("{ID}", sponsorInfo.ID);
            ReplacementDict.Add("{GymCompanyId}", sponsorInfo.GymCompanyId.ToString());
            ReplacementDict.Add("{BranchID}", sponsorInfo.BranchID);
            ReplacementDict.Add("{DocType}", sponsorInfo.DocType);
            ReplacementDict.Add("{InvoiceID}", sponsorInfo.InvoiceID.ToString());


            //Add iterator here to add all OrderLines
            for (int i = 0; i < sponsoredMembers.Count; i++)
            {
                ReplacementDict.Add("{OrderLineNo" + i.ToString() + "}", sponsoredMembers[i].OrderLineNo.ToString());
                ReplacementDict.Add("{ARItemNo" + i.ToString() + "}", sponsoredMembers[i].ARItemNo.ToString());
                ReplacementDict.Add("{CreditorInkassoID" + i.ToString() + "}", sponsoredMembers[i].CreditorInkassoID);
                ReplacementDict.Add("{CustId" + i.ToString() + "}", sponsoredMembers[i].CustId);
                ReplacementDict.Add("{LastName" + i.ToString() + "}", (anonymizeNames == true ? "" : sponsoredMembers[i].LastName));
                ReplacementDict.Add("{FirstName" + i.ToString() + "}", (anonymizeNames == true ? "" :  sponsoredMembers[i].FirstName));
                ReplacementDict.Add("{NameOnContract" + i.ToString() + "}", sponsoredMembers[i].NameOnContract);
                ReplacementDict.Add("{Address" + i.ToString() + "}", sponsoredMembers[i].Address);
                ReplacementDict.Add("{ZipCode" + i.ToString() + "}", sponsoredMembers[i].ZipCode);
                ReplacementDict.Add("{PostPlace" + i.ToString() + "}", sponsoredMembers[i].PostPlace);
                ReplacementDict.Add("{DiscountAmount" + i.ToString() + "}", sponsoredMembers[i].DiscountAmount.ToString());
                ReplacementDict.Add("{EmployeeNo" + i.ToString() + "}", sponsoredMembers[i].EmployeeNo);
                ReplacementDict.Add("{SponsorRef" + i.ToString() + "}", sponsoredMembers[i].SponsorRef);
                ReplacementDict.Add("{Visits" + i.ToString() + "}", sponsoredMembers[i].Visits.ToString());
                ReplacementDict.Add("{AmountOrderline" + i.ToString() + "}", sponsoredMembers[i].AmountOrderline.ToString());
                ReplacementDict.Add("{LastVisit" + i.ToString() + "}", sponsoredMembers[i].LastVisit.ToString());
                ReplacementDict.Add("{Amount" + i.ToString() + "}", sponsoredMembers[i].Amount.ToString());
                ReplacementDict.Add("{TrainingPeriodStart" + i.ToString() + "}", sponsoredMembers[i].TrainingPeriodStart.ToString().Replace("00:00:00", ""));
                ReplacementDict.Add("{TrainingPeriodEnd" + i.ToString() + "}", sponsoredMembers[i].TrainingPeriodEnd.ToString().Replace("00:00:00", ""));
            }

            String template = new PdfTemplates(PdfTemplatesTypes.SPONSOR_INVOICE_EXCELINE, sponsoredMembers.Count, false, 0).getHTMLTemplate();

            String replacementString = "";

            if (sponsorInfo.ViewVisit == "1")
            {

                replacementString += @"<table style = ""border-spacing: 5"">";
                replacementString += "<tr>";
                replacementString += @"<th width= ""12%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"" align = ""left"">KUNDE NR.</th>";
                replacementString += @"<th width= ""27%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">NAVN</th>";
                replacementString += @"<th width= ""24%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">PERIODE</th>";
                replacementString += @"<th width= ""9%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">BESØK</th>";
                replacementString += @"<th width= ""9%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">RABATT</th>";
                replacementString += @"<th width= ""19%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">SPONSET BELØP</th>";
                replacementString += "</tr>";

                

                for (int i = 0; i < sponsoredMembers.Count; i++)
                {
                    replacementString += "<tr>";
                    replacementString += @"<td width= ""12%"" style= ""padding-left: 0cm"">{CustId" + i.ToString() + "}</td>";
                    replacementString += @"<td width= ""27%"" >{FirstName" + i.ToString() + "} {LastName" + i.ToString() + "}</td>";
                    replacementString += @"<td width= ""24%"" style = ""text-align: center"">{TrainingPeriodStart" + i.ToString() + "} {TrainingPeriodEnd" + i.ToString() + "}</td>";
                    replacementString += @"<td width= ""9%"" style = ""text-align: center"">{Visits" + i.ToString() + "}</td>";
                    replacementString += @"<td width= ""9%"" style = ""text-align: center"">{DiscountAmount" + i.ToString() + "}</td>";
                    replacementString += @"<td width= ""19%"" style = ""text-align: center"">{Amount" + i.ToString() + "}</td>";
                    replacementString += "</tr>";
                    replacementString += "{Note" + i.ToString() + "}";
                }
                replacementString += "</table>";
            }
            else
            {
                replacementString += @"<table style = ""border-spacing: 5"">";
                replacementString += "<tr>";
                replacementString += @"<th width= ""10%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"" align = ""left"">KUNDE NR.</th>";
                replacementString += @"<th width= ""30%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">NAVN</th>";
                replacementString += @"<th width= ""27%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">PERIODE</th>";
                replacementString += @"<th width= ""10%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">RABATT</th>";
                replacementString += @"<th width= ""23%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">SPONSET BELØP</th>";
                replacementString += "</tr>"
                    ;
                for (int i = 0; i < sponsoredMembers.Count; i++)
                {
                    replacementString += "<tr>";
                    replacementString += @"<td width= ""10%"" style= ""padding-left: 0cm"">{CustId" + i.ToString() + "}</td>";
                    replacementString += @"<td width= ""30%"" >{FirstName" + i.ToString() + "} {LastName" + i.ToString() + "}</td>";
                    replacementString += @"<td width= ""27%"" style = ""text-align: center"">{TrainingPeriodStart" + i.ToString() + "} {TrainingPeriodEnd" + i.ToString() + "}</td>";
                    replacementString += @"<td width= ""10%"" style = ""text-align: center"">{DiscountAmount" + i.ToString() + "}</td>";
                    replacementString += @"<td width= ""23%"" style = ""text-align: center"">{Amount" + i.ToString() + "}</td>";
                    replacementString += "</tr>";
                    replacementString += "{Note" + i.ToString() + "}";
                }
                replacementString += "</table>";
            }

            template = template.Replace("{ReplaceMembers}", replacementString);

            foreach (var key in ReplacementDict.Keys)
            {
                template = template.Replace(key, ReplacementDict[key]);
            }

            Setoutput(template);

            using (MemoryStream ms = new MemoryStream())
            {

                var pdf = PdfGenerator.GeneratePdf(Getoutput(), PdfSharp.PageSize.A4);
                pdf.Save(ms);
                SetResult(ms.ToArray());
            }

        }
    }
}

