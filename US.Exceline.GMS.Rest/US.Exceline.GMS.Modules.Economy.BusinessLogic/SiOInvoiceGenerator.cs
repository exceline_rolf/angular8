﻿using System;
using System.Text;
using System.Xml;
using System.Net;
using System.IO;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using US.GMS.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class SiOInvoiceGenerator
    {
        static public String output;
        static public String documentType;
        static public String VoucherId;
        static public String docId;
        static public Stream XMLStream = new MemoryStream();
        static public Byte[] result;

        static public String Getoutput()
        {
            return output;
        }

        static public void Setoutput(String input)
        {
            output = input;
        }

        public Byte[] GetResult()
        {
            return result;
        }

        static public void SetResult(Byte[] input)
        {
            result = input;
        }

         public void Execute(String invoiceNumber)

        {

            HttpWebRequest request = CreateWebRequest();

            XmlDocument soapEnvelopeXml = new XmlDocument();
            String xmlInput1 = @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:quer=""http://services.agresso.com/QueryEngineService/QueryEngineV200606DotNet"">";
            xmlInput1 += "<soapenv:Header/>";
            xmlInput1 += "<soapenv:Body>";
            xmlInput1 += "<quer:GetTemplateResultAsXML>";
            xmlInput1 += "<quer:input>";
            xmlInput1 += "<quer:TemplateId>3416</quer:TemplateId>";
            xmlInput1 += "<quer:TemplateResultOptions>";
            xmlInput1 += "<quer:ShowDescriptions>true</quer:ShowDescriptions>";
            xmlInput1 += "<quer:Aggregated>true</quer:Aggregated>";
            xmlInput1 += "<quer:OverrideAggregation>false</quer:OverrideAggregation>";
            xmlInput1 += "<quer:CalculateFormulas>true</quer:CalculateFormulas>";
            xmlInput1 += "<quer:FormatAlternativeBreakColumns>true</quer:FormatAlternativeBreakColumns >";
            xmlInput1 += "<quer:RemoveHiddenColumns>false</quer:RemoveHiddenColumns>";
            xmlInput1 += "<quer:Filter>-1</quer:Filter>";
            xmlInput1 += "<quer:FirstRecord>-1</quer:FirstRecord>";
            xmlInput1 += "<quer:LastRecord>-1</quer:LastRecord>";
            xmlInput1 += "</quer:TemplateResultOptions>";
            xmlInput1 += "<quer:SearchCriteriaPropertiesList>";
            xmlInput1 += "<quer:SearchCriteriaProperties>";
            xmlInput1 += "<quer:ColumnName>order_id</quer:ColumnName>";
            xmlInput1 += "<quer:Description>Ordrenr</quer:Description>";
            xmlInput1 += "<quer:RestrictionType>=</quer:RestrictionType>";
            xmlInput1 += "<quer:FromValue>" + invoiceNumber + "</quer:FromValue>";
            xmlInput1 += "<quer:ToValue>" + invoiceNumber + "</quer:ToValue>";
            xmlInput1 += "<quer:DataType>21</quer:DataType>";
            xmlInput1 += "<quer:DataLength>18</quer:DataLength>";
            xmlInput1 += "<quer:DataCase>0</quer:DataCase>";
            xmlInput1 += "<quer:IsParameter>true</quer:IsParameter>";
            xmlInput1 += "<quer:IsVisible>true</quer:IsVisible>";
            xmlInput1 += "<quer:IsPrompt>true</quer:IsPrompt>";
            xmlInput1 += "</quer:SearchCriteriaProperties>";
            xmlInput1 += "</quer:SearchCriteriaPropertiesList>";
            xmlInput1 += "<quer:PipelineAssociatedName>?</quer:PipelineAssociatedName>";
            xmlInput1 += "</quer:input>";
            xmlInput1 += "<quer:credentials>";
            xmlInput1 += "<quer:Username>SYSMIN</quer:Username>";
            xmlInput1 += "<quer:Client>70</quer:Client>";
            xmlInput1 += "<quer:Password>SYSMIN</quer:Password>";
            xmlInput1 += "</quer:credentials>";
            xmlInput1 += "</quer:GetTemplateResultAsXML>";
            xmlInput1 += "</soapenv:Body>";
            xmlInput1 += "</soapenv:Envelope>";

            soapEnvelopeXml.LoadXml(xmlInput1);

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    string soapResult = rd.ReadToEnd();
                    int StartPosVoucher = soapResult.ToString().IndexOf("&lt;voucher_no&gt;") + 18;
                    int EndPosVoucher = soapResult.ToString().IndexOf("&lt;/voucher_no&gt;");

                    VoucherId = soapResult.ToString().Substring(StartPosVoucher, EndPosVoucher - StartPosVoucher);
                }
            }


            HttpWebRequest request2 = CreateWebRequest2();

            XmlDocument soapEnvelopeXml2 = new XmlDocument();
            String xmlInput2 = @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:doc=""http://services.agresso.com/DocArchiveService/DocArchiveV201101"">";
            xmlInput2 += "<soapenv:Header/>";
            xmlInput2 += "<soapenv:Body>";
            xmlInput2 += "<doc:GetDocumentProperties>";
            xmlInput2 += "<doc:request>";
            xmlInput2 += "<doc:DocType>FAKTURA</doc:DocType>";
            xmlInput2 += "<doc:IndexValues>";
            xmlInput2 += "<doc:string>70</doc:string>";
            xmlInput2 += "<doc:string>"+ 5991403 + "</doc:string>";
            xmlInput2 += "</doc:IndexValues>";
            xmlInput2 += "</doc:request>";
            xmlInput2 += "<doc:credentials>";
            xmlInput2 += "<doc:Username>SYSMIN</doc:Username>";
            xmlInput2 += "<doc:Client>70</doc:Client>";
            xmlInput2 += "<doc:Password>SYSMIN</doc:Password>";
            xmlInput2 += "</doc:credentials>";
            xmlInput2 += "</doc:GetDocumentProperties>";
            xmlInput2 += "</soapenv:Body>";
            xmlInput2 += "</soapenv:Envelope>";

            soapEnvelopeXml2.LoadXml(xmlInput2);

            using (Stream stream2 = request2.GetRequestStream())
            {
                soapEnvelopeXml2.Save(stream2);
            }

            using (WebResponse response2 = request2.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response2.GetResponseStream()))
                {
                    string soapResult2 = rd.ReadToEnd();
                    int StartPos = soapResult2.ToString().IndexOf("<DocId>") + 7;
                    int EndPos = soapResult2.ToString().IndexOf("</DocId>");
                    int StartPosDocumentType = soapResult2.ToString().IndexOf("<MimeType>") + 10;
                    int EndPosDocumentType = soapResult2.ToString().IndexOf("</MimeType>");
                    documentType = soapResult2.ToString().Substring(StartPosDocumentType, EndPosDocumentType - StartPosDocumentType);
                    docId = soapResult2.ToString().Substring(StartPos, EndPos - StartPos);
                }
            }

            HttpWebRequest request3 = CreateWebRequest3();
            XmlDocument soapEnvelopeXml3 = new XmlDocument();
            String xmlInput3 = @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:doc=""http://services.agresso.com/DocArchiveService/DocArchiveV201101"">";
            xmlInput3 += "<soapenv:Header/>";
            xmlInput3 += "<soapenv:Body>";
            xmlInput3 += "<doc:GetDocumentRevision>";
            xmlInput3 += "<doc:request>";
            xmlInput3 += "<doc:DocId>" + @docId + "</doc:DocId>";
            xmlInput3 += "<doc:DocType>FAKTURA</doc:DocType>";
            xmlInput3 += "<doc:RevisionNo>0</doc:RevisionNo>";
            xmlInput3 += "<doc:PageNo>1</doc:PageNo>";
            xmlInput3 += "</doc:request>";
            xmlInput3 += "<doc:credentials>";
            xmlInput3 += "<doc:Username>SYSMIN</doc:Username>";
            xmlInput3 += "<doc:Client>70</doc:Client>";
            xmlInput3 += "<doc:Password>SYSMIN</doc:Password>";
            xmlInput3 += "</doc:credentials>";
            xmlInput3 += "</doc:GetDocumentRevision>";
            xmlInput3 += "</soapenv:Body>";
            xmlInput3 += "</soapenv:Envelope>";

            soapEnvelopeXml3.LoadXml(xmlInput3);

            using (Stream stream3 = request3.GetRequestStream())
            {
                soapEnvelopeXml3.Save(stream3);
            }




            using (WebResponse response3 = request3.GetResponse())
            {
                using (StreamReader rd3 = new StreamReader(response3.GetResponseStream()))
                {
                    string soapResult3 = rd3.ReadToEnd();

                    int StartPosFileName = soapResult3.ToString().IndexOf("<FileName>") + 10;
                    int EndPosFileName = soapResult3.ToString().IndexOf("</FileName>");
                    int StartPosFileContent = soapResult3.ToString().IndexOf("<FileContent>") + 13;
                    int EndPosFileContent = soapResult3.ToString().IndexOf("</FileContent>");
                    String EncodedString = soapResult3.ToString().Substring(StartPosFileContent, EndPosFileContent - StartPosFileContent);
                    if (documentType == "application/pdf")
                    {
                        Setoutput(EncodedString);
                    }
                    else if (documentType == "text/xml")
                    {
                        byte[] someEncodedData = Convert.FromBase64String(EncodedString);
                        String someDecodedString = Encoding.GetEncoding("ISO-8859-1").GetString(someEncodedData);

                        XMLtoHTMLConverter Converter = new XMLtoHTMLConverter(someDecodedString, PdfTemplatesTypes.INVOICE_SIO);
                        Setoutput(Converter.GetResultHTMLText());

                    }

                    else
                    {
                        Setoutput("ERROR - Wrong File Format");
                    }

                }
            }
        }


        static public HttpWebRequest CreateWebRequest()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"https://sioweb004.sio.no/ubwprod-ws/service.svc?QueryEngineService/QueryEngineV200606DotNet");
            webRequest.Headers.Add("SOAPAction", "http://services.agresso.com/QueryEngineService/QueryEngineV200606DotNet/GetTemplateResultAsXML");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        static public HttpWebRequest CreateWebRequest2()
        {
            HttpWebRequest webRequest2 = (HttpWebRequest)WebRequest.Create(@"https://sioweb004.sio.no/ubwprod-ws/service.svc?DocArchiveService/DocArchiveV201101");
            webRequest2.Headers.Add("SOAPAction", "http://services.agresso.com/DocArchiveService/DocArchiveV201409/GetDocumentProperties");
            webRequest2.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest2.Accept = "text/xml";
            webRequest2.Method = "POST";
            return webRequest2;
        }

        static public HttpWebRequest CreateWebRequest3()
        {
            HttpWebRequest webRequest3 = (HttpWebRequest)WebRequest.Create(@"https://sioweb004.sio.no/ubwprod-ws/service.svc?DocArchiveService/DocArchiveV201101");
            webRequest3.Headers.Add("SOAPAction", "http://services.agresso.com/DocArchiveService/DocArchiveV201409/GetDocumentRevision");
            webRequest3.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest3.Accept = "text/xml";
            webRequest3.Method = "POST";
            return webRequest3;
        }

        public void Main(String input)
        {
           
            Execute(input);
            Byte[] res = null;

            if (documentType == "application/pdf")
            {
                res = Convert.FromBase64String(Getoutput());
                SetResult(res);
            }

            else if (documentType == "text/xml")
            {

                using (MemoryStream ms = new MemoryStream())
                {

                    var pdf = PdfGenerator.GeneratePdf(Getoutput(), PdfSharp.PageSize.A4);
                    pdf.Save(ms);
                    res = ms.ToArray();
                    SetResult(res);
                }

               
            }

        }
    }
}
