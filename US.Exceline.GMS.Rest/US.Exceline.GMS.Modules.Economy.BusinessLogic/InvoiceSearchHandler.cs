﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.Utills;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public static class InvoiceSearchHandler
    {
        public static int GetSuggestion(string firstValue, string secondValue)
        {
            if (secondValue == string.Empty)
            {
                firstValue.Replace("-", "");
                if (DataUtils.IsValiedInteger(firstValue))
                    return 3;
                else
                    return 0;
            }
            else
            {
                if (DataUtils.IsValiedInteger(firstValue))
                {
                    if (!DataUtils.IsValiedInteger(secondValue))
                        return 4;
                }
                else if (DataUtils.IsValiedInteger(secondValue))
                {
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            return 0;
        }
    }
}
