﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Ccx;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;
using System.Security.Cryptography;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
   public class TransactionRegistrationForLedger
    {
        private int salePointId;
        private int branchId;
        private String signature;
        private String transDate;
        private String transTime;
        private int nr;
        private decimal transAmntIn;
        private decimal transAmntEx;
        private String transaction;
        private String signedTransaction;

        public TransactionRegistrationForLedger(){

        }

        public void initiateRegisterOfSale(int salePointId ,int branchId ,DateTime transDate ,DateTime transTime ,int nr ,decimal transAmntIn ,decimal transAmntEx , String gymCode) {
            this.salePointId = salePointId;
            this.branchId = branchId;
            this.transDate = transDate.ToString("yyyy-MM-dd");
            this.transTime = transTime.ToString("hh:mm:ss");
            this.nr = nr;
            this.transAmntIn = transAmntIn;
            this.transAmntEx = transAmntEx;
            this.signature = getSignatureFromLastTransactionOnSalePoint(salePointId, branchId, gymCode);
            if(this.signature == "0")
            {
                EconomyFacade.AddPreviousSignatureForNextTransaction(this.salePointId, this.branchId, this.signature, gymCode);
            }
            EconomyFacade.FinalizeTransaction(salePointId,branchId,this.signature,this.transDate,this.transTime,nr,this.transAmntIn,this.transAmntEx,gymCode);
            this.transaction = this.signature + ";" + this.transDate + ";" + this.transTime + ";" + this.nr.ToString() + ";" + this.transAmntIn.ToString() + ";" + this.transAmntEx.ToString();
            ProcessHandlerForKasselov process = new ProcessHandlerForKasselov();
            process.formatInput(this.transaction);
            process.computeHash();
            byte[] hashedData = process.getHashedData();
            LogicHandlerForKasselov signMe = new LogicHandlerForKasselov();
            this.signedTransaction = signMe.signTransaction(hashedData, gymCode);
            EconomyFacade.AddPreviousSignatureForNextTransaction(this.salePointId, this.branchId, this.signedTransaction, gymCode);
            
        }

        public String getSignatureFromLastTransactionOnSalePoint(int salePointId, int branchid, String gymCode) {
            return EconomyFacade.GetLastTransaction(salePointId, branchid, gymCode);

        } 

        public double getVatRateForArticle(int invoiceNo, String gymCode) {

            return EconomyFacade.GetVatRateFromInvoiceNo(invoiceNo,gymCode);
        }

    }
}
