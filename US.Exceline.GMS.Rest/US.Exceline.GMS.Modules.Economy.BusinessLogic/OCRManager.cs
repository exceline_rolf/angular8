﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US.Exceline.GMS.Modules.Economy.Core.Utills;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class OCRManager
    {
        public static OperationResult<int> SaveFileLog(string fileName, string folderPath, string user, string fileType, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = OCRFacade.SaveFileLog(fileName, folderPath,user,fileType, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage(ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<OCRSummary> GetOCRSummary(string fileName, string destinationPath, string gymCode)
        {
            OperationResult<OCRImportSummary> processResult = new OperationResult<OCRImportSummary>();
            OperationResult<OCRSummary> result = new OperationResult<OCRSummary>();

            OCRSummary ocrSummary = new OCRSummary();
            result.OperationReturnValue = ocrSummary;
            int fileID = -1;
            ProcessFileSummary(fileName, out fileID, destinationPath, result, gymCode, processResult);

            foreach (NotificationMessage message in processResult.Notifications)
            {
                result.CreateMessage(message.Message, message.MessageType);
            }
            return result;
        }

        public static OperationResult<OCRImportSummary> ImportOCRfile(string fileName, string folderPath, string fileType, string gymCode)
        {
            OperationResult<OCRImportSummary> result = new OperationResult<OCRImportSummary>();
            result.OperationReturnValue = new OCRImportSummary();
            int fileID = -1;
            ProcessFile(fileName, out fileID, folderPath, result,fileType, gymCode);
           return result;
        }

        private static void ProcessFileSummary(string fileName, out int fileId, string destinationPath, OperationResult<OCRSummary> result, string gymCode, OperationResult<OCRImportSummary> processResult)
        {
            IFileValidationStatus validationResults = new FileValidationStatus();
            try
            {
                try
                {
                    validationResults = ValidateFile(fileName, gymCode, true, processResult);
                    fileId = validationResults.FileID;
                    if (!validationResults.Status)
                    {
                        if (validationResults.Mesages != null)
                        {
                            foreach (IMessage msg in validationResults.Mesages)
                            {
                                IMessage msgObj = new ValidationMessage();
                                msgObj = msg;
                                processResult.CreateMessage("Validation Failed : " + msgObj.Text + "", MessageTypes.ERROR);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    processResult.ErrorOccured = true;
                    processResult.CreateMessage("Error occured while validating the file " + fileName, MessageTypes.ERROR);
                    processResult.CreateMessage("Error Details : " + ex.Message + ".", MessageTypes.ERROR);
                    processResult.CreateMessage("Error Details (StackTrace) : " + ex.StackTrace + ".", MessageTypes.ERROR);
                }

                try
                {
                    IUSPOCRRecord paymentsAndNewOrCancelled = null;
                    paymentsAndNewOrCancelled = GetData(fileName, gymCode, true, processResult);
                    if (paymentsAndNewOrCancelled != null)
                    {
                        foreach (IUSPOCRAssignmentRecord assignmentRecord in paymentsAndNewOrCancelled.AssignmentRecords)
                        {
                            if (assignmentRecord.RecordTypeOfFirstRecord == OCRRecordTypes.StartTransactionAmountItem1)
                            {
                                 GetPaymentCounts(assignmentRecord, fileName, validationResults.FileID, result, gymCode);
                            }
                            if (assignmentRecord.RecordTypeOfFirstRecord == OCRRecordTypes.RecordDirectDeduct)
                            {
                               GetCancellationRecordCount(assignmentRecord, fileName, validationResults.FileID, result, gymCode);
                            }
                        }
                    }
                    else
                    {
                        processResult.CreateMessage("Failed to read OCR File. " + fileName + ".", MessageTypes.ERROR);
                        FileManager.MoveFailedFile(destinationPath);
                    }
                }
                catch (Exception ex)
                {
                    processResult.ErrorOccured = true;
                    processResult.CreateMessage("Error occured while reading file : " + fileName + ".", MessageTypes.ERROR);
                    processResult.CreateMessage("Error occured while reading file : " + fileName + ".", MessageTypes.ERROR);
                    processResult.CreateMessage("Error Details (StackTrace) : " + ex.StackTrace + ".", MessageTypes.ERROR);
                }
            }
            catch (Exception ex)
            {
                processResult.ErrorOccured = true;
                processResult.CreateMessage("Failed to process file : " + ex.Message, MessageTypes.ERROR);
            }
            fileId = validationResults.FileID;

        }

        private static bool ProcessFile(string fileName, out int fileId, string destinationPath, OperationResult<OCRImportSummary> result, string fileType, string gymCode)
        {
            bool returnBool = true;
            IFileValidationStatus validationResults = new FileValidationStatus();
            try
            {
                try
                {
                    validationResults = ValidateFile(fileName, gymCode, false, result);
                    fileId = validationResults.FileID;
                    if (!validationResults.Status)
                    {
                        if (validationResults.Mesages != null)
                        {
                            foreach (IMessage msg in validationResults.Mesages)
                            {
                                IMessage msgObj = new ValidationMessage();
                                msgObj = msg;
                                result.CreateMessage("Validation Failed : " + msgObj.Text + "", MessageTypes.ERROR);
                            }
                        }
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    result.ErrorOccured = true;
                    result.CreateMessage("Error occured while validating the file " + fileName, MessageTypes.ERROR);
                    result.CreateMessage("Error Details : " + ex.Message + ".", MessageTypes.ERROR);
                    result.CreateMessage("Error Details (StackTrace) : " + ex.StackTrace + ".", MessageTypes.ERROR);
                }

                try
                {
                    IUSPOCRRecord paymentsAndNewOrCancelled = null;
                    paymentsAndNewOrCancelled = GetData(fileName, gymCode, false, result);
                    if (paymentsAndNewOrCancelled != null)
                    {
                        foreach (IUSPOCRAssignmentRecord assignmentRecord in paymentsAndNewOrCancelled.AssignmentRecords)
                        {
                            if (assignmentRecord.RecordTypeOfFirstRecord == OCRRecordTypes.StartTransactionAmountItem1)
                            {
                                SendTransactionRecords(assignmentRecord, fileName, validationResults.FileID, result,fileType, gymCode);
                            }
                            if (assignmentRecord.RecordTypeOfFirstRecord == OCRRecordTypes.RecordDirectDeduct)
                            {
                                SendCancellationRecords(assignmentRecord, fileName, validationResults.FileID, result, gymCode);
                            }
                        }
                    }
                    else
                    {// Comes to here if paymentsAndNewOrCancelled==null
                        result.CreateMessage("Failed to read OCR File. " + fileName + ".", MessageTypes.ERROR);
                        FileManager.MoveFailedFile(destinationPath);
                    }
                }
                catch (Exception ex)
                {
                    result.ErrorOccured = true;
                    result.CreateMessage("Error occured while reading file : " + fileName + ".", MessageTypes.ERROR);
                    result.CreateMessage("Error Details (StackTrace) : " + ex.StackTrace + ".", MessageTypes.ERROR);
                }
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Failed to process file : " + ex.Message, MessageTypes.ERROR);
            }
            fileId = validationResults.FileID;
            return returnBool;
        }

        public static IFileValidationStatus ValidateFile(string filePath, string gymCode, bool isSummary, OperationResult<OCRImportSummary> result)
        {
            IUSPOCRRecord paymentsAndNewCancelled = new OCRRecord();
            IFileValidationStatus validation = new FileValidationStatus();
            validation.Mesages = new List<IMessage>();
            validation.Status = true;
            int fileID = -1;
            #region Read and Validate FileID
            try
            {
                fileID = GetFileIDbyFileName(filePath, gymCode);
                validation.FileID = fileID;
            }
            catch (Exception ex)
            {
                if (!isSummary)
                {
                    IMessage message = new Message();
                    message.ID = string.Empty;
                    message.Text = "Failed to read the file ID. " + ex.Message;
                    validation.Status = false;
                    validation.Mesages.Add(message);
                    result.CreateMessage(message.Text, MessageTypes.ERROR);
                    return validation;
                }
            }
            if (fileID <= 0)
            {
                if (!isSummary)
                {
                    IMessage message = new Message();
                    message.ID = string.Empty;
                    message.Text = "File is not in the database or already processed";
                    validation.Status = false;
                    validation.Mesages.Add(message);
                    result.CreateMessage(message.Text, MessageTypes.ERROR);
                    return validation;
                }
            }
            #endregion
            try
            {
                OCRValidationRecordCollectionByCreditorCollection validationRecords = new OCRValidationRecordCollectionByCreditorCollection(gymCode);
                paymentsAndNewCancelled = OCRFileImportHelper.ProcessFile(filePath, gymCode, isSummary, result);
                if (paymentsAndNewCancelled != null)
                {
                    try
                    {
                        validationRecords = OCRFileImportHelper.ValidateOCRRecord(paymentsAndNewCancelled, out validationRecords, gymCode);
                    }
                    catch (Exception ex)
                    {
                        OCRValidationRecord validationLogRecord = new OCRValidationRecord(-1);
                        validationLogRecord.HasCriticalError = true;
                        validationLogRecord.CriticalErrorMessage = ex.Message;
                        validationRecords.AddRecord(validationLogRecord);
                        result.CreateMessage(ex.Message, MessageTypes.ERROR);
                    }

                    try
                    {
                        validationRecords.WriteValidationLog(filePath, fileID);
                    }
                    catch (InvalidInputFileException inEx)
                    {
                        IMessage message = new Message();
                        message.ID = string.Empty;
                        message.Text = "File is invalid : " + inEx.Message;
                        validation.Status = false;
                        validation.Mesages.Add(message);
                        result.CreateMessage(inEx.Message, MessageTypes.ERROR);

                    }
                    catch (Exception ex)
                    {
                        IMessage message = new Message();
                        message.ID = string.Empty;
                        message.Text = "Failed to write validation log : " + ex.Message;
                        validation.Status = false;
                        validation.Mesages.Add(message);
                        result.CreateMessage(ex.Message, MessageTypes.ERROR);
                    }

                    foreach (OCRValidationRecordCollectionByCreditor CredRec in validationRecords.RecordsCollectionByCreditorAccountNo)
                    {
                        foreach (OCRValidationRecord rec in CredRec.ValidationRecordsList)
                        {
                            if (rec.HasCriticalError)
                            {
                                IMessage message = new Message();
                                message.ID = rec.LineIndex.ToString();

                                if (rec.CriticalErrorMessage != null)
                                    message.Text = rec.CriticalErrorMessage;
                                else
                                    message.Text = rec.ErrorMessage;
                                
                                validation.Mesages.Add(message);

                                validation.Status = false;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    IMessage message = new Message();
                    message.ID = string.Empty;
                    message.Text = "Failed to read OCR File due to file format error.";
                    validation.Status = false;
                    validation.Mesages.Add(message);
                    result.CreateMessage(message.Text, MessageTypes.ERROR);
                }
                return validation;
            }
            catch (Exception ex)
            {
                IMessage message = new Message();
                message.ID = string.Empty;
                message.Text = "Error occured when reding file id from validation filelog database";
                validation.Status = false;
                validation.Mesages.Add(message);
                result.CreateMessage(message.Text, MessageTypes.ERROR);

                return validation;
            }
        }

        private static int GetFileIDbyFileName(string fileNameWithPath, string gymCode)
        {
            int fileID = -1;
            string fileNameWithoutPath = Path.GetFileName(fileNameWithPath);
            try
            {
                // Check wheather file data is in the system
                fileID = OCRFacade.GetFileIDbyFileName(fileNameWithoutPath, gymCode);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read file ID : " + ex.Message);
            }
            return fileID;
        }

        public static IUSPOCRRecord GetData(string filePath, string gymCode, bool isSummary, OperationResult<OCRImportSummary> result)
        {
            IUSPOCRRecord paymentsAndNewCancelled = new OCRRecord();
            try
            {
                paymentsAndNewCancelled = OCRFileImportHelper.ProcessFile(filePath, gymCode, isSummary, result);
            }
            catch
            {
                FileManager.MoveFailedFile(filePath);
                throw;
            }

            return paymentsAndNewCancelled;
        }

        private static void GetPaymentCounts(IUSPOCRAssignmentRecord requiredAssignmentRecord, string fileName, int fileId, OperationResult<OCRSummary> result, string gymCode)
        {
            try
            {
                OperationResult<OCRImportSummary> paymentResult = new OperationResult<OCRImportSummary>();
                List<IUSPPayment> payments = GetPayments(requiredAssignmentRecord.TransactionRecords, requiredAssignmentRecord.StartAssignmentRecord.RecipientAccountNumber, gymCode, paymentResult);
                if (payments != null)
                {
                    result.OperationReturnValue.PaymentsCount += payments.Count;
                    result.OperationReturnValue.PaymentAmount += payments.Sum(X => Convert.ToDecimal(X.Amount, CultureInfo.InvariantCulture));
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in geeting payments " + ex.Message, MessageTypes.ERROR);
            }
        }

        private static bool SendTransactionRecords(IUSPOCRAssignmentRecord requiredAssignmentRecord, string fileName, int fileId, OperationResult<OCRImportSummary> result, string fileType, string gymCode)
        {
            bool isSuccess = false;
            List<IUSPPayment> payments = GetPayments(requiredAssignmentRecord.TransactionRecords, requiredAssignmentRecord.StartAssignmentRecord.RecipientAccountNumber, gymCode,result);
            isSuccess = AddOCRPayments(payments, requiredAssignmentRecord.StartAssignmentRecord.RecipientAccountNumber, fileName, fileId, fileType, gymCode, result);
            return isSuccess;
        }

        private static List<IUSPPayment> GetPayments(List<IUSPOCRTransactionRecord> transRecords, string creditorAccountNo, string gymCode, OperationResult<OCRImportSummary> result)
        {
            List<IUSPPayment> payments = new List<IUSPPayment>();

            List<OCRTransactionValidRecord> validTransRecords = GetValidTransRecords(transRecords, result);
            if (validTransRecords.Count == 0)
            {
                throw new Exception("No valid transaction records found.");
            }
            foreach (OCRTransactionValidRecord validTransRecord in validTransRecords)
            {
                try
                {
                    IUSPPayment payment = GetPaymentDCbyOCRTransactionValidRecord(validTransRecord, gymCode);
                    payment.Tag1 = creditorAccountNo;
                    payment.DebtorAccountNumber = validTransRecord.OCRTransactionPost2.DebetAccount;
                    payments.Add(payment);
                }
                catch (Exception ex)
                {
                   result.CreateMessage("Invalid record [line : " + validTransRecord.LineIndexOfFirstPart + "]. " + ex.Message, MessageTypes.WARNING);
                }
            }
            return payments;
        }

        private static List<OCRTransactionValidRecord> GetValidTransRecords(List<IUSPOCRTransactionRecord> transRecords, OperationResult<OCRImportSummary> result)
        {
            List<OCRErrorRecord> errorTransRecords = new List<OCRErrorRecord>();
            List<OCRTransactionValidRecord> validTransRecords = new List<OCRTransactionValidRecord>();
            OCRErrorRecord errorTransRecord;
            bool foundErrorRecordsFlag = false;
            foreach (IUSPOCRTransactionRecord transRecord in transRecords)
            {
                if (transRecord is OCRErrorRecord)
                {
                    if (!foundErrorRecordsFlag)
                    {
                        result.CreateMessage("Failed to send transactions. Error transaction Records Found. Records listed bellow.", MessageTypes.WARNING);
                        foundErrorRecordsFlag = true;
                    }
                    errorTransRecord = (OCRErrorRecord)transRecord;
                    result.CreateMessage("Error Transaction Record ::: Line : "
                        + errorTransRecord.LineIndexOfFirstPart + ". Message : " + errorTransRecord.ErrorMessage, MessageTypes.WARNING);
                   
                    errorTransRecords.Add(errorTransRecord);
                }
                else
                {
                    validTransRecords.Add((OCRTransactionValidRecord)transRecord);
                }
            }
            if (errorTransRecords.Count == 0)
            {
                return validTransRecords;
            }
            else
            { 
                return new List<OCRTransactionValidRecord>();
            }
        }

        private static IUSPPayment GetPaymentDCbyOCRTransactionValidRecord(OCRTransactionValidRecord validTransRecord, string gymCode)
        {
            IUSPPayment newPayment = new Payment();
            try
            {
                newPayment.KID = validTransRecord.OCRTransactionPost1.KID;
                newPayment.Tag2 = newPayment.KID;
                decimal amountInDec;
                try
                {
                    //MRA. Bug(Decimal culture issue) fixed. 2010.07.31
                    //amountInDec = ((decimal.Parse(validTransRecord.OCRTransactionPost1.Amount)) / 100);
                    amountInDec = ((validTransRecord.OCRTransactionPost1.Amount.ToValidDecimalForCulture()) / 100);
                }
                catch (Exception)
                {
                    throw new Exception("Amount is not valid.");
                }
                string amountInStr = amountInDec.ToString();
                string currentCul = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                if (currentCul.Equals("nb-NO"))
                {
                    amountInStr = amountInStr.Replace(",", ".");
                }
                newPayment.Amount = amountInStr;
                //newPaymentDC.ARNo
                newPayment.DueDate = validTransRecord.OCRTransactionPost1.TransDate;
                newPayment.Txt = "OCR Payment";
                newPayment.VouDate = validTransRecord.OCRTransactionPost2.PaymentDate;
                newPayment.ItemType = OCRFileImportHelper.GetItemTypeByTypes("OP", gymCode);
                newPayment.Reference2 = validTransRecord.OCRTransactionPost1.CentralID +
                                        validTransRecord.OCRTransactionPost1.DayCode +
                                        validTransRecord.OCRTransactionPost1.PartSettlement +
                                        validTransRecord.OCRTransactionPost1.Counter +
                                        validTransRecord.OCRTransactionPost1.Sign;
            }
            catch (Exception ex)
            {
                throw new Exception("Invalid data in transaction record. Line No : "
                            + validTransRecord.LineIndexOfFirstPart + ". " + ex.Message);
            }
            return newPayment;
        }

        public static bool AddOCRPayments(List<IUSPPayment> payments, string creditorAccountNumber, string fileName, int fileId, string fileType, string dbCode, OperationResult<OCRImportSummary> result)
        {
            try
            {
                Creditor creditor = OCRFacade.GetCreditorEntityRoleIDbyAccountNumber(creditorAccountNumber, "Payment", dbCode);
                if (!string.IsNullOrEmpty(creditor.InkassoId))
                {
                    Guid batrchId = Guid.NewGuid();
                    List<ImportStatus> statusList = new List<ImportStatus>();
                    //add record to payment status table 
                    foreach (IUSPPayment payment in payments)
                    {
                        ImportStatus status = new ImportStatus();
                        status.RegDate = DateTime.Today;

                        if (fileType == "OCR")
                        {
                            if (creditor.AccountSource == "BUREAU") // this was implemented when both account payments comes in 
                            {
                                status.RecordType = "BMA";
                            }
                            else
                            {
                                status.RecordType = "CMA";
                            }
                        }
                        else if (fileType == "DC")
                        {
                            status.RecordType = "BMA";
                        }

                        status.Pid = creditor.InkassoId;
                        status.BNumber = string.Empty;
                        status.Cid = payment.CustId;
                        status.TNumber = payment.Ref;
                        status.Amount = Convert.ToDecimal(payment.Amount, CultureInfo.InvariantCulture); 
                        status.KID = payment.KID.Trim();
                        status.RemitDate = GetRemitDate(payment.VouDate);
                        status.Referanceid = -1;
                        status.BatchId = batrchId.ToString();
                        status.DataSource = "System";
                        status.FileName = fileName;
                        status.FileId = fileId;
                        statusList.Add(status);
                        result.OperationReturnValue.PaymentsSum += status.Amount;
                        result.OperationReturnValue.PaymentsCount += 1;
                    }

                    bool PaymentResult = OCRFacade.AddPaymentStatus(statusList, dbCode);
                    if (PaymentResult)
                    {
                        OCRFacade.ImportOCRPayments(fileId,dbCode);
                        result.CreateMessage("Added Payments for Account no [" + creditorAccountNumber + "]", MessageTypes.INFO);
                        return true;
                    }
                    else
                    {
                        result.CreateMessage("Error in adding OCR Payment Account no [" + creditorAccountNumber + "]", MessageTypes.ERROR);
                        return false;
                    }
                }
                else
                {
                    result.OperationReturnValue.ErrorAccountPaymentSum += payments.Sum(X => decimal.Parse(X.Amount, CultureInfo.CurrentCulture));
                    result.OperationReturnValue.ErrorAccPayments += payments.Count;
                    return false;
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in adding OCR Payment Account no [" + creditorAccountNumber + "]" + ex.Message, MessageTypes.WARNING);
                return false;
            }
        }

        private static string GetRemitDate(string voucherDate)
        {
            if(!string.IsNullOrEmpty(voucherDate))
            {
                string month = string.Empty;
                string date = string.Empty;
                string year = string.Empty;


                string[] voucherDateParts = voucherDate.Split(new char[] {'-'});
                if (voucherDateParts.Length == 3)
                {
                    month = voucherDateParts[1];
                    date = voucherDateParts[2];
                    year = voucherDateParts[0].Substring(2, 2);

                    if (date.Length == 1)
                        date = "0" + date;

                    if (month.Length == 1)
                        month = "0" + month;

                }
                return (date + month + year);
            }
            return string.Empty;
        }

        private static void GetCancellationRecordCount(IUSPOCRAssignmentRecord requiredAssignmentRecord, string fileName, int fileID, OperationResult<OCRSummary> summary, string gymCode)
        {
            try
            {
                List<IUSPNewOrChanged> cancellations = GetNewOrChanged(requiredAssignmentRecord.TransactionRecords);
                summary.OperationReturnValue.NewCancellationCount += cancellations.Count; 
            }
            catch(Exception ex)
            {
                summary.CreateMessage("Error in getting cancellatiosn " + ex.Message, MessageTypes.ERROR);
            }
        }

        private static void SendCancellationRecords(IUSPOCRAssignmentRecord requiredAssignmentRecord, string fileName, int fileID, OperationResult<OCRImportSummary> result, string gymCode)
        {
            OperationResult resultset = new OperationResult();
            AddBBSContractUpdates(GetNewOrChanged(requiredAssignmentRecord.TransactionRecords), requiredAssignmentRecord.StartAssignmentRecord.RecipientAccountNumber, fileName, fileID, gymCode, result);
        }

        public static void AddBBSContractUpdates(List<IUSPNewOrChanged> newOrCancellContracts, string creditorAccountNumber, string fileName, int fileID, string dbCode, OperationResult<OCRImportSummary> result)
        {
            try
            {
                string creditorInkassoID;
                // Check creditor                
                Creditor creditor = OCRFacade.GetCreditorEntityRoleIDbyAccountNumber(creditorAccountNumber, "NewCancel", dbCode);
                creditorInkassoID = creditor.InkassoId;
                if (creditor.IsHelpAccount)
                {
                    result.CreateMessage("Found help account no  with given Account Number[" + creditorAccountNumber + "]", MessageTypes.ERROR);                   
                }
                else
                {
                    if (string.IsNullOrEmpty(creditor.InkassoId) || creditor.EntRoleId < 0)
                    {
                        result.CreateMessage("Could not find gym with given Account Number[" + creditorAccountNumber + "]", MessageTypes.ERROR); 
                    }
                    else if (creditor.EntRoleId < 0)
                    {
                        result.CreateMessage("Could not find gym's Entity Role ID given Account Number[" + creditorAccountNumber + "]", MessageTypes.ERROR); 
                    }
                    else if (string.IsNullOrEmpty(creditor.InkassoId))
                    {
                        result.CreateMessage("Could not find gym's Inkasso ID with given Account Number[" + creditorAccountNumber + "]", MessageTypes.ERROR);
                    }
                }

                if (!string.IsNullOrEmpty(creditor.InkassoId))
                {
                    Guid batchId = Guid.NewGuid();
                    List<CancellationStatus> statusList = new List<CancellationStatus>();
                    foreach (IUSPNewOrChanged newOrCancellContract in newOrCancellContracts)
                    {
                        CancellationStatus cancellationStatus = new CancellationStatus();
                        cancellationStatus.RegDate = DateTime.Today;
                        cancellationStatus.KID = newOrCancellContract.KID.Trim();
                        cancellationStatus.AccountNo = creditorAccountNumber;
                        cancellationStatus.RecordType = "SF";
                        cancellationStatus.PID = creditorInkassoID;
                        cancellationStatus.BNumber = string.Empty;
                        cancellationStatus.CID = string.Empty;

                        if (newOrCancellContract.RegisterType == "1")
                            cancellationStatus.Status = 1;
                        else if (newOrCancellContract.RegisterType == "2")
                            cancellationStatus.Status = 2;
                        else if (newOrCancellContract.RegisterType == "0")
                            cancellationStatus.Status = 0;

                        cancellationStatus.StatusDate = DateTime.Now.ToString("ddMMyyyy");
                        cancellationStatus.ReferenceId = string.Empty;
                        cancellationStatus.BatchID = batchId.ToString();
                        cancellationStatus.DataSource = "System";
                        cancellationStatus.FileID = fileID;
                        statusList.Add(cancellationStatus);

                    }

                    if (statusList.Count > 0)
                    {
                        if (OCRFacade.AddCancellationStatus(statusList, dbCode))
                        {
                            OCRFacade.ImportCancellations(dbCode);
                            result.OperationReturnValue.StatusCount += statusList.Count;
                            result.CreateMessage("Status imported with given Account Number[" + creditorAccountNumber + "]", MessageTypes.INFO);
                        }
                        else
                        {
                            result.CreateMessage("Error in importing ATG status Account Number[" + creditorAccountNumber + "]", MessageTypes.ERROR);
                        }
                    }
                }
                else
                {
                    result.OperationReturnValue.StatusCount += newOrCancellContracts.Count;
                }
            }
            catch (Exception)
            {
                result.CreateMessage("Error in status import with given Account Number[" + creditorAccountNumber + "]", MessageTypes.ERROR);
                
            }
        }

        private static void AddBBSContractUpdateSingle(IUSPNewOrChanged newOrCancellContract, string creditorAccountNumber, string creditorInkassoID, string fileName, OperationResult result, string gymCode)
        {
            try
            {
                AddBBSContractUpdateToDB(newOrCancellContract, creditorAccountNumber, creditorInkassoID, fileName, result, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Failed to add OCR New or Cancelled record : " + ex.Message, MessageTypes.ERROR);
            }
        }

        public static void AddBBSContractUpdateToDB(IUSPNewOrChanged newOrChangedContract, string creditorAccountNo, string creditorInkassoID, string fileName, OperationResult operationResult, string gymCode)
        {
            if (newOrChangedContract.RegisterType == "1")
            {              
                try
                {
                    if (newOrChangedContract.Warning.ToUpper() == "J")
                    {
                        OCRFacade.UpdateWarningType(newOrChangedContract.KID, creditorAccountNo, 1, gymCode);
                    }
                    else if (newOrChangedContract.Warning.ToUpper() == "N")
                    {
                        OCRFacade.UpdateWarningType(newOrChangedContract.KID, creditorAccountNo, 0, gymCode);
                    }
                }
                catch (Exception ex)
                {
                    operationResult.CreateMessage("Failed to update document recieve type. Creditor Account Number["
                    + creditorAccountNo + "] and KID[" + newOrChangedContract.KID + "] " + ex.Message, MessageTypes.ERROR);
                }
            }
            // parameter fileName not use in this path
            else if (newOrChangedContract.RegisterType == "2")
            {
                try
                {
                    OCRFacade.UpdateDirectDeductCancellationStatus(newOrChangedContract.KID, creditorAccountNo, "3", gymCode);
                }
                catch (Exception ex)
                {
                    operationResult.CreateMessage("Failed to update cancellation status. Creditor Account Number["
                   + creditorAccountNo + "] and KID[" + newOrChangedContract.KID + "] " + ex.Message, MessageTypes.ERROR);
                }
            }
            // parameter fileName use only in this path
            else if (newOrChangedContract.RegisterType == "0")
            {
                try
                {
                    OCRFacade.UpdateOCRAllRecords(fileName, newOrChangedContract.KID, newOrChangedContract.Warning, creditorAccountNo, gymCode);
                }
                catch (Exception ex)
                {
                    operationResult.CreateMessage("Failed to add new assignments. Creditor Account Number["
                  + creditorAccountNo + "] and KID[" + newOrChangedContract.KID + "] " + ex.Message, MessageTypes.ERROR);
                }
            }
        }

         private static List<IUSPNewOrChanged> GetNewOrChanged(List<IUSPOCRTransactionRecord> transRecords)
         {
            List<IUSPNewOrChanged> newOrChangedOCRs = new List<IUSPNewOrChanged>();
            List<OCRDirectDeductValidRecord> validDirectDeductRecords = GetValidDirectDeductRecords(transRecords);
            if (validDirectDeductRecords.Count == 0)
            {
                throw new Exception("No valid direct deduct records found.");
            }
            foreach (OCRDirectDeductValidRecord validTransRecord in validDirectDeductRecords)
            {
                try
                {
                    newOrChangedOCRs.Add(GetNewOrChangedDCbyOCRTransactionValidRecord(validTransRecord));
                }
                catch (Exception ex)
                {
                    USLogError.WriteToFile("Invalid record [line : " + validTransRecord.LineIndexOfFirstPart + "]. " + ex.Message, new Exception(), "System");
                }
            }
            return newOrChangedOCRs;
        }

        private static List<OCRDirectDeductValidRecord> GetValidDirectDeductRecords(List<IUSPOCRTransactionRecord> directDeductRecords)
        {
            List<OCRErrorRecord> errorTransRecords = new List<OCRErrorRecord>();
            List<OCRDirectDeductValidRecord> validTransRecords = new List<OCRDirectDeductValidRecord>();
            OCRErrorRecord errorTransRecord;
            bool foundErrorRecordsFlag = false;
            foreach (IUSPOCRTransactionRecord transRecord in directDeductRecords)
            {
                if (transRecord is OCRErrorRecord)
                {
                    // Found an error record.
                    // File should not be send.
                    if (!foundErrorRecordsFlag)
                    {
                        USLogError.WriteToFile("Failed to send transactions. Error transaction Records Found. Records listed bellow.", new Exception(), "System");
                        foundErrorRecordsFlag = true;
                    }
                    errorTransRecord = (OCRErrorRecord)transRecord;
                    USLogError.WriteToFile("Error Transaction Record ::: Line : "
                        + errorTransRecord.LineIndexOfFirstPart + ". Message : " + errorTransRecord.ErrorMessage, new Exception(), "System");
                    errorTransRecords.Add(errorTransRecord);
                }
                else
                {
                    validTransRecords.Add((OCRDirectDeductValidRecord)transRecord);
                }
            }
            if (errorTransRecords.Count == 0)
            { // No error record found
                //Should return valid transaction records
                return validTransRecords;
            }
            else
            {
                // There are some error records
                return new List<OCRDirectDeductValidRecord>();
            }
        }

         private static NewOrChanged GetNewOrChangedDCbyOCRTransactionValidRecord(OCRDirectDeductValidRecord validTransRecord)
        {
            NewOrChanged newPayment = new NewOrChanged();
            try
            {
                newPayment.FBOCounter = validTransRecord.FBOCounter;
                newPayment.KID = validTransRecord.KID;
                newPayment.RegisterType = validTransRecord.RegisterType;
                newPayment.Warning = validTransRecord.Warning;
            }
            catch (Exception ex)
            {
                throw new Exception("Invalid data in transaction record. Line No : "
                            + validTransRecord.LineIndexOfFirstPart + ". " + ex.Message);
            }
            return newPayment;
        }

         public static void AddProcessLog(List<string> messages, string user, int fileId, string gymCode)
         {
             try
             {
                 OCRFacade.AddProcessLog(messages, user, fileId, gymCode);
             }
             catch (Exception) { //to nothing
             }
         }

         public static bool importResult { get; set; }

         public static void AddImportHistory(OCRImportSummary oCRImportSummary, string user, string gymCode)
         {
             try
             {
                 OCRFacade.AddImportHistory(oCRImportSummary, user, gymCode);
             }
             catch (Exception ex)
             {
                 throw new Exception("Error in adding import history", ex);
             }
         }

         public static OperationResult<List<OCRImportSummary>> GetOCRImportHistory(DateTime? startDate, DateTime? endDate, string gymCode)
         {
             OperationResult<List<OCRImportSummary>> result = new OperationResult<List<OCRImportSummary>>();
             try
             {
                 result.OperationReturnValue = OCRFacade.GetOCRImportHistory(startDate, endDate, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in getting OCR Import history " + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

        public static OperationResult<List<PaymentImportLogDC>> GetPaymentImportProcessLog(DateTime fromDate, DateTime toDate, string gymCode)
        {
            var result = new OperationResult<List<PaymentImportLogDC>>();
            try
            {
                result.OperationReturnValue = OCRFacade.GetPaymentImportProcessLog(fromDate, toDate, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Payment Import Process Log " + ex.Message, MessageTypes.ERROR);
            }
            return result;
    }
    }
}
