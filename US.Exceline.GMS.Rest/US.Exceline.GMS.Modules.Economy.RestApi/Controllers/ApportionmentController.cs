﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Economy.API;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.Economy.RestApi.Controllers
{
    [RoutePrefix("Apportionment")]
    public class ApportionmentController : ApiController
    {
        [HttpGet]
        [Route("GetApportionmentData")]
        [Authorize]
        public HttpResponseMessage GetInitialApportionmentData(decimal amount, int arNo, int caseNo)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<USPApportionmentTransaction>> result = Apportionment.GetInitialApportionmentData("", "", amount, arNo, caseNo, 0, "", ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                USPApportionmentTransaction trans = new USPApportionmentTransaction();
                trans.IsError = true;

                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<USPApportionmentTransaction>() { trans }, ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        [HttpPost]
        [Route("AddApportionmentData")]
        [Authorize]
        public HttpResponseMessage AddApportionmentData(List<USPApportionmentTransaction> transactionList)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<int> result = Apportionment.AddNotApportionmentData(-1, "", -1, "", 0, -1, transactionList, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }
    }
}
