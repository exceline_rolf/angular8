﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;
using US.Exceline.GMS.Modules.Economy.API;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.GMS.API;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Exceline.GMS.Modules.Economy.Data;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Data.Commands;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.RestApi.Controllers
{
    [RoutePrefix("Economy")]
    public class EconomyController : ApiController
    {
        [HttpGet]
        [Route("GymConpanies")]
        [Authorize]
        public HttpResponseMessage GetGymCompanies()
        {
            
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<GymCompanyDC>> result = GMSCcx.GetGymCompanies();
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<GymCompanyDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }


        [HttpGet]
        [Route("GetATGSummary")]
        [Authorize]
        public HttpResponseMessage GetATGSummary(int fileType, string startDate, string endDueDate, string sendingNo)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<IUSPCreditor>> result = new OperationResult<List<IUSPCreditor>>();

            if (fileType == 0)
                result = ATG.GetATGCrediors(ATGRecordType.NotifyByCrediCare, 1, Convert.ToDateTime(startDate), Convert.ToDateTime(endDueDate), ExceConnectionManager.GetGymCode(user), "");

            if (fileType == 2)
                result = ATG.GetATGCrediors(ATGRecordType.Cancellation, 1, Convert.ToDateTime(startDate), Convert.ToDateTime(endDueDate), ExceConnectionManager.GetGymCode(user), sendingNo);

            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                if (result.OperationReturnValue.Count > 0) // only if creditors found
                {
                    string folderPath = ConfigurationManager.AppSettings["atgFilePath"];
                    folderPath = folderPath + @"\" + ExceConnectionManager.GetGymCode(user);
                    ATGSummary logg = ATG.GetATGSummary(Convert.ToDateTime(startDate), Convert.ToDateTime(endDueDate), result.OperationReturnValue, fileType, folderPath, ExceConnectionManager.GetGymCode(user), sendingNo);
                   
                    List<string> errorMsg = new List<string>();
                    foreach (string message in logg.LogRecords)
                    {
                        USLogError.WriteToFile(message, new Exception(), user);
                        errorMsg.Add(message);
                    }
                     return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(logg, ApiResponseStatus.OK));
                }else
                {
                    var logg = new ATGSummary()
                    {
                        Count = 0,
                        LogRecords = new List<string>() { "No Records" },
                        SumAmount = 0
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(logg, ApiResponseStatus.OK));
                }
            }
        }

        [HttpGet]
        [Route("GenerateATGFile")]
        [Authorize]
        public HttpResponseMessage GenerateATGFile(int fileType, string startDate, string endDueDate, string sendingNo)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<IUSPCreditor>> result = new OperationResult<List<IUSPCreditor>>();

            if (fileType == 0)
                result = ATG.GetATGCrediors(ATGRecordType.NotifyByCrediCare, 1, Convert.ToDateTime(startDate), Convert.ToDateTime(endDueDate), ExceConnectionManager.GetGymCode(user), "");

            if (fileType == 2)
                result = ATG.GetATGCrediors(ATGRecordType.Cancellation, 1, Convert.ToDateTime(startDate), Convert.ToDateTime(endDueDate), ExceConnectionManager.GetGymCode(user), sendingNo);

            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }else
            { // generate the file 
                string folderPath = ConfigurationManager.AppSettings["atgFilePath"];
                folderPath = folderPath + @"\" + ExceConnectionManager.GetGymCode(user);
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                
                OperationResult<IFileLog> logg = ATG.GenerateFile(Convert.ToDateTime(startDate), Convert.ToDateTime(endDueDate), result.OperationReturnValue, fileType, folderPath, ExceConnectionManager.GetGymCode(user), sendingNo);
                if (logg.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (string message in logg.OperationReturnValue.NotificationMessages)
                    {
                        USLogError.WriteToFile(message, new Exception(), user);
                        errorMsg.Add(message);
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    if (File.Exists(logg.OperationReturnValue.FilePath))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(logg.OperationReturnValue, ApiResponseStatus.OK));
                    }else
                    {
                        logg.OperationReturnValue.FileId = -1;
                        logg.OperationReturnValue.FilePath = null;
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(logg.OperationReturnValue, ApiResponseStatus.OK));
                    }
                }
            }
        }

        [HttpGet]
        [Route("DownloadATGFile")]
        [Authorize]
        public HttpResponseMessage DownloadATGFile(string filePath)
        {
            string user = Request.Headers.GetValues("UserName").First();
            HttpResponseMessage response = null;
            if (!File.Exists(filePath))
            {
                response = Request.CreateResponse(HttpStatusCode.Gone);
            }
            else
            {
                var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StreamContent(fileStream)
                };

                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = Path.GetFileName(fileStream.Name)
                };
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

                try
                {
                    if (!Directory.Exists(Path.GetDirectoryName(filePath) + @"\Processed"))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(filePath) + @"\Processed");
                    }

                    if (!File.Exists(Path.GetDirectoryName(filePath) + @"\Processed\" + Path.GetFileName(filePath)))
                    {
                        File.Copy(filePath, Path.GetDirectoryName(filePath) + @"\Processed\" + Path.GetFileName(filePath));
                    }

                }
                catch(Exception ex)
                {
                    USLogError.WriteToFile(ex.Message, new Exception(), user);
                }
            }
            return response;
        }
        


        [HttpGet]
        [Route("GetAccountNames")]
        [Authorize]
        public HttpResponseMessage GetAccountnames(string gymCode)
        {
            string user = Request.Headers.GetValues("UserName").First();
            GetAccounts action = new GetAccounts();
            List<AccountNamesAndNumbers> result = action.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(user));

            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            

           
          
          //  return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR));
        }

        [HttpPost]
        [Route("AddAccountNumber")]
        [Authorize]
        public HttpResponseMessage Addacountname(string description,int accnum)
        {
            if (accnum>9999) { accnum = 9999; }
            string user = Request.Headers.GetValues("UserName").First();
            AddAccountNum action = new AddAccountNum(description, accnum);
            action.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(user));

            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(1, ApiResponseStatus.OK));
        }

        [HttpGet]
        [Route("RunAccountFile")]
        [Authorize]
        public HttpResponseMessage RunAccountfile(DateTime fromtime, DateTime totime, int branchId, bool selectall)
        {
            
            string user = Request.Headers.GetValues("UserName").First();
            GetKassefil action = new GetKassefil(fromtime,totime,branchId,selectall);
            List<VariablesForKassefil> result = action.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(user));

            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
             
        }
        
        [HttpGet]
        [Route("getKeyForSalePoint")]
        [Authorize]
        public HttpResponseMessage getKeyForSalePoint(int salePointId, int branchId)
        {

            string user = Request.Headers.GetValues("UserName").First();

            List<KeyData> result = EconomyFacade.GetKeyFromDB(ExceConnectionManager.GetGymCode(user));

            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
        }

        [HttpGet]
        [Route("getPublicPEMKeyForSalePoint")]
        [Authorize]
        public HttpResponseMessage getPublicPEMKeyForSalePoint()
        {
            string user = Request.Headers.GetValues("UserName").First();

            LogicHandlerForKasselov Key = new LogicHandlerForKasselov();
            
            String result = Key.fetchPublicKeyForSalePoint(ExceConnectionManager.GetGymCode(user));

            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
          

        }
        
        [HttpGet]
        [Route("GetSalePointsByBranch")]
        [Authorize]
        public HttpResponseMessage GetSalePointsByBranch(int branchId)
        {
             string user = Request.Headers.GetValues("UserName").First();

             List<KeyData> result = EconomyFacade.GetSalePointsByBranch(branchId, ExceConnectionManager.GetGymCode(user));

             return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));



        }

        
        [HttpGet]
        [Route("GetTransactionData")]
        [Authorize]
        public HttpResponseMessage GetTransactionData(DateTime fromDate, DateTime toDate, int branchId)
        {

            string user = Request.Headers.GetValues("UserName").First();

            List<DataForLedger> result = EconomyFacade.ViewSkatteEtatenLedger(fromDate, toDate,branchId,ExceConnectionManager.GetGymCode(user));

            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));


        } 


    }
}
