﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Economy.RestApi.Models
{
    public class MovePaymentDetails
    {
        public int paymentID { get; set; }
        public int aritemno { get; set; }
        public string type { get; set; }
    }
}