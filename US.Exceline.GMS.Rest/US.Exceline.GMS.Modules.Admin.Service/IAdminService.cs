﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ServiceModel;
using US.Common.Notification.Core.DomainObjects;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin.HolydayManagement;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.Common.Web.UI.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin.ManageAnonymizing;

namespace US.Exceline.GMS.Modules.Admin.Service
{
    [ServiceContract]
    interface IAdminService
    {

        #region GymEmployee
        [OperationContract]
        string SaveGymEmployee(GymEmployeeDC employee, string user);

         [OperationContract]
        bool ValidateCreditorNo(int branchId, string user, string creditorNo);

        [OperationContract]
        int UpdateGymEmployee(GymEmployeeDC employee, int branchId, string user);

        [OperationContract]
        bool DeActivateGymEmployee(int employeeId, int branchId, string user);

        [OperationContract]
        List<GymEmployeeDC> GetGymEmployees(int branchId, string searchText, string user, bool isActive, int roleId);

        [OperationContract]
        GymEmployeeDC GetGymEmployeeByEmployeeId(int branchId, string user, int employeeId);

        [OperationContract]
        bool ValidateEmployeeFollowUp(int employeeId, DateTime endDate, string user);

        [OperationContract]
        bool DeleteGymEmployee(int memberId, int assignEmpId, string user);

        [OperationContract]
        List<GymEmployeeRoleDurationDC> GetGymEmployeeRoleDuration(int employeeId, string user);

        [OperationContract]
        bool ImportArticleList(string articleList, string user, int branchID);

        [OperationContract]
        List<GymEmployeeWorkPlanDC> GetGymEmployeeWorkPlan(int employeeId, string user);

        [OperationContract]
        List<GymEmployeeApprovalDC> GetGymEmployeeApprovals(int employeeId, string approvalType, string user);

        [OperationContract]
        bool AddEmployeeTimeEntry(EmployeeTimeEntryDC timeEntry, string gymCode);

        [OperationContract]
        List<EmployeeTimeEntryDC> GetEmployeeTimeEntries(int employeeId, string gymCode);

        [OperationContract]
        bool ApproveEmployeeTimes(int employeeId, bool isAllApproved, string timeIdString, string user);

        [OperationContract]
        int AdminApproveEmployeeWork(GymEmployeeWorkDC work, bool isApproved, string user);

        [OperationContract]
        int SaveGymEmployeeWorkScheduleItem(ScheduleItemDC scheduleItem, int resourceId, int branchId, string user);
        [OperationContract]
        List<EmployeeEventDC> GetEmployeeEvents(int employeeId, string user);

        [OperationContract]
        int SaveWorkActiveTime(GymEmployeeWorkPlanDC work, int branchId, string user);

        [OperationContract]
        int SaveWorkItem(GymEmployeeWorkDC work, int branchId, string user);

        [OperationContract]
        List<EmployeeJobDC> GetEmployeeJobs(int employeeId, string user);
        [OperationContract]
        bool DeleteGymEmployeeActiveTime(int activeTimeId, string user);

        [OperationContract]
        List<EmployeeClass> GetEmployeeClasses(int empId, string user);

        [OperationContract]
        int UpdateApproveStatus(bool IsApproved, int EntityActiveTimeID, string user);

        [OperationContract]
        List<EmployeeBookingDC> GetEmployeeBookings(int employeeId, string user);

        #endregion

        #region TimeEntry

        [OperationContract]
        List<TimeEntryDC> GetTimeEntryForAdminView(DateTime fromDate, DateTime endDate, string user);

        #endregion

        #region Resources
        [OperationContract]
        int SaveResources(ResourceDC resource, string user);

        [OperationContract]
        bool UpdateResource(ResourceDC resource, string user);

        [OperationContract]
        bool DeleteResource(int branchId, string user);

        [OperationContract]
        bool DeActivateResource(int resourceId, int branchId, string user);

        [OperationContract]
        bool SwitchResource(int resourceTd1, int resourceId2, DateTime startDate, DateTime endDate, string comment, int branchId, string user);

        [OperationContract]
        bool SwitchResourceUndo(int resId, DateTime startDate, DateTime endDate, string user, int branchId);

        [OperationContract]
        bool ValidateScheduleWithSwitchResource(int resourceId1, int resourceId2, DateTime startDate, DateTime endDate, int branchId, string user);

        [OperationContract]
        bool ValidateScheduleWithResourceId(int resourceId, DateTime startDate, DateTime endDate, int branchId, string user);

        [OperationContract]
        List<ResourceDC> GetResources(int branchId, string searchText, int categoryId, int activityId, int equipmentid, bool isActive, string user);

        [OperationContract]
        List<AvailableResourcesDC> GetAvailableResources(int branchId, int resId, string user);

        [OperationContract]
        List<AvailableResourcesDC> GetResourceAvailableTimeById(int branchId, int resId, string user);

        [OperationContract]
        List<string> ValidateResourcesWithSchedule(String roleType, List<AvailableResourcesDC> resourceList, int branchId, string user);

        [OperationContract]
        bool AddOrUpdateAvailableResources(List<AvailableResourcesDC> resourceList, int branchId, string user);

        [OperationContract]
        List<int> GetEmpolyeeForResources(List<int> resIdList, string user);
        [OperationContract]
        List<int> ValidateResWithSwitchDataRange(List<int> resourceId, DateTime startDate, DateTime endDate, string user);
        #endregion

        #region manage Contract
        [OperationContract]
        int SaveContract(PackageDC resource, string user);

        [OperationContract]
        List<PackageDC> GetContracts(int branchId, string searchText, int searchType, string searchCriteria, string user);

        [OperationContract]
        string GetNextContractId(string user, int branchId);

        [OperationContract]
        int DeleteContract(string user, int contractId);

        [OperationContract]
        List<ActivityDC> GetActivities(int branchId, string user);

        [OperationContract]
        List<PriceItemTypeDC> GetPriceItemTypes(int branchId, string user);

        #endregion

        #region Category

        [OperationContract]
        List<CategoryDC> GetCategories(string type, string user);

        [OperationContract]
        List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId);

        [OperationContract]
        int SaveCategory(CategoryDC category, string user, int branchId);

        [OperationContract]
        bool UpdateCategory(CategoryDC category, string user, int branchId);

        [OperationContract]
        int DeleteCategory(int categoryId, string user, int branchId);

        [OperationContract]
        List<CategoryDC> SearchCategories(string searchText, string searchType, string user, int branchId);

        [OperationContract]
        List<ActivityDC> GetActivitiesForBranch(int branchId, string user);

        [OperationContract]
        int AddActivitySettings(int branchId, string user, ActivitySettingDC activitySetting);

        #endregion

        #region Contract item
        [OperationContract]
        bool RemoveContractItem(int contractItemId, string user, int branchId);

        [OperationContract]
        bool AddContractItem(ContractItemDC contractItem, string user, int branchId);

        [OperationContract]
        List<ContractItemDC> GetContractItems(int branchID, string suer);

        [OperationContract]
        List<ArticleDC> GetArticlesForCategory(int categoryID, string user, int branchID);

        #endregion

        #region Task Template
        [OperationContract]
        bool SaveTaskTemplate(TaskTemplateDC taskTemplate, bool isEdit, string user);

        [OperationContract]
        bool UpdateTaskTemplate(TaskTemplateDC taskTemplate, string user);

        [OperationContract]
        bool DeleteTaskTemplate(int taskTemplateId, string user);

        [OperationContract]
        bool DeActivateTaskTemplate(int taskTemplateId, string user);

        [OperationContract]
        List<TaskTemplateDC> GetTaskTemplates(TaskTemplateType templateType, int branchId, int templateId, string user);

        [OperationContract]
        List<ExtendedTaskTemplateDC> GetTaskTemplateExtFields(int extFieldId, string user);

        [OperationContract]
        List<ExtendedFieldDC> GetExtFieldsByCategory(int categoryId, string categoryType, string user);
        #endregion

        #region Exceline Task
        [OperationContract]
        bool SaveExcelineTask(ExcelineTaskDC excelineTask, string user);

        [OperationContract]
        List<ExcelineTaskDC> GetExcelineTasks(int branchId, bool isFxied, int templateId, int assignTo, string roleType, int followupMemberId, string user);

        [OperationContract]
        bool UpdateExcelineTask(ExcelineTaskDC excelineTask, string user);


        [OperationContract]
        List<ExcelineTaskDC> SearchExcelineTask(string searchText, TaskTemplateType type, int followUpMemId, int branchId, bool isFxied, string user);

        [OperationContract]
        bool SaveTaskWithOutSchedule(ExcelineTaskDC excelineTask, string user);
        #endregion

        #region Schedule
        [OperationContract]
        bool UpdateActiveTime(EntityActiveTimeDC activeTime, string user, int branchId, bool isDelete);

        [OperationContract]
        bool UpdateSheduleItems(ScheduleDC schedule, string user);

        [OperationContract]
        List<EntityActiveTimeDC> GetEntityActiveTimes(int branchid, DateTime startDate, DateTime endDate, List<int> entityList, string entityRoleType, string user);

        [OperationContract]
        ScheduleDC GetEntitySchedule(int entityId, string entityRoleType, string user);

         [OperationContract]
        bool ValidateScheduleWithPaidBooking(int scheduleItemId, int resourceId, string user);

        [OperationContract]
        bool DeleteScheduleItem(int scheduleItemId, bool activeStatus, string user);

        [OperationContract]
        bool SaveShedule(int id, string name, string roleId, int branchId, ScheduleDC sheduleDc, string user);

        [OperationContract]
        List<ScheduleItemDC> GetScheduleItemsByScheduleId(int scheduleId, string user);

        #endregion

        #region Member

        [OperationContract]
        List<OrdinaryMemberDC> GetMembersForCommonBooking(int branchId, string searchText, string user);

        [OperationContract]
        List<EntityVisitDC> GetMembersVisits(DateTime startDate, DateTime endDate, int branchId, string user);

        [OperationContract]
        OrdinaryMemberDC GetMembersById(int branchId, int memberId, string user);

        [OperationContract]
        List<ActivityDC> GetActivitiesForCommonBooking(int entityId, string entityType, int memberId, string user);

        #endregion

        #region Booking

        [OperationContract]
        bool CheckCommonBookingAvailability(int activeTimeId, int entityId, int branchId, string entityType, DateTime startDateTime, DateTime endDateTime, string user);

        [OperationContract]
        bool SaveCommonBookingDetails(CommonBookingDC commonBookingDC, ScheduleDC scheduleDC, int branchId, string user, string scheduleCategoryType);

        #endregion

        #region common
        [OperationContract]
        string UpdateEmployees(EmployeeDC employeeDc, string user);
        [OperationContract]
        string UpdateMemeberActiveness(int trainerId, string rolId, int branchId, string user, bool isActive);

        [OperationContract]
        List<ExcelineTaskDC> GetEntityTasks(int entityId, int branchId, string entityRoleType, string user);

        [OperationContract]
        bool SaveEntityTimeEntry(TimeEntryDC timeEntry, string user);

        [OperationContract]
        List<TimeEntryDC> GetEntityTimeEntries(DateTime startTime, DateTime endTime, int entityId, string entityRoleType, string user);

        [OperationContract]
        bool DeleteEntityTimeEntry(int entityTimeEntryId, int taskId, decimal timeSpent, string user);

        [OperationContract]
        List<CategoryDC> GetTaskStatusCategories(string user, int branchId);

        [OperationContract]
        bool SaveEmployeesRoles(EmployeeDC employeeDc, string user);

        [OperationContract]
        List<RoleActivityDC> GetActivitiesForRoles(int memberId, string user);

        #endregion

        #region Article
        [OperationContract]
        int SaveArticle(ArticleDC articleDc, string user, int branchID, int activityCategoryId);
        [OperationContract]
        List<ArticleDC> GetArticles(int branchId, string user, ArticleTypes categpryType, string keyword, CategoryDC category, int activityId, bool? isObsalate, bool isActive, bool filterByGym);

        #endregion

        #region Branch

        [OperationContract]
        int SaveBranchDetails(ExcelineBranchDC branch, string user);

        [OperationContract]
        List<ExcelineCreditorGroupDC> GetBranchGroups(string user);

        [OperationContract]
        List<ExcelineBranchDC> GetBranches(string user, int branchId);

        [OperationContract]
        int UpdateBranchDetails(ExcelineBranchDC branch, string user);

        #endregion

        #region Manage Discount

        [OperationContract]
        List<OrdinaryMemberDC> GetMembersByRoleType(int branchId, string user, bool activeState, MemberRole roleType, string searchText);

        [OperationContract]
        int SaveDiscount(List<DiscountDC> discountList, string user, int branchId, int contractSettingID);

        [OperationContract]
        bool DeleteDiscount(List<int> discountIDList, string user);

        [OperationContract]
        bool DeleteShopSponsorDiscount(int discountId, string type, string user);

        

        [OperationContract]
        List<DiscountDC> GetDiscountList(int branchId, DiscountType discountType, string user);

        [OperationContract]
        List<ExcelineMemberDC> GetVenderList(int branchId, string text, string user);

        [OperationContract]
        int UpdateContractPriority(Dictionary<int, int> packages, string user);

        #endregion

        #region Invoice
        [OperationContract]
        int GenerateInvoice(List<ExcelineInvoiceDetailDC> invoiceDetails, bool ismemberRecords, int branchID, string user, bool isGenereteNow);
        [OperationContract]
        List<ExcelineInvoiceDetailDC> GetInvoiceDetails(DateTime invoiceDate, bool isMemberRecords, string user, int branchID);
        [OperationContract]
        string SearchInvoices(string searchValue, int invoiceType, string filedType, object constValue, string user, int branchId);
        [OperationContract]
        InvoiceBasicInfoDC GetInvoiceBasicInfo(int subCaseNo, string user);
        [OperationContract]
        bool CancelInvoice(int arItemNo, string invoiceNo, string user);


        #endregion

        #region Uttility
        [OperationContract]
        string GetCityForPostalCode(string postalCode, string useruser);
        [OperationContract]
        List<CalendarHoliday> GetGymHolidays(DateTime calendarDate, string user, int branchId);
        #endregion

        #region Holidays
        [OperationContract]
        List<HolidayScheduleDC> GetHolidays(int branchId, string holidayType, DateTime startDate, DateTime endDate, string user);

        [OperationContract]
        bool AddNewHoliday(HolidayScheduleDC newHoliday, string user);

        [OperationContract]
        int GetHolidayCategoryId(string categoryName, string user);

        [OperationContract]
        bool UpdateHolidayStatus(HolidayScheduleDC updateHoliday, string user);
        #endregion

        #region Notification
        [OperationContract]
        int SaveNotificationTemplate(NotificationTemplateDC notificationTemplate, string user, int branchId);

        [OperationContract]
        List<NotificationTemplateDC> GetNotificationTemplates(int branchId, string user);
        #endregion

        [OperationContract]
        GymIntegrationExcSettingsDC grt();

        #region Gym Settings
        //[OperationContract]
        //string GetGymSettings(int branchId, string user, GymSettingType gymSettingType, string systemName);

        [OperationContract]
        bool SaveGymSettings(int branchId, string user, string accessTimeDetails, GymSettingType gymSettingType);

        [OperationContract]
        Dictionary<string, object> GetSelectedGymSettings(List<string> gymSetColNames, bool isGymSettings, int branchId, string user);

        //[OperationContract]
        //string GetTerminalTypes(string user);

        [OperationContract]
        bool DeleteIntegrationSettingById(int id, string user);
        #endregion

        #region Gym Company Settings
        //[OperationContract]
        //string GetGymCompanySettings(string user, GymCompanySettingType gymCompanySettingType);

        [OperationContract]
        int AddUpdateGymCompanySettings(string settingsDetails, string user, GymCompanySettingType gymCompanySettingType);

        [OperationContract]
        List<ExceAccessProfileDC> GetAccessProfiles(string user);

        [OperationContract]
        int UpdateAccessProfiles(List<ExceAccessProfileDC> AccessProfileList, string user);

        #endregion

        #region DummyMethodForPassDomainObject
        [OperationContract]
        void DummyMethodForPassDomainObject(ExceGymSettingDC gymSetting, ExceGymCompanySettingsDC gymCompanySetting);

        [OperationContract]
        void DummyMethodForGym(GymAccessSettingDC gymSetting);
        #endregion

        #region ClassTypeSettings

        [OperationContract]
        List<ExceClassTypeDC> GetClassTypes(string user);

        [OperationContract]
        bool AddEditClassType(ExceClassTypeDC classType, string user);

        [OperationContract]
        int DeleteClassType(int classTypeId, string user);

        #endregion


        #region USC
        [OperationContract]
        void TestUSC();
        #endregion

        #region ManageDiscount
        [OperationContract]
        List<ShopSalesItemDC> GetDailySales(string user, DateTime salesDate, int branchId);

        #endregion

        [OperationContract]
        bool AttachFile(string fileName, string custId, string companyCode, string username, int branchId);

        [OperationContract]
        List<ExcelineMemberDC> GetMembers(int branchId, string user, string searchText, int activeState, MemberSearchType searchType, MemberRole memberRole, int hit, string sortName, bool isAscending);

        [OperationContract]
        OrdinaryMemberDC GetMemberDetailsByMemberId(int branchId, string user, int memberId, string memberRole);

        [OperationContract]
        List<AccountDC> GetRevenueAccounts(string user);

        [OperationContract]
        int AddUpdateRevenueAccounts(AccountDC revenueAccountDetail, string user);

        [OperationContract]
        int DeleteRevenueAccounts(AccountDC revenueAccountDetail, string user);

        [OperationContract]
        string SaveMember(OrdinaryMemberDC member, string user);

        [OperationContract]
        bool SaveDocumentData(DocumentDC document, string username, int branchId);

        [OperationContract]
        int SaveRegion(RegionDC region, string user, int branchId);

        [OperationContract]
        List<RegionDC> GetRegions(int branchId, string user, string countryId);

        [OperationContract]
        int SavePostalArea(string user, string postalCode, string postalArea, long population, long houseHold);

        [OperationContract]
        List<CountryDC> GetCountryDetails(string user);

        [OperationContract]
        List<String> GetClassKeywords(string user);

        [OperationContract]
        SalePointDC GetSalesPointByMachineName(int branchID, string machineName, string user);

        [OperationContract]
        bool AddEditClassKeyWords(List<CategoryDC> keyWords, string user);

        [OperationContract]
        string GetMem();

        [OperationContract]
        decimal SaveStock(ArticleDC article, int branchId, string user);

        [OperationContract]
        int DeleteArticle(int articleId, int branchId, string user, bool isAdminUser);

        [OperationContract]
        List<ActivityTimeDC> GetActivityTimes(int branchId, string user, bool isUnavailableTimes);

        [OperationContract]
        bool AddActivityTimes(List<ActivityTimeDC> activityTimes, string user);

        [OperationContract]
        int SaveInventory(InventoryItemDC inventoryItem, string user);

        [OperationContract]
        List<InventoryItemDC> GetInventory(int branchId, string user);

        [OperationContract]
        int DeleteInventory(int inventoryId, string user);

        [OperationContract]
        List<ArticleDC> GetInventoryDetail(int branchId, int inventoryId, string user);

        [OperationContract]
        decimal SaveInventoryDetail(List<ArticleDC> articleList, int inventoryId, string user);

        [OperationContract]
        Dictionary<decimal, decimal> GetNetValueForArticle(int inventoryId, int articleId, int counted, int branchId, string user);

        [OperationContract]
        List<InstructorDC> GetInstructors(int branchId, string searchText, string user, bool isActive, int instructorId);

        [OperationContract]
        int SaveScheduleItem(ScheduleItemDC scheduleItem, int resId, int branchId, string user);

        [OperationContract]
        ScheduleDC GetResourceScheduleItems(int resId, string roleTpye, string user);

        [OperationContract]
        List<ScheduleDC> GetResourceCalenderScheduleItems(List<int> resIdList, string roleTpye, string user);

        [OperationContract]
        int DeleteResourcesScheduleItem(int scheduleItemId, string user);

        [OperationContract]
        List<EntityActiveTimeDC> GetMemberBookingDetails(int branchId, int scheduleItemId, string user);

        [OperationContract]
        int CheckDuplicateResource(int sourceId, int destinationId, DateTime startDate, DateTime endDate, string user);

        [OperationContract]
        int SaveResourceActiveTime(EntityActiveTimeDC activeTimeItem, string user, int branchId);

        [OperationContract]
        int DeleteResourceActiveTime(int activeTimeId, string articleName, DateTime visitDateTime, string roleType, bool isArrived, bool isPaid,List<int> memberList, string user);

        [OperationContract]
        List<ArticleDC> GetArticleForResouceBooking(int activityId, int branchId, string user);

        [OperationContract]
        List<ArticleDC> GetArticleForResouce(int activityId, int branchId, string user);

        #region Task Categories
        [OperationContract]
        bool SaveTaskCategory(ExcelineTaskCategoryDC taskCategory, bool isEdit, string user);

        [OperationContract]
        bool DeleteTaskCategory(int taskCategoryId, string user);


        [OperationContract]
        List<ExcelineTaskCategoryDC> GetTaskCategories(int branchId, string user);
        #endregion

        #region followUp

        [OperationContract]
        bool AddFollowUpTemplate(FollowUpTemplateDC template, string user, int branchId);

        [OperationContract]
        List<FollowUpTemplateDC> GetFollowUpTemplates(string user, int branchId);

        [OperationContract]
        bool DeleteFollowUpTemplate(int FollowUpTemplateId, string user);

        [OperationContract]
        List<FollowUpTemplateTaskDC> GetFollowUpTaskByTemplateId(int followUpTemplateId, string user);

        [OperationContract]
        List<FollowUpDetailDC> GetFollowUpDetailByEmpId(int empId, string user);

        #endregion

        #region Job Categories
        [OperationContract]
        bool SaveJobCategory(ExcelineJobCategoryDC jobCategory, bool isEdit, string user);

        [OperationContract]
        bool DeleteJobCategory(int taskCategoryId, string user);


        [OperationContract]
        List<ExcelineJobCategoryDC> GetJobCategories(int branchId, string user);
        #endregion

        #region Job
        [OperationContract]
        int SaveExcelineJob(ScheduleItemDC scheduleItem, int branchId, string user);

        [OperationContract]
        List<ScheduleItemDC> GetJobScheduleItems(int branchId, string user);

        [OperationContract]
        int DeleteJobScheduleItem(List<int> scheduleItemIdList, string user);

        [OperationContract]
        List<ExcelineCommonTaskDC> GetAllTasksByEmployeeId(int branchId, int employeeId, string user);

        [OperationContract]
        List<ExcelineCommonTaskDC> GetFollowUpByEmployeeId(int branchId, int employeeId, string user, int hit);

        [OperationContract]
        List<ExcelineCommonTaskDC> GetJobsByEmployeeId(int branchId, int employeeId, string user, int hit);

        [OperationContract]
        int AssignTaskToEmployee(ExcelineCommonTaskDC commonTask, int employeeId, string user);

        [OperationContract]
        List<USPRole> GetEmpRoles(string user);

        [OperationContract]
        List<ExcelineRoleDc> GetExcelineEmpRoles(string user);

        [OperationContract]
        ExtendedFieldDC Test();
        #endregion
        [OperationContract]
        ArticleDC GetInvoiceFeeArticle(int branchId, string user);

        [OperationContract]
        List<SalePointDC> GetSalesPointList(int branchID, string user);

        [OperationContract]
        int SavePointOfSale(int branchId, SalePointDC pointOfSale, string user);

        [OperationContract]
        int GetSelectedHardwareProfile(int branchId, string user);

        [OperationContract]
        MemberShopAccountDC GetMemberShopAccounts(int memberId, int hit, bool itemsNeeded, string user);

        [OperationContract]
        OccuranceType getData();

        [OperationContract]
        bool GetTest();

        [OperationContract]
        SaleResultDC AddShopSales(InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberBranchId,
                         int branchID, string user);
        [OperationContract]
        bool SendSmsForBooking(Dictionary<int,string> memberIdList, int branchId, string user, string message, string mobileNo);

        [OperationContract]
        int AddMemberNotification(USNotificationTree notification, string text);

        [OperationContract]
        Dictionary<List<int>, List<int>> CheckUnusedCancelBooking(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime bookingDate, string user);

        [OperationContract]
        int AddFreeDefineNotification(Dictionary<int,string> memberIdList, string memName, DateTime startTime, DateTime endTime, string resName, DateTime bookingDate, int branchId, string user);

        [OperationContract]
        int UnavailableResourceBooking(int scheduleItemId, int activeTimeId, DateTime startDate, DateTime endDate, string user);
        [OperationContract]
        int AddCreditNoteForBooking(CreditNoteDC creditNote, List<PayModeDC> payModeDcs, int branchId, bool isReturn, string user);

        [OperationContract]
        PDFPrintResultDC ViewContractDetail(int CCId, int branchId, string user);

        [OperationContract]
        string GetConfigSettings(string type);

        //[OperationContract]
        //bool ValidateUser(string userName, string password);

        [OperationContract]
        Dictionary<List<int>, List<int>> ValidatePunchcardContractMember(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime startDate, string user);

        [OperationContract]
        string GetSessionValue(string sessionKey, string user);

        [OperationContract]
        bool AddCountryDetails(CountryDC country, string user);

        [OperationContract]
        bool AddRegionDetails(RegionDC region, string user);

        [OperationContract]
        int AddSessionValue(string sessionKey, string value, string user);

        [OperationContract]
        int SavePaymentBookingMember(int activetimeId, int articleId, int memberId, bool paid, int aritemNo, decimal amount, string paymentType,
                                     string user);

        [OperationContract]
        USPUser ValidateUserWithCard(string cardNumber, string user);

         [OperationContract]
        DailySettlementDC GetDailySettlements(int branchId, int salePointId, string user);

        #region Manage Anonymizing
        [OperationContract]
        int DeleteAnonymizingData(ExceAnonymizingDC anonymizing, int branchId, string user);

          [OperationContract]
        List<ExceAnonymizingDC> GetAnonymizingData(string user);
        #endregion

        [OperationContract]
        List<ResourceDC> GetResourceCalender(int branchId, string user, int hit);

         [OperationContract]
        ResourceDC GetResourceDetailsById(int resourceId, string user);

         [OperationContract]
        PDFPrintResultDC PrintInventoryList(int branchId, string user, int inventoryId);

        [OperationContract]
        PDFPrintResultDC PrintResourceBookingCalender(int branchId, string user, DateTime fromDate, DateTime endDate);

        [OperationContract]
        List<ExceUserBranchDC> GetUserSelectedBranches(string userName, string user);

           [OperationContract]
        List<ExcelineRoleDc> GetGymEmployeeRolesById(int employeeId, int branchId, string user);

        [OperationContract]
        List<ExcelineRoleDc> GetUserRole(string user);

          [OperationContract]
        bool UploadArticle(List<ArticleDC> articleList, string user, int branchId);

          [OperationContract]
          int ValidateGiftVoucherNumber(string voucherNumber, decimal payment, string user);

          [OperationContract]
          List<ContractSummaryDC> GetContractSummariesByEmployee(int employeeID, int branchId, DateTime createdDate, string user);

        [OperationContract]
        List<ExcACCAccessControl> GetExcAccessControlList(int branchId, string user);

        [OperationContract]
        string GetExcelineHostPath();

        [OperationContract]
        bool UpdateSettingForUserRoutine(string key, string value, string user);

        [OperationContract]
        string GetResourceBookingViewMode(string user);

        [OperationContract]
        List<ContractTemplateDC> ValidateContractConditionWithTemplate(int contractConditionId, string user);
        
        [OperationContract]
        int ValidateContractConditionWithContracts(int contractConditionId, string user);

        [OperationContract]
        ContractCondition TempContractCondition();

          [OperationContract]
        string GetMemberSearchCategory(int branchId, string user);

        [OperationContract]
        CustomerBasicInfoDC GetMemberBasicInfo(int memberId, string user);

         [OperationContract]
        List<ExceArxFormatDetail> GetArxSettingDetail(int branchId, string user);

           [OperationContract]
        List<ExceArxFormatType> GetArxFormatType(string user);

         //[OperationContract]
         //  int SaveArxSettingDetail(ExceArxFormatDetail arxFormatDetail, int tempCodeDeleteDate, int branchId, string user);

         [OperationContract]
        bool ValidateArticleByActivityId(int activityId, int articleId, string user);
    }
}
