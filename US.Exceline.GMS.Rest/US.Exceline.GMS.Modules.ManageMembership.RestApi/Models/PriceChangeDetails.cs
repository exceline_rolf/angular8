﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class PriceChangeDetails
    {
        public DateTime dueDate { get; set; }
        public bool lockInPeriod { get; set; }
        public bool priceGuarantyPeriod { get; set; }
        public decimal minimumAmount { get; set; }
        public int templateID { get; set; }
        public List<int> gyms { get; set; }
        public bool changeToNextTemplate { get; set; }

        public int hit { get; set; }
    }
}