﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class GenerateInvoice
    {
        public int invoiceBranchId { get; set; }
        public InstallmentDC installment { get; set; }
        public PaymentDetailDC paymentDetail { get; set; }
        public string invoiceType { get; set; }
        public string notificationTitle { get; set; }
        public int memberBranchID { get; set; }
    }
}