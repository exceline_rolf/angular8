﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class UpdateInvoice
    {
        public int ArItemNo { get; set; }
        public DateTime DueDate { get; set; }
        public string Comment { get; set; }
    }
}