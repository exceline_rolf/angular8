﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class WellnessIntegration
    {
      
        public OrdinaryMemberDC Member { get; set; }
        public MemberIntegrationSettingDC WellnessIntegrationSetting { get; set; }
        public WellnessOperationType WellnessOperationType { get; set; }
        public int BranchId { get; set; }
    }
}