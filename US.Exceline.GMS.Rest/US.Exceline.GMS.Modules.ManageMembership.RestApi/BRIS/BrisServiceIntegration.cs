﻿using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.Exceline.GMS.Modules.ManageMembership.RestApi.Models;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.BRIS
{
    public class BrisServiceIntegration
    {
        private BrukereOgBrukerskApi _brisInstance;
        private SkrivBrukerdataApi _skrivBrukerdataApi;
        private ITjenesteResourceApi _iTjenesteResourceApi = null;
        private Dictionary<string, string> _countryCodes = new Dictionary<string, string>();

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "item")]
        public List<OrdinaryMemberDC> SearchBrisMember(string apiKey, string userId, string firstName, string lastName, string email, string mobile, string birthDay, string cardNumber, string ssn, string gymCode)
        {

            //File.WriteAllText(@"C:\Exceline\text.txt", "Search " + Environment.NewLine);
            _brisInstance = new BrukereOgBrukerskApi();
            var body = new BrukerSokBeanDto //search files are ok 
            {
                Brukerid = userId,
                Fornavn = firstName,
                Etternavn = lastName,
                Epostadresse = email,
                Mobiltelefon = mobile,
                Fodselsdato = birthDay,
                Kortnummer = cardNumber,
                Personnummer = ssn
            };

            var response = _brisInstance.Search(apiKey, body);
            var root = JsonConvert.DeserializeObject<RootObject>(response.ToJson());

            List<BrukerDto> result = root.Results.ToList();
            List<OrdinaryMemberDC> memberList = ConvertMemberListToOrdinaryMemberDc(result, gymCode);

            //File.AppendAllText(@"C:\Exceline\text.txt", "End Search " + Environment.NewLine);
            return memberList;

            #region sample data
            //List<OrdinaryMemberDC> itemList = new List<OrdinaryMemberDC>();
            //OrdinaryMemberDC item = new OrdinaryMemberDC();
            //item.FirstName = "nuwan";
            //item.LastName = "sanjeewa";
            //item.Name = item.FirstName + " " + item.LastName;
            //item.Mobile = "0712203672";
            //item.MobilePrefix = "+47";
            //item.Email = "nuwan@gmail.com";
            //item.UserId = 787877;
            //item.BirthDate = DateTime.Now;
            //item.Gender = Gender.MALE;
            //item.Ssn = "08576883";
            //item.ActiveStatus = true;
            //item.Language = "no";
            //item.School = "bris school";
            //item.LastPaidSemesterFee = "10067";

            //OrdinaryMemberDC item1 = new OrdinaryMemberDC();
            //item1.FirstName = "nuwan1";
            //item1.LastName = "sanjeewa1";
            //item1.Mobile = "07122036721";
            //item1.MobilePrefix = "+47";
            //item1.Email = "nuwan11@gmail.com";
            //item1.UserId = 12123;
            //item1.BirthDate = DateTime.Now;
            //item1.Gender = Gender.MALE;
            //item1.Ssn = "0857688311";
            //item1.ActiveStatus = true;
            //item1.Language = "no";
            //item1.School = "bris school 2";
            //item1.LastPaidSemesterFee = "1006722";

            //itemList.Add(item);
            //itemList.Add(item1);

            //return itemList; 
            #endregion 

        }


        public OrdinaryMemberDC SaveBrisMember(string apiKey, OrdinaryMemberDC member, string gymCode)
        {
            //TODO
            var brukerDto = ConvertOrdinaryMemberDcToBrisMember(member);
            var brukerKlientRequestDto = new BrukerKlientRequestDto(brukerDto);
            _skrivBrukerdataApi = new SkrivBrukerdataApi();
            var response = _skrivBrukerdataApi.UpdateBruker(apiKey, brukerKlientRequestDto);
            var root = JsonConvert.DeserializeObject<BrukerDto>(response.ToJson());
            var result = ConvertBrisMemberToOrdinaryMemberDc(root, gymCode);

            return result;
        }

        private List<OrdinaryMemberDC> ConvertMemberListToOrdinaryMemberDc(List<BrukerDto> brukerDtos, string gymCode)
        {
            //File.AppendAllText(@"C:\Exceline\text.txt", "Convert " + Environment.NewLine);
            var itemList = new List<OrdinaryMemberDC>();
            try
            {

                foreach (var brukerDto in brukerDtos)
                {
                    var item = new OrdinaryMemberDC();
                    item.FirstName = brukerDto.Fornavn;
                    item.LastName = brukerDto.Etternavn;
                    item.Name = item.FirstName + " " + item.LastName;
                    item.Mobile = brukerDto.Mobiltelefon;
                    item.MobilePrefix = brukerDto.Mobilprefiks;
                    item.Email = brukerDto.Epostadresse;
                    item.UserId = brukerDto.BrukerId != null ? (int)brukerDto.BrukerId : -1;
                    if (brukerDto.Fodselsdato != null)
                        item.BirthDate = (DateTime)brukerDto.Fodselsdato;
                    item.Gender = brukerDto.Kjonn == 1 ? Gender.FEMALE : Gender.MALE;

                    // File.AppendAllText(@"C:\Exceline\text.txt", "Lasnd Code" + brukerDto.AdresseDto.Landkode + Environment.NewLine);
                    if (string.IsNullOrEmpty(brukerDto.Mobilprefiks))
                    {
                        if (string.IsNullOrEmpty(brukerDto.AdresseDto.Landkode))
                        {
                            item.MobilePrefix = "+47";
                        }
                        else
                        {
                            item.MobilePrefix = GetCountryCode(brukerDto.AdresseDto.Landkode, gymCode);
                        }
                    }
                    else
                    {
                        if (brukerDto.Mobilprefiks.StartsWith("+"))
                        {
                            item.MobilePrefix = brukerDto.Mobilprefiks;
                        }
                        else
                        {
                            item.MobilePrefix = "+" + brukerDto.Mobilprefiks;
                        }
                    }

                    item.Ssn = brukerDto.Fnr;
                    if (brukerDto.Aktiv != null) item.ActiveStatus = (bool)brukerDto.Aktiv;
                    item.Language = brukerDto.Sprak;
                    item.School = brukerDto.StudiestedNavn;
                    item.LastPaidSemesterFee = brukerDto.SemesteravgiftKode;
                    item.AgressoId = brukerDto.AgressoId;
                    item.Address1 = brukerDto.AdresseDto.Linje1;
                    item.Address2 = brukerDto.AdresseDto.Linje2;
                    item.PostCode = brukerDto.AdresseDto.Postnummer;
                    item.PostPlace = brukerDto.AdresseDto.Poststed;
                    item.CountryId = brukerDto.AdresseDto.Landkode;
                    itemList.Add(item);
                }
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Bris Error " + ex.Message, new Exception(), "Bris");
                //File.AppendAllText(@"C:\Exceline\text.txt", "End Error " + es.Message + Environment.NewLine);
            }
            // File.AppendAllText(@"C:\Exceline\text.txt", "End Convert " + Environment.NewLine);

            return itemList;
        }

        private OrdinaryMemberDC ConvertBrisMemberToOrdinaryMemberDc(BrukerDto brukerDtos, string gymCode)
        {
            var item = new OrdinaryMemberDC();
            item.FirstName = brukerDtos.Fornavn;
            item.LastName = brukerDtos.Etternavn;
            item.Name = item.FirstName + " " + item.LastName;
            item.Mobile = brukerDtos.Mobiltelefon;
            item.MobilePrefix = brukerDtos.Mobilprefiks;
            item.Email = brukerDtos.Epostadresse;
            item.UserId = brukerDtos.BrukerId != null ? (int)brukerDtos.BrukerId : -1;
            if (brukerDtos.Fodselsdato != null)
                item.BirthDate = (DateTime)brukerDtos.Fodselsdato;
            item.Gender = brukerDtos.Kjonn == 1 ? Gender.FEMALE : Gender.MALE;
            if (string.IsNullOrEmpty(brukerDtos.Mobilprefiks))
            {
                if (string.IsNullOrEmpty(brukerDtos.AdresseDto.Landkode))
                {
                    item.MobilePrefix = "+47";
                }
                else
                {
                    item.MobilePrefix = GetCountryCode(brukerDtos.AdresseDto.Landkode, gymCode);
                }
            }
            else
            {
                if (brukerDtos.Mobilprefiks.StartsWith("+"))
                {
                    item.MobilePrefix = brukerDtos.Mobilprefiks;
                }
                else
                {
                    item.MobilePrefix = "+" + brukerDtos.Mobilprefiks;
                }
            }
            item.Ssn = brukerDtos.Fnr;
            if (brukerDtos.Aktiv != null) item.ActiveStatus = (bool)brukerDtos.Aktiv;
            item.Language = brukerDtos.Sprak;
            item.School = brukerDtos.StudiestedNavn;
            item.LastPaidSemesterFee = brukerDtos.SemesteravgiftKode;
            item.AgressoId = brukerDtos.AgressoId;

            return item;
        }

        private BrukerDto ConvertOrdinaryMemberDcToBrisMember(OrdinaryMemberDC member)
        {
            var brukerDto = new BrukerDto();
            brukerDto.Fornavn = member.FirstName;
            brukerDto.Etternavn = member.LastName;
            brukerDto.Mobiltelefon = member.Mobile;
            brukerDto.Mobilprefiks = member.MobilePrefix;
            brukerDto.Kjonn = member.Gender == Gender.FEMALE ? 1 : 0;
            brukerDto.Fodselsdato = member.BirthDate;
            brukerDto.StudiestedNavn = member.School;
            brukerDto.Epostadresse = member.Email; // TODO
            brukerDto.Aktiv = member.ActiveStatus;
            brukerDto.SemesteravgiftKode = member.LastPaidSemesterFee;
            brukerDto.Fnr = member.Ssn;
            brukerDto.AgressoId = member.AgressoId;

            return brukerDto;
        }

        public int GetAggressoID(int brisId, string apiKey)
        {
            if (_iTjenesteResourceApi != null)
            {
                _iTjenesteResourceApi = new TjenesteResourceApi();
            }
            ApiResponse<AgressoOppslagResponse> aggressoResult = _iTjenesteResourceApi.GetAgressoIdWithHttpInfo(apiKey, brisId);
            return aggressoResult.Data.AgressoId.Value;
        }

        public int AddAgressoID(int brisID, string apiKey)
        {
            _iTjenesteResourceApi = new TjenesteResourceApi();
            ApiResponse<AgressoOppslagResponse> aggressoResult = _iTjenesteResourceApi.AddAgressoIdWithHttpInfo(apiKey, brisID);
            if (aggressoResult != null)
            {
                return aggressoResult.Data.AgressoId.Value;
            }
            else
            {
                return 0;
            }
        }

        private string GetCountryCode(string landCode, string gymCode)
        {

            //check the in memory list 
            string countryCode = string.Empty;
            countryCode = _countryCodes.FirstOrDefault(X => X.Key == landCode).Value;
            if (string.IsNullOrEmpty(countryCode))
            {
                countryCode = GMSManageMembership.GetCountryCode(landCode, gymCode).OperationReturnValue;
                _countryCodes.Add(landCode, countryCode);
            }

            if (string.IsNullOrEmpty(countryCode))
            {
                return "+47";
            }
            else
            {
                return countryCode;
            }
        }
    }
}