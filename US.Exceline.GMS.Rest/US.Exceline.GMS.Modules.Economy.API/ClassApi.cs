﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.API
{
    public class ClassApi
    {
        public static OperationResult<List<ClassDetail>> GetClassSalary(List<int> branchIdList, List<int> employeeIdList, List<int> categoryIdList, DateTime fromDate, DateTime toDate, string gymCode)
        {
            return ClassManager.GetClassSalary(branchIdList, employeeIdList, categoryIdList, fromDate, toDate, gymCode);
        }


        public static OperationResult<int> SaveClassFileDetail(string fileName, string filePath, string user, string gymCode)
        {
            return ClassManager.SaveClassFileDetail(fileName, filePath, user, gymCode);
        }

        public static OperationResult<List<ClassFileDetail>> GetClassFileDetails(string gymCode)
        {
            return ClassManager.GetClassFileDetails(gymCode);
        }
    }
}
