﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.API
{
    public class OCR
    {

        public static OperationResult<int> ImportOCRFile(string fileName, string folderPath, string user, string fileType, string gymCode, int loggingLevel)
        {
            OperationResult<int> result = new OperationResult<int>();
            result.OperationReturnValue = -1;    
            int fileId = -1;
            result =  OCRManager.SaveFileLog(fileName, folderPath, user, fileType, gymCode);
            fileId = result.OperationReturnValue;
            if (result.OperationReturnValue > 0)
            {
                OperationResult<OCRImportSummary> importResult = OCRManager.ImportOCRfile(fileName, folderPath, fileType, gymCode);
                if (importResult.ErrorOccured)
                {
                    result.OperationReturnValue = -1;
                    foreach (NotificationMessage message in importResult.Notifications)
                    {
                        if(!result.Notifications.Contains(message))
                           result.CreateMessage(message.Message, message.MessageType);
                    }
                }
                else
                {
                    result.OperationReturnValue = fileId;
                    foreach (NotificationMessage message in importResult.Notifications)
                    {
                        if (!result.Notifications.Contains(message))
                           result.CreateMessage(message.Message, message.MessageType);
                    }
                }

                //Add the history record
                importResult.OperationReturnValue.FileId = fileId;
                OCRManager.AddImportHistory(importResult.OperationReturnValue, user, gymCode);
            }
            else if (result.OperationReturnValue == -2)
            {
                result.CreateMessage("Duplicate file found", MessageTypes.ERROR);
            }

            List<string> messages = new List<string>();
            foreach (NotificationMessage message in result.Notifications)
            {
                if(loggingLevel == 1) // log all
                   messages.Add(message.Message);

                if (loggingLevel == 2) // log only warnings 
                {
                    if(message.MessageType == MessageTypes.WARNING)
                        messages.Add(message.Message);
                }
            }

            OCRManager.AddProcessLog(messages, user, fileId, gymCode);

            return result;
        }

        public static  OperationResult<OCRSummary> GetSummary(string fileName, string user, string folderPath, string gymCode)
        {
            OperationResult<OCRSummary> summaryResult = new OperationResult<OCRSummary>();
            return OCRManager.GetOCRSummary(fileName, folderPath, gymCode);
        }

        public static OperationResult<List<OCRImportSummary>> GetOCRImportHistory(DateTime? startDate, DateTime? endDate, string gymCode)
        {
            return OCRManager.GetOCRImportHistory(startDate, endDate, gymCode);
        }

        public static OperationResult<List<PaymentImportLogDC>> GetPaymentImportProcessLog(DateTime fromDate, DateTime toDate, string gymCode)
        {
            return OCRManager.GetPaymentImportProcessLog(fromDate, toDate, gymCode);
        }
    }
}
