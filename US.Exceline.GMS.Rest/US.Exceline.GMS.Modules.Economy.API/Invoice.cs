﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.API
{
    public class Invoice
    {
        public static OperationResult<List<USPARItem>> AdvanaceInvoiceSearchXML(InvoiceSearchCriteria criteria, SearchModes searchMode, object constValue, string gymCode)
        {
            return USPInvoiceSearchManager.AdvanaceInvoiceSearchXML(criteria, searchMode, constValue, gymCode);
        }

        public static OperationResult<List<USPARItem>> SearchGeneralInvoiceDataXML(string searchValue, int invoiceType, string filedType, SearchModes searchMode, object constValue, string gymCode, int paymentID)
        {
            return USPInvoiceSearchManager.SearchGeneralInvoiceDataXML(searchValue, invoiceType, filedType, searchMode, constValue, gymCode, paymentID);
        }

        public static OperationResult<bool> SendReminders(string GymCode, int branchID)
        {
            return ReminderManager.SendReminders(GymCode, branchID);
        }

        public static OperationResult<string> GenerateInvoicesforBranch(int branchID,  string invoiceKey, string orderType, string user, string gymCode)
        {
            return InvoiceManager.GenerateInvoicesforBranch(branchID, invoiceKey, orderType, user, gymCode);
        }

        public static OperationResult<string> GenerateInvoices(int branchID, DateTime? startDueDate, DateTime? endDueDate, string orderType, string user, string gymCode)
        {
            return InvoiceManager.GenerateInvoices(branchID, startDueDate, endDueDate, orderType, user,gymCode);
        }

        public static OperationResult<List<EstimatedInvoiceDetails>> GetEstimatedInvoiceDetails(List<int> gymIds, DateTime? startDueDate, DateTime? endDueDate, string gymCode)
        {
            return InvoiceManager.GetEstimatedInvoiceDetails(gymIds, startDueDate, endDueDate, gymCode);
        }

        public static OperationResult<List<EstimatedInvoiceDetails>> GetInvoicedAmounts(List<int> gymIds, DateTime? startDueDate, DateTime? endDueDate, string gymCode)
        {
            return InvoiceManager.GetInvoicedAmounts(gymIds, startDueDate, endDueDate, gymCode);
        }

        public static OperationResult<int> AddCreditorOrderLine(CreditorOrderLine OrderLine, string user, DbTransaction transaction)
        {
            return InvoiceManager.AddCreditorOrderLine(OrderLine, user, transaction);
        }

        public static OperationResult<List<IUSPClaim>> SelectedOrderLineInvoiceGenerator(List<int> creditorOrdeLineIdList, string invoiceKey, string orderType, string user, DbTransaction transaction, int installmentID, int creditorNo, string gymCode)
        {
            return InvoiceManager.SelectedOrderLineInvoiceGenerator(creditorOrdeLineIdList, invoiceKey, orderType, user, transaction, installmentID, creditorNo, gymCode);
        }

        public static OperationResult<List<InvoiceDC>> GetInvoiceList(string category, DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            return InvoiceManager.GetInvoiceList(category, fromDate, toDate, branchId, gymCode);
        }

        public static bool UpdatePDFFileParth(int id, string fileParth, int branchId, string gymCode, string user)
        {
            return InvoiceManager.UpdatePDFFileParth(id, fileParth, branchId, gymCode, user);
        }

        public static string MergePDFFile(List<string> pdfPathList, string newPDFPath)
        {
            try
            {
                return InvoiceManager.MergePDFFile(pdfPathList, newPDFPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateHistoryPDFPrintProcess(int id, int invoiceCount, int FailCount, int branchId, string user, string gymCode, string filePath)
        {
            try
            {
                return InvoiceManager.UpdateHistoryPDFPrintProcess(id, invoiceCount, FailCount, branchId, user, gymCode, filePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<HistoryPDFPrint> GetHistoryPDFPrintDetail(DateTime fromDate, DateTime toDate, string gymCode)
        {
            return InvoiceManager.GetHistoryPDFPrintDetail(fromDate, toDate, gymCode);
        }

        public static int UpdateBulkPDFPrintDetails(int id, string docType, string dataIdList, string pdfFilePath, int noOfDoc, int failCount, int branchId, string user, string gymCode)
        {
            try
            {
                return InvoiceManager.UpdateBulkPDFPrintDetails(id, docType, dataIdList, pdfFilePath, noOfDoc, failCount, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BulkPDFPrint> GetBulkPDFPrintDetailList(string type, DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            try
            {
                return InvoiceManager.GetBulkPDFPrintDetailList(type, fromDate, toDate, branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
