﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Login;
using System.Data;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Login.Data.DataAdapters.SQLServer.Command
{
    public class AnonymousUserPasswordResetAction : USDBActionBase<ExcePWRecDC>
    {
        private String _gymCode = string.Empty;
        private String _potentialUserName = string.Empty;
        public AnonymousUserPasswordResetAction(string gymCode, string potentialUserName)
        {
            _gymCode = gymCode;
            _potentialUserName = potentialUserName;
        }


        protected override ExcePWRecDC Body(DbConnection connection)
        {
            ExcePWRecDC excePWRec = new ExcePWRecDC();
            try
            {
                const string storedProcedure = "dbo.USExceGMSManageLoginAnonymousUserPasswordReset";
                DbCommand command = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                command.Parameters.Add(DataAcessUtils.CreateParam("@GymCode", DbType.String, _gymCode));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _potentialUserName));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    excePWRec.ReturnCode = Convert.ToInt32(reader["returnCode"]);
                    excePWRec.UserName = reader["UserName"].ToString();
                    excePWRec.UserEmail =reader["userEmail"].ToString();
                    excePWRec.HashBase = reader["baseOutput"].ToString();
                }
                string unEscBaseOutput;
                while ((unEscBaseOutput = Uri.UnescapeDataString(excePWRec.HashBase)) != excePWRec.HashBase)
                {
                    excePWRec.HashBase = unEscBaseOutput;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return excePWRec;
        }
    }


}
