﻿using System;
using System.Data;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Login;

namespace US.Exceline.GMS.Modules.Login.Data.DataAdapters.SQLServer.Command
{
    public class GetGymLicensAction : USDBActionBase<ExecUserDC>
    {
        private ExecUserDC _execUser;

        public GetGymLicensAction(ExecUserDC execUser,string userName)
        {
            _execUser = execUser;
            OverwriteUser(userName);
        }

        protected override ExecUserDC Body(System.Data.Common.DbConnection connection)
        {
            const string storedProcedure = "UsExceGMSGetLicenseSettings";
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    _execUser.MyWelliness = Convert.ToBoolean(reader["MyWellness"].ToString());
                    _execUser.Exor = Convert.ToBoolean(reader["Exor"].ToString());
                    _execUser.MyWorkout = Convert.ToBoolean(reader["Myworkout"].ToString());
                    _execUser.Inforgym = Convert.ToBoolean(reader["Infogym"].ToString());
                    _execUser.BoostCommunication = Convert.ToBoolean(reader["BoostCommunication"].ToString());
                    _execUser.Tripletex = Convert.ToBoolean(reader["Tripletex"].ToString());
                    _execUser.Visma = Convert.ToBoolean(reader["Visma"].ToString());
                    _execUser.UniMicro = Convert.ToBoolean(reader["UniMicro"].ToString());
                    _execUser.DocumentScanner = Convert.ToBoolean(reader["DocumentScanner"].ToString());
                    _execUser.BankTerminalIntegrated = Convert.ToBoolean(reader["BankTerminalIntegrated"].ToString());
                    _execUser.ReceiptPrinter = Convert.ToBoolean(reader["ReceiptPrinter"].ToString());
                    _execUser.NumberOfActivites = Convert.ToInt32(reader["NumberOfActivites"].ToString());
                    _execUser.FollowUp = Convert.ToBoolean(reader["FollowUp"].ToString());
                    _execUser.ExcelineAccess = Convert.ToBoolean(reader["ExcelineAccess"].ToString());
                    _execUser.GantnerAccess = Convert.ToBoolean(reader["GantnerAccess"].ToString());
                    _execUser.GantnerVending = Convert.ToBoolean(reader["GantnerVending"].ToString());
                    _execUser.GantnerTime = Convert.ToBoolean(reader["GantnerTime"].ToString());
                    _execUser.Recruitment = Convert.ToBoolean(reader["Recruitment"].ToString());
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _execUser;
        }
    }
}
