﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using US.Common.Web.UI.Core.SystemObjects;
using US.Exceline.GMS.IBooking.Data.DataAdapters;
using US.Exceline.GMS.Modules.Operations.Data.DataAdapters.ManageOperations;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations
{
    public class OperationsManager
    {
        public static OperationResult<List<ExcACCCommand>> GetCommands(int branchId, string gymCode)
        {
            OperationResult<List<ExcACCCommand>> result = new OperationResult<List<ExcACCCommand>>();
            try
            {
                result.OperationReturnValue = ManageOperationsFacade.GetCommands(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting commands " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<ExceACCSettings> GetSettings(int branchId, string gymCode)
        {
            OperationResult<ExceACCSettings> result = new OperationResult<ExceACCSettings>();
            try
            {
                result.OperationReturnValue = ManageOperationsFacade.GetSettings(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting settings " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExceACCAccessProfileTime>> GetAccessTimes(int branchId, string gymCode, string dateformat)
        {
            OperationResult<List<ExceACCAccessProfileTime>> result = new OperationResult<List<ExceACCAccessProfileTime>>();
            try
            {
                result.OperationReturnValue = ManageOperationsFacade.GetAccessTimes(branchId, gymCode, dateformat);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting access times " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

     

        public static OperationResult<List<GymACCOpenTime>> GetOpenTimes(int branchId, string gymCode)
        {
            OperationResult<List<GymACCOpenTime>> result = new OperationResult<List<GymACCOpenTime>>();
            try
            {
                result.OperationReturnValue = ManageOperationsFacade.GetOpenTimes(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting open times " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExceACCGymClosedTime>> GetClosedTimes(int branchId, string gymCode)
        {
            OperationResult<List<ExceACCGymClosedTime>> result = new OperationResult<List<ExceACCGymClosedTime>>();
            try
            {
                result.OperationReturnValue = ManageOperationsFacade.GetClosedTimes(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting closed times " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExceACCMember>> GetMembers(int branchId, string gymCode)
        {
            OperationResult<List<ExceACCMember>> result = new OperationResult<List<ExceACCMember>>();
            try
            {
                result.OperationReturnValue = ManageOperationsFacade.GetMembers(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting members " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> GetImage(int branchId, string gymCode, string customerNo)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = ManageOperationsFacade.GetImage(branchId, gymCode,customerNo);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting image " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> GetARXMembers(int branchId, string gymCode, bool isAllMember)
        {

            int systemID = GetSystemId(gymCode).OperationReturnValue;

            OperationResult<string> result = new OperationResult<string>();
            string xmlString = string.Empty;
            try
            {
                List<person> members = new List<person>();
                members = ManageOperationsFacade.GetARXMembers(branchId, gymCode, isAllMember);
                var groupMember = members.GroupBy(state => state.id).ToList();

                XDocument doc = new XDocument();
                doc.Declaration = new XDeclaration("1.0", "ISO-8859-1", "yes");

                XElement arxDataRoot = new XElement("arxdata");
                doc.Add(arxDataRoot);


                foreach (var item in groupMember)
                {

                    var cardList = members.Where(x => item.Key != null && x.id == item.Key.ToString()).ToList();
                    var pr = members.FirstOrDefault(x => item.Key != null && x.id == item.Key.ToString());

                    XElement persons = new XElement("persons");
                    XElement person = new XElement("person");
                    XElement id = new XElement("id", systemID.ToString() + "_" + pr.BranchID.ToString() + "_" + pr.id);
                    XElement fName = new XElement("first_name", pr.first_name);
                    XElement lName = new XElement("last_name", pr.last_name);
                    XElement pCode = new XElement("pin_code", pr.PinCode);

                    person.Add(id, fName, lName, pCode);

                    XElement accessCategories = new XElement("access_categories");
                    XElement accessCategory = new XElement("access_category");

                    if (string.IsNullOrEmpty(pr.AccessProfileName))
                    {
                        XElement accessName = new XElement("name", pr.BranchName.Trim());
                        accessCategory.Add(accessName);
                    }
                    else
                    {
                        XElement accessName = new XElement("name", pr.AccessProfileName);
                        accessCategory.Add(accessName);
                    }

                    XElement endadate = new XElement("end_date", pr.EndDate.Trim() + " 23:59:00");
                    accessCategory.Add(endadate);

                    accessCategories.Add(accessCategory);
                    person.Add(accessCategories);
                    persons.Add(person);
                    arxDataRoot.Add(persons);

                    XElement cards = new XElement("cards");

                    foreach (var c in cardList)
                    {
                        XElement card = new XElement("card");
                        XElement number = new XElement("number", c.CardNo);
                        XElement personid = new XElement("person_id", systemID.ToString() + "_" + pr.BranchID.ToString() + "_" + pr.id);

                        if (c.Deleted)
                        {
                            XElement formatname = new XElement("format_name", c.FormatName);
                            XElement deleted = new XElement("DELETED");
                            card.Add(number, personid, formatname, deleted);
                        }
                        else
                        {
                            XElement formatname = new XElement("format_name", c.FormatName);
                            card.Add(number, personid, formatname);
                        }

                        //if (pr.FormatName == "deleted")
                        //{
                        //    XElement formatname = new XElement("format_name", "Tastatur");
                        //    XElement deleted = new XElement("DELETED");
                        //    card.Add(number, personid, formatname, deleted);
                        //}
                        //else
                        //{
                        //    XElement formatname = new XElement("format_name", pr.FormatName);
                        //    card.Add(number, personid, formatname);
                        //}

                        cards.Add(card);
                    }

                    arxDataRoot.Add(cards);
                }


                //foreach (person pr in members)
                //{
                //    XElement persons = new XElement("persons");
                //    XElement person = new XElement("person");
                //    XElement id = new XElement("id", systemID.ToString() + "_" + pr.BranchID.ToString() + "_" + pr.id);
                //    XElement fName = new XElement("first_name", pr.first_name);
                //    XElement lName = new XElement("last_name", pr.last_name);
                //    XElement pCode = new XElement("pin_code", pr.PinCode);

                //    person.Add(id, fName, lName, pCode);

                //    XElement accessCategories = new XElement("access_categories");
                //    XElement accessCategory = new XElement("access_category");

                //    if (string.IsNullOrEmpty(pr.AccessProfileName))
                //    {
                //        XElement accessName = new XElement("name", pr.BranchName.Trim());
                //        accessCategory.Add(accessName);
                //    }
                //    else
                //    {
                //        XElement accessName = new XElement("name", pr.AccessProfileName);
                //        accessCategory.Add(accessName);
                //    }

                //    XElement endadate = new XElement("end_date", pr.EndDate.Trim() + " 23:59:00");
                //    accessCategory.Add(endadate);

                //    accessCategories.Add(accessCategory);
                //    person.Add(accessCategories);
                //    persons.Add(person);
                //    arxDataRoot.Add(persons);

                //    XElement cards = new XElement("cards");

                //    foreach (var c in pr.CardList)
                //    {
                //        XElement card = new XElement("card");
                //        XElement number = new XElement("number", c.number);
                //        XElement personid = new XElement("person_id", systemID.ToString() + "_" + pr.BranchID.ToString() + "_" + pr.id);

                //        if (c.IsDeleted)
                //        {
                //            XElement formatname = new XElement("format_name", c.format_name);
                //            XElement deleted = new XElement("DELETED");
                //            card.Add(number, personid, formatname, deleted);
                //        }
                //        else
                //        {
                //            XElement formatname = new XElement("format_name", c.format_name);
                //            card.Add(number, personid, formatname);
                //        }

                //        //if (pr.FormatName == "deleted")
                //        //{
                //        //    XElement formatname = new XElement("format_name", "Tastatur");
                //        //    XElement deleted = new XElement("DELETED");
                //        //    card.Add(number, personid, formatname, deleted);
                //        //}
                //        //else
                //        //{
                //        //    XElement formatname = new XElement("format_name", pr.FormatName);
                //        //    card.Add(number, personid, formatname);
                //        //}

                //        cards.Add(card);
                //    }

                    
                //    arxDataRoot.Add(cards);
                //}
                result.OperationReturnValue = doc.ToString();
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting arx data " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> GetSystemId(string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ManageOperationsFacade.GetSystemId(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting system id " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> AddVisitList(string gymCode, List<EntityVisitDC> entityVisitLst)
        {
            OperationResult<int> result = new OperationResult<int>();
            foreach (var entityVisitItem in entityVisitLst)
            {
                AddVisit(gymCode, entityVisitItem);
            }
            result.OperationReturnValue = ManageOperationsFacade.AddVisitList(gymCode, entityVisitLst);
            return result;
        }

        public static OperationResult<int> AddVisit(string gymCode, EntityVisitDC visit)
        {
            ContractSummaryDC selectedContract = null;
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                DateTime visitDate = Convert.ToDateTime(visit.InTime);// DateTime.ParseExact(visit.VisitDateTime, "yyyy-MM-dd hh:mm:ss,fff", CultureInfo.InvariantCulture);

                List<ContractSummaryDC> contractSummaries = ExcelineIBookingFacade.GetContractSummaries(visit.CutomerNo, gymCode);
                foreach (ContractSummaryDC contract in contractSummaries)
                {
                    if (contract.AccessProfileId > 0)
                    {
                        if (!contract.IsFreezed && contract.StartDate <= visitDate && contract.EndDate >= visitDate)
                        {
                            ContractAccessTimeDC accessTime = GetAccessTime(contract, visitDate);
                            if (accessTime != null)
                            {
                                if ((visitDate.TimeOfDay >= accessTime.InTime.Value.TimeOfDay) && (visitDate.TimeOfDay < accessTime.OutTime.Value.TimeOfDay))
                                {
                                    selectedContract = contract;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (selectedContract == null)
                {
                    foreach (ContractSummaryDC contract in contractSummaries)
                    {
                        if (contract.AccessProfileId == 0 || contract.AccessProfileId < 0)
                        {
                            if (!contract.IsFreezed && contract.StartDate <= visitDate && contract.EndDate >= visitDate)
                            {
                                        selectedContract = contract;
                                        break;
                                    }
                                }
                            }
                 }
                if (selectedContract != null)
                {
                    if (selectedContract.ContractTypeCode == "PUNCHCARD")
                    {
                        if (selectedContract.RemainingVisits > 0)
                        {
                            visit.VisitDate = visitDate;
                            visit.MemberContractId = selectedContract.Id;
                            visit.ItemName = selectedContract.TemplateName;
                            visit.ActivityId = selectedContract.ActivityId;
                            visit.VisitType = "CON";
                            visit.CountVist = true;
                        }
                    }
                    else
                    {
                        visit.VisitDate = visitDate;
                        visit.MemberContractId = selectedContract.Id;
                        visit.ItemName = selectedContract.TemplateName;
                        visit.ActivityId = selectedContract.ActivityId;
                        visit.VisitType = "CON";
                        visit.CountVist = true;
                    }
                }
                else
                {
                    visit.VisitDate = visitDate;
                    visit.VisitType = "IB";
                    visit.CountVist = true;
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating member visit - IBooking, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        private static ContractAccessTimeDC GetAccessTime(ContractSummaryDC contract, DateTime intime)
        {
            switch (intime.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Monday");

                case DayOfWeek.Tuesday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Tuesday");

                case DayOfWeek.Wednesday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Wednesday");

                case DayOfWeek.Thursday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Thursday");

                case DayOfWeek.Friday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Friday");

                case DayOfWeek.Saturday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Saturday");

                case DayOfWeek.Sunday:
                    return contract.AccesTimes.FirstOrDefault(x => x.Day == "Sunday");
            }
            return null;
        }

        public static OperationResult<int> RegisterControl(ExcACCAccessControl accessControl, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            result.OperationReturnValue = ManageOperationsFacade.RegisterControl(accessControl, gymCode);
            return result;
        }

        public static OperationResult<int> PingServer(int terminalId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            result.OperationReturnValue = ManageOperationsFacade.PingServer(terminalId, gymCode);
            return result;
        }

        public static OperationResult<int> AddEventLogs(ExcAccessEvent accessEvent, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            result.OperationReturnValue = ManageOperationsFacade.AddEventLogs(accessEvent, gymCode);
            return result;
        }

        public static OperationResult<ExceACCTerminal> GetTerminalDetails(int terminalID, string gymCode)
        {
            OperationResult<ExceACCTerminal> result = new OperationResult<ExceACCTerminal>();
            result.OperationReturnValue = ManageOperationsFacade.GetTerminalDetails(terminalID, gymCode);
            return result;
        }

        public static OperationResult<int> GetAvailableVisitByContractId(int contractId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            result.OperationReturnValue = ManageOperationsFacade.GetAvailableVisitByContractId(contractId, gymCode);
            return result;
           
        }

     
    }
}
