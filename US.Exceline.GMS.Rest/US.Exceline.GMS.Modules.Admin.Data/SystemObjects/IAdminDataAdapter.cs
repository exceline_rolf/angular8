﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.HolydayManagement;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.DomainObjects.Admin.ManageAnonymizing;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Admin.Data.SystemObjects
{
    interface IAdminDataAdapter
    {
        #region manage intructor
        List<InstructorDC> GetInstructors(int branchId, string user, bool isActive, int instructorId, string gymCode);
        #endregion

        #region manage trainer
        List<TrainerDC> GetTrainers(int branchId, string searchText, bool isActive, int trainerId, string gymCode);
        #endregion

        #region manage employee
        string SaveGymEmployee(GymEmployeeDC gymEmployee, string gymCode);
        int UpdateGymEmployee(GymEmployeeDC gymEmployee, int branchId, string user, string gymCode);
        //bool DeleteGymEmployee(int gymEmployeeId, int branchId, string user, string gymCode);
        bool DeActivateGymEmployee(int gmyEmployeeId, int branchId, string user, string gymCode);
        List<GymEmployeeDC> GetGymEmployees(int branchId, string searchText, bool isActive, int roleId, string gymCode);
        GymEmployeeDC GetGymEmployeeById(int branchId, int employeeId, string gymCode);
        bool ValidateEmployeeFollowUp(int employeeId, DateTime endDate, string gymCode);
        bool SaveEmployeesRoles(EmployeeDC employeeDc, string gymCode);
        List<RoleActivityDC> GetActivitiesForRoles(int memberId, string gymCode);
        GymEmployeeDC GetGymEmployeeByEmployeeId(int branchId, string user, int employeeId, string gymCode);
        bool DeleteGymEmployee(int gmyEmployeeId, int assignEmpId, string user, string gymCode);
        List<GymEmployeeRoleDurationDC> GetGymEmployeeRoleDuration(int employeeId, string gymCode);
        List<GymEmployeeWorkPlanDC> GetGymEmployeeWorkPlan(int employeeId, string gymCode);
        List<GymEmployeeApprovalDC> GetGymEmployeeApprovals(int employeeId, string approvalType, string gymCode);
        bool AddEmployeeTimeEntry(EmployeeTimeEntryDC timeEntry, string gymCode);
        List<EmployeeTimeEntryDC> GetEmployeeTimeEntries(int employeeId, string gymCode);
        bool ApproveEmployeeTimes(int employeeId, bool isAllApproved, string empIdList, string gymCode);
        int SaveGymEmployeeWork(ScheduleItemDC scheduleItem, int resourceId, int branchId, string gymCode);
        int SaveWorkActiveTime(GymEmployeeWorkPlanDC work, int branchId, string gymCode);
        int SaveWorkItem(GymEmployeeWorkDC work, int branchId, string gymCode);
        int AdminApproveEmployeeWork(GymEmployeeWorkDC work, bool isApproved, string user, string gymCode);
        bool DeleteGymEmployeeActiveTime(int activeTimeId, string gymCode);
        List<EmployeeClass> GetEmployeeClasses(int empId, string gymCode);
        List<DateTime> GetClassDateByEmployee(int scheduleItemId, string gymCode);
        int UpdateApproveStatus(bool IsApproved, int EntityActiveTimeID, string user);
        List<EmployeeBookingDC> GetEmployeeBookings(int empId, string gymCode);
        List<ExcelineRoleDc> GetGymEmployeeRolesById(int employeeId, int branchId, string user);
        
        
        #endregion

        #region manageentity
        List<ExcelineTaskDC> GetEntityTasks(int entityId, int branchId, string entityRoleType, string gymCode);
        bool SaveEntityTimeEntry(TimeEntryDC timeEntry, string gymCode);
        List<TimeEntryDC> GetEntityTimeEntries(DateTime startTime, DateTime endTime, int entityId, string entityRoleType, string gymCode);
        #endregion

        #region manage Resources
        int UpdateContractVisits(int selectedContractId, int punchedContractId, int bookingId, string gymCode, string user);
        List<ResourceBookingElemnt> GetResourceBooking(int scheduleId, string _startDateTime, string _endDateTime, string gymCode, string user);
        List<ContractInfo> GetPunchcardContractsOnMember(int memberId, string activity, string gymCode, string user);
        int IsArticlePunchcard(int articleId, string gymCode, string user);
        int SaveResources(ResourceDC resource, string user, string gymCode);
        bool UpdateResource(ResourceDC resource, string user, string gymCode);
        bool DeleteResource(int branchId, string user, string gymCode);
        List<ScheduleItemDC> GetBookingsOnDeletedScheduleItem(int resourceId, string gymCode); 
        bool DeActivateResource(int resourceId, int branchId, string user, string gymCode);
        bool SwitchResource(int resourceTd1, int resourceId2, DateTime startDate, DateTime endDate, string comment, int branchId, string user, string gymCode);

        bool SwitchResourceUndo(int resId, DateTime startDate, DateTime endDate, string user, int branchId,
                                string gymCode);
        int ChangeResourceOnBooking(int scheduleItemId, int resourceId, string comment, string gymCode, string user, int branchId);
        bool ValidateScheduleWithResourceId(int resourceId, DateTime startDate, DateTime endDate, int branchId, string user, string gymCode);
        bool ValidateScheduleWithSwitchResource(int resourceId1, int resourceId2, DateTime startDate, DateTime endDate, int branchId, string user, string gymCode);

        List<ResourceDC> GetResources(int branchId, string searchText, int categoryId, int activityId, int equipmentid,
                                      bool isActive, string gymCode);

        ResourceDC GetResourceDetailsById(int resourceId, string gymCode);

        List<ResourceDC> GetResourceCalender(int branchId, string gymCode, int hit);
        List<ResourceActiveTimeDC> GetResourceActiveTimes(int branchId, DateTime startTime, DateTime endTime, int entNO, string gymCode);
        List<AvailableResourcesDC> GetAvailableResources(int branchId, int resId, string gymCode);
        List<AvailableResourcesDC> GetResourceAvailableTimeById(int branchId, int resId, string gymCode);
        List<string> ValidateResourcesWithSchedule(string roleType, List<AvailableResourcesDC> resourceList, int branchId, string gymCode);
        bool AddOrUpdateAvailableResources(List<AvailableResourcesDC> resourceList, int branchId, string user);

        #endregion

        #region manage Contract
        int SaveContract(PackageDC resource, string user, string gymCode);
        int UpdateContractPriority(Dictionary<int, int> packages, string user, string gymCode);
        bool UpdateContract(PackageDC resource, string user);
        bool DeActivateContract(int resourceId, int branchId, string user);
        List<PackageDC> GetContracts(int branchId, string searchText, int searchType, string searchCriteria, string gymCode);
        string GetNextContractId(int branchId, string gymCode);
        int DeleteContract(int contractId, string gymCode);
        List<PriceItemTypeDC> GetPriceItemTypes(int branchId, string gymCode);
        #endregion

        #region Contract Item
        bool AddContractItem(ContractItemDC contractItem, string user, int branchId, string gymCode);
        bool RemoveContractItem(int contractItem, string user, int branchId, string gymCode);
        List<ContractItemDC> GetContractItems(int branchId, string gymCode);
        #endregion

        #region manage common
        string UpdateEmployees(EmployeeDC employeeDc, string user, string gymCode);
        string UpdateMemeberActiveness(int trainerId, string rolId, int branchId, string user, bool isActive, string gymCode);
        bool DeleteEntityTimeEntry(int timeEntryId, int taskId, decimal timeSpent, string gymCode);
        List<CategoryDC> GetTaskStatusCategories(int branchId, string gymCode);
        #endregion

        #region Branch
        int SaveBranchDetails(ExcelineBranchDC branch, string gymCode);
        bool ValidateCreditorNo(int branchId, string gymCode, string creditorNo);
        List<ExcelineCreditorGroupDC> GetBranchGroups(string gymCode);
        List<ExcelineBranchDC> GetBranches(string gymCode, string userName, int branchId);
        int UpdateBranchDetails(ExcelineBranchDC branch, string gymCode);
        #endregion

        #region Manage TimeEntries
        List<TimeEntryDC> GetTimeEntryForAdminView(DateTime startDate, DateTime endDate, string gymCode);
        #endregion

        #region Member
        List<EntityVisitDC> GetMemberVisits(DateTime startDate, DateTime endDate, int branchId, string gymCode);
        List<string> GetAccessPoints(string gymCode);
        #endregion

        #region Holidays
        List<HolidayScheduleDC> GetHolidays(int branchId, string holidayType, DateTime startDate, DateTime endDate, string gymCode);
        bool AddNewHoliday(HolidayScheduleDC newHoliday, string gymCode);
        int GetHolidayCategoryId(string categoryName, string gymCode);
        bool UpdateHolidayStatus(HolidayScheduleDC updateHoliday, string gymCode);
        #endregion

        #region Category
        int AddActivitySettings(ActivitySettingDC activitySetting, string gymCode, string user);
        #endregion

        #region Manage Inventory Discount
        bool AddDiscount(ShopDiscountDC newDiscount, string gymCode);
        bool UpdateDiscount(ShopDiscountDC updateDiscount, string gymCode);
        List<ShopDiscountDC> GetDiscountsList(int branchId, bool isActive, string gymCode);
        List<ArticleDC> GetInventoryList(int branchId, string gymCode);
        bool DeleteDiscount(int discountId, string discountType, string gymCode);

        # endregion

        #region Revenue Account
        List<AccountDC> GetRevenueAccounts(string gymCode);
        int AddUpdateRevenueAccounts(AccountDC revenueAccountDetail, string user, string gymCode);
        int DeleteRevenueAccounts(AccountDC revenueAccountDetail, string user, string gymCode);
        #endregion

        #region System Setting

        List<VatCodeDC> GetVatCode(string gymCode);
        int AddUpdateVatCodes(VatCodeDC vatCodeDetail, string user, string gymCode);

        SystemSettingOtherSettingDC GetOtherSettings(string gymCode);
        int AddUpdateOtherSettings(SystemSettingOtherSettingDC otherSettingsDetail, string user, string gymCode);
        List<string> GetClassKeyword(string gymCode);
                
        SystemSettingBasicInfoDC GetSystemSettingBasicInfo(string gymCode);
        SystemSettingEconomySettingDC GetSystemSettingEconomySetting(string gymCode);
        List<ContractCondition> GetSystemSettingContractConditions(string gymCode);

        int AddUpdateSystemSettingContractCondition(ContractCondition SystemSettingContractCondition, string user, string gymCode);
        int AddUpdateSystemSettingBasicInfo(SystemSettingBasicInfoDC SystemSettingBasicInfo, string user, string gymCode);
        int AddUpdateSystemSettingEconomySetting(SystemSettingEconomySettingDC SystemSettingEconomySetting, string user, string gymCode);

        #endregion

        decimal SaveStock(ArticleDC article,int branchId, string gymCode, string user);
        int DeleteArticle(int articleId, int branchId, string gymCode, string user,bool isAdminUser);
        List<ActivityTimeDC> GetActivityUnavailableTimes(int branchId, string gymCode);
        bool AddActivityUnavailableTimes(List<ActivityTimeDC> activityTimes, string gymCode);
        int SaveInventory(InventoryItemDC inventoryItem, string gymCode, string user);
        List<InventoryItemDC> GetInventory(int branchId, string gymCode);
        int DeleteInventory(int inventoryId, string gymCode);
        List<ArticleDC> GetInventoryDetail(int branchId, int inventoryId, string gymCode);
        decimal SaveInventoryDetail(List<ArticleDC> articleList, int inventoryId, string gymCode);
        Dictionary<decimal, decimal> GetNetValueForArticle(int inventoryId, int articleId, int counted, int branchId, string gymCode);
        int SaveScheduleItem(ScheduleItemDC scheduleItem, int resId, int branchId, string gymCode);
        ScheduleDC GetResourceScheduleItems(int resId, string roleTpye, string gymCode);
        List<ScheduleDC> GetResourceCalenderScheduleItems(List<int> resIdList, string roleTpye, string gymCode);
        List<EntityActiveTimeDC> GetMemberBookingDetails(int branchId, int scheduleItemId, string gymCode);
        int DeleteResourcesScheduleItem(int scheduleItemId, string gymCode);
        bool ValidateScheduleWithPaidBooking(int scheduleItemId, int resourceId, string gymCode);
        int CheckDuplicateResource(int sourceId, int destinationId, DateTime startDate, DateTime endDate, string gymCode);

        #region followUp

        bool AddFollowUpTemplate(FollowUpTemplateDC followUpTemplate, string gymCode, int branchId);
        List<FollowUpTemplateDC> GetFollowUpTemplates(string gymCode, int branchId);
        bool DeleteFollowUpTemplate(int followUpId, string gymCode);
        List<FollowUpTemplateTaskDC> GetTaskCategoryByFollowUpTemplateId(int templateId, string gymCode);

        #endregion

        #region Manage Jobs
        int SaveExcelineJob(ScheduleItemDC scheduleItem, int branchId, string gymCode);
        List<ScheduleItemDC> GetJobScheduleItems(int branchId, string gymCode);
        int DeleteScheduleItem(List<int> scheduleItemIds, string gymCode);
        int AssignTaskToEmployee(ExcelineCommonTaskDC commonTask, int employeeId, string gymCode, string user);
        #endregion

        #region Manage Employee Calendar
        List<ExcelineCommonTaskDC> GetAllTasksByEmployeeId(int branchId, int employeeId, string gymCode, string user);

        List<ExcelineCommonTaskDC> GetFollowUpByEmployeeId(int branchId, int employeeId, string gymCode, string user,
                                                           int hit);

        List<ExcelineCommonTaskDC> GetJobsByEmployeeId(int branchId, int employeeId, string gymCode, string user,
                                                       int hit);
        #endregion

        int SaveResourceActiveTimePunchard(EntityActiveTimeDC activeTimeItem, int contractId, string gymCode, int branchId, string user);
        int SaveResourceActiveTimeRepeatable(List<EntityActiveTimeDC> activeTimeItemList, string gymCode, string user);
        int SaveResourceActiveTimeRepeatablePunchcard(List<EntityActiveTimeDC> activeTimeItemList, int contractId, string gymCode, string user);
        int SaveResourceActiveTime(EntityActiveTimeDC activeTimeItem, string gymCode, int branchId, string user);
        string CheckActiveTimeOverlap(EntityActiveTimeDC activeTimeItem, string gymCode);
        int DeleteResourceActiveTime(int activeTimeId,string articleName,DateTime visitDateTime, string roleType, bool isArrived, bool isPaid,List<int> memberList, string gymCode);
        List<ArticleDC> GetArticleForResouceBooking(int activityId, int branchId,string gymCode);
        List<ArticleDC> GetArticleForResouce(int activityId, int branchId, string gymCode);
        Dictionary<string, List<int>> CheckUnusedCancelBooking(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime bookingDate, string gymCode);
        int UnavailableResourceBooking(int scheduleItemId, int activeTimeId, DateTime startDate, DateTime endDate,
                                       string gymCode, string user);

        List<EmployeeEventDC> GetEmployeeEvents(int employeeNumber, string gymCode);
        List<EmployeeJobDC> GetEmployeeJobs(int employeeNumber, string gymCode);
        List<ExcelineMemberDC> GetSponsorsforSponsoring(int branchId, DateTime startDate, DateTime endDate, string gymCode);
        bool GenerateSponsorOrders(SponsorShipGenerationDetailsDC sponsorDetails, string gymCode, string user);
        bool GetSponsoringProcessStatus(string guiID, string gymCode, string user);
        Dictionary<string, List<int>> ValidatePunchcardContractMember(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime startDate, string gymCode);

        int SavePaymentBookingMember(int activetimeId, int articleId, int memberId, bool paid, int aritemNo, decimal amount,string paymentType,
                                     string gymCode);

        List<int> GetEmpolyeeForResources(List<int> resIdList, string gymCode);

        List<int> ValidateResWithSwitchDataRange(List<int> resourceIds, DateTime startDate, DateTime endDate,
                                            string gymCode);

        CategoryDC GetCategoryByCode(string categoryCode, string gymCode);

        #region Manage Anonymizing Data
        int DeleteAnonymizingData(ExceAnonymizingDC anonymizing,string user, int branchId, string gymCode);
        List<ExceAnonymizingDC> GetAnonymizingData(string gymCode);

        #endregion

        List<ExcACCAccessControl> GetExcAccessControlList(int branchId, string gymCode);

        ExceACCMember GetLastAccMember(string gymCode, int branchId, int accTerminalId);

        string GetResourceBookingViewMode(string user, string gymCode);
        bool ValidateArticleByActivityId(int activityId, int articleId, string gymCode);

        List<ExceArxFormatDetail> GetArxSettingDetail(int branchId, string gymCode);
        List<ExceArxFormatType> GetArxFormatType(string gymCode);
        int SaveArxSettingDetail(ExceArxFormatDetail arxFormatDetail, int tempCodeDeleteDate, int branchId, string user, string gymCode, string accessContolTypes);
        List<ContractTemplateDC> ValidateContractConditionWithTemplate(int contractConditionId, string gymCode);
        int ValidateContractConditionWithContracts(int contractConditionId, string gymCode);


    }
}
