// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.


[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "item", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.GetGymCompanyOtherSettingsAction.#Body(System.Data.Common.DbConnection)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "obj", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.AddOrUpdateAvailableResourcesAction.#Body(System.Data.Common.DbConnection)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "ex", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.DeleteInventoryAction.#Body(System.Data.Common.DbConnection)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.DeleteResourceActiveTimeAction.#_isArrived")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.DeleteResourceActiveTimeAction.#_isPaid")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.GetActivityUnavailableTimeAction.#_isFromAceedControl")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.GetEmployeeClassesAction.#_gymCode")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.GetResourceAction.#_branchId")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.GetResourceAction.#_name")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.GetResourceAction.#_isActive")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.GetResourceAction.#_categoryId")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.GetResourceAction.#_gymCode")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.GetResourceScheduleItemsAction.#_roleTpye")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.SaveExcelineJobAction.#_dataTableEntity")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.SaveExcelineJobAction.#_resId")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.SaveGymEmployeeWorkAction.#_dataTableEntity")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.SaveGymEmployeeWorkAction.#_resId")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.SaveWorkActiveTimeAction.#_branchId")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.SaveWorkItemAction.#_branchId")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.SaveWorkItemAction.#_startDateTime")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.SaveWorkItemAction.#_endDateTime")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.ValidatePunchcardContractMemberAction.#_memberIdList")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.ValidateScheduleWithResourceIdAction.#_branchId")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.ValidateScheduleWithResourceIdAction.#_user")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.ValidateScheduleWithSwitchResourceAction.#_branchId")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands.ValidateScheduleWithSwitchResourceAction.#_user")]
