﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/29/2012 8:25:22 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageEmployees
{
    public class TrainerFacade
    {

        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

       
        public static List<TrainerDC> GetTrainers(int branchId, string searchText, bool isActive, int trainerId, string gymCode)
        {
            return GetDataAdapter().GetTrainers(branchId, searchText, isActive, trainerId, gymCode);
        }
    }
}
