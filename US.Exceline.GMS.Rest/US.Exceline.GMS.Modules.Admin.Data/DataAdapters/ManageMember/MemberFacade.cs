﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "06/06/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageMember
{
    public class MemberFacade
    {
        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

        public static List<string> GetAccessPoints(string gymCode)
        {
            return GetDataAdapter().GetAccessPoints(gymCode);
        }

        public static List<EntityVisitDC> GetMemberVisits(DateTime startDate, DateTime endDate, int branchId, string gymCode)
        {
            return GetDataAdapter().GetMemberVisits(startDate, endDate, branchId, gymCode);
        }

        public static List<ExcelineMemberDC> GetSponsorsforSponsoring(int branchId, DateTime startDate, DateTime endDate, string gymCode)
        {
            return GetDataAdapter().GetSponsorsforSponsoring(branchId,startDate,endDate, gymCode);
        }

        public static bool GetSponsoringProcessStatus(string guiID, string gymCode, string user)
        {
            return GetDataAdapter().GetSponsoringProcessStatus( guiID,  gymCode,  user);
        }


        public static bool GenerateSponsorOrders(US.GMS.Core.SystemObjects.SponsorShipGenerationDetailsDC sponsorDetails, string gymCode , string user)
        {
            return GetDataAdapter().GenerateSponsorOrders(sponsorDetails, gymCode, user);
        }
    }
}
