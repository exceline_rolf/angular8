﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class GetArticleForResouceBookingAction : USDBActionBase<List<ArticleDC>>
   {
       private int _activityId;
       private int _branchId;
       public GetArticleForResouceBookingAction(int activityId,int branchId)
       {
           _activityId = activityId;
           _branchId = branchId;
       }
        protected override List<ArticleDC> Body(DbConnection connection)
        {
            var articleList = new List<ArticleDC>();
            const string storedProcedureName = "USExceGMSAdminGetArticleForResouceBooking";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                if (_activityId > 0)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _activityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ArticleDC article = new ArticleDC();
                    article.Id = Convert.ToInt32(reader["ID"]);
                    article.ArticleNo = reader["ArticleNo"].ToString();
                    article.Description = reader["Name"].ToString();
                    article.DefaultPrice = Convert.ToDecimal(reader["DefaultPrice"]);
                    article.CategoryCode = reader["Code"].ToString();
                    if (reader["PunchCard"] != DBNull.Value)
                    article.IsPunchCard = Convert.ToBoolean(reader["PunchCard"]);
                    if (reader["ContractBooking"] != DBNull.Value)
                    article.IsContractBooking = Convert.ToBoolean(reader["ContractBooking"]);
                    if (reader["NoOfMinutes"] != DBNull.Value)
                    article.NoOfMinutes = Convert.ToInt32(reader["NoOfMinutes"]);
                    articleList.Add(article);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return articleList;
        }
    }
}
