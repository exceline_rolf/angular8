﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Payments;

namespace US.Payment.Data.Admin.EconomySettings.DBCommands
{
    public class GetVatCodeAction : USDBActionBase<List<VatCodeDC>>
    {
        public GetVatCodeAction()
        {
        }

        protected override List<VatCodeDC> Body(DbConnection connection)
        {
            List<VatCodeDC> uspVatCodeList = new List<VatCodeDC>();
            try
            {
                const string storedProcedure = "USP_GetVATCodes";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    VatCodeDC vatCode = new VatCodeDC();
                    vatCode.VatAccount = new AccountDC();
                    vatCode.Id = Convert.ToInt32(reader["ID"]);
                    vatCode.VatCode = Convert.ToInt32(reader["VATCode"]);
                    vatCode.Name = Convert.ToString(reader["Name"]);
                    vatCode.Rate = Convert.ToDecimal(reader["Rate"]);
                    vatCode.VatAccount.ID = Convert.ToInt32(reader["AccountId"]);
                    vatCode.VatAccount.AccountNo = Convert.ToString(reader["AccountNo"]);
                    vatCode.VatAccount.Name = Convert.ToString(reader["AccountName"]);
                    vatCode.Description = Convert.ToString(reader["Description"]);
                    vatCode.VersionId = Convert.ToInt32(reader["VersionID"]);
                    vatCode.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    vatCode.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    vatCode.RegisteredUser = Convert.ToString(reader["RegUser"]);
                    vatCode.RegisteredDate = Convert.ToDateTime(reader["RegDate"]);
                    uspVatCodeList.Add(vatCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return uspVatCodeList;
        }
    }
}
