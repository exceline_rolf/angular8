﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/29/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetTimeEntriesForAdminViewAction : USDBActionBase<List<TimeEntryDC>>
    {
        private DateTime _startDate;
        private DateTime _endDate;

        public GetTimeEntriesForAdminViewAction(DateTime startDate, DateTime endDate)
        {
            _startDate = startDate;
            _endDate = endDate;
        }

        protected override List<TimeEntryDC> Body(DbConnection connection)
        {
            List<TimeEntryDC> timeEntryList = new List<TimeEntryDC>();
            string StoredProcedureName = "USExceGMSAdminGetTimeEntriesForAdminView";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    TimeEntryDC timeEntry = new TimeEntryDC();
                    timeEntry.Id = Convert.ToInt32(reader["ID"]);
                    if (reader["FirstName"] != DBNull.Value)
                    {
                        timeEntry.EntityName = reader["FirstName"].ToString();
                    }
                    if (reader["LastName"] != DBNull.Value)
                    {
                        timeEntry.EntityName += " " + reader["LastName"].ToString();
                    }
                    timeEntry.TaskName = reader["TaskName"].ToString();
                    timeEntry.TaskId = Convert.ToInt32(reader["TaskId"]);
                    timeEntry.ExecutedDate = Convert.ToDateTime(reader["ExecutedDate"]);
                    if (reader["SpendTime"] != DBNull.Value)
                    {
                        timeEntry.SpendTime = Convert.ToDecimal(reader["SpendTime"]);
                    }
                    if (reader["SpendTime"] != DBNull.Value)
                    {
                        timeEntry.TimeChange = Convert.ToDecimal(reader["SpendTime"]);
                    }
                    if (reader["Comment"] != DBNull.Value)
                    {
                        timeEntry.Comment = reader["Comment"].ToString();
                    }
                    if (reader["DueDate"] != DBNull.Value)
                    {
                        timeEntry.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    }
                    if (reader["TotalTimeSpent"] != DBNull.Value)
                    {
                        timeEntry.TotalTimeSpent = Convert.ToDecimal(reader["TotalTimeSpent"]);
                    }
                    timeEntryList.Add(timeEntry);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return timeEntryList;
        }

    }
}
