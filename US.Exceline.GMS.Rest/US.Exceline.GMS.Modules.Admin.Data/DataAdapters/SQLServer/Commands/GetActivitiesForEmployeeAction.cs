﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetActivitiesForEmployeeAction : USDBActionBase<List<ActivityDC>>
    {
        private int _branchId;
        private int _employeeId;
        private string _roleId = string.Empty;
        public GetActivitiesForEmployeeAction(int branchId, int employeeId, string roleId)
        {
            _branchId = branchId;
            _employeeId = employeeId;
            _roleId = roleId;
        }

        protected override List<ActivityDC> Body(System.Data.Common.DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminGetActivitiesForEmployee";
            List<ActivityDC> activityList = new List<ActivityDC>();
            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, _employeeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@roleId", DbType.String, _roleId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ActivityDC ActivityItem = new ActivityDC();
                    ActivityItem.Id = Convert.ToInt32(reader["ID"]);
                    ActivityItem.Name = Convert.ToString(reader["Name"]);
                    ActivityItem.BranchId = Convert.ToInt32(reader["BranchId"]);
                    ActivityItem.IsActive = Convert.ToBoolean(reader["IsActive"]);

                    activityList.Add(ActivityItem);
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return activityList;
        }
    }
}
