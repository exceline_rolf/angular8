﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractItemsAction : USDBActionBase<List<ContractItemDC>>
    {
        private int _branchId = -1;

        public GetContractItemsAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ContractItemDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ContractItemDC> contractItems = new List<ContractItemDC>();
            string spName = "USExceGMSManageMembershipGetContractItems";
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ContractItemDC item = new ContractItemDC();
                    item.Id = Convert.ToInt32(reader["ID"]);
                    item.ItemName = Convert.ToString(reader["Name"]);
                    item.UnitPrice = Convert.ToDecimal(reader["UnitPrice"]);
                    item.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                    item.Price = Convert.ToDecimal(reader["UnitPrice"]);
                    contractItems.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return contractItems;
        }
    }
}
