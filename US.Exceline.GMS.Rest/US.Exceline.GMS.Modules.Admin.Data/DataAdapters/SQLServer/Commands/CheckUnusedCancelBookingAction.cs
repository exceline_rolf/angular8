﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class CheckUnusedCancelBookingAction : USDBActionBase<List<int>>
   {
       private int _activityId;
       private DateTime _bookingDate;
       private DataTable _dataTable;
        private string _accountNo = string.Empty;
       public CheckUnusedCancelBookingAction(int activityId,string accountNo, List<int> memberIdList, DateTime bookingDate)
       {
           _activityId = activityId;
           _dataTable = GetMemberIdLst(memberIdList);
           _bookingDate = bookingDate;
           _accountNo = accountNo;
       }

       private DataTable GetMemberIdLst(List<int> memberIdLst)
       {
           _dataTable = new DataTable();
           _dataTable.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
           if (memberIdLst != null && memberIdLst.Any())
               foreach (var item in memberIdLst)
               {
                   DataRow dataTableRow = _dataTable.NewRow();
                   dataTableRow["ID"] = item;
                   _dataTable.Rows.Add(dataTableRow);
               }
           return _dataTable;
       }

       protected override List<int> Body(DbConnection connection)
        {
            var validMemLst = new List<int>();
            const string storedProcedureName = "USExceGMSAdminCheckUnusedCancelBooking";
            try
            {

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _activityId));
                if (!string.IsNullOrEmpty(_accountNo))
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccountNo", DbType.String, _accountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BookingDate", DbType.DateTime, _bookingDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberIds", SqlDbType.Structured, _dataTable));
                
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int validMemberId = Convert.ToInt32(reader["MemberId"]);
                  //  string accountNo = reader["AccountNo"].ToString().Trim();
                    validMemLst.Add(validMemberId);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return validMemLst;
        }
    }
}
