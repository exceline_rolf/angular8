﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Common;
using US_DataAccess;
using System.Data;
using US.GMS.Core.DomainObjects.Admin;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetDiscountsListAction : USDBActionBase<List<ShopDiscountDC>>
    {
        private int _branchId;
        private bool _isActive;

        public GetDiscountsListAction(int branchId, bool isActive)
        {
            this._branchId = branchId;
            this._isActive = isActive;
        }

        protected override List<ShopDiscountDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ShopDiscountDC> discountsList = new List<ShopDiscountDC>();
            string storedProcedure = "USExceGMSShopGetDiscounts";
            string storedProcedure2 = "USExceGMSShopGetDiscountsItems";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isActive", DbType.Boolean, _isActive));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ShopDiscountDC discount = new ShopDiscountDC();
                    discount.Id = Convert.ToInt32(reader["ID"].ToString());
                    discount.ArticleID = Convert.ToInt32(reader["ArticleID"].ToString());
                    discount.Name = reader["Name"].ToString();
                    discount.VendorName = reader["VendorName"].ToString();
                    discount.Description = reader["Description"].ToString();
                    discount.Discount = Convert.ToDouble(reader["Discount"].ToString());
                    discount.DiscountPercentage = discount.Discount.ToString() + " " + "%";
                    discount.StartDate = Convert.ToDateTime(reader["StartDate"].ToString());
                    discount.EndDate = Convert.ToDateTime(reader["EndDate"].ToString());
                    discount.NoOfFreeItems = Convert.ToInt32(reader["NoOfFreeItems"].ToString());
                    discount.MinNoOfArticles = Convert.ToInt32(reader["MinNoOfArticles"].ToString());
                    if (discount.MinNoOfArticles > 0)
                    {
                        discount.MinNoContent = discount.MinNoOfArticles.ToString();
                    }
                    else
                    {
                        discount.MinNoContent = "";
                    }
                    discount.VendorId = Convert.ToInt32(reader["VendorId"].ToString());
                    discount.ArticleCategoryId = Convert.ToInt32(reader["ArticleCategoryId"].ToString());
                    discountsList.Add(discount);
                }

                reader.Close();

                foreach (ShopDiscountDC discount in discountsList)
                {
                    if (discount.Id != 0)
                    {
                        DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure2);
                        cmd2.Parameters.Add(DataAcessUtils.CreateParam("@disId", DbType.Int32, discount.Id));
                        cmd2.Parameters.Add(DataAcessUtils.CreateParam("@isActive", DbType.Boolean, _isActive));
                        cmd2.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                        DbDataReader reader2 = cmd2.ExecuteReader();

                        while (reader2.Read())
                        {
                            int itemId;
                            string itemName;
                            itemId = Convert.ToInt32(reader2["ItemId"].ToString());
                            itemName = reader2["ItemName"].ToString();
                            discount.ItemList.Add(itemId);
                            discount.ItemNames.Add(itemName);
                        }

                        reader2.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return discountsList;
        }
    }
}
