﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymEmployeeRoleDurationAction : USDBActionBase<List<GymEmployeeRoleDurationDC>>
    {
        private int _employeeId = -1;

        public GetGymEmployeeRoleDurationAction(int employeeId)
        {
            this._employeeId = employeeId;   
        }
        protected override List<GymEmployeeRoleDurationDC> Body(System.Data.Common.DbConnection connection)
        {
            List<GymEmployeeRoleDurationDC> gymEmployeeRoleDurationList = new List<GymEmployeeRoleDurationDC>();
            string StoredProcedureName = "USExceGMSAdminGetGymEmployeeRoleDuration";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeId", DbType.Int32, _employeeId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    GymEmployeeRoleDurationDC duration = new GymEmployeeRoleDurationDC();
                    duration.RoleId = Convert.ToInt32(reader["ID"]);
                    duration.RoleName = Convert.ToString(reader["RoleName"]);                   
                    gymEmployeeRoleDurationList.Add(duration);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return gymEmployeeRoleDurationList;
        }
    }
}
