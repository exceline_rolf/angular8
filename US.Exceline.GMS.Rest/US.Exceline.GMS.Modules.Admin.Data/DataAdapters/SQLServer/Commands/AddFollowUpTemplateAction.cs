﻿using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class AddFollowUpTemplateAction : USDBActionBase<bool>
    {
        private readonly FollowUpTemplateDC _followUpTemplate;
        private readonly int _branchId;

        public AddFollowUpTemplateAction(FollowUpTemplateDC followUpTemplate, int branchId)
        {
            _followUpTemplate = followUpTemplate;
            _branchId = branchId;
        }

        protected override bool Body(DbConnection connection)
        {
            bool isSuccess;
            const string spName = "USExceGMSAdminAddEditFollowUpTemplate";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _followUpTemplate.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _followUpTemplate.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryId", DbType.Int32, _followUpTemplate.Category.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _followUpTemplate.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedDate", DbType.Date, DateTime.Now));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDefault", DbType.Boolean, _followUpTemplate.IsDefault));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowupTask", SqlDbType.Structured, GetFollowUpTask()));
                cmd.ExecuteNonQuery();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSuccess;
        }

        private DataTable GetFollowUpTask()
        {
            DataTable dataTable = new DataTable();
            try
            {
                dataTable.Columns.Add(new DataColumn("FollowUpTemplateId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("TaskCategoryId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("NumberOfDays", typeof(int)));
                dataTable.Columns.Add(new DataColumn("IsNextFollowUp", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("Text", typeof(string)));

                foreach (FollowUpTemplateTaskDC followUpTask in _followUpTemplate.FollowUpTaskList)
                {
                    DataRow dataTableRow = dataTable.NewRow();
                    dataTableRow["FollowUpTemplateId"] = followUpTask.FollowUpTemplateId;
                    dataTableRow["TaskCategoryId"] = followUpTask.ExcelineTaskCategory.Id;
                    dataTableRow["NumberOfDays"] = followUpTask.NumberOfDays;
                    dataTableRow["IsNextFollowUp"] = followUpTask.IsNextFollowUpValue;
                    dataTableRow["Text"] = followUpTask.Text;
                    dataTable.Rows.Add(dataTableRow);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dataTable;
        }
    }

}
