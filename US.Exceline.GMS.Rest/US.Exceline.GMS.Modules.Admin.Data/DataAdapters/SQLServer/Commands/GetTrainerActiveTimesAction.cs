﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/29/2012 8:28:20 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class GetTrainerActiveTimesAction : USDBActionBase<List<TrainerActiveTimeDC>>
    {
        private int _branchId;
        private DateTime _startDate;
        private DateTime _endDate;
        private int _entNO;

        public GetTrainerActiveTimesAction(int _branchId, DateTime _startDate, DateTime _endDate, int _entNO)
        {
            this._branchId = _branchId;
            this._startDate = _startDate;
            this._endDate = _endDate;
            this._entNO = _entNO;
        }

        protected override List<TrainerActiveTimeDC> Body(DbConnection connection)
        {
            List<TrainerActiveTimeDC> trainerActiveTimeList = new List<TrainerActiveTimeDC>();
            string storedProcedureName = "USExceGMSAdminGetTrainerActiveTimes";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityNo", DbType.Int32, _entNO));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    TrainerActiveTimeDC trainerActiveTime = new TrainerActiveTimeDC();
                    trainerActiveTime.Id = Convert.ToInt32(reader["ID"]);
                    trainerActiveTime.SheduleItemId = Convert.ToInt32(reader["ScheduleItemId"]);
                    if(reader["StartDateTime"] != DBNull.Value)
                      trainerActiveTime.StartDateTime = Convert.ToDateTime(reader["StartDateTime"]);
                    if(reader["EndDateTime"] != DBNull.Value)
                      trainerActiveTime.EndDateTime = Convert.ToDateTime(reader["EndDateTime"]);

                    trainerActiveTime.EntityId = Convert.ToInt32(reader["EntityId"]);
                    trainerActiveTime.EntityRoleType = Convert.ToString(reader["EntityRoleType"]);
                    if (!string.IsNullOrEmpty(reader["Name"].ToString()))
                    {
                        trainerActiveTime.Name = Convert.ToString(reader["Name"]);
                    }
                    trainerActiveTimeList.Add(trainerActiveTime);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return trainerActiveTimeList;
        }
    }
}
