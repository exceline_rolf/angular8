﻿// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : Unicorngma
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Payments;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteVatAccountsAction : USDBActionBase<bool>
    {
        private VatCodeDC _vatAccountDetail;
        private string _user;
        public DeleteVatAccountsAction(VatCodeDC vatAccountDetail, String user)
        {
            this._vatAccountDetail = vatAccountDetail;
            this._user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSAdminDeleteVatAccounts";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", System.Data.DbType.Int32, _vatAccountDetail.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VatCode", System.Data.DbType.Int32, _vatAccountDetail.VatCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                cmd.ExecuteScalar();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
