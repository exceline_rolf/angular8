﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateResWithSwitchDataRangeAction : USDBActionBase<List<int>>
    {
        private DataTable _dataTable;
        private int _resourceId;
        private DateTime _startDate;
        private DateTime _endDate;
        public ValidateResWithSwitchDataRangeAction(List<int> resourceIdList,DateTime startDate,DateTime endDate)
        {
            //_resourceId = resourceId;
            _startDate = startDate;
            _endDate = endDate;
            _dataTable = GetResourceIdLst(resourceIdList);
        }

        private DataTable GetResourceIdLst(List<int> memberIdLst)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
            if (memberIdLst != null && memberIdLst.Any())
                foreach (var item in memberIdLst)
                {
                    DataRow dataTableRow = _dataTable.NewRow();
                    dataTableRow["ID"] = item;
                    _dataTable.Rows.Add(dataTableRow);
                }
            return _dataTable;
        }

        protected override List<int> Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAdminValidateResourceWithSwitchDataRange";
            var notValidResLst = new List<int>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ResourceIds", SqlDbType.Structured, _dataTable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _startDate));
                if (_endDate == DateTime.MinValue)
                    _endDate = DateTime.MaxValue;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _endDate));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int notValidResId = Convert.ToInt32(reader["ResourceId"]);
                    notValidResLst.Add(notValidResId);
                }

                //DbParameter para = new SqlParameter();
                //para.DbType = DbType.Boolean;
                //para.ParameterName = "@OutPut";
                //para.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(para);
                //cmd.ExecuteNonQuery();
                //bool result = Convert.ToBoolean(para.Value);
                //return result;
            }
            catch (Exception)
            {
                throw;
            }
            return notValidResLst;
        }
    }
}
