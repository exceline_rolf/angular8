﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Common;
using US_DataAccess;
using System.Data;
using US.GMS.Core.DomainObjects.Admin;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetDiscountListAction : USDBActionBase<List<ShopDiscountDC>>
    {
        private int _branchId;
        private bool _isActive;

        public GetDiscountListAction(int branchId, bool isActive)
        {
            this._branchId = branchId;
            this._isActive = isActive;
        }

        protected override List<ShopDiscountDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ShopDiscountDC> inventoryItemList = new List<ShopDiscountDC>();
            string storedProcedure = "";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isActive", DbType.Boolean, _isActive));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ShopDiscountDC discount = new ShopDiscountDC();

                    discount.Name = reader["Name"].ToString();
                    discount.Discount = Convert.ToDouble(reader["Discount"].ToString());
                    discount.StartDate = Convert.ToDateTime(reader["StartDate"].ToString());
                    discount.EndDate = Convert.ToDateTime(reader["EndDate"].ToString());

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return inventoryItemList;
        }
    }
}
