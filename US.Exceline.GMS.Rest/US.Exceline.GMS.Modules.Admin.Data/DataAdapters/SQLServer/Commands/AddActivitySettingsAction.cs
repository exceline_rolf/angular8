﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class AddActivitySettingsAction:USDBActionBase<int>
    {
        private ActivitySettingDC _activitySetting;
        private string _user;

        public AddActivitySettingsAction(ActivitySettingDC activitySetting, string user)
        {
            this._activitySetting = activitySetting;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            int _activitySettingId = -1;
            string StoredProcedureName = "USExceGMSAdminAddActivitySettings";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _activitySetting.ActivityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsContractTimeLimited", DbType.Boolean, _activitySetting.IsContractTimeLimited));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPunchCardLimited", DbType.Boolean, _activitySetting.IsPunchCardLimited));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsSMSRemindered", DbType.Boolean, _activitySetting.IsSMSRemindered));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DaysInAdvanceForBooking", DbType.Int32, _activitySetting.DaysInAdvanceForBooking));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsTrial", DbType.Boolean, _activitySetting.IsTrial));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsContractBooking", DbType.Boolean, _activitySetting.IsContractBooking));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BookingVIAIBooking", DbType.Boolean, _activitySetting.IsBookViaIbooking));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _activitySetting.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsBookingActivity", DbType.Boolean, _activitySetting.IsBookingActivity));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsBasicActivity", DbType.Boolean, _activitySetting.IsBasicActivity));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                object obj = cmd.ExecuteScalar();
                _activitySettingId = Convert.ToInt32(obj);
            }
            catch 
            {
                throw;
            }
            return _activitySettingId;
        }
    }
}
