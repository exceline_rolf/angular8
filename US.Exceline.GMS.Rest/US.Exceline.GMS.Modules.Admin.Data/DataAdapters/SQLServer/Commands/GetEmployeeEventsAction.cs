﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetEmployeeEventsAction : USDBActionBase<List<EmployeeEventDC>>
    {
        private int _employeeNo;

        public GetEmployeeEventsAction(int employeeNo)
        {
            _employeeNo = employeeNo;
        
        }

        protected override List<EmployeeEventDC> Body(System.Data.Common.DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminGetEmployeeEvents";
            List<EmployeeEventDC> employeeEventList = new List<EmployeeEventDC>();
            try 
            {
            
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeNumber", DbType.Int32, _employeeNo));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    EmployeeEventDC employeeEvent = new EmployeeEventDC();
                    employeeEvent.Id = Convert.ToInt32(reader["ID"]);
                    employeeEvent.Code = Convert.ToString(reader["Code"]);
                    employeeEvent.Decription = Convert.ToString(reader["Description"]);
                    employeeEvent.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    employeeEvent.Time = (TimeSpan)reader["TimeStamp"];
                    employeeEvent.Status  = Convert.ToString(reader["Status"]);
                    employeeEvent.BranchId = Convert.ToInt32(reader["BranchId"]);
                    employeeEvent.BranchName = reader["BranchName"].ToString();
                    employeeEventList.Add(employeeEvent);
                }
            }
            catch(Exception ex)
            {
                throw ex;        
            }
            return employeeEventList;
        }

    }
}
