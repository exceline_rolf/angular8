﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteFollowUpTemplateAction : USDBActionBase<bool>
    {

        private int _followUpTemplateId;

        public DeleteFollowUpTemplateAction(int followUpTemplateId)
        {
            _followUpTemplateId = followUpTemplateId;

        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool isDeleteSucces = false;
            string sp = "USExceGMSAdminDeleteFollowUpTemplate";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, sp);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowUpTemplateId", DbType.Int32, _followUpTemplateId));
                cmd.ExecuteNonQuery();
                isDeleteSucces = true;

            }
            catch
            { 
                
            }
            return isDeleteSucces;
        }


    }
}
