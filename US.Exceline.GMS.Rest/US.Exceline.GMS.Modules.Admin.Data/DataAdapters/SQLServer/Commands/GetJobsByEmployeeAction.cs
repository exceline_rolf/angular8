﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class GetJobsByEmployeeAction : USDBActionBase<List<ExcelineCommonTaskDC>>
    {
        private readonly int _branchId;
        private readonly int _employeeId;
        private readonly string _user = string.Empty;
        private readonly int _hit;
        public GetJobsByEmployeeAction(int branchId, int employeeId, string user, int hit)
        {
            _branchId = branchId;
            _employeeId = employeeId;
            _user = user;
            _hit = hit;
        }
        protected override List<ExcelineCommonTaskDC> Body(DbConnection connection)
        {
            var allTaskList = new List<ExcelineCommonTaskDC>();
            const string storedProcedureName = "USExceGMSAdminGetExcelineJobsByEmployee";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeId", DbType.Int32, _employeeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@hit", DbType.String, _hit));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var task = new ExcelineCommonTaskDC();
                    task.Id = Convert.ToInt32(reader["Id"]);
                    task.Text = reader["Name"].ToString();
                    task.EntityRoleType = Convert.ToString(reader["EntityRoleType"]);
                    task.Category = reader["Category"].ToString();
                    if (reader["PlanDate"] != DBNull.Value)
                    {
                        task.PlanDate = Convert.ToDateTime(reader["PlanDate"]);
                    }
                    if (reader["StartTime"] != DBNull.Value)
                    {
                        task.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    }
                    if (reader["EndTime"] != DBNull.Value)
                    {
                        task.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    }
                    task.TaskTemplateId = Convert.ToInt32(reader["FollowUpTemplateId"]);
                    task.FollowupMemName = reader["FollowUpMember"].ToString();
                    task.FollowupMemRole = reader["FollowUpMemberRole"].ToString();
                    task.FollowUpMemberId = Convert.ToInt32(reader["FollowUpMemberId"]);
                    task.IsEmployee = Convert.ToBoolean(reader["IsEmployee"]);
                    task.IsFollowUp = Convert.ToBoolean(reader["IsFollowUp"]);
                    allTaskList.Add(task);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return allTaskList;
        }
    }
}
