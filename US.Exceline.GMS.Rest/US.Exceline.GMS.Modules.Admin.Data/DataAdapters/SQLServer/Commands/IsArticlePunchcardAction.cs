﻿// --------------------------------------------------------------------------
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : Martin Tømmerås
// Created Timestamp : 23.08.2018 - 08:45
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class IsArticlePunchcardAction : USDBActionBase<int>
    {
        private int _articleId;
        private int _punchcardVal;

        public IsArticlePunchcardAction(int articleId)
        {
            this._articleId = articleId;
        }

        protected override int Body(DbConnection connection)
        {
            int result;
            string StoredProcedureName = "USExceGMSIsArticledPunchcard";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, _articleId));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.Boolean;
                output.ParameterName = "@OutPut";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(output.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
    }
}
