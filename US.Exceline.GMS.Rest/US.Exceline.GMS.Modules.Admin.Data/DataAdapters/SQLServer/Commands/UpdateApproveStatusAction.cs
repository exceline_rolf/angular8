﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class UpdateApproveStatusAction : USDBActionBase<int>
    {
        private bool _isApproved = true;
        private int _entityActiveTimeID = -1;
        private string _user = string.Empty;

        public UpdateApproveStatusAction(bool IsApproved, int EntityActiveTimeID, string user)
        {
            _isApproved = IsApproved;
            _entityActiveTimeID = EntityActiveTimeID;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            const string storedProcedureName = "UpdateApproveStatus";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityActiveTimeID", DbType.Int32, _entityActiveTimeID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsApproved", DbType.Boolean, _isApproved));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 1;
        }
    }
}
