﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class GetInventoryAction : USDBActionBase<List<InventoryItemDC>>
   {
       private int _branchId;
       public GetInventoryAction(int branchId)
       {
           _branchId = branchId;
       }
        protected override List<InventoryItemDC> Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSAdminGetInventory";
            List<InventoryItemDC> inventoryItemLst = new List<InventoryItemDC>();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    InventoryItemDC inventory = new InventoryItemDC();
                    inventory.Id = Convert.ToInt32(reader["ID"]);
                    inventory.BranchId = Convert.ToInt32(reader["BranchId"]);
                    inventory.Name = reader["Name"].ToString();
                    if (reader["StartDate"] != DBNull.Value)
                    inventory.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    if (reader["ApprovedDate"] != DBNull.Value)
                    inventory.ApprovedDate = Convert.ToDateTime(reader["ApprovedDate"]);
                    inventory.NetValue = Convert.ToDecimal(reader["NetValue"]);

                    inventoryItemLst.Add(inventory);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return inventoryItemLst;
        }
    }
}
