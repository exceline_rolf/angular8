﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class ValidatePunchcardContractMemberWithAccountAction : USDBActionBase<List<int>>
    {
    
         private DataTable _dataTable;
        private readonly DateTime _startDate;
        private readonly int _activityId = -1;
        private readonly string _accountNo = string.Empty;
       private int _branchId;
        public ValidatePunchcardContractMemberWithAccountAction(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime startDate)
       {
           _activityId = activityId;
           _dataTable = GetMemberIdLst(memberIdList);
           _startDate = startDate;
            _accountNo = accountNo;
            _branchId = branchId;
       }

        private DataTable GetMemberIdLst(List<int> memberIdLst)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
            if (memberIdLst != null && memberIdLst.Any())
                foreach (var item in memberIdLst)
                {
                    DataRow dataTableRow = _dataTable.NewRow();
                    dataTableRow["ID"] = item;
                    _dataTable.Rows.Add(dataTableRow);
                }
            return _dataTable;
        }

        protected override List<int> Body(DbConnection connection)
        {
            var validMemLst = new List<int>();
            string spName = "USExceGMSAdminValidatePunchcardContractMemberWithAccount";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _activityId));
                if (!string.IsNullOrEmpty(_accountNo))
                    command.Parameters.Add(DataAcessUtils.CreateParam("@AccountNo", DbType.String, _accountNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _startDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", SqlDbType.Structured, _dataTable));
                
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    int validMemberId = Convert.ToInt32(reader["MemberId"]);
                    validMemLst.Add(validMemberId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return validMemLst;
        }
    }
}
