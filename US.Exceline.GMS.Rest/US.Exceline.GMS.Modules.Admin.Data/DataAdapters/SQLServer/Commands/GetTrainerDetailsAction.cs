﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.ManageContracts;


namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetTrainerDetailsAction : USDBActionBase<List<TrainerDC>>
    {
        int _branchId = 0;
        string _user = string.Empty;
        private bool _isActive = true;
        private List<TrainerDC> trainerLst = new List<TrainerDC>();
        public GetTrainerDetailsAction(int branchId, string user, bool isActive, int trainerId)
        {
            _branchId = branchId;
            _user = user;
            _isActive = isActive;

            if (!string.IsNullOrEmpty(_user))
            {
                string[] sTextArray = _user.Split(new char[] { ':' });
                if (sTextArray.Length == 1)
                {
                    string[] textParts = sTextArray[0].Split(new char[] { ' ' });
                    if (textParts.Length > 1)
                    {
                        _user = textParts[0] + " AND " + textParts[1];
                    }
                }
                else
                {
                    string[] textParts = sTextArray[1].Split(new char[] { ' ' });
                    if (textParts.Length > 1)
                    {
                        _user = textParts[0] + " AND " + textParts[1];
                    }
                }
            }
        }

        protected override List<TrainerDC> Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminGetTrainers";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                if (!string.IsNullOrEmpty(_user))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeState", DbType.Boolean, _isActive));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    TrainerDC trainer = new TrainerDC();
                    trainer.Id = Convert.ToInt32(reader["EmployeeId"]);
                    trainer.FirstName = Convert.ToString(reader["FirstName"]);
                    trainer.LastName = Convert.ToString(reader["LastName"]);
                    trainer.Name = trainer.FirstName + " " + trainer.LastName;
                    trainer.Address1 = Convert.ToString(reader["Address1"]);
                    trainer.Address2 = Convert.ToString(reader["Address2"]);
                    trainer.Address3 = Convert.ToString(reader["Address3"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["TelHome"])))
                    {
                        trainer.Mobile = Convert.ToString(reader["TelMobile"]).Trim();
                    }

                    if (reader["TelHome"] != DBNull.Value)
                    {
                        trainer.Private = Convert.ToString(reader["TelHome"]).Trim();
                    }

                    trainer.EntityRoleType = "TRA";
                    trainer.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    trainer.RoleId = Convert.ToString(reader["RoleId"]);
                    trainerLst.Add(trainer);
                }
            }

            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }

            return trainerLst;
        }


        public TrainerDC RunOnTransation(DbTransaction dbTransaction, TrainerDC tra)
        {

            string StoredProcedureName = "USExceGMSAdminGetActivitiesForEmployee";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = dbTransaction.Connection;
                cmd.Transaction = dbTransaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, tra.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@roleId", DbType.String, tra.RoleId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ActivityDC ActivityItem = new ActivityDC();
                    ActivityItem.Id = Convert.ToInt32(reader["ID"]);
                    ActivityItem.Name = Convert.ToString(reader["Name"]);
                    ActivityItem.BranchId = Convert.ToInt32(reader["BranchId"]);
                    ActivityItem.IsActive = Convert.ToBoolean(reader["IsActive"]);
                    tra.ActivityList.Add(ActivityItem);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tra;
        }
    }
}

