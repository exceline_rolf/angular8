﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class ValidateScheduleWithResourceIdAction : USDBActionBase<bool>
    {
        private int _resourceId     = -1;
        private DateTime _startDate = DateTime.Today;
        private DateTime _endDate   = DateTime.MaxValue;
        private int _branchId       = -1;
        private string _user        = string.Empty;

        public ValidateScheduleWithResourceIdAction(int resourceId, DateTime startDate, DateTime endDate, int branchId, string user)
        {
            _resourceId = resourceId;
            _startDate  = startDate;
            _endDate    = endDate;
            _branchId   = branchId;
            _user       = user;
        }

        protected override bool Body(DbConnection connection)
        {
            string storedProcedureName = "USExceGMSAdminValidateScheduleWithResourceID";
            string scheduleId = string.Empty;

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@resourceId", DbType.Int32, _resourceId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    scheduleId = Convert.ToString(reader["ScheduleId"]); break;
                }
            }
            catch (Exception)
            {
            }

            if (string.IsNullOrEmpty(scheduleId))
                return false;
            else
                return true;
        }
    }
}
