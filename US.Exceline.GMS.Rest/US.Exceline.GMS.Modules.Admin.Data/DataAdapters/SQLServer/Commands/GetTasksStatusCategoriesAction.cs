﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/31/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetTasksStatusCategoriesAction : USDBActionBase<List<CategoryDC>>
    {
        private int _branchId;
        public GetTasksStatusCategoriesAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<CategoryDC> Body(DbConnection connection)
        {
            List<CategoryDC> categoryList = new List<CategoryDC>();
            string storedProcedure = "USExceGMSAdminGetTasksStatusCategory";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                                while(reader.Read())
                {
                    CategoryDC category = new CategoryDC();
                    category.Id = Convert.ToInt32(reader["ID"]);
                    category.Name  = Convert.ToString(reader["Name"]);
                    categoryList.Add(category);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return categoryList;
        }
    }
}
