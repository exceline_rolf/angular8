﻿// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : Unicorngma
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------

using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class AddUpdateGymCompanyOtherSettingsAction : USDBActionBase<int>
    {
        private readonly SystemSettingOtherSettingDC _gymCompanyOtherSettingsDetail;
        private readonly string _user;
        public AddUpdateGymCompanyOtherSettingsAction(SystemSettingOtherSettingDC gymCompanyOtherSettingsDetail, String user)
        {
            _gymCompanyOtherSettingsDetail = gymCompanyOtherSettingsDetail;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            var outputId = -1;
            const string storedProcedureName = "USExceGMSAdminAddGymCompanyOtherSettings";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.String, _gymCompanyOtherSettingsDetail.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassGeneratingDays", DbType.String, _gymCompanyOtherSettingsDetail.ClassGeneratingDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NoOfCopiesForInvoice", DbType.String, _gymCompanyOtherSettingsDetail.NoOfCopiesForInvoice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NetsSendingNo", DbType.String, _gymCompanyOtherSettingsDetail.NetsSendingNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CustomerNo", DbType.String, _gymCompanyOtherSettingsDetail.CustomerNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractNo", DbType.String, _gymCompanyOtherSettingsDetail.ContractNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User",DbType.String,_user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExcludingConnCategories", DbType.String, _gymCompanyOtherSettingsDetail.ExcludingCategories));
                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                outputId = Convert.ToInt32(para1.Value);
            }
            catch
            {
                throw;
            }
            return outputId;
        }
    }
}
