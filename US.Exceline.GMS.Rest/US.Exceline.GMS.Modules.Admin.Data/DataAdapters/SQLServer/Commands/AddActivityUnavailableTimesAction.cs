﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class AddActivityUnavailableTimesAction : USDBActionBase<bool>
    {
        private List<ActivityTimeDC> _activityTimes; 
        public AddActivityUnavailableTimesAction(List<ActivityTimeDC> activityTimes)
        {
            _activityTimes = activityTimes;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSAdminAddActivityUnAvailableTimes";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@UnavailableTimes", SqlDbType.Structured, GetDataTable(_activityTimes)));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private DataTable GetDataTable(List<ActivityTimeDC> activeTimes)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("Id", typeof(int)));
            dataTable.Columns.Add(new DataColumn("ActivityId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("StartDate", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("EndDate", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("StartTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("EndTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("BranchId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Reason", typeof(string)));

            foreach (ActivityTimeDC activitytime in activeTimes)
            {
                DataRow row = dataTable.NewRow();
                row["Id"] = activitytime.Id;
                row["ActivityId"] = activitytime.ActivityId;
                row["StartDate"] = activitytime.Startdate;
                row["EndDate"] = activitytime.EndDate;
                if(activitytime.StartTime != null)
                  row["StartTime"] = activitytime.StartTime;
                if(activitytime.EndTime != null)
                  row["EndTime"] = activitytime.EndTime;
                row["BranchId"] = activitytime.BranchId;
                if (activitytime.Reason != null)
                     row["Reason"] = activitytime.Reason;
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }
    }
}
