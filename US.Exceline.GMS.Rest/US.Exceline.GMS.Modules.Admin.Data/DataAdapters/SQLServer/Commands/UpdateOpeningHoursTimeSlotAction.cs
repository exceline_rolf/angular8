﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US.GMS.Core.DomainObjects.Admin.ManageOpeningHours;
using System.Data.Common;
using US_DataAccess;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateOpeningHoursTimeSlotAction : USDBActionBase<bool>
    {
        private ExcelineTimeSlotDC _timeSlot;
        private string _day = String.Empty;
        private bool _status = false;


        public UpdateOpeningHoursTimeSlotAction(ExcelineTimeSlotDC timeSlot, string day, bool status)
        {
            this._timeSlot = timeSlot;
            this._day = day;
            this._status = status;
        }

        protected override bool Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminUpdateTimeSchedule";
            bool returnValue = false;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.String, this._timeSlot.StartTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.String, this._timeSlot.EndTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Day", DbType.String, _day));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Status", DbType.Boolean, _status));

                cmd.ExecuteNonQuery();
                returnValue = true;
            }
            catch (Exception ex)
            {
                returnValue = false;
                throw ex;
            }
            return returnValue;
        }
    }
}
