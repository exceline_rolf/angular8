﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetBranchesAction : USDBActionBase<List<ExcelineBranchDC>>
    {
        private int _branchId = -1;
        private string _userName;

        public GetBranchesAction(int branchId,string userName)
        {
            _branchId = branchId;
            _userName = userName;
        }

        protected override List<ExcelineBranchDC> Body(DbConnection connection)
        {
            List<ExcelineBranchDC> branchList = new List<ExcelineBranchDC>();
            string storedProcedure = "USExceGMSAdminGetBranches";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
               // if(_branchId != -1)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExcelineBranchDC item = new ExcelineBranchDC();
                    item.Id = Convert.ToInt32(reader["BranchId"]);
                    item.EntNo = Convert.ToInt32(reader["EntNo"]);
                    if(reader["IsExpressGym"] != DBNull.Value)
                    {
                        item.IsExpressGym = Convert.ToBoolean(reader["IsExpressGym"]);  
                    }
                    if(_branchId == -1)
                        item.BranchName = item.Id.ToString() +"_" +reader["BranchName"].ToString();
                    else
                        item.BranchName = reader["BranchName"].ToString();

                    item.OfficialName = reader["OfficialName"].ToString();
                    item.Web = reader["Web"].ToString();
                    item.CountryMobilePrefix = reader["MobilePrefix"].ToString();
                    item.GroupId = Convert.ToInt32(reader["GroupId"]);
                    item.GroupName = reader["GroupName"].ToString();
                    item.RegisteredDate = Convert.ToDateTime(reader["RegisteredDate"]);
                    item.Addr1 = reader["Addr1"].ToString();
                    item.Addr2 = reader["Addr2"].ToString();
                    item.Addr3 = reader["Addr3"].ToString();
                    item.ZipCode = reader["ZipCode"].ToString();
                    item.ZipName = reader["ZipName"].ToString();
                    item.CreditorCollectionId = Convert.ToInt32(reader["CreditorInkassoID"]);
                    item.CountryId = reader["CountryId"].ToString();
                    item.TelWork = reader["TelWork"].ToString().Trim();
                    item.Email = reader["Email"].ToString();
                    item.Fax = reader["Fax"].ToString().Trim();
                    item.BankAccountNo = reader["AccountNo"].ToString();
                    item.KidSwapAccountNo = reader["KIDSwapAccountNo"].ToString();
                    item.CollectionAccountNo = reader["ColletionAccountNo"].ToString();
                    item.RegisteredNo = reader["RegisteredNo"].ToString();
                    item.Region = reader["Region"].ToString();
                    item.RegionId = Convert.ToInt32(reader["RegionId"]);
                    item.Area = Convert.ToInt32(reader["Area"]);
                    item.OtherAdminMobile = reader["OtherAdminMobile"].ToString();
                    item.OtherAdminEmail = reader["OtherAdminEmail"].ToString();
                    item.BureauAccountNO = reader["BureauAccountNo"].ToString();
                    item.ReorderSmsReceiver = reader["ReorderSmsReceiver"].ToString();
                    if (reader["CompanyIdSalary"] != DBNull.Value)
                        item.CompanyIdSalary = Convert.ToInt32(reader["CompanyIdSalary"]);
                    branchList.Add(item);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return branchList;
        }
    }
}
