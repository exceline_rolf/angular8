﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class ApproveEmployeeTimesAction :  USDBActionBase<bool>
    {
        private int _employeeId;
        private bool _isAllApproved;
        private string _timeIdList;

        public ApproveEmployeeTimesAction(int employeeId, bool isAllApproved, string timeIdList)
        {
            _employeeId = employeeId;
            _isAllApproved = isAllApproved;
            _timeIdList = timeIdList;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSAdminApproveEmployeeTimes";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _employeeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAllApproved", DbType.Boolean , _isAllApproved));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TimeIdList", DbType.String, _timeIdList));
                cmd.ExecuteScalar();
                result = true;
            }
            catch
            {
                throw;
            }
            return result;
        }



    }
}
