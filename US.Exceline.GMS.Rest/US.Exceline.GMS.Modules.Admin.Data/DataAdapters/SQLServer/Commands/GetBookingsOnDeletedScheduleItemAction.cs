﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "4/1/2012 10:39:15 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class GetBookingsOnDeletedScheduleItemAction : USDBActionBase<List<ScheduleItemDC>>
    {
        private int _resourceId = -1;
        private object somethingg;

        public GetBookingsOnDeletedScheduleItemAction(int resourceId)
        {
            _resourceId = resourceId;
        }

        protected override List<ScheduleItemDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ScheduleItemDC> scheduleItems = new List<ScheduleItemDC>();
            string StoredProcedureName = "USExceGMSCheckForBookingsOnNonActiveScheduleItems";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ResourceId", DbType.Int32, _resourceId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ScheduleItemDC item = new ScheduleItemDC();
                    item.Id = Convert.ToInt32(reader["ID"]);
                    // item.Occurrence = Convert.ToString(reader["Occurrence"]);
                    item.Day = Convert.ToString(reader["Day"]);
                    item.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    item.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    item.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    item.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    item.EntityRoleType = Convert.ToString(reader["EntityRoleType"]);
                    item.ParentId = Convert.ToInt32(reader["ParentId"]);
                    item.WeekType = Convert.ToInt32(reader["WeekType"]);
                    scheduleItems.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return scheduleItems;
        }

    }
}
