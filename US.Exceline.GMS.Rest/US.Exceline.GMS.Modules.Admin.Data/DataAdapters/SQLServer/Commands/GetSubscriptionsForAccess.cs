﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetSubscriptionsForAccess : USDBActionBase<List<SubscriptionInfo>>
    {
  
        public GetSubscriptionsForAccess()
        {

        }

        protected override List<SubscriptionInfo> Body(System.Data.Common.DbConnection connection)
        {
            string StoredProcedureName = "GetSubscriptionsForAccess";
            List<SubscriptionInfo> subscriptionList = new List<SubscriptionInfo>();
          
           
            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    SubscriptionInfo SubscriptionItem = new SubscriptionInfo();
                    SubscriptionItem.ModuleId = Convert.ToInt32(reader["ModuleId"]);
                    SubscriptionItem.FeatureId = Convert.ToInt32(reader["FeatureId"]);
                    SubscriptionItem.OperationId = Convert.ToInt32(reader["OperationId"]);
                    SubscriptionItem.SubOperationId = Convert.ToInt32(reader["SubOperationId"]);
                    SubscriptionItem.DisplayName = Convert.ToString(reader["DisplayName"]);
                    SubscriptionItem.FirstSubbedDate = Convert.ToDateTime(reader["FirstSubbedDate"]);
                    SubscriptionItem.IsActive = Convert.ToBoolean(reader["IsActive"]);
                    SubscriptionItem.IsBasic = Convert.ToBoolean(reader["IsBasic"]);
                    SubscriptionItem.AdminOnly = Convert.ToBoolean(reader["AdminOnly"]);
                    subscriptionList.Add(SubscriptionItem);
                    
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
          
            return subscriptionList;
        }
    }
}
