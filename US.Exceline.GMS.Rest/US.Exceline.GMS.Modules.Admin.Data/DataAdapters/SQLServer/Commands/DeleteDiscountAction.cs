﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteDiscountAction : USDBActionBase<bool>
    {
        private int _discountId;
        private string _type;


        public DeleteDiscountAction(int disocuntId, string type)
        {
            _discountId = disocuntId;
            _type = type;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool status = false;
            string StoredProcedureName = "USExceGMSAdminDeleteGroupShopDiscount";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@discountId", DbType.Int32, _discountId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@discountType", DbType.String, _type));
                cmd.ExecuteNonQuery();
                status = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }


    }
}
