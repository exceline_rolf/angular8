﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.SystemObjects;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetActivitiesForRoleAction : USDBActionBase<List<RoleActivityDC>> 
    {
        private int _memberId;
        private List<RoleActivityDC> _activityList = new List<RoleActivityDC>();
        public GetActivitiesForRoleAction(int memberId)
        {
            _memberId = memberId;
        }

        protected override List<RoleActivityDC> Body(System.Data.Common.DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminGetActivitiesForRole";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId",DbType.Int32, _memberId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    RoleActivityDC activity = new RoleActivityDC();
                    activity.RoleId = Convert.ToString(reader["RoleId"]);
                    activity.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    activity.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                    _activityList.Add(activity);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _activityList;
        }
    }
}
