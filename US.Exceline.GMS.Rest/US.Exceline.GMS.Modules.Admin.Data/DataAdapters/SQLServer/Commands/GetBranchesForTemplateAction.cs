﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/1/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetBranchesForTemplateAction : USDBActionBase<List<int>>
    {
        private int _templateId = -1;

        public GetBranchesForTemplateAction(int templateId)
        {
            _templateId = templateId;
        }

        protected override List<int> Body(System.Data.Common.DbConnection connection)
        {
            List<int> branchIdList = new List<int>();
            string SPGetItems = "USExceGMSAdminGetBranchesByTemplateNumber";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, SPGetItems);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@templateID", DbType.Int32, _templateId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int branchId = Convert.ToInt32(reader["ID"]);
                    branchIdList.Add(branchId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return branchIdList;
        }
    }
}
