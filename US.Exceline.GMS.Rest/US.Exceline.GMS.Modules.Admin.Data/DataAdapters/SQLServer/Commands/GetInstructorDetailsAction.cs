﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using System.IO;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetInstructorDetailsAction : USDBActionBase<List<InstructorDC>>
    {
        int _branchId = 0;
        string _user;
        private bool _isActive = true;

        public GetInstructorDetailsAction(int branchId, string User, bool isActive, int instructorId)
        {
            _branchId = branchId;
            _user = User;
            _isActive = isActive;

            if (!string.IsNullOrEmpty(_user))
            {
                string[] sTextArray = _user.Split(new char[] { ':' });
                if (sTextArray.Length == 1)
                {
                    string[] textParts = sTextArray[0].Split(new char[] { ' ' });
                    if (textParts.Length > 1)
                    {
                        _user = textParts[0] + " AND " + textParts[1];
                    }
                }
                else
                {
                    string[] textParts = sTextArray[1].Split(new char[] { ' ' });
                    if (textParts.Length > 1)
                    {
                        _user = textParts[0] + " AND " + textParts[1];
                    }

                }
            }
        }

        protected override List<InstructorDC> Body(DbConnection connection)
        {
            List<InstructorDC> instructorLst = new List<InstructorDC>();
            string StoredProcedureName = "USExceGMSAdminGetInstructors";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                if (!string.IsNullOrEmpty(_user))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeState", DbType.String, _isActive));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    InstructorDC instructer = new InstructorDC();
                    instructer.Id = Convert.ToInt32(reader["EmployeeId"]);
                    instructer.EntNo = Convert.ToInt32(reader["EntNo"]);
                    instructer.CustId = Convert.ToString(reader["CustId"]);
                    if (string.IsNullOrEmpty(_user))
                    {
                        instructer.BranchId = Convert.ToInt32(reader["BranchId"]);
                        instructer.BranchName = Convert.ToString(reader["BranchName"]);
                    }
                    else
                    {
                        instructer.BranchId = Convert.ToInt32(reader["HomeBranchId"]);
                    }
                    instructer.FirstName = reader["FirstName"].ToString();
                    instructer.LastName = reader["LastName"].ToString();
                    instructer.Name = instructer.FirstName + " " + instructer.LastName;
                    instructer.Email = reader["Email"].ToString();
                    instructer.BirthDay = Convert.ToDateTime(reader["BirthDate"]);
                    if (!string.IsNullOrEmpty(reader["Address1"].ToString()))
                    {
                        instructer.Address1 = Convert.ToString(reader["Address1"]);
                    }
                    if (!string.IsNullOrEmpty(reader["Address2"].ToString()))
                    {
                        instructer.Address2 = Convert.ToString(reader["Address2"]);
                    }
                    if (!string.IsNullOrEmpty(reader["Address3"].ToString()))
                    {
                        instructer.Address3 = Convert.ToString(reader["Address3"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ZipCode"].ToString()))
                    {
                        instructer.PostCode = Convert.ToString(reader["ZipCode"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ZipName"].ToString()))
                    {
                        instructer.PostPlace = Convert.ToString(reader["ZipName"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["TelMobile"])))
                    {
                        instructer.Mobile = Convert.ToString(reader["TelMobile"]).Trim();
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["TelWork"])))
                    {
                        instructer.Work = Convert.ToString(reader["TelWork"]).Trim();
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["TelHome"])))
                    {
                        instructer.Private = Convert.ToString(reader["TelHome"]).Trim();
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Gender"])))
                    {
                        instructer.Gender = Convert.ToString(reader["Gender"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Description"])))
                    {
                        instructer.Description = Convert.ToString(reader["Description"]);
                    }

                    instructer.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    instructer.ImagePath = Convert.ToString(reader["ImagePath"]);
                    instructer.RoleId = reader["RoleId"].ToString();
                    if (!string.IsNullOrEmpty(instructer.ImagePath))
                    {
                        instructer.ProfilePicture = GetMemberProfilePicture(instructer.ImagePath);
                    }
                    instructorLst.Add(instructer);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return instructorLst;
        }

        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }

        public InstructorDC RunOnTransation(DbTransaction dbTransaction, InstructorDC ins)
        {

            string StoredProcedureName = "USExceGMSAdminGetActivitiesForEmployee";
            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = dbTransaction.Connection;
                cmd.Transaction = dbTransaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, ins.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@roleId", DbType.String, ins.RoleId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ActivityDC ActivityItem = new ActivityDC();
                    ActivityItem.Id = Convert.ToInt32(reader["ID"]);
                    ActivityItem.Name = Convert.ToString(reader["Name"]);
                    ActivityItem.BranchId = Convert.ToInt32(reader["BranchId"]);
                    ActivityItem.IsActive = Convert.ToBoolean(reader["IsActive"]);

                    ins.ActivityList.Add(ActivityItem);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ins;

        }
    }
}