﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymEmployeeWorkPlanAction : USDBActionBase<List<GymEmployeeWorkPlanDC>>
    {
        private int _employeeId = -1;
      

        public GetGymEmployeeWorkPlanAction(int employeeId)
        {
            this._employeeId = employeeId;
        }
        protected override List<GymEmployeeWorkPlanDC> Body(System.Data.Common.DbConnection connection)
        {
            List<GymEmployeeWorkPlanDC> gymEmployeeWorkPlanList = new List<GymEmployeeWorkPlanDC>();
            string StoredProcedureName = "USExceGMSAdminGetGymEmployeeWorkPlan";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymemployeeId", DbType.Int32, _employeeId));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    GymEmployeeWorkPlanDC plan = new GymEmployeeWorkPlanDC();
                    plan.Id = Convert.ToInt32(reader["ID"]);
                    plan.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    plan.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    plan.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    plan.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    plan.Day = Convert.ToString(reader["Day"]);
                    plan.WeekType = Convert.ToInt32(reader["WeekType"]);
                    plan.Resource = Convert.ToString(reader["ResourceName"]);
                    plan.IsSalaryPerBooking = Convert.ToBoolean(reader["SalaryPerBooking"]);
                    plan.IsDefault = Convert.ToBoolean(reader["IsDefault"]);
                    plan.BranchId = Convert.ToInt32(reader["BranchId"]);
                    plan.BranchName = reader["BranchName"].ToString();
                   GymEmployeeWorkPlanDC plan2 = gymEmployeeWorkPlanList.Where(x => x.Id == plan.Id).SingleOrDefault();
                    if(plan2 == null)
                    {
                      gymEmployeeWorkPlanList.Add(plan);
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return gymEmployeeWorkPlanList;
        }
    }
}
