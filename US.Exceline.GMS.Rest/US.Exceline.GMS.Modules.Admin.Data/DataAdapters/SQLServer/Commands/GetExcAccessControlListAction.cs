﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetExcAccessControlListAction : USDBActionBase<List<ExcACCAccessControl>>
    {
        private readonly int _branchId = -1;

        public GetExcAccessControlListAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ExcACCAccessControl> Body(DbConnection connection)
        {
            var accAccessControlList = new List<ExcACCAccessControl>();
            const string storedProcedure = "USExceGMSAdminGetExcAccessControlList";
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var control = new ExcACCAccessControl
                    {
                        ID = Convert.ToInt32(reader["Id"]),
                        Name = Convert.ToString(reader["Name"])
                    };
                    accAccessControlList.Add(control);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return accAccessControlList;
        }
    }
}
