﻿// --------------------------------------------------------------------------
// Copyright(c) <2013> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : DKA
// Created Timestamp : 18/3/2013 10:39:08 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Collections.Generic;
namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class UpdateContractPriorityAction : USDBActionBase<int>
    {
        private Dictionary<int, int> _packages;
        public UpdateContractPriorityAction(Dictionary<int, int> packages)
        {
            this._packages = packages;
        }
        protected override int Body(DbConnection connection)
        {
            int contractId = -1;
            string StoredProcedureName = "USExceGMSAdminUpdateContractPriority";
            // DbTransaction transaction = connection.BeginTransaction();
            try
            {
                foreach (KeyValuePair<int, int> item in _packages)
                {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                //cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ID", DbType.Int32, item.Key));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Priority", DbType.Int32, item.Value));
                object obj = cmd.ExecuteScalar();
                contractId = Convert.ToInt32(obj);
                }
            }
            catch (Exception ex)
            {
                contractId = -1;
                // transaction.Rollback();
                throw ex;
            }
            // transaction.Commit();
            return contractId;
        }


        private DataTable GetDataTable(Dictionary<int, int> packages)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("Priority", typeof(int)));


            int count = 1;
            foreach (KeyValuePair<int, int> item in _packages)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["ID"] = Convert.ToInt32(item.Key);
                dataTableRow["Priority"] = Convert.ToInt32(item.Value);
                dataTable.Rows.Add(dataTableRow);
            }
            return dataTable;
        }
    }
}
