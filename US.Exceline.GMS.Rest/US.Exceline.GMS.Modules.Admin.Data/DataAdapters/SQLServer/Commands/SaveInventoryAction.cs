﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
  public class SaveInventoryAction : USDBActionBase<int>
  {
      private InventoryItemDC _inventoryItem;
      private string _user= string.Empty;
      public SaveInventoryAction(InventoryItemDC inventoryItem, string user)
      {
          _inventoryItem = inventoryItem;
          _user = user;
      }
        protected override int Body(DbConnection connection)
        {
            int inventoryId;
            const string storedProcedureName = "USExceGMSAdminSaveInventory";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _inventoryItem.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _inventoryItem.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _inventoryItem.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _inventoryItem.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ApprovedDate", DbType.DateTime, _inventoryItem.ApprovedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
               
                
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                inventoryId = Convert.ToInt32(output.Value);
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return inventoryId;
        }
    }
}
