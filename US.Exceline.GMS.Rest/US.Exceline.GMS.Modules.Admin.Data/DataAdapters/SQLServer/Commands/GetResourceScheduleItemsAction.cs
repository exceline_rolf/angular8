﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetResourceScheduleItemsAction : USDBActionBase<ScheduleDC>
    {
        private readonly int _entityId;
        private readonly string _gymCode = string.Empty;
        public GetResourceScheduleItemsAction(int entityId, string roleTpye, string gymCode)
        {
            _entityId = entityId;
            _gymCode = gymCode;

        }


        protected override ScheduleDC Body(DbConnection connection)
        {
            var entitySchedule = new ScheduleDC {EntityId  = _entityId, SheduleItemList = new ObservableCollection<ScheduleItemDC>()};

            const string storedProcedureName = "USExceGMSAdminGetResourceScheduleItems";
            DbDataReader reader =null;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId ", DbType.Int32, _entityId));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRole", DbType.String, _roleTpye));
                reader = cmd.ExecuteReader();

                

                while (reader.Read())
                {
                    var scheduleItem = new ScheduleItemDC();
                    scheduleItem.Id = Convert.ToInt32(reader["Id"]);
                    scheduleItem.ScheduleId = Convert.ToInt32(reader["ScheduleId"]);
                    scheduleItem.Day = reader["Day"].ToString();
                    scheduleItem.WeekType = Convert.ToInt32(reader["WeekType"]);
                    scheduleItem.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    scheduleItem.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    scheduleItem.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    scheduleItem.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    scheduleItem.WeekType = Convert.ToInt32(reader["WeekType"]);
                    scheduleItem.EmpId = Convert.ToInt32(reader["EmpId"]);
                    scheduleItem.EmpName = reader["EmpName"].ToString();
                    scheduleItem.ResourceId = Convert.ToInt32(reader["AdditionalResourceId"]);
                    scheduleItem.ResourcesName = reader["AdditionalResourceName"].ToString();
                    scheduleItem.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    scheduleItem.IsBooking = Convert.ToBoolean(reader["HasResBooking"]);

                    if (Convert.ToBoolean(reader["IsHasEmp"]))
                    {
                        try
                        {
                            var action = new GetEmpolyeeForScheduleItemAction(scheduleItem.Id);
                            scheduleItem.EmployeeList = action.Execute(EnumDatabase.Exceline, _gymCode);
                        }
                        catch
                        {
                        }

                    }

                    if (Convert.ToBoolean(reader["IsSwitchRes"]))
                    {
                        try
                        {
                            var actionSwitchTime = new GetSwitchTimeForScheduleItemAction(scheduleItem.Id);
                            scheduleItem.SwitchDataRange = actionSwitchTime.Execute(EnumDatabase.Exceline, _gymCode);
                        }
                        catch
                        {
                        }
                    }
                    entitySchedule.SheduleItemList.Add(scheduleItem);
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return entitySchedule;
        }
    }
}
