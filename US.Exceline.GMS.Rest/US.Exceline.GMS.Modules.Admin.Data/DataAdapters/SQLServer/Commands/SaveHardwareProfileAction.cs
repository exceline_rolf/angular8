﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveHardwareProfileAction : USDBActionBase<bool>
    {
        private ExceHardwareProfileDC _exceHardwareProfile;
        private int _branchId;

        public SaveHardwareProfileAction(ExceHardwareProfileDC exceHardwareProfile, int branchId)
        {
            _exceHardwareProfile = exceHardwareProfile;
            _branchId = branchId;
        }

        protected override bool Body(DbConnection connection)
        {
            bool _isSave = false;
            try
            {
                string storedProcedureName = "USExceGMSAdminSaveHardwareProfile";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ID", DbType.Int32, _exceHardwareProfile.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _exceHardwareProfile.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@HardwareProfileId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();
                int hardwareProfileId = Convert.ToInt32(param.Value);


                foreach (ExceHardwareProfileItemDC item in _exceHardwareProfile.HardwareProfileItemList)
                {
                    string storedProcedureName2 = "USExceGMSAdminSaveHardwareProfileItem";
                    DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName2);
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@ID", DbType.Int32, item.Id));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@HardwareProfileId", DbType.Int32, hardwareProfileId));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@CategoryId", DbType.Int32, item.CategoryId));
                    cmd2.ExecuteNonQuery();
                    _isSave = true;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isSave;
        }
    }
}
