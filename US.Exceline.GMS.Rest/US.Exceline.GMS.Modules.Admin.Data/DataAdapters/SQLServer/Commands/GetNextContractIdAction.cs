﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "4/17/2012 4:59:15 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class GetNextContractIdAction : USDBActionBase<string>
    {
        private int _branchId = -1;
        public GetNextContractIdAction(int barnchId)
        {
            _branchId = barnchId;
        }
        protected override string Body(DbConnection connection)
        {
            string contractId = string.Empty;
            string StoredProcedureName = "USExceGMSAdminGetNextContractId";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));
                object obj = cmd.ExecuteScalar();
                contractId = Convert.ToString(obj);
            }

            catch (Exception ex)
            {
                contractId = string.Empty;
                throw ex;
            }
            return contractId;
        }
    }
}
