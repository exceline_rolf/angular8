﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.Utils;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetJobScheduleItemsAction : USDBActionBase<List<ScheduleItemDC>>
    {
        private readonly int _branchId = 0;
        public GetJobScheduleItemsAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<ScheduleItemDC> Body(DbConnection connection)
        {
            List<ScheduleItemDC> sheduleItemList = new List<ScheduleItemDC>();

            const string storedProcedureName = "USExceGMSAdminGetJobScheduleItems";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId ", DbType.Int32, _branchId));
                // cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId ", DbType.Int32, _entityId));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRole", DbType.String, _roleTpye));


                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var scheduleItem = new ScheduleItemDC();
                    scheduleItem.Id = Convert.ToInt32(reader["Id"]);
                    scheduleItem.ScheduleId = Convert.ToInt32(reader["ScheduleId"]);
                    //  string occurrence = reader["Occurrence"].ToString();
                    //if (!string.IsNullOrEmpty(occurrence.Trim()))
                    //    scheduleItem.Occurrence = (ScheduleTypes) Enum.Parse(typeof (ScheduleTypes), occurrence, true);

                    scheduleItem.Day = reader["Day"].ToString();
                    scheduleItem.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    scheduleItem.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    scheduleItem.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    scheduleItem.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    scheduleItem.WeekType = Convert.ToInt32(reader["WeekType"]);
                    scheduleItem.EntityRoleType = reader["EntityRoleType"].ToString();
                    scheduleItem.EmpId = Convert.ToInt32(reader["EmpId"]);
                    scheduleItem.EmpName = reader["EmpName"].ToString();
                    scheduleItem.RoleId = Convert.ToInt32(reader["RoleId"]);
                    scheduleItem.RoleName = reader["RoleName"].ToString();
                    scheduleItem.ExcelineJob.JobCategoryId = Convert.ToInt32(reader["JobCategoryId"]);
                    scheduleItem.ExcelineJob.JobCategoryName = reader["JobCategoryName"].ToString();
                    scheduleItem.ExcelineJob.Title = reader["JobTitle"].ToString();
                    if (!String.IsNullOrEmpty(reader["ExtenedInfo"].ToString()))
                    {
                        ExtendedFieldInfoDC fieldInfo = new ExtendedFieldInfoDC();
                        fieldInfo = (ExtendedFieldInfoDC)XMLUtils.DesrializeXMLToObject(reader["ExtenedInfo"].ToString(), typeof(ExtendedFieldInfoDC));
                        scheduleItem.ExcelineJob.ExtendedFieldsList = fieldInfo.ExtendedFieldsList;
                    }
                    //scheduleItem.IsFixed = Convert.ToBoolean(reader["IsFixed"]);
                    //scheduleItem.LastActiveTimeGeneratedDate = Convert.ToDateTime(reader["LastActivetimeDate"]);

                    sheduleItemList.Add(scheduleItem);
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return sheduleItemList;
        }
    }
}
