﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetSwitchTimeForScheduleItemAction : USDBActionBase<Dictionary<int, List<DateTime>>>
    {
        private int _scheduleItemId;

        public GetSwitchTimeForScheduleItemAction(int scheduleItemId)
        {
            _scheduleItemId = scheduleItemId;
        }


        protected override Dictionary<int, List<DateTime>> Body(DbConnection connection)
        {

            var switchTimeLst = new Dictionary<int, List<DateTime>>();
            const string storedProcedureName = "USExceGMSAdminGetSwitchTimeForScheduleItem";
            DbDataReader reader = null;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItemId));
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int id = Convert.ToInt32(reader["ID"]);
                    var dateTimeLst = new List<DateTime>();
                    DateTime startDate = Convert.ToDateTime(reader["StartDate"]);
                    DateTime endDate = Convert.ToDateTime(reader["EndDate"]);

                    dateTimeLst.Add(startDate);
                    dateTimeLst.Add(endDate);
                      
                    switchTimeLst.Add(id, dateTimeLst);
                } 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return switchTimeLst;
       
        }
    }
}
