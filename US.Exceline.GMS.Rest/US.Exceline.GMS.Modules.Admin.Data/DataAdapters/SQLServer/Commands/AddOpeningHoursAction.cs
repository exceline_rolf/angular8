﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US.GMS.Core.DomainObjects.Admin.ManageOpeningHours;
using System.Data.Common;
using US_DataAccess;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class AddTimeScheduleAction : USDBActionBase<bool>
    {
        private ExcelineTimeScheduleDC _timeSchedule;


        public AddTimeScheduleAction(ExcelineTimeScheduleDC timeSchedule)
        {
            this._timeSchedule = timeSchedule;
        }

        protected override bool Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminAddTimeSchedule";
            bool returnValue = false;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityCategory", DbType.String, this._timeSchedule.ActivityCategory));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.String, this._timeSchedule.StartTime.ToShortTimeString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.String, this._timeSchedule.EndTime.ToShortTimeString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TimeInterval", DbType.Int32, this._timeSchedule.TimeInterval));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MonAvailStatus", DbType.Boolean, this._timeSchedule.MonAvailStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TueAvailStatus", DbType.Boolean, this._timeSchedule.TueAvailStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@WedAvailStatus", DbType.Boolean, this._timeSchedule.WedAvailStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ThuAvailStatus", DbType.Boolean, this._timeSchedule.ThuAvailStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FriAvailStatus", DbType.Boolean, this._timeSchedule.FriAvailStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SatAvailStatus", DbType.Boolean, this._timeSchedule.SatAvailStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SunAvailStatus", DbType.Boolean, this._timeSchedule.SunAvailStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, this._timeSchedule.Comment));                
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, this._timeSchedule.BranchId));
                cmd.ExecuteNonQuery();
                returnValue = true;
            }

            catch (Exception ex)
            {
                returnValue = false;
                throw ex;
            }
            
            return returnValue;
        }
    }
}
