﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymEmployeeApprovalsAction : USDBActionBase<List<GymEmployeeApprovalDC>>
    {
        private int _employeeId = -1;
        private string _approvalType = string.Empty;

        public GetGymEmployeeApprovalsAction(int employeeId, string approvalType)
        {
             _employeeId = employeeId;
            _approvalType = approvalType;
        }
        protected override List<GymEmployeeApprovalDC> Body(System.Data.Common.DbConnection connection)
        {
            List<GymEmployeeApprovalDC> approvalList = new List<GymEmployeeApprovalDC>();
            string StoredProcedureName = "USExceGMSAdminGetEmployeeApprovals";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _employeeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ApprovalType", DbType.String, _approvalType));
                
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    GymEmployeeApprovalDC approval = new GymEmployeeApprovalDC();
                    approval.Id = Convert.ToInt32(reader["Id"]);
                    approval.ScheduleItemId = Convert.ToInt32(reader["ScheduleItemId"]);
                    approval.Date = Convert.ToDateTime(reader["Date"]);
                    approval.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    approval.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    approval.ApprovalType = _approvalType;
                    approval.EmployeeId = _employeeId;
                    approval.BranchId = Convert.ToInt32(reader["BranchId"]);
                    approval.BranchName = reader["BranchName"].ToString();
                    approvalList.Add(approval);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return approvalList;
        }
    }
}
