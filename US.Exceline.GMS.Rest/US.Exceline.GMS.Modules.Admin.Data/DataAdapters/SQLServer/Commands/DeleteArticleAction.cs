﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
  public class DeleteArticleAction : USDBActionBase<int>
    {
        private readonly int _articleId;
        private readonly int _branchId;
        private readonly string _user = string.Empty;
        private readonly bool _isAdminUser;

        public DeleteArticleAction(int articleId, int branchId, string user,bool isAdminUser)
        {
            _articleId = articleId;
            _branchId = branchId;
            _user = user;
            _isAdminUser = isAdminUser;
        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            const string storedProcedureName = "USExceGMSAdminDeleteArticle";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, _articleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAdminUser", DbType.Boolean, _isAdminUser));
               
                cmd.ExecuteNonQuery();
                result = 1;
            }
            catch (Exception ex)
            {
                result = -1;
                throw ex;
            }
            return result;
        }
    }
}
