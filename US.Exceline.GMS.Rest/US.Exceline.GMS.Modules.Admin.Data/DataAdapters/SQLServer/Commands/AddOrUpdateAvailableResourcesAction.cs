﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class AddOrUpdateAvailableResourcesAction : USDBActionBase<bool>
    {
        private AvailableResourcesDC _resource;
        private int _branchId;
        private string _user;

        public AddOrUpdateAvailableResourcesAction(AvailableResourcesDC resource, int branchId, string user)
        {
            _resource = resource;
            _branchId = branchId;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSAdminAddUpdateAvailableResource";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, _resource.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@resourID", DbType.Int32, _resource.ResourID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@day", DbType.String, _resource.Day));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromTime", DbType.DateTime, _resource.FromTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toTime", DbType.DateTime, _resource.ToTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
              
                object obj = cmd.ExecuteScalar();
                result = true;
            }
            catch
            {
                throw;
            }
            return result;
        }
    }
}
