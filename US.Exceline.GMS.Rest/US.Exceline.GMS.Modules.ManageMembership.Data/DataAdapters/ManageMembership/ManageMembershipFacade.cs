﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.ManageMembership.Data.SystemObjects;
using US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.DomainObjects.AccessControl;
using US.Common.Notification.Core.DomainObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.ManageMembership
{
    public class ManageMembershipFacade
    {
        private static IManageMembershipDataAdapter GetDataAdapter()
        {
            return new SQLServerManageMembershipDataAdapter();
        }

        #region Manage Member
        public static string SaveMember(OrdinaryMemberDC member, string gymCode, string user)
        {
            return GetDataAdapter().SaveMember(member, gymCode, user);
        }

        public static bool ValidateMemberBrisId(int brisId,string gymCode)
        {
            return GetDataAdapter().ValidateMemberBrisId(brisId, gymCode);
        }

        public static List<OrdinaryMemberDC> GetGroupMemebersByGroup(int branchId, string searchText, bool IsActive, int groupId, string gymCode)
        {
            return GetDataAdapter().GetGroupMemebersByGroup(branchId, searchText, IsActive, groupId, gymCode);
        }
        public static bool AddMemberToGroup(int memberId, int groupId, string user, string gymCode)
        {
            return GetDataAdapter().AddMemberToGroup(memberId, groupId, user, gymCode);
        }

        public static bool RemoveMemberFromGroup(int memberId, int groupId, string user, string gymCode)
        {
            return GetDataAdapter().RemoveMemberFromGroup(memberId, groupId, user, gymCode);
        }

        public static string UpdateMember(OrdinaryMemberDC member, string gymCode, string user)
        {
            return GetDataAdapter().UpdateMember(member, gymCode,user);
        }

        public static bool UpdateMemberImagePath(int memberId, string imagePath, string gymCode)
        {
            return GetDataAdapter().UpdateMemberImagePath(memberId, imagePath, gymCode);
        }

        public static CreditNoteDC GetCreditNoteDetails(int arItemNo, string gymCode)
        {
            return GetDataAdapter().GetCreditNoteDetails(arItemNo, gymCode);
        }

        public static string GetInvoicePathByARitemNo(int arItemNo, string gymCode)
        {
            return GetDataAdapter().GetInvoicePathByARitemNo(arItemNo, gymCode);
        }

        public static bool DisableMember(int memberId, bool activeState, string comment, string gymCode, string user)
        {
            return GetDataAdapter().DisableMember(memberId, activeState, comment, gymCode, user);
        }

        public static PackageDC GetContractTemplateDetails(string type, int templateID, string gymCode)
        {
            return GetDataAdapter().GetContractTemplateDetails(type, templateID, gymCode);
        }

        public static List<int> GetMemberCount(string gymCode, int branchId)
        {
            return GetDataAdapter().GetMemberCount(gymCode, branchId);
        }
   
         public static bool MembercardVisit(int memberId, string user, string gymCode)
        {
            return GetDataAdapter().MembercardVisit(memberId, user, gymCode);
        }

        public static List<MemberForMemberlist> GetMemberList(FilterMemberList parameters, string user, string gymCode)
        {
            return GetDataAdapter().GetMemberList(parameters, user, gymCode);
        }

        public static List<ExcelineMemberDC> GetMembers(int branchId, string searchText, int statuse, MemberSearchType searchType, MemberRole memberRole, string user, string gymCode, int hit, bool isHeaderClick, bool isAscending, string sortName)
        {
            return GetDataAdapter().GetMemberDetails(branchId, searchText, statuse, searchType, memberRole, user,gymCode, hit, isHeaderClick, isAscending, sortName);
        }
        public static List<OrdinaryMemberDC> GetMembersByStatus(int statusId, string systemName, string user, string gymCode)
        {
            return GetDataAdapter().GetMembersByStatus( statusId,  systemName,  user,  gymCode);
        }

        public static List<PackageDC> GetContracts(int branchId, int contractType, string gymCode)
        {
            return GetDataAdapter().GetContractsList(branchId, contractType, gymCode);
        }

        public static bool SetContractSequenceID(List<PackageDC> contractList, int branchID, string gymCode)
        {
            return GetDataAdapter().SetContractSequenceID(contractList, branchID, gymCode);
        }

        public static ContractSaveResultDC SaveMemberContract(MemberContractDC memberContract, string gymCode, string user)
        {
            return GetDataAdapter().SaveMemberContract(memberContract, gymCode, user);
        }

        public static MemberContractDC GetMemberContractDetails(int contractId, int branchId, string gymCode)
        {
            return GetDataAdapter().GetMemberContractDetails(contractId, branchId, gymCode);
        }

        public static List<ContractItemDC> GetContractTemplateItems(int contractId, int branchId, string gymCode)
        {
            return GetDataAdapter().GetItemsForContractTemplate(contractId, branchId, gymCode);
        }

        public static List<InstallmentDC> GetInstallments(int memberContractId, string gymCode)
        {
            return GetDataAdapter().GetInstallments(memberContractId, gymCode);
        }

        public static bool RenewMemberContract(MemberContractDC renewedContract, List<InstallmentDC> installmentList, string gymCode, int branchId, bool isAutoRenew, string user)
        {
            return GetDataAdapter().RenewMemberContract(renewedContract, installmentList, gymCode, branchId, isAutoRenew, user);
        }

        public static bool UpdateMemberInstallment(InstallmentDC installment, string gymCode)
        {
            return GetDataAdapter().UpdateMemberInstallment(installment, gymCode);
        }

        public static EntityVisitDetailsDC GetMemberVisits(int memberId, DateTime selectedDate, int branchId, string gymCode, string user)
        {
            return GetDataAdapter().GetMemberVisits(memberId, selectedDate, branchId, gymCode, user);
        }

        public static bool UpdateMemberAddonInstallments(List<InstallmentDC> installments, string gymCode)
        {
            return GetDataAdapter().UpdateMemberAddonInstallments(installments, gymCode);
        }

        public static int SwitchPayer(int memberId, int payerId, DateTime activatedDate, bool isSave, string gymCode)
        {
            return GetDataAdapter().SwitchPayer(memberId, payerId, activatedDate, isSave, gymCode);
        }

        public static int SaveMemberVisit(EntityVisitDC memberVisit, string gymCode, string user)
        {
            return GetDataAdapter().SaveMemberVisit(memberVisit, gymCode, user);
        }

        public static bool DeleteMemberVisit(int memberVisitId, int memberContractId, string gymCode)
        {
            return GetDataAdapter().DeleteMemberVisit(memberVisitId, memberContractId, gymCode);
        }

        public static bool IntroducePayer(int memberId, int introduceId, string gymCode)
        {
            return GetDataAdapter().IntroducePayer(memberId, introduceId, gymCode);
        }

        public static bool CancelMemberContract(int memberContractId, string canceledBy, string comment, List<int> installmentIds, int minNumber, int tergetInstallmentId, string gymCode)
        {
            return GetDataAdapter().CancelMemberContract(memberContractId, canceledBy, comment, installmentIds, minNumber, tergetInstallmentId, gymCode);
        }

        public static List<MemberContractDC> GetMemberContractsForActivity(int memberId, int activityId, int branchId, string gymCode)
        {
            return GetDataAdapter().GetMemberContractsForActivity(memberId, activityId, branchId, gymCode);
        }

        public static List<ClassDetailDC> GetClassByMemberId(int branchId, int memberId, DateTime? classDate,string user, string gymCode)
        {
            return GetDataAdapter().GetClassByMemberId(branchId, memberId, classDate,user, gymCode);
        }

        public static bool AddFamilyMember(int memberId, int familyMemberId, int memberContractId, string gymCode)
        {
            return GetDataAdapter().AddFamilyMember(memberId, familyMemberId, memberContractId, gymCode);
        }

        public static List<InstallmentDC> AddFamilyMember(decimal installmentAmount, int updatingInstallmentNo, MemberContractDC memberContract, string gymCode)
        {
            return GetDataAdapter().UpdateInstallmentsWithContract(installmentAmount, updatingInstallmentNo, memberContract, gymCode);
        }

        public static bool SaveSponsor(int memberId, int sponsorId, int branchId, string user, string gymCode)
        {
            return GetDataAdapter().SaveSponsor(memberId, sponsorId, branchId, user, gymCode);
        }

        public static string SaveMemberParent(OrdinaryMemberDC memberParent, int branchId, string user, string gymCode)
        {
            return GetDataAdapter().SaveMemberParent(memberParent, branchId, user, gymCode);
        }

        public static OrdinaryMemberDC GetMemberDetailsByMemberId(int branchId, string user, int memberId, string memberRole, string gymCode)
        {
            return GetDataAdapter().GetMemberDetailsByMemberId(branchId, user, memberId, memberRole, gymCode);
        }

        public static List<ExcePaymentInfoDC> GetMemberPaymentsHistory(int memberId, string paymentType, string gymCode, int hit, string user)
        {
            return GetDataAdapter().GetMemberPaymentsHistory(memberId, paymentType, gymCode, hit, user);
        }

        public static List<CategoryDC> GetInterestCategoryByMember(int memberId, int branchId, string gymCode)
        {
            return GetDataAdapter().GetInterestCategoryByMember(memberId, branchId, gymCode);
        }

        public static bool SaveInterestCategoryByMember(int memberId, int branchId, List<CategoryDC> interestCategoryList, string gymCode)
        {
            return GetDataAdapter().SaveInterestCategoryByMember(interestCategoryList, memberId, branchId, gymCode);
        }

        public static List<OrdinaryMemberDC> GetSwitchPayers(int branchId, int memberId, string gymCode)
        {
            return GetDataAdapter().GetSwitchPayers(branchId, memberId, gymCode);
        }

        public static List<ExcelineMemberDC> GetIntroducedMembers(int memberId, int branchId, string gymCode)
        {
            return GetDataAdapter().GetIntroducedMembers(memberId, branchId, gymCode);
        }

        public static bool UpdateIntroducedMembers(int memberId, DateTime? creditedDate, string creditedText, bool isDelete, string gymCode)
        {
            return GetDataAdapter().UpdateIntroducedMembers(memberId, creditedDate, creditedText, isDelete, gymCode);
        }

        public static Dictionary<int, string> GetMemberStatus(string gymCode, bool getAllStatuses)
        {
            return GetDataAdapter().GetMemberStatus(gymCode, getAllStatuses);
        }

        public static bool DeleteCreditNote(int creditNoteId, string gymCode)
        {
            return GetDataAdapter().DeleteCreditNote(creditNoteId, gymCode);
        }
        #endregion

        #region Sponser Contract




        public static List<ExcelineMemberDC> GetMembersBySponsorId(int sponsorId, int branchId, string gymcode)
        {
            return GetDataAdapter().GetMembersBySponsorId(sponsorId, branchId, gymcode);
        }

        public static bool RemoveSponsorshipOfMember(int sponsoredRecordId, string gymCode , string user)
        {
            return GetDataAdapter().RemoveSponsorshipOfMember(sponsoredRecordId, gymCode, user);
        }


        public static List<SponsoredMemberDC> GetSponsoredMemberList(int branchId, int sponsorId, string gymcode)
        {
            return GetDataAdapter().GetSponsoredMemberList(branchId, sponsorId, gymcode);
        }

        public static List<int> GetSponsoredMemberListByTimePeriod(int sponsorId, Dictionary<int, List<DateTime>> sponsorMembers, string gymcode)
        {
            return GetDataAdapter().GetSponsoredMemberListByTimePeriod(sponsorId, sponsorMembers, gymcode);
        }

        public static List<DiscountDC> GetGroupDiscountByType(int branchId, string user, int discountTypeId, string gymCode, int sponsorId)
        {
            return GetDataAdapter().GetGroupDiscountByType(branchId, user, discountTypeId, gymCode, sponsorId);
        }

        public static List<EmployeeCategoryDC> GetSponsorEmployeeCategoryList(int branchId, string gymcode, int sponsorId)
        {
            return GetDataAdapter().GetSponsorEmployeeCategoryList(branchId, gymcode, sponsorId);
        }

        public static OrdinaryMemberDC GetEmployeeCategoryBySponsorId(int branchId, string gymcode, int sponsorId)
        {
            return GetDataAdapter().GetEmployeeCategoryBySponsorId(branchId, gymcode, sponsorId);
        }

        public static SponsorSettingDC GetSponsorSetting(int branchId, string user, int sponsorId, string gymCode)
        {
            return GetDataAdapter().GetSponsorSetting(branchId, user, sponsorId, gymCode);
        }
        public static bool SaveSponsorSetting(SponsorSettingDC sponsorSetting, int branchId, string user, string gymCode)
        {
            return GetDataAdapter().SaveSponsorSetting(sponsorSetting, branchId, user, gymCode);
        }
        public static bool DeleteDiscount(int discountID, string gymCode)
        {
            return GetDataAdapter().DeleteDiscount(discountID, gymCode);
        }
        public static bool AddSponserShipForMember(int memContractId, int sponserContractId, int employeeCategotyID, int actionType, string employeeRef, string gymCode)
        {
            return GetDataAdapter().AddSponserShipForMember(memContractId, sponserContractId, employeeCategotyID, actionType, employeeRef, gymCode);
        }
        public static bool DeleteEmployeeCategory(int categoryID, string gymCode)
        {
            return GetDataAdapter().DeleteEmployeeCategory(categoryID, gymCode);
        }
        public static List<MemberContractDC> GetContractsForSponsership(int activityId, int branchId, DateTime endDate, DateTime startDate, string gymCode)
        {
            return GetDataAdapter().GetContractsForSponsership(activityId, branchId, endDate, startDate, gymCode);
        }

        public static int SaveSponserContract(SponsorSettingDC sponserContract, int branchId, string user, string gymCode)
        {
            return GetDataAdapter().SaveSponserContract(sponserContract, branchId, user, gymCode);
        }

        public static bool CancelSponsorContract(int contractId, string user, string comment, string gymCode)
        {
            return GetDataAdapter().CancelSponsorContract(contractId, user, comment, gymCode);
        }
        public static List<OrdinaryMemberDC> GetMembersByRoleType(int branchId, string user, int status, MemberRole roleType, string searchText, string gymCode)
        {
            return GetDataAdapter().GetMembersByRoleType(branchId, user, status, roleType, searchText, gymCode);
        }
        public static List<MemberContractDC> GetSponserContractsByActivity(int sponserId, int activityId, int branchId, string gymCode)
        {
            return GetDataAdapter().GetSponserContractsByActivity(sponserId, activityId, branchId, gymCode);
        }
        #endregion

        #region Manage Discount
        public static int SaveDiscount(List<DiscountDC> discountList, string user, int branchId, int contractSettingID, string gymCode)
        {
            return GetDataAdapter().SaveDiscount(discountList, user, branchId, contractSettingID, gymCode);
        }

        public static List<DiscountDC> GetDiscountList(int branchId, DiscountType discountType, string gymCode)
        {
            return GetDataAdapter().GetDiscountList(branchId, discountType, gymCode);
        }

        public static List<DiscountDC> GetDiscountsForActivity(int branchId, int activityId, string gymCode)
        {
            return GetDataAdapter().GetDiscountsForActivity(branchId, activityId, gymCode);
        }

        public static ShopSalesDC GetMemberPerchaseHistory(int memberId, DateTime fromDate, DateTime toDate, string gymCode, int branchId, string user)
        {
            return GetDataAdapter().GetMemberPerchaseHistory(memberId, fromDate, toDate, gymCode, branchId, user);
        }

        public static List<MemberForMemberlist> GetVisitedMembers(string user, string gymCode)
        {
            return GetDataAdapter().GetVisitedMembers(user, gymCode);
        }


        #endregion

        #region Freeze Contract

        public static int SaveContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, string gymCode)
        {
            return GetDataAdapter().SaveContractFreezeItem(freezeItem, freezeInstallments, user, gymCode);
        }

        public static List<ContractFreezeItemDC> GetContractFreezeItems(int memberContractId, string gymCode)
        {
            return GetDataAdapter().GetContractFreezeItems(memberContractId, gymCode);
        }

        public static List<ContractFreezeInstallmentDC> GetContractFreezeInstallments(int freezeItemId, string gymCode)
        {
            return GetDataAdapter().GetContractFreezeInstallments(freezeItemId, gymCode);
        }

        public static List<InstallmentDC> GetInstallmentsToFreeze(int memberContractId, string gymCode)
        {
            return GetDataAdapter().GetInstallmentsToFreeze(memberContractId, gymCode);
        }

        public static int ExtendContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, string gymCode)
        {
            return GetDataAdapter().ExtendContractFreezeItem(freezeItem, freezeInstallments, user, gymCode);
        }

        public static int UnfreezeContract(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> unfreezeInstallments, string user, string gymCode)
        {
            return GetDataAdapter().UnfreezeContract(freezeItem, unfreezeInstallments, user, gymCode);
        }

        public static int IsFreezeAllowed(int memberContractId, int memberId, string gymCode, int branchId)
        {
            return GetDataAdapter().IsFreezeAllowed(memberContractId, memberId, gymCode, branchId);
        }

        #endregion

        #region Contract
        public static bool SaveMemberInstallments(List<InstallmentDC> installmentList, bool initialGenInstmnts, DateTime startDate, DateTime endDate, int memberContractId, int membercontractNo, int branchId, string gymCode, int renewedTemplateId)
        {
            return GetDataAdapter().SaveMemberInstallments(installmentList, initialGenInstmnts, startDate, endDate, memberContractId, membercontractNo, branchId, gymCode, renewedTemplateId);
        }
        public static List<ContractSummaryDC> GetFamilyMemberContracts(int memberId, int branchId, string gymCode, bool isFreezDetailNeeded)
        {
            return GetDataAdapter().GetFamilyMemberContracts(memberId, branchId, gymCode,  isFreezDetailNeeded);
        }

        public static int SaveNote(int branchId, string use, int memberId, string note, string gymCode)
        {
            return GetDataAdapter().SaveNote(branchId, use, memberId, note, gymCode);
        }

        public static OrdinaryMemberDC GetNote(int branchId, string user, int memberId, string gymCode)
        {
            return GetDataAdapter().GetNote(branchId, user, memberId, gymCode);
        }

        public static List<ExcelineMemberDC> GetGroupMembers(int groupId, string gymCode)
        {
            return GetDataAdapter().GetGroupMembers(groupId, gymCode);
        }


        public static List<ExcelineMemberDC> GetFamilyMembers(int memberId, string gymCode)
        {
            return GetDataAdapter().GetFamilyMembers(memberId, gymCode);
        }

        public static List<FollowUpDC> GetFollowUps(int memberId, int followUpId, string gymCode, string user)
        {
            return GetDataAdapter().GetFollowUps(memberId, followUpId, gymCode, user);
        }

        public static bool SaveFollowUpPopup(int followUpId, String gymCode)
        {
            return GetDataAdapter().SaveFollowUpPopup(followUpId, gymCode);
        }

        public static List<FollowUpTemplateTaskDC> GetFollowUpTask(string gymCode)
        {
            return GetDataAdapter().GetFollowUpTask(gymCode);
        }

        public static int SaveFollowUp(List<FollowUpDC> followUpList, string gymCode, string user)
        {
            return GetDataAdapter().SaveFollowUp(followUpList, gymCode, user);
        }

        public static List<ExcelineMemberDC> GetListOfGuardian(int guardianId, string gymCode)
        {
            return GetDataAdapter().GetListOfGuardian(guardianId, gymCode);
        }

        public static double ChangePrePaidAccount(string increase, float amount, int memberId, int branchId, string gymCode, string user)
        {
            return GetDataAdapter().ChangePrePaidAccount(increase, amount, memberId, branchId, gymCode, user);
        }

        public static bool SaveEconomyDetails(OrdinaryMemberDC member, string gymCode, string user)
        {
            return GetDataAdapter().SaveEconomyDetails(member, gymCode, user);
        }

        public static OrdinaryMemberDC GetEconomyDetails(int memberId, string gymCode)
        {
            return GetDataAdapter().GetEconomyDetails(memberId, gymCode);
        }

        public static string SaveMemberInfoWithNotes(OrdinaryMemberDC member, string gymCode)
        {
            return GetDataAdapter().SaveMemberInfoWithNotes(member, gymCode);
        }

        public static OrdinaryMemberDC GetMemberInfoWithNotes(int branchId, int memberId, string gymCode)
        {
            return GetDataAdapter().GetMemberInfoWithNotes(branchId, memberId, gymCode);
        }

        public static List<ContractBookingDC> GetContractBookings(int membercontractId, string gymCode)
        {
            return GetDataAdapter().GetContractBookings(membercontractId, gymCode);
        }

        public static int UpdateMemberContract(MemberContractDC memberContract, string gymCode, string user)
        {
            return GetDataAdapter().UpdateContract(memberContract, gymCode, user);
        }

        public static bool DeleteInstallment(List<int> installmentIdList, int memberContractId, string gymCode, string user)
        {
            return GetDataAdapter().DeleteInstallment(installmentIdList, memberContractId, gymCode, user);
        }

        public static bool ResignContract(ContractResignDetailsDC resignDetails, string gymCode, string user)
        {
            return GetDataAdapter().ResignContract(resignDetails, gymCode, user);
        }

        public static List<InstallmentDC> GetMemberOrders(int memberId, string type, string gymCode, string user)
        {
            return GetDataAdapter().GetMemberOrders(memberId, type, gymCode, user);
        }
        #endregion

        public static List<ExcelineMemberDC> GetContractMembers(int memberContractId, string gymCode)
        {
            return GetDataAdapter().GetContractMembers(memberContractId, gymCode);
        }

        public static bool UpdateContractOrder(List<InstallmentDC> installmentList, string gymCode, string user)
        {
            return GetDataAdapter().UpdateContractOrder(installmentList, gymCode, user);
        }

        public static bool RenewContract(int memberContractId, int branchId, string gymCode)
        {
            return GetDataAdapter().RenewContract(memberContractId, branchId, gymCode);
        }



        public static InstallmentDC GetMemberContractLastInstallment(int memberContractId, string gymCode)
        {
            return GetDataAdapter().GetMemberContractLastInstallment(memberContractId, gymCode);
        }

        public static bool SaveRenewContractDetails(RenewSummaryDC renewSummary, int branchId, string gymCode, string user)
        {
            return GetDataAdapter().SaveRenewContractDetails(renewSummary, branchId, gymCode,user);
        }

        public static int SaveContractGroupMembers(int groupId, List<ExcelineMemberDC> groupMemberList, int contractId, string gymCode, string user)
        {
            return GetDataAdapter().SaveContractGroupMembers(groupId, groupMemberList, contractId, gymCode, user);
        }

        public static bool CheckBookingAvailability(int resourceID, string day, DateTime startTime, DateTime endTime, string gymCode)
        {
            return GetDataAdapter().CheckBookingAvailability(resourceID, day, startTime, endTime, gymCode);
        }

        public static int AddInstallment(InstallmentDC installment, string gymCode, string user)
        {
            return GetDataAdapter().AddInstallment(installment, gymCode, user);
        }

        public static int AddBlancInstallment(int MemberId, int BranchId, string gymCode, string user)
        {
            return GetDataAdapter().AddBlancInstallment(MemberId, BranchId, gymCode, user);
        }

        public static string GetMemberSearchCategory(int branchId, string gymCode)
        {
            return GetDataAdapter().GetMemberSearchCategory(branchId, gymCode);
        }

        public static List<RenewContractDC> GetContractsForAutoRenew(int branchId, string gymCode)
        {
            return GetDataAdapter().GetContractsForAutoRenew(branchId, gymCode);
        }
        public static bool SaveRenewMemberContractItem(List<ContractItemDC> itemList, int memberContractId, int branchId, string gymCode)
        {
            return GetDataAdapter().SaveRenewMemberContractItem(itemList, memberContractId, branchId, gymCode);
        }

        public static bool SaveDocumentData(DocumentDC document, string username, int branchId)
        {
            return GetDataAdapter().SaveDocumentData(document, username, branchId);
        }

        public static List<DocumentDC> GetDocumentData(bool isActive, string custId, string searchText, int documentType, string gymCode, int branchId)
        {
            return GetDataAdapter().GetDocumentData(isActive, custId, searchText, documentType, gymCode, branchId);
        }

        public static bool DeleteDocumentData(int docId, string username, int branchId)
        {
            return GetDataAdapter().DeleteDocumentData(docId, username, branchId);
        }

        public static bool SetPDFFileParth(int id, string fileParth, string flag, int branchId, string GymCode, string user)
        {
            return GetDataAdapter().SetPDFFileParth(id, fileParth, flag, branchId, GymCode, user);
        }

        public static CustomerBasicInfoDC GetMemberBasicInfo(int memberId, string gymcode)
        {
            return GetDataAdapter().GetMemberBasicInfo(memberId, gymcode);
        }

        public static int GetVisitCount(int memberId, DateTime startDate, DateTime endDate, string gymCode)
        {
            return GetDataAdapter().GetVisitCount(memberId, startDate, endDate, gymCode);
        }

        public static List<InstallmentDC> MergeOrders(List<InstallmentDC> updatedOrders, List<int> removedOrder, int memberContractId, string gymCode, string user)
        {
            return GetDataAdapter().MergeOrders(updatedOrders, removedOrder, memberContractId, gymCode, user);
        }

        public static List<FollowUpDetailDC> GetFollowUpDetailByEmpId(int empId, string gymCode)
        {
            return GetDataAdapter().GetFollowUpDetailByEmpId(empId, gymCode);
        }

        public static List<MemberBookingDC> GetMemberBookings(int memberId, string gymCode, string user)
        {
            return GetDataAdapter().GetMemberBookings(memberId, gymCode, user);
        }

        public static List<ExcelineMemberDC> GetOtherMembersBooking(int scheduleId, string gymCode)
        {
            return GetDataAdapter().GetOtherMembersForBooking(scheduleId, gymCode);
        }

        public static List<ExceACCMember> GetMemberDetailsForCard(string cardNo, string accessType, string gymCode)
        {
            return GetDataAdapter().GetMemberDetailsForCard(cardNo, accessType, gymCode);
        }


        public static List<ExceACCMember> GetMemberDetailsForExcAccessController(string cardNo,int branchId, string gymCode)
        {
            return GetDataAdapter().GetMemberDetailsForExcAccessController(cardNo,branchId, gymCode);
        }

      
        public static List<InstallmentDC> GetSponsorOrders(List<int> sponsors, string gymCode)
        {
            return GetDataAdapter().GetSponsorOrders(sponsors, gymCode);
        }

        public static bool AddUpdateMemberIntegrationSettingsAction(MemberIntegrationSettingDC memberIntegrationSettingsDetail, string gymCode)
        {
            return GetDataAdapter().AddUpdateMemberIntegrationSettings(memberIntegrationSettingsDetail, gymCode);
        }

        public static List<MemberIntegrationSettingDC> GetMemberIntegrationSettings(int branchId, int memberId, string gymCode)
        {
            return GetDataAdapter().GetMemberIntegrationSettings(branchId, memberId, gymCode);
        }

        public static int CheckContractGymChange(int memberID, int memberContractID, int oldBranchID, int newBranchID, string gymCode)
        {
            return GetDataAdapter().CheckContractGymChange(memberID, memberContractID, oldBranchID, newBranchID, gymCode);
        }

        public static int ValidateChangeGymInMember(int currentBranchID, int newBranchID, int branchID, int memberID, string user, string gymCode)
        {
            return GetDataAdapter().ValidateChangeGymInMember(currentBranchID, newBranchID, branchID, memberID, user, gymCode);
        }

        public static ArticleDC GetTimeMachineArticle(string gymCode)
        {
            return GetDataAdapter().GetTimeMachineArticle(gymCode);
        }

        public static bool UpdateShopItemAndGatPurchase(GatpurchaseShopItem gatPurchaseShopItem)
        {
            return GetDataAdapter().UpdateShopItemAndGatPurchase(gatPurchaseShopItem);
        }

        public static int ValidateMemberWithStatus(int memberId, int statusId, string user, string gymCode)
        {
            return GetDataAdapter().ValidateMemberWithStatus(memberId, statusId, user, gymCode);
        }

        public static int GetPriceChangeContractCount(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate)
        {
            return GetDataAdapter().GetPriceChangeContractCount(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, gymCode, changeToNextTemplate);
        }

        public static bool UpdatePrice(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate, string user)
        {
            return GetDataAdapter().UpdatePrice(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, gymCode, changeToNextTemplate, user);
        }

        public static List<ExcelineContractDC> GetPriceUpdateContracts(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode,bool changeToNextTemplate, int hit)
        {
            return GetDataAdapter().GetPriceUpdateContracts(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, gymCode, changeToNextTemplate, hit);
        }

        public static List<PackageDC> GetTemplatesForPriceUpdate(string gymCode)
        {
            return GetDataAdapter().GetTemplatesForPriceUpdate(gymCode);
        }

        public static bool InfoGymUpdateExceMember(string infoGymId, int memberId, string user)
        {
            return GetDataAdapter().InfoGymUpdateExceMember(infoGymId, memberId, user);
        }

        public static List<ContractSummaryDC> GetContractSummaries(int memberId, int branchId, string gymCode, bool isFreezDetailNeeded, string user, bool isFreezdView)
        {
            return GetDataAdapter().GetContractSummaries(memberId, branchId, gymCode, isFreezDetailNeeded, user, isFreezdView);
        }

        public static List<ContractSummaryDC> GetContractSummariesByEmployee(int employeeID, int branchId, DateTime createdDate, string gymCode)
        {
            return GetDataAdapter().GetContractSummariesByEmployee(employeeID, branchId, createdDate, gymCode);
        }

        public static DateTime? ValidateSponsoringGeneration(string gymCode)
        {
            return GetDataAdapter().ValidateSponsoringGeneration(gymCode);
        }

        public static bool UpdateBRISMemberData(OrdinaryMemberDC member, string gymCode)
        {
            return GetDataAdapter().UpdateBRISMemberData(member, gymCode);
        }

        public static string GetCountryCode(string landCode, string gymCode)
        {
            return GetDataAdapter().GetCountryCode(landCode, gymCode);
        }

        public static bool UpdateAgressoId(int memberID, int aggressoID, string gymCode)
        {
            return GetDataAdapter().UpdateAgressoId(memberID, aggressoID, gymCode);
        }

        public static bool UpdateBRISStatus(int brisID, string message, string response, string status, string gymCode)
        {
            return GetDataAdapter().UpdateBRISStatus(brisID, message, response, status, gymCode);
        }

        public static bool RemoveContractResign(int memberContractId, string user, string gymCode)
        {
            return GetDataAdapter().RemoveContractResign(memberContractId, user, gymCode);
        }

        public static bool UpdateInvoicePdfPath(int arItemNo, string invoiceFilePath, string gymCode)
        {
            return GetDataAdapter().UpdateInvoicePdfPath(arItemNo, invoiceFilePath, gymCode);
        }

        public static bool ImageChangedAddEvent(USNotificationTree notification, string gymcode, string user)
        {
            return GetDataAdapter().ImageChangedAddEvent(notification, gymcode, user);
        }

        public static MemberContractPrintInfo GetMemberContract(int contractId, string gymcode)
        {
            return GetDataAdapter().GetMemberContract(contractId, gymcode);
        }

        public static List<MonthlyServicesForContract> GetMonthlyService(int contractId, string gymcode)
        {
            return GetDataAdapter().GetMonthlyService(contractId, gymcode);
        }

        public static bool RegisterChangeOfInvoiceDueDate(AlterationInDueDateData changeData, String user, String gymCode)
        {
            return GetDataAdapter().RegisterChangeOfInvoiceDueDate( changeData,  user,  gymCode);
        }

    }
}
