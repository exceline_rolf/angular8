﻿using System;
using System.Collections.Generic;
using System.Linq;
using US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands;
using US.Exceline.GMS.Modules.ManageMembership.Data.SystemObjects;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.Utils;
using US.GMS.Data.DataAdapters.SQLServer.Commands;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.Economy;
using US.Common.Notification.Core.DomainObjects;


namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer
{
    public class SQLServerManageMembershipDataAdapter : IManageMembershipDataAdapter
    {
        #region IMembershiManagementDataAdapter Members

        public string SaveMember(OrdinaryMemberDC member, string gymCode, string user)
        {
            string _memberNo = "-1";
            int memberId = -1;
            SaveMemberAction action = new SaveMemberAction(member, user);
            _memberNo = action.Execute(EnumDatabase.Exceline, gymCode);
            string[] sTextArray = _memberNo.Split(new char[] { ':' });
            if (sTextArray.Length > 0)
            {
                memberId = Convert.ToInt32(sTextArray[0]);
            }
            if (member != null)
            {
                if (member.Role == MemberRole.MEM && memberId > 0)
                {
                    if (member.IntroduceMemberLst != null && member.IntroduceMemberLst.Count > 0)
                    {
                        foreach (ExcelineMemberDC introduceMember in member.IntroduceMemberLst)
                        {
                            AddIntroducerToMemberAction addMemberAction = new AddIntroducerToMemberAction(introduceMember.Id, memberId);
                            addMemberAction.Execute(EnumDatabase.Exceline, gymCode);
                        }
                    }
                    if (member.GroupId > 0)
                    {
                        try
                        {
                            GetGroupMembersAction actionGetGroupMember = new GetGroupMembersAction(member.GroupId);
                            member.GroupMemberList = actionGetGroupMember.Execute(EnumDatabase.Exceline, gymCode);
                            member.GroupMemberLstCount = member.GroupMemberList.Count();
                            return _memberNo = string.Format("{0}:{1}", _memberNo, member.GroupMemberLstCount);
                        }
                        catch
                        {
                            // throw;
                        }
                    }
                }
            }

            if (member != null)
            {
                if (member.Role == MemberRole.COM)
                {
                    if (member.GroupMemberList != null && member.GroupMemberList.Count > 0)
                    {
                        foreach (ExcelineMemberDC grpMember in member.GroupMemberList)
                        {
                            AddMemberToGroupAction addMemberAction = new AddMemberToGroupAction(grpMember.Id, memberId, string.Empty);
                            addMemberAction.Execute(EnumDatabase.Exceline, gymCode);
                        }
                    }
                }
            }


            return _memberNo;
        }

        public bool ValidateMemberBrisId(int brisId, string gymCode)
        {
            var action = new ValidateMemberBrisIdAction(brisId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public string UpdateMember(OrdinaryMemberDC member, string gymCode, string user)
        {
            string memberNo = "-1";
            UpdateMemberAction action = new UpdateMemberAction(member, user);
            memberNo = action.Execute(EnumDatabase.Exceline, gymCode);
            return memberNo;
        }

        public bool UpdateMemberImagePath(int memberId, string imagePath, string gymCode)
        {
            UpdateMemberImagePathAction action = new UpdateMemberImagePathAction(memberId, imagePath);
            return action.Execute(EnumDatabase.Exceline, gymCode);

        }

        public bool DisableMember(int memberId, bool activeState, string comment, string gymCode, string user)
        {
            bool result = false;
            DisableMemberAction action = new DisableMemberAction(memberId, activeState, comment, user);
            result = action.Execute(EnumDatabase.Exceline, gymCode);
            return result;
        }

        public List<int> GetMemberCount(string gymCode, int branchId)
        {
            List<int> memberCounts = new List<int>();
            GetMemberCountAction action = new GetMemberCountAction(branchId);
            memberCounts = action.Execute(EnumDatabase.Exceline, gymCode);
            return memberCounts;
        }
        
         public bool MembercardVisit(int memberId, string user, string gymCode)
        {
            bool returnVal;
            MembercardVisitAction action = new MembercardVisitAction(memberId, user);
            returnVal = action.Execute(EnumDatabase.Exceline, gymCode);
            return returnVal;
        }

        public List<MemberForMemberlist> GetMemberList(FilterMemberList parameter, string user, string gymCode)
        {
            List<MemberForMemberlist> memberLst = new List<MemberForMemberlist>();
            GetMemberListAction action = new GetMemberListAction(parameter);
            memberLst = action.Execute(EnumDatabase.Exceline, gymCode);
            return memberLst;
        }


        public List<ExcelineMemberDC> GetMemberDetails(int branchId, string searchText, int statuse, MemberSearchType searchType, MemberRole memberRole, string user, string gymCode, int hit, bool isHeaderClick, bool isAscending, string sortName)
        {
            List<ExcelineMemberDC> memberLst = new List<ExcelineMemberDC>();
            GetMemberDetailsAction action = new GetMemberDetailsAction(branchId, searchText, statuse, searchType, memberRole, user, hit, isHeaderClick, isAscending, sortName);
            memberLst = action.Execute(EnumDatabase.Exceline, gymCode);
            return memberLst;
        }

        public List<OrdinaryMemberDC> GetMembersByStatus(int statusId, string systemName, string user, string gymCode)
        {
            List<OrdinaryMemberDC> memberLst = new List<OrdinaryMemberDC>();
            GetMembersByStatusAction action = new GetMembersByStatusAction( statusId,  systemName,  user,  gymCode);
            memberLst = action.Execute(EnumDatabase.Exceline, gymCode);
            return memberLst;
        }


        public bool SaveRenewMemberContractItem(List<ContractItemDC> itemList, int memberContractId, int branchId, string gymCode)
        {
            bool result = false;
            foreach (ContractItemDC item in itemList)
            {
                SaveRenewMemberContractItemAction action = new SaveRenewMemberContractItemAction(item, memberContractId, branchId);
                result = action.Execute(EnumDatabase.Exceline, gymCode);
            }
            return result;
        }


        public List<PackageDC> GetContractsList(int branchId, int contractType, string gymCode)
        {
            GetContractsAction action = new GetContractsAction(branchId, contractType);
            List<PackageDC> packageList = action.Execute(EnumDatabase.Exceline, gymCode);
            foreach (PackageDC package in packageList)
            {
                GetContractTemplateItemsAction itemAction = new GetContractTemplateItemsAction(package.PackageId, branchId);
                List<ContractItemDC> items = itemAction.Execute(EnumDatabase.Exceline, gymCode);
                foreach (ContractItemDC contractItem in items)
                {
                    if (contractItem.IsStartUpItem)
                        package.StartUpItemList.Add(contractItem);
                    else
                        package.EveryMonthItemList.Add(contractItem);
                }
            }
            return packageList;
        }

        public bool SetContractSequenceID(List<PackageDC> contractList, int branchID, string gymCode)
        {
            SetContractSequenceIDAction action = new SetContractSequenceIDAction(contractList, branchID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public PackageDC GetContractTemplateDetails(string type, int templateID, string gymCode)
        {
            PackageDC package = new PackageDC();
            GetContractTemplateDetailsAction action = new GetContractTemplateDetailsAction(type, templateID);
            package = action.Execute(EnumDatabase.Exceline, gymCode);
            if (package != null)
            {
                GetContractTemplateItemsAction itemAction = new GetContractTemplateItemsAction(package.PackageId, 1); // branch is is useless
                List<ContractItemDC> items = itemAction.Execute(EnumDatabase.Exceline, gymCode);
                foreach (ContractItemDC contractItem in items)
                {
                    if (contractItem.IsStartUpItem)
                        package.StartUpItemList.Add(contractItem);
                    else
                        package.EveryMonthItemList.Add(contractItem);
                }
            }
            return package;
        }

        public CreditNoteDC GetCreditNoteDetails(int arItemNo, string gymCode)
        {
            GetCreditNotesDetails action = new GetCreditNotesDetails(arItemNo, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
           
        }

        public string GetInvoicePathByARitemNo(int arItemNo, string gymCode)
        {
            GetInvoicePathByARitemNoAction action = new GetInvoicePathByARitemNoAction(arItemNo);
            return action.Execute(EnumDatabase.Exceline, gymCode);

        }


        public bool DeleteCreditNote(int creditNoteId, string gymCode)
        {
            DeleteCreditNoteAction action = new DeleteCreditNoteAction(creditNoteId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public List<PackageDC> GetContracts(int branchId, int contractType, string gymCode)
        {
            GetContractsAction action = new GetContractsAction(branchId, contractType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ContractSaveResultDC SaveMemberContract(MemberContractDC memberContract, string gymCode, string user)
        {
            SaveMemberContractAction action = new SaveMemberContractAction(memberContract, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public InstallmentDC GetMemberContractLastInstallment(int memberContractId, string gymCode)
        {
            GetMemberContractLastInstallmentAction action = new GetMemberContractLastInstallmentAction(memberContractId, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public MemberContractDC GetMemberContractDetails(int contractId, int branchId, string gymCode)
        {
            GetMemberContractDetailsAction action = new GetMemberContractDetailsAction(contractId, branchId, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ContractItemDC> GetItemsForContractTemplate(int contractId, int branchId, string gymCode)
        {
            GetContractTemplateItemsAction action = new GetContractTemplateItemsAction(contractId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool RenewMemberContract(MemberContractDC renewedContract, List<InstallmentDC> installmentList, string gymCode, int branchId, bool isAutoRenew, string user)
        {

            RenewMemberContractAction action = new RenewMemberContractAction(renewedContract, installmentList, branchId, gymCode, isAutoRenew, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveRenewContractDetails(RenewSummaryDC renewSummary, int branchId, string gymCode, string user)
        {
            SaveRenewContractDetailsAction action = new SaveRenewContractDetailsAction(renewSummary, branchId, gymCode, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public EntityVisitDetailsDC GetMemberVisits(int memberId, DateTime selectedDate, int branchId, string gymCode, string user)
        {
            GetMemberVisitsAction action = new GetMemberVisitsAction(memberId, selectedDate, branchId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        
        public List<MemberForMemberlist> GetVisitedMembers(string user, string gymCode)
        {
            GetVisitedMembersAction action = new GetVisitedMembersAction(user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveMemberVisit(EntityVisitDC memberVisit, string gymCode, string user)
        {
            SaveMemberVisitAction action = new SaveMemberVisitAction(memberVisit, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool DeleteMemberVisit(int memberVisitId, int memberContractId, string gymCode)
        {
            DeleteMemberVisitAction action = new DeleteMemberVisitAction(memberVisitId, memberContractId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<InstallmentDC> UpdateInstallmentsWithContract(decimal installmentAmount, int updatingInstallmentNo, MemberContractDC memberContract, string gymCode)
        {
            UpdateInstallmentsAndContractAction action = new UpdateInstallmentsAndContractAction(installmentAmount, updatingInstallmentNo, memberContract);
            bool status = action.Execute(EnumDatabase.Exceline, gymCode);

            if (status)
            {
                List<InstallmentDC> installments = GetInstallments(memberContract.Id, gymCode);
                return installments;
            }
            else
            {
                return new List<InstallmentDC>();
            }
        }


        public OrdinaryMemberDC GetMemberDetailsByMemberId(int branchId, string user, int memberId, string memberRole, string gymCode)
        {
            OrdinaryMemberDC member = new OrdinaryMemberDC();
            GetMemberDetailsByMemberIdAction action = new GetMemberDetailsByMemberIdAction(branchId, user, memberId, memberRole, gymCode);
            member = action.Execute(EnumDatabase.Exceline, gymCode);
            return member;
        }

        public List<ExcePaymentInfoDC> GetMemberPaymentsHistory(int memberId, string paymentType, string gymCode, int hit, string user)
        {
            GetMemberPaymentsHistoryAction action = new GetMemberPaymentsHistoryAction(memberId, paymentType, hit, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Manage Member
        public List<InstallmentDC> GetInstallments(int memberContractId, string gymCode)
        {
            GetInstallmentsAction insAction = new GetInstallmentsAction(memberContractId, gymCode);
            return insAction.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<OrdinaryMemberDC> GetGroupMemebersByGroup(int branchId, string searchText, bool IsActive, int groupId, string gymCode)
        {
            try
            {
                GetGroupMemebersAction action = new GetGroupMemebersAction(branchId, searchText, IsActive, groupId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool UpdateMemberInstallment(InstallmentDC installment, string gymCode)
        {
            UpdateMemberInstallment action = new UpdateMemberInstallment(installment);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateMemberAddonInstallments(List<InstallmentDC> installments, string gymCode)
        {
            bool isUpdated = false;
            foreach (InstallmentDC installment in installments)
            {
                UpdateMemberInstallment action = new UpdateMemberInstallment(installment);
                isUpdated = action.Execute(EnumDatabase.Exceline, gymCode);
            }
            return isUpdated;
        }

        public int SwitchPayer(int memberId, int payerId, DateTime activatedDate, bool isSave, string gymCode)
        {
            SwitchPayerAction action = new SwitchPayerAction(memberId, payerId, activatedDate, isSave);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool IntroducePayer(int memberId, int introduceId, string gymCode)
        {
            IntroducePayerAction action = new IntroducePayerAction(memberId, introduceId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool CancelMemberContract(int memberContractId, string canceledBy, string comment, List<int> installmentIds, int minNumber, int tergetInstallmentId, string gymCode)
        {
            CancelMemberContractAction action = new CancelMemberContractAction(memberContractId, canceledBy, comment, installmentIds, minNumber, tergetInstallmentId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<MemberContractDC> GetMemberContractsForActivity(int memberId, int activityId, int branchId, string gymCode)
        {
            GetMemberContractsForActivityAction action = new GetMemberContractsForActivityAction(memberId, activityId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ClassDetailDC> GetClassByMemberId(int branchId, int memberId, DateTime? classDate,string user, string gymCode)
        {
            GetClassesForMemberAction action = new GetClassesForMemberAction(branchId, memberId,classDate, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddFamilyMember(int memberId, int familyMemberId, int memberContractId, string gymCode)
        {
            SaveFamilyMemberAction action = new SaveFamilyMemberAction(memberId, familyMemberId, memberContractId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveSponsor(int memberId, int sponsorId, int branchId, string user, string gymCode)
        {
            SaveSponsorAction action = new SaveSponsorAction(memberId, sponsorId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public string SaveMemberParent(OrdinaryMemberDC memberParent, int branchId, string user, string gymCode)
        {
            SaveMemberParentAction action = new SaveMemberParentAction(memberParent, branchId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<CategoryDC> GetInterestCategoryByMember(int memberId, int branchId, string gymCode)
        {
            GetInterestCategoryByMemberAction action = new GetInterestCategoryByMemberAction(memberId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveInterestCategoryByMember(List<CategoryDC> InterestCategoryList, int memberId, int branchId, string gymCode)
        {
            SaveInterestCategoryByMemberAction action = new SaveInterestCategoryByMemberAction(memberId, branchId, InterestCategoryList);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<OrdinaryMemberDC> GetSwitchPayers(int branchId, int memberId, string gymCode)
        {
            GetSwitchPayersAction action = new GetSwitchPayersAction(branchId, memberId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ContractBookingDC> GetContractBookings(int membercontractId, string gymCode)
        {
            GetContractBookingsAction action = new GetContractBookingsAction(membercontractId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineMemberDC> GetIntroducedMembers(int memberId, int branchId, string gymCode)
        {
            GetIntroducedMembersAction action = new GetIntroducedMembersAction(memberId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateIntroducedMembers(int memberId, DateTime? creditedDate, string creditedText, bool isDelete, string gymCode)
        {
            try
            {
                UpdateIntroducedMemberAction action = new UpdateIntroducedMemberAction(memberId, creditedDate, creditedText, isDelete);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Sponser Contract
        public List<ExcelineMemberDC> GetMembersBySponsorId(int sponsorId, int branchId, string gymcode)
        {
            try
            {
                GetMembersBySponsorIdAction action = new GetMembersBySponsorIdAction(sponsorId, branchId, gymcode);
                return action.Execute(EnumDatabase.Exceline, gymcode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RemoveSponsorshipOfMember(int sponsoredRecordId, string gymCode, string user)
        {
            try
            {
                RemoveSponsorshipOfMemberAction action = new RemoveSponsorshipOfMemberAction(sponsoredRecordId, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<DiscountDC> GetDiscountList(int branchId, DiscountType discountType, string gymCode)
        {
            try
            {
                GetDiscountsAction action = new GetDiscountsAction(branchId, discountType);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<DiscountDC> GetGroupDiscountByType(int branchId, string user, int discountTypeId, string gymCode, int sponsorId)
        {
            try
            {
                GetGroupDiscountByTypeAction action = new GetGroupDiscountByTypeAction(branchId, user, discountTypeId, sponsorId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<EmployeeCategoryDC> GetSponsorEmployeeCategoryList(int branchId, string gymcode, int sponsorId)
        {


            try
            {
                GetSponsorEmployeeCategoryAction action = new GetSponsorEmployeeCategoryAction(branchId, gymcode, sponsorId);
                return action.Execute(EnumDatabase.Exceline, gymcode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SponsoredMemberDC> GetSponsoredMemberList(int branchId, int sponsorId, string gymcode)
        {
            try
            {
                GetSponsoredMemberListAction action = new GetSponsoredMemberListAction(branchId, sponsorId);
                return action.Execute(EnumDatabase.Exceline, gymcode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<int> GetSponsoredMemberListByTimePeriod(int sponsorId, Dictionary<int, List<DateTime>> sponsorMembers, string gymcode)
        {
            try
            {
                GetSponsoredMemberListByTimePeriodAction action = new GetSponsoredMemberListByTimePeriodAction(sponsorId, sponsorMembers);
                return action.Execute(EnumDatabase.Exceline, gymcode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SponsorSettingDC GetSponsorSetting(int branchId, string user, int sponsorId, string gymCode)
        {
            try
            {
                GetSponsorSettingAction action = new GetSponsorSettingAction(branchId, user, sponsorId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveSponsorSetting(SponsorSettingDC sponsorSetting, int branchId, string user, string gymCode)
        {
            try
            {
                sponsorSetting.BranchId = branchId;
                SaveSponsorSettingAction action = new SaveSponsorSettingAction(sponsorSetting, user, gymCode);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddSponserShipForMember(int memContractId, int sponserContractId, int employeeCategotyID, int actionType, string employeeRef, string gymCode)
        {
            try
            {

                AddSponserShipForMemberAction action = new AddSponserShipForMemberAction(memContractId, sponserContractId, employeeCategotyID, actionType, employeeRef);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteDiscount(int discountID, string gymCode)
        {
            try
            {
                DeleteDiscountAction action = new DeleteDiscountAction(discountID);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteEmployeeCategory(int categoryID, string gymCode)
        {
            try
            {
                DeleteSposorEmployeeCategoryAction action = new DeleteSposorEmployeeCategoryAction(categoryID);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<MemberContractDC> GetContractsForSponsership(int activityId, int branchId, DateTime endDate, DateTime startDate, string gymCode)
        {
            try
            {
                GetContractsForSponsershipAction action = new GetContractsForSponsershipAction(activityId, branchId, endDate, startDate);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveSponserContract(SponsorSettingDC sponserContract, int branchId, string user, string gymCode)
        {
            try
            {
                SaveSponserContractAction action = new SaveSponserContractAction(sponserContract, branchId, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OrdinaryMemberDC> GetMembersByRoleType(int branchId, string user, int status, MemberRole roleType, string searchText, string gymCode)
        {
            try
            {
                GetMembersByRoleTypeAction action = new GetMembersByRoleTypeAction(branchId, user, status, roleType, searchText);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<MemberContractDC> GetSponserContractsByActivity(int sponserId, int activityId, int branchId, string gymCode)
        {
            try
            {
                GetSponserContractsByActivityAction action = new GetSponserContractsByActivityAction(sponserId, activityId, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CancelSponsorContract(int contractId, string user, string comment, string gymCode)
        {
            try
            {
                CancelSponsorContractAction action = new CancelSponsorContractAction(contractId, user, comment);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateContract(MemberContractDC contract, string gymCode, string user)
        {
            UpdateContractAction action = new UpdateContractAction(contract, false, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Manage Discounts
        public bool AddMemberToGroup(int memberId, int groupId, string user, string gymCode)
        {
            AddMemberToGroupAction action = new AddMemberToGroupAction(memberId, groupId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool RemoveMemberFromGroup(int memberId, int groupId, string user, string gymCode)
        {
            RemoveMemberFromGroupAction action = new RemoveMemberFromGroupAction(memberId, groupId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveDiscount(List<DiscountDC> discountList, string user, int branchId, int contractSettingID, string gymCode)
        {
            SaveDiscountAction action = new SaveDiscountAction(discountList, user, branchId, contractSettingID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<DiscountDC> GetDiscountsForActivity(int branchId, int activityId, string gymCode)
        {
            GetDiscountsForActivityAction action = new GetDiscountsForActivityAction(branchId, activityId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ShopSalesDC GetMemberPerchaseHistory(int memberId, DateTime fromDate, DateTime toDate, string gymCode, int branchId, string user)
        {
            GetMemberPurchaseHistoryAction action = new GetMemberPurchaseHistoryAction(memberId, fromDate, toDate, branchId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Freeze Contract

        public int SaveContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, string gymCode)
        {
            SaveContractFreezeItemAction action = new SaveContractFreezeItemAction(freezeItem, freezeInstallments, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ContractFreezeItemDC> GetContractFreezeItems(int memberContractId, string gymCode)
        {
            GetContractFreezeItemsAction action = new GetContractFreezeItemsAction(memberContractId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ContractFreezeInstallmentDC> GetContractFreezeInstallments(int freezeItemId, string gymCode)
        {
            GetContractFreezeInstallmentsAction action = new GetContractFreezeInstallmentsAction(freezeItemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<InstallmentDC> GetInstallmentsToFreeze(int memberContractId, string gymCode)
        {
            GetInstallmentsToFreezeAction action = new GetInstallmentsToFreezeAction(memberContractId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int ExtendContractFreezeItem(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> freezeInstallments, string user, string gymCode)
        {
            ExtendContractFreezeItemAction action = new ExtendContractFreezeItemAction(freezeItem, freezeInstallments, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int UnfreezeContract(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> unfreezeInstallments, string user, string gymCode)
        {
            UnfreezeContractAction action = new UnfreezeContractAction(freezeItem, unfreezeInstallments, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int IsFreezeAllowed(int memberContractId, int memberId, string gymCode, int branchId)
        {
            IsFreezeAllowedAction action = new IsFreezeAllowedAction(memberContractId, memberId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        #endregion

        #region Contract

        public List<ContractSummaryDC> GetFamilyMemberContracts(int memberId, int branchId, string gymCode, bool isFreezDetailNeeded)
        {
            GetFamilyMemberContractsAction action = new GetFamilyMemberContractsAction(memberId, branchId, gymCode,  isFreezDetailNeeded);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        public int SaveNote(int branchId, string use, int memberId, string note, string gymCode)
        {
            SaveNoteAction action = new SaveNoteAction(memberId, note);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public bool SaveMemberInstallments(List<InstallmentDC> installmentList, bool initialGenInstmnts, DateTime startDate, DateTime endDate, int memberContractId, int membercontractNo, int branchId, string gymCode, int renewedTemplateId)
        {
            SaveMemberInstallmentsAction action = new SaveMemberInstallmentsAction(installmentList, initialGenInstmnts, startDate, endDate, memberContractId, membercontractNo, branchId, renewedTemplateId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public OrdinaryMemberDC GetNote(int branchId, string user, int memberId, string gymCode)
        {
            GetNoteAction action = new GetNoteAction(memberId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineMemberDC> GetGroupMembers(int groupId, string gymCode)
        {
            GetGroupMembersAction action = new GetGroupMembersAction(groupId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool RenewContract(int memberContractId, int branchId, string gymCode)
        {
            RenewContractByIdAction action = new RenewContractByIdAction(memberContractId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<RenewContractDC> GetContractsForAutoRenew(int branchId, string gymCode)
        {
            GetContractsForAutoRenewAction action = new GetContractsForAutoRenewAction(branchId, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineMemberDC> GetFamilyMembers(int memberId, string gymCode)
        {
            GetFamilyMembersAction action = new GetFamilyMembersAction(memberId, true);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<FollowUpDC> GetFollowUps(int memberId, int followUpId, string gymCode, string user)
        {
            GetFollowUpsAction action = new GetFollowUpsAction(memberId, followUpId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveFollowUpPopup(int followUpId, string gymCode)
        {
            SaveFollowUpPopUpAction action = new SaveFollowUpPopUpAction(followUpId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<FollowUpTemplateTaskDC> GetFollowUpTask(string gymCode)
        {
            GetFollowUpTaskAction action = new GetFollowUpTaskAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveFollowUp(List<FollowUpDC> followUpList, string gymCode, string user)
        {
            int output = 0;
            foreach (var item in followUpList)
            {
                SaveFollowUpAction action = new SaveFollowUpAction(item, user);
                output = action.Execute(EnumDatabase.Exceline, gymCode);
            }
            return output;
        }

        public bool DeleteInstallment(List<int> installmentIdList, int memberContractId, string gymCode, string user)
        {
            DeleteInstallmentAction action = new DeleteInstallmentAction(installmentIdList, memberContractId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ResignContract(ContractResignDetailsDC resignDetails, string gymCode, string user)
        {
            ResignContractAction action = new ResignContractAction(resignDetails, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<InstallmentDC> GetMemberOrders(int memberId, string type, string gymCode, string user)
        {
            GetMemberOrdersAction action = new GetMemberOrdersAction(memberId, type, gymCode, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public OrdinaryMemberDC GetEconomyDetails(int memberId, string gymCode)
        {
            GetEconomyDetailsAction action = new GetEconomyDetailsAction(memberId, gymCode);
            OrdinaryMemberDC memberForEconomy = action.Execute(EnumDatabase.Exceline, gymCode);

            if (memberForEconomy.Id > 0)
            {
                try
                {
                    GetListOfGuardianAction actionGuardian = new GetListOfGuardianAction(memberForEconomy.Id);
                    memberForEconomy.GuardianLst = actionGuardian.Execute(EnumDatabase.Exceline, gymCode);
                }
                catch
                {
                    // throw;
                }
            }
            if (memberForEconomy.SponserId > 0)
            {
                try
                {
                    GetEmployeeCategoryBySponsorIdAction actionEmpCategory = new GetEmployeeCategoryBySponsorIdAction(memberForEconomy.BranchId, memberForEconomy.SponserId);
                    memberForEconomy.EmployeeCategoryList = actionEmpCategory.Execute(EnumDatabase.Exceline, gymCode);
                }
                catch
                {
                    // throw;
                }
            }
            return memberForEconomy;
        }

        public List<ExcelineMemberDC> GetListOfGuardian(int guardianId, string gymCode)
        {
            try
            {
                GetListOfGuardianAction action = new GetListOfGuardianAction(guardianId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double ChangePrePaidAccount(string increase, float amount, int memberId, int branchId, string gymCode, string user)
        {
            try
            {
                ChangePrePaidAccountAction action = new ChangePrePaidAccountAction(increase, amount, branchId, user, memberId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveEconomyDetails(OrdinaryMemberDC member, string gymCode, string user)
        {
            try
            {
                SaveEconomyDetailsAction action = new SaveEconomyDetailsAction(member, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OrdinaryMemberDC GetMemberInfoWithNotes(int branchId, int memberId, string gymCode)
        {
            try
            {
                GetMemberInfoWithNotesAction action = new GetMemberInfoWithNotesAction(branchId, memberId, gymCode);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SaveMemberInfoWithNotes(OrdinaryMemberDC member, string gymCode)
        {
            try
            {
                SaveMemberInfoWithNotesAction action = new SaveMemberInfoWithNotesAction(member);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OrdinaryMemberDC GetEmployeeCategoryBySponsorId(int branchId, string gymcode, int sponsorId)
        {
            var member = new OrdinaryMemberDC();
            GetEmployeeCategoryBySponsorIdAction action = new GetEmployeeCategoryBySponsorIdAction(branchId, sponsorId);
            member.EmployeeCategoryList = action.Execute(EnumDatabase.Exceline, gymcode);
            try
            {
                GetDiscountCategoryBySponsorIdAction actionDis = new GetDiscountCategoryBySponsorIdAction(branchId, sponsorId);
                member.DiscountCategoy = actionDis.Execute(EnumDatabase.Exceline, gymcode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return member;
        }

        public List<ExcelineMemberDC> GetContractMembers(int memberContractId, string gymCode)
        {
            try
            {
                GetContractMembersAction action = new GetContractMembersAction(memberContractId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Dictionary<int, string> GetMemberStatus(string gymCode, bool getAllStatuses)
        {
            try
            {
                GetMemberStatusAction action = new GetMemberStatusAction(getAllStatuses);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateContractOrder(List<InstallmentDC> installmentList, string gymCode, string user)
        {
            try
            {
                UpdateContractOrderAction action = new UpdateContractOrderAction(installmentList, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveContractGroupMembers(int groupId, List<ExcelineMemberDC> groupMemberLst, int contractId, string gymCode, string user)
        {
            try
            {
                SaveContractGroupMembersAction action = new SaveContractGroupMembersAction(groupId, groupMemberLst, contractId, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public bool CheckBookingAvailability(int resourceID, string day, DateTime startTime, DateTime endTime, string gymCode)
        {
            try
            {
                CheckBookingAvailabilityAction action = new CheckBookingAvailabilityAction(resourceID, day, startTime, endTime);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public int AddInstallment(InstallmentDC installment, string gymCode, string user)
        {
            try
            {
                AddInstallmentAction action = new AddInstallmentAction(installment, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public int AddBlancInstallment(int MemberId, int BranchId, string gymCode, string user)
        {
            try
            {
                AddBlancInstallmentAction action = new AddBlancInstallmentAction(MemberId, BranchId, gymCode, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public string GetMemberSearchCategory(int branchId, string gymCode)
        {
            try
            {
                GetMemberSearchCategoryAction action = new GetMemberSearchCategoryAction(branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }


        public bool SaveDocumentData(DocumentDC document, string username, int branchId)
        {
            try
            {
                SaveDocumentDataAction action = new SaveDocumentDataAction(document);
                return action.Execute(EnumDatabase.Exceline, username);
            }
            catch
            {
                throw;
            }
        }


        public List<DocumentDC> GetDocumentData(bool isActive, string custId, string searchText, int documentType, string gymCode, int branchId)
        {
            try
            {
                GetDocumentDataAction action = new GetDocumentDataAction(isActive, custId, searchText, documentType, branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }


        public bool DeleteDocumentData(int docId, string gymCode, int branchId)
        {
            try
            {
                DeleteDocumentDataAction action = new DeleteDocumentDataAction(docId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public bool SetPDFFileParth(int id, string fileParth, string flag, int branchId, string gymCode, string user)
        {
            try
            {
                SetPDFFileParthAction action = new SetPDFFileParthAction(id, fileParth, flag, branchId, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public CustomerBasicInfoDC GetMemberBasicInfo(int memberId, string gymCode)
        {
            try
            {
                GetCustomerBasicInfoAction action = new GetCustomerBasicInfoAction(memberId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public int GetVisitCount(int memberId, DateTime startDate, DateTime endDate, string gymCode)
        {
            try
            {
                GetVisitCountAction action = new GetVisitCountAction(memberId, startDate, endDate);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public List<InstallmentDC> MergeOrders(List<InstallmentDC> updatedOrders, List<int> removedOrder, int memberContractId, string gymCode, string user)
        {
            try
            {
                MergeOrdersAction action = new MergeOrdersAction(updatedOrders, removedOrder, memberContractId, gymCode, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public List<FollowUpDetailDC> GetFollowUpDetailByEmpId(int empId, string gymCode)
        {
            try
            {
                GetFollowUpDetailByEmpIdAction action = new GetFollowUpDetailByEmpIdAction(empId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public List<MemberBookingDC> GetMemberBookings(int memberId, string gymCode, string user)
        {
            try
            {
                GetMemberBookingsAction action = new GetMemberBookingsAction(memberId, user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public List<ExcelineMemberDC> GetOtherMembersForBooking(int scheduleId, string gymCode)
        {
            try
            {
                GetOtherMembersForBookingAction action = new GetOtherMembersForBookingAction(scheduleId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public List<ExceACCMember> GetMemberDetailsForCard(string cardNo, string accessType, string gymCode)
        {
            GetmemberDetailsForCardAction action = new GetmemberDetailsForCardAction(cardNo, accessType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceACCMember> GetMemberDetailsForExcAccessController(string cardNo,int branchId,string gymCode)
        {
            var action = new GetMemberDetailsForExcAccessControllerAction(cardNo, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

       

        public List<InstallmentDC> GetSponsorOrders(List<int> sponsors, string gymCode)
        {
            GetSponsorOrdersAction action = new Commands.GetSponsorOrdersAction(sponsors, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddUpdateMemberIntegrationSettings(MemberIntegrationSettingDC memberIntegrationSettingsDetail, string gymCode)
        {
            AddUpdateMemberIntegrationSettingsAction action = new AddUpdateMemberIntegrationSettingsAction(memberIntegrationSettingsDetail);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<MemberIntegrationSettingDC> GetMemberIntegrationSettings(int branchId, int memberId, string gymCode)
        {
            GetMemberIntegrationSettingsAction action = new GetMemberIntegrationSettingsAction(branchId, memberId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int CheckContractGymChange(int memberID, int memberContractID, int oldBranchID, int newBranchID, string gymCode)
        {
            CheckContractChangeAction action = new CheckContractChangeAction(memberID, memberContractID, oldBranchID, newBranchID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int ValidateChangeGymInMember(int currentBranchID, int newBranchID, int branchID, int memberID, string user,string gymCode)
        {
            var action = new ValidateChangeGymInMemberAction(currentBranchID, newBranchID, branchID, memberID, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public ArticleDC GetTimeMachineArticle(string gymCode)
        {
            GetTimeMachineArticleAction action = new GetTimeMachineArticleAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public bool UpdateShopItemAndGatPurchase(GatpurchaseShopItem gatPurchaseShopItem)
        {
            UpdateShopAccountItemAndGatPurchaseAction action = new UpdateShopAccountItemAndGatPurchaseAction(gatPurchaseShopItem);
            return action.Execute(EnumDatabase.Exceline, gatPurchaseShopItem.GymCode);
        }

        public int ValidateMemberWithStatus(int memberId, int statusId, string user, string gymCode)
        {
            var action = new ValidateMemberWithStatusAction(memberId, statusId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int GetPriceChangeContractCount(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate)
        {
            GetPriceChangeContractCountAction action = new GetPriceChangeContractCountAction(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, changeToNextTemplate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public bool UpdatePrice(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate, string user)
        {
            PriceUpdateAction action = new PriceUpdateAction(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, changeToNextTemplate, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineContractDC> GetPriceUpdateContracts(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, string gymCode, bool changeToNextTemplate, int hit)
        {
            GetPriceUpdateContractsAction action = new GetPriceUpdateContractsAction(dueDate, lockInPeriod, priceGuarantyPeriod, minimumAmount, templateID, gyms, changeToNextTemplate, hit);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public List<PackageDC> GetTemplatesForPriceUpdate(string gymCode)
        {
            GetTemplatesforPriceUpdateAction action = new GetTemplatesforPriceUpdateAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public bool InfoGymUpdateExceMember(string infoGymId, int memberId, string user)
        {
            var action = new InfoGymUpdateExceMemberAction(infoGymId, memberId, user);
            return action.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(user));
        }
        
        public List<ContractSummaryDC> GetContractSummaries(int memberId, int branchId, string gymCode, bool isFreezDetailNeeded, string user, bool isFreezdView)
        {
            try
            {
                GetMemberContractSummaryAction action = new GetMemberContractSummaryAction(memberId, branchId, gymCode, isFreezDetailNeeded, user, isFreezdView);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public List<ContractSummaryDC> GetContractSummariesByEmployee(int employeeID, int branchId, DateTime createdDate, string gymCode)
        {
            try
            {
                var action = new GetContractSummariesByEmployeeAction(employeeID, branchId, createdDate);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public DateTime? ValidateSponsoringGeneration(string gymCode)
        {
            try
            {
                var action = new ValidateSponsoringGenerationAction();
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateBRISMemberData(OrdinaryMemberDC member, string gymCode)
        {
            UpdateBRISMemberDataAction action = new UpdateBRISMemberDataAction(member);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public string GetCountryCode(string landCode, string gymCode)
        {
            GetCountryCodeAction action = new GetCountryCodeAction(landCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public bool UpdateAgressoId(int memberID, int aggressoID, string gymCode)
        {
            UpdateAgressoIdAction action = new UpdateAgressoIdAction(aggressoID, memberID);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public bool UpdateBRISStatus(int brisID, string message, string response, string status, string gymCode)
        {
            UpdateMemberSyncStatusAction action = new UpdateMemberSyncStatusAction(brisID, message, response, status);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool RemoveContractResign(int memberContractId, string user, string gymCode)
        {
            RemoveContractResignAction action = new RemoveContractResignAction(memberContractId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateInvoicePdfPath(int arItemNo, string invoiceFilePath, string gymCode)
        {
            UpdateInvoicePdfPathAction action = new UpdateInvoicePdfPathAction(arItemNo, invoiceFilePath);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ImageChangedAddEvent(USNotificationTree notification, string gymcode, string user)
        {
            ImageChangedAddEventAction action = new ImageChangedAddEventAction(notification, user);
            return action.Execute(EnumDatabase.Exceline, gymcode);
        }

        public MemberContractPrintInfo GetMemberContract(int contractId, String gymcode)
        {
            GetMemberContractAction action = new GetMemberContractAction(contractId, gymcode);
            return action.Execute(EnumDatabase.Exceline, gymcode);
        }

        public List<MonthlyServicesForContract> GetMonthlyService(int contractId, string gymcode)
        {
            GetMonthlyServicesAction action = new GetMonthlyServicesAction(contractId, gymcode);
            return action.Execute(EnumDatabase.Exceline, gymcode);
        }

        public bool RegisterChangeOfInvoiceDueDate(AlterationInDueDateData changeData, String user, String gymCode)
        {
            RegisterChangeOfInvoiceDueDateAction action = new RegisterChangeOfInvoiceDueDateAction( changeData,  user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
    }
}
