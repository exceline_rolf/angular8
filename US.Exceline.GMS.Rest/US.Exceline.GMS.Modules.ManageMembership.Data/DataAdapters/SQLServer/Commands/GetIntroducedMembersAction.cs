﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetIntroducedMembersAction : USDBActionBase<List<ExcelineMemberDC>>
    {
        private readonly int _memberId;
        private readonly int _brachId;

        public GetIntroducedMembersAction(int memberId, int branchId)
        {
            _memberId = memberId;
            _brachId = branchId;
        }

        protected override List<ExcelineMemberDC> Body(DbConnection connection)
        {
            List<ExcelineMemberDC> members = new List<ExcelineMemberDC>();
            const string spName = "USExceGMSManageMembershipGetIntroduceMemberByMemberId";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", System.Data.DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _brachId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExcelineMemberDC member = new ExcelineMemberDC();
                    member.Id = Convert.ToInt32(reader["Id"]);
                    member.Name = Convert.ToString(reader["Name"]);
                    member.CustId = Convert.ToString(reader["CustId"]);
                    member.IntroduceDate = Convert.ToDateTime(reader["IntroduceDate"]);
                    member.ContractSignDate = Convert.ToDateTime(reader["ContractSignDate"]);
                    member.CreditedDate = Convert.ToDateTime(reader["CreditedDate"]);
                    member.CreditedText = Convert.ToString(reader["CreditedText"]);

                    members.Add(member);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return members;
        }
    }
}
