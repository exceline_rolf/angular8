﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetFollowUpTaskAction : USDBActionBase<List<FollowUpTemplateTaskDC>>
    {

        public GetFollowUpTaskAction()
        {

        }

        protected override List<FollowUpTemplateTaskDC> Body(DbConnection connection)
        {
            List<FollowUpTemplateTaskDC> followUpTasksList = new List<FollowUpTemplateTaskDC>();
            const string spName2 = "USExceGMSGetFollowUpTasks";
            try
            {
                DbCommand command2 = CreateCommand(CommandType.StoredProcedure, spName2);
                DbDataReader reader2 = command2.ExecuteReader();

                while (reader2.Read())
                {
                    FollowUpTemplateTaskDC followUpTask = new FollowUpTemplateTaskDC();
                    followUpTask.ExcelineTaskCategory = new ExcelineTaskCategoryDC();
                    followUpTask.ExcelineTaskCategory.Id = Convert.ToInt32(reader2["ID"]);
                    followUpTask.ExcelineTaskCategory.Name = Convert.ToString(reader2["Name"]);
                    followUpTask.ExcelineTaskCategory.IsAssignToEmp = Convert.ToBoolean(reader2["IsAssignToEmp"]);
                    followUpTask.ExcelineTaskCategory.IsAssignToRole = Convert.ToBoolean(reader2["IsAssignToRole"]);
                    followUpTask.ExcelineTaskCategory.IsStartDate = Convert.ToBoolean(reader2["IsStartDate"]);
                    followUpTask.ExcelineTaskCategory.IsEndDate = Convert.ToBoolean(reader2["IsEndDate"]);
                    followUpTask.ExcelineTaskCategory.IsDueTime = Convert.ToBoolean(reader2["IsDueTime"]);
                    followUpTask.ExcelineTaskCategory.IsStartTime = Convert.ToBoolean(reader2["IsStartTime"]);
                    followUpTask.ExcelineTaskCategory.IsEndTime = Convert.ToBoolean(reader2["IsEndTime"]);
                    followUpTask.ExcelineTaskCategory.IsFoundDate = Convert.ToBoolean(reader2["IsFoundDate"]);
                    followUpTask.ExcelineTaskCategory.IsReturnDate = Convert.ToBoolean(reader2["IsReturnDate"]);
                    followUpTask.ExcelineTaskCategory.IsDueDate = Convert.ToBoolean(reader2["IsDueDate"]);
                    followUpTask.ExcelineTaskCategory.IsNoOfDays = Convert.ToBoolean(reader2["IsNoOfDays"]);
                    followUpTask.ExcelineTaskCategory.IsPhoneNo = Convert.ToBoolean(reader2["IsPhoneNo"]);
                    followUpTask.ExcelineTaskCategory.IsNextFollowUp = Convert.ToBoolean(reader2["IsNextFollowUp"]);
                    followUpTask.ExcelineTaskCategory.IsAutomatedSMS = Convert.ToBoolean(reader2["IsAutomatedSMS"]);
                    followUpTask.ExcelineTaskCategory.IsAutomatedEmail = Convert.ToBoolean(reader2["IsAutomatedEmail"]);
                    followUpTask.ExcelineTaskCategory.CreatedDate = Convert.ToDateTime(reader2["CreatedDate"]);
                    followUpTask.ExcelineTaskCategory.LastModifiedDate = Convert.ToDateTime(reader2["LastModifiedDate"]);
                    followUpTask.ExcelineTaskCategory.CreatedUser = Convert.ToString(reader2["CreatedUser"]);
                    followUpTask.ExcelineTaskCategory.LastModifiedUser = Convert.ToString(reader2["LastModifiedUser"]);
                    followUpTask.ExcelineTaskCategory.IsDefault = Convert.ToBoolean(reader2["IsDefault"]);
                    followUpTasksList.Add(followUpTask);
                }
                reader2.Close();
                foreach (var item in followUpTasksList)
                {
                    item.ExcelineTaskCategory.ExtendedFieldsList = GetTaskTemplateExtendedFieldsAction(item.ExcelineTaskCategory.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return followUpTasksList;
        }

        public List<ExtendedFieldDC> GetTaskTemplateExtendedFieldsAction(int taskCategoryId)
        {
            List<ExtendedFieldDC> extFieldTemplateList = new List<ExtendedFieldDC>();
            const string storedProcedureName = "USExceGMSAdminGetTaskCategoryExtFields";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryId", DbType.Int32, taskCategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryType", DbType.String, "FOLLOWUP"));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedFieldDC extTaskTemplate = new ExtendedFieldDC();
                    extTaskTemplate.Id = Convert.ToInt32(reader["Id"]);
                    extTaskTemplate.Title = reader["Name"].ToString();
                    extTaskTemplate.FieldType = (CommonUITypes)Enum.Parse(typeof(CommonUITypes), reader["FieldType"].ToString(), true);
                    extFieldTemplateList.Add(extTaskTemplate);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return extFieldTemplateList;
        }
    }
}
