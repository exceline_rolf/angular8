﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/28/2012 5:23:32 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;
using System.IO;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMembersByRoleTypeAction : USDBActionBase<List<OrdinaryMemberDC>>
    {
        private int _branchId = 0;
        private string _searchText = null;
        private int _status = -1;
        private MemberRole _roleType = MemberRole.NONE;

        public GetMembersByRoleTypeAction(int branchId, string user, int status, MemberRole roleType, string searchText)
        {
            _branchId = branchId;
            _status = status;
            _searchText = searchText;
            _roleType = roleType;
        }

        protected override List<OrdinaryMemberDC> Body(DbConnection connection)
        {
            List<OrdinaryMemberDC> ordinaryMemberLst = new List<OrdinaryMemberDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetMembersByRole";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Int32, _status));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@roleType", DbType.String, _roleType.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@searchText", DbType.String, _searchText));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    OrdinaryMemberDC ordineryMember = new OrdinaryMemberDC();
                    ordineryMember.EntNo = Convert.ToInt32(reader["EntNo"]);
                    ordineryMember.CustId = Convert.ToString(reader["CustId"]);
                    ordineryMember.Id = Convert.ToInt32(reader["MemberId"]);
                    ordineryMember.BranchId = Convert.ToInt32(reader["BranchId"]);
                    ordineryMember.FirstName = reader["FirstName"].ToString();
                    ordineryMember.LastName = reader["LastName"].ToString();
                    ordineryMember.CompanyName = reader["FirstName"].ToString();
                    
                    ordineryMember.Name =( ordineryMember.FirstName + " " + ordineryMember.LastName).Trim();
                    ordineryMember.SponserName = ordineryMember.Name;
                    ordineryMember.FaxNo = reader["FaxNo"].ToString().Trim();
                    string roleType = reader["RoleId"].ToString();
                    if (!string.IsNullOrEmpty(roleType.Trim()))
                    {
                        try
                        {
                            ordineryMember.Role = (MemberRole)Enum.Parse(typeof(MemberRole), roleType);
                        }
                        catch
                        {
                            ordineryMember.Role = MemberRole.NONE;
                        }
                    }
                    ordineryMember.BirthDate = Convert.ToDateTime(reader["BirthDate"]);
                    ordineryMember.Address1 = reader["Address1"].ToString();
                    ordineryMember.Address2 = reader["Address2"].ToString();
                    ordineryMember.Address3 = reader["Address3"].ToString();
                    ordineryMember.PostCode = reader["ZipCodde"].ToString();
                    ordineryMember.PostPlace = reader["ZipName"].ToString();
                    ordineryMember.Mobile = reader["Mobile"].ToString().Trim();
                    ordineryMember.WorkTeleNo = reader["TeleWork"].ToString().Trim();
                    ordineryMember.PrivateTeleNo = reader["TeleHome"].ToString().Trim();
                    ordineryMember.Email = reader["Email"].ToString();
                    ordineryMember.Instructor = reader["IntroduceByName"].ToString();
                    ordineryMember.PayerName = reader["PayerName"].ToString();
                    ordineryMember.MemberStatuse = Convert.ToInt32(reader["ActiveStatus"]);
                    ordineryMember.AccountNo = reader["AccountNumber"].ToString();
                    string gender = reader["Gender"].ToString();
                    if (!string.IsNullOrEmpty(gender.Trim()))
                    {
                        try
                        {
                            ordineryMember.Gender = (Gender)Enum.Parse(typeof(Gender), gender);
                        }
                        catch
                        {
                            //throw;
                        }
                      
                    }
                    ordineryMember.ImagePath = reader["ImagePath"].ToString();
                    if (!string.IsNullOrEmpty(ordineryMember.ImagePath))
                    {
                        ordineryMember.ProfilePicture = GetMemberProfilePicture(ordineryMember.ImagePath);
                    }
                    ordinaryMemberLst.Add(ordineryMember);
                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return ordinaryMemberLst;
        }

        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }
    }
}
