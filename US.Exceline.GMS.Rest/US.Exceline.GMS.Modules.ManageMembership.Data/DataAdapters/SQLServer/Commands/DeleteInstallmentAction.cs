﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteInstallmentAction : USDBActionBase<bool>
    {
        private List<int> _installmentIdList = new List<int>();
        private int _memberContractId = -1;
        private string _user = string.Empty;
        public DeleteInstallmentAction(List<int> installmentIdList, int memberContractId, string user)
        {
            _installmentIdList = installmentIdList;
            _memberContractId = memberContractId;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipDeleteInstallment";
            DbTransaction transaction = connection.BeginTransaction();
            try
            {
                for (int i = 0; i < _installmentIdList.Count; i++)
                {
                    DbCommand command = new SqlCommand();
                    command.Connection = transaction.Connection;
                    command.Transaction = transaction;
                    command.CommandText = spName;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentId", System.Data.DbType.Int32, _installmentIdList[i]));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContracId", System.Data.DbType.Int32, _memberContractId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
                return true;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSManageMembershipDeleteInstallment";
            DbTransaction tran = transaction;
            try
            {
                for (int i = 0; i < _installmentIdList.Count; i++)
                {
                    DbCommand command = new SqlCommand();
                    command.Connection = tran.Connection;
                    command.Transaction = tran;
                    command.CommandText = spName;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentId", System.Data.DbType.Int32, _installmentIdList[i]));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContracId", System.Data.DbType.Int32, _memberContractId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                    command.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
