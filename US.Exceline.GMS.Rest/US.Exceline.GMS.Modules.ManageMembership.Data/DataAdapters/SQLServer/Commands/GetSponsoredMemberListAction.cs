﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetSponsoredMemberListAction : USDBActionBase<List<SponsoredMemberDC>>
    {
        private int _branchId = 0;
        private int _sponsorId = -1;

        public GetSponsoredMemberListAction(int branchId, int sponsorId)
        {
            _branchId = branchId;
            _sponsorId = sponsorId;

        }
        protected override List<SponsoredMemberDC> Body(System.Data.Common.DbConnection connection)
        {
            List<SponsoredMemberDC> _sponsoredMemberList = new List<SponsoredMemberDC>();
            string storedProcedureName = "USExceGMSManageMembershipGetSponsoredMemberList";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponsorId", DbType.Int32, _sponsorId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    SponsoredMemberDC _sponsoredMember = new SponsoredMemberDC();
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["ID"]).Trim()))
                    {
                        _sponsoredMember.Id = Convert.ToInt32(reader["ID"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["SponsorId"]).Trim()))
                    {
                        _sponsoredMember.SponsorId = Convert.ToInt32(reader["SponsorId"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["StartDate"]).Trim()))
                    {
                        _sponsoredMember.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["EndDate"]).Trim()))
                    {
                        _sponsoredMember.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["SponsoringLevelId"]).Trim()))
                    {
                        _sponsoredMember.SponsoredLevelId = Convert.ToInt32(reader["SponsoringLevelId"]);
                    }
                    _sponsoredMember.MemberNo = Convert.ToString(reader["MemberNo"]);
                    _sponsoredMember.MemberName = Convert.ToString(reader["MemberName"]);
                    _sponsoredMember.BranchName = Convert.ToString(reader["BranchName"]);

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["MemberId"]).Trim()))
                    {
                        _sponsoredMember.MemberId = Convert.ToInt32(reader["MemberId"]);
                    }
                    _sponsoredMemberList.Add(_sponsoredMember);
                }
                return _sponsoredMemberList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
