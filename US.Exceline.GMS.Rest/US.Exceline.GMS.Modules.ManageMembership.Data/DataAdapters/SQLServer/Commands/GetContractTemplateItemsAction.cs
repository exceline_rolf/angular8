﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : UUS.Exceline.GMS.Modules.ManageMembership.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "4/27/2012 3:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractTemplateItemsAction : USDBActionBase<List<ContractItemDC>>
    {
        private int _branchId;
        private int _contractId;

        public GetContractTemplateItemsAction(int contractId, int branchId)
        {
            _branchId = branchId;
            _contractId = contractId;
        }

        protected override List<ContractItemDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ContractItemDC > contractItemList = new List<ContractItemDC>();
            string StoredProcedureName = "USExceGMSAdminGetItemsForContractTemplate";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractId", DbType.Int32, _contractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ContractItemDC contractItem = new ContractItemDC();
                    contractItem.Id = Convert.ToInt32(reader["Id"]);
                    contractItem.IsActivityArticle = Convert.ToBoolean(reader["IsActivityArticle"]);
                    contractItem.ItemName = reader["Name"].ToString();
                    contractItem.UnitPrice = Convert.ToDecimal(reader["UnitPrice"]);
                    contractItem.ArticleId = Convert.ToInt32(reader["ArticleId"]);
                    contractItem.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                    contractItem.IsStartUpItem = Convert.ToBoolean(reader["IsStartUpItem"]);
                    contractItem.NumberOfOrders = Convert.ToInt32(reader["NumberOfOrders"]);
                    contractItem.Quantity = Convert.ToInt32(reader["Units"]);
                    contractItem.Price = Convert.ToDecimal(reader["Price"]);
                    contractItem.IsSelected = true;
                    contractItem.ActityCode = Convert.ToString(reader["ActivityCode"]);
                    contractItem.ActivityID = Convert.ToInt32(reader["ActivityId"]);
                    contractItem.StartOrder = Convert.ToInt32(reader["StartOrder"]);
                    contractItem.EndOrder = Convert.ToInt32(reader["EndOrder"]);
                    contractItem.IsAvailableForGym = Convert.ToBoolean(reader["IsAvailable"]);
                    contractItem.CategoryCode = Convert.ToString(reader["CategoryCode"]);
                    contractItemList.Add(contractItem);
                }                 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return  contractItemList;
        }


    }
}
