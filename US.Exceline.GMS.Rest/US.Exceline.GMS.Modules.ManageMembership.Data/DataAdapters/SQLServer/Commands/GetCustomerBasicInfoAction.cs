﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetCustomerBasicInfoAction : USDBActionBase<CustomerBasicInfoDC>
    {
        private int _memberId = -1;
        public GetCustomerBasicInfoAction(int memberId)
        {
            _memberId = memberId;
        }

        protected override CustomerBasicInfoDC Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipGetCustomerBasicInfo";
            CustomerBasicInfoDC basicInfo = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", System.Data.DbType.Int32, _memberId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    basicInfo = new CustomerBasicInfoDC();
                    basicInfo.LastName = Convert.ToString(reader["LastName"]);
                    basicInfo.FirstName = Convert.ToString(reader["FirstName"]);
                    basicInfo.Address = Convert.ToString(reader["Addr1"]);
                    basicInfo.PostalCode = Convert.ToString(reader["ZipCode"]);
                    basicInfo.ZipName = Convert.ToString(reader["ZipName"]);
                    if (reader["ContractStartDate"] != DBNull.Value)
                        basicInfo.ContractStartDate = Convert.ToDateTime(reader["ContractStartDate"]);
                    if (reader["ContractEndDate"] != DBNull.Value)
                        basicInfo.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]);
                    if (reader["LastVisitDate"] != DBNull.Value)
                        basicInfo.LastVisit = Convert.ToDateTime(reader["LastVisitDate"]);
                    basicInfo.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    basicInfo.TemplateNo = Convert.ToString(reader["TemplateNo"]);
                    basicInfo.AccountNo = Convert.ToString(reader["AccountNumber"]);
                    basicInfo.Image = GetImage(Convert.ToString(reader["ImagePath"]));
                    basicInfo.OnAccountBalance = Convert.ToDecimal(reader["OnAccountBalanace"]);
                    basicInfo.RoleId = reader["RoleId"].ToString();
                }
                return basicInfo;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private byte[] GetImage(string imagePath)
        {
            if (!string.IsNullOrEmpty(imagePath))
            {
                if (File.Exists(imagePath))
                {
                    byte[] byteArray = System.IO.File.ReadAllBytes(imagePath);
                    return byteArray;
                }
            }
            return new byte[0];
        }
    }
}
