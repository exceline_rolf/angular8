﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/10/2012 8:26:15 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteSposorEmployeeCategoryAction : USDBActionBase<bool>
    {
        private int _categoryID = -1;

        public DeleteSposorEmployeeCategoryAction(int categoryID)
        {
            _categoryID = categoryID;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSManageMembershipRemoveEmployeeCategory";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _categoryID));
                cmd.ExecuteNonQuery();
                result = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
