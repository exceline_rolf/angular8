﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetCountryCodeAction : USDBActionBase<string>
    {
        private string _landCode = string.Empty;
        public GetCountryCodeAction(string landCode)
        {
            _landCode = landCode;
        }

        protected override string Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSGetCountryCodeByLandCode";
            string countryCode = string.Empty;
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@landCode", System.Data.DbType.String, _landCode));
                countryCode = Convert.ToString(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return countryCode;
        }
    }
}
