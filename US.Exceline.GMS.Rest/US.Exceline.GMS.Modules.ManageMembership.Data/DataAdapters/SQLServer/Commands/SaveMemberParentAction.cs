﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveMemberParentAction : USDBActionBase<string>
    {
        private OrdinaryMemberDC _memberParent = new OrdinaryMemberDC();
        private string _user = string.Empty;
        private int _branchId = -1;

        public SaveMemberParentAction(OrdinaryMemberDC memberParent, int branchId, string user)
       {
           _memberParent = memberParent;
           _user = user;
           _branchId = branchId;
       }


       protected override string Body(DbConnection connection)
       {
           string result ="ERROR";
           string StoredProcedureName = "USExceGMSManageMembershipSaveMemberParent";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
               
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId",DbType.Int32 , _memberParent.MemberParent.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@parentId", DbType.Int32, _memberParent.MemberParent.ParentId));
                if (!string.IsNullOrEmpty(_memberParent.MemberParent.ParentFirstName))//when parent first name empty then not added parent 
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@parentFirstName", DbType.String, _memberParent.MemberParent.ParentFirstName));
                if (!string.IsNullOrEmpty(_memberParent.MemberParent.ParentLastName))//when parent last name empty then not added parent 
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@parentLastName", DbType.String, _memberParent.MemberParent.ParentLastName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address", DbType.String, _memberParent.MemberParent.Address));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@address2", DbType.String, _memberParent.MemberParent.Address2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postCode", DbType.String, _memberParent.MemberParent.PostCode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@postPlace", DbType.String, _memberParent.MemberParent.PostPlace));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sms", DbType.String, _memberParent.MemberParent.Sms));

                //Economy data 
                //..............
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceCharge", DbType.Boolean, _memberParent.InvoiceChage));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@smsInvoice", DbType.Boolean, _memberParent.SmsInvoice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ref", DbType.String, _memberParent.Ref));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@accountno", DbType.String, _memberParent.AccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@empNo", DbType.String, _memberParent.EmployeeNo));
                //.........


                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                result = Convert.ToString(output.Value);
               // result=true;
               
            }
            catch (Exception ex)
            {
                throw  ex;
            }
            return result;
        }
       }
    }

