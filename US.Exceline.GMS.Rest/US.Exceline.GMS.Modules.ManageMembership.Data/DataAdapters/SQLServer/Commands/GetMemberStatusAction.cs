﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
   public class GetMemberStatusAction : USDBActionBase<Dictionary<int,string>>
    {
        private bool _getAll;

        public GetMemberStatusAction(bool getAll)
        {
            _getAll = getAll;
        }

        protected override Dictionary<int, string> Body(DbConnection connection)
        {
            var status = new Dictionary<int, string>();
            const string storedProcedureName = "USExceGMSManageMembershipGetMemberStatus";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GetAll", System.Data.DbType.Boolean, _getAll));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int statusNo = Convert.ToInt32(reader["StatusNo"]);
                    string statusName = Convert.ToString(reader["StatusName"]); 
                    
                    status.Add(statusNo,statusName);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return status;
        }
    }
}
