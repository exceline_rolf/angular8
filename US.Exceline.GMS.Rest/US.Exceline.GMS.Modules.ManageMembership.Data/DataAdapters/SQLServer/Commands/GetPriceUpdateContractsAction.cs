﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetPriceUpdateContractsAction : USDBActionBase<List<ExcelineContractDC>>
    {

        private DateTime _duedate;
        private bool _lockInPeriod = false;
        private bool _priceGuarantyPeriod = false;
        private decimal _minimumAmount = 0;
        private int _templateId = -1;
        private List<int> _gyms = new List<int>();
        private int _hit;
        private bool _changeToNextTemplate = false;

        public GetPriceUpdateContractsAction(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, bool changeToNextTemplate, int hit)
        {
            _duedate = dueDate;
            _lockInPeriod = lockInPeriod;
            _priceGuarantyPeriod = priceGuarantyPeriod;
            _templateId = templateID;
            _gyms = gyms;
            _minimumAmount = minimumAmount;
            _changeToNextTemplate = changeToNextTemplate;
            _hit = hit;
        }

        protected override List<ExcelineContractDC> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSGetPriceUpdateContracts";
            List<ExcelineContractDC> contractList = new List<ExcelineContractDC>();
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DueDate", System.Data.DbType.Date, _duedate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lockInPeriod", System.Data.DbType.Boolean, _lockInPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priceGuarantyPeriod", System.Data.DbType.Boolean, _priceGuarantyPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinimumAmount", System.Data.DbType.Decimal, _minimumAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ChangeToNextTemplate", System.Data.DbType.Boolean, _changeToNextTemplate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Hit", System.Data.DbType.Int32, _hit));
                if (_templateId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateID", System.Data.DbType.Int32, _templateId));
                }
                var parameter = new SqlParameter
                {
                    ParameterName = "@Branches",
                    SqlDbType = SqlDbType.Structured,
                    Value = GetIds(_gyms)
                };
                cmd.Parameters.Add(parameter);

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExcelineContractDC con = new ExcelineContractDC();
                    con.Id = Convert.ToInt32(reader["ID"]);
                    con.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    con.MemberCustId = Convert.ToString(reader["CustId"]);
                    con.MemberName = Convert.ToString(reader["Name"]);
                    con.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]);
                    con.ContractPrize = Convert.ToDecimal(reader["ContractAmount"]);
                    con.TemplateNo = Convert.ToString(reader["TemplateNo"]);
                    con.ActivityName = Convert.ToString(reader["ActivityName"]);
                    con.BranchName = Convert.ToString(reader["BranchName"]);
                    contractList.Add(con);
                }
            }
            catch (Exception )
            {
                throw;
            }

            return contractList;
        }

        private DataTable GetIds(List<int> gyms)
        {
            DataTable branches = new DataTable();
            DataColumn col = new DataColumn("ID", typeof(Int32));
            branches.Columns.Add(col);
            foreach (var id in gyms)
            {
                branches.Rows.Add(id);
            }
            return branches;
        }
    }
}
