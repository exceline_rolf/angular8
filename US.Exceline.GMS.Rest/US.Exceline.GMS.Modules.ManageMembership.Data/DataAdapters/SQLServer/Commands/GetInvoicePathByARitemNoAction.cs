﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetInvoicePathByARitemNoAction : USDBActionBase<string>
    {
        private int _arItemNo = -1;
        string _filePath = string.Empty;

        public GetInvoicePathByARitemNoAction(int aritemNo)
        {
            _arItemNo = aritemNo;

        }
        protected override string Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSGetInvoicePathByARitemNo";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", DbType.Int32, _arItemNo));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {


                    _filePath = Convert.ToString(reader["FilePath"]);

                }
                return _filePath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
