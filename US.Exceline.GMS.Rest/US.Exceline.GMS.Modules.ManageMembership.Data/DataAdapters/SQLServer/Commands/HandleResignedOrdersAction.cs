﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class HandleResignedOrdersAction : USDBActionBase<bool>
    {
        private List<int> _installmentIdList = new List<int>();
        private List<int> _mergedInstallmentList = new List<int>();
        private int _memberContractId = -1;
        private string _user = string.Empty;
        public HandleResignedOrdersAction(List<int> installmentIdList, List<int> mergedInstallmentList, int memberContractId, string user)
        {
            _installmentIdList = installmentIdList;
            _mergedInstallmentList = mergedInstallmentList;
            _memberContractId = memberContractId;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipHandleResignedOrders";
            DbTransaction transaction = connection.BeginTransaction();
            try
            {
                DbCommand command = new SqlCommand();
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.CommandText = spName;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContracId", System.Data.DbType.Int32, _memberContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));

                var parameter = new SqlParameter
                {
                    ParameterName = "@Installments",
                    SqlDbType = SqlDbType.Structured,
                    Value = GetIds(_installmentIdList, _mergedInstallmentList)
                };
                command.Parameters.Add(parameter);
                command.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSManageMembershipHandleResignedOrders";
            DbTransaction tran = transaction;
            try
            {
                DbCommand command = new SqlCommand();
                command.Connection = tran.Connection;
                command.Transaction = tran;
                command.CommandText = spName;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContracId", System.Data.DbType.Int32, _memberContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));

                var parameter = new SqlParameter
                {
                    ParameterName = "@Installments",
                    SqlDbType = SqlDbType.Structured,
                    Value = GetIds(_installmentIdList, _mergedInstallmentList)
                };
                command.Parameters.Add(parameter);
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable GetIds(List<int> installments, List<int> mergedInstallments)
        {
            DataTable branches = new DataTable();
            DataColumn col = new DataColumn("ID", typeof(Int32));
            DataColumn col2 = new DataColumn("MERGED", typeof(Boolean));
            branches.Columns.Add(col);
            branches.Columns.Add(col2);
            installments.ForEach(x =>
            {
                DataRow row = branches.NewRow();
                row["ID"] = x;
                row["MERGED"] = false;
                branches.Rows.Add(row);
            });

            mergedInstallments.ForEach(x =>
            {
                DataRow row = branches.NewRow();
                row["ID"] = x;
                row["MERGED"] = true;
                branches.Rows.Add(row);
            });

            return branches;
        }
    }
}
