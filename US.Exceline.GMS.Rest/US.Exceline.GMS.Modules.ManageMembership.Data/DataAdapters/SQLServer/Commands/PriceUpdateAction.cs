﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class PriceUpdateAction : USDBActionBase<bool>
    {
        private DateTime _duedate;
        private bool _lockInPeriod = false;
        private bool _priceGuarantyPeriod = false;
        private decimal _minimumAmount = 0;
        private int _templateId = -1;
        private List<int> _gyms = new List<int>();
        private bool _changeToNextTemplate = false;
        private string _user = string.Empty;

        public PriceUpdateAction(DateTime dueDate, bool lockInPeriod, bool priceGuarantyPeriod, decimal minimumAmount, int templateID, List<int> gyms, bool changeToNextTemplate, string user)
        {
            _duedate = dueDate;
            _lockInPeriod = lockInPeriod;
            _priceGuarantyPeriod = priceGuarantyPeriod;
            _templateId = templateID;
            _gyms = gyms;
            _minimumAmount = minimumAmount;
            _changeToNextTemplate = changeToNextTemplate;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSUpdatePrice";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DueDate", System.Data.DbType.Date, _duedate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lockInPeriod", System.Data.DbType.Boolean, _lockInPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priceGuarantyPeriod", System.Data.DbType.Boolean, _priceGuarantyPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinimumAmount", System.Data.DbType.Decimal, _minimumAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ChangeToNextTemplate", System.Data.DbType.Boolean, _changeToNextTemplate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));

                if (_templateId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateID", System.Data.DbType.Int32, _templateId));
                }
                var parameter = new SqlParameter
                {
                    ParameterName = "@Branches",
                    SqlDbType = SqlDbType.Structured,
                    Value = GetIds(_gyms)
                };
                cmd.Parameters.Add(parameter);
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        private DataTable GetIds(List<int> gyms)
        {
            DataTable branches = new DataTable();
            DataColumn col = new DataColumn("ID", typeof(Int32));
            branches.Columns.Add(col);
            foreach (var id in gyms)
            {
                branches.Rows.Add(id);
            }
            return branches;
        }
    }
}
