﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetRenewContractTemplatesAction : USDBActionBase<List<PackageDC>>
    {
        private int _memeberContractId = 0;
        public GetRenewContractTemplatesAction(int memeberContractId)
        {
            _memeberContractId = memeberContractId;

        }
        protected override List<PackageDC> Body(System.Data.Common.DbConnection connection)
        {
            List<PackageDC> renewContractTemplateList = new List<PackageDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetRenewTemplates";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _memeberContractId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    PackageDC currentTemplate = new PackageDC();
                    currentTemplate.PackageName = reader["Name"].ToString();
                    if (reader["Id"] != DBNull.Value)
                        currentTemplate.PackageId = Convert.ToInt32(reader["Id"]);
                    if (reader["PackagePrice"] != DBNull.Value)
                        currentTemplate.PackagePrice = Convert.ToDecimal(reader["PackagePrice"]);
                    if (reader["NumberOfInstallments"] != DBNull.Value)
                        currentTemplate.NumOfInstallments = Convert.ToInt32(reader["NumberOfInstallments"]);
                    if (reader["RatePerInstallment"] != DBNull.Value)
                        currentTemplate.RatePerInstallment = Convert.ToDecimal(reader["RatePerInstallment"]);
                    if (reader["OrderPrice"] != DBNull.Value)
                        currentTemplate.OrderPrice = Convert.ToDecimal(reader["OrderPrice"]);
                    if (reader["StartupItemPrice"] != DBNull.Value)
                        currentTemplate.StartUpItemPrice = Convert.ToDecimal(reader["StartupItemPrice"]);
                    if (reader["EveryMonthItemPrice"] != DBNull.Value)
                        currentTemplate.EveryMonthItemsPrice = Convert.ToDecimal(reader["EveryMonthItemPrice"]);
                    if (reader["StartDate"] != DBNull.Value)
                        currentTemplate.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    if (reader["EndDate"] != DBNull.Value)
                        currentTemplate.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    if (reader["NoOfMonths"] != DBNull.Value)
                        currentTemplate.NoOfMonths = Convert.ToInt32(reader["NoOfMonths"]);
                    if (reader["ArticleId"] != DBNull.Value)
                    currentTemplate.ArticleId = Convert.ToInt32(reader["ArticleId"]);
                    if (reader["Priority"] != DBNull.Value)
                        currentTemplate.Priority = Convert.ToInt32(reader["Priority"]);
                    currentTemplate.NextContractTemplateName = Convert.ToString(reader["NextTemplateNo"]);

                 
                    if (renewContractTemplateList.Contains(currentTemplate))
                    {
                    }
                    else
                    {
                        renewContractTemplateList.Add(currentTemplate);
                    }
                }
                return renewContractTemplateList;

            }
            catch
            {
                throw;
            }
        }
    }
}
