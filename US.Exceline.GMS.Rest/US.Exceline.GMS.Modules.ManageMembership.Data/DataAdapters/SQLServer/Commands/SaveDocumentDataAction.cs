﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    class SaveDocumentDataAction : USDBActionBase<bool>
    {
        private DocumentDC _document;

        public SaveDocumentDataAction(DocumentDC document)
        {
            this._document = document;

        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSMemberSaveDocumentData";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FileId", DbType.Int32, _document.FileId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FileName", DbType.String, _document.FileName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _document.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FilePath", DbType.String, _document.FilePath));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UploadedUser", DbType.String, _document.UploadedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DocumentType", DbType.Int32, _document.DocumentType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserId", DbType.String, _document.CusId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.Boolean, _document.IsActive));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractNo", DbType.Int32, _document.ContractNo));
                cmd.ExecuteNonQuery();
                
                result = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
