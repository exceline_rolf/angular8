﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetVisitCountAction : USDBActionBase<int>
    {
        private int _memberId = -1;
        private DateTime _startDate;
        private DateTime _endDate;
        public GetVisitCountAction(int memberId, DateTime startDate, DateTime endDate)
        {
            _memberId = memberId;
            _startDate = startDate;
            _endDate = endDate;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int visitCount = 0;
            string spname = "USExceGMSManageMembershipGetVisitCount";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spname);
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", System.Data.DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", System.Data.DbType.DateTime, _startDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", System.Data.DbType.DateTime, _endDate));
                visitCount = Convert.ToInt32(command.ExecuteScalar());
                return visitCount;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
