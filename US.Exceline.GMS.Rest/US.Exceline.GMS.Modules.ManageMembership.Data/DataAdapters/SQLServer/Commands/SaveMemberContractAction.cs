﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "4/3/2012 11:09:43 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    class SaveMemberContractAction : USDBActionBase<ContractSaveResultDC>
    {
        MemberContractDC _memberContract;
        private int _memberContractId;
        private string _user = string.Empty;

        public SaveMemberContractAction(MemberContractDC memberContract, string user)
        {
            _memberContract = memberContract;
            _user = user;
        }

        protected override ContractSaveResultDC Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageMembershipSaveMemberContract";
            DbTransaction transaction = null;
            ContractSaveResultDC result = new ContractSaveResultDC();
            try
            {
                transaction = connection.BeginTransaction();
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberContract.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractId", DbType.Int32, _memberContract.ContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@enrollmentFee", DbType.Decimal, _memberContract.EnrollmentFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amountPerInstallment", DbType.Decimal, _memberContract.AmountPerInstallment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@remainingBalance", DbType.Decimal, _memberContract.RemainingBalance));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@accountNo", DbType.String, _memberContract.AccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@numberofInstallments", DbType.Int32, _memberContract.NoOfInstallments));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractStartDate", DbType.DateTime, _memberContract.ContractStartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractEndDate", DbType.DateTime, _memberContract.ContractEndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@trainerId", DbType.Int32, _memberContract.TrainerId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _memberContract.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _memberContract.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SignedDate", DbType.DateTime, _memberContract.CreatedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isDeductFromBank", DbType.Boolean, _memberContract.IsDeductFromBank));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityId", DbType.Int32, _memberContract.ActivityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isATG", DbType.Boolean, _memberContract.IsATG));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractDescription", DbType.String, _memberContract.ContractDescription));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@extracActivityId", DbType.Int32, _memberContract.ExtracActivityId));
                
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@numberOfVisits", DbType.Int32, _memberContract.NumberOfVisits));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractPrice", DbType.Decimal, _memberContract.ContractPrize));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lockInperiod", DbType.Decimal, _memberContract.LockInPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@guarantyPeriod", DbType.Decimal, _memberContract.GuarantyPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceDetails", DbType.Boolean, _memberContract.IsInvoiceDetails));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@autoRenew", DbType.Boolean, _memberContract.AutoRenew));
                if(_memberContract.AccessProfileId > 0)
                  cmd.Parameters.Add(DataAcessUtils.CreateParam("@accessProfileId", DbType.Int32, _memberContract.AccessProfileId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@noofMonths", DbType.Int32, _memberContract.NoOfMonths));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lockUntillDate", DbType.DateTime, _memberContract.LockInPeriodUntilDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@orderPrice", DbType.Decimal, _memberContract.OrderPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startupItemPrice", DbType.Decimal, _memberContract.StartUpItemPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@monthlyItemPrice", DbType.Decimal, _memberContract.EveryMonthItemPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priceGuarantyUntillDate", DbType.DateTime, _memberContract.PriceGuarantyUntillDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstDueDate", DbType.DateTime, _memberContract.FirstDueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@secondDueDate", DbType.DateTime, _memberContract.SecondDueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@creditDueDays", DbType.Int32, _memberContract.CreditoDueDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@bookingprice", DbType.Decimal, _memberContract.BookingPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SignUpCategoryId", DbType.Int32, _memberContract.SignUpCategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RestPlusMonthValue", DbType.Decimal, _memberContract.RestPlusMonthAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@nextTemplateID", DbType.Int32, _memberContract.NextTemplateId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@maxATGAmount", DbType.Decimal, _memberContract.MaxATGAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));

                if (_memberContract.MemberFeeArticleID > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberFeeArticleId", DbType.Int32, _memberContract.MemberFeeArticleID));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberFeeMonth", DbType.Decimal, _memberContract.MemberFeeMonth));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberFeeLastInvoicedDate", DbType.DateTime, _memberContract.MemFeeLastInvoicedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractCategoryID", DbType.Int32, _memberContract.CategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractConditionID", DbType.Int32, _memberContract.ContractConditionId));
                

                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@memberContractId";
                outputPara.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara);

                DbParameter outputPara1 = new SqlParameter();
                outputPara1.DbType = DbType.Int32;
                outputPara1.ParameterName = "@MemberConNo";
                outputPara1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara1);

                DbParameter outputPara2 = new SqlParameter();
                outputPara2.DbType = DbType.Boolean;
                outputPara2.ParameterName = "@ISGymChanged";
                outputPara2.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara2);

                cmd.ExecuteNonQuery();
                _memberContractId = Convert.ToInt32(outputPara.Value);

                result.MemberContractId = _memberContractId;
                result.IsGymChanged = Convert.ToBoolean(outputPara2.Value);
                if (_memberContractId > 0)
                {
                    int memberContractNo = Convert.ToInt32(outputPara1.Value);
                    result.MemberContractNo = memberContractNo.ToString();

                    SaveContractItems(_memberContract.EveryMonthItemList, _memberContract.StartUpItemList, _memberContractId, transaction, _user);
                    if (_memberContract.ContractBookingList.Count > 0)
                        SaveContractBookings(_memberContract.ContractBookingList, _memberContractId, _memberContract.ContractStartDate, _memberContract.ContractEndDate, _memberContract.BranchId, _memberContract.CreatedUser, _memberContract.MemberId, transaction);

                    if (_memberContract.InstalllmentList != null)
                        SaveMemberInstallments(_memberContract.InstalllmentList, _memberContractId, _memberContract.ContractStartDate, _memberContract.ContractEndDate, memberContractNo, transaction, _memberContract.BranchId, -1);
                    if (_memberContract.GroupMembers.Count > 0)
                        SaveGroupMembers(_memberContract.MemberId, _memberContract.GroupMembers, _memberContractId, transaction, _user);
                    transaction.Commit();
                }
                else
                {
                    transaction.Rollback();
                }
            }
            catch (Exception ex){
                transaction.Rollback();
                throw ex;
            }
            return result;
        }

        private void SaveContractItems(List<ContractItemDC> everyMonthItemList, List<ContractItemDC> startupItemList, int memberContractId, DbTransaction transaction, string user)
        {
            try
            {
                if (everyMonthItemList != null)
                {
                    foreach (ContractItemDC adon in everyMonthItemList)
                    {
                        SaveMemberContractItemsAction adonAction = new SaveMemberContractItemsAction(adon,memberContractId,  _memberContract.BranchId,false, user);
                        adonAction.SaveContractItems(transaction);
                    }
                }

                if (startupItemList != null)
                {
                    foreach (ContractItemDC fItem in startupItemList)
                    {
                        SaveMemberContractItemsAction adonAction = new SaveMemberContractItemsAction(fItem, memberContractId, _memberContract.BranchId,true, user);
                        adonAction.SaveContractItems(transaction);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SaveContractBookings(List<ContractBookingDC> booking, int memberContractId, DateTime contractStartDate, DateTime contractEndDate, int branchId, string user,int memberId, DbTransaction transaction)
        {
            SaveMemberContractBookingsAction action = new SaveMemberContractBookingsAction(booking, memberContractId, contractStartDate, contractEndDate, branchId, user, memberId);
            action.RunOnTransaction(transaction);
        }

        private void SaveMemberInstallments(List<InstallmentDC> installments, int memberContractId, DateTime startDate, DateTime endDate,int contractNo,DbTransaction transaction, int branchID, int renewdTemplateId)
        {
            SaveMemberInstallmentsAction action = new SaveMemberInstallmentsAction(installments, true, startDate, endDate, memberContractId, contractNo, branchID,  renewdTemplateId);
            action.RunOnTransaction(transaction);
        }

        private void SaveGroupMembers(int groupId, List<ExcelineMemberDC>groupMembers, int contractId, DbTransaction transaction, string user)
        {
            try
            {
                SaveContractGroupMembersAction action = new SaveContractGroupMembersAction(groupId,groupMembers,contractId, user);
                action.RunOnTransaction(transaction);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
