﻿using System;
using System.Collections.Generic;
using System.Data;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberOrdersAction : USDBActionBase<List<InstallmentDC>>
    {
        private readonly int _memberId = -1;
        private readonly string _gymCode = string.Empty;
        private readonly string _type = string.Empty;
        private readonly string _user = string.Empty;
        public GetMemberOrdersAction(int memberId, string type, string gymCode, string user)
        {
            _memberId = memberId;
            _gymCode = gymCode;
            _type = type;
            _user = user;
        }

        protected override List<InstallmentDC> Body(System.Data.Common.DbConnection connection)
        {
            var installmentsList = new List<InstallmentDC>();
            const string spName = "USExceGMSManageMembershipGetMemberOrders";
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _type));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));

                var reader = command.ExecuteReader();
                var count = 1;
                while (reader.Read())
                {
                    var installment = new InstallmentDC();
                    installment.InstallmentId = count;
                    installment.Id = Convert.ToInt32(reader["ID"]);
                    installment.TableId = Convert.ToInt32(reader["ID"]);
                    installment.MemberId = Convert.ToInt32(reader["MemberId"]);
                    installment.MemberContractId = Convert.ToInt32(reader["MemberContractId"]);
                    installment.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    installment.Text = Convert.ToString(reader["Text"]);
                    installment.InstallmentDate = Convert.ToDateTime(reader["InstallmentDate"]);
                    installment.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    installment.Amount = Convert.ToDecimal(reader["Amount"]);
                    if (reader["AddOnPrice"] != DBNull.Value)
                        installment.AdonPrice = Convert.ToDecimal(reader["AddOnPrice"]);
                    if (reader["Discount"] != DBNull.Value)
                        installment.Discount = Convert.ToDecimal(reader["Discount"]);
                    if (reader["SponsoredAmount"] != DBNull.Value)
                        installment.SponsoredAmount = Convert.ToDecimal(reader["SponsoredAmount"]);
                    installment.Comment = reader["Comment"].ToString();
                    if (reader["OriginalMatuarity"] != DBNull.Value)
                        installment.OriginalDueDate = Convert.ToDateTime(reader["OriginalMatuarity"]);
                    installment.Balance = Convert.ToDecimal(reader["Balance"]);
                    if (reader["InvoiceGeneratedDate"] != DBNull.Value)
                        installment.InvoiceGeneratedDate = Convert.ToDateTime(reader["InvoiceGeneratedDate"]);
                    installment.IsInvoiced = Convert.ToBoolean(reader["InvoiceGenerated"]);
                    installment.InstallmentNo = Convert.ToInt32(reader["InstallmentNo"]);
                    if (reader["TSDate"] != DBNull.Value)
                        installment.TrainingPeriodStart = Convert.ToDateTime(reader["TSDate"]);
                    if (reader["TEDate"] != DBNull.Value)
                        installment.TrainingPeriodEnd = Convert.ToDateTime(reader["TEDate"]);
                    installment.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    installment.IsOtherPay = Convert.ToBoolean(reader["IsOtherPay"]);
                    installment.ContractName = Convert.ToString(reader["TemplateName"]);
                    installment.ActivityName = Convert.ToString(reader["ActivityName"]);
                    installment.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    installment.OriginalCustomer = Convert.ToString(reader["OriginalCustomer"]);
                    installment.EstimatedOrderAmount = Convert.ToDecimal(reader["EstimatedOrderAmount"]);
                    installment.OrderNo = Convert.ToString(reader["OrderNo"]);
                    installment.ActivityPrice = Convert.ToDecimal(reader["ActivityPrice"]);
                    if (reader["EstimatedInvoiceDate"] != DBNull.Value)
                        installment.EstimatedInvoiceDate = Convert.ToDateTime(reader["EstimatedInvoiceDate"]);
                    installment.AdonText = Convert.ToString(reader["AdonText"]);
                    installment.PayerName = Convert.ToString(reader["PayerName"]);
                    if (reader["PayerID"] != DBNull.Value)
                        installment.PayerId = Convert.ToInt32(reader["PayerID"]);
                    installment.IsSpOrder = Convert.ToBoolean(reader["IsSpOrder"]);
                    installment.IsInvoiceFeeAdded = Convert.ToBoolean(reader["IsInvoiceFeeAdded"]);
                    installment.BranchName = Convert.ToString(reader["BranchName"]);
                    installment.ServiceAmount = Convert.ToDecimal(reader["ServiceAmount"]);
                    installment.ItemAmount = Convert.ToDecimal(reader["ItemAmount"]);
                    installment.InstallmentType = Convert.ToString(reader["InstallmentType"]);
                    installment.MemberName = Convert.ToString(reader["MemberName"]);
                    installment.SponsorName = Convert.ToString(reader["SponsorName"]);
                    installment.MinVisits = Convert.ToInt32(reader["MinVisits"]);
                    installment.IsSponsored = Convert.ToBoolean(reader["IsSponsored"]);
                    installment.SponsorArticleID = Convert.ToInt32(reader["SponsorArtilceID"]);
                    installment.DiscountArticleID = Convert.ToInt32(reader["DiscountArtilceID"]);
                    installment.GymID = Convert.ToInt32(reader["GymID"]);
                    installment.CreditPeriod = Convert.ToInt32(reader["CreditPeriod"]);
                    installment.MemberCreditPeriod = Convert.ToInt32(reader["MemberCreditPeriod"]);
                    installment.SponsorCreditPeriod = Convert.ToInt32(reader["SponsorCreditPeriod"]);
                    installmentsList.Add(installment);
                    count++;
                }
                reader.Close();
            }
            catch
            {
                throw;
            }
            if (installmentsList.Count > 0)
            {
                foreach (var item in installmentsList)
                {
                    if (item.IsSpOrder)
                    {
                        var spAction = new GetSponsorInstallmentDetailsAction(item.Id);
                        item.SponsorItemList = spAction.Execute(EnumDatabase.Exceline, _gymCode);

                        var adonAction = new GetOrderShareAction(item.Id);
                        item.AddOnList = adonAction.Execute(EnumDatabase.Exceline, _gymCode);

                    }
                    else
                    {
                        var adonAction = new GetOrderShareAction(item.Id);
                        item.AddOnList = adonAction.Execute(EnumDatabase.Exceline, _gymCode);

                        foreach (var addon in item.AddOnList)
                        {
                            if (item.IsSponsored)
                            {
                                if ((item.Discount > 0 && addon.Price < 0 && addon.ItemName == "Rabatt") || (addon.ArticleId == item.DiscountArticleID))
                                {
                                    addon.Description = "Rabatt - " + item.BranchName;
                                }

                                if ((item.SponsoredAmount >= 0 && addon.Price <= 0 && addon.ItemName == "Sponset Beløp") || (addon.ArticleId == item.SponsorArticleID))
                                {
                                    if (item.MinVisits > 0)
                                    {
                                        addon.Description = "Sponset Beløp - " + item.SponsorName + " - " + addon.NoOfVisits.ToString() + " - besøk ";
                                    }

                                    else
                                    {
                                        addon.Description = "Sponset Beløp - " + item.SponsorName;
                                    }
                                }
                            }
                        }


                        if (!item.IsSponsored)
                        {
                            if (item.Discount > 0 && (!item.AddOnList.Exists(x => x.ArticleId == item.DiscountArticleID)))
                            {

                                var discountAddon = new ContractItemDC();
                                discountAddon.Id = -1;
                                discountAddon.Price = item.Discount * -1;
                                discountAddon.UnitPrice = 0;
                                discountAddon.ArticleId = 0;
                                discountAddon.ItemName = "Rabatt";
                                discountAddon.IsStartUpItem = false;
                                discountAddon.Quantity = 1;
                                discountAddon.StockLevel = 0;
                                discountAddon.Discount = 0;
                                discountAddon.IsActivityArticle = false;
                                discountAddon.Description = "Rabatt - " + item.BranchName;
                                discountAddon.Priority = 0;
                                discountAddon.IsBookingArticle = false;
                                discountAddon.IsSponsored = false;
                                item.AddOnList.Add(discountAddon);
                            }

                            if (item.SponsoredAmount > 0 && (!item.AddOnList.Exists(x => x.ArticleId == item.SponsorArticleID) ))
                            {

                                var sponsAddon = new ContractItemDC();
                                sponsAddon.Id = -1;
                                sponsAddon.Price = item.SponsoredAmount * -1;
                                sponsAddon.UnitPrice = 0;
                                sponsAddon.ArticleId = 0;
                                sponsAddon.ItemName = "Sponset Beløp";
                                sponsAddon.IsStartUpItem = false;
                                sponsAddon.Quantity = 1;
                                sponsAddon.StockLevel = 0;
                                sponsAddon.Discount = 0;
                                sponsAddon.IsActivityArticle = false;
                                if (item.MinVisits > 0)
                                {
                                    sponsAddon.Description = "Sponset Beløp - " + item.SponsorName + " - " + item.NoOfVisits.ToString() + " - besøk ";
                                }

                                else
                                {
                                    sponsAddon.Description = "Sponset Beløp - " + item.SponsorName;
                                }

                                sponsAddon.Priority = 0;
                                sponsAddon.IsBookingArticle = false;
                                sponsAddon.IsSponsored = false;
                                item.AddOnList.Add(sponsAddon);
                            }

                        }
               
                    }

                    foreach (ContractItemDC addon in item.AddOnList)
                    {
                        if (addon.IsAddedFromShop)
                        {
                            item.HasAddonFromShop = true;
                        }
                    }
                }
            }
            return installmentsList;
        }
    }
}
