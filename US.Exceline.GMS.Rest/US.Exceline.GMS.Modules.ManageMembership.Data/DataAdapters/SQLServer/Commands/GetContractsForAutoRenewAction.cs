﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : NAD
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractsForAutoRenewAction : USDBActionBase<List<RenewContractDC>>
    {
        private int _branchId;
        private string _gymCode = string.Empty;

        public GetContractsForAutoRenewAction(int branchId, string gymCode)
        {
            _branchId = branchId;
            _gymCode = gymCode;

        }
        protected override List<RenewContractDC> Body(System.Data.Common.DbConnection connection)
        {
            List<RenewContractDC> renewContractList = new List<RenewContractDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetAutoRenewContracts";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    RenewContractDC renewContract = new RenewContractDC();
                    if (reader["MemberContractID"] != DBNull.Value)
                        renewContract.MemberContractID = Convert.ToInt32(reader["MemberContractID"]);
                    if (reader["MemberContractNo"] != DBNull.Value)
                        renewContract.MemberContractNo = Convert.ToInt32(reader["MemberContractNo"]);
                    if (reader["TemplateID"] != DBNull.Value)
                        renewContract.OldTemplateId = Convert.ToInt32(reader["TemplateID"]);
                    renewContract.ActivityId = Convert.ToInt32(reader["ActivityID"]);
                    renewContract.IsInvoiceFeeAdded = Convert.ToBoolean(reader["AddInvoiceFee"]);

                    if (reader["IsATG"] != DBNull.Value)
                        renewContract.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    if (reader["MemberId"] != DBNull.Value)
                        renewContract.MemberId = Convert.ToInt32(reader["MemberId"]);
                    if (reader["MemberName"] != DBNull.Value)
                        renewContract.MemberName = Convert.ToString(reader["MemberName"]);
                    if (reader["ContractEndDate"] != DBNull.Value)
                        renewContract.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]);
                    if (reader["DueDate"] != DBNull.Value)
                        renewContract.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    if (reader["OriginalDueDate"] != DBNull.Value)
                        renewContract.OriginalDueDate = Convert.ToDateTime(reader["OriginalDueDate"]);
                    if (reader["InstallmentNo"] != DBNull.Value)
                        renewContract.InstallmentNo = Convert.ToInt32(reader["InstallmentNo"]);
                    if (reader["TrainingPeriodStart"] != DBNull.Value)
                        renewContract.TrainingPeriodStart = Convert.ToDateTime(reader["TrainingPeriodStart"]);
                    if (reader["TrainingPeriodEnd"] != DBNull.Value)
                        renewContract.TrainingPeriodEnd = Convert.ToDateTime(reader["TrainingPeriodEnd"]);
                    if (reader["InstallmentDate"] != DBNull.Value)
                        renewContract.InstallmentDate = Convert.ToDateTime(reader["InstallmentDate"]);
                    if (reader["MemberID"] != DBNull.Value)
                        renewContract.MemberID = Convert.ToInt32(reader["MemberID"]);
                    if (reader["PayerId"] != DBNull.Value)
                        renewContract.PayerId = Convert.ToInt32(reader["PayerId"]);
                    if (reader["PayerName"] != DBNull.Value)
                        renewContract.PayerName = Convert.ToString(reader["PayerName"]);
                    if (reader["NumOfInstallments"] != DBNull.Value)
                        renewContract.NumOfInstallments = Convert.ToInt32(reader["NumOfInstallments"]);
                    if (reader["OrderPrice"] != DBNull.Value)
                        renewContract.OrderPrice = Convert.ToInt32(reader["OrderPrice"]);
                    if (reader["PackageName"] != DBNull.Value)
                        renewContract.TemplateName = Convert.ToString(reader["PackageName"]);
                    if (reader["ArticleId"] != DBNull.Value)
                        renewContract.ArticleId = Convert.ToInt32(reader["ArticleId"]);
                    if (reader["PackageId"] != DBNull.Value)
                        renewContract.TemplateId = Convert.ToInt32(reader["PackageId"]);
                    if (reader["NumOfMonths"] != DBNull.Value)
                        renewContract.NoOfMonths = Convert.ToInt32(reader["NumOfMonths"]);
                    renewContract.ActivityArticelName = Convert.ToString(reader["ActivityArticleName"]);
                    if (reader["FixEndDate"] != DBNull.Value)
                        renewContract.FixEndDate = Convert.ToDateTime(reader["FixEndDate"]);
                    renewContract.FixedDueDay = Convert.ToInt32(reader["FixDueDay"]);
                    renewContract.ItemList = GetContractItems(renewContract.TemplateId, _branchId);
                    renewContract.MonthlyItemList = GetContractMonthlyItems(renewContract.MemberContractID, renewContract.ArticleId);
                    renewContractList.Add(renewContract);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return renewContractList;
        }

        private List<ContractItemDC> GetContractItems(int templateId, int branchId)
        {
            List<ContractItemDC> items = new List<ContractItemDC>();
            GetContractTemplateItemsAction itemAction = new GetContractTemplateItemsAction(templateId, branchId);
            items = itemAction.Execute(EnumDatabase.Exceline, _gymCode);
            return items;
        }

        private List<ContractItemDC> GetContractMonthlyItems(int contractId, int articleID)
        {
            List<ContractItemDC> items = new List<ContractItemDC>();
            GetMemberContractItemsAction itemsAction = new GetMemberContractItemsAction(contractId, articleID);
            items = itemsAction.Execute(EnumDatabase.Exceline, _gymCode);
            return items;
        }
    }
}
