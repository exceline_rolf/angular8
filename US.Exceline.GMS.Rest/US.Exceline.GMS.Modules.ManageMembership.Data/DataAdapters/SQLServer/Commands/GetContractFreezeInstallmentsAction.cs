﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractFreezeInstallmentsAction : USDBActionBase<List<ContractFreezeInstallmentDC>>
    {
        private int _freezeItemId;

        public GetContractFreezeInstallmentsAction(int freezeItemId)
        {
            this._freezeItemId = freezeItemId;
        }

        protected override List<ContractFreezeInstallmentDC> Body(DbConnection connection)
        {
            List<ContractFreezeInstallmentDC> freezeInstallmentList = new List<ContractFreezeInstallmentDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetFreezeItemInstallments";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@freezeItemId", DbType.Int32, _freezeItemId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ContractFreezeInstallmentDC freezedInstallment = new ContractFreezeInstallmentDC
                    {
                        Id = Convert.ToInt32(reader["ContractFreezeInstallmentId"]),
                        FreezeItemid = Convert.ToInt32(reader["FreezeItemId"]),
                        MemberId = Convert.ToInt32(reader["FreezeMemberId"]),
                        MemberContractId = Convert.ToInt32(reader["FreezeMemberContractId"]),
                        InstallmentId = Convert.ToInt32(reader["FreezeInstallmentId"]),
                        InstallmentText = reader["FreezeInstallmentText"].ToString(),
                        InstallmentDate = Convert.ToDateTime(reader["FreezeInstallmentDate"]),
                        InstallmentDueDate = Convert.ToDateTime(reader["FreezeInstallmentDueDate"]),
                        InstallmentAmount = Convert.ToDecimal(reader["FreezeInstallmentAmount"]),
                        TrainingPeriodStart = reader["TrainingPeriodStart"] != DBNull.Value ? Convert.ToDateTime(reader["TrainingPeriodStart"]) : (DateTime?)null,
                        TrainingPeriodEnd = reader["TrainingPeriodEnd"] != DBNull.Value ? Convert.ToDateTime(reader["TrainingPeriodEnd"]) : (DateTime?)null,
                        ExtendTrainingPeriodDays = Convert.ToInt32(reader["ExtendTrainingPeriodDays"]),
                        IsUnfreeze = Convert.ToBoolean(reader["IsUnfreeze"]),
                        OrderNo = Convert.ToString(reader["OrderNo"])
                    };
                    freezeInstallmentList.Add(freezedInstallment);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return freezeInstallmentList;
        }
    }
}
