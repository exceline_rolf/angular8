﻿
// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : NAD
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberContractLastInstallmentAction : USDBActionBase<InstallmentDC>
    {

        private int _memberContractId = -1;
        private string _gymCode = string.Empty;

        public GetMemberContractLastInstallmentAction(int memberContractId, string gymCode)
        {
            _memberContractId = memberContractId;
            _gymCode = gymCode;
        }

        protected override InstallmentDC Body(System.Data.Common.DbConnection connection)
        {
            InstallmentDC installment = new InstallmentDC();
            string spName = "USExceGMSManageMembershipGetContractLastInstallment";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", System.Data.DbType.Int32, _memberContractId));

                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    installment.MemberId = Convert.ToInt32(reader["MemberId"]);
                    installment.Id = Convert.ToInt32(reader["ID"]);
                    installment.TableId = Convert.ToInt32(reader["ID"]);
                    installment.MemberContractId = Convert.ToInt32(reader["MemberContractId"]);
                    installment.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    installment.Text = Convert.ToString(reader["Text"]);
                    installment.InstallmentDate = Convert.ToDateTime(reader["InstallmentDate"]);
                    installment.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    installment.Amount = Convert.ToDecimal(reader["Amount"]);
                    if (reader["AddOnPrice"] != DBNull.Value)
                        installment.AdonPrice = Convert.ToDecimal(reader["AddOnPrice"]);
                    if (reader["Discount"] != DBNull.Value)
                        installment.Discount = Convert.ToDecimal(reader["Discount"]);
                    if (reader["SponsoredAmount"] != DBNull.Value)
                        installment.SponsoredAmount = Convert.ToDecimal(reader["SponsoredAmount"]);
                    installment.Comment = reader["Comment"].ToString();
                    if (reader["OriginalMatuarity"] != DBNull.Value)
                        installment.OriginalDueDate = Convert.ToDateTime(reader["OriginalMatuarity"]);
                    installment.Balance = Convert.ToDecimal(reader["Balance"]);
                    if (reader["InvoiceGeneratedDate"] != DBNull.Value)
                        installment.InvoiceGeneratedDate = Convert.ToDateTime(reader["InvoiceGeneratedDate"]);
                    installment.InstallmentNo = Convert.ToInt32(reader["InstallmentNo"]);
                    installment.IsInvoiced = Convert.ToBoolean(reader["InvoiceGenerated"]);
                    if (reader["TSDate"] != DBNull.Value)
                        installment.TrainingPeriodStart = Convert.ToDateTime(reader["TSDate"]);
                    if (reader["TEDate"] != DBNull.Value)
                        installment.TrainingPeriodEnd = Convert.ToDateTime(reader["TEDate"]);
                    installment.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    installment.IsOtherPay = Convert.ToBoolean(reader["IsOtherPay"]);
                    installment.ContractName = Convert.ToString(reader["TemplateName"]);
                    installment.ActivityName = Convert.ToString(reader["ActivityName"]);
                    installment.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    installment.OriginalCustomer = Convert.ToString(reader["OriginalMember"]);
                    installment.EstimatedOrderAmount = Convert.ToDecimal(reader["Amount"]);
                    installment.OrderNo = Convert.ToString(reader["OrderNo"]);
                    installment.ActivityPrice = Convert.ToDecimal(reader["ActivityPrice"]);
                    if (reader["EstimatedInvoiceDate"] != DBNull.Value)
                        if (reader["EstimatedInvoiceDate"] != DBNull.Value)
                            installment.EstimatedInvoiceDate = Convert.ToDateTime(reader["EstimatedInvoiceDate"]);

                    installment.AdonText = Convert.ToString(reader["AdonText"]);
                    installment.PayerName = Convert.ToString(reader["PayerName"]);
                    if (reader["PayerID"] != DBNull.Value)
                        installment.PayerId = Convert.ToInt32(reader["PayerID"]);
                    installment.IsSpOrder = Convert.ToBoolean(reader["IsSpOrder"]);
                    installment.IsInvoiceFeeAdded = Convert.ToBoolean(reader["IsInvoiceFee"]);
                    installment.BranchName = Convert.ToString(reader["BranchName"]);
                    installment.IsLocked = Convert.ToBoolean(reader["IsLocked"]);
                    installment.ServiceAmount = Convert.ToDecimal(reader["ServiceAmount"]);
                    installment.ItemAmount = Convert.ToDecimal(reader["ItemAmount"]);
                    installment.InstallmentType = Convert.ToString(reader["InstallmentType"]);
                    installment.TemplateId = Convert.ToInt32(reader["TemplateId"]);
                    if (installment.Id > 0)
                    {
                        GetOrderShareAction adonAction = new GetOrderShareAction(installment.Id);
                        installment.AddOnList = adonAction.Execute(EnumDatabase.Exceline, _gymCode);
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return installment;
        }
    }
}
