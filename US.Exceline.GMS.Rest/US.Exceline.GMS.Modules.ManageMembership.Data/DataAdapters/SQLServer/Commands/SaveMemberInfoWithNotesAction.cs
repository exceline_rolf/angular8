﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
  public  class SaveMemberInfoWithNotesAction  : USDBActionBase<string>
    {
      private OrdinaryMemberDC _member = new OrdinaryMemberDC();
      private DataTable _dataTabel = null;
      private DataTable _dataTabelFamily = null;
      private DataTable _memberATGStatusTable = null;
      private int _groupId = -1;
      private int _familyId = -1;
      public SaveMemberInfoWithNotesAction(OrdinaryMemberDC member)
      {
          _member = member;
            
             
          if (member.GroupMemberList != null )
          {
              _groupId = member.Id;
              _dataTabel = GetGroupMemberLst(member.GroupMemberList);
             
          }
             
          if (member.FamilyMmeberList != null )
          {
              _familyId = member.Id;
              _dataTabelFamily = GetFamilyMemberLst(member.FamilyMmeberList);
            
          }

          if (_member.AtgStatusList != null)
          {
              _memberATGStatusTable = GetATGStatus(member.AtgStatusList);
          }  
      }

      private DataTable GetFamilyMemberLst(List<ExcelineMemberDC> familyMemberLst)
      {
          _dataTabelFamily = new DataTable();
          _dataTabelFamily.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
          _dataTabelFamily.Columns.Add(new DataColumn("FamilyId", Type.GetType("System.Int32")));


          foreach (ExcelineMemberDC member in familyMemberLst)
          {
              DataRow dataTableRow = _dataTabelFamily.NewRow();
              dataTableRow["ID"] = member.Id;
              dataTableRow["FamilyId"] = _familyId;

              _dataTabelFamily.Rows.Add(dataTableRow);
          }

          return _dataTabelFamily;
      }


      private DataTable GetGroupMemberLst(List<ExcelineMemberDC> groupMemberLst)
      {
          _dataTabel = new DataTable();
          _dataTabel.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
          _dataTabel.Columns.Add(new DataColumn("GroupId", Type.GetType("System.Int32")));


          foreach (ExcelineMemberDC member in groupMemberLst)
          {
              DataRow dataTableRow = _dataTabel.NewRow();
              dataTableRow["ID"] = member.Id;
              dataTableRow["GroupId"] = _groupId;

              _dataTabel.Rows.Add(dataTableRow);
          }

          return _dataTabel;
      }

      private DataTable GetATGStatus(List<MemberATGStatusInfo> statusList)
      {
          _memberATGStatusTable = new DataTable();
          _memberATGStatusTable.Columns.Add(new DataColumn("ID", typeof(int)));
          _memberATGStatusTable.Columns.Add(new DataColumn("MemberID", typeof(int)));
          _memberATGStatusTable.Columns.Add(new DataColumn("AccountNo", typeof(string)));
          _memberATGStatusTable.Columns.Add(new DataColumn("ATGStatus", typeof(int)));
          _memberATGStatusTable.Columns.Add(new DataColumn("LastUpdatedDate", typeof(DateTime)));

          foreach (MemberATGStatusInfo statusInfo in statusList)
          {
              DataRow dataTableRow = _memberATGStatusTable.NewRow();
              dataTableRow["ID"] = statusInfo.Id;
              dataTableRow["MemberID"] = statusInfo.MemberID;
              dataTableRow["AccountNo"] = statusInfo.AccountNumber;
              dataTableRow["ATGStatus"] = GetSelectedStatusIndex(statusInfo.SelectedStatus);
              if(statusInfo.LastUpdatedDateTime.HasValue)
                 dataTableRow["LastUpdatedDate"] = statusInfo.LastUpdatedDateTime;
              _memberATGStatusTable.Rows.Add(dataTableRow);
          }
          return _memberATGStatusTable;
      }

      private int GetSelectedStatusIndex(int status)
      {
          switch (status)
          {
              case 0:
                  return 1;
              case 1:
                  return 2;
              case 2:
                  return 5;
              case 3:
                  return 6;
              default:
                  return -1;
          }
      }

        protected override string Body(DbConnection connection)
        {
            string atgKid = string.Empty;
            const string storedProcedureName = "USExceGMSManageMembershipSaveMemberInfoWithNotes";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _member.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _member.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowUp", DbType.Boolean, _member.IsFollowUp));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@adcSignedDate", DbType.DateTime, _member.AdcSignedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@note", DbType.String, _member.Note));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sendAdvertisement", DbType.Boolean, _member.IsSendAdvertisement));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@guestCardNo", DbType.String, _member.GuestCardNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@chargeLimitAtg", DbType.Decimal, _member.ChargeLimitAtg));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@oldKid", DbType.String, _member.OldKid));
                  
                if (_member.InstructorId > 0)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InstructorId", DbType.Int32, _member.InstructorId));
                if (_member.ContactPersonId > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContactPersonId", DbType.Int32, _member.ContactPersonId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccountNumber", DbType.String, _member.AccountNo));
                if (_member.IntroducedById != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IntroducerId", DbType.Int32, _member.IntroducedById));
                if (_groupId != -1)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@groupId", DbType.Int32, _groupId));
                if (_familyId != -1)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@familyId", DbType.Int32, _familyId));

                if (_member.GroupMemberList != null )
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberLst", SqlDbType.Structured, _dataTabel));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberLst", SqlDbType.Structured, null));
                }

                if (_member.FamilyMmeberList != null )
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@familyLst", SqlDbType.Structured, _dataTabelFamily));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@familyLst", SqlDbType.Structured, null));
                }

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberATGStatus", SqlDbType.Structured, _memberATGStatusTable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAddEmployee", DbType.Boolean, _member.IsAddEmployee));

                if (_member.LinkedEmployeeId > 0)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LinkedEmployeeId", DbType.Int32, _member.LinkedEmployeeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsRemovedEmployee", DbType.Int32, _member.IsRemovedEmployee));


                if(!string.IsNullOrEmpty(_member.ModifiedUser))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _member.ModifiedUser));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GatCardNo", DbType.String, _member.GATCardNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@checkFingerPrint", SqlDbType.Bit, _member.CheckFingerPrint));
                if (_member.PinCode != 0 && _member.PinCode != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@pinCode", DbType.Int32, _member.PinCode));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BasicContractId", DbType.Int32, _member.BasicContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BasicContractenddate", DbType.DateTime, _member.CtEndDate));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 100;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);

                cmd.ExecuteNonQuery();
                atgKid = Convert.ToString(output.Value);

            }
            catch (Exception ex)
            {
                
                throw ex;

            }
            return atgKid;
        }
}
}
