﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractBookingsAction : USDBActionBase<List<ContractBookingDC>>
    {
        private int _contractId = -1;
        public GetContractBookingsAction(int contractId)
        {
            _contractId = contractId;
        }

        protected override List<ContractBookingDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ContractBookingDC> bookingsList = new List<ContractBookingDC>();
            string spName = "USExceGMSManageMembershipGetContractBookings";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@contractId", System.Data.DbType.Int32, _contractId));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ContractBookingDC booking = new ContractBookingDC();
                    booking.BookingId = Convert.ToInt32(reader["ID"]);
                    booking.BookingAmount = Convert.ToDecimal(reader["BookingAmount"]);
                    booking.BookingStartTime = Convert.ToDateTime(reader["StartTime"]);
                    booking.BookingEndTime = Convert.ToDateTime(reader["EndTime"]);
                    booking.Day = Convert.ToString(reader["Day"]);
                    booking.ResourceId = Convert.ToInt32(reader["ResourceId"]);
                    booking.ResourceName = Convert.ToString(reader["Name"]);
                    bookingsList.Add(booking);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bookingsList;
        }
    }
}
