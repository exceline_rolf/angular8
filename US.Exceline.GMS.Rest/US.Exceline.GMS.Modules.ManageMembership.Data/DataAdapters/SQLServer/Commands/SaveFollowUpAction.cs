﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : GMA
// Created Timestamp : "10/24/2013 6:07:55 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.Utils;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveFollowUpAction : USDBActionBase<int>
    {
        private readonly FollowUpDC _followUp;
        private DataTable _dataTable;
        private int _outputId = -1;
        private string _user = string.Empty;

        public SaveFollowUpAction(FollowUpDC followUp, string user)
        {
            _followUp = followUp;
            _user = user;
            if (followUp.FollowUpDetailList != null)
                _dataTable = GetFollowUpDetailDataTable(followUp.FollowUpDetailList);
        }

        private DataTable GetFollowUpDetailDataTable(List<FollowUpDetailDC> followUpDetailList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("Id", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("Name", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("TaskCategoryId", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("StartDate", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("EndDate", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("StartTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("EndTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("FoundDate", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("ReturnDate", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("DueDate", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("DueTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("NumOfDays", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("PhoneNo", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("Sms", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("ExtenedInfo", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("CreatedDateTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("LastModifiedDateTime", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("CreatedUser", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("LastModifiedUser", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("ActiveStatus", typeof(bool)));
            _dataTable.Columns.Add(new DataColumn("IsShowInCalandar", typeof(bool)));
            _dataTable.Columns.Add(new DataColumn("FollowUpId", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("AssignedEmpId", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("EntRoleId", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("CompletedDate", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("CompletedEmpId", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("PlannedDate", typeof(DateTime)));
            _dataTable.Columns.Add(new DataColumn("IsFixed", typeof(bool)));
            _dataTable.Columns.Add(new DataColumn("Email", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("Description", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("IsNextFollowUp", typeof(bool)));
            _dataTable.Columns.Add(new DataColumn("Comment", typeof(string)));
            _dataTable.Columns.Add(new DataColumn("CompletedStatus", typeof(bool)));
            _dataTable.Columns.Add(new DataColumn("IsPopUp", typeof(bool)));
            _dataTable.Columns.Add(new DataColumn("IsMoved", typeof(bool)));

            foreach (FollowUpDetailDC followUpDetail in followUpDetailList)
            {
                DataRow dataTableRow = _dataTable.NewRow();
                dataTableRow["Id"] = followUpDetail.Id;
                dataTableRow["Name"] = followUpDetail.Name;
                dataTableRow["TaskCategoryId"] = followUpDetail.TaskCategoryId ?? (object)DBNull.Value;
                dataTableRow["StartDate"] = followUpDetail.StartDate ?? (object)DBNull.Value;
                dataTableRow["EndDate"] = followUpDetail.EndDate ?? (object)DBNull.Value;
                dataTableRow["StartTime"] = followUpDetail.StartTime ?? (object)DBNull.Value;
                dataTableRow["EndTime"] = followUpDetail.EndTime ?? (object)DBNull.Value;
                dataTableRow["FoundDate"] = followUpDetail.FoundDate ?? (object)DBNull.Value;
                dataTableRow["ReturnDate"] = followUpDetail.ReturnDate ?? (object)DBNull.Value;
                dataTableRow["DueDate"] = followUpDetail.DueDate ?? (object)DBNull.Value;
                dataTableRow["DueTime"] = followUpDetail.DueTime ?? (object)DBNull.Value;
                dataTableRow["NumOfDays"] = followUpDetail.NumOfDays;
                dataTableRow["PhoneNo"] = followUpDetail.PhoneNo ?? (object)DBNull.Value;
                dataTableRow["Sms"] = followUpDetail.AutomatedSMS ?? (object)DBNull.Value;
                dataTableRow["ExtenedInfo"] = XMLUtils.SerializeDataContractObjectToXML(new ExtendedFieldInfoDC() { ExtendedFieldsList = followUpDetail.ExtendedFieldsList });
                dataTableRow["CreatedDateTime"] = followUpDetail.CreatedDateTime ?? (object)DBNull.Value;
                dataTableRow["LastModifiedDateTime"] = followUpDetail.LastModifiedDateTime ?? (object)DBNull.Value;
                dataTableRow["CreatedUser"] = followUpDetail.CreatedUser ?? (object)DBNull.Value;
                dataTableRow["LastModifiedUser"] = followUpDetail.LastModifiedUser ?? (object)DBNull.Value;
                dataTableRow["ActiveStatus"] = followUpDetail.ActiveStatus ?? (object)DBNull.Value;
                dataTableRow["IsShowInCalandar"] = followUpDetail.IsShowInCalandar ?? (object)DBNull.Value;
                dataTableRow["FollowUpId"] = followUpDetail.FollowUpId ?? (object)DBNull.Value;
                dataTableRow["AssignedEmpId"] = followUpDetail.AssignedEmpId ?? (object)DBNull.Value;
                dataTableRow["EntRoleId"] = followUpDetail.EntRoleId ?? (object)DBNull.Value;
                dataTableRow["CompletedDate"] = followUpDetail.CompletedDate ?? (object)DBNull.Value;
                dataTableRow["CompletedEmpId"] = followUpDetail.CompletedEmpId ?? (object)DBNull.Value;
                dataTableRow["PlannedDate"] = followUpDetail.PlannedDate ?? (object)DBNull.Value;
                dataTableRow["IsFixed"] = followUpDetail.IsFixed;
                dataTableRow["Email"] = followUpDetail.AutomatedEmail ?? (object)DBNull.Value;
                dataTableRow["Description"] = followUpDetail.Description ?? (object)DBNull.Value;
                dataTableRow["IsNextFollowUp"] = followUpDetail.IsNextFollowUpValue;
                dataTableRow["Comment"] = followUpDetail.Comment ?? (object)DBNull.Value;
                dataTableRow["CompletedStatus"] = followUpDetail.CompletedStatus;
                dataTableRow["IsPopUp"] = followUpDetail.IsPopUp;
                dataTableRow["IsMoved"] = followUpDetail.IsMoved;
                _dataTable.Rows.Add(dataTableRow);
            }

            return _dataTable;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAddFollowUps";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _followUp.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _followUp.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowUpTemplateId", DbType.Int32, _followUp.FollowUpTemplateId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _followUp.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SmsPhoneNo", DbType.String, _followUp.SmsPhoneNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SmsPrefix", DbType.String, _followUp.SmsPrefix));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmailAddress", DbType.String, _followUp.EmailAddress));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsInsert", DbType.String, _followUp.IsInsert));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowUpDetailList", SqlDbType.Structured, _dataTable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@outId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
                if (param.Value != DBNull.Value) _outputId = Convert.ToInt32(param.Value);
                return _outputId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
