﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateInvoicePdfPathAction : USDBActionBase<bool>
    {
        private int _arItemNo;
        private string _invoiceFilePath = string.Empty;
        public UpdateInvoicePdfPathAction(int arItemNo, string invoiceFilePath)
        {
            _arItemNo = arItemNo;
            _invoiceFilePath = invoiceFilePath;
        }
        protected override bool Body(DbConnection connection)
        {
            bool result;
            const string storedProcedureName = "USExceGMSManageMembershipUpdateInvoicePdfPathByArItemId";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", DbType.Int32, _arItemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceFilePath", DbType.String, _invoiceFilePath));

                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
