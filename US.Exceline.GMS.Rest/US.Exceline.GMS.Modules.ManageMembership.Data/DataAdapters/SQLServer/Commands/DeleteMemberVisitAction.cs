﻿using US_DataAccess;
using System.Data;
using System.Data.Common;
/* --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.ManageMembership.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/11/2012 2:45:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteMemberVisitAction : USDBActionBase<bool>
    {
        private int _memberVisitId;
        private int _memberContractId;

        public DeleteMemberVisitAction(int memberVisitId, int memberContractId)
        {
            _memberVisitId = memberVisitId;
            _memberContractId = memberContractId;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSManageMembershipDeleteMemberVisit";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityVisitId", DbType.Int32, _memberVisitId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _memberContractId));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@code", DbType.Int32, _memberContractId)
                cmd.ExecuteNonQuery();
                result = true;
                 
            }
            catch (Exception ex)
            { 
                 
                throw ex;
            }
            return result;
        }

    }
}
