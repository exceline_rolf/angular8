﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateExternalCustomerAction : USDBActionBase<bool>
   {
       private readonly int _memberId;
       private readonly int _userId;
       public UpdateExternalCustomerAction(int memberId, int userId)
       {
           _memberId = memberId;
           _userId = userId;

       }

       protected override bool Body(DbConnection connection)
       {
           bool result = false;
           const string storedProcedureName = "USExceGMSManageMembershipUpdateExternalCustomerId";

           try
           {
               DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberId));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserId", DbType.Int32, _userId));
               cmd.ExecuteNonQuery();
               result = true;

           }
           catch (Exception ex)
           {

               throw ex;
           }
           return result;
       }
   }
}
