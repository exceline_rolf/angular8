﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetInfoCategotysForMemberAction : USDBActionBase<List<CategoryDC>>
    {
        private int _memberId;
        private int _branchId;
        public GetInfoCategotysForMemberAction(int memberId, int branchId)
        {
            _memberId = memberId;
            _branchId = branchId;
        }

        protected override List<CategoryDC> Body(DbConnection connection)
        {
            List<CategoryDC> infoCategotyListforMember = new List<CategoryDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetInfoCategotysForMember";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CategoryDC InfoCategoryItem = new CategoryDC();
                    InfoCategoryItem.Id = Convert.ToInt32(reader["InfoCategoryId"]);
                    InfoCategoryItem.Code = Convert.ToString(reader["InfoCategoryName"]);
                    InfoCategoryItem.Name = Convert.ToString(reader["InfoCategoryCode"]);
                    InfoCategoryItem.IsCheck = true;
                    infoCategotyListforMember.Add(InfoCategoryItem);

                }
            }
            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return infoCategotyListforMember;

        }
    }
}
