﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/8/2012 5:39:29 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateContractSettingRefAction : USDBActionBase<bool>
    {
        private int _contarctId = -1;
        private int _settingId = -1;

        public UpdateContractSettingRefAction(int contarctId, int settingId)
        {
            _contarctId = contarctId;
            _settingId = settingId;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;

            string StoredProcedureName = "USExceGMSManageMembershipUpdateContractSettingRef";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractId", DbType.Int32, _contarctId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SettingId", DbType.Int32, _settingId));
                cmd.ExecuteNonQuery();
                result = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public bool UpdateSettingRef(DbTransaction transaction)
        {
            string spName = "USExceGMSManageMembershipUpdateContractSettingRef";
            bool result = false;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContarctId", System.Data.DbType.Int32, _contarctId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SettingId", System.Data.DbType.Int32, _settingId));
                command.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
