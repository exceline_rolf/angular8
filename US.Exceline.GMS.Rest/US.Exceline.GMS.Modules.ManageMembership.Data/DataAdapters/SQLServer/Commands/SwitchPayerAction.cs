﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SwitchPayerAction : USDBActionBase<int>
    {
        private int _memberId;
        private int _payerId;
        private DateTime _activatedDate;
        private bool _isSave = true;

        public SwitchPayerAction(int memberId, int payerId, DateTime activatedDate, bool isSave)
        {
            _memberId=memberId;
            _payerId = payerId;
            _activatedDate = activatedDate;
            _isSave = isSave;
        }

        protected override int Body(DbConnection connection)
        {
           // bool _isSwitched = false;
            int payerId = -2;
            string StoredProcedureName = "USExceGMSManageMembershipSaveSwichPayer";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@payerId", DbType.Int32, _payerId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@datetime", DbType.DateTime, _activatedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isSave", DbType.Boolean, _isSave));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@outId";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);



                cmd.ExecuteNonQuery();
                if (output.Value != null)
                {
                    payerId = Convert.ToInt32(output.Value);
                }
                else
                {
                    payerId = -2;
                }
               
            }

            catch (Exception ex)
            {
                throw ex ;
            }
            return payerId;
        }
    }
}
