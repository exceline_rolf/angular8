﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class ResignContractAction : USDBActionBase<bool>
    {
        private ContractResignDetailsDC _resignDetails = null;
        private string _user = string.Empty;
        public ResignContractAction(ContractResignDetailsDC resignDetails, string user)
        {
            _resignDetails = resignDetails;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipSaveResignation";
            DbTransaction transaction = connection.BeginTransaction();
            try
            {
                DbCommand command = new SqlCommand();
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = spName;

                command.Parameters.Add(new SqlParameter("@MemberContractId", _resignDetails.MemberContractId));
                command.Parameters.Add(new SqlParameter("@ResignDate", _resignDetails.ResignDate));
                command.Parameters.Add(new SqlParameter("@ContractEndDate", _resignDetails.ContractEndDate));
                command.Parameters.Add(new SqlParameter("@LockInPeriod", _resignDetails.LockInPeriod));
                if(_resignDetails.LockUntillDate.HasValue)
                  command.Parameters.Add(new SqlParameter("@LockUntillDate", _resignDetails.LockUntillDate));
                command.Parameters.Add(new SqlParameter("@ResignCategoryId", _resignDetails.ResignCategoryId));
                command.Parameters.Add(new SqlParameter("@MemberFeeArticleID", _resignDetails.MemberFeeArticleID));
                command.Parameters.Add(new SqlParameter("@User", _user));
                command.ExecuteNonQuery();

                //Update the deleted orders
                HandleResignedOrdersAction handleOrderaction = new HandleResignedOrdersAction(_resignDetails.DeletedOrders, _resignDetails.MergedOrders, _resignDetails.MemberContractId, _user);
                handleOrderaction.RunOnTransaction(transaction);

                //Update the merged order
                foreach(InstallmentDC ins in _resignDetails.UpdatedInstallment)
                {
                    UpdateMemberInstallment updateAction = new UpdateMemberInstallment(ins);
                    updateAction.RunOnTransaction(transaction);
                }
                transaction.Commit();
                return true;
            }
            catch 
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
