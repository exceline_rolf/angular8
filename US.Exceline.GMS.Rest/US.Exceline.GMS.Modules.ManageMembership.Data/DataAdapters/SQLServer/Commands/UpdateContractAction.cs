﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System.Linq;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using System;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateContractAction : USDBActionBase<int>
    {
        private MemberContractDC _memberContract;
        private bool _isRenew = false;
        private string _user = string.Empty;

        public UpdateContractAction(MemberContractDC contract, bool isRenew, string user)
        {
            _memberContract = contract;
            _isRenew = isRenew;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipUpdateMemberContract";
            DbTransaction transaction = null;
            int status =1;
            try
            {
                transaction = connection.BeginTransaction();
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, _memberContract.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@templateId", DbType.Int32, _memberContract.ContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@nextTemplateId", DbType.Int32, _memberContract.NextTemplateId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startupItemPrice", DbType.Decimal, _memberContract.StartUpItemPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@monthlyItemPrice", DbType.Decimal, _memberContract.EveryMonthItemPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractPrice", DbType.Decimal, _memberContract.ContractPrize));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractStartDate", DbType.DateTime, _memberContract.ContractStartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractEndDate", DbType.DateTime, _memberContract.ContractEndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lockUntillDate", DbType.DateTime, _memberContract.LockInPeriodUntilDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priceGuarantyUntillDate", DbType.DateTime, _memberContract.PriceGuarantyUntillDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isATG", DbType.Boolean, _memberContract.IsATG));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceDetails", DbType.Boolean, _memberContract.IsInvoiceDetails));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@autoRenew", DbType.Boolean, _memberContract.AutoRenew));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityId", DbType.Int32, _memberContract.ActivityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@signupCategoryId", DbType.Int32, _memberContract.SignUpCategoryId));
                if(_memberContract.RenewedTemplateID  != -1)
                   cmd.Parameters.Add(DataAcessUtils.CreateParam("@RenewedTemplate", DbType.Int32, _memberContract.RenewedTemplateID));
                if (_memberContract.RenewEffectiveDate.HasValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@RenewEffectDate", DbType.DateTime, _memberContract.RenewEffectiveDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsRenew", DbType.Boolean, _isRenew));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AvailabelVisits", DbType.Int32, _memberContract.AvailableVisits));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SoldVisit", DbType.Int32, _memberContract.NumberOfVisits));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PackageTypeId", DbType.Int32, _memberContract.ContractTypeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _memberContract.BranchId));
                if (_memberContract.AccessProfileId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@accessProfileId", DbType.Int32, _memberContract.AccessProfileId));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _memberContract.CreatedUser));

                if (_memberContract.MemberFeeArticleID > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberFeeArticleId", DbType.Int32, _memberContract.MemberFeeArticleID));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberFeeMonth", DbType.Decimal, _memberContract.MemberFeeMonth));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberFeeLastInvoicedDate", DbType.DateTime, _memberContract.MemFeeLastInvoicedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractConditionId", DbType.Int32, _memberContract.ContractConditionId));

                DbParameter outPara = new SqlParameter();
                outPara.DbType = System.Data.DbType.Int32;
                outPara.ParameterName = "@StatusCode";
                outPara.Direction = System.Data.ParameterDirection.Output;
                outPara.Value = 1;
                cmd.Parameters.Add(outPara);
                cmd.ExecuteNonQuery();

                status = Convert.ToInt32(outPara.Value);
                if (status == 1 || status == 2)
                {

                    UpdateMemberContractItems itemUpdateAction = new UpdateMemberContractItems(_memberContract.Id, _memberContract.StartUpItemList, _memberContract.EveryMonthItemList, _user);
                    itemUpdateAction.RunOnTransaction(transaction);

                    foreach (InstallmentDC installment in _memberContract.InstalllmentList)
                    {
                        // change createdUser on installment to user --> so modifieduser is user and not createdUser (createduser of the installment) 
                        // - Martin Tømmerås 08.02.2019
                        installment.CreatedUser = _user;
                        UpdateMemberInstallment updateInstallmentAction = new UpdateMemberInstallment(installment);
                        updateInstallmentAction.RunOnTransaction(transaction);
                    }

                    if (!_isRenew)
                    {
                        SaveContractGroupMembersAction updateMembersAction = new SaveContractGroupMembersAction(_memberContract.MemberId, _memberContract.GroupMembers, _memberContract.Id, _user);
                        updateMembersAction.RunOnTransaction(transaction);
                    }
                    transaction.Commit();
                    return status;
                }else
                {
                    transaction.Rollback();
                    return status;
                }

            }
            catch
            {
                transaction.Rollback();
                throw;
            }

        }
    }
}
