﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetFamilyMembersAction : USDBActionBase<List<ExcelineMemberDC>>
    {
      private int _memberId;
      private bool _activeState = true;
      public GetFamilyMembersAction(int memberId,bool activeState)
      {
          _memberId = memberId;
          _activeState = activeState;
      }

      protected override List<ExcelineMemberDC> Body(DbConnection connection)
        {
            List<ExcelineMemberDC> ordinaryMemberLst = new List<ExcelineMemberDC>();
            const string storedProcedureName = "USExceGMSManageMembershipGetFamilyMember";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Boolean, _activeState));
               
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExcelineMemberDC ordineryMember = new ExcelineMemberDC
                        {
                            Name = reader["FamilyMemberName"].ToString(),
                            Id = Convert.ToInt32(reader["FamilyMemberId"]),
                            CustId = reader["CustId"].ToString()
                        };
                    ordinaryMemberLst.Add(ordineryMember);
                }
            }

            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return ordinaryMemberLst;
        }

    }
}
