﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class AddSponserShipForMemberAction : USDBActionBase<bool>
    {
        private int _memContractId = -1;
        private int _sponserContractId = -1;
        private int _employeeCategoty = -1;
        private int _actionType = -1;
        private string _employeeRef = string.Empty;
        //   if actionType=0-->remove else -->add  
        public AddSponserShipForMemberAction(int memContractId, int sponserContractId, int employeeCategotyID, int actionType, string employeeRef)
        {
            _memContractId = memContractId;
            _sponserContractId = sponserContractId;
            _employeeCategoty = employeeCategotyID;
            _employeeRef = employeeRef;
            _actionType = actionType;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSManageMembershipAddSponserShip";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memContractId", DbType.Int32, _memContractId));
                if (_actionType == 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponserContractId", DbType.Int32, null));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeCategoty", DbType.Int32, null));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeRef", DbType.String, _employeeRef));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponserContractId", DbType.Int32, _sponserContractId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeCategoty", DbType.Int32, _employeeCategoty));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeRef", DbType.String, _employeeRef));
                }
                cmd.ExecuteNonQuery();
                result = true;

            }
            catch 
            {
                throw;
            }
            return result;
        }
    }
}
