﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMembersByStatusAction : USDBActionBase<List<OrdinaryMemberDC>>
    {
        private readonly int _statusId = 1;
        private string _systemName = string.Empty;
       // private string _user = string.Empty;
       // private string _gymCode = string.Empty;

        public GetMembersByStatusAction(int statusId, string systemName, string user, string gymCode)
        {
            _statusId = statusId;
            _systemName = systemName;
           // _user = user;
           // _gymCode = gymCode;
        }

        protected override List<OrdinaryMemberDC> Body(DbConnection connection)
        {
            List<OrdinaryMemberDC> ordinaryMemberList = new List<OrdinaryMemberDC>();
            const string storedProcedureName = "USExceGMSGetMembersByStatus";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@statusID", DbType.Int32, _statusId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@systemName", DbType.String, _systemName));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    OrdinaryMemberDC ordineryMember = new OrdinaryMemberDC();
                    ordineryMember.Id = Convert.ToInt32(reader["MemberID"]);
                    ordineryMember.Address1 = reader["Addr1"].ToString();
                    ordineryMember.Address2 = reader["Addr2"].ToString();
                    ordineryMember.Address3 = reader["Addr3"].ToString();
                    if (reader["Born"] != DBNull.Value)
                        ordineryMember.BirthDate = Convert.ToDateTime(reader["Born"]);
                    ordineryMember.PostPlace = reader["ZipName"].ToString();
                    ordineryMember.PostCode = reader["ZipCode"].ToString();
                    ordineryMember.Email = Convert.ToString(reader["Email"]);
                    ordineryMember.FirstName = reader["FirstName"].ToString();
                    ordineryMember.LastName = reader["LastName"].ToString();
                    ordineryMember.Name = reader["Name"].ToString();
                    string gender = reader["Gender"].ToString();
                    if (!string.IsNullOrEmpty(gender.Trim()))
                    {
                        try
                        {
                            ordineryMember.Gender = (Gender)Enum.Parse(typeof(Gender), gender);
                        }
                        catch { }
                    }
                    ordineryMember.Mobile = Convert.ToString(reader["TelMobile"]).Trim();
                    ordineryMember.PrivateTeleNo = Convert.ToString(reader["TelHome"]).Trim();
                    ordineryMember.WorkTeleNo = Convert.ToString(reader["TelWork"]).Trim();
                    ordineryMember.IntegratedId = Convert.ToInt32(reader["IntegratedId"]);
                    ordineryMember.CustId = Convert.ToString(reader["CustId"]);
                    if (!string.IsNullOrEmpty(gender.Trim()))
                    {
                        try
                        {
                            ordineryMember.Gender = (Gender)Enum.Parse(typeof(Gender), gender);
                        }
                        catch { }
                    }
                    if (reader["BranchId"] != DBNull.Value)
                        ordineryMember.BranchId = Convert.ToInt32(reader["BranchId"]);
                    if (ordineryMember.IntegratedId > 0)
                    {
                        MemberIntegrationSettingDC _integrationSetting = new MemberIntegrationSettingDC();
                        _integrationSetting.SystemName = reader["SystemName"].ToString();
                        _integrationSetting.FacilityUserId = reader["FacilityUserId"].ToString();
                        _integrationSetting.UserId = reader["UserId"].ToString();
                        _integrationSetting.Token = reader["Token"].ToString();
                        _integrationSetting.MemberId = ordineryMember.Id;
                        _integrationSetting.BranchId = ordineryMember.BranchId;

                        ordineryMember.IntegrationSetting = _integrationSetting;
                    }
                    ordinaryMemberList.Add(ordineryMember);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ordinaryMemberList;
        }
    }
}
