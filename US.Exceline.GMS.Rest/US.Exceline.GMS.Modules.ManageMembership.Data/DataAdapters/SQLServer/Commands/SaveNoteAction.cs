﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveNoteAction : USDBActionBase<int>
    {
        private int _memberId = -1;
        private string _note = string.Empty;
        public SaveNoteAction(int memberid,string note)
        {
            _memberId = memberid;
            _note = note;
        }

        protected override int Body(DbConnection connection)
        {
            int noteId = -1;
            string StoredProcedureName = "USExceGMSManageMembershipAddNote";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@note", DbType.String, _note));


                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@outId";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);



                cmd.ExecuteNonQuery();
                if (output.Value != null)
                {
                    noteId = Convert.ToInt32(output.Value);
                }
                else
                {
                    noteId = -1;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return noteId;
        }
    }
}
