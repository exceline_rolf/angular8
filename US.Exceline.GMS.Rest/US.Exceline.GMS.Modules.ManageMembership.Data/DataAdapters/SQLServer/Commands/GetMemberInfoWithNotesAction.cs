﻿using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberInfoWithNotesAction : USDBActionBase<OrdinaryMemberDC>
    {
        private readonly int _memberId = -1;
        private readonly int _branchId = -1;
        private string _gymCode = string.Empty;

         public GetMemberInfoWithNotesAction(int branchId, int memberId, string gymCode)
        {
            _memberId = memberId;
            _branchId = branchId;
            _gymCode = gymCode;
        }
        protected override OrdinaryMemberDC Body(DbConnection connection)
        {
            OrdinaryMemberDC ordinaryMember = new OrdinaryMemberDC();
            const string storedProcedureName = "USExceGMSManageMembershipGetMemberInfoWithNotes";
            DbDataReader reader = null;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ordinaryMember.Id = Convert.ToInt32(reader["MemberId"]);
                    ordinaryMember.MemberCardNo = Convert.ToString(reader["MemCardNo"]);
                    ordinaryMember.Role = (MemberRole)Enum.Parse(typeof(MemberRole), Convert.ToString(reader["RoleId"]));
                    ordinaryMember.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    ordinaryMember.RegisterDate = Convert.ToDateTime(reader["CreatedDate"]);
                    ordinaryMember.InstructorId = Convert.ToInt32(reader["InstructorId"]);
                    ordinaryMember.Instructor = Convert.ToString(reader["InstructorName"]);
                    if (reader["ContactPersonId"] != DBNull.Value)
                    ordinaryMember.ContactPersonId = Convert.ToInt32(reader["ContactPersonId"]);
                    ordinaryMember.ContactPersonName = Convert.ToString(reader["ContactPersonName"]);
                    ordinaryMember.IntroducedById = Convert.ToInt32(reader["IntroduceById"]);
                    if (reader["IntroduceDate"] != DBNull.Value)
                    ordinaryMember.IntroduceDate = Convert.ToDateTime(reader["IntroduceDate"]);
                    ordinaryMember.IntroducedByName = Convert.ToString(reader["IntroduceByName"]);
                    ordinaryMember.FamilyMemberId = Convert.ToInt32(reader["FamilyId"]);
                    ordinaryMember.FamilyMemberName = Convert.ToString(reader["FamilyMemberName"]);
                    ordinaryMember.FamilyMemberCustId = Convert.ToString(reader["FamilyMemberCustId"]);
                    ordinaryMember.FamilyMemberBranchId = Convert.ToInt32(reader["FamilyMemberBranchId"]);
                    //ordinaryMember.FamilyMemberRoleId = (MemberRole)Enum.Parse(typeof(MemberRole), Convert.ToString(reader["FamilyMemberRoleId"]));
                    ordinaryMember.GroupId = Convert.ToInt32(reader["GroupId"]);
                    ordinaryMember.GroupName = Convert.ToString(reader["GroupName"]);
                    ordinaryMember.GroupRoleId = Convert.ToString(reader["GroupRoleId"]);
                    ordinaryMember.IsSendAdvertisement = Convert.ToBoolean(reader["SendAdvertisement"]);
                    ordinaryMember.Note = Convert.ToString(reader["Note"]);
                    ordinaryMember.BranchName = Convert.ToString(reader["BranchName"]);
                    ordinaryMember.BranchId = Convert.ToInt32(reader["BranchID"]);
                    if (reader["LastVisitDate"] != DBNull.Value)
                        ordinaryMember.LastVisitDate = Convert.ToDateTime(reader["LastVisitDate"]);
                    ordinaryMember.NoOfVisiteLast30 = Convert.ToInt32(reader["NoOfVisiteLast30"]);
                    ordinaryMember.NoOfVisiteLast365 = Convert.ToInt32(reader["NoOfVisiteLast365"]);
                    ordinaryMember.CtName = Convert.ToString(reader["ContractTemplateName"]);
                    ordinaryMember.CtNumber = Convert.ToString(reader["ContractTemplateNo"]);
                    if (reader["CTStartDate"] != DBNull.Value)
                    ordinaryMember.CtStartDate = Convert.ToDateTime(reader["CTStartDate"]);
                    if (reader["CTEndDate"] != DBNull.Value)
                    ordinaryMember.CtEndDate = Convert.ToDateTime(reader["CTEndDate"]);
                    if (reader["FreezeFromDate"] != DBNull.Value)
                    ordinaryMember.FreezeFromDate = Convert.ToDateTime(reader["FreezeFromDate"]);
                    if (reader["FreezeToDate"] != DBNull.Value)
                    ordinaryMember.FreezeToDate = Convert.ToDateTime(reader["FreezeToDate"]);
                    if (reader["ADCSignedDate"] != DBNull.Value)
                    ordinaryMember.AdcSignedDate = Convert.ToDateTime(reader["ADCSignedDate"]);
                    if (reader["LastFollowUpDate"] != DBNull.Value)
                        ordinaryMember.LastFollowUpDate = Convert.ToDateTime(reader["LastFollowUpDate"]);
                    ordinaryMember.NoOfFollowUpLast30 = Convert.ToInt32(reader["NoOfFollowUpLast30"]);
                    ordinaryMember.NoOfFollowUpLast365 = Convert.ToInt32(reader["NoOfFollowUpLast365"]);
                    if (reader["NextFollowUpDate"] != DBNull.Value)
                        ordinaryMember.NextFollowUpDate = Convert.ToDateTime(reader["NextFollowUpDate"]);
                    if (reader["ATGLastUpdated"] != DBNull.Value)
                        ordinaryMember.AtgLastUpdated = Convert.ToDateTime(reader["ATGLastUpdated"]);
                    ordinaryMember.IsFollowUp = Convert.ToBoolean(reader["IsFollowUp"]);
                    ordinaryMember.ChargeLimitAtg = Convert.ToDecimal(reader["AtgChargeLimit"]);
                    ordinaryMember.AtgKid = Convert.ToString(reader["AtgKid"]);
                    ordinaryMember.OldKid = Convert.ToString(reader["OldKid"]);
                    ordinaryMember.GuestCardNo = Convert.ToString(reader["GuestCardNo"]);
                    ordinaryMember.FamilyMmeberLstCount = Convert.ToInt32(reader["FMCount"]);
                    ordinaryMember.GroupMemberLstCount = Convert.ToInt32(reader["GMCount"]);
                    ordinaryMember.MemberShipPeriod = Convert.ToInt32(reader["MemberShipPeriod"]);
                    ordinaryMember.AccountNo = Convert.ToString(reader["AccountNumber"]);
                    ordinaryMember.GATCardNo = Convert.ToString(reader["GatCardNo"]);
                    ordinaryMember.CheckFingerPrint = Convert.ToBoolean(reader["CheckFingerPrint"]);
                    ordinaryMember.HaveOthergymContracts = Convert.ToBoolean(reader["HaveContractInOtherGym"]);
                    if (reader["LinkEmployeeId"] != DBNull.Value)
                        ordinaryMember.LinkedEmployeeId = Convert.ToInt32(reader["LinkEmployeeId"]);
                    if (reader["LinkEmployeeCustId"] != DBNull.Value)
                        ordinaryMember.LinkedEmployeeCustId = reader["LinkEmployeeCustId"].ToString();
                    if (reader["PinCode"] != DBNull.Value)
                    ordinaryMember.PinCode = Convert.ToInt32(reader["PinCode"]);

                    ordinaryMember.School = Convert.ToString(reader["School"]);
                    ordinaryMember.LastPaidSemesterFee = Convert.ToString(reader["LastPaidSemesterFee"]);
                    ordinaryMember.BasicContractId = Convert.ToInt32(reader["BasicContractId"]);
                    ordinaryMember.IsUsingGantner = Convert.ToBoolean(reader["IsUsingGantner"]);
                }

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                GetMembeATGStatusAction statusAction = new GetMembeATGStatusAction(_memberId);
                ordinaryMember.AtgStatusList = statusAction.Execute(EnumDatabase.Exceline, _gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ordinaryMember;
        }
    }
}
