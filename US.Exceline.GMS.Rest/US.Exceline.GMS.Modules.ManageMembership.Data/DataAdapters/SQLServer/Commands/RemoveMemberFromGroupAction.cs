﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class RemoveMemberFromGroupAction : USDBActionBase<bool>
    {
        private int _memberId = -1;
        private int _groupId = -1;
        private string _user = string.Empty;

        public RemoveMemberFromGroupAction(int memberId, int groupId, string user)
        {
            _memberId = memberId;
            _groupId = groupId;
            _user = user;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSManageMembershipRemoveMemberFromGroup";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberID", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@groupID", DbType.Int32, _groupId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
