﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateContractOrderAction : USDBActionBase<bool>
    {
        private List<InstallmentDC> _installmentList;
        private string _user = string.Empty;

        public UpdateContractOrderAction(List<InstallmentDC> installmentList, string user)
        {
            _installmentList = installmentList;
            _user = user;
        }
        protected override bool Body(DbConnection connection)
        {
            DbTransaction transaction = null;
            bool result = false;
            try
            {
                if (_installmentList != null)
                {
                    transaction = connection.BeginTransaction();

                    const string storedProcedureName = "USExceGMSManageMembershipUpdateContractOrder";
                    DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                    cmd.Transaction = transaction;

                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _installmentList[0].MemberId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentList", SqlDbType.Structured, GetDetailsTable()));

                    cmd.ExecuteNonQuery();
                    result = true;

                    transaction.Commit();
                }
            }
            catch
            {
                result = false;
                if (transaction != null) transaction.Rollback();
            }
            return result;
        }

        private DataTable GetDetailsTable()
        {
            try
            {
                var id = 1;

                var dataTable = new DataTable();
                dataTable.Columns.Add(new DataColumn("InstallmentID", typeof(int)));
                dataTable.Columns.Add(new DataColumn("dueDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("estimatedInvoiceDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("trainingPeriodStart", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("trainingPeriodEnd", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("text", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Priority", typeof(int)));

                foreach (var item in _installmentList)
                {
                    var dataRow = dataTable.NewRow();
                    dataRow["InstallmentID"] = item.Id;
                    dataRow["dueDate"] = item.DueDate;
                    dataRow["estimatedInvoiceDate"] = item.EstimatedInvoiceDate;
                    dataRow["trainingPeriodStart"] = item.TrainingPeriodStart;
                    dataRow["trainingPeriodEnd"] = item.TrainingPeriodEnd; ;
                    dataRow["text"] = item.Text;
                    dataRow["Priority"] = id;
                    dataTable.Rows.Add(dataRow);
                    id += 1;
                }

                return dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
    }
}
