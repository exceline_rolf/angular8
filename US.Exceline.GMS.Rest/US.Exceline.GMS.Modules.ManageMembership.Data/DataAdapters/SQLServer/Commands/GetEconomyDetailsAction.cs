﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetEconomyDetailsAction : USDBActionBase<OrdinaryMemberDC>
    {
        private int _memberId = -1;
        private string _gymCode = string.Empty;


        public GetEconomyDetailsAction(int memberId, string gymCode)
        {
            _memberId = memberId;
            _gymCode = gymCode;
        }
        protected override OrdinaryMemberDC Body(DbConnection connection)
        {
            OrdinaryMemberDC ordinaryMember = new OrdinaryMemberDC();
            const string storedProcedureName = "USExceGMSManageMembershipGetEconomyDetails";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ordinaryMember.Id = Convert.ToInt32(reader["MemberId"]);
                    ordinaryMember.BranchId = Convert.ToInt32(reader["BranchId"]);
                    ordinaryMember.AccountNo = Convert.ToString(reader["AccountNumber"]);

                    ordinaryMember.InvoiceChage = Convert.ToBoolean(reader["InvoiceCharge"]);
                    ordinaryMember.SmsInvoice = Convert.ToBoolean(reader["SmsInvoice"]);
                    ordinaryMember.GuardianId = Convert.ToInt32(reader["GuardianId"]);
                    ordinaryMember.GuardianBranchId = Convert.ToInt32(reader["GuardianBranchId"]);
                    ordinaryMember.GuardianCustId = Convert.ToString(reader["GuardianCustId"]);
                    ordinaryMember.GuardianFirstName = Convert.ToString(reader["GuardianFirstName"]);
                    ordinaryMember.GuardianLastName = Convert.ToString(reader["GuardianLastName"]);
                    ordinaryMember.GuardianAddress1 = Convert.ToString(reader["GuardianAddress"]);
                    ordinaryMember.GuardianAddress2 = Convert.ToString(reader["GuardianAddress2"]);
                    ordinaryMember.GuardianMobile = Convert.ToString(reader["GuardianSMS"]);
                    ordinaryMember.GuardianZipCode = Convert.ToString(reader["GuardianZipCode"]);
                    ordinaryMember.GuardianZipName = Convert.ToString(reader["GuardianZipName"]);
                    ordinaryMember.CreditBalance = Convert.ToDecimal(reader["CreditBalance"]);
                    ordinaryMember.EmployeeCategoryForSponser.Id = Convert.ToInt32(reader["SponserCategoryId"]);
                    ordinaryMember.DiscountCategoy = Convert.ToString(reader["DiscountCategoy"]);
                    ordinaryMember.EmployeeNo = Convert.ToString(reader["EmployeeNo"]);
                    ordinaryMember.Ref = Convert.ToString(reader["Ref"]);
                    ordinaryMember.ShopPchtoDate = Convert.ToDecimal(reader["ShopPchtoDate"]);
                    ordinaryMember.BilledLast365 = Convert.ToDecimal(reader["BilledLast365Dats"]);
                    ordinaryMember.ShoppchLast365 = Convert.ToDecimal(reader["ShoppchLast365"]);
                    ordinaryMember.OnAccountBalance = Convert.ToDecimal(reader["OnAccountBalance"]);
                    ordinaryMember.DueBalance = Convert.ToDecimal(reader["DueBalance"]);
                    ordinaryMember.DueDate = Convert.ToInt32(reader["DueDay"]);

                    if (reader["NextDueDate"] != DBNull.Value)
                        ordinaryMember.NextDueDate = Convert.ToDateTime(reader["NextDueDate"]);
                    if (reader["LastDueDate"] != DBNull.Value)
                        ordinaryMember.LastDueDate = Convert.ToDateTime(reader["LastDueDate"]);
                    ordinaryMember.BilledToDate = Convert.ToDecimal(reader["BilledToDate"]);
                    ordinaryMember.CreditPeriod = Convert.ToInt32(reader["CreditPeriod"]);
                    ordinaryMember.PrepaidBalance = Convert.ToDecimal(reader["Preapaidbalance"]);
                    if (reader["AgressoId"] != DBNull.Value)
                    ordinaryMember.AgressoId = Convert.ToInt32(reader["AgressoId"]);

                    SponsoredMemberDC sponsor = GetSponsoredDetails();
                    if (sponsor != null)
                    {
                        ordinaryMember.SponserId = sponsor.SponsorId;
                        ordinaryMember.SponserName = sponsor.SponsorName;
                        ordinaryMember.SponsoringId = sponsor.Id;
                        ordinaryMember.SponserRole = sponsor.Role;
                        ordinaryMember.SponsorBranchId = sponsor.SponsorBranchId;
                        if (sponsor.SponsorNo <= 0)
                        {
                            ordinaryMember.SponserCustId = string.Empty;
                        }
                        else
                        {
                            ordinaryMember.SponserCustId = sponsor.SponsorNo.ToString();
                        }
                        ordinaryMember.EmployeeCategoryName = sponsor.SponsoredLevelName;
                        ordinaryMember.EmployeeCategoryForSponser.Id = sponsor.SponsoredLevelId;
                        ordinaryMember.EmployeeCategoryForSponser.Name = sponsor.SponsoredLevelName;

                        if (sponsor.StartDate == DateTime.MinValue)
                        {
                            ordinaryMember.SponsorStartDate = DateTime.Now;
                        }
                        else
                        {
                            ordinaryMember.SponsorStartDate = sponsor.StartDate;
                        }

                        if (sponsor.EndDate == DateTime.MinValue)
                        {
                            ordinaryMember.SponsorEndDate = null;
                        }
                        else
                        {
                            ordinaryMember.SponsorEndDate = sponsor.EndDate;
                        }
                        ordinaryMember.IsStartDateEnable = false;
                    }
                    else
                    {
                        ordinaryMember.SponsorStartDate = DateTime.Now;
                        ordinaryMember.IsStartDateEnable = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ordinaryMember;
        }

        private SponsoredMemberDC GetSponsoredDetails()
        {
            SponsoredMemberDC sponsor = new SponsoredMemberDC();
            GetSponsorDetailsAction action = new GetSponsorDetailsAction(_memberId);
            sponsor = action.Execute(EnumDatabase.Exceline, _gymCode);
            return sponsor;
        }
    }
}
