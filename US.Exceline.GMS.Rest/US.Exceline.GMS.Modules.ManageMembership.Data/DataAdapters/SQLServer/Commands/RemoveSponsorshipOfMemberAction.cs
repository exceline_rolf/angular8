﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class RemoveSponsorshipOfMemberAction : USDBActionBase<bool>
    {
        private readonly int _sponsoredRecordId;
        private string _user = string.Empty;

        public RemoveSponsorshipOfMemberAction(int sponsoredRecordId, string user)
        {
            _sponsoredRecordId = sponsoredRecordId;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSManageMembershipRemoveSponsorshipOfMember";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponsoredRecordId", DbType.Int32, _sponsoredRecordId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
