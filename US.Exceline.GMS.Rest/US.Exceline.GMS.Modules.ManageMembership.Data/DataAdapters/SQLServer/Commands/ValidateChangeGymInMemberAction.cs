﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateChangeGymInMemberAction : USDBActionBase<int>
    {
        private readonly int _currentBranchID = -1;
        private readonly int _newBranchID = -1;
        private readonly int _branchID = -1;
        private readonly int _memberID = -1;
        private string _user = string.Empty;

        public ValidateChangeGymInMemberAction(int currentBranchID, int newBranchID, int branchID, int memberID, string user)
        {
            _currentBranchID = currentBranchID;
            _newBranchID = newBranchID;
            _branchID = branchID;
            _memberID = memberID;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSMemberValidateChangeGym";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CurrentBranchId", DbType.Int32, _currentBranchID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NewBranchId", DbType.Int32, _newBranchID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberID));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@outId";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);

                cmd.ExecuteNonQuery();
                if (output.Value != null)
                {
                    return Convert.ToInt32(output.Value);
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
