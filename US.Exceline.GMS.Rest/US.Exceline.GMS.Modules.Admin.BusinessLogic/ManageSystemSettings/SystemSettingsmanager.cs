﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/29/2012 16:15:31
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageSystemSettings;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageSystemSettings
{
    public class SystemSettingsManager
    {
        public static OperationResult<int> SaveBranchDetails(ExcelineBranchDC branch, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.SaveBranchDetails(branch, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Branch Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ValidateCreditorNo(int branchId, string gymCode, string creditorNo)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.ValidateCreditorNo(branchId, gymCode, creditorNo);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in validating creditorno" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineCreditorGroupDC>> GetBranchGroups(string user, string gymCode)
        {
            OperationResult<List<ExcelineCreditorGroupDC>> result = new OperationResult<List<ExcelineCreditorGroupDC>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetBranchGroups(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Branch Groups" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineBranchDC>> GetBranches(string user, string gymCode, int branchId)
        {
            OperationResult<List<ExcelineBranchDC>> result = new OperationResult<List<ExcelineBranchDC>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetBranches(user,gymCode,branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Branches" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> UpdateBranchDetails(ExcelineBranchDC branch, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.UpdateBranchDetails(branch, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Branch Upadting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> AddActivitySettings(ActivitySettingDC activitySetting, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();

            try
            {
                result.OperationReturnValue = SystemSettingsFacade.AddActivitySettings(activitySetting, gymCode, user);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving Activity Setting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ArticleDC>> GetInventoryList(int branchId, string gymCode)
        {
            OperationResult<List<ArticleDC>> result = new OperationResult<List<ArticleDC>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetInventoryList(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting inventory items" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        #region Gym Company Settings
        #region Get Gym Company Settings
        public static OperationResult<string> GetGymCompanySettings(string gymCode, GymCompanySettingType gymCompanySettingType)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                switch (gymCompanySettingType)
                {
                    case GymCompanySettingType.INFO:
                        SystemSettingBasicInfoDC SystemSettingBasicInfo = SystemSettingsFacade.GetSystemSettingBasicInfo(gymCode);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(SystemSettingBasicInfo);
                        break;
                    case GymCompanySettingType.VAT:
                        List<VatCodeDC> gymCompanyVatSettings = SystemSettingsFacade.GetVatCode(gymCode);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(gymCompanyVatSettings);
                        break;
                    case GymCompanySettingType.ECONOMY:
                        SystemSettingEconomySettingDC SystemSettingEconomySetting = SystemSettingsFacade.GetSystemSettingEconomySetting(gymCode);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(SystemSettingEconomySetting);
                        break;
                    case GymCompanySettingType.OTHER:
                        SystemSettingOtherSettingDC gymCompanyOtherSettings =  SystemSettingsFacade.GetOtherSettings(gymCode);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(gymCompanyOtherSettings);
                        break;
                    case GymCompanySettingType.CONTRACT:
                        List<ContractCondition> contractConditions = SystemSettingsFacade.GetSystemSettingContractConditions(gymCode);
                        result.OperationReturnValue = XMLUtils.SerializeDataContractObjectToXML(contractConditions);
                        break; 
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting GymCompanySetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        //---------------------------------------------
        public static OperationResult<SystemSettingBasicInfoDC> GetGymCompanyInfoSettings(string gymCode)
        {
            OperationResult<SystemSettingBasicInfoDC> result = new OperationResult<SystemSettingBasicInfoDC>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetSystemSettingBasicInfo(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting GymCompanySetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<VatCodeDC>> GetGymCompanyVATSettings(string gymCode)
        {
            OperationResult<List<VatCodeDC>> result = new OperationResult<List<VatCodeDC>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetVatCode(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting GymCompanySetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<SystemSettingEconomySettingDC> GetGymCompanyEconomySettings(string gymCode)
        {
            OperationResult<SystemSettingEconomySettingDC> result = new OperationResult<SystemSettingEconomySettingDC>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetSystemSettingEconomySetting(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting GymCompanySetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<SystemSettingOtherSettingDC> GetGymCompanyOtherSettings(string gymCode)
        {
            OperationResult<SystemSettingOtherSettingDC> result = new OperationResult<SystemSettingOtherSettingDC>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetOtherSettings(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting GymCompanySetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ContractCondition>> GetGymCompanyContractConditions(string gymCode)
        {
            OperationResult<List<ContractCondition>> result = new OperationResult<List<ContractCondition>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetSystemSettingContractConditions(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting GymCompanySetting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        #endregion

        #region SaveGymCompanySettings
        public static OperationResult<int> AddUpdateGymCompanySettings(string settingsDetails, string user, string gymCode, GymCompanySettingType gymCompanySettingType)
        {
            var result = new OperationResult<int>();
            try
            {
                switch (gymCompanySettingType)
                {
                    case GymCompanySettingType.VAT:
                        var vatCodeSettings = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(VatCodeDC)) as VatCodeDC;
                        result.OperationReturnValue = SystemSettingsFacade.AddUpdateVatCodes(vatCodeSettings, user, gymCode);
                        break;
                    case GymCompanySettingType.OTHER:
                        var otherSettings = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(SystemSettingOtherSettingDC)) as SystemSettingOtherSettingDC;
                        result.OperationReturnValue = SystemSettingsFacade.AddUpdateOtherSettings(otherSettings, user, gymCode);
                        break;
                    case GymCompanySettingType.INFO:
                        var SystemSettingBasicInfo = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(SystemSettingBasicInfoDC)) as SystemSettingBasicInfoDC;
                        result.OperationReturnValue = SystemSettingsFacade.AddUpdateSystemSettingBasicInfo(SystemSettingBasicInfo, user, gymCode);
                        break;
                    case GymCompanySettingType.ECONOMY:
                        var SystemSettingEconomySetting = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(SystemSettingEconomySettingDC)) as SystemSettingEconomySettingDC;
                        result.OperationReturnValue = SystemSettingsFacade.AddUpdateSystemSettingEconomySetting(SystemSettingEconomySetting, user, gymCode);
                        break;
                    case GymCompanySettingType.CONTRACT:
                        var contractCondition = XMLUtils.DesrializeXMLToObject(settingsDetails, typeof(ContractCondition)) as ContractCondition;
                        result.OperationReturnValue = SystemSettingsFacade.AddUpdateSystemSettingContractCondition(contractCondition, user, gymCode);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving Gym Company Settings" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveGymCompanyBasicSettings(SystemSettingBasicInfoDC gymCompanyBasicSettings, string user, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.AddUpdateSystemSettingBasicInfo(gymCompanyBasicSettings, user, gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving Gym Company Basic Settings" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<int> SaveGymCompanyOtherSettings(SystemSettingOtherSettingDC otherSettings, string user, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.AddUpdateOtherSettings(otherSettings, user, gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving Gym Company Other Settings" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveGymCompanyContractSettings(ContractCondition contractSettings, string user, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.AddUpdateSystemSettingContractCondition(contractSettings, user, gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving Gym Company Other Settings" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveGymCompanyVATSettings(VatCodeDC vatCodeDetail, string user, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.AddUpdateVatCodes(vatCodeDetail, user, gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving Gym Company VAT Settings" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveGymCompanyEconomySettings(SystemSettingEconomySettingDC SystemSettingEconomySetting, string user, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.AddUpdateSystemSettingEconomySetting(SystemSettingEconomySetting, user, gymCode);
            }

            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving Gym Company Economy Settings" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        #endregion

        #region GetClassKeyword
        public static OperationResult<List<string>> GetClassKeyword(string gymCode)
        {
            OperationResult<List<string>> result = new OperationResult<List<string>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetClassKeyword(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Get Class Keyword Settings" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        } 
        #endregion
        #endregion

        public static OperationResult<List<AccountDC>> GetRevenueAccounts(string gymCode)
        {
            OperationResult<List<AccountDC>> result = new OperationResult<List<AccountDC>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetRevenueAccounts(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Get Revenue Accounts" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> AddUpdateRevenueAccounts(AccountDC revenueAccountDetail, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.AddUpdateRevenueAccounts(revenueAccountDetail, user, gymCode);
            } 
            catch (Exception ex)
            {
                result.CreateMessage("Error in Revenue Account Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> DeleteRevenueAccounts(AccountDC revenueAccountDetail, string user, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.DeleteRevenueAccounts(revenueAccountDetail, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Deleting Revenue Account" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<decimal> SaveStock(ArticleDC article,int branchId, string gymCode, string user)
        {
            OperationResult<decimal> result = new OperationResult<decimal>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.SaveStock(article, branchId,gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving stock" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> DeleteArticle(int articleId, int branchId, string gymCode, string user, bool isAdminUser)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.DeleteArticle(articleId, branchId, gymCode, user, isAdminUser);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in deleting article" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ActivityTimeDC>> GetActivityTimes(int branchId, bool isUnavailableTimes, string gymCode)
        {
            OperationResult<List<ActivityTimeDC>> result = new OperationResult<List<ActivityTimeDC>>();
            try
            {
                if(isUnavailableTimes)
                    result.OperationReturnValue = SystemSettingsFacade.GetActivityUnavailableTimes(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting activity times" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> AddActivityTimes(List<ActivityTimeDC> activityTimes, string gymCode, bool isUnavailableTimes)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                if (isUnavailableTimes)
                    result.OperationReturnValue = SystemSettingsFacade.AddActivityUnavailableTimes(activityTimes, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in adding activity times" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<int> SaveInventory(InventoryItemDC inventoryItem, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.SaveInventory(inventoryItem, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving Inventory" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }



        public static OperationResult<List<InventoryItemDC>> GetInventory(int branchId, string gymCode)
        {
            OperationResult<List<InventoryItemDC>> result = new OperationResult<List<InventoryItemDC>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetInventory(branchId,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting InventoryItem Groups" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> DeleteInventory(int inventoryId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.DeleteInventory(inventoryId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Deleting Inventory" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ArticleDC>> GetInventoryDetail(int branchId, int inventoryId, string gymCode)
        {
            OperationResult<List<ArticleDC>> result = new OperationResult<List<ArticleDC>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetInventoryDetail(branchId,inventoryId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting InventoryList" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<decimal> SaveInventoryDetail(List<ArticleDC> articleList, int inventoryId, string gymCode)
        {
            OperationResult<decimal> result = new OperationResult<decimal>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.SaveInventoryDetail(articleList,inventoryId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving InventoryDetail" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<Dictionary<decimal, decimal>> GetNetValueForArticle(int inventoryId, int articleId, int counted, int branchId, string gymCode)
        {
            OperationResult<Dictionary<decimal, decimal>> result = new OperationResult<Dictionary<decimal, decimal>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetNetValueForArticle(inventoryId, articleId,counted,branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting NetValue for Article" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> AddFollowUpTemplate(FollowUpTemplateDC followUpTemplate, string gymCode, int branchId)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.AddFollowUpTemplate(followUpTemplate, gymCode, branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erro in Adding Adding FollowUp template" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<FollowUpTemplateDC>> GetFollowUpTemplates(string gymCode, int branchId)
        {
            OperationResult<List<FollowUpTemplateDC>> result = new OperationResult<List<FollowUpTemplateDC>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetFollowUpTemplateList(gymCode, branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erro in getting followup Templates" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteFollowUpTemplate(int followUpTemplateId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.DeleteFollowUpTemplate(followUpTemplateId,gymCode) ;
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erro in deleting followup Templates" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<FollowUpTemplateTaskDC>> GetFollowUpTaskByTemplateId(int followUpTemplateId, string gymCode)
        {
            OperationResult<List<FollowUpTemplateTaskDC>> result = new OperationResult<List<FollowUpTemplateTaskDC>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetTaskByFollowUpTemplateId(followUpTemplateId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erro in Getting  followup tasks" + ex.Message, MessageTypes.ERROR);
            }
            return result;    
        }

        public static OperationResult<CategoryDC> GetCategoryByCode(string categoryCode, string gymCode)
        {
            var result = new OperationResult<CategoryDC>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetCategoryByCode(categoryCode, gymCode);
            }
            catch (Exception ex)
            {
               result.CreateMessage("Erro in Getting Category" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static List<ExcACCAccessControl> GetExcAccessControlList(int branchId, string gymCode)
        {
            try
            {
                return SystemSettingsFacade.GetExcAccessControlList(branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ExceACCMember GetLastAccMember(string gymCode, int branchId, int accTerminalId)
        {
            try
            {
                return SystemSettingsFacade.GetLastAccMember(gymCode, branchId, accTerminalId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OperationResult<List<ExceArxFormatDetail>> GetArxSettingDetail(int branchId, string gymCode)
        {
            var result = new OperationResult<List<ExceArxFormatDetail>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetArxSettingDetail(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erro in Arx Setting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<List<ExceArxFormatType>> GetArxFormatType( string gymCode)
        {
            var result = new OperationResult<List<ExceArxFormatType>>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.GetArxFormatType(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erro in Arx format type" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static List<ContractTemplateDC> ValidateContractConditionWithTemplate(int contractConditionId, string gymCode)
        {
            try
            {
                return SystemSettingsFacade.ValidateContractConditionWithTemplate(contractConditionId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int ValidateContractConditionWithContracts(int contractConditionId, string gymCode)
        {
            try
            {
                return SystemSettingsFacade.ValidateContractConditionWithContracts(contractConditionId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static OperationResult<int> SaveArxSettingDetail(ExceArxFormatDetail arxFormatDetail, int tempCodeDeleteDate, int branchId, string user, string gymCode, string accessContolTypes)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemSettingsFacade.SaveArxSettingDetail(arxFormatDetail, tempCodeDeleteDate, branchId, user, gymCode, accessContolTypes);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Erro in Arx Setting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

    }
}
