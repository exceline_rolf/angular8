﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin.HolydayManagement;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageHolidays;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageHolidays
{
    public class HolidaysManager
    {
        public static OperationResult<List<HolidayScheduleDC>> GetHolidays(int branchId, string holidayType, DateTime startDate, DateTime endDate, string gymCode)
        {
            OperationResult<List<HolidayScheduleDC>> result = new OperationResult<List<HolidayScheduleDC>>();
            try
            {
                result.OperationReturnValue = HolidayFacade.GetHolidays(branchId, holidayType, startDate, endDate,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Holidays" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> AddNewHoliday(HolidayScheduleDC newHoliday, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = HolidayFacade.AddNewHoliday(newHoliday,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Adding new Holiday Schedule" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> GetHolidayCategoryId(string categoryName, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = HolidayFacade.GetHolidayCategoryId(categoryName,  gymCode);
            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in Getting Category Id" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateHolidayStatus(HolidayScheduleDC updateHoliday, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = HolidayFacade.UpdateHolidayStatus(updateHoliday,  gymCode);
            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in Updating Holiday Status" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

    }
}
