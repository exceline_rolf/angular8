﻿using System.Collections.Generic;
using System.ServiceModel;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.Login;
using US.Common.Web.UI.Core.SystemObjects;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Login.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IExcelineLoginService
    {
        [OperationContract]
        string ValidateUser(string userName, string password);

        [OperationContract]
        USPUser ValidateUserWithCard(string cardNumber, string user);

        [OperationContract]
        List<ExceUserBranchDC> GetUserSelectedBranches(string userName, string user);

        [OperationContract]
        int SaveNotification(NotificationDC notification, string createdUser, int branchId);

        [OperationContract]
        bool EditUSPUserBasicInfo(bool isPasswdChange, ExecUserDC uspUser, string loggeduser);

        [OperationContract]
        List<ExecUserDC> GetLoggedUserDetails(string userName, string key);

        [OperationContract]
        string GetGymSettings(int branchId, string user, GymSettingType gymSettingType);

        [OperationContract]
        void DummyMethodForPassDomainObject(ExceGymSettingDC gymSetting);

        [OperationContract]
        bool AddEditHardwareProfileId(int userId, int hardwareId, string user);

        [OperationContract]
        string GetSesstionValue(string user, string sessionKey);

        [OperationContract]
        int SendNewPassword(string employeeName, string template);

        [OperationContract]
        List<string> GetVersionNoList(string userName);

        [OperationContract]
        bool SaveUserCulture(string culture, string userName);

        [OperationContract]
        bool UpdateSettingForUserRoutine(string key, string value, string user);

        [OperationContract]
        int GetLoginGymByUser(string user);
    }
}
