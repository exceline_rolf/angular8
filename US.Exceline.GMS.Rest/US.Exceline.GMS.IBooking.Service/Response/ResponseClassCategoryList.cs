﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseClassCategoryList
    {
        public ResponseClassCategoryList() { }
        public ResponseClassCategoryList(List<ExceIBookingClassCategory> categories, ResponseStatus status)
        {
            ClassCategories = categories;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingClassCategory> ClassCategories { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}