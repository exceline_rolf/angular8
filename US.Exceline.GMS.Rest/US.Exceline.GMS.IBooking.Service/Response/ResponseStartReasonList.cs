﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseStartReasonList
    {
        public ResponseStartReasonList() { }
        public ResponseStartReasonList(List<ExceIBookingStartReason> startReasonList, ResponseStatus status)
        {
            StartReasons = startReasonList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingStartReason> StartReasons { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}