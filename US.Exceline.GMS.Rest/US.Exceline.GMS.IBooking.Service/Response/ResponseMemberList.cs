﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseMemberList
    {
        public ResponseMemberList() { }
        public ResponseMemberList(List<ExceIBookingMember> memberList, ResponseStatus status)
        {
            Members = memberList;
            Status = status;
        }

        [DataMember(Order=1)]
        public List<ExceIBookingMember> Members { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}