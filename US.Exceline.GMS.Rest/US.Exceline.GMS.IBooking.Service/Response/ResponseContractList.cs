﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseContractList
    {
        public ResponseContractList() { }
        public ResponseContractList(List<ExceIBookingContractFreeze> contractList, ResponseStatus status)
        {
            Contracts = contractList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingContractFreeze> Contracts { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}