﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseInterestList
    {
        public ResponseInterestList() { }
        public ResponseInterestList(List<ExceIBookingInterest> interestList, ResponseStatus status)
        {
            Interests = interestList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingInterest> Interests { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}