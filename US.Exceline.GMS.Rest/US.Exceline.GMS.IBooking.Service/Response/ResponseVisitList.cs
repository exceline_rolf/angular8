﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseVisitList
    {
        public ResponseVisitList() { }
        public ResponseVisitList(List<ExceIBookingVisitCount> VisitList, ResponseStatus status)
        {
            Visit = VisitList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingVisitCount> Visit { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}