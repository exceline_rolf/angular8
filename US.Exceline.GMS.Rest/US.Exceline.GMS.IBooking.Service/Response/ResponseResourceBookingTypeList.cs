﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseResourceBookingTypeList
    {
        public ResponseResourceBookingTypeList() { }
        public ResponseResourceBookingTypeList(List<ExceIBookingResourceBookingType> resourceBookingTypeList, ResponseStatus status)
        {
            ResourceBookingTypeList = resourceBookingTypeList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingResourceBookingType> ResourceBookingTypeList { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}