﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseResourceCalendarList
    {
        public ResponseResourceCalendarList() { }
        public ResponseResourceCalendarList(List<ExceIBookingResourceCalendar> resourceCalendarList, ResponseStatus status)
        {
            ResourceCalendarList = resourceCalendarList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingResourceCalendar> ResourceCalendarList { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}