﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseFreezeResignCategoryList
    {
        public ResponseFreezeResignCategoryList() { }
        public ResponseFreezeResignCategoryList(List<ExceIBookingFreezeResignCategories> categories, ResponseStatus status)
        {
            FreezeResignCategories = categories;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingFreezeResignCategories> FreezeResignCategories { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}