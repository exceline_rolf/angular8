﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseExcelineShowUpList
    {
        public ResponseExcelineShowUpList() { }
        public ResponseExcelineShowUpList(List<ExceIBookingShowUp> showUpList, ResponseStatus status)
        {
            ShowUps = showUpList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingShowUp> ShowUps { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}