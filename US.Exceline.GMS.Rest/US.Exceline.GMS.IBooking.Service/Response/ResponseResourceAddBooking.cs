﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    public class ResponseResourceAddBooking
    {
        public ResponseResourceAddBooking() { }
        public ResponseResourceAddBooking(int bookingId, ResponseStatus status)
        {
            BookingId = bookingId;
            Status = status;
        }

        [DataMember(Order = 1)]
         public int BookingId { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}