﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.API;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseAPIMemberList
    {
        public ResponseAPIMemberList() { }
        public ResponseAPIMemberList(List<ExceAPIMember> memberList, ResponseStatus status)
        {
            Members = memberList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceAPIMember> Members { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}