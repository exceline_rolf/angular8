﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponsePaymentList
    {
        public ResponsePaymentList() { }
        public ResponsePaymentList(List<ExceIBookingPayment> branchList, ResponseStatus status)
        {
            Payments = branchList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingPayment> Payments { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}