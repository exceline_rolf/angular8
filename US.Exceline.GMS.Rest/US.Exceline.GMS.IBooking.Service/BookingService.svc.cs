﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Activation;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Communication.API;
using US.Communication.Core.SystemCore.DomainObjects;
using US.Exceline.GMS.IBooking.API;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.IBooking;
using US.Exceline.GMS.IBooking.Service.Response;
using US.Common.Logging.API;
using US.GMS.Core.DomainObjects.Common;
using US.Common.Notification.Core.Enums;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.API;
using System.Globalization;
using US.Exceline.GMS.IBooking.Core;
using US.Exceline.GMS.IBooking.Service.BRIS;
using IO.Swagger.Model;
using US.GMS.Core.DomainObjects.API;

namespace US.Exceline.GMS.IBooking.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BookingService : IBookingService
    {
        //#region GetMemberData
        ///// <summary>
        ///// GetMemberData
        ///// </summary>
        ///// <param name="systemId">company id</param>
        ///// <param name="branchId">branch id</param>
        ///// <returns></returns>
        public ResponseMemberList GetMemberDataChanges(string systemID, string gymId, string changedSinceDays, string apiKey)
        {
            ResponseMemberList responseMemberList;
            try
            {                

                if (AuthenticationManager.Authenticate("GetMemberDataChanges", apiKey, systemID, gymId))
                {

                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemID).OperationReturnValue;
                    OperationResult<List<ExceIBookingMember>> list = GMSExcelineIBooking.GetIBookingMembers(gymCode, Convert.ToInt32(gymId), Convert.ToInt32(changedSinceDays), Convert.ToInt32(systemID));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseMemberList = new ResponseMemberList(null, status);
                        return responseMemberList;
                    }
                    else
                    {
                        var memberList = list.OperationReturnValue;
                        if (memberList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseMemberList = new ResponseMemberList(memberList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Members Found");
                            responseMemberList = new ResponseMemberList(null, status);
                        }
                        return responseMemberList;
                    }

                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseMemberList = new ResponseMemberList(null, accessstatus);
                    return responseMemberList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseMemberList = new ResponseMemberList(null, status);
                return responseMemberList;
            }
        }

        public ResponseMemberList GetMemberData(string systemID, string gymId, string apiKey)
        {
            ResponseMemberList responseMemberList;
            try
            {
                if (AuthenticationManager.Authenticate("GetMemberData", apiKey, systemID, gymId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemID).OperationReturnValue;
                    OperationResult<List<ExceIBookingMember>> list = GMSExcelineIBooking.GetIBookingMembers(gymCode, Convert.ToInt32(gymId), Convert.ToInt32(systemID));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseMemberList = new ResponseMemberList(null, status);
                        return responseMemberList;
                    }
                    else
                    {
                        var memberList = list.OperationReturnValue;
                        if (memberList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseMemberList = new ResponseMemberList(memberList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Members Found");
                            responseMemberList = new ResponseMemberList(null, status);
                        }
                        return responseMemberList;
                    }

                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseMemberList = new ResponseMemberList(null, accessstatus);
                    return responseMemberList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseMemberList = new ResponseMemberList(null, status);
                return responseMemberList;
            }
        }

        //#endregion

        //#region GetMemberById
        ///// <summary>
        ///// GetMemberData
        ///// </summary>
        ///// <param name="systemId">company id</param>
        ///// <param name="branchId">branch id</param>
        ///// <returns></returns>
        public ResponseMemberList GetMemberById(string systemID, string gymId, string customerNo, string apiKey)
        {
            ResponseMemberList responseMemberList;
            try
            {
                if (AuthenticationManager.Authenticate("GetMemberById", apiKey, systemID, gymId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemID).OperationReturnValue;
                    OperationResult<List<ExceIBookingMember>> list = GMSExcelineIBooking.GetIBookingMemberById(gymCode, Convert.ToInt32(gymId), customerNo, Convert.ToInt32(systemID));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseMemberList = new ResponseMemberList(null, status);
                        return responseMemberList;
                    }
                    else
                    {
                        var memberList = list.OperationReturnValue;
                        if (memberList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseMemberList = new ResponseMemberList(memberList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Members Found");
                            responseMemberList = new ResponseMemberList(null, status);
                        }
                        return responseMemberList;
                    }

                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseMemberList = new ResponseMemberList(null, accessstatus);
                    return responseMemberList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseMemberList = new ResponseMemberList(null, status);
                return responseMemberList;
            }
        }
        //#endregion

        //#region GetCompanyData
        ///// <summary>
        ///// Get Company Details
        ///// </summary>
        ///// <param name="CompanyId">company id</param>
        ///// <returns></returns>
        public ResponseSystemDetails GetSystemData(string systemId, string apiKey)
        {
            ResponseSystemDetails responseSystemDetails;
            try
            {
                if (AuthenticationManager.Authenticate("GetSystemData", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<ExceIBookingSystem> systemData = GMSExcelineIBooking.GetIBookingSystemDetails(gymCode, Convert.ToInt32(systemId));
                    if (systemData.ErrorOccured)
                    {
                        foreach (NotificationMessage message in systemData.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", systemData.Notifications.FirstOrDefault().Message);
                        responseSystemDetails = new ResponseSystemDetails(null, status);
                        return responseSystemDetails;
                    }
                    else
                    {
                        var company = systemData.OperationReturnValue;
                        if (company != null && !String.IsNullOrEmpty(company.SystemName))
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseSystemDetails = new ResponseSystemDetails(company, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "System Not Found");
                            responseSystemDetails = new ResponseSystemDetails(company, status);
                        }
                        return responseSystemDetails;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseSystemDetails = new ResponseSystemDetails(null, accessstatus);
                    return responseSystemDetails;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseSystemDetails = new ResponseSystemDetails(null, status);
                return responseSystemDetails;
            }
        }
        //#endregion

        //#region GetBranches
        ///// <summary>
        ///// Get All Branch Details
        ///// </summary>
        ///// <param name="CompanyId">company id</param>
        ///// <returns></returns>
        public ResponseBranchList GetGyms(string systemId, string apiKey)
        {
            string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
            ResponseBranchList responseBranchList;
            try
            {
                if (AuthenticationManager.Authenticate("GetGyms", apiKey, systemId))
                {
                    OperationResult<List<ExceIBookingGym>> list = GMSExcelineIBooking.GetIBookingGyms(gymCode);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseBranchList = new ResponseBranchList(null, status);
                        return responseBranchList;
                    }
                    else
                    {
                        var branchList = list.OperationReturnValue;
                        if (branchList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseBranchList = new ResponseBranchList(branchList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Branches Found");
                            responseBranchList = new ResponseBranchList(null, status);
                        }
                        return responseBranchList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseBranchList = new ResponseBranchList(null, accessstatus);
                    return responseBranchList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseBranchList = new ResponseBranchList(null, status);
                return responseBranchList;
            }
        }
        //#endregion

        //#region GetWebOfferings
        ///// <summary>
        ///// Get All WebOfferings
        ///// </summary>
        ///// <param name="CompanyId">company id</param>
        ///// <returns></returns>
        public ResponseWebOfferingList GetWebOfferings(string systemId, string apiKey)
        {
            string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
            ResponseWebOfferingList responseWebOfferingList;
            try
            {
                if (AuthenticationManager.Authenticate("GetWebOfferings", apiKey, systemId))
                {
                    OperationResult<List<ExceIBookingWebOffering>> list = GMSExcelineIBooking.GetIBookingWebOfferings(gymCode);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseWebOfferingList = new ResponseWebOfferingList(null, status);
                        return responseWebOfferingList;
                    }
                    else
                    {
                        var webOfferingList = list.OperationReturnValue;
                        if (webOfferingList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseWebOfferingList = new ResponseWebOfferingList(webOfferingList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No WebOfferings Found");
                            responseWebOfferingList = new ResponseWebOfferingList(null, status);
                        }
                        return responseWebOfferingList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseWebOfferingList = new ResponseWebOfferingList(null, accessstatus);
                    return responseWebOfferingList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseWebOfferingList = new ResponseWebOfferingList(null, status);
                return responseWebOfferingList;
            }
        }
        //#endregion

        //#region GetInstructors
        ///// <summary>
        ///// Get All Instructors Details
        ///// </summary>
        ///// <returns></returns>
        public ResponseInstructorList GetInstructors(string systemId, string apiKey)
        {
            ResponseInstructorList responseInstructorsList;
            try
            {
                if (AuthenticationManager.Authenticate("GetInstructors", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingInstructor>> list = GMSExcelineIBooking.GetIBookingInstructors(gymCode);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseInstructorsList = new ResponseInstructorList(null, status);
                        return responseInstructorsList;
                    }
                    else
                    {
                        var instructorList = list.OperationReturnValue;
                        if (instructorList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseInstructorsList = new ResponseInstructorList(instructorList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Instructors Found");
                            responseInstructorsList = new ResponseInstructorList(null, status);
                        }
                        return responseInstructorsList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseInstructorsList = new ResponseInstructorList(null, accessstatus);
                    return responseInstructorsList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseInstructorsList = new ResponseInstructorList(null, status);
                return responseInstructorsList;
            }
        }
        //#endregion

        //#region GetClassCategories
        ///// <summary>
        ///// Get Class Categories
        ///// </summary>
        ///// <param name="systemId">systemId</param>
        ///// <param name="branchId">branch id</param>
        ///// <returns></returns>
        public ResponseClassCategoryList GetClassCategories(string systemId, string apiKey)
        {
            ResponseClassCategoryList responseClassCategoryList;
            try
            {
                if (AuthenticationManager.Authenticate("GetClassCategories", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingClassCategory>> list = GMSExcelineIBooking.GetIBookingClassCategories(gymCode);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseClassCategoryList = new ResponseClassCategoryList(null, status);
                        return responseClassCategoryList;
                    }
                    else
                    {
                        var classList = list.OperationReturnValue;
                        if (classList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseClassCategoryList = new ResponseClassCategoryList(classList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Classes Found");
                            responseClassCategoryList = new ResponseClassCategoryList(null, status);
                        }
                        return responseClassCategoryList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseClassCategoryList = new ResponseClassCategoryList(null, accessstatus);
                    return responseClassCategoryList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseClassCategoryList = new ResponseClassCategoryList(null, status);
                return responseClassCategoryList;
            }
        }
        //#endregion

        //#region GetClassKeywords
        ///// <summary>
        ///// Get Class Keywords
        ///// </summary>
        ///// <param name="systemId">systemId</param>
        ///// <param name="branchId">branch id</param>
        ///// <returns></returns>
        public ResponseClassKeywordList GetClassKeywords(string systemId, string apiKey)
        {
            ResponseClassKeywordList responseClassKeywordList;
            try
            {
                if (AuthenticationManager.Authenticate("GetClassKeywords", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingClassKeyword>> list = GMSExcelineIBooking.GetIBookingClassKeyword(gymCode);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseClassKeywordList = new ResponseClassKeywordList(null, status);
                        return responseClassKeywordList;
                    }
                    else
                    {
                        var classKeywordList = list.OperationReturnValue;
                        if (classKeywordList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseClassKeywordList = new ResponseClassKeywordList(classKeywordList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Class Keywords Found");
                            responseClassKeywordList = new ResponseClassKeywordList(null, status);
                        }
                        return responseClassKeywordList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseClassKeywordList = new ResponseClassKeywordList(null, accessstatus);
                    return responseClassKeywordList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseClassKeywordList = new ResponseClassKeywordList(null, status);
                return responseClassKeywordList;
            }
        }
        //#endregion

        //#region GetClassLevels
        ///// <summary>
        ///// Get Class Levels
        ///// </summary>
        ///// <returns></returns>
        public ResponseClassLevelList GetClassLevels(string systemId, string apiKey)
        {
            ResponseClassLevelList responseClassLevelList;
            try
            {
                if (AuthenticationManager.Authenticate("GetClassLevels", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<CategoryDC>> list = GMSExcelineIBooking.GetCategoriesByType("CLASSLEVEL", gymCode);
                    if (list.ErrorOccured)
                    {

                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseClassLevelList = new ResponseClassLevelList(null, status);
                        return responseClassLevelList;
                    }
                    else
                    {
                        var classLevelCategoryList = list.OperationReturnValue;
                        List<ExceIBookingClassLevel> classLevelList = new List<ExceIBookingClassLevel>();
                        if (classLevelCategoryList.Count > 0)
                        {
                            foreach (CategoryDC cat in classLevelCategoryList)
                            {
                                ExceIBookingClassLevel classLevel = new ExceIBookingClassLevel();
                                classLevel.ClasslevelId = cat.Id;
                                classLevel.ClasslevelName = cat.Name;
                                classLevelList.Add(classLevel);
                            }
                            var status = new ResponseStatus("1", "Successfull");
                            responseClassLevelList = new ResponseClassLevelList(classLevelList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Class Levels Found");
                            responseClassLevelList = new ResponseClassLevelList(null, status);
                        }
                        return responseClassLevelList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseClassLevelList = new ResponseClassLevelList(null, accessstatus);
                    return responseClassLevelList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseClassLevelList = new ResponseClassLevelList(null, status);
                return responseClassLevelList;
            }
        }
        //#endregion

        //#region GetStartReasons
        ///// <summary>
        ///// Get StartReasons
        ///// </summary>
        ///// <param name="CompanyId">company id</param>
        ///// <param name="BranchId">branch id</param>
        ///// <returns></returns>
        public ResponseStartReasonList GetStartReasons(string systemId, string apiKey)
        {
            ResponseStartReasonList responseStartReasonsList;
            try
            {
                if (AuthenticationManager.Authenticate("GetStartReasons", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingStartReason>> list = GMSExcelineIBooking.GetIBookingStartReasons(gymCode);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseStartReasonsList = new ResponseStartReasonList(null, status);
                        return responseStartReasonsList;
                    }
                    else
                    {
                        var startReasonList = list.OperationReturnValue;
                        if (startReasonList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseStartReasonsList = new ResponseStartReasonList(startReasonList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Start Reasons Found");
                            responseStartReasonsList = new ResponseStartReasonList(null, status);
                        }
                        return responseStartReasonsList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseStartReasonsList = new ResponseStartReasonList(null, accessstatus);
                    return responseStartReasonsList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseStartReasonsList = new ResponseStartReasonList(null, status);
                return responseStartReasonsList;
            }
        }
        //#endregion

        //#region GetClassTypes
        ///// <summary>
        ///// Get Class Types
        ///// </summary>
        ///// <returns></returns>

        public ResponseClassTypeList GetClassTypes(string systemId, string apiKey)
        {
            ResponseClassTypeList responseClassTypeList;
            try
            {
                if (AuthenticationManager.Authenticate("GetClassTypes", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingClassType>> list = GMSExcelineIBooking.GetIBookingClassTypes(gymCode);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseClassTypeList = new ResponseClassTypeList(null, status);
                        return responseClassTypeList;
                    }
                    else
                    {
                        var classList = list.OperationReturnValue;
                        if (classList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseClassTypeList = new ResponseClassTypeList(classList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Classes Found");
                            responseClassTypeList = new ResponseClassTypeList(null, status);
                        }
                        return responseClassTypeList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseClassTypeList = new ResponseClassTypeList(null, accessstatus);
                    return responseClassTypeList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseClassTypeList = new ResponseClassTypeList(null, status);
                return responseClassTypeList;
            }
        }
        //#endregion

        //#region GetClassCalendar
        ///// <summary>
        ///// Get ClassCalendar
        ///// </summary>
        ///// <param name="CompanyId">company id</param>
        ///// <returns></returns>
        public ResponseClassScheduleCalenderList GetClassCalendar(string systemId, string gymId, string fromDate, string toDate, string apiKey)
        {
            ResponseClassScheduleCalenderList responseClassScheduleList;
            try
            {
                if (AuthenticationManager.Authenticate("GetClassCalendar", apiKey, systemId, gymId))
                {
                    DateTime fromDt = Convert.ToDateTime(fromDate);
                    DateTime toDt = Convert.ToDateTime(toDate);
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingClassCalendar>> list = GMSExcelineIBooking.GetIBookingClassSchedules(gymCode, Convert.ToInt32(gymId), fromDt, toDt);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseClassScheduleList = new ResponseClassScheduleCalenderList(null, status);
                        return responseClassScheduleList;
                    }
                    else
                    {
                        var scheduleList = list.OperationReturnValue;
                        if (scheduleList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseClassScheduleList = new ResponseClassScheduleCalenderList(scheduleList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Class Schedules Found");
                            responseClassScheduleList = new ResponseClassScheduleCalenderList(null, status);
                        }
                        return responseClassScheduleList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseClassScheduleList = new ResponseClassScheduleCalenderList(null, accessstatus);
                    return responseClassScheduleList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseClassScheduleList = new ResponseClassScheduleCalenderList(null, status);
                return responseClassScheduleList;
            }
        }
        //#endregion

        //#region GetExcelingeShowUps
        ///// <summary>
        ///// Get ExcelineShowUps
        ///// </summary>
        ///// <param name="CompanyId">company id</param>
        ///// <param name="BranchId">branch id</param>
        ///// <param name="FromDate">from Date</param>
        ///// <param name="TopDate">to Date</param>
        ///// <returns></returns>
        public ResponseExcelineShowUpList GetExcelingeShowUps(string systemId, string gymId, string fromDate, string toDate, string apiKey)
        {
            ResponseExcelineShowUpList responseExcelineShowUpList;
            try
            {
                if (AuthenticationManager.Authenticate("GetExcelingeShowUps", apiKey, systemId, gymId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingShowUp>> list = GMSExcelineIBooking.GetIBookingExcelineShowUps(gymCode, Convert.ToInt32(gymId), Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseExcelineShowUpList = new ResponseExcelineShowUpList(null, status);
                        return responseExcelineShowUpList;
                    }
                    else
                    {
                        var showUpList = list.OperationReturnValue;
                        if (showUpList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseExcelineShowUpList = new ResponseExcelineShowUpList(showUpList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No ShowUps Found");
                            responseExcelineShowUpList = new ResponseExcelineShowUpList(null, status);
                        }
                        return responseExcelineShowUpList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseExcelineShowUpList = new ResponseExcelineShowUpList(null, accessstatus);
                    return responseExcelineShowUpList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseExcelineShowUpList = new ResponseExcelineShowUpList(null, status);
                return responseExcelineShowUpList;
            }
        }
        //#endregion

        //#region GetPayments
        ///// <summary>
        ///// Get Payments
        ///// </summary>
        ///// <param name="CompanyId">company id</param>
        ///// <param name="BranchId">branch id</param>
        ///// <param name="FromDate">from Date</param>
        ///// <param name="TopDate"
        public ResponsePaymentList GetPayments(string systemId, string gymId, string fromDate, string toDate, string apiKey)
        {
            ResponsePaymentList responsePaymentList;
            try
            {
                if (AuthenticationManager.Authenticate("GetPayments", apiKey, systemId, gymId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingPayment>> list = GMSExcelineIBooking.GetIBookingPayments(gymCode, Convert.ToInt32(gymId), Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responsePaymentList = new ResponsePaymentList(null, status);
                        return responsePaymentList;
                    }
                    else
                    {
                        var paymentList = list.OperationReturnValue;
                        if (paymentList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responsePaymentList = new ResponsePaymentList(paymentList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Payments Found");
                            responsePaymentList = new ResponsePaymentList(null, status);
                        }
                        return responsePaymentList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responsePaymentList = new ResponsePaymentList(null, accessstatus);
                    return responsePaymentList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responsePaymentList = new ResponsePaymentList(null, status);
                return responsePaymentList;
            }
        }
        //#endregion

        //#region RegisterNewMember
        ///// <summary>
        ///// Register New member
        ///// </summary>
        ///// <param name="bookingDetails">booking details</param>
        ///// <returns></returns>
        public ResponseStatus RegisterNewMember(ExceIBookingNewMember NewMember, string apiKey2)
        {
            int savedMemberID = -1;
            int guardianID = -1;
            int custId = -1;
            int guardianCustID = -1;
            int memberContractID = -1;
            string cardNo = string.Empty;
            string gymCode = string.Empty;
            bool briIntegrated = false;
            string apiKey = string.Empty;
            int welcomeType = 1; // 1-SMS, 2- Email, 3- Both
            try
            {
                if (AuthenticationManager.Authenticate("RegisterNewMember", apiKey2, NewMember.SystemId, NewMember.GymId))
                {
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(Convert.ToString(NewMember.SystemId)).OperationReturnValue;
                    string saveStatusDetails = GMSExcelineIBooking.RegisterNewIBookingMember(gymCode, NewMember, "System").OperationReturnValue;
                    decimal paidAmount = Convert.ToDecimal(NewMember.PaidAmount, new CultureInfo("en-US"));
                    string[] statusParts = saveStatusDetails.Split(new char[] { '/' });
                    string saveStatus = statusParts[0];
                    if (statusParts.Length > 1)
                        savedMemberID = Convert.ToInt32(statusParts[1]);
                    if (statusParts.Length > 2)
                        custId = Convert.ToInt32(statusParts[2]);
                    if (statusParts.Length > 3)
                        cardNo = statusParts[3];

                    if (statusParts.Length > 4)
                        guardianID = Convert.ToInt32(statusParts[4]);

                    if (statusParts.Length > 5 && !string.IsNullOrEmpty(statusParts[5]))
                        guardianCustID = Convert.ToInt32(statusParts[5]);

                    if (statusParts.Length > 6)
                        memberContractID = Convert.ToInt32(statusParts[6]);

                    if (saveStatus.Equals("SUCCESS"))
                    {
                        var status = new ResponseStatus("1", "Successfull") { CustomerId = custId, GuardianCustomerId = guardianCustID };

                        //Send the SMS
                        if (!string.IsNullOrEmpty(NewMember.OfferingId) && NewMember.OfferingId != "-1")
                        {
                            try
                            {
                                welcomeType = Convert.ToInt32(ConfigurationManager.AppSettings["WelComeType"].ToString());

                                ExceIBookingNotificationDetails notificationDetails = GMSExcelineIBooking.GetTemplateDetails(gymCode, NewMember.OfferingId, Convert.ToInt32(NewMember.GymId), "TEMP");

                                // create parameter string for notification
                                Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                                paraList.Add(TemplateFieldEnum.FIRSTNAME, NewMember.FirstName);
                                paraList.Add(TemplateFieldEnum.ATGLIMIT, notificationDetails.ATGAmount.ToString());
                                paraList.Add(TemplateFieldEnum.GYMNAME, notificationDetails.GymName);
                                paraList.Add(TemplateFieldEnum.TEMPCODE, cardNo);

                                if (welcomeType == 2)
                                    SendWelComeEmail(TextTemplateEnum.WELCOMEEMAIL, paraList, savedMemberID, Convert.ToInt32(NewMember.GymId), gymCode);
                                else if (welcomeType == 1)
                                    SendSMS(TextTemplateEnum.WELCOMESMS, paraList, savedMemberID, Convert.ToInt32(NewMember.GymId), gymCode);
                                else if (welcomeType == 3)
                                {
                                    SendSMS(TextTemplateEnum.WELCOMESMS, paraList, savedMemberID, Convert.ToInt32(NewMember.GymId), gymCode);
                                    SendWelComeEmail(TextTemplateEnum.WELCOMEEMAIL, paraList, savedMemberID, Convert.ToInt32(NewMember.GymId), gymCode);
                                }

                                //Invoice First Order
                                List<InstallmentDC> todayOrders = GMSExcelineIBooking.GetTodayOrder(gymCode, savedMemberID, memberContractID);
                                foreach (InstallmentDC order in todayOrders)
                                {
                                    try
                                    {
                                        PaymentDetailDC paymentDetails = new PaymentDetailDC();
                                        paymentDetails.InvoiceAmount = order.Amount;
                                        OperationResult<SaleResultDC> newInvoice = GMSPayment.GenerateInvoice(Convert.ToInt32(NewMember.GymId), gymCode, order, paymentDetails, "SMS", gymCode, Convert.ToInt32(NewMember.GymId));

                                        if (paidAmount > 0)
                                        {
                                            PayModeDC paymentMode = new PayModeDC();
                                            paymentMode.Amount = paidAmount;
                                            paymentMode.PaidDate = DateTime.Now;
                                            paymentMode.PaymentType = "BANKSTATEMENT";
                                            paymentMode.PaymentTypeCode = "BANKSTATEMENT";
                                            paymentMode.PaymentTypeId = -1;

                                            ExcelineInvoiceDetailDC InvoiceDetails = GetInvoiceDetails(newInvoice.OperationReturnValue.AritemNo, Convert.ToInt32(NewMember.GymId), gymCode);
                                            decimal amount = InvoiceDetails.InvoiceAmount;
                                            paymentDetails.Creditorno = InvoiceDetails.CreditorNo;
                                            paymentDetails.CustId = InvoiceDetails.CustId;
                                            paymentDetails.Kid = InvoiceDetails.BasicDetails.KID;
                                            paymentDetails.PaidAmount = paidAmount;
                                            paymentDetails.PaidMemberId = (guardianID > 0) ? guardianID : savedMemberID;
                                            paymentDetails.PaidUser = "IBooking";
                                            paymentDetails.PaymentSource = PaymentTypes.INVOICE;
                                            paymentDetails.PaymentSourceId = newInvoice.OperationReturnValue.AritemNo.ToString();
                                            paymentDetails.PayModes = new List<PayModeDC>() { paymentMode };
                                            paymentDetails.Ref = InvoiceDetails.InvoiceNo;
                                            paymentDetails.SalepointId = -1;

                                            OperationResult<SaleResultDC> result = GMSPayment.RegisterInvoicePayment(paymentDetails, gymCode, Convert.ToInt32(NewMember.GymId), null, "Ibooking");
                                            if (result.ErrorOccured)
                                            {
                                                GMSExcelineIBooking.AddStatus(NewMember, 0, "Error in Adding payment " + result.Notifications[0].Message, gymCode);
                                            }
                                            paidAmount = paidAmount - amount;
                                        }
                                        else
                                        {
                                            ExcelineInvoiceDetailDC InvoiceDetails = GetInvoiceDetails(newInvoice.OperationReturnValue.AritemNo, Convert.ToInt32(NewMember.GymId), gymCode);
                                            if (InvoiceDetails != null)
                                            {
                                                if (InvoiceDetails.InvoiceAmount > 0)
                                                {
                                                    // create parameter string for notification
                                                    Dictionary<TemplateFieldEnum, string> paraList1 = new Dictionary<TemplateFieldEnum, string>();
                                                    paraList1.Add(TemplateFieldEnum.INVOICENO, InvoiceDetails.InvoiceNo.ToString());
                                                    paraList1.Add(TemplateFieldEnum.DUEDATE, InvoiceDetails.InvoiceDueDate.Value.ToShortDateString());
                                                    paraList1.Add(TemplateFieldEnum.AMOUNT, InvoiceDetails.InvoiceAmount.ToString());
                                                    paraList1.Add(TemplateFieldEnum.KID, InvoiceDetails.BasicDetails.KID.ToString());
                                                    paraList1.Add(TemplateFieldEnum.GYMACCOUNT, InvoiceDetails.OtherDetails.BranchAccountNo);
                                                    paraList1.Add(TemplateFieldEnum.GYMNAME, InvoiceDetails.OtherDetails.BranchName.ToString());
                                                    paraList1.Add(TemplateFieldEnum.CREATEDUSER, gymCode);
                                                    paraList1.Add(TemplateFieldEnum.DATE, DateTime.Now.ToShortDateString());
                                                    SendSMS(TextTemplateEnum.SMSINVOICE, paraList1, (guardianID > 0) ? guardianID : savedMemberID, Convert.ToInt32(NewMember.GymId), gymCode);

                                                    SendEmail(paraList1, (guardianID > 0) ? guardianID : savedMemberID, InvoiceDetails.GymID, InvoiceDetails.ArItemNO, gymCode);
                                                }
                                            }

                                        }
                                        //Get the latest oRder and invoice 
                                    }
                                    catch (Exception ex)
                                    {
                                        GMSExcelineIBooking.AddStatus(NewMember, 0, "Invoice Error " + ex.Message, gymCode);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                GMSExcelineIBooking.AddStatus(NewMember, 0, "Invoice generation Error " + ex.Message, gymCode);
                                USLogError.WriteToFile("Error : ", ex, gymCode);
                            }

                            //upload to arx server 
                            try
                            {
                                OperationResult<ARXSetting> arxsetting = GMSExcelineIBooking.GetARXSettings(Convert.ToInt32(NewMember.SystemId));
                                string fileLocation = ConfigurationManager.AppSettings["ARX_FIle_Location"].ToString();
                                if (arxsetting != null)
                                {
                                    GMSExcelineIBooking.UploadARXData(savedMemberID, memberContractID, gymCode, Convert.ToInt32(NewMember.SystemId), fileLocation, custId.ToString(), arxsetting.OperationReturnValue);
                                }
                            }
                            catch (Exception ex)
                            {
                                GMSExcelineIBooking.AddStatus(NewMember, 0, "Error in ARX upload " + ex.Message, gymCode);
                            }
                        }

                        GMSExcelineIBooking.AddStatus(NewMember, 1, "SUCCESS", gymCode);

                        // Handle bris Integration 
                        briIntegrated = ConfigurationManager.AppSettings["BrisIntegration"].ToString() == "1" ? true : false;
                        if (briIntegrated)
                        {
                            try
                            {
                                BrisIntegrationManager brisManager = new BrisIntegrationManager();
                                apiKey = ConfigurationManager.AppSettings["Bris_Integration_ApiKey"].ToString();
                                if (savedMemberID > 0 && NewMember.BrisID <= 0)
                                {
                                    bool briStatus = brisManager.SaveBrisMember(apiKey, NewMember, savedMemberID, gymCode);
                                }
                                else
                                {
                                    //get the bris Data 
                                    BrukerDto brisMember = brisManager.SearchMembers(NewMember.BrisID, apiKey);
                                    if (brisMember != null)
                                    {
                                        if (!(brisMember.AgressoId > 0))
                                        {
                                            //get the agresso ID
                                            brisMember.AgressoId = brisManager.GetAggressoID(NewMember.BrisID, apiKey);
                                        }
                                        //update member data 
                                        brisManager.UpdateBrisData(NewMember, brisMember, savedMemberID, gymCode);
                                    }
                                    else
                                    {
                                        GMSExcelineIBooking.AddStatus(NewMember, 1, "BRIS Error member not found for brisID :" + NewMember.BrisID.ToString(), gymCode);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                GMSExcelineIBooking.AddStatus(NewMember, 1, "BRIS Error :" + ex.Message, gymCode);
                            }
                        }
                        return status;
                    }
                    else if (saveStatus.Equals("NOOFFER"))
                    {
                        GMSExcelineIBooking.AddStatus(NewMember, 0, "NOOFFER", gymCode);
                        var status = new ResponseStatus("2", "Offering is not found") { CustomerId = custId, GuardianCustomerId = guardianCustID };
                        return status;
                       
                    }
                    else if (saveStatus.Equals("DUPLICATECONTRACTS"))
                    {
                        GMSExcelineIBooking.AddStatus(NewMember, 0, "DUPLICATECONTRACTS", gymCode);
                        var status = new ResponseStatus("7", "Two contracts cannot be created for the basic activity") { CustomerId = custId, GuardianCustomerId = guardianCustID };
                        return status;

                    }
                    else if (saveStatus.Equals("RETURNSHOP"))
                    {
                        GMSExcelineIBooking.AddStatus(NewMember, 0, "RETURNSHOP", gymCode);
                        var status = new ResponseStatus("8", "Please return the shop account balance since gym is going to be changed") { CustomerId = custId, GuardianCustomerId = guardianCustID };
                        return status;
                    }
                    else if (saveStatus.Equals("RETURNPREPAID"))
                    {
                        GMSExcelineIBooking.AddStatus(NewMember, 0, "RETURNPREPAID", gymCode);
                        var status = new ResponseStatus("9", "Please return the prepaid account balance since gym is going to be changed") { CustomerId = custId, GuardianCustomerId = guardianCustID };
                        return status;
                    }
                    else if (saveStatus.Equals("PAYTHESHOP"))
                    {
                        GMSExcelineIBooking.AddStatus(NewMember, 0, "PAYTHESHOP", gymCode);
                        var status = new ResponseStatus("10", "Please pay the minus balance of shop account since gym is going to be changed") { CustomerId = custId, GuardianCustomerId = guardianCustID };
                        return status;
                    }
                    else if (saveStatus.Equals("CONTRACTSAVEERROR"))
                    {
                        GMSExcelineIBooking.AddStatus(NewMember, 0, "CONTRACTSAVEERROR", gymCode);
                        var status = new ResponseStatus("2", "Error in contract Save") { CustomerId = custId, GuardianCustomerId = guardianCustID };
                        return status;
                    }
                    else if (saveStatus.Equals("CONTRACTNOTCREATED"))
                    {
                        GMSExcelineIBooking.AddStatus(NewMember, 0, "CONTRACTNOTCREATED", gymCode);
                        var status = new ResponseStatus("2", "Error in contract creation") { CustomerId = custId, GuardianCustomerId = guardianCustID };
                        return status;
                    }
                    else if (saveStatus.Equals("MEMBERNOTSAVED"))
                    {
                        GMSExcelineIBooking.AddStatus(NewMember, 0, "MEMBERSAVEERROR", gymCode);
                        var status = new ResponseStatus("2", "Error in member registration") { CustomerId = custId, GuardianCustomerId = guardianCustID };
                        return status;
                    }
                    else if (saveStatus.Equals("CONTRACTGENERATIONERROR"))
                    {
                        GMSExcelineIBooking.AddStatus(NewMember, 0, "CONTRACTGENERATIONERROR", gymCode);
                        var status = new ResponseStatus("2", "Error in contract generation") { CustomerId = custId, GuardianCustomerId = guardianCustID };
                        return status;
                    }
                    else
                    {
                        GMSExcelineIBooking.AddStatus(NewMember, 0, "Error in registration : " + saveStatus, gymCode);
                        var status = new ResponseStatus("2", "Error in registration : " + saveStatus) { CustomerId = custId, GuardianCustomerId = guardianCustID };
                        return status;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ") { CustomerId = custId, GuardianCustomerId = guardianCustID };
                    return accessstatus;
                }
            }
            catch (Exception ex)
            {
                GMSExcelineIBooking.AddStatus(NewMember, 0, ex.Message, gymCode);
                var status = new ResponseStatus("5", "Error in registration") { CustomerId = custId, GuardianCustomerId = guardianCustID };
                return status;
            }
        }

        public ExcelineInvoiceDetailDC GetInvoiceDetails(int invoiceID, int branchId, string user)
        {
            var result = GMSInvoice.GetInvoiceDetails(invoiceID, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }

                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        private void SendWelComeEmail(TextTemplateEnum type, Dictionary<TemplateFieldEnum, string> paraList, int memberId, int branchId, string gymCode)
        {
            // create common notification
            USCommonNotificationDC commonNotification = new USCommonNotificationDC();
            commonNotification.IsTemplateNotification = true;
            commonNotification.Type = type;
            commonNotification.Method = NotificationMethodType.EMAIL;
            commonNotification.ParameterString = paraList;
            commonNotification.BranchID = branchId;
            commonNotification.CreatedUser = gymCode;
            commonNotification.Role = "MEM";
            commonNotification.NotificationType = NotificationTypeEnum.Messages;
            commonNotification.Severity = NotificationSeverityEnum.Minor;
            commonNotification.MemberID = memberId;
            commonNotification.Status = NotificationStatusEnum.New;
            commonNotification.Title = "Velkommen som medlem";

            USImportResult<int> EMAILresult = NotificationAPI.AddNotification(commonNotification);

            if (EMAILresult.ErrorOccured)
            {
                foreach (USNotificationMessage message in EMAILresult.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                    }
                }
            }
            else
            {
                string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
                string outputFormat = "EMAIL";

                try
                {
                    // send EMAIL through the USC API
                    IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, gymCode, EMAILresult.MethodReturnValue.ToString());

                    NotificationStatusEnum status = NotificationStatusEnum.Attended;

                    if (emailStatus != null)
                        status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                    var statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                    // update status in notification table
                    USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(EMAILresult.MethodReturnValue, gymCode, status, statusMessage);

                    if (updateResult.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in updateResult.NotificationMessages)
                        {
                            if (message.ErrorLevel == ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    USLogError.WriteToFile(e.Message, new Exception(), "IBooking");
                }
            }
        }

        private void SendSMS(TextTemplateEnum type, Dictionary<TemplateFieldEnum, string> paraList, int memberId, int branchId, string gymCode)
        {
            // create common notification
            USCommonNotificationDC commonNotification = new USCommonNotificationDC();
            commonNotification.IsTemplateNotification = true;
            commonNotification.Type = type;
            commonNotification.Method = NotificationMethodType.SMS;
            commonNotification.ParameterString = paraList;
            commonNotification.BranchID = branchId;
            commonNotification.CreatedUser = gymCode;
            commonNotification.Role = "MEM";
            commonNotification.NotificationType = NotificationTypeEnum.Messages;
            commonNotification.Severity = NotificationSeverityEnum.Minor;
            commonNotification.MemberID = memberId;
            commonNotification.Status = NotificationStatusEnum.New;
            commonNotification.Title = "SMS Notification";

            //add SMS notification into notification tables
            USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), gymCode);
                    }
                }
            }
            else
            {
                string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                string outputFormat = "SMS";

                try
                {
                    // send SMS through the USC API
                    IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, gymCode, result.MethodReturnValue.ToString());

                    NotificationStatusEnum status = NotificationStatusEnum.Attended;

                    if (smsStatus != null)
                        status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;

                    var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                    // update status in notification table
                    USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, gymCode, status, statusMessage);

                    if (updateResult.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in updateResult.NotificationMessages)
                        {
                            if (message.ErrorLevel == ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), gymCode);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    USLogError.WriteToFile(e.Message, new Exception(), gymCode);
                }
            }
        }

        private void SendEmail(Dictionary<TemplateFieldEnum, string> paraList, int memberId, int branchId, int arItemNo, string gymCode)
        {
            PDFPrintResultDC pdfResult = new PDFPrintResultDC();
            var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"];
            var dataDictionary = new Dictionary<string, string>() { { "itemID", arItemNo.ToString() }, { "gymCode", gymCode } };
            pdfResult.FileParth = GetFilePathForPdf(appUrl, "PDF", gymCode, dataDictionary);


            // create common notification
            USCommonNotificationDC commonNotification = new USCommonNotificationDC();
            commonNotification.IsTemplateNotification = true;
            commonNotification.Type = TextTemplateEnum.EMAILINVOICE;
            commonNotification.Method = NotificationMethodType.EMAIL;
            commonNotification.ParameterString = paraList;
            commonNotification.BranchID = branchId;
            commonNotification.CreatedUser = gymCode;
            commonNotification.Role = "MEM";
            commonNotification.NotificationType = NotificationTypeEnum.Messages;
            commonNotification.Severity = NotificationSeverityEnum.Minor;
            commonNotification.MemberID = memberId;
            commonNotification.Status = NotificationStatusEnum.New;
            commonNotification.AttachmentFileParth = pdfResult.FileParth;
            commonNotification.Title = "E-postfaktura";

            USImportResult<int> EMAILresult = NotificationAPI.AddNotification(commonNotification);

            if (EMAILresult.ErrorOccured)
            {
                foreach (USNotificationMessage message in EMAILresult.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                    }
                }
                pdfResult.ErrorStateId = -4;
            }
            else
            {
                string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
                string outputFormat = "EMAIL";

                try
                {
                    // send EMAIL through the USC API
                    IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, gymCode, EMAILresult.MethodReturnValue.ToString());

                    NotificationStatusEnum status = NotificationStatusEnum.Attended;

                    if (emailStatus != null)
                        status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                    var statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                    // update status in notification table
                    USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(EMAILresult.MethodReturnValue, gymCode, status, statusMessage);

                    if (updateResult.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in updateResult.NotificationMessages)
                        {
                            if (message.ErrorLevel == ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                            }
                        }
                        pdfResult.ErrorStateId = -4;
                    }
                }
                catch (Exception e)
                {
                    USLogError.WriteToFile(e.Message, new Exception(), "IBooking");
                    pdfResult.ErrorStateId = -4;
                }
            }
        }

        private string GetFilePathForPdf(string application, string outputPlugging, string gymCode, Dictionary<string, string> dataDictionary)
        {
            var filePath = string.Empty;
            try
            {
                var list = ExcecuteWithUSC(application, outputPlugging, dataDictionary, gymCode, "IBooking");
                foreach (var item in list)
                {
                    if (item == null) continue;
                    var message = item.Messages.First(mssge => mssge.Header.ToLower().Equals("filepath"));
                    if (message != null)
                    {
                        filePath = message.Message;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), "IBooking");
            }
            return filePath;
        }

        private List<IProcessingStatus> ExcecuteWithUSC(string application, string outputPlugging, Dictionary<string, string> dataDictionary, string gymCode, string user)
        {
            List<IProcessingStatus> lists = null;
            try
            {
                lists = TemplateManager.ExecuteTemplate(dataDictionary, application, outputPlugging, gymCode);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                throw ex;
            }
            return lists;
        }

        //#endregion

        //#region UpdateClass
        ///// <summary>
        ///// Update class
        ///// </summary>
        ///// <param name="exceClass">exceClass</param>
        ///// <returns></returns>
        public ResponseStatus UpdateClass(ExceIBookingUpdateClass exceClass, string apiKey)
        {
            try
            {
                if (AuthenticationManager.Authenticate("UpdateClass", apiKey, exceClass.SystemId, exceClass.GymId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(Convert.ToString(exceClass.SystemId)).OperationReturnValue;
                    bool validClassId = GMSExcelineIBooking.GetIBookingEntityValid(Convert.ToInt32(exceClass.ClassId), "CLS", Convert.ToInt32(exceClass.GymId), gymCode).OperationReturnValue;

                    if (validClassId)
                    {
                        int saveStatus = GMSExcelineIBooking.UpdateIBookingClass(gymCode, exceClass).OperationReturnValue;
                        if (saveStatus > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            return status;
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "Error Updating Class");
                            return status;
                        }
                    }

                    else
                    {
                        string message = (!validClassId ? "Invalid Class Id" : "");
                        var status = new ResponseStatus("4", message);
                        return status;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }
        }
        //#endregion


        //#region CancelContract
        ///// <summary>
        ///// Cancel Contract
        ///// </summary>
        ///// <param name="CancelContract">CancelContract</param>
        ///// <returns></returns>
        ///// 
        public ResponseStatus CancelIBookingContract(string systemId, string gymId, string customerId, string apiKey)
        {
            try
            {
                if (AuthenticationManager.Authenticate("CancelIBookingContract", apiKey, systemId, gymId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(Convert.ToString(systemId)).OperationReturnValue;
                    bool validMemContractId = GMSExcelineIBooking.GetIBookingEntityValid(Convert.ToInt32(customerId), "MEM", Convert.ToInt32(gymId), gymCode).OperationReturnValue;

                    if (validMemContractId)
                    {
                        ContractResignDetails resignDetails = GMSExcelineIBooking.CancelIBookingContract(Convert.ToInt32(systemId), Convert.ToInt32(gymId), Convert.ToInt32(customerId), gymCode).OperationReturnValue;
                        if (resignDetails != null)
                        {
                            var status = new ResponseStatus("1", "Successfull") { ExpirationDate = resignDetails.ContractEndDate.Value.ToString("yyyy.MM.dd") };
                            PrintResignDocument(resignDetails, "IBooking", Convert.ToInt32(gymId));
                            return status;
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "Error  Cancelling Contract");
                            return status;
                        }
                    }
                    else
                    {
                        string message = (!validMemContractId ? "Invalid Member Contract Id" : "");
                        var status = new ResponseStatus("4", message);
                        return status;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }
        }

        private void PrintResignDocument(ContractResignDetails resignDetails, string user, int branchId)
        {
            try
            {
                var memnberContractID = resignDetails.ID.ToString();
                var contractEndDate = resignDetails.ContractEndDate.Value.ToString("dd.MM.yyyy");
                var numberofOrder = "0";
                var totalOrderAmount = "0";
                var lastDueDate = "";

                numberofOrder = resignDetails.OrderCount.ToString();
                totalOrderAmount = resignDetails.OrderAmount.ToString("N");
                lastDueDate = resignDetails.LastDueDate.Value.ToString("dd.MM.yyyy");

                var appUrl = ConfigurationManager.AppSettings["USC_Resign_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "MemberContractID", memnberContractID }, { "ContractEndDate", contractEndDate }, { "NumberOfOrder", numberofOrder }, { "TotalOrderAmount", totalOrderAmount }, { "LastDueDate", lastDueDate }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                var pdfFileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(resignDetails.ID, pdfFileParth, "RESG", branchId, user);
            }
            catch (Exception) { }
        }

        private void SetPDFFileParth(int id, string fileParth, string flag, int branchId, string user)
        {
            GMSExcelineIBooking.SetPDFFileParth(id, fileParth, flag, branchId, ExceConnectionManager.GetGymCode(user), user);
        }
        //#endregion

        //#region AddClassBooking
        ///// <summary>
        ///// Add Class Booking
        ///// </summary>
        ///// <param name="AddClassBooking">AddClassBooking</param>
        ///// <returns></returns>
        ///// 
        public ResponseStatus AddIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string apiKey)
        {
            try
            {
                if (AuthenticationManager.Authenticate("AddIBookingClassBooking", apiKey, Convert.ToString(systemId), Convert.ToString(gymId)))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(Convert.ToString(systemId)).OperationReturnValue;
                    bool validMemContractId = GMSExcelineIBooking.GetIBookingEntityValid(Convert.ToInt32(customerId), "MEM", Convert.ToInt32(gymId), gymCode).OperationReturnValue;

                    if (validMemContractId)
                    {
                        int addStatus = GMSExcelineIBooking.AddIBookingClassBooking(systemId, gymId, classId, customerId, gymCode).OperationReturnValue;
                        if (addStatus > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            return status;
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "Error Adding ClassBooking");
                            return status;
                        }
                    }

                    else
                    {
                        string message = (!validMemContractId ? "Invalid Member Id" : "");
                        var status = new ResponseStatus("4", message);
                        return status;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }
        }
        //#endregion

        //#region DeleteClassBooking
        ///// <summary>
        ///// Add Class Booking
        ///// </summary>
        ///// <param name="AddClassBooking">AddClassBooking</param>
        ///// <returns></returns>
        ///// 
        public ResponseStatus DeleteIBookingClassBooking(int systemId, int gymId, int classId, int customerId, string apiKey)
        {
            try
            {
                if (AuthenticationManager.Authenticate("DeleteIBookingClassBooking", apiKey, Convert.ToString(systemId), Convert.ToString(gymId)))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(Convert.ToString(systemId)).OperationReturnValue;
                    bool validMemClassId = GMSExcelineIBooking.GetIBookingEntityValid(Convert.ToInt32(classId), "MEM", Convert.ToInt32(gymId), gymCode).OperationReturnValue;

                    if (validMemClassId)
                    {
                        int deleteStatus = GMSExcelineIBooking.DeleteIBookingClassBooking(systemId, gymId, classId, customerId, gymCode).OperationReturnValue;
                        if (deleteStatus > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            return status;
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "Error Deleting ClassBooking");
                            return status;
                        }
                    }

                    else
                    {
                        string message = (!validMemClassId ? "Invalid Member Class Id" : "");
                        var status = new ResponseStatus("4", message);
                        return status;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }
        }
        //#endregion

        //#region UpdateMemberClassVisit
        ///// <summary>
        ///// Update Member Class Visit
        ///// </summary>
        ///// <param name="visit">visit</param>
        ///// <returns></returns>
        public ResponseStatus UpdateMemberClassVisit(ExceIBookingMemberClassVisit visit, string apiKey)
        {
            try
            {
                if (AuthenticationManager.Authenticate("UpdateMemberClassVisit", apiKey, visit.SystemId, visit.GymId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(Convert.ToString(visit.SystemId)).OperationReturnValue;
                    bool validCustomerId = GMSExcelineIBooking.GetIBookingEntityValid(Convert.ToInt32(visit.CustomerId), "MEM", Convert.ToInt32(visit.GymId), gymCode).OperationReturnValue;
                    bool validClassId = GMSExcelineIBooking.GetIBookingEntityValid(Convert.ToInt32(visit.ClassId), "CLS", Convert.ToInt32(visit.GymId), gymCode).OperationReturnValue;

                    if (validCustomerId && validClassId)
                    {
                        int saveStatus = GMSExcelineIBooking.UpdateIBookingMemberClassVisit(gymCode, visit).OperationReturnValue;
                        if (saveStatus > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            return status;
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "Error Saving Booking Details");
                            return status;
                        }
                    }
                    else
                    {
                        string message = (!validCustomerId) ? "Invalid Customer Id" : (!validClassId ? "Invalid Class Id" : "");
                        var status = new ResponseStatus("4", message);
                        return status;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }
        }
        //#endregion

        //#region UpdateMemberVisit
        ///// <summary>
        ///// Update Member Visit
        ///// </summary>
        ///// <param name="visit">visit</param>
        ///// <returns></returns>
        public ResponseStatus UpdateMemberVisit(ExceIBookingMemberVisit visit, string apiKey)
        {
            try
            {
                if (AuthenticationManager.Authenticate("UpdateMemberVisit", apiKey, visit.SystemId, visit.GymId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(Convert.ToString(visit.SystemId)).OperationReturnValue;
                    bool validCustomerId = GMSExcelineIBooking.GetIBookingEntityValid(Convert.ToInt32(visit.CustomerNo), "MEM", Convert.ToInt32(visit.GymId), gymCode).OperationReturnValue;
                    if (validCustomerId)
                    {
                        int saveStatus = GMSExcelineIBooking.UpdateIBookingMemberVisit(gymCode, visit).OperationReturnValue;
                        if (saveStatus > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            return status;
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "Error Saving visit Details");
                            return status;
                        }
                    }
                    else
                    {
                        string message = "Invalid Customer No";
                        var status = new ResponseStatus("4", message);
                        return status;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }
        }
        //#endregion

        //#region UpdateMember
        ///// <summary>
        ///// Update IBooking Member
        ///// </summary>
        ///// <param name="UpdatedMember">member</param>
        ///// <returns></returns>
        public ResponseStatus UpdateMember(ExceIBookingUpdateMember UpdatedMember, string apiKey)
        {
            try
            {
                if (AuthenticationManager.Authenticate("UpdateMember", apiKey, UpdatedMember.SystemId, UpdatedMember.GymId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(Convert.ToString(UpdatedMember.SystemId)).OperationReturnValue;
                    if (!string.IsNullOrEmpty(UpdatedMember.CustomerNo))
                    {
                        int saveStatus = GMSExcelineIBooking.UpdateIBookingMember(gymCode, UpdatedMember).OperationReturnValue;
                        if (saveStatus == -2)
                        {
                            string message = "Invalid Customer No";
                            var status = new ResponseStatus("4", message);
                            return status;
                        }
                        else if (saveStatus > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            return status;
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "Error on Updating Member");
                            return status;
                        }
                    }
                    else
                    {
                        string message = "Invalid Customer No";
                        var status = new ResponseStatus("4", message);
                        return status;

                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }
        }
        //#endregion

        //#region GetResourceCalendar
        ///// <summary>
        ///// Get Resource Calendar
        ///// </summary>
        ///// <param name="CompanyId">company id</param>
        ///// <param name="BranchId">branch id</param>
        ///// <param name="FromDate">from Date</param>
        ///// <param name="TopDate">to Date</param>
        ///// <returns></returns>
        public ResponseResourceCalendarList GetResourceCalendar(string systemId, string branchId, string fromDate, string toDate, string apiKey)
        {
            ResponseResourceCalendarList responseResourceCalendarList;
            try
            {
                if (AuthenticationManager.Authenticate("GetResourceCalendar", apiKey, systemId, branchId))
                {

                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                OperationResult<List<ExceIBookingResourceCalendar>> list = GMSExcelineIBooking.GetResourceCalendar(gymCode, Convert.ToInt32(branchId), Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate));
                if (list.ErrorOccured)
                {
                    foreach (NotificationMessage message in list.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                    }
                    var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                    responseResourceCalendarList = new ResponseResourceCalendarList(null, status);
                    return responseResourceCalendarList;
                }
                else
                {
                    var resourceCalendarList = list.OperationReturnValue;
                    if (resourceCalendarList.Count > 0)
                    {
                        var status = new ResponseStatus("1", "Successfull");
                        responseResourceCalendarList = new ResponseResourceCalendarList(resourceCalendarList, status);
                    }
                    else
                    {
                        var status = new ResponseStatus("2", "No Resource Calendar Found");
                        responseResourceCalendarList = new ResponseResourceCalendarList(null, status);
                    }
                    return responseResourceCalendarList;
                }
            }

                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseResourceCalendarList = new ResponseResourceCalendarList(null, accessstatus);
                    return responseResourceCalendarList;
                }

            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseResourceCalendarList = new ResponseResourceCalendarList(null, status);
                return responseResourceCalendarList;
            }
        }
        //#endregion

        //#region GetResourceBookingTypes
        ///// <summary>
        ///// Get ResourceBookingTypes
        ///// </summary>
        ///// <param name="CompanyId">company id</param>
        ///// <param name="BranchId">branch id</param>
        ///// <returns></returns>
        //public ResponseResourceBookingTypeList GetResourceBookingTypes(string systemId, string branchId)
        //{
        //    ResponseResourceBookingTypeList responseResourceBookingTypeList;
        //    try
        //    {
        //        string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
        //        OperationResult<List<ExceIBookingResourceBookingType>> list = GMSExcelineIBooking.GetResourceBookingTypes(gymCode, Convert.ToInt32(branchId));
        //        if (list.ErrorOccured)
        //        {
        //            foreach (NotificationMessage message in list.Notifications)
        //            {
        //                USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
        //            }
        //            var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
        //            responseResourceBookingTypeList = new ResponseResourceBookingTypeList(null, status);
        //            return responseResourceBookingTypeList;
        //        }
        //        else
        //        {
        //            var resourceBookingTypeList = list.OperationReturnValue;
        //            if (resourceBookingTypeList.Count > 0)
        //            {
        //                var status = new ResponseStatus("1", "Successfull");
        //                responseResourceBookingTypeList = new ResponseResourceBookingTypeList(resourceBookingTypeList, status);
        //            }
        //            else
        //            {
        //                var status = new ResponseStatus("2", "No Resource Booking Type Found");
        //                responseResourceBookingTypeList = new ResponseResourceBookingTypeList(null, status);
        //            }
        //            return responseResourceBookingTypeList;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
        //        responseResourceBookingTypeList = new ResponseResourceBookingTypeList(null, status);
        //        return responseResourceBookingTypeList;
        //    }
        //}

        public ResponseResourceBookingTypeList GetResourceBookingTypes(string systemId, string branchId, string apiKey)
        {
            ResponseResourceBookingTypeList responseResourceBookingTypeList;
            try
            {

                if (AuthenticationManager.Authenticate("GetResourceBookingTypes", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingResourceBookingType>> list = GMSExcelineIBooking.GetResourceBookingTypes(gymCode, Convert.ToInt32(branchId));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseResourceBookingTypeList = new ResponseResourceBookingTypeList(null, status);
                        return responseResourceBookingTypeList;
                    }
                    else
                    {
                        var resourceBookingTypeList = list.OperationReturnValue;
                        if (resourceBookingTypeList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseResourceBookingTypeList = new ResponseResourceBookingTypeList(resourceBookingTypeList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Resource Booking Type Found");
                            responseResourceBookingTypeList = new ResponseResourceBookingTypeList(null, status);
                        }
                        return responseResourceBookingTypeList;
                    }
                }
                else
                {
                    var status = new ResponseStatus("6", "Access denied ");
                    responseResourceBookingTypeList = new ResponseResourceBookingTypeList(null, status);
                    return responseResourceBookingTypeList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseResourceBookingTypeList = new ResponseResourceBookingTypeList(null, status);
                return responseResourceBookingTypeList;
            }
        }



        //#endregion

        //#region GetResourceBooking
        ///// <summary>
        ///// Get ResourceBooking
        ///// </summary>
        ///// <param name="CompanyId">company id</param>
        ///// <param name="BranchId">branch id</param>
        ///// <param name="FromDate">from Date</param>
        ///// <param name="TopDate">to Date</param>
        ///// <returns></returns>


        //#region ResourceBooking

        public ResponseResourceBookingList GetResourceBooking(string systemId, string branchId, string fromDate, string toDate, string apiKey)
        {
            ResponseResourceBookingList responseResourceBookingList;
            try
            {
                if (AuthenticationManager.Authenticate("GetResourceBooking", apiKey, systemId, branchId))
                {
                    int localBranchId;
                    DateTime localFromDate;
                    DateTime localToDate;

                    if (Int32.TryParse(branchId, out localBranchId) && DateTime.TryParse(fromDate, out localFromDate) &&
                        DateTime.TryParse(toDate, out localToDate))
                    {
                        string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                        OperationResult<List<ExceIBookingResourceBooking>> list =
                            GMSExcelineIBooking.GetResourceBooking(gymCode, localBranchId, localFromDate, localToDate);
                        if (list.ErrorOccured)
                        {
                            foreach (NotificationMessage message in list.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                            }
                            var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                            responseResourceBookingList = new ResponseResourceBookingList(null, status);
                            return responseResourceBookingList;
                        }
                        else
                        {
                            var resourceBookingList = list.OperationReturnValue;
                            if (resourceBookingList.Count > 0)
                            {
                                var status = new ResponseStatus("1", "Successfull");
                                responseResourceBookingList = new ResponseResourceBookingList(resourceBookingList,
                                                                                              status);
                            }
                            else
                            {
                                var status = new ResponseStatus("2", "No Resource Booking Found");
                                responseResourceBookingList = new ResponseResourceBookingList(null, status);
                            }
                            return responseResourceBookingList;
                        }
                    }
                    else
                    {
                        var status = new ResponseStatus("3", "input not in a correct format");
                        responseResourceBookingList = new ResponseResourceBookingList(null, status);
                        return responseResourceBookingList;
                    }
                }
                else
                {
                    var status = new ResponseStatus("6", "Access denied ");
                    responseResourceBookingList = new ResponseResourceBookingList(null, status);
                    return responseResourceBookingList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseResourceBookingList = new ResponseResourceBookingList(null, status);
                return responseResourceBookingList;
            }
        }


        public ResponseResourceCategorieList GetResourceCategories(string systemId, string branchId, string apiKey)
        {
            ResponseResourceCategorieList responseResourceCategorieList;
            try
            {
                if (AuthenticationManager.Authenticate("GetResourceCategories", apiKey, systemId, branchId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<CategoryDC>> list = GMSExcelineIBooking.GetCategoriesByType("RESOURCE", gymCode);
                    if (list.ErrorOccured)
                    {

                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseResourceCategorieList = new ResponseResourceCategorieList(null, status);
                        return responseResourceCategorieList;
                    }
                    else
                    {
                        var resourceCategoryList = list.OperationReturnValue;
                        var resourceBookingCategoryList = new List<ExceIBookingResourceCategoty>();
                        if (resourceCategoryList.Count > 0)
                        {
                            resourceBookingCategoryList.AddRange(
                                resourceCategoryList.Select(cat => new ExceIBookingResourceCategoty
                                    {
                                        CategoryId = cat.Id,
                                        CategoryName = cat.Name
                                    }));
                            var status = new ResponseStatus("1", "Successfull");
                            responseResourceCategorieList =
                                new ResponseResourceCategorieList(resourceBookingCategoryList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Resource Category Found");
                            responseResourceCategorieList = new ResponseResourceCategorieList(null, status);
                        }
                        return responseResourceCategorieList;
                    }
                }
                else
                {
                    var status = new ResponseStatus("6", "Access denied ");
                    responseResourceCategorieList = new ResponseResourceCategorieList(null, status);
                    return responseResourceCategorieList;
                }

            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseResourceCategorieList = new ResponseResourceCategorieList(null, status);
                return responseResourceCategorieList;
            }
        }


        public ResponseResourceCategorieList GetResourceBookingCategories(string systemId, string branchId, string apiKey)
        {
            ResponseResourceCategorieList responseResourceCategorieList;
            try
            {
                if (AuthenticationManager.Authenticate("GetResourceBookingCategories", apiKey, systemId, branchId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<CategoryDC>> list = GMSExcelineIBooking.GetCategoriesByType("BOOKINGCATEGORY",
                                                                                                     gymCode);
                    if (list.ErrorOccured)
                    {

                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseResourceCategorieList = new ResponseResourceCategorieList(null, status);
                        return responseResourceCategorieList;
                    }
                    else
                    {
                        var resourceCategoryList = list.OperationReturnValue;
                        var resourceBookingCategoryList = new List<ExceIBookingResourceCategoty>();
                        if (resourceCategoryList.Count > 0)
                        {

                            resourceBookingCategoryList.AddRange(
                                resourceCategoryList.Select(cat => new ExceIBookingResourceCategoty
                                    {
                                        CategoryId = cat.Id,
                                        CategoryName = cat.Name,
                                        CategoryColor = cat.Color
                                    }));
                            var status = new ResponseStatus("1", "Successfull");
                            responseResourceCategorieList =
                                new ResponseResourceCategorieList(resourceBookingCategoryList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Resource Booking Category Found");
                            responseResourceCategorieList = new ResponseResourceCategorieList(null, status);
                        }
                        return responseResourceCategorieList;
                    }
                }
                else
                {
                    var status = new ResponseStatus("6", "Access denied ");
                    responseResourceCategorieList = new ResponseResourceCategorieList(null, status);
                    return responseResourceCategorieList;
                }

            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseResourceCategorieList = new ResponseResourceCategorieList(null, status);
                return responseResourceCategorieList;
            }
        }


        public ResponseResourceList GetResources(string systemId, string branchId, string apiKey)
        {
            ResponseResourceList responseResourceList;
            try
            {
                if (AuthenticationManager.Authenticate("GetResources", apiKey, systemId, branchId))
                {
                    int localBranchId;
                    if (Int32.TryParse(branchId, out localBranchId))
                    {
                        string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                        OperationResult<List<ExceIBookingResource>> list = GMSExcelineIBooking.GetResources(gymCode,
                                                                                                            localBranchId);
                        if (list.ErrorOccured)
                        {
                            foreach (NotificationMessage message in list.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                            }
                            var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                            responseResourceList = new ResponseResourceList(null, status);
                            return responseResourceList;
                        }
                        else
                        {
                            var resourceList = list.OperationReturnValue;
                            if (resourceList.Count > 0)
                            {
                                var status = new ResponseStatus("1", "Successfull");
                                responseResourceList = new ResponseResourceList(resourceList, status);
                            }
                            else
                            {
                                var status = new ResponseStatus("2", "No Resource Found");
                                responseResourceList = new ResponseResourceList(null, status);
                            }
                            return responseResourceList;
                        }
                    }
                    else
                    {
                        var status = new ResponseStatus("3", "input not in a correct format");
                        responseResourceList = new ResponseResourceList(null, status);
                        return responseResourceList;
                    }
                }
                else
                {
                    var status = new ResponseStatus("6", "Access denied ");
                    responseResourceList = new ResponseResourceList(null, status);
                    return responseResourceList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseResourceList = new ResponseResourceList(null, status);
                return responseResourceList;
            }
        }


        public ResponseResourceScheduleList GetResourceSchedules(string systemId, string branchId, string fromDate, string toDate, string resourceId, string apiKey)
        {
            ResponseResourceScheduleList responseResourceList;
            try
            {
                DateTime startDate;
                DateTime endDate;
                if (DateTime.TryParse(fromDate, out startDate) &&
                   DateTime.TryParse(toDate, out endDate))
                {
                    if (AuthenticationManager.Authenticate("GetResourceSchedules", apiKey, systemId, branchId))
                    {
                        string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                        OperationResult<List<ExceIBookingResourceSchedule>> list =
                            GMSExcelineIBooking.GetResourceSchedules(gymCode, Convert.ToInt32(branchId),
                                                                     Convert.ToInt32(resourceId), startDate, endDate);
                        if (list.ErrorOccured)
                        {
                            foreach (NotificationMessage message in list.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                            }
                            var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                            responseResourceList = new ResponseResourceScheduleList(null, status);
                            return responseResourceList;
                        }
                        else
                        {
                            var resourceList = list.OperationReturnValue;
                            if (resourceList.Count > 0)
                            {
                                var status = new ResponseStatus("1", "Successfull");
                                responseResourceList = new ResponseResourceScheduleList(resourceList, status);
                            }
                            else
                            {
                                var status = new ResponseStatus("2", "No Resouce Schedule Found");
                                responseResourceList = new ResponseResourceScheduleList(null, status);
                            }
                            return responseResourceList;
                        }
                    }
                    else
                    {
                        var status = new ResponseStatus("6", "Access denied ");
                        responseResourceList = new ResponseResourceScheduleList(null, status);
                        return responseResourceList;
                    }
                }
                else
                {
                    var status = new ResponseStatus("3", "Error in datetime formate");
                    responseResourceList = new ResponseResourceScheduleList(null, status);
                    return responseResourceList;
                }


            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseResourceList = new ResponseResourceScheduleList(null, status);
                return responseResourceList;
            }
        }


        public ResponseResourceAvailableTimeList GetResourceAvailableTimes(string systemId, string branchId, string fromDate, string toDate, string resourceId, string apiKey)
        {
            ResponseResourceAvailableTimeList resourceAvailableTimeList;
            try
            {
                if (AuthenticationManager.Authenticate("GetResourceAvailableTimes", apiKey, systemId, branchId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingResourceAvailableTime>> list =
                        GMSExcelineIBooking.GetResourceAvailableTimes(gymCode, Convert.ToInt32(branchId),
                                                                      Convert.ToInt32(resourceId));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        resourceAvailableTimeList = new ResponseResourceAvailableTimeList(null, status);
                        return resourceAvailableTimeList;
                    }
                    else
                    {
                        var resourceList = list.OperationReturnValue;
                        if (resourceList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            resourceAvailableTimeList = new ResponseResourceAvailableTimeList(resourceList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Resouce AvailableTime Found");
                            resourceAvailableTimeList = new ResponseResourceAvailableTimeList(null, status);
                        }
                        return resourceAvailableTimeList;
                    }
                }
                else
                {
                    var status = new ResponseStatus("6", "Access denied ");
                    resourceAvailableTimeList = new ResponseResourceAvailableTimeList(null, status);
                    return resourceAvailableTimeList;
                }

            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command" + ex.Message);
                resourceAvailableTimeList = new ResponseResourceAvailableTimeList(null, status);
                return resourceAvailableTimeList;
            }
        }


        public ResponseResourceAddBooking AddResourceBooking(ExceIBookingResourceBooking resourceBooking, string apiKey)
        {
            ResponseResourceAddBooking responseResourceAddBooking;
            try
            {

                if (AuthenticationManager.Authenticate("AddResourceBooking", apiKey, Convert.ToString(resourceBooking.SystemId), Convert.ToString(resourceBooking.BranchId)))
                {
                    DateTime fromDate;
                    DateTime toDate;
                    if (DateTime.TryParse(resourceBooking.FromDateTime, out fromDate) &&
                        DateTime.TryParse(resourceBooking.ToDateTime, out toDate))
                    {
                        string gymCode =
                            GMSExcelineIBooking.GetGymCodeByCompanyId(Convert.ToString(resourceBooking.SystemId))
                                               .OperationReturnValue;
                        var resList = new List<int> { resourceBooking.ResourceId };
                        //if (resourceBooking.ExtraResourceIdList != null)
                        //    resourceBooking.ExtraResourceIdList.ForEach(resList.Add);

                        OperationResult<Dictionary<int, int>> result = GMSExcelineIBooking.ValidateAvailableForBooking(gymCode, resList, fromDate, toDate);

                        Dictionary<int, int> scheduleItemIdList = result.OperationReturnValue;
                        if (scheduleItemIdList != null)
                        {
                            if (!scheduleItemIdList.ToList().Any(x => x.Value <= 0))
                            {
                                resourceBooking.ScheduleItemIdList = new Dictionary<int, int>();
                                //set the main resouce as first item
                                scheduleItemIdList.Where(x => x.Key == resourceBooking.ResourceId)
                                                  .ToList()
                                                  .ForEach(x => resourceBooking.ScheduleItemIdList.Add(x.Key, x.Value));
                                scheduleItemIdList.Where(x => x.Key != resourceBooking.ResourceId)
                                                  .ToList()
                                                  .ForEach(x => resourceBooking.ScheduleItemIdList.Add(x.Key, x.Value));

                                if (scheduleItemIdList.ContainsKey(resourceBooking.ResourceId))
                                    resourceBooking.ScheduleItemId = scheduleItemIdList[resourceBooking.ResourceId];

                                int saveStatus =
                                    GMSExcelineIBooking.AddResourceBooking(gymCode, resourceBooking)
                                                       .OperationReturnValue;
                                if (saveStatus > 0)
                                {
                                    var status = new ResponseStatus("1", "Successfull");
                                    responseResourceAddBooking = new ResponseResourceAddBooking(saveStatus, status);
                                    return responseResourceAddBooking;
                                }
                                else if (saveStatus == -2)
                                {
                                    var status = new ResponseStatus("3", "MemberIds not valid");
                                    responseResourceAddBooking = new ResponseResourceAddBooking(-1, status);
                                    return responseResourceAddBooking;
                                }
                                else if (saveStatus == -3)
                                {
                                    var status = new ResponseStatus("3", "ResourceIds not valid");
                                    responseResourceAddBooking = new ResponseResourceAddBooking(-1, status);
                                    return responseResourceAddBooking;

                                }
                                else
                                {
                                    var status = new ResponseStatus("2", "Error Saving Booking Details");
                                    responseResourceAddBooking = new ResponseResourceAddBooking(-1, status);
                                    return responseResourceAddBooking;

                                }
                            }
                            else
                            {
                                int invalidResId = scheduleItemIdList.ToList().FirstOrDefault(x => x.Value == -1).Key;
                                string message = "There are no available time";
                                // "there isn't available time for resource id :" + invalidResId;
                                var status = new ResponseStatus("4", message);
                                responseResourceAddBooking = new ResponseResourceAddBooking(-1, status);
                                return responseResourceAddBooking;
                            }
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "Error Saving Booking Details");
                            responseResourceAddBooking = new ResponseResourceAddBooking(-1, status);
                            return responseResourceAddBooking;
                        }
                    }
                    else
                    {
                        var status = new ResponseStatus("3", "Error in datetime formate");
                        responseResourceAddBooking = new ResponseResourceAddBooking(-1, status);
                        return responseResourceAddBooking;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseResourceAddBooking = new ResponseResourceAddBooking(-1, accessstatus);
                    return responseResourceAddBooking;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseResourceAddBooking = new ResponseResourceAddBooking(-1, status);
                return responseResourceAddBooking;
            }
        }

        public ResponseStatus DeleteResourceBooking(string systemId, string branchId, string bookingId, string apiKey)
        {
            try
            {
                if (AuthenticationManager.Authenticate("DeleteResourceBooking", apiKey, systemId, branchId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    //if (validCustomerId)
                    //{
                    int saveStatus = GMSExcelineIBooking.DeleteResourceBooking(gymCode, Convert.ToInt32(bookingId), Convert.ToInt32(branchId)).OperationReturnValue;
                    if (saveStatus > 0)
                    {
                        var status = new ResponseStatus("1", "Successfull");
                        return status;
                    }
                    else if (saveStatus == -2)
                    {
                        var status = new ResponseStatus("3", "BookingId is not valid");
                        return status;
                    }
                    else
                    {
                        var status = new ResponseStatus("2", "Error Deleting Booking");
                        return status;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }
        }

        public ResponseStatus SetResourceBookingAsPaid(ExceIBookingResourceBookingPaid resourceBookingPaid, string apiKey)
        {
            try
            {
                //if (AuthenticationManager.Authenticate("SetResourceBookingAsPaid", apiKey, Convert.ToString(resourceBookingPaid.SystemId), Convert.ToString(resourceBookingPaid.GymID)))
                if (AuthenticationManager.Authenticate("SetResourceBookingAsPaid", apiKey, Convert.ToString(resourceBookingPaid.SystemId)))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(resourceBookingPaid.SystemId.ToString()).OperationReturnValue;
                    OperationResult<int> result = GMSExcelineIBooking.SetResourceBookingAsPaid(resourceBookingPaid, gymCode);
                    if (result.OperationReturnValue == 1)
                    {
                        var status = new ResponseStatus("1", "Successfull");
                        return status;
                    }
                    else if (result.OperationReturnValue == 2)
                    {
                        var status = new ResponseStatus("2", "Error Adding Payment");
                        return status;
                    }
                    else if (result.OperationReturnValue == 3)
                    {
                        var status = new ResponseStatus("2", "Error Adding Payment");
                        return status;
                    }
                    else if (result.OperationReturnValue == 4)
                    {
                        var status = new ResponseStatus("3", "Booking Not Found");
                        return status;
                    }
                    else if (result.OperationReturnValue == 5)
                    {
                        var status = new ResponseStatus("5", "Error Executing Command " + result.Notifications[0]);
                        return status;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }

            var status1 = new ResponseStatus("5", "Error Executing Command ");
            return status1;
        }

        //#endregion

        //#endregion


        public ResponseInvoiceList GetInvoices(string systemID, string gymID, string fromDate, string toDate, string apiKey)
        {
            ResponseInvoiceList invoiceListResponse;
            try
            {
                if (AuthenticationManager.Authenticate("GetInvoices", apiKey, systemID, gymID))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemID).OperationReturnValue;
                    string DocumentSrvice = ConfigurationManager.AppSettings["DocumentServiceURL"].ToString();
                    string FileFolderPath = ConfigurationManager.AppSettings["USC_Configuration_Folder"].ToString() + @"\OUTPUT\PDF";
                    OperationResult<List<ExceIBookingInvoice>> list = GMSExcelineIBooking.GetInvoices(Convert.ToInt32(gymID), gymCode, Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), DocumentSrvice, FileFolderPath);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        invoiceListResponse = new ResponseInvoiceList(null, status);
                        return invoiceListResponse;
                    }
                    else
                    {
                        var invoiceList = list.OperationReturnValue;
                        if (invoiceList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            invoiceListResponse = new ResponseInvoiceList(invoiceList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Invoices Found");
                            invoiceListResponse = new ResponseInvoiceList(null, status);
                        }
                        return invoiceListResponse;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    invoiceListResponse = new ResponseInvoiceList(null, accessstatus);
                    return invoiceListResponse;
                }

            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                invoiceListResponse = new ResponseInvoiceList(null, status);
                return invoiceListResponse;
            }
        }





        //#region GetInterests
        ///// <summary>
        ///// Get All Interests
        ///// </summary>
        ///// <returns></returns>
        public ResponseInterestList GetInterests(string systemId, string apiKey)
        {
            ResponseInterestList responseInterestList;
            try
            {
                if (AuthenticationManager.Authenticate("GetInterests", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingInterest>> list = GMSExcelineIBooking.GetIBookingInterests(gymCode);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseInterestList = new ResponseInterestList(null, status);
                        return responseInterestList;
                    }
                    else
                    {
                        var interestList = list.OperationReturnValue;
                        if (interestList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseInterestList = new ResponseInterestList(interestList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Interests Found");
                            responseInterestList = new ResponseInterestList(null, status);
                        }
                        return responseInterestList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseInterestList = new ResponseInterestList(null, accessstatus);
                    return responseInterestList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseInterestList = new ResponseInterestList(null, status);
                return responseInterestList;
            }
        }
        //#endregion

        //#region Member class visit

        public ResponseStatus UpdateMemberClassVisits(ExceIBookingClassVisit classVisit, string apiKey)
        {
            try
            {
                if (AuthenticationManager.Authenticate("UpdateMemberClassVisits", apiKey, classVisit.SystemId, classVisit.GymID))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(classVisit.SystemId).OperationReturnValue;
                    int visitStatus = GMSExcelineIBooking.AddClassVisit(classVisit.GymID, classVisit.ClassID, classVisit.Visits, gymCode);
                    if (visitStatus > 0)
                    {
                        var status = new ResponseStatus("1", "Successfull");
                        return status;
                    }
                    else
                    {
                        var status = new ResponseStatus("2", "Error Saving Booking Details");
                        return status;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }
        }




        //#endregion 

        //#region GetGyms
        public ResponseMemberGymDetailList GetMemberGyms(string systemID, string apiKey)
        {
            ResponseMemberGymDetailList responseMemberGymList;
            try
            {
                if (AuthenticationManager.Authenticate("GetMemberGyms", apiKey, systemID))
                {

                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemID).OperationReturnValue;
                    OperationResult<List<ExceIBookingMemberGymDetail>> list = GMSExcelineIBooking.GetIBookingMemberGyms(gymCode);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseMemberGymList = new ResponseMemberGymDetailList(null, status);
                        return responseMemberGymList;
                    }
                    else
                    {
                        var memberList = list.OperationReturnValue;
                        if (memberList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseMemberGymList = new ResponseMemberGymDetailList(memberList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Members Found");
                            responseMemberGymList = new ResponseMemberGymDetailList(null, status);
                        }
                        return responseMemberGymList;
                    }

                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseMemberGymList = new ResponseMemberGymDetailList(null, accessstatus);
                    return responseMemberGymList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseMemberGymList = new ResponseMemberGymDetailList(null, status);
                return responseMemberGymList;
            }
        }
        //#endregion

        #region GetMembersModifiedAfterAGivenDate
        public ResponseMemberModifiedList GetMembersUpdatedByDate(string systemId, string gymId, string changedSinceDays, string apiKey)
        {
            ResponseMemberModifiedList responseMemberList;

            try
            {
                if (AuthenticationManager.Authenticate("GetMembersUpdatedByDate", apiKey, systemId, gymId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId.ToString()).OperationReturnValue;
                    OperationResult<List<ExceIBookingModifiedMember>> memberList = GMSExcelineIBooking.GetMembersUpdatedByDate(changedSinceDays, systemId, gymId, gymCode);

                    if (memberList.ErrorOccured)
                    {
                        foreach (NotificationMessage message in memberList.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", memberList.Notifications.FirstOrDefault().Message);
                        responseMemberList = new ResponseMemberModifiedList(null, status);
                        return responseMemberList;
                    }
                    else
                    {
                        var memberListTemp = memberList.OperationReturnValue;
                        if (memberListTemp.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseMemberList = new ResponseMemberModifiedList(memberListTemp, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Members Found");
                            responseMemberList = new ResponseMemberModifiedList(null, status);
                        }
                        return responseMemberList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseMemberList = new ResponseMemberModifiedList(null, accessstatus);
                    return responseMemberList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseMemberList = new ResponseMemberModifiedList(null, status);
                return responseMemberList;
            }


        }
        #endregion


        //#region GetAllContracts
        ///// <summary>
        ///// Get all contracts
        ///// </summary>
        ///// <param name="bookingDetails">booking details</param>
        ///// <returns></returns>
       
        public ResponseAllContractList GetAllContracts(string systemId, string apiKey)
        {
            ResponseAllContractList responseContractList;

            try
            {
                if (AuthenticationManager.Authenticate("GetAllContracts", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId.ToString()).OperationReturnValue;
                    OperationResult<List<ExceIBookingAllContracts>> contractList = GMSExcelineIBooking.GetAllContracts(systemId, gymCode);

                    if (contractList.ErrorOccured)
                    {
                        foreach (NotificationMessage message in contractList.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", contractList.Notifications.FirstOrDefault().Message);
                        responseContractList = new ResponseAllContractList(null, status);
                        return responseContractList;
                    }
                    else
                    {
                        var contractListTemp = contractList.OperationReturnValue;
                        if (contractListTemp.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseContractList = new ResponseAllContractList(contractListTemp, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No contracts Found");
                            responseContractList = new ResponseAllContractList(null, status);
                        }
                        return responseContractList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseContractList = new ResponseAllContractList(null, accessstatus);
                    return responseContractList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseContractList = new ResponseAllContractList(null, status);
                return responseContractList;
            }


        }        


        //#region RegisterNewFreeze
        ///// <summary>
        ///// Register New member
        ///// </summary>
        ///// <param name="bookingDetails">booking details</param>
        ///// <returns></returns>

        public ResponseStatus RegisterNewFreeze(ExceIBookingNewFreeze freezeItem, string apiKey)
        {
            string gymCode = string.Empty;                                    

            try
            {
                if (AuthenticationManager.Authenticate("RegisterNewFreeze", apiKey, freezeItem.SystemId, freezeItem.GymId))
                {                    
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(Convert.ToString(freezeItem.SystemId)).OperationReturnValue;

                    int saveStatusDetails = GMSExcelineIBooking.RegisterNewFreeze(gymCode, freezeItem).OperationReturnValue;                    

                    if (saveStatusDetails > 0)
                    {
                        //manage the notification 
                        var status = new ResponseStatus("1", "Successful");                        

                        return status;
                    }

                    else
                    {
                        var accessstatus = new ResponseStatus("5", "Error Executing Command ");
                        return accessstatus;
                    }

                }

                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }

            }
            
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }
            
        }


        //#region RegisterNewFreeze
        ///// <summary>
        ///// Register New member
        ///// </summary>
        ///// <param name="bookingDetails">booking details</param>
        ///// <returns></returns>

        public ResponseStatus RegisterResign(ExceIBookingNewResign resignItem, string apiKey)
        {            
            string gymCode = string.Empty;

            try
            {
                if (AuthenticationManager.Authenticate("RegisterResign", apiKey, resignItem.SystemId))
                {
                    gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(Convert.ToString(resignItem.SystemId)).OperationReturnValue;

                    int saveStatusDetails = GMSExcelineIBooking.RegisterResign(gymCode, resignItem).OperationReturnValue;

                    if (saveStatusDetails == 1)
                    {
                        //manage the notification 
                        var status = new ResponseStatus("1", "Successful");

                        return status;
                    }

                    else
                    {
                        var accessstatus = new ResponseStatus("5", "Error Executing Command ");
                        return accessstatus;
                    }

                }

                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    return accessstatus;
                }

            }

            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                return status;
            }

        }
        //#endregion

        //#region GetMemberById
        ///// <summary>
        ///// GetMemberData
        ///// </summary>
        ///// <param name="systemId">company id</param>
        ///// <param name="branchId">branch id</param>
        ///// <returns></returns>
        public ResponseContractList GetContractByMemberId(string systemID, string customerNo, string apiKey)
        {
            ResponseContractList responseContractList;
            try
            {
                if (AuthenticationManager.Authenticate("GetContractByMemberId", apiKey, systemID))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemID).OperationReturnValue;
                    OperationResult<List<ExceIBookingContractFreeze>> list = GMSExcelineIBooking.GetContractByMemberId(gymCode, customerNo, Convert.ToInt32(systemID));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseContractList = new ResponseContractList(null, status);
                        return responseContractList;
                    }
                    else
                    {
                        var memberList = list.OperationReturnValue;
                        if (memberList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseContractList = new ResponseContractList(memberList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Contract Found");
                            responseContractList = new ResponseContractList(null, status);
                        }
                        return responseContractList;
                    }

                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseContractList = new ResponseContractList(null, accessstatus);
                    return responseContractList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseContractList = new ResponseContractList(null, status);
                return responseContractList;
            }
        }
        //#endregion


        //#region GetFreezeByCustId
        ///// <summary>
        ///// GetMemberData
        ///// </summary>
        ///// <param name="systemId">company id</param>
        ///// <param name="branchId">branch id</param>
        ///// <returns></returns>
        public ResponseFreezeList GetFreezeByCustId(string systemID, string memberId, string apiKey)
        {
            ResponseFreezeList responseFreezeList;
            try
            {
                if (AuthenticationManager.Authenticate("GetFreezeByCustId", apiKey, systemID))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemID).OperationReturnValue;
                    OperationResult<List<ExceIBookingListFreeze>> list = GMSExcelineIBooking.GetFreezeByCustId(gymCode, memberId, Convert.ToInt32(systemID));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseFreezeList = new ResponseFreezeList(null, status);
                        return responseFreezeList;
                    }
                    else
                    {
                        var memberFreezeList = list.OperationReturnValue;
                        if (memberFreezeList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successful");
                            responseFreezeList = new ResponseFreezeList(memberFreezeList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Freeze Found");
                            responseFreezeList = new ResponseFreezeList(null, status);
                        }
                        return responseFreezeList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseFreezeList = new ResponseFreezeList(null, accessstatus);
                    return responseFreezeList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseFreezeList = new ResponseFreezeList(null, status);
                return responseFreezeList;
            }
        }
        //#endregion


        //#region GetMemberData
        ///// <summary>
        ///// GetMemberData
        ///// </summary>
        ///// <param name="systemId">company id</param>
        ///// <param name="branchId">branch id</param>
        ///// <returns></returns>
        public ResponseAPIMemberList GetAPIMembers(string systemID, string gymId, string changedSinceDays, string apiKey)
        {
            ResponseAPIMemberList responseAPIMemberList;
            try
            {

                if (AuthenticationManager.Authenticate("GetAPIMembers", apiKey, systemID, gymId))
                {

                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemID).OperationReturnValue;
                    OperationResult<List<ExceAPIMember>> list = GMSExcelineIBooking.GetAPIMembers(gymCode, Convert.ToInt32(gymId), Convert.ToInt32(changedSinceDays), Convert.ToInt32(systemID));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseAPIMemberList = new ResponseAPIMemberList(null, status);
                        return responseAPIMemberList;
                    }
                    else
                    {
                        var memberList = list.OperationReturnValue;
                        if (memberList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseAPIMemberList = new ResponseAPIMemberList(memberList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Members Found");
                            responseAPIMemberList = new ResponseAPIMemberList(null, status);
                        }
                        return responseAPIMemberList;
                    }

                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseAPIMemberList = new ResponseAPIMemberList(null, accessstatus);
                    return responseAPIMemberList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseAPIMemberList = new ResponseAPIMemberList(null, status);
                return responseAPIMemberList;
            }
        }

        //#region GetFreezeResignCategories
        ///// <summary>
        ///// Get GetFreezeResignCategories
        ///// </summary>
        ///// <param name="systemId">systemId</param>
        ///// <param name="branchId">branch id</param>
        ///// <returns></returns>
        public ResponseFreezeResignCategoryList GetFreezeResignCategories(string systemId, string apiKey)
        {
            ResponseFreezeResignCategoryList responseFreezeResignCategoryList;
            try
            {
                if (AuthenticationManager.Authenticate("GetFreezeResignCategories", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<ExceIBookingFreezeResignCategories>> list = GMSExcelineIBooking.GetFreezeResignCategories(gymCode);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseFreezeResignCategoryList = new ResponseFreezeResignCategoryList(null, status);
                        return responseFreezeResignCategoryList;
                    }
                    else
                    {
                        var classList = list.OperationReturnValue;
                        if (classList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseFreezeResignCategoryList = new ResponseFreezeResignCategoryList(classList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Freeze/Resign categories Found");
                            responseFreezeResignCategoryList = new ResponseFreezeResignCategoryList(null, status);
                        }
                        return responseFreezeResignCategoryList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseFreezeResignCategoryList = new ResponseFreezeResignCategoryList(null, accessstatus);
                    return responseFreezeResignCategoryList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseFreezeResignCategoryList = new ResponseFreezeResignCategoryList(null, status);
                return responseFreezeResignCategoryList;
            }
        }
        //#endregion

        //#region GetBrisIdActiveFreezed
        ///// <summary>
        ///// Get GetFreezeResignCategories
        ///// </summary>
        ///// <param name="systemId">systemId</param>
        ///// <param name="branchId">branch id</param>
        ///// <returns></returns>
        public ResponseBrisIdActiveFreezedList GetBrisIdActiveFreezed(string systemId, string apiKey)
        {
            ResponseBrisIdActiveFreezedList responseBrisIdActiveFreezedList;
            try
            {
                if (AuthenticationManager.Authenticate("GetBrisIdActiveFreezed", apiKey, systemId))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemId).OperationReturnValue;
                    OperationResult<List<int?>> list = GMSExcelineIBooking.GetBrisIdActiveFreezed(gymCode);
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "BrisId");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseBrisIdActiveFreezedList = new ResponseBrisIdActiveFreezedList(null, status);
                        return responseBrisIdActiveFreezedList;
                    }
                    else
                    {
                        var classList = list.OperationReturnValue;
                        if (classList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseBrisIdActiveFreezedList = new ResponseBrisIdActiveFreezedList(classList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No BrisIds Found");
                            responseBrisIdActiveFreezedList = new ResponseBrisIdActiveFreezedList(null, status);
                        }
                        return responseBrisIdActiveFreezedList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseBrisIdActiveFreezedList = new ResponseBrisIdActiveFreezedList(null, accessstatus);
                    return responseBrisIdActiveFreezedList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseBrisIdActiveFreezedList = new ResponseBrisIdActiveFreezedList(null, status);
                return responseBrisIdActiveFreezedList;
            }
        }
        //#endregion


        public ResponseMemberList NTNUIGetMemberDataChanges(string systemID, string changedSinceDays, string apiKey)
        {
            ResponseMemberList responseMemberList;
            try
            {

                if (AuthenticationManager.Authenticate("NTNUIGetMemberDataChanges", apiKey, systemID))
                {

                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemID).OperationReturnValue;
                    OperationResult<List<ExceIBookingMember>> list = GMSExcelineIBooking.NTNUIGetIBookingMembers(gymCode, Convert.ToInt32(changedSinceDays), Convert.ToInt32(systemID));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseMemberList = new ResponseMemberList(null, status);
                        return responseMemberList;
                    }
                    else
                    {
                        var memberList = list.OperationReturnValue;
                        if (memberList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successfull");
                            responseMemberList = new ResponseMemberList(memberList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Members Found");
                            responseMemberList = new ResponseMemberList(null, status);
                        }
                        return responseMemberList;
                    }

                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseMemberList = new ResponseMemberList(null, accessstatus);
                    return responseMemberList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseMemberList = new ResponseMemberList(null, status);
                return responseMemberList;
            }
        }



        //#region GetFreezeByCustId
        ///// <summary>
        ///// GetMemberData
        ///// </summary>
        ///// <param name="systemId">company id</param>
        ///// <param name="branchId">branch id</param>
        ///// <returns></returns>
        public ResponseVisitList GetVisitCount(string systemID, string branchId, string year, string apiKey)
        {
            ResponseVisitList responseVisitList;
            try
            {
                if (AuthenticationManager.Authenticate("GetVisitCount", apiKey, systemID))
                {
                    string gymCode = GMSExcelineIBooking.GetGymCodeByCompanyId(systemID).OperationReturnValue;
                    OperationResult<List<ExceIBookingVisitCount>> list = GMSExcelineIBooking.GetVisitCount(gymCode, Convert.ToInt32(branchId), Convert.ToInt32(year), Convert.ToInt32(systemID));
                    if (list.ErrorOccured)
                    {
                        foreach (NotificationMessage message in list.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), "IBooking");
                        }
                        var status = new ResponseStatus("3", list.Notifications.FirstOrDefault().Message);
                        responseVisitList = new ResponseVisitList(null, status);
                        return responseVisitList;
                    }
                    else
                    {
                        var VisitList = list.OperationReturnValue;
                        if (VisitList.Count > 0)
                        {
                            var status = new ResponseStatus("1", "Successful");
                            responseVisitList = new ResponseVisitList(VisitList, status);
                        }
                        else
                        {
                            var status = new ResponseStatus("2", "No Count Found");
                            responseVisitList = new ResponseVisitList(null, status);
                        }
                        return responseVisitList;
                    }
                }
                else
                {
                    var accessstatus = new ResponseStatus("6", "Access denied ");
                    responseVisitList = new ResponseVisitList(null, accessstatus);
                    return responseVisitList;
                }
            }
            catch (Exception ex)
            {
                var status = new ResponseStatus("5", "Error Executing Command " + ex.Message);
                responseVisitList = new ResponseVisitList(null, status);
                return responseVisitList;
            }
        }
        //#endregion



    }
}
