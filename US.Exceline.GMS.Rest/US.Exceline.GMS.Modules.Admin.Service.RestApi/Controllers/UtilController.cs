﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.GMS.API;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.Admin.Service.RestApi.Controllers
{
    [RoutePrefix("api/Admin/Util")]
    public class UtilController : ApiController
    {
        [HttpGet]
        [Route("GetCategory")]
        [Authorize]
        public HttpResponseMessage GetCategories(string type)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<CategoryDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetCategoryTypes")]
        [Authorize]
        public HttpResponseMessage GetCategoryTypes(int branchId, string name)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSCategory.GetCategoryTypes(name, user, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<CategoryDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }
        [HttpPost]
        [Route("SaveCategory")]
        [Authorize]
        public HttpResponseMessage SaveCategory(CategoryDC category,int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSCategory.SaveCategory(category, user, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("UpdateCategory")]
        [Authorize]
        public HttpResponseMessage UpdateCategory(CategoryDC category, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSCategory.UpdateCategory(category, user, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

 

    }
}